<?php
	$url=$_GET['url'];
	$info=getimagesize($url);
	if($info[2]==IMAGETYPE_JPEG) {
		$img_source=imagecreatefromjpeg($url);
	} else if($info[2]==IMAGETYPE_GIF) {
		$img_source=imagecreatefromgif($url);
	} else if($info[2]==IMAGETYPE_PNG) {
		$img_source=imagecreatefrompng($url);
	}
	$newwidth=$_GET['wid'];
	$newheight=$_GET['hei'];
	$ratio=$newwidth/$newheight;
	$owidth=$info[0];
	$oheight=$info[1];
	$s_ratio=$owidth/$oheight;
	
	$thumbimg = imagecreatetruecolor($newwidth,$newheight);
    if($info[2]==IMAGETYPE_PNG){
	        imagealphablending($thumbimg, false);
                imagesavealpha($thumbimg, true);	
		}
	if($s_ratio>$ratio) {
		$src_x=($owidth-($newwidth*($oheight/$newheight)))/2;
		$src_width=$newwidth*($oheight/$newheight);
		
		imagecopyresampled($thumbimg, $img_source, 0, 0, $src_x, 0, $newwidth, $newheight, $src_width, $oheight);
	} else {
		$src_y=($oheight-($newheight*($owidth/$newwidth)))/2;
		$src_height=$newheight*($owidth/$newwidth);
		imagecopyresampled($thumbimg, $img_source, 0, 0, 0, $src_y, $newwidth, $newheight, $owidth, $src_height);
	}
	if($info[2]==IMAGETYPE_JPEG) {
		header('Content-Type: image/jpeg');
		imagejpeg($thumbimg);
	} else if($info[2]==IMAGETYPE_GIF) {
		header('Content-Type: image/gif');
		imagegif($thumbimg);
	} else if($info[2]==IMAGETYPE_PNG) {
		
		header('Content-Type: image/png');
		imagepng($thumbimg);
	}
?>
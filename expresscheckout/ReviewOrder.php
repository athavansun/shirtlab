<?php
include_once('../config/configure.php');

/********************************************
ReviewOrder.php

This file is called after the user clicks on a button during
the checkout process to use PayPal's Express Checkout. The
user logs in to their PayPal account.

This file is called twice.

On the first pass, the code executes the if statement:

if (! isset ($token))

The code collects transaction parameters from the form
displayed by SetExpressCheckout.html then constructs and
sends a SetExpressCheckout request string to the PayPal
server. The paymentType variable becomes the PAYMENTACTION
parameter of the request string. The RETURNURL parameter
is set to this file; this is how ReviewOrder.php is called
twice.

On the second pass, the code executes the else statement.

On the first pass, the buyer completed the authorization in
their PayPal account; now the code gets the payer details
by sending a GetExpressCheckoutDetails request to the PayPal
server. Then the code calls GetExpressCheckoutDetails.php.

Note: Be sure to check the value of PAYPAL_URL. The buyer is
sent to this URL to authorize payment with their PayPal
account. For testing purposes, this should be set to the
PayPal sandbox.

Called by SetExpressCheckout.html.

Calls GetExpressCheckoutDetails.php, CallerService.php,
and APIError.php.

********************************************/

require_once 'CallerService.php';


session_start();



/* An express checkout transaction starts with a token, that
   identifies to PayPal your transaction
   In this example, when the script sees a token, the script
   knows that the buyer has already authorized payment through
   paypal.  If no token was found, the action is to send the buyer
   to PayPal to first authorize payment
   */

$token = $_REQUEST['token'];

if(! isset($token)) {

		/* The servername and serverport tells PayPal where the buyer
		   should be directed back to after authorizing payment.
		   In this case, its the local webserver that is running this script
		   Using the servername and serverport, the return URL is the first
		   portion of the URL that buyers will return to after authorizing payment
		   */
		   $serverName = $_SERVER['SERVER_NAME'];
		   $serverPort = $_SERVER['SERVER_PORT'];
		   //$url=dirname('http://'.$serverName.':'.$serverPort.$_SERVER['REQUEST_URI']);                   
                   $url = 'http://'.$_SERVER[HTTP_HOST].'/new';
                   $url = SITE_URL;
                   
		   $currencyCodeType=$_REQUEST['currencyCodeType'];
		   $paymentType=$_REQUEST['paymentType'];

                   $personName        = $_REQUEST['PERSONNAME'];
		   $SHIPTOSTREET      = $_REQUEST['SHIPTOSTREET'];
		   $SHIPTOCITY        = $_REQUEST['SHIPTOCITY'];		   
		   $SHIPTOCOUNTRYCODE = $_REQUEST['SHIPTOCOUNTRYCODE'];
		   $SHIPTOZIP         = $_REQUEST['SHIPTOZIP'];
                   
                   //=============changes start here==============                   
                   $orderId = $_REQUEST['orderId'];
                   $total  = $_REQUEST['total'];
                   $discount = number_format($_REQUEST['discount'], 2, ".", "");
                   $vat = number_format($_REQUEST['vat'], 2, '.', '');
                   $itemamt = 0.00;
                   $queryString = '';
                   $amt = 0.00;
                   for($i = 0; $i < $total; $i++) {
                       //$name    = 'L_NAME'.$i;
                       $name    ='L_PAYMENTREQUEST_0_NAME'.$i;
                       //$amount  = 'L_AMT'.$i;
                       $number='L_PAYMENTREQUEST_0_NUMBER'.$i;
                       
                       $description='L_PAYMENTREQUEST_0_DESC'.$i;
                       
                       $amount  ='L_PAYMENTREQUEST_0_AMT'.$i;
                       //$qty     = 'L_QTY'.$i;
                       $qty     ='L_PAYMENTREQUEST_0_QTY'.$i;
                       
                       $queryString .= '&'.$name.'='.$_REQUEST[$name].
                                       '&'.$number.'='.$_REQUEST[$number].
                                       '&'.$description.'='.$_REQUEST[$description].
                                       '&'.$amount.'='. $_REQUEST[$amount] .
                                       '&'.$qty.'='.$_REQUEST[$qty];              
                       $itemamt += $_REQUEST[$qty] * $_REQUEST[$amount];                       
                   }
//                   $amt = $itemamt + $vat - $discount;
//                   $maxamt= $itemamt + $vat;
                   $amt = $itemamt - $discount;
                   $maxamt= $itemamt;
                   $nvpstr="";
                   
//                   echo "amount = ", $amt;
//                   echo '<br />';
//                   echo "item amount = ", $itemamt;
//                   echo '<br />';
//                   echo "max amount = ", $maxamt;
//                   echo '<br />';
//                   echo "vat = ", $vat;
//                   echo '<br />';
//                   echo "discount = ", $discount;
//                   echo '<br />';
//                   echo $queryString;
//                   exit;
                   //================changes ends here============


		 /* The returnURL is the location where buyers return when a
			payment has been succesfully authorized.
			The cancelURL is the location buyers are sent to when they hit the
			cancel button during authorization of payment during the PayPal flow
			*/

//		   $returnURL =urlencode($url.'/ReviewOrder.php?currencyCodeType='.$currencyCodeType.'&paymentType='.$paymentType);
//		   $cancelURL =urlencode("$url/SetExpressCheckout.php?paymentType=$paymentType" );
                   
                   
                   //$returnURL =urlencode($url.'/expresscheckout/ReviewOrder.php?currencyCodeType='.$currencyCodeType.'&paymentType='.$paymentType);
                   $returnURL =urlencode($url.'expresscheckout/ReviewOrder.php?currencyCodeType='.$currencyCodeType.'&paymentType='.$paymentType.'&orderId='.$orderId);
		   //$cancelURL =urlencode("$url/setExpressCheckout.php?paymentType=$paymentType" );
                   $cancelURL =urlencode($url."quantitydeliverytime.php" );
                   
                   
//                   $returnURL =urlencode($url.'/test.php?oId='.base64_encode($orderId));
//		   $cancelURL =urlencode("$url/cancelled.php?paymentType=$paymentType" );

		
           //$shiptoAddress = "&SHIPTONAME=$personName&SHIPTOSTREET=$SHIPTOSTREET&SHIPTOCITY=$SHIPTOCITY&SHIPTOCOUNTRYCODE=$SHIPTOCOUNTRYCODE&SHIPTOZIP=$SHIPTOZIP";
           $shiptoAddress = "&PAYMENTREQUEST_0_SHIPTONAME=$personName&PAYMENTREQUEST_0_SHIPTOSTREET=$SHIPTOSTREET&PAYMENTREQUEST_0_SHIPTOCITY=$SHIPTOCITY&PAYMENTREQUEST_0_SHIPTOSTATE=CA&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=$SHIPTOCOUNTRYCODE&PAYMENTREQUEST_0_SHIPTOZIP=$SHIPTOZIP";
 	
           //$nvpstr="&ADDRESSOVERRIDE=1$shiptoAddress".$queryString."&MAXAMT=".(string)$maxamt."&AMT=".(string)$amt."&ITEMAMT=".(string)$itemamt."&CALLBACKTIMEOUT=4&L_SHIPPINGOPTIONAMOUNT0=0&L_SHIPPINGOPTIONNAME0= &L_SHIPPINGOPTIONISDEFAULT0=true&INSURANCEAMT=0.00&INSURANCEOPTIONOFFERED=true&CALLBACK=https://www.ppcallback.com/callback.pl&SHIPPINGAMT=0&SHIPDISCAMT=-".$discount."&TAXAMT=0&L_NUMBER0=1000&L_DESC0=Size: 8.8-oz&L_NUMBER1=10001&L_DESC1=Size: Two 24-piece boxes&L_ITEMWEIGHTVALUE1=0.5&L_ITEMWEIGHTUNIT1=lbs&ReturnUrl=".$returnURL."&CANCELURL=".$cancelURL ."&CURRENCYCODE=".$currencyCodeType."&PAYMENTACTION=".$paymentType;
           $nvpstr="&ADDROVERRIDE=0$shiptoAddress".$queryString.
                   "&PAYMENTREQUEST_0_ITEMAMT=".(string)($itemamt).
                  "&PAYMENTREQUEST_0_TAXAMT=".(string)($vat).
                  "&PAYMENTREQUEST_0_AMT=".(string)($amt+$vat).
                  "&MAXAMT=".(string)($maxamt+$vat).
                  "&CALLBACKTIMEOUT=4&L_SHIPPINGOPTIONAMOUNT0=0&L_SHIPPINGOPTIONNAME0= &L_SHIPPINGOPTIONISDEFAULT0=true&CALLBACK=https://www.ppcallback.com/callback.pl&SHIPPINGAMT=0&PAYMENTREQUEST_0_SHIPDISCAMT=-".$discount.
                  "&ReturnUrl=".$returnURL."&CANCELURL=".$cancelURL ."&PAYMENTREQUEST_0_CURRENCYCODE=".$currencyCodeType."&PAYMENTREQUEST_0_PAYMENTACTION=".$paymentType;
           $nvpstr="&ADDROVERRIDE=0$shiptoAddress".$queryString.
                   "&PAYMENTREQUEST_0_ITEMAMT=".(string)($itemamt).
                  "&PAYMENTREQUEST_0_TAXAMT=".(string)($vat).
                  "&PAYMENTREQUEST_0_AMT=".(string)($amt+$vat).
                  "&MAXAMT=".(string)($maxamt+$vat).
                  "&CALLBACKTIMEOUT=4&L_SHIPPINGOPTIONAMOUNT0=0&L_SHIPPINGOPTIONNAME0= &L_SHIPPINGOPTIONISDEFAULT0=true&CALLBACK=https://www.ppcallback.com/callback.pl&PAYMENTREQUEST_0_SHIPPINGAMT=0&PAYMENTREQUEST_0_SHIPDISCAMT=-".$discount.
                  "&RETURNURL=".$returnURL."&CANCELURL=".$cancelURL ."&PAYMENTREQUEST_0_CURRENCYCODE=".$currencyCodeType."&PAYMENTREQUEST_0_PAYMENTACTION=".$paymentType;
          //echo $nvpstr = $nvpHeader.$nvpstr;exit;
          $nvpstr = $nvpHeader.$nvpstr;
           
		 	/* Make the call to PayPal to set the Express Checkout token
			If the API call succeded, then redirect the buyer to PayPal
			to begin to authorize payment.  If an error occured, show the
			resulting errors
			*/
		   $resArray=hash_call("SetExpressCheckout",$nvpstr);
		   $_SESSION['reshash']=$resArray;

		   $ack = strtoupper($resArray["ACK"]);
		   if($ack=="SUCCESS"){                       
					// Redirect to paypal.com here
					$token = urldecode($resArray["TOKEN"]);
					$payPalURL = PAYPAL_URL.$token;
					header("Location: ".$payPalURL);
				  } else  {
					 //Redirecting to APIError.php to display errors.
						$location = "APIError.php";
						header("Location: $location");
					}
} else {
		 /* At this point, the buyer has completed in authorizing payment
			at PayPal.  The script will now call PayPal with the details
			of the authorization, incuding any shipping information of the
			buyer.  Remember, the authorization is not a completed transaction
			at this state - the buyer still needs an additional step to finalize
			the transaction
			*/

		   $token =urlencode( $_REQUEST['token']);

		 /* Build a second API request to PayPal, using the token as the
			ID to get the details on the payment authorization
			*/
		   $nvpstr="&TOKEN=".$token;

		   $nvpstr = $nvpHeader.$nvpstr;
		 /* Make the API call and store the results in an array.  If the
			call was a success, show the authorization details, and provide
			an action to complete the payment.  If failed, show the error
			*/
		   $resArray=hash_call("GetExpressCheckoutDetails",$nvpstr);
		   $_SESSION['reshash']=$resArray;
		   $ack = strtoupper($resArray["ACK"]);
                   
		   if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){                       
                        require_once "GetExpressCheckoutDetails.php";
                        
			  } else  {
				//Redirecting to APIError.php to display errors.
				$location = "APIError.php";
				header("Location: $location");
			  }
}
?>

<?php
/********************************************************
GetExpressCheckoutDetails.php

This functionality is called after the buyer returns from
PayPal and has authorized the payment.

Displays the payer details returned by the
GetExpressCheckoutDetails response and calls
DoExpressCheckoutPayment.php to complete the payment
authorization.

Called by ReviewOrder.php.

Calls DoExpressCheckoutPayment.php and APIError.php.

********************************************************/

session_start();

/* Collect the necessary information to complete the
   authorization for the PayPal payment
   */

$_SESSION['token']=$_REQUEST['token'];
$_SESSION['payer_id'] = $_REQUEST['PayerID'];

$_SESSION['paymentAmount']=$_REQUEST['paymentAmount'];
$_SESSION['currCodeType']=$_REQUEST['currencyCodeType'];
$_SESSION['paymentType']=$_REQUEST['paymentType'];
$_SESSION['orderId']=$_REQUEST['orderId'];

$resArray=$_SESSION['reshash'];
//$_SESSION['TotalAmount']= $resArray['AMT'] + $resArray['SHIPDISCAMT'];
$_SESSION['TotalAmount']= $resArray['AMT'];


/* Display the  API response back to the browser .
   If the response from PayPal was a success, display the response parameters
   */

?>



<html>
<head>
    <title>PayPal NVP SDK - ExpressCheckout-Instant API- Simplified Order Review Page</title>
    <link href="sdk.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="expresscheckout" action="DoExpressCheckoutPayment.php" method="POST">	  
    </form>
</body>
<script src="http://code.jquery.com/jquery-1.9.1.js"> </script>
<script>
    $(document).ready(function(){
        $("#expresscheckout").submit();
    });
</script>
</html>


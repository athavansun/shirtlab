<?php include_once('includes/header.php'); ?>
<?php
    if($_GET['q'] == '')
    {
        header('Location:product.php');
        exit();
    }
    include_once ('includes/function/function.php');
    $proObj = new Product();
    $genObj = new GeneralFunctions();
    
    $total = $proObj->searchTotalProduct($_GET['q']);    
    $page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
    $limit = $genObj->getSystemConfigValue('NUMBER_OF_PRODUCT_ON_SEARCH'); //1;
    $startpoint = ($page * $limit) - $limit;
    $keyword = urldecode($_GET['q']);
    $url = 'search.php?q='.urlencode($_GET['q']).'&';
?>
<script src="js/jquery.jqzoom1.0.1.js" type="text/javascript"></script>
<link href="css/pagination.css" rel="stylesheet" type="text/css" />
<link href="css/paging-grey.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.jqZoomWindow{margin-top:0px; margin-left:30px; border:1px solid #c7c7c7; behavior: url(PIE.htc); border-radius:5px; background-color:#eeeeee;}
.product-style-large a{ margin-left:60px;}
.product-style-large title{ display:none; text-indent:-999999px;}
.preload{ display:none;}
</style>
<div id="content">
	<div class="myaccount">
            <?=pagination($total,$limit,$page, $url);?>
        </div>
	<div class="product-style" id="loader">
            <ul>
                <?=$proObj->searchProduct($keyword, $startpoint, $limit);?>
            </ul>	
	</div>	
	<div class="jersy-tshirt-mtarr" id="jersy-tshirt-mtarr"></div>
</div>

<?php include_once('includes/footer.php'); ?>









<?php include_once('includes/header.php'); ?>
<?php
	$genObj = new GeneralFunctions();	
    $orderObj = new QuantityDelivery();
	require_once('validation_class.php');	

    if(isset($_POST['submit'])) {
        
    }    
    
 if(isset($_POST['checkout'])){
	$obj = new validationclass();
	
	$obj->fnAdd("cardNumber", $_POST["cardNumber"], "req", 'Please enter card number.');
	$obj->fnAdd("expiryMonth", $_POST["expiryMonth"], "req", 'Please enter expiry month.');
    $obj->fnAdd('expiryYear',$_POST['expiryYear'], 'req', 'Please enter expiry year.');
	$obj->fnAdd('cardCVV',$_POST['cardCVV'], 'req', 'Please enter card CVV.');
	
	$arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
		
	$arr_error['cardNumber'] = $obj->fnGetErr($arr_error['cardNumber']);
	$arr_error['expiryMonth'] = $obj->fnGetErr($arr_error['expiryMonth']);
	$arr_error['expiryYear'] = $obj->fnGetErr($arr_error['expiryYear']);
	$arr_error['cardCVV'] = $obj->fnGetErr($arr_error['cardCVV']);
}
?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
	jQuery(document).ready( function() {
		jQuery("#paymentForm").validationEngine();
	});
</script>
<div id="content" >
	<div class="subpage-border-top">
		<div class="subpage-border-bottom">
			<div class="subpage-border-mid">
			<form name="paymentForm" id="paymentForm" method="post" action="">
                <input type="hidden" name="isSave" value="<?=$queryStr?>" />
				<div class="loginpage-bottom">
					<h2>Payment Page</h2>
						<div class="loginpage-mid">							
							<ul class="second" id="paymentInfo" style="display:block;">         
                            <li>
                              <label>Card Type</label>
                             <div class="right_in_box">
                             <select name="cardType" id="cardType" class="cardType" style="width:235px; ">
                                <option value="Visa" selected="selected">&nbsp;Visa&nbsp;</option>
                                <option value="MasterCard">&nbsp;Master Card&nbsp;</option>
                                <option value="Amex">&nbsp;American Express&nbsp;</option>
                                <option value="Discover">&nbsp;Discover&nbsp;</option>
                            </select>                                 
                            <?=$arr_error['cardType'];?>
                            </div>
                            </li>
                            <li>
                            <label>Card Number</label>
                            <div class="right_in_box new">
                            <input type="text" name="cardNumber" id="cardNumber" />
                            <?=$arr_error['cardNumber'];?>
                            </div>
                            
                            </li>
                            <li>
                            <label>Expiry Date</label>
                            <div class="right_in_box">
                            <ul>
                            <li>
                            <label>Month</label>
                            <input maxlength="2" type="text" name="expiryMonth" id="expiryMonth" />
                             <?=$arr_error['expiryMonth'];?>
                            </li>
                            <li>
                            <label>Year</label>
                            <input maxlength="4" type="text" name="expiryYear" id="expiryYear" />
                            <?=$arr_error['expiryYear'];?>
                            </li>
                            <li class="rt_space">
                            <label>CVV</label>
                            <input maxlength="4" type="text" name="cardCVV" id="cardCVV" />
                            <?=$arr_error['cardCVV'];?>
                            </li>
                            
                            </ul>
                            
                            </div>
                            </li>
                            <li>
                            </li>
                            <div class="right_in_box" style="width: 132px; margin-right: 200px;">
                            <div class="br_right_bt">
                            <div>
                            <input type="submit" value="Checkout" name="checkout" title="checkout" />
                            </div>
                            </div>
                            </div>
                            </ul>
					  </div>
				</div>
			</form>
			<div style="display: none;">
					<div id="inline1" style="width:500px;height:200px;overflow:auto;">
						<?php
							$page = $genObj->getStaticPageInformation();
							echo $page->pageContaint;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
  <!--Content-->
</div>
<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#various1").fancybox({
			'titlePosition'		: 'inside',
			'transitionIn'		: 'elastic',
			'transitionOut'		: 'elastic'
		});
	});
</script>
<?php include_once('includes/footer.php'); ?>
<link type="text/css" href="admin/datepicker/themes/base/ui.all.css" rel="stylesheet" />
<!--<script src="admin/js/jquery-1.6.2.js"></script>-->
<script src="admin/js/jquery.ui.core.js"></script>
<script src="admin/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $('#birthday').datepicker({
            dateFormat: 'yy-mm-dd',
            showButtonPanel: true
    });
});
</script>
     

<?php

exit;
function saveOrder($post) {        
        
        $countryName = '';
        $discount = 0;
        $subTotal = 0;
        $totalQuantity = 0;
        $totalAmount = 0;
        $saveOrderId = 0;
        
        if(isset($post['countryId']) && $post['countryId'] !== '') {
            $countryId = $post['countryId'];
        }else {
            $countryId = $_SESSION[DEFAULTCOUNTRYID];
        }
        //$countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$countryId."' and langId = '".$_SESSION['DEFAULTLANGUAGEID']."'");
        if(isset($post['currencyId']) && $post['currencyId'] !== '') {
            $currencyId = $post['currencyId'];
        }else {
            $currencyId = $_SESSION[DEFAULTCURRENCYID];
        }
        if(isset($post['deliveryTime']) && $post['deliveryTime'] !== '') {
            $deliveryTime = $post[deliveryTime];
        } else {
            $deliveryTime = $_SESSION[DELIVERYTIME];
        }
        $denominationId = $this->fetchValue(TBL_GIFT_SEND, "giftDenominationId", " giftCertificateCode = '".$post['giftVoucher']."' and couponExpired = '0'");
        $discount = $this->fetchValue(TBL_GIFT_DENOMINATION, "value", "id = '".$denominationId."'");
        if($currencyId != '1') {
            $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".__CURRENCYID__."'");
            $curValuePoint = $discount / $curValue;
            
            $value = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");
            $discount = $curValuePoint * $value;
        }
        
        
        //============generating order number====================
        $countrySign = $this->fetchValue(TBL_COUNTRY, "country_2_code", " id='".$_SESSION[DEFAULTCOUNTRYID]."'");
        $date .= date('ymdhms');
        $invoiceAdd = $this->fetchValue(TBL_ADDRESS, "firma", " id = '".$post['invoiceAdd']."'");
        $orderNo = $countrySign.$date.$invoiceAdd;
                
        $amtResult = $this->executeQry("select orderId, sum(quantity) as totalQuantity, sum(final_price) as totalAmount from ".TBL_BASKET." where userId = ".$_SESSION[USER_ID]." group by orderId");
        
        if($amtResult) {
            $amtObject = $this->fetch_object($amtResult);            
            $subTotal = $amtObject->totalAmount;
            $totalQuantity = $amtObject->totalQuantity;
            $totalAmount = $subTotal - $discount;
            $ordId = $amtObject->orderId;
        }
        
        $orderReslut = $this->fetchValue(TBL_ORDER, "id", " id = '".$ordId."'");
        
        if($orderReslut) {
            //=========================update order table======================            
           $query = "update ".TBL_ORDER." set orderDate = '".date('Y-m-d')."', ipAddress = '".$_SERVER[REMOTE_ADDR]."', invoiceAddId = '".$post['invoiceAdd']."', deliveryAddId = '".$post['deliveryAdd']."', graficAddId = '".$post['graficAdd']."', ordReceiptId = '".$orderNo."', currencyId = '".$_SESSION[DEFAULTCURRENCYID]."', langId = '".$_SESSION[DEFAULTLANGUAGEID]."', ordQuantity = '".$totalQuantity."', ordSubTotal = '".$subTotal."', discount_ammount = '".$discount."', ordTotal = '".$totalAmount."' where id = '".$ordId."'";
           $this->executeQry($query);
           
           //=========================update order details======================                      
           $orderDetailsId = array();
           $orderIdResult = $this->executeQry("select id from ".TBL_ORDERDETAIL." where orderId = '".$ordId."'");
           $orderIdNum = $this->num_rows($orderIdResult);
           if($orderIdNum > 0) {
               while($orderIdRow = $this->fetch_object($orderIdResult) ) {
                   $orderDetailsId [] = $orderIdRow->id;
               }
           }
                      
           $basketResultSet = $this->executeQry("select * from ".TBL_BASKET." where userId = '".$_SESSION[USER_ID]."' and orderId ='".$ordId."'");
            $numRecord = $this->num_rows($basketResultSet);
            if($numRecord > 0) {
                while($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into ".TBL_ORDERDETAIL." (orderId, rawProductId, productId, quantity, unit_price, final_price) values ( '".$basketRow->orderId."', '".$basketRow->rawProductId."', '".$basketRow->mainProdId."', '".$basketRow->quantity."', '".$basketRow->unit_price."', '".$basketRow->final_price."')"; 
                    $this->executeQry($orderDetailQuery);
                    $orderDetailId = mysql_insert_id();
                                                            
                    //=====================update order size table===============                
                    $orderSizeResult = $this->executeQry("select * from ".TBL_BASKET_SIZE." where basketId = '".$basketRow->id."'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if($orderSizeNum > 0) {
                        while($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into ".TBL_ORDER_SIZE." ( orderDetailId, sizeId, quantity) values ( '".$orderDetailId."', '".$orderSizeRow->sizeId."', '".$orderSizeRow->quantity."')";
                            $this->executeQry($orderSizeQuery);                            
                        }
                    }
                    
                    //=====================update order color table===============                    
                    $orderColorResult = $this->executeQry("select * from ".TBL_BASKET_COLOR." where basketId = '".$basketRow->id."'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if($orderColorNum > 0) {
                        while($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into ".TBL_ORDER_COLOR." ( orderDetailId, viewId, colorId, colorCode, type) values ( '".$orderDetailId."', '".$orderColorRow->viewId."', '".$orderColorRow->colorId."', '".$orderColorRow->colorCode."', '".$orderColorRow->type."')";
                            $this->executeQry($orderColorQuery);
                        }
                    }
                    
                }                
            }
            foreach($orderDetailsId as $id) {
                $this->executeQry("delete from ".TBL_ORDERDETAIL." where id = '".$id."'");
                $this->executeQry("delete from ".TBL_ORDER_SIZE." where orderDetailId = '".$id."'");
                $this->executeQry("delete from ".TBL_ORDER_COLOR." where orderDetailId = '".$id."'");
            }
            $saveOrderId = $ordId;         
        }else {
            //=========================insert into order table======================
            $query = "insert into ".TBL_ORDER." (orderDate, ipAddress, sessId, userId, invoiceAddId, deliveryAddId, graficAddId, ordReceiptId, currencyId, langId, ordQuantity, discount_ammount, ordSubTotal, ordTotal) values ('".date('Y-m-d')."', '".$_SERVER[REMOTE_ADDR]."', '".session_id()."', '".$_SESSION[USER_ID]."', '".$post['invoiceAdd']."', '".$post['deliveryAdd']."', '".$post['graficAdd']."', '".$orderNo."', '".$_SESSION[DEFAULTCURRENCYID]."', '".$_SESSION[DEFAULTLANGUAGEID]."', '".$totalQuantity."', '".$discount."', '".$subTotal."', '".$totalAmount."')";
            $this->executeQry($query);
            $orderId = mysql_insert_id();
            
            //===================update orderId in basket table=====================
            $this->executeQry("update ".TBL_BASKET." set orderId = '".$orderId."' where userId = '".$_SESSION[USER_ID]."'");
            
            //======================insert into order detials table=================
            $basketResultSet = $this->executeQry("select * from ".TBL_BASKET." where userId = '".$_SESSION[USER_ID]."' and orderId = '".$orderId."'");
            $numRecord = $this->num_rows($basketResultSet);
            if($numRecord > 0) {
                while($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into ".TBL_ORDERDETAIL." (orderId, rawProductId, productId, quantity, unit_price, final_price) values ( '".$basketRow->orderId."', '".$basketRow->rawProductId."', '".$basketRow->mainProdId."', '".$basketRow->quantity."', '".$basketRow->unit_price."', '".$basketRow->final_price."')";
                    $this->executeQry($orderDetailQuery);                    
                    $orderDetailId = mysql_insert_id();
            
                    //=====================insert into order size table===============
                    $orderSizeResult = $this->executeQry("select * from ".TBL_BASKET_SIZE." where basketId = '".$basketRow->id."'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if($orderSizeNum > 0) {
                        while($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into ".TBL_ORDER_SIZE." ( orderDetailId, sizeId, quantity) values ( '".$orderDetailId."', '".$orderSizeRow->sizeId."', '".$orderSizeRow->quantity."')";
                            $this->executeQry($orderSizeQuery);                            
                        }
                    }
                    
                    //=====================insert into order color table===============
                    $orderColorResult = $this->executeQry("select * from ".TBL_BASKET_COLOR." where basketId = '".$basketRow->id."'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if($orderColorNum > 0) {
                        while($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into ".TBL_ORDER_COLOR." ( orderDetailId, viewId, colorId, colorCode, type) values ( '".$orderDetailId."', '".$orderColorRow->viewId."', '".$orderColorRow->colorId."', '".$orderColorRow->colorCode."', '".$orderColorRow->type."')";
                            $this->executeQry($orderColorQuery);
                        }
                    }                    
                }                
            }
            $saveOrderId = $orderId;
        }
        $id = base64_encode($saveOrderId);
        $paymentType = $this->checkAmountInDollar($currencyId, $totalAmount);        
        $paymentMethod = base64_encode($paymentType);
        header("Location:sendOrder.php?id=".$id."&p=".$paymentMethod);exit;
    }















exit;
session_start();
//require_once "includes/JSON.php";
require_once "includes/classes/DB.php";
require_once "includes/classes/MySqlDriver.php";
require_once "includes/classes/MailFunction.php";
require_once "includes/classes/GeneralFunctions.php";

class Test extends MySqlDriver{
    function sendMail() {
        $mailFunctionObj = new MailFunction;
        $generalDataObj = new GeneralFunctions();        
        $qryresult = $this->selectQry(TBL_USER,"email = '$post[email]'",'','');
        echo "<pre>";
        while($line = $this->fetch_object($qryresult)) {
            print_r($line);
        }
        $qryvalue = $this->getResultRow($qryresult);
        $uid = $qryvalue[id]; 
        $updateqry = "UPDATE ".TBL_USER." SET `fpwdcode` = '$newmoveid' WHERE `id` ='$uid'";
        $this->executeQry($updateqry);            
        $mailFunctionObj->mailValue("11",$_SESSION[DEFAULTLANGUAGEID],$uid,$newmoveid);
        $_SESSION['ERROR_MSG']=msgSuccessFail(1,LANG_EMAIL_TO_RESET_PASSWORD_MSG);
        header("Location:login.php");
        echo "<script language=javascript>window.location.href='login.php';</script>";exit;
    }
}


$obj = new Test();
$obj->sendMail();
//$obj->insert();
//$obj->update();




exit;

/*
 * //=========================insert into order detials====================
            $orderDetailVal = array();
            $orderSizeVal = array();
            $orderColorVal = array();
            $basketId = array();
            
            $basketResultSet = $this->executeQry("select * from ".TBL_BASKET." where userId = '".$_SESSION[USER_ID]."' and sessionId = '".  session_id()."'");
            $numRecord = $this->num_rows($basketResultSet);
            if($numRecord > 0) {            
                while($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailVal[] = "( '".$basketRow->orderId."', '".$basketRow->rawProductId."', '".$basketRow->mainProdId."', '".$basketRow->quantity."', '".$basketRow->unit_price."', '".$basketRow->final_price."')";
                    $basketId[] = $basketRow->id;
                }
                $val = implode(',', $orderDetailVal);
                $orderQuery = "insert into ".TBL_ORDERDETAIL." (orderId, rawProductId, productId, quantity, unit_price, final_price) values ".$val;
                $this->executeQry($orderQuery);
 */






















































exit;

session_start();
require_once "includes/JSON.php";
require_once "includes/classes/DB.php";
require_once "includes/classes/MySqlDriver.php";



class A extends MySqlDriver{
    
    public function addRecords() {
        //$get = $_SESSION['quantity'];
        //echo "<pre>";print_r($_SESSION[quantity]);exit;
        
        $jsonObj = new JSON();
        $decoObj = (object)$decoObj;        
                
        $decoResult = $this->executeQry("select collectionName, decoInfo from ".TBL_COLLECTION_DATA." where userId = '".$_SESSION[USER_ID]."' and pId = '".$_SESSION['quantity'][pId]."'");
        $numDeco = $this->num_rows($decoResult);
        if($numDeco > 0) {
            while($decoRow = $this->fetch_object($decoResult)) {
                $decoInfo = $jsonObj->decode($decoRow->decoInfo);
            }
        }
        
        $numberOfView = count($decoInfo);
        foreach($decoInfo as $decoObj) {
            $viewId = $decoObj->viewId; 
            $numberOfDeco = count($decoObj->decoContentArray);
            if($numberOfDeco > 0) {
                //print_r($decoObj->decoContentArray);
                $status = "enabled";
            }else {
                $status = "disabled";
            }
        }
        exit;
    }
    
    function getProductPrice($valuePoint, $quantity = '1') {
        $curId = $_SESSION['DEFAULTCURRENCYID'];
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='".$curId."'");
        $sql = $this->executeQry("select c.*, cd.sign from ".TBL_CURRENCY." as c inner join ".TBL_CURRENCY_DETAIL." as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '".$curId."'");

        $line = $this->getResultObject($sql);
        
		$leftPlace = ($line->showIn==0)?html_entity_decode($line->sign)." ":"";
		$rightPlace =($line->showIn==1)?" ".html_entity_decode($line->sign):"";        
        $price = $valuePoint * $currencyValue;
        $final_price = $leftPlace."".number_format($price * $quantity, $line->decimalPlace,".", $line->SeparatorFrom)."".$rightPlace;        
             
        return $final_price;
    }
    
}

$obj = new A();
//$obj->addRecords();

//echo $obj->getProductPrice(50);

//=============================================================================
//Quantity page info
//=============================================================================

exit;
function getQuantityPageInfo() {        
        $html = '';
        $htmlButton = '';        
        $cond = '';        
        $viewName = array();
        $viewId = array();        
        $product = (object)$product;
        $ctr = 1;

        if($_SESSION[USER_ID] != '') {
            //$cond = " where userId = '".$_SESSION['USER_ID']."' and sessionId = '".session_id()."'";
            $cond = " where userId = '".$_SESSION['USER_ID']."'";
        } else {
            $cond = " where sessionId = '".session_id()."'";
        }
        
        $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id='".$_SESSION[DELIVERYTIME]."'");        
        $query = "select * from ".TBL_BASKET.$cond;
        $result = $this->executeQry($query);
        $num = $this->num_rows($result);
        
        if($num > 0) {
            while($line = mysql_fetch_object($result)) {
                $unitPrice = 0;
                $totalPrice = 0;
                //echo $line->id;
                $totalQty = $this->fetchValue(TBL_BASKET, "quantity", "id = '".$line->id."'");
                $product->totalProduct += $totalQty;
                $viewObj = $this->getDecoView($line->id, $line->mainProdId, $totalQty);
                $unitPrice =  $viewObj->decoPrice;
                $totalPrice = $viewObj->totalDecoPrice;
                $country = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "countryId = '".$_SESSION[DEFAULTCOUNTRYID]."'");
                
                if($ctr == 1) {
                $html .= '<div class="delivery-product-basic">                                
                        <span id="deliveryCountry">Country of delivery: '.$country.'</span>
                        <span id="deliveryTime">Deliverytime: '.$deliveryTime.' Days</span>';
                        
                } else {
                    $html .= '<div class="delivery-product-basic">';
                }
                $mainResult = $this->executeQry("select * from ".TBL_MAINPRODUCT." where id = '".$line->mainProdId."'");                
                $mainProd = $this->getResultObject($mainResult);              
                $styleCode = $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '".$mainProd->styleId."'");
                $quality = $this->fetchValue(TBL_FABRICCHARGE, 'fQuality', " id = '".$mainProd->qualityId."'");
                $query =  "select viewImage, viewId from ".TBL_MAINPRODUCT_VIEW." where mainProdId = '".$line->mainProdId."'";
                $res = $this->executeQry($query);
                $n = $this->num_rows($res);
                if($n > 0) {
                    $html .= '<ul>';
                    while($row = $this->fetch_object($res)) {
                        $html .='<li><a href="#"><img src="'.SITE_URL.__MAINPRODTHUMB__.$row->viewImage.'" height="140" width="100" alt="image" /></a></li>';
                        $viewName[] = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '".$row->viewId."'");
                        $viewId[] = $row->viewId;
                    }                    
                    
                    $html .= '</ul>
                        <strong>Product '.$ctr.'<span>: Prod-Code: <span>'.$styleCode.'</span></span></strong></div>                            
                        <div class="fabric-quality">          
                        <strong>Fabric-Quality: <strong>'.$quality.'</strong></strong>';
                        
                        $obj = $this->getProductDetails($line->id, $mainProd->rawProdId, $mainProd->qualityId, $mainProd->styleId);
                            $unitPrice += $obj->unitPrice;
                            $totalPrice += $obj->totalPrice;
                            
                            $html .= '<table id="productDeco">
                            <tr class="size-description">
                            <td style="width:520px"> '.$obj->size.'// Total: '.$obj->totalQty.' pcs // Value-Points per shirt '.$obj->productVP.'</td>
                            
                            <td style="width:60px" class="price">'.$obj->productPrice.'</td>
                            <td style="width:60px" class="price">'.$obj->productTotalPrice.'</td>
                            </tr>';                        
                        $html .= $viewObj->en;
                        $html .='</table></div>';                            
                        $html .= '<div class="product-comment">                        
                        <table class="about-comment">';
                        $html .= $viewObj->dis;
                        $html .= $this->quantitySurcharge($unitPrice);
                        $deliveryObj = $this->getDeliverySurcharge($unitPrice, $obj->totalQty);
                        $unitPrice = $deliveryObj->unitPrice;                        
                        $totalPrice = $deliveryObj->unitPrice * $obj->totalQty;
                        $product->totalPrice += $totalPrice;
                        $html .= $deliveryObj->html;
                        $html .='</table>
                        <span>Comment to Product '.$ctr.':</span>
                        <textarea rows="0" cols="0"></textarea>
                        </div>';
                }
                
                $htmlButton .='<a href="javascript:void(0);" class="fabric-button" onclick="enterFabricQuantity(\''.$line->id.'\', \'fabricQuantity\', \''.$mainProd->rawProdId.'\', \''.$mainProd->styleId.'\', \''.$mainProd->qualityId.'\', \''.$line->mainProdId.'\');">
                <span>Enter Fabric Quality and Quantitys and Print/Embro-Details</span>
                </a>';
                $this->executeQry("update ".TBL_BASKET." set currencyId = '".$_SESSION[DEFAULTCURRENCYID]."', unit_price = '".$unitPrice."', final_price ='".$totalPrice."' where id = '".$line->id."'"); 
                                
                $ctr++;
            }
            $html .= '<div>
                    <table width="95%" class="total_price">
                      <tr>
                        <td width="70%">Total Product: '.$product->totalProduct.' </td>
                        <td align="right" width="10%"> Total: </td>
                        <td width="20%" align="right">'.$this->getPriceSign($product->totalPrice).'</td>
                      </tr>
                    </table>
                </div>';
            
            $html .= '</div></div>';

            $html .= '<div class="delivery-page-right-part">				
            <select name="countryId" onChange="changeCountry(this.value);">'.$this->getCountry().'</select>
            <select name="currencyId" onChange="changeCurrency(this.value);">'.$this->getCurrency().'</select>
            <select name="deliveryTime" onChange="changeDeliveryTime(this.value);">'.$this->getDeliveryTime().'</select>';
            $html .= $htmlButton;
        }
        return $html;
    }













exit;

//===========color pallet==========
        $colorArr = array();        
        $isBasket = $this->fetchValue(TBL_BASKET_COLOR, 'id', " basketId = '".$post['basketId']."'");
        if($isBasket) {            
            $this->executeQry("Delete from ".TBL_BASKET_COLOR." where basketId = '".$post['basketId']."'");
        }
        
        $num = $post['colorQty'];
        if($num != '') {
            for($i = 1; $i <= $num; $i++) {
                $color = "color".$i;
                $colorCode = $this->fetchValue(TBL_PALLETCOLOR, 'colorCode', "id = '".$post[$color]."'");
                $colorArr[] = "('".$post['basketId']."', '".$colorCode."')";
            }
            $val = implode(',', $colorArr);
            $query = "insert into ".TBL_BASKET_COLOR." (basketId, colorCode) values ".$val;
            $this->executeQry($query);            
        }
        //=============Ends here==============
        for($i = 1; $i <= 8; $i++) {
                        if($i == 1) {
                            $quantityOfColor = $viewName."ColorQty";
                            $design = $viewName."Design";
                            $photoPrint = $viewName."PhotoPrint";
                        }
                        
                        $colorName = $viewName."Color".$i;
                        
                    }                 
        
        



$html .= '<li>
               <label>---Chest:---</label>
                    <ul>                        
                        <li> 
                            <select id="chestDesign" name="chest" size="1" style="width:auto;">
                                <option value="0">Choose Print or Embroidery</option>
                                <option value="1">Print</option>
                                <option value="2">Embroidery</option>                                
                           </select>
                        </li>
                      <li>
                          <label>Quantity of colors:</label>
                          <div class="input-text">
                              <input type="text" name="colorQty" value="" onkeyup="enablePallet(this.value);"/>
                          </div>
                      </li>
                      <li>
                          <label>Photo-print</label>
                          <input type="checkbox" name="photoPrint" />
                      </li>
                  </ul>
              </li>
              <li>
              <ul class="color-type">
                  <li>
                      <label>Color 1:</label>
                      <select name="color1" id="color1" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 2:</label>
                     <select name="color2" id="color2" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 3:</label>
                      <select name="color3" id="color3" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 4:</label>
                     <select name="color4" id="color4" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 5:</label>
                      <select name="color5" id="color5" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 6:</label>
                      <select name="color6" id="color6" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 7:</label>
                      <select name="color7" id="color7" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
                   <li>
                      <label>Color 8:</label>
                      <select name="color8" id="color8" size="1" style="width:110px;" class="colorPallet" disabled>';
                        $html .= $palletColor;
               $html    .='</select>
                   </li>
               </ul>
               </li>
               <li>
                  <label>Comment on ---chest--- deco:</label>
                  <textarea rows="0" cols="0"></textarea>
               </li>';
              


//====================View Image===================
        $viewName = array();
        $viewResult = $this->executeQry("select viewId, viewImage from ".TBL_MAINPRODUCT_VIEW." where mainProdId = '".$get['mainProdId']."'");
        $num = $this->num_rows($viewResult);
        if($num > 0) {
            while($viewRow = $this->fetch_object($viewResult)) {
                $viewName[] = $this->fetchValue(TBL_VIEWDESC, "viewName", " viewId = '".$viewRow->viewId."' and langId = '".$_SESSION[DEFAULTLANGUAGEID]."'");
            }
        }





exit;
//=====================DATE TIME===================
//=============add==============
$date = new DateTime();
$date->add(new DateInterval('P10D'));
echo $date->format('Y-m-d') . "\n";

//add date using strtotime
//$time = strtotime('1 Aug 2012');
$time = time();
$d = strtotime("+1 day", $time);    // 1 day, 1 week, 1 month, 1 year
echo date('d-m-Y', $d);


//=============sub==============
$date = new DateTime();
$date->sub(new DateInterval('P10D'));
echo $date->format('Y-m-d') . "\n";

//difference between two date
$datetime1 = date_create('2009-10-11');
$datetime2 = date_create('2009-10-13');
$interval = date_diff($datetime1, $datetime2);
echo $interval->format('%R%m month %d Days');

exit;
//============================Example ends here===========================
    searchProduct('1 sparx');
    function searchProduct($keyword) {
        
        $cond = '';
        $pNameArr = array();
        $pDescArr = array();
        $pCatIdArr = array();
        
        $key = explode(' ', $keyword);
        foreach ($key as $word)
        {
            $pCatIdArr[] = "P.catId = '".$word."'";
            $pNameArr[] = "PD.productName like '"."%".$word."%"."'";
            $pDescArr[] = "PD.productDesc like '"."%".$word."%"."'";
        }
        
        $pCatId = implode(" or ", $pCatIdArr);
        $pName = implode(" or ",$pNameArr);
        $pDesc = implode(" or ",$pDescArr);
        
        $cond .= " and P.status = '1' and P.isDeleted = '0'";
        $cond .= " and (".$pCatId." or ".$pName." or ".$pDesc.")";
        
        
        $query = "select distinct(P.id), PD.productName, PD.productDesc from ecart_product as P INNER JOIN ecart_product_description as PD ON (P.id = PD.pId) ".$cond;
                
        echo $query;
        exit();
    }

    include_once 'admin/includes/JSON.php';
    $str = '[{"viewId":"1","decoContentArray":[{"name":"4-300dpi","widthCM":"19.03","designId":"43","heighCM":"14.07"}],"prodId":"1"},{"viewId":"2","decoContentArray":[],"prodId":"1"},{"viewId":"3","decoContentArray":[],"prodId":"1"},{"viewId":"4","decoContentArray":[],"prodId":"1"}]';
    $jsonObj = new JSON();
    $t = $jsonObj->decode($str);
    echo '<pre>';
    print_r($t);
    exit();
    
//    $rs = fOpen('http://192.168.1.236/demo/1.png', 'r');
//    $encodeString = file_get_contents('http://192.168.1.236/demo/1.png');

    exit;
    $image = array();
    $image[] = 'files/test/1.png';
    $image[] = 'files/test/2.png';
    $image[] = 'files/test/3.png';
    $image[] = 'files/test/4.png';
    $image[] = 'files/test/5.png';
    $image[] = 'files/test/6.png';
    $image[] = 'files/test/2.png';
    $image[] = 'files/test/3.png';
    $image[] = 'files/test/4.png';
    $image[] = 'files/test/5.png';

    $c = count($image);
    $dest = 'files/test/appendImg.png';
    
    if ($c > 0) {
        $imgArr = array_chunk($image, '4');
        //$imgArr = array_reverse($imgArr);
        $ctr = 1;
        $tmpImgArr = array();
//        echo '<pre>';
//        print_r($imgArr);
        foreach ($imgArr as $image)
        {
            $str = implode(' ', $image);
//            foreach ($image as $img) {
//                $str.= ' ' . $img;
//            }
            
            $tmpImage = 'files/test/'.$ctr;
            $tmpImgArr[] = $tmpImage;
            exec("convert  $str  +append -trim $tmpImage");
            $ctr ++;
        }
        
        $strComp=' -size 1500x1500 xc:none ';
        if(count($tmpImgArr)>0){
            $i=0;
            $y=0;
            foreach($tmpImgArr as $tmpValue){
                list($width,$height)=getimagesize($tmpValue);
                
                if($i > 0)
                {
                    $y+=$height+10;
                }
               $strComp.=' "'.$tmpValue.'" -geometry +0+"'.$y.'"  -composite '; 
               $i++;
            }
            
        exec('convert '.$strComp.' -trim "'.$dest.'"');
        } 
        
    }
    
    echo '<img src="'.$dest.'" />';
    exit();
    


	
    echo '<pre>';
    print_r(unserialize($str));
    exit();

    $destImage = '4-300dpi.png';
    $sourceImage = '4.png';
    exec('convert -units PixelsPerInch '.$sourceImage.' -density 300 '.$destImage);
    exec('identify -format "%x x %y" '.$destImage.'',$storeval); // Identify dpi formate
    print_r($storeval);
    
    exit();

            $deco = '<designs>
  <deco>
    <designId>390</designId>
    <name>e-1</name>
    <productName>2/1/</productName>
    <uploadUrl>http://125.63.71.182/NR193/files/userdeco/original/589845316.png</uploadUrl>
    <thumbUrl>http://125.63.71.182/NR193/files/userdeco/thumb/589845316.png</thumbUrl>
    <imgDpi>100</imgDpi>
    <occurrence>21</occurrence>
    <x>0</x>
    <y>0</y>
    <preWidth>0</preWidth>
    <preHeight>0</preHeight>
    <width>94</width>
    <height>125</height>
    <imageWidth>345</imageWidth>
    <imageHeight>460</imageHeight>
    <rotation>0</rotation>
  </deco>  
</designs>';
echo '<pre>';
$tmp = removeDecoFromXml($deco, '390','decoBasket');
echo $tmp;

        function removeDecoFromXml($deco,$decoId, $type = '')
        {
            if($deco != '')
            {
                if($type == '')
                {
                    $xmlString = '<design><deco>'.$deco.'</deco></design>';
                    $designXml = new SimpleXMLElement($xmlString);
    //                print_r($designXml);
    //                echo $designXml->deco->designId;
                    if($designXml->deco->designId == $decoId)
                    {
                        unset($designXml->deco);
                        $decoTmp = $designXml->asXML();
                    }
                    else
                    {
                        $decoTmp = $designXml->asXML();
                    }
                    $find = array('<?xml version="1.0"?>','<design>','<deco>','</deco>','</design>');
                    $decoXml = str_replace($find, '', $decoTmp);
    //                echo '<br />';
    //                print_r($decoXml);
                }
                else
                {
                    
                    $designXml = new SimpleXMLElement($deco);
                    //print_r($designXml);
                    $decoArr = array();
                    foreach ($designXml->deco as $design)
                    {
                        //print_r($design);
                        if($design->designId == $decoId)
                        {
                            echo 'hello';
                            unset($design->deco);
                            //$decoTmp = $design->asXML();
                        }
                        else
                        {
                            $decoArr[] = $design->asXML();
                        }
                        
                        unset($design);
                        
                    }
                    echo 'after';
                    print_r($decoArr);
//                    if(count($design) > 0)
                    $decoTmp = implode('', $decoArr);
                    $decoTmp = '<designs>'.$decoTmp . '</designs>';
                    $decoXml = str_replace('<?xml version="1.0"?>', '', $decoTmp);
    //                echo '<br />';
                    //print_r($decoXml);
                }
            }            
            $decoXml = str_replace('<design/>','',$decoXml);
            $breaks = array("\r\n", "\n", "\r",'  ');
            $newtext = str_replace($breaks, "", $decoXml);
            return $newtext;
        }
        exit();
            
            $str = 'O:8:"stdClass":7:{s:9:"colorType";s:6:"Normal";s:3:"pid";s:1:"1";s:15:"isVarintDefault";s:1:"2";s:13:"coloringStyle";s:8:"unicolor";s:17:"isProductDeafault";s:1:"1";s:7:"colorId";s:1:"1";s:16:"isQualityDefault";s:1:"2";}';
            $str1= unserialize($str);
            
            $str2 = 'O:8:"stdClass":7:{s:9:"colorType";s:6:"Normal";s:3:"pid";s:1:"2";s:15:"isVarintDefault";s:1:"1";s:13:"coloringStyle";s:8:"unicolor";s:17:"isProductDeafault";s:1:"1";s:7:"colorId";s:1:"1";s:16:"isQualityDefault";s:1:"2";}';
            $str21= unserialize($str2);
            
            //echo '<pre>';
            //print_r($str1);
            //print_r($str21);
            exit();

            $infoArr = '[{"ViewName":"Front","contentArray":["<designId>1355131427010</designId><parentId>0</parentId><name>Screenshot from</name><productName>1/</productName><uploadUrl>http://125.63.71.182/NR193/files/userdeco/original/861059129.png</uploadUrl><thumbUrl>http://125.63.71.182/NR193/files/userdeco/thumb/861059129.png</thumbUrl><imgDpi>72</imgDpi><occurrence>1</occurrence><x>23</x><y>19.2</y><width>71</width><height>39</height><imageWidth>1366</imageWidth><imageHeight>768</imageHeight><rotation>0</rotation>","<designId>1355131434021</designId><parentId>0</parentId><name>Screenshot from</name><productName>1/</productName><uploadUrl>http://125.63.71.182/NR193/files/userdeco/original/861059129.png</uploadUrl><thumbUrl>http://125.63.71.182/NR193/files/userdeco/thumb/861059129.png</thumbUrl><imgDpi>72</imgDpi><occurrence>1</occurrence><x>25.6</x><y>109.9</y><width>56</width><height>31</height><imageWidth>1366</imageWidth><imageHeight>768</imageHeight><rotation>0</rotation>"]}]';
        $jsonObj = new JSON();
	//echo '<pre>';
        //$t = $infoArr;
	$t = $jsonObj->decode($infoArr);
        //print_r($t);
        //exit();
        $newDataArr = array();
        foreach ($t as $view)
        {
            $viewObj = (object) $viewObj;
            $viewObj->ViewName = $view->ViewName;
            $viewObj->contentArray = array();
            foreach ($view->contentArray as $deco)
            {                
                //echo '<br />'.$deco;
                if($deco != '')
                {
                    //$newDeco = removeDecoFromXml($deco,'1355131434021');
                    if(strlen($newDeco) > 5)
                        $viewObj->contentArray[] = $newDeco;
                }
            }
            $newDataArr[] = $viewObj;
            unset($viewObj);
        }
        
        //echo '<br />Old xml=';
        //print_r($newDataArr);
        
   //     echo '<br>Old data Array<br>';
        
        
        
        
        
       // print_r($t);
        exit();
?>

<?php
	//++++++++++ For DPI Alert On tool
	
	//~ $destImage = 'rose2b.jpg';	
	//~ exec('convert -units PixelsPerInch rose2b11.jpg -density 300 rose2b.jpg');
	//~ 
	//~ exec('identify -format "%x x %y" '.$destImage.'',$storeval); // Identify dpi formate
//~ 
	//~ print_r($storeval);exit;
	//~ $storeval = implode('', $storeval); ;
	//~ $storeval = explode(' ', $storeval);
	//~ // FOR getting PixelsPerInch or PixelsPerCentemeter
	//~ $imgDpi =  $storeval[0];
	//~ $imgWidthType = $storeval[1];
 //~ 
	//~ if($imgWidthType=='PixelsPerCentimeter') {
		//~ $imgDpi = ($imgDpi* 2.54); /// DPI
	//~ } else {
		//~ $imgDpi;
	//~ }
?>

//=========================HTML of Quantity and delivery time=====================
<?php
    $html .= '<li>
               <label>Back:</label>
                    <ul>                        
                        <li> 
                            <select size="1" class="chzn-select" style="width:110px;" >
                                <option value="0">Choose Print or Embroidery</option>
                           </select>
                        </li>
                      <li>
                          <label>Quantity of colors:</label>
                          <div class="input-text">
                              <input type="text" value="" />
                          </div>
                      </li>
                      <li>
                          <label>Photo-print</label>
                          <input type="checkbox" name="" />
                      </li>
                  </ul>
              </li>
              <li>
              <ul class="color-type">
                  <li>
                      <label>Color 1:</label>
                      <select size="1" class="chzn-select" style="width:110px;" >
                        <option value="0">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 2:</label>
                     <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 3:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 4:</label>
                     <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                       <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 5:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 6:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 7:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 8:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
               </ul>
               </li>
               <li>
                  <label>Comment on chest deco:</label>
                  <textarea rows="0" cols="0"></textarea>
               </li>
               
<li>
               <label>Right Sleeve:</label>
                    <ul>                        
                        <li> 
                            <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                                <option value="0">Choose Print or Embroidery</option>
                           </select>
                        </li>
                      <li>
                          <label>Quantity of colors:</label>
                          <div class="input-text">
                              <input type="text" value="" />
                          </div>
                      </li>
                      <li>
                          <label>Photo-print</label>
                          <input type="checkbox" name="photoPrint" />
                      </li>
                  </ul>
              </li>
              <li>
              <ul class="color-type">
                  <li>
                      <label>Color 1:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="0">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 2:</label>
                     <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 3:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 4:</label>
                     <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                       <option value="">Pantone No.</option>
                      </select>
                   </li>
                   <li>
                      <label>Color 5:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 6:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 7:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>                    
                      </select>
                   </li>
                   <li>
                      <label>Color 8:</label>
                      <select name="select1" size="1" class="chzn-select" style="width:110px;" >
                        <option value="">Pantone No.</option>
                      </select>
                   </li>
               </ul>
               </li>
               <li>
                  <label>Comment on chest deco:</label>
                  <textarea rows="0" cols="0"></textarea>
               </li>
               <li></li>';
    
    
    function orderInformation() {
	    $generalObj = new GeneralFunctions();
		$menuObj =  new Menu;
		$cond = " 1=1 "; 
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
		$cond .= " AND (".TBL_ORDER.".`customerName` LIKE '%$searchtxt%'  OR ".TBL_ORDER.".`ordReceiptId` LIKE '%$searchtxt%')  ";
		}
		$query = " SELECT * FROM ".TBL_ORDER." WHERE $cond AND paymentType = 'paypal' ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"ordReceiptId";
		    $order = $_GET[order]? $_GET[order]:"DESC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {	
			$i = 1;			
			while($line = $this->getResultObject($rst)) {
				$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
				$div_id = "status".$line->id;
				if($line->orderStatus==0) $status = "InActive";
				elseif($line->orderStatus==1) $status = "Active";
					
				$genTable .= '<tr class="'.$highlight.'" id="'.$line->id.'" >';
				$genTable .= '<th>';
				$genTable .= '<input name="chk[]" value="'.$line->id.'" type="checkbox">';
				$genTable .= '</th>';
				$genTable .= '<td>'.$i.'</td>';	
				//$genTable .= '<td>'.$line->ordReceiptId.'</td>';
				//$genTable .= '<td>'.stripslashes($line->customerName).'</td>';
				//$genTable .= '<td>'.$generalObj->displayPrice($line->ordTotal+ $line->shippingVal - $line->couponDiscount).'</td>';
				$genTable .= '<td>'.date(DEFAULTDATEFORMAT,strtotime($line->orderDate)).'</td>';	
				//$genTable .= '<td><a  rel="shadowbox;width=800;height=600" title="'.stripslashes($line->customerName).'" href="viewOrder.php?uid='.base64_encode($line->id).'"><img src="images/view.png" alt="View" width="16" height="16" border="0" /></a></td>';
				
				//$genTable .= '<td>'.$this->getOrderStatus($line->orderStatus,$line->id).'</td>';
				if($menuObj->checkDeletePermission()){
					$genTable .= '<td><a href="javascript:void(0);" onClick="if(confirm(\'Are you sure to delete this Resord?\')){window.location.href=\'pass.php?action=manageorder&type=delete&id='.$line->id.'&page=$page\'}else{}" ><img src="images/drop.png" height="16" width="16" border="0" title="Delete" /></a></td>';
				}else {$genTable .= '<td>N/A</td>';}
				if($line->pdf_generated == 1) {
					$genTable .= '<td><div style="width:90px;" id="'.$div_id.'"><a id="pdfDown'.$line->ordReceiptId.'" style="display: block;" title="Order Detail" href="download.php?fileName='.$line->ordReceiptId.'.zip">Download PDF</a></div></td>';
				}else {
					$genTable .= '<td><div style="width:90px;" id="'.$div_id.'"><a style="cursor:pointer;" onClick="javascript:generatePDF(\''.$div_id.'\',\''.$line->id.'\',\'PDF\')">Generate PDF</a></div></td>';
				}
				if($line->pdf_generated == 1) {
					$genTable .= '<td><div style="width:30px;" id="'.$div_id.'_d"><a style="cursor:pointer;" onClick="javascript:deleteGenPdf(\''.$div_id.'\',\''.$line->id.'\',\'PDF\')"><img src="images/drop.png" height="16" width="16" border="0" title="Delete" /></a></div></td>';
				}
				else {
					$genTable .= '<td>N/A</td>';
				}
				$genTable.='</tr>';
			$i++;				 
			}
		  }
		  		switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					   <input type='hidden' name='page' value='".$currpage."'>
					</td><td class='page_info' align='center' width='200'>Total Records Found ".$totalrecords."</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
		}	
		return $genTable;
	}
?>

<?php
 //show errors
//ini_set("display_errors", 1);
//error_reporting(E_ALL);
$docRoot = $_SERVER['DOCUMENT_ROOT']; 
$docRoot .= "/";
//$sitepath = "http://www.shirtlabweb.com/";
//$sitepath = "http://www.shirtlabweb.com/new/";
//$sitepath = "http://www.shirtlabweb.com/";
$sitepath = "http://localhost/";
$imageMagickPath  = "/usr/bin/convert";
$sitename = "Shirtlab";

define('__SITENAME__', $sitename);
define('__SITEPATH__', $sitepath);

/*
 * 
 * 
 * MySQL® Database Wizard

Added user “shirtl_shirtl” with password “wX7rbH$Z;WX{”.
Step 3: Add User to the Database

User: shirtl_shirtl
Database: shirtl_new_db

 */


// Local Database Settings 
//$config['server'] = 'localhost'; 
//$config['database'] = 'shirtl_new_db';
//$config['user'] ='shirtl_shirtl';
//$config['pass'] ='wX7rbH$Z;WX{';
//no refresh 
$config['server'] = '192.168.3.64'; 
$config['database'] = 'shirtl_9';//'shirtl_shirtlab_9';
//$config['user'] ='shirtl_9';//'shirtl_shirtl_9';
//$config['pass'] ='admin@123';
$config['user'] ='root';//'shirtl_shirtl_9';
$config['pass'] ='root';

//=================================================================

date_default_timezone_set('Asia/Kolkata');

define('CONFIGSERVER', $config['server']);
define('CONFIGDATABASE', $config['database']);
define('CONFIGUSER', $config['user']);
define('CONFIGPASS', $config['pass']);
define('CONFIGIMAGEMAGICPATH', $imageMagickPath);
define("ABSOLUTEPATH" , $docRoot);
$desOriginalImage  = "files/design/original";
$desThumbImage 	   = "files/design/thumb";
$desSwfImage 	   = "files/design/swf";
$desOrigPng 	   = "files/design/originaldesign";	//EPS
$desLargeThumb 	   = "files/design/largethumb";
define('DESORIGPNG',$desOrigPng);
$userUpdOriginalImage  	= "files/userimage/orignal";
$userUpdLargeImage 		= "files/userimage/large";
$userUpdThumbImage 		= "files/userimage/thumb";
//============pallet color image=============== 
define('__COLORTHUMB__',"files/color/thumb/");
define('__COLORORIGINAL__',"files/color/original/");

//define('NOIMAGE',_SITEPATH_.'files/noimage/noimage.jpeg');

define('IMAGEMAGICPATH', $imageMagickPath);
define('SITE_URL', $sitepath);
define('SEARCHTEXT', "Search");
define('DEFAULTDATEFORMAT', "d-m-Y");
define('__BASE_DIR_FRONT__', $docRoot); 
define('__BASE_DIR_ADMIN__', $docRoot.'admin/'); 
define('AMFPHPPATH', __BASE_DIR_ADMIN__."amfphp/services/");
define('BASEUPLOADFILE',$docRoot."files/");
define('BASEUPLOADFILEPATH',SITE_URL."files/");
define('FLAGORIGINAL',BASEUPLOADFILE."flag/flagoriginal/");
define('FLAGTHUMB',BASEUPLOADFILE."flag/flagthumb/");
define('FLAGPATH',BASEUPLOADFILEPATH."flag/flagthumb/");
define('__FLAGURL__',"files/flag/flagthumb/");
define('__NEWSLETTERORIGINAL__',BASEUPLOADFILE."newsletter/original/");
define('__NEWSLETTERPATH__',BASEUPLOADFILEPATH."newsletter/original/");

#------page image--------
define('__PAGETHUMBIMAGE__',"files/pageimage/thumb/");
define('__PAGEORIGINALIMAGE__',"files/pageimage/original/");
define('PAGEORIGINALIMAGEPATH',BASEUPLOADFILE."pageimage/original/");
define('__PAGELARGEIMAGE__',"files/pageimage/large/");

define('__PAGEEXTRALARGEIMAGE__',BASEUPLOADFILE."pageimage/extralarge/");
define('__PAGEEXTRALARGEIMAGEPATH__',BASEUPLOADFILEPATH."pageimage/extralarge/");

#---------language files---------
define('__DIR_LANGUAGES__', __BASE_DIR_FRONT__. 'languages/');

define('__EMAILFILE__',BASEUPLOADFILE."email/xls/");

define('__TESTIMONIALORIGINAL__',BASEUPLOADFILE."testimonial/original/");
define('__TESTIMONIALLARGE__',BASEUPLOADFILE."testimonial/large/");
define('__TESTIMONIALTHUMB__',BASEUPLOADFILE."testimonial/thumb/");
define('__TESTIMONIALPATH__',BASEUPLOADFILEPATH."testimonial/original/");

#--------color pallet image-------
define('__PALLETIMGORIG__',"files/color/thumb/");
define('__PALLETIMGTHUMB__',"files/color/original/");


#--------raw product image-------
define('__PRODUCTTHUMB__',"files/rawproduct/productimage/thumb60x60/");
define('__PRODUCTTHUMB_125x160_',"files/rawproduct/productimage/thumb125x160/");
define('__PRODUCTTHUMB_150x265_',"files/rawproduct/productimage/thumb150x265/");
define('__PRODUCTORIGINAL__',"files/rawproduct/productimage/original/");
define('__PRODUCTLARGE__',"files/rawproduct/productimage/large/");

define('__PRODUCTTOOLTHUMB__',"files/rawproduct/toolImage/thumb/");
define('__PRODUCTTOOLORIGINAL__',"files/rawproduct/toolImage/original/");
define('__PRODUCTTOOLLARGE__',"files/rawproduct/toolImage/large/");

define('__PRODUCTBASICTHUMB__',"files/rawproduct/basicimage/thumb/");
define('__PRODUCTBASICORIGINAL__',"files/rawproduct/basicimage/original/");

define('__PRODUCTPARTTHUMB__',"files/rawproduct/productPart/thumb/");
define('__PRODUCTPARTORIGINAL__',"files/rawproduct/productPart/original/");
define('__PRODUCTPARTLARGE__',"files/rawproduct/productPart/large/");

define('__PRODUCTVIEWTHUMB__',"files/rawproduct/viewimage/thumb/");
define('__PRODUCTVIEWORIGINAL__',"files/rawproduct/viewimage/original/");

define('__PRODUCTPDF__',"files/rawproduct/pdf/");

#---------product zoom image------------
define('__ZOOMPRODUCTTHUMB__',"files/rawproduct/productimage/zoomImage/thumb/");
define('__ZOOMPRODUCTLARGE__',"files/rawproduct/productimage/zoomImage/large/");
define('__ZOOMPRODUCTORIGINAL__',"files/rawproduct/productimage/zoomImage/original/");
define('__ZOOMPRODUCTEXTRALARGE__',"files/rawproduct/productimage/zoomImage/extralarge/");

#------banner--------
define('__BANNERTHUMB__',"files/banner/thumb/");
define('__BANNERORIGINAL__',"files/banner/original/");

#-------deco upload-------
define('__DECOTHUMB__',"files/userdeco/thumb/");
define('__DECOORIGINAL__',"files/userdeco/original/");
define('__DECOIMAGE__',"files/userdeco/image/");

#--------------define tempimage path-----------
define('__TEMPIMAGE__',"files/tempImage/");

#--------------main product image path-----------
define('__MAINPRODTHUMB__',"files/mainproduct/thumb/");
define('__MAINPRODORIGINAL__',"files/mainproduct/original/");

#---------------photo print constant-------------
define('__PHOTOPRINT__',"Photo Print");
define('__PHOTOPRINTCOLOR__', 6);
#---------------default currency id-------------
define('__CURRENCYID__',"1");

#--------------Quantity Surcharge--------------
define('__QUANTITYSURCHARGE__',"25");

define('__DECOPREFIX__',"org");
?>

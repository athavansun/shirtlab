<?php
define('PREFIX', 'ecart_'); 

define('TBL_ADMINLOGIN', PREFIX.'adminlogin'); 

define('TBL_MENU', PREFIX.'menu');
define('TBL_ADMINPERMISSION', PREFIX.'adminpermission');
define('TBL_ADMINLEVEL', PREFIX.'adminlevel');
define('TBL_SYSTEMCONFIG', PREFIX.'system_config');
define('TBL_CATEGORY', PREFIX.'category');
define('TBL_CATEGORY_DESCRIPTION', PREFIX.'categorydesc');
define('TBL_LANGUAGE', PREFIX.'language');
define('TBL_LANGATTRIBUTE', PREFIX.'languageattribute'); 
define('TBL_SESSIONDETAIL', PREFIX.'sessiondetail');
define('TBL_LOGDETAIL', PREFIX.'logdetail');

define('TBL_MAILTYPE', PREFIX.'mail_type');
define('TBL_MAILSETTING', PREFIX.'mail_setting'); 

#----------newsletter---------
define('TBL_NEWSLETTER', PREFIX.'newsletter');
define('TBL_NEWSLETTERTBL_USERTYPE', PREFIX.'newsletterusertype');
define('TBL_NEWSLETTERSUBSCRIBER', PREFIX.'newslettersubscriber');
define('TBL_NEWSLETTERTEMPLATE', PREFIX.'newsletter_template');

define('TBL_USER', PREFIX.'user');
define('TBL_GIFTCERTIFICATE', PREFIX.'giftcertificate');
define('TBL_GIFTCERTIFICATEDESC', PREFIX.'giftcertificatedesc');


define('TBL_STATICPAGETYPE', PREFIX.'staticpagetype');
define('TBL_STATICPAGESETTING', PREFIX.'staticpage_setting');

define('TBL_COUPON', PREFIX.'coupon');
define('TBL_COUPON_DESCRIPTION', PREFIX.'coupondesc');
define('TBL_COUPON_TO_PRODUCT', PREFIX.'coupon_to_product');
define('TBL_COUPON_TO_CUSTOMER', PREFIX.'coupon_to_customer');

define('TBL_CONTACTUSTYPE', PREFIX.'contactustype');
define('TBL_CONTACTUS', PREFIX.'contactus');

define('TBL_COUNTRY', PREFIX.'country');
define('TBL_COUNTRY_DESCRIPTION', PREFIX.'countrydesc'); 
define('TBL_COUNTRYSETTING', PREFIX.'country_setting');
define('TBL_COUNTRYSETTINGDESC', PREFIX.'country_settingdesc'); 

define('TBL_STATE', PREFIX.'state');
define('TBL_STATE_DESCRIPTION', PREFIX.'statedesc');

#---------raw product---------
define('TBL_PRODUCT', PREFIX.'product');
define('TBL_PRODUCT_DESCRIPTION', PREFIX.'product_description');
define('TBL_PRODUCTSIZE', PREFIX.'product_size');
define('TBL_PRODUCT_VIEW', PREFIX.'product_view');
define('TBL_PRODUCT_TOOL', PREFIX.'product_tool');
define('TBL_PRODUCT_PART', PREFIX.'product_part');
define('TBL_PRODUCT_SPECIAL_PART', PREFIX.'product_special_part');

define('TBL_PRODUCT_STYLE', PREFIX.'product_style');
define('TBL_PRODUCT_STYLEDESC', PREFIX.'product_styledesc');
define('TBL_PRODUCT_FABRIC', PREFIX.'product_fabric');

define('TBL_PRODUCT_PAGE_IMAGE', PREFIX.'product_page_image');
define('TBL_PRODUCT_ZOOMIMAGE', PREFIX.'product_zoomImage');
define('TBL_PRODUCT_FABRICGROUP', PREFIX.'product_fabricgroup');

#---------- title table replace usertitle -------------
define('TBL_TITLE', PREFIX.'title');
define('TBL_TITLEDESC', PREFIX.'titledesc');

define('TBL_CURRENCY_DETAIL', PREFIX.'currency_detail');

define('TBL_CURRENCY', PREFIX.'currency');
define('TBL_LANGUAGEATTRIBUTE', PREFIX.'languageattribute');
define('TBL_ATTRIBUTE', PREFIX.'attribute');

define('TBL_EMAILSCHEDULE', PREFIX.'email_schedule');

define('TBL_COUNTYDESC', PREFIX.'countydesc');

define('TBL_ORDER', PREFIX.'orders');
define('TBL_ORDERDETAIL', PREFIX.'orderdetail');
define('TBL_ORDERDETAIL_ATTRIBUTES', PREFIX.'orderdetail_attributes');
define('TBL_ORDER_DECO', PREFIX.'order_deco');
define('TBL_ORDER_CUMULATION', PREFIX.'order_cumulation');
#-------- color pallet-------
define('TBL_PALLET', PREFIX.'pallet');
define('TBL_PALLETDESC', PREFIX.'palletdesc');
define('TBL_PALLETCOLOR', PREFIX.'pallet_color');
define('TBL_SPECIAL_COLOR', PREFIX.'special_color');

define('TBL_TOOL', PREFIX.'tool');
define('TBL_TOOLDESC', PREFIX.'tooldesc');

define('TBL_VIEW', PREFIX.'view');
define('TBL_VIEWDESC', PREFIX.'viewdesc');

#---------Main Product-------------
define('TBL_MAINPRODUCT', PREFIX.'mainproduct');
define('TBL_MAINPRODUCT_VIEW', PREFIX.'mainproduct_view');


define('TBL_SIZE', PREFIX.'size');
define('TBL_SIZEDESC', PREFIX.'sizedesc');
define('TBL_SIZEPRICE', PREFIX.'size_price');
define('TBL_SIZEATTRIBUTE', PREFIX.'size_attribute');
define('TBL_SIZE_GROUP', PREFIX.'size_group');
define('TBL_SIZE_GROUPDESC', PREFIX.'size_groupdesc');


define('TBL_USERIMAGE', PREFIX.'userimage');

define('TBL_ORDERSTATUS', PREFIX.'orderstatus');
define('TBL_ORDERSTATUSDESC', PREFIX.'orderstatusdesc');
define('TBL_ORDER_SIZE', PREFIX.'order_size');
define('TBL_ORDER_COLOR', PREFIX.'order_color');
define('TBL_ORDER_PAYMENTSTATUS', PREFIX.'order_paymentstatus');
define('TBL_ORDER_PAYMENTSTATUSDESC', PREFIX.'order_paymentstatusdesc');



define('TBL_BASKET', PREFIX.'basket');
define('TBL_BASKET_SIZE', PREFIX.'basket_size');
define('TBL_BASKET_COLOR', PREFIX.'basket_color');
define('TBL_BASKET_DECO', PREFIX.'basket_deco');
define('TBL_BASKET_DECO_QTY', PREFIX.'basket_deco_quantity');
define('TBL_BASKET_CUMULATION', PREFIX.'basket_cumulation');
define('TBL_PRODUCT_DETAIL_IMAGE', PREFIX.'product_detail_image');
define('TBL_COMMITLOG', PREFIX.'commitlog');



#-----delivery time
define('TBL_DELIVERY_TIME', PREFIX.'deliverytime');
define('TBL_DELIVERY_TIME_CHARGES', PREFIX.'deliverytime_charge');
define('TBL_VALUE_POINTS', PREFIX.'valuepoint');

#-------- color pallet-------
define('TBL_PALLET', PREFIX.'pallet');
define('TBL_PALLETDESC', PREFIX.'palletdesc');
define('TBL_PALLETCOLOR', PREFIX.'pallet_color');

#--------Fabric price--------
define('TBL_FABRIC', PREFIX.'fabric');
define('TBL_FABRICDESC', PREFIX.'fabricdesc');
define('TBL_FABRICCHARGE', PREFIX.'fabric_charge');define('TBL_CRDISCOUNT' , PREFIX.'cumulative_ratio_discount');

#--------Embroidery discount--------
define('TBL_EMBROIDERY_RATE', PREFIX.'embroidery_min_rate');
define('TBL_EMBROIDERY_DISCOUNT', PREFIX.'embroidery_discount');

#--------Accessories discount--------
define('TBL_ACCESSORIES', PREFIX.'accessories');
define('TBL_ACCESSORIESDESC', PREFIX.'accessoriesdesc');
define('TBL_ACCESSORIES_DISCOUNT', PREFIX.'accessories_discount');

#-------------Banner---------------
define('TBL_BANNER', PREFIX.'banner');
define('TBL_BANNER_DESCRIPTION', PREFIX.'bannerdesc');

#-----------------Country Setting Panel----------------------
define('TBL_COUNRTYSETTING', PREFIX.'country_setting');
define('TBL_COUNRTYSETTINGDESC', PREFIX.'country_settingdesc');

#---------designer tool----------
define('TBL_COLLECTION', PREFIX.'pro_collection');
define('TBL_COLLECTION_DATA', PREFIX.'pro_collection_data');
define('TBL_COLLECTION_DECO', PREFIX.'pro_collection_deco');
define('TBL_COLLECTION_IMAGE', PREFIX.'pro_collection_image');

define('TBL_ADDRESS', PREFIX.'address');

#--------Printing discount--------
define('TBL_PRINTING_COLOR', PREFIX.'printing_color');

#-------------Gift Voucher---------------
define('TBL_GIFT', PREFIX.'gift');
define('TBL_GIFT_DENOMINATION', PREFIX.'gift_denomination');
define('TBL_GIFT_DENOMINATION_PER', PREFIX.'gift_denomination_per');
define('TBL_GIFT_DETAIL', PREFIX.'gift_detail');
define('TBL_GIFT_TEMPLATE', PREFIX.'gift_template');
define('TBL_FONT_NAME', PREFIX.'font_name');
define('TBL_GIFT_SEND', PREFIX.'gift_send');

#---------QUANTITY SURCHARGE-------------
define('TBL_QUANTITY_SURCHARGE', PREFIX.'quantity_surcharge');

#---------Pantone Number-------------
define('TBL_PANTONE', PREFIX.'pantone');


?>

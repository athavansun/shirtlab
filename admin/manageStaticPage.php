<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/
$staticpageObj = new StaticPage();

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->
<!--          *****************            DRAG AND DROP  - START        ***************   -->
<!--<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>-->
<!--          *****************            DRAG AND DROP  - END          ***************   -->
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>Static Pages
   <? if($menuObj->checkAddPermission()) { ?>
   <a class="btn small fr" href="addStaticPageType.php">Add New Page</a>
   <? } ?>
   </h1>
<fieldset>
<form name="ecartFrm"  method="post" action="pass.php?action=staticpagetype&type=deleteall"   >
 <label>Static Pages Details</label>
<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />

<div class="top-filter bg02">
<div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
	<div id="search-main-div">
		<ul>
			<li>Select Action :</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						   	</select>
			</li>
			<li><input type="submit" value="Submit" name="Input"></li>
			<li><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:'Search'?>" onClick="clickclear(this, 'Search')" onBlur="clickrecall(this,'Search')"/></li>
			<li><input name="GO" type="submit" value="Go" /></li>
			<li><a href="<?=$_SERVER['PHP_SELF']?>">Reset</a></li>
		</ul>
</div>	
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
   <table class="documentation">
					<tr>
                    	<thead>
 								<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></th>
								<th>SL.No</th>
 								<th><?=orderBy("manageStaticPage.php?searchtxt=$searchtxt","staticPageName","Page Name")?></th>
								<th><?=orderBy("manageStaticPage.php?searchtxt=$searchtxt","pageUrl","Page URL")?></th>
								<th>Details</th>
								<th>Edit</th>
                <th>Delete</th>
						 </thead>
					</tr>	 
	<?  echo $staticpageObj->allStaticPageTypeInformation();?>
</table>
</form>
</fieldset>
</section>
</body>
</html>
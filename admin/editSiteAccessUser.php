<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageSiteAccessUser.php", "");
//$userObj = new User();
$siteuserObj = new SiteAccessUser();
$generalObj = new GeneralFunctions();
$id = $_GET['uid'] ? $_GET['uid'] : 0;
$id = base64_decode($id);
require_once('validation_class.php');
$obj = new validationclass();
if (isset($_POST['submit'])) {
    $obj->fnAdd('userName', $_POST['userName'], 'req', 'Please Enter User Name.');
    if ($_POST['password'] != "") {
        $obj->fnAdd('password', $_POST['password'], 'req', 'Please Enter password.');
    }
    //=================================================================================
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
    //=================================================================================
    $arr_error[userName] = $obj->fnGetErr($arr_error[userName]);
    $arr_error[password] = $obj->fnGetErr($arr_error[password]);
    if ($siteuserObj->isUserNameExists($_POST['userName'], $id)) {
        $arr_error[userName] = "User Name already exist. ";
    }
    //echo "<pre>";  print_r($arr_error);exit;
    elseif ($str_validate) {
        //if (empty($arr_error[firstName]) && empty($arr_error[lastName]) && empty($arr_error[postCode]) && empty($arr_error[city]) &&  empty($arr_error[countryId]) && empty($arr_error[stateId]) && empty($arr_error[phoneNo]) && empty($arr_error[status]) && isset($_POST['submit'])) {
        $_POST = postwithoutspace($_POST);
        $siteuserObj->editRecord($_POST);
    }
} else {
    $_POST = displayWithStripslashes($siteuserObj->getUserById($id));
    //echo "<pre>";  print_r($_POST); echo "</pre>";exit;
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions   ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script type="text/javascript">
    function hrefBack1() {
        window.location = 'manageSiteAccessUser.php';
    }
</script>
</head>
<body>
    <? include('includes/header.php'); ?>
    <section id="content">
        <h1>Edit Site Access User Profile</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $_POST['id'] ?>" >
            <fieldset>
                <label> Site Access User Details</label> 
<?= $_SESSION['SESS_MSG'] ?>
                <!-- Start : Username ------->
                <section>
                    <label for="userName">User Name </label>
                    <div>
                        <input type="text" name="userName" id="userName"  value="<?= $_POST[userName] ?>" />
<?= $arr_error[userName] ?>
                    </div>
                </section>
                <!-- Start : Password ------->
                <section>
                    <label for="Password">Password</label>
                    <div>
                        <input type="text" name="password" id="Password"  value="<?= $_POST[password] ?>" />
<?= $arr_error[password] ?>
                    </div>
                </section>
            </fieldset>
            <fieldset>
                <section>
                    <label>&nbsp;</label>
                    <div style=" width:78%;">
                        <input type="submit" name="submit"   value="Submit" />
                        <input type="button" name="back" id="back" value="Back"   onclick="javascript:;
                          hrefBack1()"/>
                    </div>
                </section>
            </fieldset>
        </form>
    </section>
    <? unset($_SESSION['SESS_MSG']); ?>
<?php include_once('includes/footer.php'); ?>
    <link type="text/css" href="datepicker/themes/base/ui.datepicker.css" rel="stylesheet" />
    <script src="js/jquery-1.6.2.js"></script>
    <script src="js/jquery.ui.core.js"></script>
    <script src="js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
                          $(function () {
                              $('#dob_date').datepicker({
                                  dateFormat: 'yy-mm-dd',
                                  showButtonPanel: true
                              });
                          });
    </script>

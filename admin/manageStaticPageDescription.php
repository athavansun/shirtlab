<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStaticPage.php");
$id = $_GET[id]?$_GET[id]:0;
$id = base64_decode($id);
/*---Basic for Each Page Starts----*/
$staticpageObj = new StaticPage();
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');
window.onload = function(){
	Shadowbox.init();
};
</script>

</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>Static Pages Content<? if($menuObj->checkAddPermission("manageStaticPage.php")) { ?><a class="btn small fr" href="addStaticPage.php?pageId=<?=base64_encode($id)?>">Add Static Page </a><? } ?></h1>
<fieldset>
<form name="ecartFrm"  method="post" action="pass.php?action=staticpagetype&type=deleteall"   >
 <label><a href="manageStaticPage.php">Static Pages</a> >> <?=stripslashes($staticpageObj->fetchValue(TBL_STATICPAGETYPE,"staticPageName","id='$id'"))?></label>
<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />

<div class="top-filter bg02">
		
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
   <table class="documentation">
					<tr>
                    	<thead>
								<th>SL.No</th>
								<th><?=orderBy("manageStaticPageDescription.php?id=".base64_encode($id)."&searchtxt=$searchtxt",TBL_LANGUAGE.".languageName","Language")?></th>
								<th><?=orderBy("manageStaticPageDescription.php?id=".base64_encode($id)."&searchtxt=$searchtxt","pageTitle","Title")?></th>
								<th>View</th>
								<th>Edit</th>
								<th>Delete</th>
						 </thead>
					</tr>	 
	<? echo $staticpageObj->allStaticPageDetailsInformation($id);?>
</table>
</form>
</fieldset>
</section>
</body>
</html>
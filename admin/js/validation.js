function hrefBack() {
	history.go(-1);	
}

function displayCursor()
{
	document.frmLogin.userName.focus();
}	

function validationLogin()
{
	if(document.getElementById('userName').value==""){
		alert(PLEASE_ENTER_USERNAME);
		document.getElementById('userName').focus();
		return false;
	}
		
	if(document.getElementById('userPassword').value=="")
	{
		alert(PLEASE_ENTER_VALID_PASSWORD);
		document.getElementById('userPassword').focus();
		return false;
	}
}


function validationAddAdmin()
{ 
	document.getElementById('chk_userName').innerHTML = '';
	document.getElementById('chk_userEmail').innerHTML = '';
	document.getElementById('chk_password').innerHTML = '';
	
	if(document.frmUser.userName.value=="")
	{
		document.getElementById('chk_userName').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_USERNAME+'</span>';
		document.frmUser.userName.focus();
		return false;
	} 

	if(document.frmUser.userEmail.value=="")
	{
		document.getElementById('chk_userEmail').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_EMAIL_ID+'</span>';
		document.frmUser.userEmail.focus();
		return false;
	}

	var emailRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	str = document.getElementById('userEmail').value;
	if(!str.match(emailRegEx)){
		document.getElementById('chk_userEmail').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_VALID_EMAIL_ADDRESS+'</span>';
		document.frmUser.userEmail.focus();
		return false;		
	}
	
	if(document.frmUser.password.value=="")
	{
		document.getElementById('chk_password').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_PASSWORD+' </span>';
		document.frmUser.password.focus();
		return false;
	}
		
	if(document.frmUser.password.value.length<6)
	{
		document.getElementById('chk_password').innerHTML = '<span class="alert-red alert-icon">'+IT_SHOULD_ATLEAST_6_CHARACTERS+'</span>';
		document.frmUser.password.select();
		document.frmUser.password.focus();
		return false;
	}
}

function validationEditAdmin()
{ 
	document.getElementById('chk_userName').innerHTML = '';
	document.getElementById('chk_userEmail').innerHTML = '';
	document.getElementById('chk_password').innerHTML = '';
	
	if(document.frmUser.userName.value=="")
	{
		document.getElementById('chk_userName').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_USERNAME+'</span>';
		document.frmUser.userName.focus();
		return false;
	} 

	if(document.frmUser.userEmail.value=="")
	{
		document.getElementById('chk_userEmail').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_EMAIL_ID+'</span>';
		document.frmUser.userEmail.focus();
		return false;
	}

	var emailRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	str = document.getElementById('userEmail').value;
	if(!str.match(emailRegEx)){
		document.getElementById('chk_userEmail').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_VALID_EMAIL_ADDRESS+'</span>';
		document.frmUser.userEmail.focus();
		return false;		
	}
			
	if(document.frmUser.password.value!="" && document.frmUser.password.value.length<6)
	{
		document.getElementById('chk_password').innerHTML = '<span class="alert-red alert-icon">'+IT_SHOULD_ATLEAST_6_CHARACTERS+'</span>';
		document.frmUser.password.select();
		document.frmUser.password.focus();
		return false;
	}
}


function validationCategory()
{
	if(document.getElementById('categoryName').value=="")
	{
		document.getElementById('chk_categoryName').innerHTML = '<span class="alert-red alert-icon">'+PLEASE_ENTER_CATEGORY_NAME+'</span>';
		document.getElementById('categoryName').focus();
		return false;
	}		
}

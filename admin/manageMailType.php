<?
/*---Basic for Each Page Starts----*/

session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');//print_r($_SESSION);
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Ends----*/

$mailSettingObj = new MailSetting();
if ($action == 'mailtype') {
	if($type == 'deleteall'){
		header("Location:manageMailType.php?searchtxt=".$_POST['searchtext']);
	}
}


?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>


<!--          *****************            DRAG AND DROP  - START        ***************   -->
<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>
<!--          *****************            DRAG AND DROP  - END          ***************   -->
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>Email Type</h1>
<fieldset>
<form name="ecartFrm" method="post" action="pass.php?action=mailtype&type=deleteall"   >
 <label>Email Type Details</label>

<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />

<div class="top-filter bg02"> 	
	<div id="search-main-div">
		<ul>

			<li class="selectall">
				<!--<div id="check"> <a href="javascript:void(NULL)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" style="display:none;"><a href="javascript:void(NULL)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>-->
			</li>
			<li class="action"><!--Action:--></li>
			<li>
				<!--<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission()) ){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>
						<? } ?>		
                 </select>-->
			</li>
			<li><!--<input name="Input" type="submit" value="Submit"  class=""/>--></li>

			<li style="padding-left:400px;"><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:SEARCHTEXT?>" onClick="clickclear(this, 'Search')" onBlur="clickrecall(this,'Search')"/></li>
			<li><input name="GO" type="submit" value="Go"  class=""/></li>

			<li class="showall"><a href="manageMailType.php">Reset</a></li>

		</ul>	
	</div>	
</div>
<? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
   <table class="documentation">
					<tr>
                    	<thead>
								<th>SL. No</th>
                 <th>ID</th>
								<th><?=orderBy("manageMailType.php?searchtxt=$searchtxt","mailType",'Email Type')?></th>
								<th>View Mail</th>
						 </thead>
					</tr>	 
	<?
		echo $mailSettingObj->allMailTypeInformation();
	?>
</table>
</form>
</fieldset>
</section>
</body>
</html>

<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTool.php", "edit_record");
/* ---Basic for Each Page Ends---- */
$toolObj = new Tool();
$genObj = new GeneralFunctions();
if (isset($_POST['submit'])) {

   require_once('validation_class.php');
   $obj = new validationclass();
   $rst = $toolObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");

   $num = $toolObj->getTotalRow($rst);
   if ($num) {
      $langIdArr = array();
      while ($line = $toolObj->getResultObject($rst)) {
         array_push($langIdArr, $line->id);
      }

      $obj->fnAdd('VariableName', $_POST['VariableName'], 'req', "Please enter variable");

      foreach ($langIdArr as $key => $value) {
         $obj->fnAdd('toolName_' . $value, $_POST['toolName_' . $value], 'req', "Please enter name");
      }

      $arr_error = $obj->fnValidate();
      $str_validate = (count($arr_error)) ? 0 : 1;
      $arr_error[VariableName] = $obj->fnGetErr($arr_error['VariableName']);

      foreach ($langIdArr as $key => $value) {
         $arr_error['toolName_' . $value] = $obj->fnGetErr($arr_error['toolName_' . $value]);
      }
     
      if ($str_validate) {
         $_POST = postwithoutspace($_POST);
         $toolObj->editRecord($_POST);
      }
   }
}
$result = $toolObj->getResult(base64_decode($_GET['id']), $_REQUEST['frontOrAdmin']);
?>

<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
   function hrefBack1(){
      window.location='manageTool.php?frontOrAdmin=<?= $_REQUEST['frontOrAdmin'] ?>';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head> 
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Tool</h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
           <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>" />
         <input type="hidden" name="page" value="<?=$_GET['page']?>" />
         <fieldset>
            <label>Edit Tool Variable</label>
            <?= $_SESSION['SESS_MSG'] ?>
            <section>
               <label>Variable</label>
               <div>
                  <!-- readonly="1"-->
                  <input type="text" readonly="readonly" name="VariableName" id="m__Variable" value="<?= $_POST['VariableName'] ? $_POST['VariableName'] : $result->variableName ?>" />
                  <div><?= $arr_error['VariableName'] ?></div>
               </div>
            </section>
            <section>
               <label>Value</label>
               <div>
                  <?
                  $genObj = new GeneralFunctions();
                  echo $genObj->getLanguageEditTextarea('toolName', 'm__Name', TBL_TOOLDESC, base64_decode($_GET['id']), "id", $arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
                  ?>
               </div>
            </section>
         </fieldset>
         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">
                  <input type="submit" name="submit"   value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
      </form>
   </section>
   <? include_once('includes/footer.php');?>
   <? unset($_SESSION['SESS_MSG']); ?>

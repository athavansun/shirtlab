<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProductPageImage.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$prodObj = new Products();
$fabricObj = new ProductImage();

$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();

if(isset($_GET['catId'])) {
    $catId = base64_decode($_GET['catId']);
    //$isDefault = $fabricObj->fetchValue(TBL_PRODUCT_PAGE_IMAGE, "isDefault", "catId = '".$catId."'");
    $rs = $fabricObj->executeQry("Select * from ".TBL_PRODUCT_PAGE_IMAGE." where catId = '".$catId."'");
    $result = $fabricObj->getResultObject($rs);    
}

if(isset($_POST['submit'])) {
    $str_validate = 1;
    if(!empty($_FILES['productImage']['name']) || !empty($_FILES['sizeImage']['name'])) {
        require_once('validation_class.php');
        $obj = new validationclass();

        $obj->fnAdd('productImage', $_FILES['productImage']['name'], 'req', "Please Upload Product Page Image.");
        $obj->fnAdd('sizeImage', $_FILES['sizeImage']['name'], 'req', "Please Upload Product Size Image.");

        $arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;

        $arr_error[productImage]=$obj->fnGetErr($arr_error[productImage]);
        $arr_error[sizeImage]=$obj->fnGetErr($arr_error[sizeImage]);

        if($_FILES['productImage']['name']){        
            $filename = stripslashes($_FILES['productImage']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);             
            if(!$fabricObj ->checkExtensions($extension)){  
                $arr_error[productImage] = '<span class="alert-red alert-icon">Upload Only '.$fabricObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") .' image extension.</span>';
                $str_validate=0;
            }
        }


        if($_FILES['sizeImage']['name']){           
            $filename = stripslashes($_FILES['sizeImage']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);             
            if(!$fabricObj ->checkExtensions($extension)){  
                $arr_error[sizeImage] = '<span class="alert-red alert-icon">Upload Only '.$fabricObj ->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") .' image extension.</span>';
                $str_validate=0;
            }
        }
    } 
    
    if($str_validate){
		$_POST = postwithoutspace($_POST);
		$fabricObj->editProductImage($_POST, $_FILES);
	}
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageProductPageImage.php';
}

</script>
<!-- New Drop Down menu -->

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Edit Product Page Image</h1><fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">		
		 <fieldset>  
            <label>Product Page Image</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                <label>Category Name</label>
                <div>
                	<select name="catId" onChange="return showTypeOnNameChange(this.value);" disabled="disabled">
                		<option value="">Select Category Name</option>                		
                                <?= $prodObj->getCategoryListSingle(isset($_POST['catId'])?$_POST['catId']:base64_decode($_GET['catId'])); ?>
                	</select>
                	<?= $arr_error['catId']?>
			    </div>
            </section>
            
            <section>
                <label>Product Page Image<span class="spancolor">*</span></label>
                <div><input type="file" class="file browse-button" name="productImage" id="m__productImage" value="<?=$_POST[pageImage] ?>" />                    
                 <?=$arr_error['productImage'] ?>	                    
                </div>
                <div>
                    <img src="<?= SITE_URL.__PAGETHUMBIMAGE__.$result->productPageImage; ?>" />
                    <pre>image dimension should be 424 * 145</pre>   
                </div>
            </section>            

            <section>
            <label>Product Size Image<span class="spancolor">*</span></label>
            <div><input type="file" class="file browse-button" name="sizeImage" id="m__sizeImage" value="<?=$_POST[sizeImage] ?>" />
             <?=$arr_error['sizeImage'] ?>	            
            </div>
            <div>
                <img src="<?= SITE_URL.__PAGETHUMBIMAGE__.$result->sizeImage; ?>" />
                <pre>image dimension should be 148 * 242</pre>
            </div>
            </section>  
            
            <section>
                <label>Make Default<span class="spancolor">*</span></label>
                <div><input type="checkbox" name="isDefault" <?php if($result->isDefault == '1'){ echo "checked=checked"; echo " readonly";} ?> />
                </div>
            </section>
            
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                 <input type="hidden" name="catId" value="<?= base64_decode($_GET['catId']) ?>" />
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>            
        </fieldset>
        </form> </fieldset>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

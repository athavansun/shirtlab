<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageContactUs.php");
$contactUsObj = new ContactUs();
$id =  base64_decode($_GET['id']);
$id = $id?$id:0;
$row = $contactUsObj->checkIsPageExists($id);

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$obj->fnAdd("replyText",$_POST["replyText"], "req", "Please enter Reply Message.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[replyText]=$obj->fnGetErr($arr_error[replyText]);		
	
	if(empty($arr_error[replyText]) && isset($_POST['submit'])){
		$_POST = postwithoutspace($_POST);
		$contactUsObj->addContactUsReply($_POST);
	}	
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js,Top Pageoptions ?>
</head>
<body>
<?// include('includes/header.php'); ?>
<section>
	<h1>View Contact Us</h1>
<fieldset>
    <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
        <div id="mainDiv"><div><?=$_SESSION['SESS_MSG']?></div>

        <input type="hidden" name="id" value="<?=$id?>" />
        <input type="hidden" name="langId" value="<?=$row['langId']?>" />
        <fieldset>        
            <section>
                <label>Usert Type</label>
                <div>
                    <?
						if($row['userId'])
						{
							echo '<h3>Registered user.</h3>';
							$registerDate = $contactUsObj->fetchValue(TBL_USER,'registerDate',"id = '".$row['userId']."'");
							$accNo = '<section>
										<label>Account No</label>
										<div>'.substr(date('ymd\'hisA', strtotime($registerDate)), 0, -1).'</div>
									</section>';
						}
						else
						{
							echo '<h3>Guest/Interested user.</h3>';
							$accNo = '';
						}
					?>
                </div>
            </section>
            <?=$accNo;?>
            <section>
                <label>Subject</label>
                <div>
                    <?=$row['subject'];?>                    
                </div>
            </section>
            <section>
                <label>Country</label>
                <div>
                    <?
                         $country = $contactUsObj->fetchValue(TBL_COUNTRY_DESCRIPTION,"countryName","countryId='$row[countryId]'");
                         echo $country;
                    ?>                    
                </div>
            </section>
            <section>
                <label>Language</label>
                <div>
                    <?
                         echo stripslashes($contactUsObj->fetchValue(TBL_LANGUAGE,"languageName","id='$row[langId]'"));
                    ?>                    
                </div>
            </section>
            <section>
                <label for="name">Name</label>
                <div>
                    <?
                         echo stripslashes($row['name']);
                    ?>
                </div>
            </section>
            <section>
                <label for="email">Email Id</label>
                <div>
                    <?
                         echo stripslashes($row['emailId']);
                    ?>                    
                </div>
            </section>
            <section>
                <label for="message">Message</label>
                <div>
                    <?
                         echo nl2br(stripslashes($row['messageText']));
                    ?>                    
                </div>
            </section>
            <section>
                <label for="messageDate">Message Date</label>
                <div>
                    <?
                         echo date(DEFAULTDATEFORMAT,strtotime($row['addDate']));
                    ?>                    
                </div>
            </section>
             <?php if($row[replyBy] >0){ ?>
            <section>
                <label for="replyBy">Reply By</label>
                <div>
                    <?
                         echo $contactUsObj->fetchValue(TBL_ADMINLOGIN,"username","id='$row[replyBy]'");
                    ?>                    
                </div>
            </section>
            <section>
                <label for="replymessage">Reply Message</label>
                <div>
                    <?
                         echo nl2br(stripslashes($row['replyText']));
                    ?>                    
                </div>
            </section>
            <section>
                <label for="replyDate">Reply Date</label>
                <div>
                    <?
                         echo date(DEFAULTDATEFORMAT,strtotime($row['replyDate']));
                    ?>                    
                </div>
            </section>
            <?php }else{ ?>
            <section>
                <label for="reply">Reply</label>
                <div>
					Not replied yet!
                    <!--<textarea name="replyText" id="m__Reply_Message" class="wel-textarea" rows="5" cols="40"><?=stripslashes($_POST['replyText'])?></textarea>-->
                </div>
            </section>
            <?php  }  ?>
            </fieldset>
            
            <fieldset>	 
          <!--<section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <?php if($row[replyBy] <=0){ ?>
                 <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
		<?php } ?>
                <input type="button" name="back" id="back" value="Back"
class="main-body-sub-submit" style="cursor:pointer;" 
onclick="javascript:;hrefBack1();"/>
              </div>
          </section>-->
        </fieldset>
        </div>
	</form>
</fieldset>
</section>
</form>
<div id="divTemp" style="display:none;"></div>
<? unset($_SESSION['SESS_MSG']); ?>
<script language="javascript"> 
	function hrefBack1(){
		window.location='manageContactUs.php';
	}
</script>
<?php include_once('includes/footer.php');?>

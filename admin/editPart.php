<?php 
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("managePart.php", "");
/* ---Basic for Each Page Starts---- */
$partObj = new Part();
$generalFunctionObj = new GeneralFunctions();

if (isset($_POST['submit'])) {
   	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $partObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
	$num = $partObj->getTotalRow($rst);
   	if ($num > 0) {
        $langIdArr = array();
      	while ($line = $partObj->getResultObject($rst)) {
        	 array_push($langIdArr, $line->id);
      	}
      	foreach ($langIdArr as $key => $value) {      	
            $obj->fnAdd('partTitle_' . $value, $_POST['partTitle_' . $value], 'req', "Please enter part title");
      	}
	    $arr_error = $obj->fnValidate();
	    $str_validate = (count($arr_error)) ? 0 : 1;		
	    foreach ($langIdArr as $key => $value) {
        	$arr_error['partTitle_' . $value] = $obj->fnGetErr($arr_error['partTitle_' . $value]);
   	  	}
   	  	foreach($langIdArr as $key=>$value) {   				
			if($partObj->checkTitleExists($_POST['partTitle_'.$value],$value, base64_decode($_GET['id']))) {
				$arr_error['partTitle_'.$value] ='<span class="alert-red alert-icon">Title already exist</span>';
				if($arr_error['partTitle_'.$value]){ 
					$errorArr = 1;							
				}
			}
   	  	}
   	  	if ($str_validate && $errorArr == 0) {
	    	$partObj->editRecord($_POST);
		}   	  	
   	}
}
?>
<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script type="text/javascript">
   function hrefBack1(){
      window.location='managePart.php';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Part</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" action="">
            <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>">
            <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
            <fieldset>
               <label>Edit Part Title</label>
<?= $_SESSION['SESS_MSG'] ?>
               <section>
                  <label>Part Title</label>
                  <div>
                 <?= $generalFunctionObj->getLanguageEditTextBox('partTitle', 'm__partTitle', TBL_PARTDESC, base64_decode($_GET['id']), "partId", $arr_error) ?>
                  </div>
               </section>
            </fieldset>

            <fieldset>
               <section>  
                  <label>&nbsp;</label>
                  <div style=" width:78%;">
                     <input type="submit" name="submit"   value="Submit" />
                     <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
                  </div>
               </section>
            </fieldset>
         </form>
      </fieldset>
   </section>
<? unset($_SESSION['SESS_MSG']); ?>

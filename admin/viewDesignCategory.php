<?php
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageDesignCategory.php","");
/*---Basic for Each Page Ends----*/

$catObj = new DesignCategory();
$genObj = new GeneralFunctions();

$cid  = $_GET['cid']?$_GET['cid']:0;
$result = $catObj->getResult(base64_decode($_GET['id']));
if($cid)
	$addHadding = 'Subcategory';
else
   $addHadding  = 'Category';
   
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>Design <?=$addHadding ?> Detail</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>
        <section>
       	  <label for="categoryName"><?=$addHadding ?> Name:</label>
             <div>
         <?=$genObj->getLanguageViewTextBox('categoryName',TBL_DESIGNCATEGORY_DESCRIPTION,base64_decode($_GET['id']),"catId");?>
            </div>
        </section>
		
        <?php /* <section>
       	  <label for="categoryName"><?=$addHadding ?> Image</label>
             <div>
    <img src="<?=__DESIGNCATEGORYTHUMBPATH__.$result->categoryImage?>">
            </div>
        </section>*/ ?>
     </fieldset>       
     </form>  
    </section>        
</body>
</html>
<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$proObj = new Products();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
$pId = $_GET['pId'];
$sId = $_GET['sId'];
if(isset($_POST['addFabric']))
{
	if(count($_POST['fabric']) > 0)	
		$proObj->addProductFabric($_POST);
	else
		$errorArr['fabric'] = '<span class="alert-red alert-icon">Please Select At Least One Fabric.</span>';
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head><style>
	.chb{
		float:left;
		margin-left:50px;
		border-color:gray;
		}
</style>
<body>
<section id="content-detail">
 <fieldset>
		<form id="formProdStyle" name="uploadform_style" method="post" enctype="multipart/form-data" action="" >		
			<fieldset>
				<section>
					<label>Fabric Quality</label>						
					<?=$proObj->getFabricQuality($proObj->getEditProdFabric($pId,$sId),$pId);?>
					<?=$errorArr['fabric'];?>
				</section>
			</fieldset>
			<fieldset>
				<section>
					<div>								
						<input type="hidden" name="pId" value="<?=$pId?>" />
						<input type="hidden" name="sId" value="<?=$sId?>" />
						<input type="submit" name="addFabric" style="cursor:pointer;" value="Submit" />
					</div>
				</section>
			</fieldset>
		</form>
	</fieldset>
</section>
<? include_once('includes/footer.php');?><script>
	$(".chb").each(function()
{
    $(this).change(function()
    {
        $(".chb").prop('checked',false);
        $(this).prop('checked',true);
    });
});
</script>

<?
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu(); 
$menuObj->checkPermission();
$giftCertObj = new GiftCertificate();
$generalFunctionsObj = new GeneralFunctions();
?>
<?php echo headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/common.js" language="javascript" type="text/javascript"></script>
<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">

<style>
                ul li {
                        padding : 0px 0px 0px 0px;
                        margin-left:2px;
                }
</style>

<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>

<script type="text/javascript">	
    Shadowbox.loadSkin('classic', 'lightbox/src/skin');
    Shadowbox.loadLanguage('en', 'lightbox/src/lang');
    Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
    window.onload = function(){
        Shadowbox.init();
    };	
</script>

</head>
<body>
<? include('includes/header.php'); ?>

<section id="content">
   <h1>Miscellaneous <? if($menuObj->checkAddPermission()){ ?><a class="btn small fr" href="generateGiftCertificate.php">Send Gift Voucher</a><a class="btn small fr" href="addGiftCertificate.php">Add Gift Voucher</a><? } ?></h1>
<fieldset>
<form name="ecartFrm"  method="post" action="" >
 <label>Manage Gift Voucher</label>
<input type="hidden" name="page" id="page" value="<?=$_GET['page'] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />
<div class="top-filter bg02">
<div id="search-main-div">
		<ul>
			<li><b>Code</b><p><input type="text" name="code" id="code" value="<?=$_POST['code'] ?>" ></p></li>
			<li><b>Recipient</b><p><input type="text" name="recipient" id="recipient" value="<?=$_POST['recipient'] ?>" ></p></li>
			<li><b>Purchaser</b><p><input type="text" name="purchaser" id="purchaser" value="<?=$_POST['purchaser'] ?>" ></p></li>
			<li><b>Expiry Date</b>
			<p>
				<select name="expiryDate" id="expiryDate" onChange="datePicker(this.value);" >
				<?php
					$sel_1 = ($_POST['expiryDate'] == 0?'selected="selected"':'')
				?>
				<option value="0" <?=$sel_1?> >Any Date</option>
				<?php
					$sel_2 = ($_POST['expiryDate'] == 1?'selected="selected"':'')
				?>
				<option value="1" <?=$sel_2?>>Between</option>
				</select>
			</p>
			<div style="position:relative;">
			<div id="dataPicker" style="visibility:hidden; position:absolute; height:45px; width:270px;" >
			FROM<input style="width:100px;" type="text" id="from" name="from" value="<?=$_POST['from'] ?>"/>
			TO<input style="width:100px;" type="text" id="to" name="to" value="<?=$_POST['to'] ?>"/>
			</div>
			</div>
			</li>
			<li>&nbsp;<p><input name="search" type="submit" value="Search" /></p></li>
			<li>&nbsp;<p><a href="<?=$_SERVER['PHP_SELF']?>">Reset</a></p></li>
		</ul>
</div>
</div>
<? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
<table class="documentation">	
	<tr>      
	<thead>
		<th>SL NO</th>		
		<th>CODE</th>
		<th>RECIPIENT</th>
		<th>PURCHASER</th>
                <th>%</th>
		<th>AMOUNT</th>
		<th>USED</th>
<!--		<th>UNUSED</th>
-->
		<th>EXPIRY DATE</th>		
		<th>Delete</th>		
	</thead>	
	</tr>	 	
	<?php echo $giftCertObj->valDetail($_POST);?>
</table>
</form>
</fieldset>
</section>

</body>
</html>

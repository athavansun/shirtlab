<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//require_once('includes/classes/deliveryTime.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */
$printObj = new Printing();

if (isset($_POST['submit'])) {  
	
	$langIdArr = array();    

	require_once('validation_class.php');
	$obj = new validationclass();
        //$obj->fnAdd('valuePoint', $_POST['valuePoint'], 'req', 'Please Enter Value Point.');
        $obj->fnAdd('cm2', $_POST['cm2'], 'req', 'Please Enter minimum cm.');
                   
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	//$arr_error['valuePoint']=$obj->fnGetErr($arr_error['valuePoint']);
	$arr_error['cm2']=$obj->fnGetErr($arr_error['cm2']);

	if($str_validate) {
	    $printObj->editPrintingDiscount($_POST);
            $color = $printObj->executeQry("Select * from ".TBL_PRINTING_COLOR." order by color");                  $discount = $printObj->executeQry("Select * from ".TBL_EMBROIDERY_DISCOUNT." where 1 and type = '1' order by pcs");
	}
}else {
	$color = $printObj->executeQry("Select * from ".TBL_PRINTING_COLOR." order by color");	
	$discount = $printObj->executeQry("Select * from ".TBL_EMBROIDERY_DISCOUNT." where 1 and type = '1' order by pcs");
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
     	  <h1>Printing</h1>	   
         <!-- <h1>Printing</h1>	 --> 
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);">
   <?		echo $_SESSION['SESS_MSG'];                
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
            	<label>Printing Rate Panel<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addColorQuantity.php">Add Color Quantity</a><? } ?></label>                
                <?php
                    while($line = $printObj->getResultObject($color)){ ?>                    	
                        <section>
                            <div style="width:55%; float:left;">                                                                                   
                                    <span style="font-size: 13px; margin-left:10px"><?= $line->color;?> Color-Print -> 1cm2 is </span>
                            </div>                                                    
                            <div style="width:29%; float:left;">
                                <input type="text" name="valuePoint_<?= $line->id;?>" id="valuePoint_<?= $line->id;?>" value="<?= $line->valuePoint; ?>" class="wel" style="width:75px;" />
                                <span style="font-size: 13px;"> Value-Points </span>
                            </div>
                             <div style="width:5%; float:left;">
                             	<a class='i_trashcan edit' href='javascript:void(0);'  onClick="if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=managePrinting&type=deleteColor&id=<?php echo $line->id;?> &page=$page'}else{}" >Delete</a>
                             </div>                                                                 
                        </section>
                <?  }                
                ?>
                <section>
                	<div style="width:55%; float:left;">                                                                                   
						<span style="font-size: 13px; margin-left:10px">Min. Quantity in cm2</span>
                    </div>
                	<div style="width:40%; float:left;">                	
						<input type="text" name="cm2" id="m__cm2" value="<?php echo $printObj->fetchValue(TBL_EMBROIDERY_RATE, "min_cm", " type = '1'"); ?>" class="wel" style="width:250px;" /><span style="font-size: 13px; margin-left:5px;">cm2</span>
						<?=$arr_error[cm2]?>    		
                	</div>                	
                </section>
                <section></section>
                <label>Printing Quantity-Discount <? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addPrintingDiscount.php">Add Printing Discount</a><? } ?></label>
                <?php
                    while($line = $printObj->getResultObject($discount)){ ?>                    	
                        <section>                    
                            <div style="width:55%; float:left;">                                                                                   
                            <span style="font-size: 13px; margin-left: 25px;"><?= $line->pcs;?> pcs</span>
                            </div>                           
                            
                            <div style="width:7%; float:left;">   
                            <?php
                                if($line->sign == 0){
                                    $status = "minus";
                                } else if($line->sign == 1) {
                                    $status = "x";
                                } else {
                                    $status = "plus";
                                }
                            ?>
                                    <span style="font-size: 13px; margin-left:11px;"><?= $status;?></span>
                            </div>                                                          
                            <div style="width:22%; float:left;"> <?php if($line->sign != 1) { ?>                                                      
                                <input type="text" name="charge_<?= $line->id;?>" id="charge_<?= $line->id;?>" value="<?= $line->charge; ?>" class="wel" style="width:75px;" />     <?php }else { echo '<span style="font-size: 13px; margin-left:75px;">0</span>';} ?>
                                <span style="font-size: 17px;"> % </span>
                            </div>
                             <div style="width:5%; float:left;">
                             	<a class='i_trashcan edit' href='javascript:void(0);'  onClick="if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=managePrinting&type=delete&id=<?php echo $line->id;?> &page=$page'}else{}" >Delete</a>
                             </div>                                                                 
                        </section>
                <?  }                
                ?>   
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>

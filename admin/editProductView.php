<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php", "edit_record");
/* ---Basic for Each Page Ends---- */

$prodObj = new Products();
$prodId = base64_decode($_GET['prodId']);
$colorId = base64_decode($_GET['colorId']);
//get color name & code
$colorArr = $prodObj->getViewColorDetail($colorId);
$colImg = __PRODUCTCOLORTHUMBPATH__.$colorArr['img'];
//

$viewArr = $prodObj->getColorViewImageUploadEdit($prodId,$colorId);
?>
	<?=headcontent()?>
	<script src="js/file/jquery.filestyle.js"></script>
	<!-- Color Picker (START) -->
	<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
	<script src="colorpicker/color_functions.js"></script>
	<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
	<!-- Color Picker (END)	-->
</head>
<body>
  <?php
    if(count($viewArr ) > 0)
    {
        $genObj = new GeneralFunctions();
    	?>
    	<section id="content11">
    		<!-- <h1>Manage Product</h1> -->
			<fieldset>
				<form name="frmUser" method="post" action="pass.php?action=productColor&type=updadeColorImage" id="frmUser" enctype="multipart/form-data">
					<fieldset>
						<label>Edit Product Color And Images</label>
						<? //echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
						 <section>
							<label for="designcolorcode">Design Color Code</label>
							<div>
								<table style="padding:0px;margin:0px;"  border="0">
									<tr>
										<td><input type="text" name="designColorCode" id="m__design_color_code" maxlength="7" value="<?=$colorArr['designCode']?>" /></td>
										<td style="text-align:left; width:250px;"><img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmUser.designColorCode)" border="0" style="cursor:pointer;"></td>
									</tr>
								</table>
								<div id="chk_designColorCode"><?=$arr_error['designColorCode'];?></div>
							</div>
						</section>
						<section>
							<label for="colorcode">Color Code</label>
							<div>
								<table style="padding:0px;margin:0px;border:0px;">
									<tr>
										<td><input type="text" name="colorCode" id="m__color__code" maxlength="7" value="<?=$colorArr['code']?>" /></td>
										<td style="text-align:left; width:250px;"><img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmUser.colorCode)" border="0" style="cursor:pointer;"></td>
									</tr>
								</table>
								<div id="chk_colorCode"><?=$arr_error['colorCode'];?></div>
							</div>
						</section>
						<section>
							<label for="name">Color Name</label>
							<div>
								<?=$genObj->getLanguageEditTextBox('colorTitle','m__Color_Title',TBL_PRODUCT_COLOR_DESCRIPTION,$colorId,"product_color_id",$arr_error);?>
				 				<?//=$genObj->getLanguageTextBoxEngValidation('colorName','m__Color_Name',$arr_error);?>
							</div>
						</section>
						<section>
							<label for="productImage">Product Images</label>
							<div>
								<table>
							 	<?php 
									foreach($viewArr as $val)
        							{
							            $viewidarr[$val['imageType']] = $val[viewid]; // product view id
							            ?>
        							    <tr>
    										<td>
                								<img src="<?=$val['img']?>" alt="" />
                								 <label style="width:100%;float:left;text-align:center;"><?=$val['imageType']?></label>
                						    </td>
       										<td>
                								<div style="float:left;">
                									<input name="prodImage<?=$val['viewid']?>" type="file" class="browse-button" />
                								</div>
                								<? if($val['viewid1']) { // not in use ?>
                									<span style="margin-top:12px; float:right;">
                										<a href="javascript:;" onClick="deleteRawProdImg('<?=$prodId?>','<?=$colorId?>','<?=$val['viewid']?>');">
                										<img src="images/close_small.png" alt="Close" title="Remove" border="0" /></a>
                									</span>
                								<? } ?>
                							</td>
                						</tr>
              							<?php
        							}?></table>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<section>
							<label for="name">&nbsp;</label>
							<div>
							
							<!-- <a id="fancybox-close" style="display:inline;"></a> <input type="submit" name="" value="Update"> 
							<button type="button" name="back" onClick="hrefback;" id="back">Back</button>-->
							<button type="submit" name="">Submit</button>
							<input type="hidden" name="productId" value="<?=$prodId?>" />
        					<input type="hidden" name="colorId" value="<?=$colorId?>" />
        					<input type="hidden" name="viewidcsv" value='<?=serialize($viewidarr)?>' />
        					
							</div>
						</section>
					</fieldset>
				</form>
			</fieldset>
		</section>
		<footer>&copy;copyright by Flop Happy</footer><div id="nav-under-bg"><!-- --></div>
    	<?php } ?>
</body>
</html>
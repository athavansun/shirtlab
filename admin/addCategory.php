<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCategory.php","add_record");

/*---Basic for Each Page Ends----*/
$catObj = new Category();
$genObj = new GeneralFunctions();
$cid  = $_GET['cid']?$_GET['cid']:0;
$catObj->checkCategoryExist($cid);
if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $catObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND isDefault = '1' order by languageName asc","","");		
	$num = $catObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $catObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
		foreach($langIdArr as $key=>$value){
			$obj->fnAdd('categoryName_'.$value,$_POST['categoryName_'.$value], 'req','Please enter category name');			
		}
				
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['categoryName_'.$value]=$obj->fnGetErr($arr_error['categoryName_'.$value]);
		}	
		

		foreach($langIdArr as $key=>$value) {
			if($catObj->isCategoryNameExist($_POST['categoryName_'.$value],$value,'',$cid)) {
				$arr_error['categoryName_'.$value] ='Category already exist';
				if($arr_error['categoryName_'.$value]) 
					$errorArr = 1;				
			}
		}	
                
/*
		$filename = stripslashes($_FILES['categoryImage']['name']);
		$extension = findexts($filename);
		$extension = strtolower($extension);	 	 
		if(!$catObj->checkExtensions($extension) && $filename) {
			$arr_error[categoryImage] = "Upload Only ".$catObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
			if($arr_error[categoryImage]) 
				$errorArr = 1;	
		}
*/
		
		if($errorArr == 0 && $str_validate){
			$_POST = postwithoutspace($_POST);
			$catObj->addRecord($_POST);
		}
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/custom-form-elements.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageCategory.php?cid=<?=$_GET['cid'] ?>';
}
</script>

</head>
<body>
<? include('includes/header.php');
if($cid) $addHadding = 'Sub Category';
else $addHadding  = 'Category';
?>
  <section id="content">
  		<h1>Product <?=$addHadding?></h1>
	<fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		 <fieldset>  
        	<input type="hidden" name="cid" value="<?=$cid?>">
            <label>Add <?=$addHadding ?></label>
			<?=$_SESSION['SESS_MSG']?>
			
            <section>
                  <label><?=$addHadding?> Name </label>
                  <div>
				  <?=$genObj->getLanguageTextBoxEngValidation('categoryName','m__Category_Name',$arr_error);?>
				  </div>	
            </section> 
            <!-- <section> 
                   <label for="description">Category Image</label>
                  <div><input type="file" name="categoryImage" class="browse-button" value="" />
					 <div><?=$arr_error['categoryImage']?></div>
                  </div>
            </section> -->
           </fieldset>        
           <fieldset> 
		<section>  
		<label>&nbsp;</label>
		<div style=" width:78%;">
			<input type="submit" name="submit" value="Submit" />
			<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
		</div>
             </section>             
        </fieldset>
        </form>
	</fieldset>
	</section>
<? include_once('includes/footer.php');?>
<? unset($_SESSION['SESS_MSG']); ?>

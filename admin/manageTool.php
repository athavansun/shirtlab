<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/

$toolbj = new Tool();
?>

<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- Light Box Starts -->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');
window.onload = function(){
	Shadowbox.init();
};
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>View<? if($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addTool.php">Add View</a><? } ?></h1>
<fieldset>
<form name="ecartFrm"  method="post" action="pass.php?action=Tool&type=deleteall" >
 <label>View Details</label>
<input type="hidden" name="page" id="page" value="<?=$_GET['page'] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />

<div class="top-filter bg02">
 <div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
	<div id="search-main-div">
		<ul>
			<li>Action:</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission())){  ?>
                    	<option value="enableall">Enable Selected</option>
                    	<option value="disableall">Disable Selected</option>
			<? } ?>
                  	</select>
			</li>
			<li><input type="submit" value="Submit" name="Input"></li>
			</ul>
</div>
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
   <table class="documentation">
					<tr>
                    	<thead>
 								<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
								<th>SN.</th>
								<th><?=orderBy("manageTool.php?searchtxt=$searchtxt","variableName","Variable")?></th>
								<th><?=orderBy("manageTool.php?searchtxt=$searchtxt","toolName","Name")?></th>
								<th>Status</th>
                <th>View</th>
								<th>Edit</th>
								<th>Delete</th>
						 </thead>
					</tr>
	<?=$toolbj->valDetail()?>
</table>
</form>
</fieldset>
</section>
<? include_once('includes/footer.php');?>

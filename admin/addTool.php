<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTool.php","add_record");
/*---Basic for Each Page Ends----*/
$toolObj = new Tool();
$genObj = new GeneralFunctions();
if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$rst = $toolObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND 	isDefault = '1' order by languageName asc","","");

	$num = $toolObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $toolObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}

		$obj->fnAdd('VariableName',$_POST['VariableName'], 'req','Please enter variable.');
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('toolName_'.$value,$_POST['toolName_'.$value], 'req','Please enter value.');
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		$arr_error[VariableName]=$obj->fnGetErr($arr_error['VariableName']);
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['toolName_'.$value]=$obj->fnGetErr($arr_error['toolName_'.$value]);
		}
		
		if($toolObj->isVariableNameExist($_POST['VariableName'],'')) {
			$arr_error['VariableName'] ='<span class="alert-red alert-icon">Variable name already exist.</span>';
			$str_validate = 0;
		}
		
		
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$toolObj->addRecord($_POST);
		}
	}
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<?// =getjsconstant();?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageTool.php?frontOrAdmin=<?=$_REQUEST['frontOrAdmin']?>';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Tool</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <input type="hidden" name="frontOrAdmin" id="frontOrAdmin" value="<?=$_REQUEST['frontOrAdmin']?>" >   
		 <fieldset>  
            <label>Add Designer Tool Variable</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                  <label>Variable </label>
                  <div>
 				  	<input type="text" name="VariableName" id="m__Variable" value="<?=$_POST['VariableName']?>" />
				  <div><?=$arr_error['VariableName']?></div>
				  </div>	
            </section>
            <section> 
                   <label>Value </label>
                  <div>
			          <?=$genObj->getLangTextareaEngValidated('toolName','m__Name',$arr_error);?>
                  </div>
            </section>
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
             </div>
           </section>             
        </fieldset>
        </form>
	</section>
<? include_once('includes/footer.php');?>
<? unset($_SESSION['SESS_MSG']); ?>

<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCurrency.php","add_record");
$currObj = new Currency();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('currencyName',$_POST['currencyName'], 'req', 'Please Enter Currency Name.');            
	$obj->fnAdd("decimalPlace", $_POST["decimalPlace"], "req", "Please enter  Decimal Place.");
    $obj->fnAdd("decimalPlace", $_POST["decimalPlace"], "float", "Decimal Place value is wrong.");
	$obj->fnAdd("currencyCode", $_POST["currencyCode"], "req", "Please enter Currency Code.");
	$obj->fnAdd("currencySign", $_POST["currencySign"], "req", "Please enter Currency Sign.");
	$obj->fnAdd("currencySeprator", $_POST["currencySeprator"], "req", "Please enter Currency Seprator.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;        
	$arr_error[currencyName]=$obj->fnGetErr($arr_error[currencyName]);	
	$arr_error[decimalPlace]=$obj->fnGetErr($arr_error[decimalPlace]);
	$arr_error[currencyCode]=$obj->fnGetErr($arr_error[currencyCode]);
	$arr_error[currencySign]=$obj->fnGetErr($arr_error[currencySign]);
	$arr_error[currencySeprator]=$obj->fnGetErr($arr_error[currencySeprator]);

	if(empty($arr_error[decimalPlace]) && empty($arr_error[currencyCode]) && empty($arr_error[currencySign]) && empty($arr_error[currencySeprator]) && isset($_POST['submit'])){
		$_POST = postwithoutspace($_POST);
		$currObj->addRecord($_POST);		
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageCurrency.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Currency</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
			<fieldset>
				
				<label>Add Currency</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!-- Start : Currency Name ------->
				<section>
					  <label for="FirstName">Currency Name</label>
					  <div>
					  	<input type="text" name="currencyName" id="m__currencyName"  class="wel" value="<?=stripslashes($_POST[currencyName])?>" />
						<?=$arr_error[currencyName]?>
					  </div>					 
				</section> 
				
					<!-- Start : Symbol show in ------->
				<section>
					  <label for="FirstName">Symbol Show In</label>
					  <div>
					  	<input type="radio" name="showIn" value="0" checked="checked" /> Left <input type="radio" name="showIn" value="1" /> Right						
					  </div>					 
				</section> 	
	
								
				<!-- Start : Decimal places ------->
				<section>
					  <label for="FirstName">Decimal Places</label>
					  <div>
					  	<input type="text" name="decimalPlace" id="m__decimalPlace"  class="wel" value="<? echo ($_POST[decimalPlace] != "")?$_POST[decimalPlace]:"0" ?>" />
						<?=$arr_error[decimalPlace]?>
					  </div>					 
				</section> 	
					
				<!-- Start : Make it Default------->
				<section>
					  <label for="FirstName">Make it Default</label>
					  <div>
					  	<input type="checkbox" name="isDefault" value="1" />						
					  </div>					 
				</section> 	
				
				<!-- Start : Currency Seprator ------->
				<section>
					  <label for="FirstName">Currency Seprator</label>
					  <div>
					  	 <input type="text" name="currencySeprator" id="m__currencySeprator"  class="wel" value="<?=stripslashes($_POST[currencySeprator])?>" />
						<?=$arr_error[currencySeprator]?>
					  </div>					 
				</section> 		
				
				<!-- Start : Currency Code------->
				<section>
					  <label for="FirstName">Currency Code</label>
					  <div>
					  	<input type="text" name="currencyCode" id="m__currencyCode"  class="wel" value="<?=stripslashes($_POST[currencyCode])?>" />
						<?=$arr_error[currencyCode]?>
					  </div>					 
				</section>		
						
				<!-- Start : Currency Sign------->
				<section>
					  <label for="FirstName">Currency Sign</label>
					  <div>
					  	<input type="text" name="currencySign" id="m__currencySign"  class="wel" value="<?=stripslashes($_POST[currencySign])?>" />
						<?=$arr_error[currencySign]?>
					  </div>					 
				</section>		
						
					
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />					
                                        <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>       
				</div>
             </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageLanguage.php","add_record");
/*---Basic for Each Page Ends----*/
$langObj = new Language();
$genObj = new GeneralFunctions();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {       
	$obj->fnAdd('languageName',$_POST['languageName'], 'req', 'Please Enter Language Name.');
	$obj->fnAdd('languageCode', $_POST['languageCode'], 'req', "Please enter  Language Code.");
    $obj->fnAdd('languageFlag', $_FILES['languageFlag']['name'], 'req', "Please Upload Language Flag.");
        
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
        
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[languageCode]=$obj->fnGetErr($arr_error[languageCode]);
    $arr_error[languageFlag]=$obj->fnGetErr($arr_error[languageFlag]);
	
         
	if($_POST['languageName']!="" && $langObj->islanguageNameExit($_POST['languageName'])){ 
            $arr_error[languageName] = '<span class="alert-red alert-icon">Language Name Already Exists.</span>';
            $str_validate=0;
        }
	  
	if($_POST['languageCode']!="" && $langObj->islanguageCodeExit($_POST['languageCode'])){ 
            $arr_error[languageCode] = '<span class="alert-red alert-icon">Language Code Already Exists.</span>';
            $str_validate=0;
        }
	
        
        if($_FILES['languageFlag']['name']){
           
            $filename = stripslashes($_FILES['languageFlag']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);             
            if(!$langObj->checkExtensions($extension)){  
                $arr_error[languageFlag] = '<span class="alert-red alert-icon">Upload Only '.$langObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") .' image extension.</span>';
                $str_validate=0;
            }
        }
	
	
	if($str_validate){
            $_POST = postwithoutspace($_POST);
            $langObj->addNewLanguage($_POST, $_FILES);
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageLanguage.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
    <section id="content">
            <h1>Language</h1>
            <fieldset>
                <form name="langForm" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
                    <fieldset>
                    <label>Add Language</label>
                    <?=$_SESSION['SESS_MSG']?>

                    <section>
                    <label>Language Name<span class="spancolor">*</span></label>
                    <div><input type="text" name="languageName" id="m__Language_Name"  value="<?=$_POST[languageName] ?>" />
                    <?=$arr_error['languageName'] ?>				  
                    </div>
                    </section>
                    
                    <section>
                    <label>Language Code<span class="spancolor">*</span></label>
                    <div><input type="text" name="languageCode" id="m__Language_Code"  value="" />
                    <?=$arr_error['languageCode'] ?>	
                    </div>
                    </section>
                    
                    <section>
                    <label>Language Flag<span class="spancolor">*</span></label>
                    <div><input type="file" class="browse-button" name="languageFlag" id="m__Language_Flag" value="<?=$_POST[languageFlag] ?>" />
                     <?=$arr_error['languageFlag'] ?>	
                    </div>
                    </section>            
                    </fieldset>   
                    
                    <!-- Submit Button------------>
                    <fieldset> 
                    <section>  
                    <label>&nbsp;</label>
                    <div style=" width:78%;">
                    <input type="submit" name="submit"   value="Submit" />
                    <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
                    </div>
                    </section>
                    </fieldset>
                    
                </form>
            </fieldset>
    </section>
<? include_once('includes/footer.php');?>	
<? unset($_SESSION['SESS_MSG']); ?>

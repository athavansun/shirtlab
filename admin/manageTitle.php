<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/
$titleObj = new Title();
$genFunObj = new GeneralFunctions();
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- Light Box Starts -->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->
<!--          *****************            DRAG AND DROP  - START        ***************   -->
<!--<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>-->
<!--          *****************            DRAG AND DROP  - END          ***************   -->
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" >
$(function(){
	$('.dragbox')
	.each(function(){
		$(this).hover(function(){
			$(this).find('ul').addClass('collapse');
		}, function(){
			$(this).find('ul').removeClass('collapse');
		})
		.find('h2').hover(function(){
			$(this).find('.configure').css('visibility', 'visible');
		}, function(){
			$(this).find('.configure').css('visibility', 'hidden');
		})
		.click(function(){
			$(this).siblings('.dragbox-content').toggle();
		})
		.end()
		.find('.configure').css('visibility', 'hidden');
	});
	$('.column').sortable({
		connectWith: '.column',
		handle: 'ul',
		cursor: 'move',
		placeholder: 'placeholder',
		forcePlaceholderSize: true,
		opacity: 0.4,
		stop: function(event, ui){
			$(ui.item).find('ul').click();
			var sortorder='';
			$('.column').each(function(){
				var itemorder=$(this).sortable('toArray');
				var columnId=$(this).attr('id');
				sortorder+=columnId+'='+itemorder.toString()+'&';
			});
			current_order = document.getElementById('currentOrder').value;
			Page = document.getElementById('page').value;
			Limit = document.getElementById('Pagelimit').value;
	        sortorder = sortorder+'&Current Order='+current_order+'&page='+Page+'&limit='+Limit;
		   	//alert(sortorder);
		    sortOrderProduct(sortorder,'view');
			/*Pass sortorder variable to server using ajax to save state*/
		}
	})
	.disableSelection();
});
</script>
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>Manage Title <? if($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addTitle.php">Add New Title</a><? } ?></h1>
<fieldset>
<form name="ecartFrm"  method="post" action="pass.php?action=title&type=deleteall">
 <label>Title</label>
<input type="hidden" name="page" id="page" value="<?=$_GET['page'] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />

<div class="top-filter bg02">
 <div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
	<div id="search-main-div">
		<ul>
			<li>Action :</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission())){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>	
						<? } ?>
                  	</select>
			</li>
			<li><input type="submit" value="Submit" name="Input"></li>
			<li><input name="searchtext" type="text"  style="margin-left:0px;" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:SEARCHTEXT?>" onClick="clickclear(this, '<?=SEARCHTEXT?>')" onBlur="clickrecall(this,'<?=SEARCHTEXT?>')"/></li>
			<li><input name="GO" type="submit" value="Go" /></li>
			<li class="showall"><a href="<?=$_SERVER['PHP_SELF']?>"><b>Reset</b></a></li>
			</ul>
</div>	
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
   <table class="documentation">
		<tr>
      <thead>
 			<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></th>
			<th>SN</th>       
			<th><?=orderBy("manageTitle.php?searchtxt=$searchtxt","titleName",'Title')?></th>         
			<th>Status</th>
			<th>Edit</th>	
			<th>Delete</th>
			</thead>
		</tr>	 
	<?=$titleObj->valDetail();?>
</table>
</form>
</fieldset>
</section>
</body>
</html>
<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStaticPage.php","add_record");
$staticpageObj = new StaticPage();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('pageName',$_POST['pageName'], 'req', 'Please Enter Language Name.');
	$obj->fnAdd("pageUrl", $_POST["pageUrl"], "req", "Please enter  Language Code.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[pageName]=$obj->fnGetErr($arr_error[pageName]);
	$arr_error[pageUrl]=$obj->fnGetErr($arr_error[pageUrl]);

	if($staticpageObj->isPageNameExit($_POST['pageName']))
		 {
		   $arr_error[pageName] = "Page Name already exist. ";
		 }
	if($staticpageObj->isPageUrlExit($_POST['pageUrl']))
		 {
		   $arr_error[pageUrl] = "Page Url altredy exist. ";
		 }


	if(empty($arr_error[pageName]) && empty($arr_error[pageUrl]) && isset($_POST['submit'])){
	$_POST = postwithoutspace($_POST);
	$staticpageObj->addStaticPageType($_POST);
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageStaticPage.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Static Page</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		 <fieldset>  
            <label>Add Static Page Type</label>
			<?=$_SESSION['SESS_MSG']?>
            <section>
                  <label>Page Name</label>
                  <div>
                       	<input type="text" name="pageName" id="m__Page_Name" value="<?=stripslashes($_POST['pageName'])?>" />				 
					    <div><?=$arr_error['pageName']?></div>
				  </div>	
            </section>
            <section>
                  <label>Page URL</label>
                  <div>
                    <input type="text" name="pageUrl" id="m__Page_Url" value="<?=stripslashes($_POST['pageUrl'])?>" />
					<div><?=$arr_error['pageUrl']?></div>
				  </div>	
            </section>
			
		</fieldset>
             <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
              </div>
                </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
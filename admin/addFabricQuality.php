<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabricQuality.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$fabricObj = new Fabric();
if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();

	$rst = $fabricObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' and isDefault ='1' order by languageName asc","","");		
	$num = $fabricObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $fabricObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
			
		$obj->fnAdd('fabricType',$_POST['fabricType'], 'req','Please Select Fabric Type');
		
		$obj->fnAdd('fQuality',$_POST['fQuality'], 'req','Please Enter Fabric Quality');
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		$arr_error['fabricType']=$obj->fnGetErr($arr_error['fabricType']);
		$arr_error['fQuality']=$obj->fnGetErr($arr_error['fQuality']);
		//print_r($arr_error);
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$fabricObj->addFabricQuality($_POST);
		}
	}
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!--  <script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script> 		 -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageFabricQuality.php';
}
</script>
<!-- New Drop Down menu -->

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Fabric</h1><fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">		
		 <fieldset>  
            <label>Fabric</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                <label>Fabric Type</label>
				<div>
					<select name="fabricType">
						<option value="">Select Fabric Type</option>
						<?= $fabricObj->getFabricType();?>
					</select>	
					<?=$arr_error['fabricType']?>	
				</div>
            </section>
            <section>
                <label>Fabric Quality</label>          
			    <div>					
					<input type="text" name="fQuality" id="fQuality"  value="" />
					<?=$arr_error['fQuality']?>
				</div>				
            </section>           
               
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form> </fieldset>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

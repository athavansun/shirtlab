<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFaq.php","edit_record");
/*---Basic for Each Page Starts----*/

$faqObj = new Faq();
$genObj = new GeneralFunctions();

require_once('validation_class.php');
$obj = new validationclass();
$errorArr = 0;
if(isset($_POST['submit'])) {
	$rst = $faqObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $faqObj->getTotalRow($rst);	
	if($num > 0){
		$langIdArr = array();		
		while($line = $faqObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('question_'.$value,$_POST['question_'.$value], 'req', 'Please Enter Question.');			
		}
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('answer_'.$value,$_POST['answer_'.$value], 'req', 'Please Enter Answers.');			
		}
		
		$arr_error = $obj->fnValidate();
	    $str_validate = (count($arr_error)) ? 0 : 1;
		$arr_error[price]=$obj->fnGetErr($arr_error[price]);
		foreach($langIdArr as $key=>$value) {
			$arr_error['question_'.$value]=$obj->fnGetErr($arr_error['question_'.$value]);
			if($arr_error['question_'.$value]) 
				$errorArr = 1;
		}
		foreach($langIdArr as $key=>$value) {
			$arr_error['answer_'.$value]=$obj->fnGetErr($arr_error['answer_'.$value]);
			if($arr_error['answer_'.$value]) 
				$errorArr = 1;
		}
		
		
		/*foreach($langIdArr as $key=>$value) {
			if($faqObj->isViewExist($_POST['viewName_'.$value],$value,$_POST[id]))
			{ 
				$arr_error['viewName_'.$value] = "Name already exist. ";
				if($arr_error['viewName_'.$value]) 
					$errorArr = 1;				
			}
		}	*/
		
	}

	if($errorArr == 0  && isset($_POST['submit'])){
	    
		$faqObj->editRecord($_POST);		
	}
}

$result = $faqObj->getResult(base64_decode($_GET['id']));

?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageFaq.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
  		<h1>Size</h1>
  		<fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">	
  		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		 <fieldset>  
            <label>Edit Faq</label>
			<?=$_SESSION['SESS_MSG']?>
            <section>
                  <label>Question</label>
                  <div>
				<?=$genObj->getLanguageEditTextarea('question','m__Question',TBL_FAQDESC,base64_decode($_GET['id']),"faqId",$arr_error);?>
			 </div>	
            </section>
            <section>
                  <label>Question</label>
                  <div>
				<?=$genObj->getLanguageEditTextarea('answer','m__Answre',TBL_FAQDESC,base64_decode($_GET['id']),"faqId",$arr_error);?>
			 </div>	
            </section>
            </fieldset>
            <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
              </div>
                </section>
        </fieldset>
	</form>
	</fieldset>
</section>


<?php /*

<div id="nav-under-bg"><!-- --></div>
  <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
  <input type="hidden" name="page" value="<?=$_GET['page']?>">	
		<div class="main-body-div-new">
          <div class="main-body-div-header">Edit Faq</div>
		  <!-- left position -->
            <div class="main-body-div4" id="mainDiv">
              <div class="add-main-body-left-new">
                <ul>
                  <li class="add-main-body-left-new-text" style="clear:both; width:500px;" ><span class="small_error_message">
                    <?=$_SESSION['SESS_MSG']?>
                  </span></li>
                 <li class="lable">Question<span class="spancolor">*</span></li>
					  <?	$genObj = new GeneralFunctions();
							echo $genObj->getLanguageEditTextarea('question','m__Question',TBL_FAQDESC,base64_decode($_GET['id']),"faqId",$arr_error); //1->type,2->name,3->id
						?>
					 <li class="lable">Answer<span class="spancolor">*</span></li>
					  <?	$genObj = new GeneralFunctions();
							echo $genObj->getLanguageEditTextarea('answer','m__Answre',TBL_FAQDESC,base64_decode($_GET['id']),"faqId",$arr_error); //1->type,2->name,3->id
						?>
                </ul>
              </div>
              <div class="main-body-sub">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                &nbsp;
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack()"/>
              </div>
            </div>
</div>

</form>
 */?>

<? unset($_SESSION['SESS_MSG']); ?>
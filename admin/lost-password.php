<?php
/*---Basic for Each Page Starts----*/
session_id();
ob_start();
session_start();
require_once('config/configure.php');
//require('../includes/messages.php');
require_once('includes/function/autoload.php');
require_once('validation_class.php');
$obj = new validationclass();
$loginObj = new Login();
if(isset($_POST['submit'])) {
 	$obj->fnAdd('email',$_POST['email'], 'req', LANG_INVALID_EMAIL_ID_MSG);
	$obj->fnAdd('email',$_POST['email'], 'email', LANG_INVALID_EMAIL_ID_MSG);
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error['email'] = $obj->fnGetErr($arr_error['email']);
	
	if($loginObj->isExistsAdminEmailId($_POST['email'])== false)
	  $arr_error['email'] = "Email Id does not exist.";
	
	if(empty($arr_error[email]) && isset($_POST['submit'])){
		$loginObj->adminResendPassword($_POST);
	}
}
/*---Basic for Each Page Ends----*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
 	<title>Login <?=SITENAME?></title>
 	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/main/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" class="theme">
 	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
 	<!-- Use Google CDN for jQuery and jQuery UI -->
<!-- 	<script src="js/jquery.min.js"></script> -->
<!-- 	<script src="js/jquery-ui.min.js"></script> -->
 	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
 	<!-- some basic functions -->
 	<!-- all Third Party Plugins -->
<!-- 	<script src="js/plugins.js"></script> -->
 	<!-- Whitelabel Plugins -->
<!-- 	<script src="js/wl_Alert.js"></script> -->
<!-- 	<script src="js/wl_Dialog.js"></script> -->
<!-- 	<script src="js/wl_Form.js"></script> -->
 	<!-- configuration to overwrite settings -->
<!-- 	<script src="js/config.js"></script> -->
 	<!-- the script which handles all the access to plugins etc... -->
<!--  	<script src="js/login.js"></script> -->
<!--<script language="javascript" src="js/validation.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Login to enter into <?=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />-->
</head>
<body  id="login" style="width:500px;">
	<header>
		<div id="logo"><a href="../" target="_blank"><?=SITENAME?></a></div>
		<div id="logo" ><a  href="index.php" style="margin-left: 179px;">Login</a></div>
	</header>
	<section id="content">
		<form name="forgot-password" method="post" action="" onSubmit="return validationResendPassword()">
			<? if(!empty($_SESSION['SESS_MSG']) || !empty($arr_error['email'])) { ?><span class="alert-red alert-icon"><?=$_SESSION['SESS_MSG']?><?=$arr_error['email']?> <? }?></span>
			<fieldset>
				<section><label for="username">Enter Your Email</label>
					<div style="float:left;">
						<input type="text" value="" name="email" id="email"  />
						<div>&nbsp;</div>
						<button class="fr" type="submit"  name="submit" id="submit" >Submit</button>
					</div>
				</section>
			</fieldset>
		</form>
		</section>
		<footer>&copy;Copyright by <?=SITENAME?></footer>
		<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>
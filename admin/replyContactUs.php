<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageContactUs.php");
$contactUsObj = new ContactUs();
$id =  base64_decode($_GET['id']);
$id = $id?$id:0;
$row = $contactUsObj->checkIsPageExists($id);

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$obj->fnAdd("replyText",$_POST["replyText"], "req", "Please enter Reply Message.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[replyText]=$obj->fnGetErr($arr_error[replyText]);		
	
	if(empty($arr_error[replyText]) && isset($_POST['submit'])){
		$_POST = postwithoutspace($_POST);
		$contactUsObj->addContactUsReply($_POST);
	}	
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js,Top Pageoptions ?>
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
	<h1>View Contact Us</h1>
<fieldset>
    <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
<!--        <label>Edit <?=$addHadding ?></label>-->
        <div id="mainDiv"><div><?=$_SESSION['SESS_MSG']?></div>
<!--   		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
   		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		<input type="hidden" name="cid" value="<?=$cid?>">	-->
        <input type="hidden" name="id" value="<?=$id?>" />
        <input type="hidden" name="langId" value="<?=$row['langId']?>" />
        <fieldset>
            <section>
                <label>Usert Type</label>
                <div>
                    <?
						if($row['userId'])
						{
							echo '<h3>Registered user.</h3>';
							$registerDate = $contactUsObj->fetchValue(TBL_USER,'registerDate',"id = '".$row['userId']."'");
							$accNo = '<section>
										<label>Account No</label>
										<div>'.substr(date('ymd\'hisA', strtotime($registerDate)), 0, -1).'</div>
									</section>';
						}
						else
						{
							echo '<h3>Guest/Interested user.</h3>';
							$accNo = '';
						}
					?>
                </div>
            </section>
            <?=$accNo;?>
            <section>
                <label for="language">Language</label>
                <div>
                        <input type="hidden" name="langId" value="<?=$row['langId']?>" />
                    <?
                         echo stripslashes($contactUsObj->fetchValue(TBL_LANGUAGE,"languageName","id='$row[langId]'"));
                    ?>
                    <div id="chk_language"><?=$arr_error['language'];?></div>
                </div>
            </section>
            <section>
                <label for="name">Name</label>
                <div>
                    <?
                         echo stripslashes($row['name']);
                    ?>
                    <div id="chk_name"><?=$arr_error['name'];?></div>
                </div>
            </section>
            <section>
                <label for="email">Email Id</label>
                <div>
                    <?
                         echo stripslashes($row['emailId']);
                    ?>
                    <div id="chk_email"><?=$arr_error['email'];?></div>
                </div>
            </section>
            <section>
                <label for="message">Message</label>
                <div>
                    <?
                         echo nl2br(stripslashes($row['messageText']));
                    ?>
                    <div id="chk_message"><?=$arr_error['message'];?></div>
                </div>
            </section>
            <section>
                <label for="messageDate">Message Date</label>
                <div>
                    <?
                         echo date(DEFAULTDATEFORMAT,strtotime($row['addDate']));
                    ?>
                    <div id="chk_messageDate"><?=$arr_error['messageDate'];?></div>
                </div>
            </section>
             <?php if($row[replyBy] >0){ ?>
            <section>
                <label for="replyBy">Reply By</label>
                <div>
                    <?
                         echo $contactUsObj->fetchValue(TBL_ADMINLOGIN,"username","id='$row[replyBy]'");
                    ?>
                    <div id="chk_email"><?=$arr_error['replyBy'];?></div>
                </div>
            </section>
            <section>
                <label for="replymessage">Reply Message</label>
                <div>
                    <?
                         echo nl2br(stripslashes($row['replyText']));
                    ?>
                    <div id="replymessage"><?=$arr_error['replymessage'];?></div>
                </div>
            </section>
            <section>
                <label for="replyDate">Reply Date</label>
                <div>
                    <?
                         echo date(DEFAULTDATEFORMAT,strtotime($row['replyDate']));
                    ?>
                    <div id="chk_replyDate"><?=$arr_error['replyDate'];?></div>
                </div>
            </section>
            <?php }else{ ?>
            <section>
                <label for="reply">Reply</label>
                <div>
                    <textarea name="replyText" id="m__Reply_Message" class="wel-textarea" rows="5" cols="40"><?=stripslashes($_POST['replyText'])?></textarea>
                    <div id="chk_reply"><?=$arr_error['reply'];?></div>
                </div>
            </section>
            <?php  }  ?>
            </fieldset>
            
            <fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <?php if($row[replyBy] <=0){ ?>
                 <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
		<?php } ?>
                <input type="button" name="back" id="back" value="Back"
class="main-body-sub-submit" style="cursor:pointer;" 
onclick="javascript:;hrefBack1();"/>
              </div>
          </section>
        </fieldset>
        </div>
	</form>
</fieldset>
</section>
</form>
<div id="divTemp" style="display:none;"></div>
<? unset($_SESSION['SESS_MSG']); ?>
<script language="javascript"> 
	function hrefBack1(){
		window.location='manageContactUs.php';
	}
</script>
<?php include_once('includes/footer.php');?>

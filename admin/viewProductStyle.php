<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$proObj = new Products();
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
</head>
<body>
         <section id="content-detail">
     <form>
 	<fieldset>
    <label>View Product Style Detail</label>
        <section>
       	  <label for="categoryName">Style Code:</label>
             <div>
				<?=$proObj->fetchValue(TBL_PRODUCT_STYLE,"styleCode","id = '".base64_decode($_GET['sId'])."'");?>
            </div>
        </section>
        <section>
       	  <label for="categoryName">Style Basic Image</label>
             <div>
				<img width="60" src="<?=SITE_URL.__PRODUCTBASICTHUMB__.$proObj->fetchValue(TBL_PRODUCT_STYLE,"styleImage","id = '".base64_decode($_GET['sId'])."'");?>">
            </div>
        </section>
        <section>
       	  <label for="categoryName">Style Part Details</label>
             <div>
				<?=$proObj->viewProductPart(base64_decode($_GET['pId']),base64_decode($_GET['sId']));?>
            </div>
        </section>
     </fieldset>
     </form>
    </section>
<? include_once('includes/footer.php');?>

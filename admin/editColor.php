<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageColor.php","edit_record");
/*---Basic for Each Page Ends----*/

$colorObj = new Color();
$genObj = new GeneralFunctions();

$cid  = $_GET['id']?base64_decode($_GET['id']):0;
//$catObj->checkCategoryExist($cid);

if(isset($_POST['save'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	
	$editid = $_POST[id];
	/*$rst = $colorObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $colorObj->getTotalRow($rst);	
	if($num > 0){
		$langIdArr = array();		
		
		while($line = $colorObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}*/
	$langIdArr = $genObj->langIdArr();
	if(count($langIdArr)){
		$obj->fnAdd('colorCode',$_POST['colorCode'], 'req', 'Please enter Color Code.');	
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('colorName_'.$value,$_POST['colorName_'.$value], 'req', 'Please enter Color Name.');			
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['colorName_'.$value]=$obj->fnGetErr($arr_error['colorName_'.$value]);
		}
		
		if($colorObj->isColorExist($_POST['colorCode'],$_POST[id])) { 
			$arr_error['colorCode'] = '<span class="alert-red alert-icon">Color Code already exist.</span>';
			if($arr_error['colorCode']) 
				$str_validate=0;	
		}else{
			$arr_error['colorCode']=$obj->fnGetErr($arr_error['colorCode']);
		}				
	}
	
	if($str_validate){
		$colorObj->editRecord($_POST);		
	}
}

$result = $colorObj->getResult($cid);
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<!--				Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)		-->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageColor.php';
}
</script>
<script src="js/file/jquery.filestyle.js"></script>
</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
	<h1>Color</h1>
	<fieldset>
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>Edit Color</label>
		<input type="hidden" name="id" value="<?=$cid?>">		
		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
   		<div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset>  
           <section>
			 <!--<label>Color Code<span class="spancolor">*</span></label>
                  <div><input type="text" name="colorCode" id="m__color__code" value="<?='#'.$result->colorCode ?>" maxlength="9" />&nbsp;&nbsp;<img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmUser.colorCode)" border="0" style="cursor:pointer;">
				  <?=$arr_error['colorCode'] ?>				  
				  </div>
            </section>-->
		  <section>
			 <label>Color Code<span class="spancolor">*</span></label>
                  <div><input type="text" name="colorCode" id="m__color__code" style="width:100px;" value="<?=$result->colorCode ?>" maxlength="9" />
				  <?=$arr_error['colorCode'] ?>				  
				  </div>
            </section>
			<section>
                  <label>Color Image <span class="spancolor">*</span> </label>
                  <div>
				  <img src="images/noimageavailable.jpg" />
				  <input type="file" name="colorImage" class="file browse-button" />
				  <?=$arr_error['colorImage']?>
				  </div>                  
            </section> 
		
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="save" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
          </section>
        </fieldset>
</div>
	</form>
	</fieldset>
</section>
   <div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>

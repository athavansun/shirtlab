<?
/*---Basic for Each Page Starts----*/

session_start();

require_once('config/configure.php');

require_once('includes/function/autoload.php');

$loginObj = new Login();

$loginObj->checkSession();

$pageName = getPageName();

$menuObj = new Menu();

$menuObj->checkPermission("manageMailType.php");

/*---Basic for Each Page Ends----*/
$id = $_GET[id]?$_GET[id]:0;
$mailSettingObj = new MailSetting();
if ($action == 'mailtype') {

	if($type == 'deleteall'){

		header("Location:manageMailType.php?searchtxt=".$_POST['searchtext']);

	}

}

?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>

<!--          *****************            DRAG AND DROP  - START        ***************   -->
<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>
<!--          *****************            DRAG AND DROP  - END          ***************   -->
</head>
<body>
<? include('includes/header.php'); ?>

	<section id="content">
	   <h1>Email Template Setting
	   <? if($mailSettingObj->getRemainingLanguage($id,'','1')){ ?><a class="btn small fr"  href="addMailTemplate.php?mailId=<?=base64_encode($id)?>">Add Mail Template</a>
	   <? }?>
	   </h1>

	<fieldset>
	<form name="ecartFrm" method="post" action="pass.php?action=mailtypeDescription&type=deleteall"   >
	<label><a href="manageMailType.php">Email Type</a>&nbsp;>> &nbsp;<?=stripslashes($mailSettingObj->fetchValue(TBL_MAILTYPE,"mailType","id='$id'"))?></label>
	
	<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
	<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />
	
	<div class="top-filter bg02"> 	
		<div id="search-main-div">
			<ul>		
				<li><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:Search?>" onClick="clickclear(this, 'Search')" onBlur="clickrecall(this,'Search')"/></li>
				<li><input name="GO" type="submit" value="Go"  class=""/></li>
				<li class="showall"><a href="manageMailDescription.php?id=<?=$_GET['id']?>">Reset</a></li>
			</ul>
		</div>	
	</div>
	<? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
	   <table class="documentation">
						<tr>
							<thead>
								<th>SL.No</th>
								<th>ID</th>
								<th>Language</th>
								<th>
								<?=orderBy("manageMailDescription.php?id=$id&searchtxt=$searchtxt","mailSubject",Subject)?>
								</th>								
								<th>Edit</th>
							 </thead>
						</tr>	 
		<?
			echo $mailSettingObj->allMailTypeDetailsInformation($id);
		?>
	</table>
	<input type="hidden" name="id" value="<?=$id?>" />
	</form>
	</fieldset>
	</section>

</body>
</html>
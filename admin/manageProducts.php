<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
/*---Basic for Each Page Starts----*/

$prodObj = new Products();
$generalFunctionsObj = new GeneralFunctions();
//$isDPPrice = $generalFunctionsObj->haveRow(TBL_DECORATION_PROCESS_PRICING_RELATION,"id");
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<script language='javascript' type='text/javascript' src='js/perpage.js'></script>

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>
<? include('includes/header.php'); ?>
	  <section id="content">
	 <h1>Product Details<? if($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addProduct.php">Add New Product</a><? } ?></h1>
	<fieldset>
	<form name="ecartFrm" method="post"
action="pass.php?action=product&type=deleteall" >
	<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
	<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />
	<fieldset>
		 <div id="search-main-div" class="top-filter bg02">
			<div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
	 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
	 
			<ul>
				<li class="action">Action:</li>
				<li>
					<select name="action">
							<option value="">Select Action</option>
							
							<? 
							if(($menuObj->checkDeletePermission())){  ?>
							<option value="deleteselected">Delete Selected</option>
							<? }?>
							<? if(($menuObj->checkEditPermission()) ){  ?>
							<option value="enableall">Enable Selected</option>	
							<option value="disableall">Disable Selected</option>	
							<? }?>										
					 </select>
				</li>
				<li><input name="Input" type="submit" value="Submit"  class=""/></li>
				<li><input name="searchtext" type="text"
class="adminsearch" value="<?=$searchtxt=
$_GET['searchtxt']?$_GET['searchtxt']:'searchtext'?>" onClick="clickclear(this,
'searchtext')" onBlur="clickrecall(this,'Searchtext')"/></li>
				<li><input name="GO" type="submit" value="Go"  class=""/></li>
				<li class="showall"><a href="<?=basename($_SERVER['PHP_SELF'])?>">Reset</a></li>
			</ul>
	</div>
	<? //$p = SEARCHTEXT?>
	<div><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	
	  <table class="documentation">
		<tr>
			<thead>
				<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></th>
				<th>SN.</th>
				<th><?=orderBy("manageProducts.php?page=$_GET[page]","catId","Category")?></th>
				<th><?=orderBy("manageProducts.php?page=$_GET[page]","productName","Product Name")?></th>
				<th>Images</th>
				<th style="width:75px;">Status</th>
				<th>View</th>
				<th>Edit</th>
				<th>Delete</th>
			</thead>
		</tr>	
		<?
		echo $prodObj->valDetail();
		?>
		</table>
	 </fieldset>
	</form>
	</fieldset>
	</section>
<? //} ?>
<?php include_once('includes/footer.php'); ?>

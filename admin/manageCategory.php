<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();

/*---Basic for Each Page Starts----*/

$catObj = new Category();
$cid  = $_GET['cid']?$_GET['cid']:"";
$catObj->checkCategoryExist($cid);
if ($action == 'category') {
	if($type == 'deleteall'){
		$categoryObj->deleteAllValues($_POST);
	}
}
if($cid) $addHadding = "Sub Category";
else $addHadding  = "Category";
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>

<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->
<!--				Ligh Box Ends 	-->
<!--          *****************            DRAG AND DROP  - START        ***************   -->
<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>
<!--          *****************            DRAG AND DROP  - END          ***************   -->

</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
   <h1>Product <?=$addHadding ?><? if($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addCategory.php<?=$cid?"?cid=$cid":""?>">Add New <?=$addHadding ?></a><? } ?></h1>
	<fieldset>
		<form name="ecartFrm" id="f"  method="post" action="pass.php?action=category&type=deleteall"   >
		<label><?=$addHadding?> Details <span style="font-size:11px;"><?=$catObj->getBreadCrumb($cid)?></span></label>
		<input type="hidden" name="cid" id="cid" value="<?=$cid?>">
		<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
		<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />

<div class="top-filter bg02">
 <div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
<!--  	<div><?=$catObj->getBreadCrumb($cid)?></div> -->
	<div id="search-main-div">
		<ul>
			<li>Action:</li>
			<li>
				<select name="action">
					<option value="">Select Action</option>
					<? if(($menuObj->checkDeletePermission())){  ?>
					<option value="deleteselected">Delete Selected</option>
					<? } ?>
					<? if(($menuObj->checkEditPermission())){  ?>
					<option value="enableall">Enable Selected</option>	
					<option value="disableall">Disable Selected</option>	
					<? } ?>
				</select>
			</li>
			<li><input name="Input" type="submit" value="Submit"  class=""/></li>
			<!--<li><div><?=$catObj->getBreadCrumb($cid)?></div></li>-->
		</ul>
	</div>	
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
		
   <table class="documentation">
	<tr>
		<thead>
			<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
			<th>SN</th>
			<th>ID</th>
			<th><?=orderBy("manageCategory.php?cid=$cid","categoryName",'Category Name')?></th>
			<th>Sequence&nbsp;<input type="submit" name="sort" value="Sort"></th>
			<th>Status</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
	</tr>
	<?
		echo $catObj->valDetail($cid);
	?>
</table>
</form>
</fieldset>
</section>
<? include_once('includes/footer.php');?>

<?
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
//$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$proObj = new Products();
if(isset($_POST['savePart']))
{
	require_once('validation_class.php');
	$obj = new validationclass();
	//$errorArr = 0;
	
	$obj->fnAdd('styleCode',$_POST['styleCode'], 'req', 'Please Enter style code.');
	//===============================================================
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	//===============================================================
	
	$arr_error['styleCode']=$obj->fnGetErr($arr_error['styleCode']);
	$permitableExt = $proObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'");
	if($_FILES['styleImage']['name'] != '') {
					
		$fileext = findexts($_FILES['styleImage']['name']);
		if($fileext) {
			if($proObj->checkExtensions($fileext) == false ) {
				$arr_error['styleImage'] = '<span class="alert-red alert-icon">Invalid Image Extention,Please Upload Image With Extention '.$permitableExt.'.</span>';
				$str_validate=0;
			}
		}
		else {
			$arr_error['styleImage'] = '<span class="alert-red alert-icon">Invalid Image Extention, Please Upload Image With Extention '.$permitableExt.'.</span>';
			$str_validate=0;
		}	
	}
        
        if($_POST['percent'] == '' ) {
            $arr_error['percent'] = '<span class="alert-red alert-icon">Please put zero incase of null.</span>';
            $str_validate=0;
        }
        
	if($str_validate)
	{
//		echo '<pre>';
//		print_r($_POST);
//		print_r($_FILES);
//		echo '</pre>';exit;
		$proObj->editStylePart($_POST, $_FILES);
	}
}

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
 <section id="content-detail">
 <form id="formStylePart" method="post" enctype="multipart/form-data" action="">
 	<fieldset>
    <label>Edit Product Style</label>
        <section>
       	  <label>Style Code:</label>
             <div>
				<? $styleCode = $proObj->fetchValue(TBL_PRODUCT_STYLE,"styleCode","id = '".base64_decode($_GET['sId'])."'");?>
				<input type="text" name="styleCode" value="<?=$styleCode;?>" />
				<?=$arr_error['styleCode'];?>
            </div>
        </section>
        <section>
       	  <label>Is basic Style?</label>
             <div>
                <?php
                    if($proObj->fetchValue(TBL_PRODUCT_STYLE,"isBasic","id = '".base64_decode($_GET['sId'])."'") == '1')
                            $sel = 'checked="checked"';
                    else
                            $sel1 = 'checked="checked"';
                ?>
            Yes <input type="radio" <?=$sel;?> name="isBasic" value="1" />
            No <input type="radio" <?=$sel1;?> name="isBasic" value="0" />
				
            </div>
        </section>
        <section>
            <label>Value Point</label>
            <div>
                <?php
                    $chk ='';$chk1 = '';
                    if($proObj->fetchValue(TBL_PRODUCT_STYLE,"sign","id = '".base64_decode($_GET['sId'])."'") == '0')
                            $chk1 = 'selected="selected"';
                    else
                            $chk = 'selected="selected"';
                ?>
                <select name="sign">
                    <option <?=$chk?> value="2"> + </option>
                    <option <?=$chk1?> value="0"> - </option>
                </select>
                &nbsp;&nbsp;% <input type="text" name="percent" value="<?=$proObj->fetchValue(TBL_PRODUCT_STYLE,"percent","id = '".base64_decode($_GET['sId'])."'")?>" style="width: 80px;" />
                <?=$arr_error['percent']?>
            </div>
        </section>
        <section>
       	  <label>Style Basic Image</label>
             <div>
				 <?php
					$styleImg = $proObj->fetchValue(TBL_PRODUCT_STYLE,"styleImage","id = '".base64_decode($_GET['sId'])."'");
					$image = checkImgAvailable(__PRODUCTBASICTHUMB__.$styleImg);
				 ?>
				<img width="60" src="<?=$image;?>" /> <br />
				<input type="hidden" name="oldStyleImage" value="<?=$styleImg;?>" />
				<input type="file" name="styleImage" />
				<?=$arr_error['styleImage'];?>
				<pre>Please upload minimum size of image should be 100(W)X100(H)</pre>
            </div>
            
        </section>
        <label>Edit Style Part</label>
        <input type="hidden" id="hitCount" value="100" />
        <?=$proObj->editProductStylePart(base64_decode($_GET['pId']),base64_decode($_GET['sId']));?>
        <section>
			<div>
				<input type="hidden" name="sId" value="<?=base64_decode($_GET['sId']);?>" />
				<input type="hidden" name="pId" value="<?=base64_decode($_GET['pId']);?>" />
				<input type="submit" name="savePart" style="cursor:pointer;" value="Submit" />				
			</div>
		</section>
     </fieldset>     
     </form>
    </section>
    <script type="text/javascript">
		jQuery(document).ready( function() {
			// binds form submission and fields to the validation engine
			jQuery("#formStylePart").validationEngine();
		});
		function addMorePart(viewName,viewId)
		{
			//alert(viewName+"--"+viewId);
			var hCount = parseInt($("#hitCount").val());
			hCount = hCount + 1;
			$("#hitCount").val(hCount);
			//alert(hCount);
			var item = '<tr id="partTr'+viewId+hCount+'">\<td style="text-align:left;">\
					<input type="text" name="sequence'+viewId+'[]" value="0" class="text-input" />\
				</td>\
				<td style="text-align:left;">\
					<select name="visible'+viewId+'[]"><option value="1">Yes</option><option value="0">No</option></select>\
				</td>\<td style="text-align:left;">\
					<td style="text-align:left;">\
					<select name="colorable'+viewId+'[]"><option value="1">Yes</option><option value="0">No</option></select>\
					</td>\
					<td style="text-align:left;">\
					<select name="partTitle'+viewId+'[]"><?=$proObj->getProductPartList();?></select>\
					</td>\
					<td>\
					<input type="file" class="validate[required] text-input" data-prompt-position="topLeft" name="'+viewName+viewId+'[]" />\
					</td>\
					<td><input type="button" onclick="return removePart(\'partTr'+viewId+hCount+'\');" value="Remove Part" /></td>\
				</tr>';
			$("#"+viewName).append(item);
		}
		function removePart(element)
		{
			$("#"+element).remove();
		}
    </script>
    
<? include_once('includes/footer.php');?>

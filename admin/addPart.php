<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTitle.php","add_record");
$partObj = new Part();
$generalObj = new GeneralFunctions();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $partObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' and isDefault ='1' order by languageName asc","","");		
	$num = $partObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $partObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {			
			$obj->fnAdd('partTitle_'.$value, $_POST['partTitle_'.$value], 'req','Please Part Enter Title');
		}
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['partTitle_'.$value]=$obj->fnGetErr($arr_error['partTitle_'.$value]);
		}
				
		foreach($langIdArr as $key=>$value) {			
			if($partObj->checkTitleExists($_POST['partTitle_'.$value],$value,'')) {
				$arr_error['partTitle_'.$value] ='<span class="alert-red alert-icon">Title already exist</span>';
				if($arr_error['partTitle_'.$value]) 
				$str_validate = 0;							
			}
		}
		
		if($str_validate){
			$_POST = postwithoutspace($_POST);			
			$partObj->addRecord($_POST);
		}
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/country.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='managePart.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Part Title</h1>
        <form name="frmUser" id="frmUser" method="post" action="">
			<fieldset>  
            <label>Add Part</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                  <label>Part Title</label>
                  <div>
			<?=$generalObj->getLanguageTextBoxEngValidation('partTitle','m__partTitle',$arr_error);?>
			</div>
            </section>
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

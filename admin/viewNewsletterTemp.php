<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageNewsletterTemp.php");
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();
$id = base64_decode($_GET['id']);
$row = $newsletterObj->newsLetterTempToEdit($id);
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>View Newsletter Template Details</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>
		  
			<!-- Start : Template Name------------>		  
       	 	<section>
       	 	 	<label for="TemplateName">Template Name:</label>
            	 <div>
				 <?=stripslashes($row['templateName'])?>
				 </div>
        	</section>
			
				<!-- Start : Template Content------------>		
			<section>
       	 	 	<label for="Default">Default:</label>
             	<div><?=$row[isDefault]?"Yes":"No"?></div>
       		</section>
		
			<!-- Start : Template Content------------>		
			<section>
       	 	 	<label for="TemplateContent">Template Content:</label>
             	<div><?=$row[templateContaint]?></div>
       		</section>
			
			<!-- Start : Template Status------------>		
			<section>
       	  		<label for="Status">Status </label>
             	<div><?=$row[status]?Active:Inactive?></div>
			</section>
		
        
     </fieldset>       
     </form>  
    </section>        
</body>
</html>
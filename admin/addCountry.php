<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCountry.php","add_record");
/*---Basic for Each Page Ends----*/
$countryObj = new Country();
$genObj = new GeneralFunctions();
if(isset($_POST['submit'])) {
	require_once('validation_class.php');	
	$obj = new validationclass();
		
		$obj->fnAdd('countryName',$_POST['countryName'], 'req','Please Enter Country Name');
		//~ $obj->fnAdd('countryCode',$_POST['countryCode'], 'req', 'Please Enter Country Code (3).');
		$obj->fnAdd('countryCode2',$_POST['countryCode2'], 'req', 'Please Enter Country Code.');
		
		
		// CHECK ERROR ARRAY INDEXES : $str_vlaidate=0 if error exists=========================
		$arr_error = $obj->fnValidate();
		
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		// GET ERROR ARRAY WITH ERROR MESSAGES=================================================
		
		$arr_error[countryName]=$obj->fnGetErr($arr_error[countryName]);
		//~ $arr_error[countryCode]=$obj->fnGetErr($arr_error[countryCode]);
		$arr_error[countryCode2]=$obj->fnGetErr($arr_error[countryCode2]);
		
		$value= $_SESSION['DEFAULTLANGUAGEID'];//$value=__DEFAULT_ACTIVE_LANG_ID__;
		if($countryObj->isCountryNameExist($_POST['countryName'],$value,'')){
		
			$arr_error[countryName] = '<span class="alert-red alert-icon">Country Name Already Exists</span>';
			$str_validate=0;				
		}
		
		
		///////// ADD RECORD IF NO ERROR: $str_validate==1==========================================
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$countryObj->addRecord($_POST,$_FILES);
		}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageCountry.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Country</h1>
  		<fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			<fieldset>
				<input type="hidden" name="cid" value="<?=$cid?>">
				<label>Add Country</label>
				<?=$_SESSION['SESS_MSG']?>				
				
				<!--<section>
					  <label for="CountryCODE">Country Code (3) </label>
					  <div><input type="text" name="countryCode" id="m__countryCode"  value="<?=$_POST['countryCode']?>" /><?=$arr_error[countryCode]?></div>	
				</section>-->
					<section>
					  <label for="CountryCODE">Country Code</label>
					  <div><input type="text" name="countryCode2" id="m__countryCode2"  value="<?=$_POST['countryCode']?>" /><?=$arr_error[countryCode2]?></div>	
				</section> 
				<section>
					  <label for="CountryName">Country Name </label>
					  <div><input type="text" name="countryName" id="m__countryName"  value="<?=$_POST['countryName']?>" /><?=$arr_error[countryName]?></div>
				</section> 
				
				<section>
              			<label for="countryFlag">Country Flag</label>
                		<div>
                   			<input type="file" name="flag" id="m__flag"  class="browse-button" value="" />
              			</div>
        		 </section>
			 </fieldset> 
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
				</div>
             </section>             
        </fieldset>
        </form>
        </fieldset>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

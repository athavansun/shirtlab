<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","add_record");
/*---Basic for Each Page Ends----*/

$prodObj = new MainProducts();
$genObj = new GeneralFunctions();

//////////////////////////////////////////////////////////////////////////////////////////////

if(isset($_POST['submit']))
{	
	/*
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";exit; 	*/
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $prodObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");
	$num = $prodObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();
		while($line = $prodObj->getResultObject($rst)) {			
			array_push($langIdArr,$line->id);
		}

		foreach($langIdArr as $key=>$value) {						
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('colorName_'.$value,$_POST['colorName_'.$value], 'req', 'Please Enter Color Name.');			
		}
		
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
		
		$obj->fnAdd('colorCode',$_POST['colorCode'], 'req', 'Please Enter Color Code.');
		//$obj->fnAdd('designColorCode',$_POST['designColorCode'], 'req', 'Please Enter Design Color Code.');
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		
		//===============================================================
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		//===============================================================
		/*
		if($_POST['prodStyle']) {
			if($prodObj->isProductStyleExist($_POST['prodStyle'])) {
				$arr_error['prodStyle'] = '<span class="alert-red alert-icon">Product Already Added In Selected Style.</span>';
					$str_validate=0;
			}
		} */
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);
			//if($arr_error['productName_'.$value]) $errorArr = 1;
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['colorName_'.$value]=$obj->fnGetErr($arr_error['colorName_'.$value]);
			//if($arr_error['colorName_'.$value])	$errorArr = 1;
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);
			//if($arr_error['productDesc_'.$value]) $errorArr = 1;
		}
		
		//----------------- Image validation ----------------------------------------------------------
		$permitableExt = $prodObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'");//checkExtensions(0);
		foreach($prodObj->images as $key=>$val) {			
			if(!empty($_FILES[$val]['name'])) {
				$fileext = findexts($_FILES[$val]['name']);
				if($fileext) {
					if($prodObj->checkExtensions($fileext) == false ) {
						$arr_error['prodImage'] = '<span class="alert-red alert-icon">Invalid Image Extention For '.$key.',Please Upload Image With Extention '.$permitableExt.'.</span>';
						$str_validate=0;break;
					}
				}
				else {
					$arr_error['prodImage'] = '<span class="alert-red alert-icon">Invalid Image Extention For '.$key.',Please Upload Image With Extention '.$permitableExt.'.</span>';
					$str_validate=0;break;
				}	
			}
			else {
				$arr_error['prodImage'] = '<span class="alert-red alert-icon">Please Uploade An Image For '.$key.'</span>';
				$str_validate = 0;break;
			}
		}
		// ------------------ validation for size --------------------
		$flag = true;
		if(!empty($_POST['sizeidcsv']))
		{
			$sizeidarr= explode(',',$_POST['sizeidcsv']);
			if(sizeof($sizeidarr) > 0)
			{
				foreach($sizeidarr as $sizeid)
				{
					$sizeval = 'size'.$sizeid;
					if($_POST[$sizeval])
					{		
						$flag = false; break;
					}			
					
				}
			}
		}
		if($flag) {
			$arr_error['size'] = '<span class="alert-red alert-icon">Please Select At Least One Size.</span>';
			$str_validate = 0;
		}
		//------------------------------------------------------------
		
		$arr_error['productPrice']=$obj->fnGetErr($arr_error['productPrice']);
		$arr_error['colorCode']=$obj->fnGetErr($arr_error['colorCode']);
		//$arr_error['designColorCode']=$obj->fnGetErr($arr_error['designColorCode']);
		
		if($str_validate) {
			$prodObj->addRecord($_POST, $_FILES);
		}
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/jquery.filestyle.js"></script>
<!--<script language="javascript" type="text/javascript" >
var fileerror_message='<?//="Upload Only ".$catObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION' and fulfillment_id='0' and site_id='0'") ." image extension."?>'</script>
<script src="js/file/jquery.filestyle.js"></script>
<!-- <script language="javascript" src="js/requiredValidation.js"></script>--> 
  <!-- New Drop Down menu -->
  <!-- Menu head -->
 <!--	Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--	Color Picker (END)		-->

</head>
<body>
<? include('includes/header.php'); ?>

 <section id="content">
    <h1>Manage Main Products</h1>
	<!-- left position -->
	<fieldset>
		<form name="frmUser" id="frmUser" method="post" enctype="multipart/form-data" onsubmit="javascript: return validateFrm(this);">
		<input type="hidden" name="cid" value="<?=$cid?>">
		<!-- <input type="hidden" name="atid" id="atid" value="<?//=$allatributeid?>">  -->
		<!-- <input type="hidden" name="atvid" id="atvid" value="<? //=$allatributevalueid?>">  -->
		
		<!---->
		<!--<div id="mainDiv">-->
		<fieldset>
			<label>Add Product</label>
			<!--<div id="div_duplicateUser"></div>
			<div id="div_duplicateEmail"></div>
			<div id="divMessage"></div>-->
			<? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
			<section>
				<label for="categoryId">Product Style</label>
				<div>
					<select name="prodStyle" id="prodStyle">
						<?
							//$genObj = new GeneralFunctions();
							echo $prodObj->getStyleList();
						?>
					</select>
					<div id="chk_prodStyle"><?=$arr_error['prodStyle'];?></div>
				</div>
			</section>
			<section>
				<label for="categoryId">Product Category</label>
				<div>
					<select name="prodCategory" id="prodCategory">
						<?
							//$genObj = new GeneralFunctions();
							echo $prodObj->getCategoryList();
						?>
					</select>
					<div id="chk_prodCategory"><?=$arr_error['prodCategory'];?></div>
				</div>
			</section>
			<section>
				<label for="name">Product Name</label>
				<div>
				 <?=$genObj->getLanguageTextBoxEngValidation('productName','m__productName',$arr_error);?>
				
<!-- 				<input type="text" name="name" id="m__name" class="wel" value="<?=$_POST[name]?>" /> -->
<!-- 				<div id="chk_name"><?=$arr_error['name'];?></div> -->
				</div>
			</section>
			<section>
				<label for="name">Product Price</label>
				<div><input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=stripslashes($_POST[productPrice])?>" maxlength="8" onkeyup="return isNum12(this.value);"   />
				<div id="chk_productPrice"><?=$arr_error['productPrice'];?></div>
				</div>
			</section>
			 <section>
				<label for="description">Product Description</label>
				<div>
				<?=$genObj->getLangTextareaEngValidated('productDesc','m__Product_Description',$arr_error);?>
				
				<!--<textarea name="description" id="m__description" cols="" rows=""><?=$_POST[description]?></textarea>
				<div id="chk_description"><?=$arr_error['description'];?></div>-->
				</div>
			</section>
			<section>
				<label for="colorcode">Color Code</label>
				<div>
					<table style="padding:0px;margin:0px;"  border="0">
						<tr>
							<td><input type="text" name="colorCode" id="m__color__code" maxlength="7" value="<?=$_POST[colorCode] ?>" /></td>
							<td style="text-align:left; width:160px;"><img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmUser.colorCode)" border="0" style="cursor:pointer;"></td>
						</tr>
					</table>
					<div id="chk_colorCode"><?=$arr_error['colorCode'];?></div>
				</div>
			</section>
			<section>
				<label for="name">Color Name</label>
				<div>
				 <?=$genObj->getLanguageTextBoxEngValidation('colorName','m__Color_Name',$arr_error);   ?>
				<!--<input type="text" name="name" id="m__name" class="wel" value="<?//=$_POST[name]?>" />
				<div id="chk_name"><?=$arr_error['name'];?></div>-->
				</div>
			</section>
<!--			<section>
				<label for="name">Color Price</label>
				<div><input type="text" name="colorPrice" value="" />
 				<div id="chk_productPrice"><?=$arr_error['productPrice'];?></div>
				</div>
			</section> -->
			<section>
				<label for="name">Product Image</label>
				<div>
					<?=$prodObj->getColorViewImageUpload(); ?>
					<div id="chk_prodImage"><?=$arr_error['prodImage'];?></div>
				</div>
			</section>
			<section>
				<label for="productSize">Product Size Detail</label>
				<div> <!--<div id="productSize" >-->
					<? //=$genObj->getFirstProductType();
						//$fptid=($_POST['product_type_id']!='')?$_POST['product_type_id']:$genObj->getFirstProductType();?>
					<?=$prodObj->getProductSize(); ?>
					<div id="chk_size"><?=$arr_error['size']?></div>
					<!--</div>-->
				</div>
				<input type="hidden" name="theValue" id="theValue" value="1">
				<input type="hidden" name="theValueNos" id="theValueNos" value="1">
				<!-- <input type="hidden" name="arrayId" id="arrayId" value="<? //=$prodObj->getProductArray() ?>">  -->
				<input type="hidden" name="productId" value="<?=$productid?>" />
				<input type="hidden" name="page" value="<?=$_GET[page]?>" />
				<!-- <input type="hidden" name="sizeoptionno" id="sizeoptionno" value="<?//=$prodObj->getProductArray() ?>">  -->
			</section>
			
		</fieldset>
		<fieldset>
			<section>
				<div>
				<button type="submit" name="submit">Submit</button>
				<button type="button" name="back" onClick="hrefBack()" id="back">Back</button>
				</div>
			</section>
		</fieldset>
		</form>
	</fieldset>
	<div id="divTemp" style="display:none;"></div> 
</section>
   
<script language="javascript">
// <!--

function hrefBack()
{
	window.location.href = "manageMainProducts.php";
}
</script>
<footer>&copy;copyright by <?=__SITENAME__?></footer><div id="nav-under-bg"><!-- --></div>
</body>
</html>

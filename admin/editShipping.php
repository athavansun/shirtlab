<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageShipping.php","edit_record");
/*---Basic for Each Page Starts----*/
$shipObj = new Shipping();
$genObj = new GeneralFunctions();

if(isset($_POST['submit']))
{
	//var_dump($_POST);die;	
	require_once('validation_class.php');
	$obj = new validationclass();
	//$errorArr = 0;

	$obj->fnAdd('firstPairCost',$_POST['firstPairCost'], 'req','Please Enter Cost For First Pair Flip-Flop');
	$obj->fnAdd('firstPairCost',$_POST['firstPairCost'], 'float','Please Enter Correct Value');
	$obj->fnAdd('additionalPairCost',$_POST['additionalPairCost'], 'req','Please Enter Cost For Additional Pair Flip-Flop');
	$obj->fnAdd('additionalPairCost',$_POST['additionalPairCost'], 'float','Please Enter Correct Value');
    if($_POST['freeShipping'] == 1) {
   		//echo 'rsas';die;
    	$obj->fnAdd('minPairsForFreeShipping',$_POST['minPairsForFreeShipping'], 'req','Please Enter Num Of Minimum Pair Flip-Flop For Free Shipping.');
		//$obj->fnAdd('minPairsForFreeShipping',$_POST['minPairsForFreeShipping'], 'num','Please Enter Correct Value');		
    }	
   //=====================================================		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
   //=====================================================
		
	$arr_error['firstPairCost']=$obj->fnGetErr($arr_error['firstPairCost']);
	$arr_error['additionalPairCost']=$obj->fnGetErr($arr_error['additionalPairCost']);
	$arr_error['minPairsForFreeShipping']=$obj->fnGetErr($arr_error['minPairsForFreeShipping']);
	
	if($shipObj->isCountryExistsEdit($_POST['countryId'],$_POST['preCntryId'])) {
		$arr_error['countryId'] = '<span class="alert-red alert-icon">Duplicate Entry For Selected Country</span>';
		$str_validate = 0;
	}
	//var_dump($arr_error);die;
	//echo $str_validate; die;
	if($str_validate){
		//echo 'Rashid'; die;
		$shipObj->editRecord($_POST);	
	}
}
$result = $shipObj->getResult(base64_decode($_GET['id']));?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!-- <script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script> -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageShipping.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Shipping</h1></h1>
  		<fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">	
  		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
  		<input type="hidden" name="preCntryId" value="<?=$result->countryId?>">
		 <fieldset>  
            <label>Edit Shipping</label>
			<?=$_SESSION['SESS_MSG']?>
           	<section>
                  <label for="countryId" >Select Country</label>
                  <div>
                  	<select name="countryId" id="countryId" >
							<?=$genObj->getCountryInDropdown($result->countryId);?>
							</select>
							<div id="chk_countryId"><?=$arr_error['countryId'];?></div>
						</div>
            </section>
				<section>
                  <label>Cost For First Pair</label>
                  <div>
							<input type="text" name="firstPairCost" value="<?=$_POST['firstPairCost']?$_POST['firstPairCost']:$result->firstPairCost?>" />
							<div id="chk_firstPairCost"><?=$arr_error['firstPairCost'];?></div>
						</div>
            </section>
            <section>
                  <label>Cost For Additional Pairs</label>
                  <div>
							<input type="text" name="additionalPairCost" value="<?=$_POST['additionalPairCost']?$_POST['additionalPairCost']:$result->additionalPairCost?>" />
							<div id="chk_additionalPairCost"><?=$arr_error['additionalPairCost'];?></div>
						</div>
            </section>
             <section>
                  <label>Free Shiping</label>
                  <div>
					<input type="radio" name="freeShipping" value="0" <? if($_POST['freeShipping'] == 0 || $result->minPairsForFreeShipping == 0) echo 'checked="checked"';?> onchange="showHideDiv1(this.value);" >&nbsp;No&nbsp;&nbsp;&nbsp;
					<input type="radio" name="freeShipping" value="1" <? if($_POST['freeShipping'] == 1 || $result->minPairsForFreeShipping > 0) echo 'checked="checked"';?> onchange="showHideDiv1(this.value);">&nbsp;&nbsp;Yes
						</div>
            </section>
            <?php
				$none = 'none';            	
            	if($result->minPairsForFreeShipping > 0 || $_POST['freeShipping'] == 1 ) $none = 'block';
            ?>
            <section id="mpfrsDivID" style="display:<?=$none?>;">
                  <label>Minimum Num of Pairs For Free Shipping</label>
                  <div>
							<input type="text" name="minPairsForFreeShipping" value="<?=$_POST['minPairsForFreeShipping']?$_POST['minPairsForFreeShipping']:$result->minPairsForFreeShipping?>" />
							<div id="chk_minPairsForFreeShipping"><?=$arr_error['minPairsForFreeShipping'];?></div>
						</div>
            </section>
		</fieldset>
        <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
              </div>
                </section>
        </fieldset>
	</form>
	</fieldset>
</section>
<? unset($_SESSION['SESS_MSG']); ?>
<script type="text/javascript" >
 function showHideDiv1(val)
 {
 		//alert(val);
 		if(val == 1) document.getElementById('mpfrsDivID').style.display='block';
 		if(val == 0) document.getElementById('mpfrsDivID').style.display='none';	
 }
</script>
<footer>&copy;copyright by Flop Happy</footer>
<div id="nav-under-bg"><!-- --></div>
</body>
</html>

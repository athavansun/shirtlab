<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();

$menuObj->checkPermission("manageLanguage.php","");

/*---Basic for Each Page Ends----*/
$langObj = new Language();
$genObj = new GeneralFunctions();

if(isset($_POST['submit'])) {
	
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $langObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $langObj->getTotalRow($rst);	
	
	if($num){
		$langIdArr = array();		
		while($line = $langObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('constantVal_'.$value,$_POST['constantVal_'.$value], 'req', 'Please Enter Constant Value.');			
		}
		
			$obj->fnAdd('constant',$_POST['constant'], 'req', 'Please Enter Constant.');
			
		$arr_error = $obj->fnValidate();

		foreach($langIdArr as $key=>$value) {
			$arr_error['constantVal_'.$value]=$obj->fnGetErr($arr_error['constantVal_'.$value]);
			if($arr_error['constantVal_'.$value]) 
			$errorArr = 1;
		}
		$arr_error['constant']=$obj->fnGetErr($arr_error['constant']);
			if($arr_error['constant']) 
				$errorArr = 1;
		
		if($langObj->isConstantExits($_POST['constant']))
				$arr_error['constant'] = "Constant already exist.";
				if($arr_error['constant']) 
					$errorArr = 1;	
		
		if($errorArr == 0 && isset($_POST['submit'])){
			$_POST = postwithoutspace($_POST);
			$langObj->addConstant($_POST);
		}
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/custom-form-elements.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='setattribute.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Language</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		 <fieldset>  
            <label>Add New Constant</label>
			<?=$_SESSION['SESS_MSG']?>
			
            <!--<section> 
                   <label>Select View</label>
                  	<div>
				<select name='frontBack' id="frontBack" style="width:100px;" >
					<option value="back" <?php if($_POST['frontBack']=="back") echo "selected";?> ><?=LANG_BACKEND?></option>
					<option value="front" <?php if($_POST['frontBack']=="front") echo "selected";?> ><?=LANG_FRONTEND?></option>
				</select>
					<div><?=$arr_error['categoryImage']?></div>
                  </div>
            </section> -->
			
            <section>
                  <label>Constant Name </label>
                  <div>
				  		<input type="text" name="constant" id="m__constant" />
				  <div><?=$arr_error['constant']?></div>
				  </div>	
            </section> 

            <section>
                  <label>Constant Value </label>
                  <div>
				  <?=$genObj->getLanguageTextBox('constantVal','m__constant_val',$arr_error);   ?>
				  </div>	
            </section> 
          </fieldset>  
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
				</div>
             </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

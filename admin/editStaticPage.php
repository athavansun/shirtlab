<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStaticPage.php","add_record");
$staticpageObj = new StaticPage();
$pageId = base64_decode($_GET['pageId']);
$id =  base64_decode($_GET['id']);
$row = $staticpageObj->checkIsPageExists($pageId,$id);

require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	
	$obj->fnAdd("pageTitle", $_POST["pageTitle"], "req", LANG_PLEASE_ENTER_PAGE_TITLE);
	$obj->fnAdd("message", strip_tags($_POST["message"]), "req", LANG_PLEASE_ENTER_PAGE_CONTENT);
	
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[pageTitle]=$obj->fnGetErr($arr_error[pageTitle]);
	
	$arr_error[message]=$obj->fnGetErr($arr_error[message]);
	
	if($str_validate){
		//$_POST = postwithoutspace($_POST);
		$staticpageObj->editNewStaticPageContaint($_POST);
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageStaticPageDescription.php?id=<?=base64_encode($pageId)?>';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Static Pages</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<input type="hidden" name="pageTypeId" value="<?=$pageId?>" />
		<input type="hidden" name="id" value="<?=$id?>" />
		 <fieldset>  
            <label>Edit >> &nbsp;<?=stripslashes($staticpageObj->fetchValue(TBL_STATICPAGETYPE,"staticPageName","id='$pageId'"))?></label>
			<?=$_SESSION['SESS_MSG']?>
            <section>
                  <label>Language Name</label>
                  <div>
                       	<?=stripslashes($staticpageObj->fetchValue(TBL_LANGUAGE,"languageName","id='$row[langId]'"))?>				
				  </div>	
            </section>
            <section>
                  <label>Page Title </label>
                  <div>
                    <input type="text" name="pageTitle" id="m__Page_Title" value="<?=stripslashes($_POST['pageTitle']?$_POST['pageTitle']:$row['pageTitle']);?>" />
					<div><?=$arr_error['pageTitle']?></div>
				  </div>	
            </section>
			<section>
                  <label>Page Content</label>
                  <div>
	               	<?php
					$message = $_POST['message']?$_POST['message']:$row['pageContaint'];
					$oFCKeditor = new FCKeditor('message') ;
					$oFCKeditor->BasePath = 'fckeditor/' ;
					$oFCKeditor->Height	= 400;
					$oFCKeditor->Width	= 560;
					$oFCKeditor->Value = stripslashes($message);
					$oFCKeditor->Create();
					?>
					<div><?=$arr_error['message']?></div>
				  </div>	
            </section>
		</fieldset>
             <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
              </div>
                </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
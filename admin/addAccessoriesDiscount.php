<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("managePlainProduct.php","");
$accObj = new PlainProduct();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {	
	$obj->fnAdd('accessories',$_POST['accessories'], 'req', 'Please Select Fabric Type.');
	$obj->fnAdd('pcs',$_POST['pcs'], 'req', 'Please Enter Number of pcs.');
    $obj->fnAdd('sign',$_POST['sign'], 'req', 'Please select sign.');	
	    
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[accessories]=$obj->fnGetErr($arr_error[accessories]);
	$arr_error[pcs]=$obj->fnGetErr($arr_error[pcs]);
	$arr_error[sign]=$obj->fnGetErr($arr_error[sign]);
	
	if($str_validate){
		$_POST = postwithoutspace($_POST);
		$accObj->addFabricDiscount($_POST);		                
	}
} else {
	$fId = $_GET['fId'];	
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='managePlainProduct.php';
}

function checkSign(item) {
	if(item == 1) {		
		document.getElementById("m__charges").disabled = true; 
	}else {		
		document.getElementById("m__charges").disabled = false;
	}
}

</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Discount</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
			<fieldset>
				
				<label>Discount</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<section>
                	<label>Select Discount Table</label>
                	<div>
                            <select name="accessories" onchange="return viewProduct(this.value);">
                                    <option value="">Select Discount Table</option>
                                    <?=$accObj->getFabricType($fId?$fId:$_POST['accessories']);?>
                            </select>
                            <?=$arr_error[accessories]?>                            
                	</div>                  	           	
                </section>
                				
				<section>
					  <label for="pcs">Pcs</label>
					  <div>
					  	<input type="text" name="pcs" id="m__pcs"  class="wel" value="<?=stripslashes($_POST[pcs])?>" style="width:200px;" />
						<?=$arr_error[pcs]?>
					  </div>					 
				</section>
				
				<section>
                                    <label for="pcs">Sign</label>
                                    <div>
                                        <select name="sign" onchange="return checkSign(this.value);">
                                            <option value="" <?= (!isset($_POST['sign']))?"selected='selected'":"";?>>Select Sign</option>                                                                   <option value="0" <?= (isset($_POST['sign']) && $_POST['sign']== 0)?"selected='selected'":"";?>>minus</option>
                                            <option value="2" <?= ($_POST['sign'] == 2)?"selected='selected'":"";?>>plus</option>
                                            <option value="1" <?= ($_POST['sign'] == 1)?"selected='selected'":"";?>>x</option>                                    
		                </select>
						<?=$arr_error[sign]?>
					  </div>					 
				</section>
																
				<section>
					  <label for="pcs">Charges</label>
					  <div>
					  	<input type="text" name="charges" id="m__charges"  class="wel" value="<?=stripslashes($_POST[charges])?>" style="width:200px;" /><span style="margin-left:3px; font-size: 17px;">%</span>
						<?=$arr_error[charges]?>
					  </div>					 
				</section>
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />					
                    <input type="button" name="back" id="back" value="Back"   onclick="javascript:hrefBack1()"/>       
				</div>
             </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
//$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
//$genObj = new GeneralFunctions();
$proObj = new Products();
	
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
$pId = $_GET['pId'];

if(isset($_POST['zoomImage'])) {
    require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
    
//    echo "<pre>";
//    print_r($_FILES);exit;
    
    foreach($_FILES as $key => $value) {     
        $obj->fnAdd($key, $_FILES[$key]['name'], 'req', 'Please Select Image to Upload');
    }
    
    //=====================================================		
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
    //=====================================================
    
    foreach($_FILES as $key=>$value) {
        $arr_error[$key] = $obj->fnGetErr($arr_error[$key]);    
    }

    //var_dump($arr_error); die;
    if($str_validate){
        $_POST = postwithoutspace($_POST);
        $proObj->addZoomableImage($_POST, $_FILES);
    }
	
}

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<section id="content-detail">
 <fieldset>
		<form id="zoomFrm" name="uploadZoomableImage" method="post" enctype="multipart/form-data" action="" >
		<fieldset>
			<section>
				<label style="width:100%;font-weight:bold;" for="name">
					<b>Add New Zoomable Image</b>
                    <div style="float:right; margin-right:30px;">
                        <input type="button" name="addmore" value="Add More" onclick="insertField();" />
                    </div>
				</label>
                
			</section>
			<section id="addField">
				<label for="name">Upload Image</label>
				<div>
					<input type="file" name="zoomImage_1" id="m__zoomImage_1" class="file browse-button validate[required] text-input" />
                    <pre>Please upload minimum size of image should be 293 * 380 (width * height)</pre>
				</div>
			</section>			
			
		</fieldset>
		<fieldset>
			<section>
				<div>
					<input type="hidden" name="pId" value="<?=$pId?>" />
					<input type="submit" name="zoomImage" style="cursor:pointer;" value="Submit" />				
				</div>
			</section>
		</fieldset>
		</form>
	</fieldset>
</section>
<script type="text/javascript">
	 i = 1;
    function insertField() {
        i++;
        element = '<div><input type="file" name="zoomImage_'+i+'" id="m_zoomImage_'+i+'" value="" class="file browse-button validate[required] text-input" /></div>';
        
        $("#addField").append(element);        
    }    
    
	jQuery(document).ready( function() {
		// binds form submission and fields to the validation engine
		jQuery("#zoomFrm").validationEngine();
	});
</script>
<? include_once('includes/footer.php');?>

<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');

$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */

$sysObj = new DeliveryTime();
$result = $sysObj->executeQry("Select * from ".TBL_DELIVERY_TIME." order by days asc");

if (isset($_POST['submit'])) {
   //echo "<pre>";print_r($_POST);exit;    
   $sysObj->editDeliveryTime($_POST);
   $result = $sysObj->executeQry("Select * from ".TBL_DELIVERY_TIME." order by days asc");
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
   <h1>Manage Delivery Time<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addDeliveryTime.php">Add Delivery Time</a><? } ?></h1>	     		  
<!--  <h1>Delivery Time Settings</h1>  -->	  
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);">
                  <? echo $_SESSION['SESS_MSG'];                
					 unset($_SESSION['SESS_MSG']); ?>
					 
            <fieldset>
                <label>Delivery Time</label>
                <?php
                    while($line = $sysObj->getResultObject($result)){ ?>                        
                        <section>                    
                            <div style="width:20%; float:left;">                                               
                                    <input type="text" name="day_<?= $line->id;?>" id="day_<?= $line->id;?>" value="<?= $line->days?>" class="wel" style="width:75px;"/>
                                    <span style="font-size: medium;">Days</span>
                            </div>   
                    		<div style="float:left; width:45%; margin-top:3px;">        
                    		<?php if($line->id <= 2) {?>
                                <span style="font-size:13px; margin-left: 5px;"></span>       
                           <?php } else {
                            //only Swiss // plus Germany with valid Custom number   
                            ?>
                                <span style="font-size:13px ; margin-left: 5px;"></span>
                           <?php } ?></div>
                            <div style="width:20%; float:left;">                   
                                    <span style="font-size: medium;">up to</span>
                                    <input type="text" name="pcs_<?=$line->id;?>" id="pcs_<?= $line->id;?>" value="<?= $line->pcs; ?>" class="wel" style="width:75px;"/>
                                    <span style="font-size: medium;">pcs</span>
                            </div>                                        
                            <div style="width:5%; float:left;">
                             	<a class='i_trashcan edit' href='javascript:void(0);' onClick="if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=manageDeliveryTime&type=delete&id=<?= $line->id;?> &page=<?= $pageName ?>'}else{}" >Delete</a>
                            </div>
                        </section>                
                
                <?  }                
                ?>     
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>
<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageUser.php", "");
$userObj = new User();
$generalObj = new GeneralFunctions();

$id = $_GET['uid'] ? $_GET['uid'] : 0;
$id = base64_decode($id);

require_once('validation_class.php');
$obj = new validationclass();
if (isset($_POST['submit'])) {   
    $obj->fnAdd('firma',$_POST['firma'], 'req', 'Please Enter firma/organisation.');
	$obj->fnAdd('title',$_POST['title'], 'req', 'Please select Title.');
	$obj->fnAdd('firstName',$_POST['firstName'], 'req', 'Please Enter First Name.');
	$obj->fnAdd("lastName", $_POST["lastName"], "req", "Please enter  Last Name.");
	$obj->fnAdd('email',$_POST['email'], 'req', 'Please Enter Email.');
	$obj->fnAdd("address", $_POST["address"], "req", "Please enter address.");
	$obj->fnAdd("postCode", $_POST["postCode"], "req", "Please enter  Zip Code.");
	$obj->fnAdd("city", $_POST["city"], "req", "Please enter  City.");
	$obj->fnAdd('countryId',$_POST['countryId'], 'req', 'Please select country.');
	$obj->fnAdd('phoneNo',$_POST['phoneNo'], 'req', 'Please Enter Phone Number.');
	$obj->fnAdd('birthday',$_POST['birthday'], 'req', 'Please select your birthday.');
	$obj->fnAdd('mobile',$_POST['mobile'], 'req', 'Please Enter Mobile Number.');
	$obj->fnAdd('language',$_POST['language'], 'req', 'Please select language.');
//	$obj->fnAdd('password',$_POST['password'], 'req', 'Please Enter password.');
	$obj->fnAdd('status',$_POST['status'], 'req', 'Please select status.');
//   $obj->fnAdd('email', $_POST['email'], 'req', 'Please Enter Email.');
//  $obj->fnAdd("email", $_POST["email"], "email", "Please enter  valid email.");
//   $obj->fnAdd("streetAdd", $_POST["streetAdd"], "req", "Please enter  Street.");
   //=================================================================================
   $arr_error = $obj->fnValidate();
   $str_validate = (count($arr_error)) ? 0 : 1;
   //=================================================================================
	$arr_error[firma]=$obj->fnGetErr($arr_error[firma]);
	$arr_error[title]=$obj->fnGetErr($arr_error[title]);
	$arr_error[firstName]=$obj->fnGetErr($arr_error[firstName]);
	$arr_error[lastName]=$obj->fnGetErr($arr_error[lastName]);
	$arr_error[email]=$obj->fnGetErr($arr_error[email]);
	$arr_error[address]=$obj->fnGetErr($arr_error[address]);
   	$arr_error[postCode]=$obj->fnGetErr($arr_error[postCode]);
	$arr_error[city]=$obj->fnGetErr($arr_error[city]);
	$arr_error[countryId]=$obj->fnGetErr($arr_error[countryId]);
	$arr_error[phoneNo]=$obj->fnGetErr($arr_error[phoneNo]);
	$arr_error[mobile]=$obj->fnGetErr($arr_error[mobile]);
	$arr_error[birthday]=$obj->fnGetErr($arr_error[birthday]);
	$arr_error[language]=$obj->fnGetErr($arr_error[language]);
//	$arr_error[password]=$obj->fnGetErr($arr_error[password]);
	$arr_error[status]=$obj->fnGetErr($arr_error[status]);
   
   
   if ($userObj->isUserEmailExists($_POST['email'])) {
      $arr_error[email] = "Email already exist. ";
   }

   //echo "<pre>";  print_r($arr_error);exit;
   if($str_validate) {
   //if (empty($arr_error[firstName]) && empty($arr_error[lastName]) && empty($arr_error[postCode]) && empty($arr_error[city]) &&  empty($arr_error[countryId]) && empty($arr_error[stateId]) && empty($arr_error[phoneNo]) && empty($arr_error[status]) && isset($_POST['submit'])) {
      $_POST = postwithoutspace($_POST);
      $userObj->editRecord($_POST);
   }  
} else {
	$_POST = displayWithStripslashes($userObj->getUserById($id));
	//echo "<pre>";  print_r($_POST); echo "</pre>";exit;
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
   function hrefBack1(){
      window.location='manageUser.php';
   }
</script>

</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Edit User general profile</h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
         <input type="hidden" name="id" value="<?= $_POST['id'] ?>" >
         <fieldset>

            <label>User Details</label> 
<?= $_SESSION['SESS_MSG'] ?>
            <!-- Start : Title 
            <section>
               <label for="Username">Title </label>
               <div>
                  <select name="title" id="m__Title" class="wel" >
         <?= $userObj->getTitleInDropdown($_POST[2] ? $_POST[2] : $_POST[2]); ?>
                  </select>
<?= $arr_error[title] ?>
               </div>
            </section>
            
            -->
            <!-- Start : Username ------->
        <!--				<section>
					  <label for="Username"><?= LANG_USERNAME ?> </label>
					  <div>
                <input type="text" name="username" id="m__Username"  value="<?= $_POST[username] ?>" />
<?= $arr_error[username] ?>
					  </div>					 
				</section> -->

			<!-- Start : Organisation ------->
				<section>
					  <label for="Title">Firma/Organisation/Club/Association etc.</label>
					  <div>
					  	<input type="text" name="firma" id="m__firma"  value="<?= $_POST[firma]?>" />
						<?=$arr_error[firma]?>
					  </div>					 
				</section>		
			
				<!-- Start : Title ------->
		     	<section>
					  <label for="Title">Title </label>
					  <div>
					  <select name="title" id="m__Title" class="wel" >
				  			<?=$userObj->getTitleInDropdown($_POST['title'] ? $_POST['title'] : $_POST[title]);?>
				  	</select>  
						<?=$arr_error[title]?>
					  </div>
				</section>

            <!-- Start : First Name ------->
            <section>
               <label for="FirstName">First Name </label>
               <div>
                  <input type="text" name="firstName" id="m__First_Name"  value="<?= $_POST[firstName] ?>" />
<?= $arr_error[firstName] ?>
               </div>
            </section>


            <!-- Start : Last Name ------->
            <section>
               <label for="LastName">Last Name </label>
               <div>
                  <input type="text" name="lastName" id="m__Last_Name"  value="<?= $_POST[lastName] ?>" />
<?= $arr_error[lastName] ?>
               </div>
            </section>

            <!-- Start : Email ------->
            <section>
               <label for="Email">Email Id</label>
               <input type="hidden" name="email" value="<?= $_POST[email] ?>" >
               <div><?= $_POST[email] ?></div>
            </section>
            
            <!-- Start :Gender 
            <section>
               <label for="Gender">Gender</label>
               <div>
                  <input type="radio" name="gender" value="M" <?php if (($_POST['gender'] == "M") || $_POST['gender'] == "") { echo "checked='checked'";} ?> />             Male <input type="radio" name="gender" value="F"  <?php if ($_POST['gender'] == "F") { echo "checked='checked'";} ?> />Female
                  <?= $arr_error[gender] ?>
               </div>
            </section>
            -->
                        
            <section>
				  <label for="CEmail">Address*</label>
				  <div>
				  	<input type="text" name="address" id="m__address"  value="<?= $_POST[address]?>" />
					<?=$arr_error[address]?>
				  </div>					 
			</section> 		
            
            <section>
				  <label for="CEmail">Address</label>
				  <div>
				  	<input type="text" name="address1" id="m__address1"  value="<?=$_POST[address1]?>" />
					<?=$arr_error[address]?>
				  </div>					 
			</section>		
            
             <!-- Start : Post Code ------->
            <section>
               <label for="PostCode">Post / Zip Code </label>
               <div>
                  <input type="text" name="postCode" id="m__Zip_Code" value="<?= $_POST[zip] ?>" />
                  <?= $arr_error[postCode] ?>

               </div>
            </section>
            
            <!-- Start : City ------->
            <section>
               <label for="City">City </label>
               <div>
                  <input type="text" name="city" id="m__city"  value="<?= $_POST[city] ?>" />
                  <?= $arr_error[city] ?>

               </div>
            </section>
            
            <!-- Start : Country ------->

            <section>
               <label for="Country">Country</label>
               <div>
                  <select name="countryId" id="m__Country" class="wel" onchange="return showStateOnCountryChange(this.value);" >
                     <?= $generalObj->getCountryInDropdown($_POST['countryId'] ? $_POST['countryId'] : $_POST[countryId]); ?>
                  </select>
                  <?= $arr_error[countryId] ?>
               </div>
            </section>
            
            <!-- Start : Phone  ------->
            <section>
               <label for="Phone">Contact Number </label>
               <div>
                  <input type="text" name="phoneNo" id="m__Contact_Number"  value="<?= $_POST[phoneNo] ?>" />
                  <?= $arr_error[phoneNo] ?>
               </div>
            </section>

            <!-- Start : Mobile ------->
			<section>
				  <label for="mobile">Mobile Phone*</label>
				  <div>
				  	 <input type="text" name="mobile" id="m__mobile"  value="<?=stripslashes($_POST[mobile])?>" />
					 <?=$arr_error[mobile]?>
				  </div>					 
			</section> 
            
            <!-- Start : Birthday ------->
			<section>
				  <label for="Country">Birthday</label>
				  <div>	
					<input style="width: 150px;" type="text" name="birthday" id="dob_date" class="dob_date" value="<?= $_POST['birthday'] ?>" />
					<?=$arr_error[birthday]?>
				  </div>					  	
			</section> 
			
			<!-- Start : Language------->
				<section>
					  <label for="language">Language</label>
					  <div>
					  <select name="language">
						<?= $generalObj->getLanguageInDropDown($_POST['language'] ?$_POST['language']:$_POST['language'])?>
					  </select>
						<?=$arr_error[language]?>
					  </div>					  	
				</section> 
				
            <!-- Start : Password ------->
            <section>
               <label for="Password">Password</label>
               <div>
                  <input type="password" name="password" id="Password"  value="" />
                  <?= $arr_error[password] ?>
               </div>
            </section>
            <!-- Start : Status  ------->
            <section>
               <label for="Status">Status </label>
               <div>
                  <select name="status" id="m__Status">
                     <option value="">--Please Select--</option><br />
                     <option value="1" <? if ($_POST[status] == "1") {echo "selected='selected'"; } ?> >Active</option>
                     <option value="0"  <? if ($_POST[status] == "0") {echo "selected='selected'"; } ?>>InActive</option>
                     <option value="2"  <? if ($_POST[status] == "2") { echo "selected='selected'"; } ?>>Banned</option>
                  </select>
                    <?= $arr_error[status] ?>
               </div>
            </section>

         </fieldset>

         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">

                  <input type="submit" name="submit"   value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
      </form>
   </section>

<? unset($_SESSION['SESS_MSG']); ?>
<?php include_once('includes/footer.php');?>
<link type="text/css" href="datepicker/themes/base/ui.datepicker.css" rel="stylesheet" />
<script src="js/jquery-1.6.2.js"></script>
<script src="js/jquery.ui.core.js"></script>
<script src="js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
$(function() {
    $('#dob_date').datepicker({
            dateFormat: 'yy-mm-dd',
            showButtonPanel: true
    });
});
</script>
	

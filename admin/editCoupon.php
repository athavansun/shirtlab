<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCoupon.php","add_record");
/*---Basic for Each Page Starts----*/

$couponObj = new Coupon();
$genObj = new GeneralFunctions();

require_once('validation_class.php');
$obj = new validationclass();
$errorArr = 0;
if(isset($_POST['submit'])) {
	$obj->fnAdd('couponCode',$_POST['couponCode'], 'req', 'Please enter Coupon Code.');
	$rst = $couponObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");
	$num = $couponObj->getTotalRow($rst);
	if($num > 0){
		$langIdArr = array();
		while($line = $couponObj->getResultObject($rst)) {
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('couponName_'.$value,$_POST['couponName_'.$value], 'req', 'Please Enter coupon name.');
		}
		$arr_error2 = $obj->fnValidate();
		$str_validate = (count($arr_error2)) ? 0 : 1;
		if($_POST['couponDiscountType'] =='Percent' && $_POST['discountValue'] >100)
	   {
		   $arr_error[discountValue]='<span class="alert-red alert-icon">The amount must be Less than 100%</span>';
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error2['couponName_'.$value]=$obj->fnGetErr($arr_error2['couponName_'.$value]);
			if($arr_error2['couponName_'.$value])
				$errorArr = 1;
		}
	}

	$obj->fnAdd("discountValue", $_POST["discountValue"], "req", "Please enter  Discount Value.");

	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[couponCode]=$obj->fnGetErr($arr_error[couponCode]);
	$arr_error[discountValue]=$obj->fnGetErr($arr_error[discountValue]);


	/*
	if($_POST['qty_each_user'] < 1 || !is_int((int)$_POST['qty_each_user']))
	{
		$arr_error['qty_each_user'] = "This value should atleast 1";
	}
	*/

	if($errorArr == 0 && empty($arr_error[couponCode]) && empty($arr_error[discountValue]) && empty($arr_error[qty_each_user]) && isset($_POST['submit'])){
		$couponObj->editRecord($_POST);
	}
}

$result = $couponObj->getResult(base64_decode($_GET['id']));
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageCoupon.php';
}
</script>
<!-- New Drop Down menu -->



<!-- START: Find Random Number --->
<script type="text/javascript">
function find_randomNumber(){
	 
	  xmlhttp=getobject();
  	var query="?action=coupon&type=generateNumber";
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4)
		{							
			var chek = xmlhttp.responseText;			
			document.getElementById("coponGen").innerHTML = xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","pass.php"+query,true);
	xmlhttp.send(null);	
	 
 }

</script>
<!--- END: Find Random Number ---->
</head>
<body onLoad="checkCouponExc('<?=$productExcCheck?>','product');checkCouponExc('<?=$productExcCheck?>','store');">
<? include('includes/header.php'); ?>
 <section id="content">
	<h1>Coupon</h1>
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>Edit Coupon</label>
  <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
  <input type="hidden" name="page" value="<?=$_GET['page']?>">	
			
        <div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset>  
		
        <!-- START:COUPON CODE -->
			<section>
			  <label for="CouponCode">Coupon Code</label>
			  <div>
				<span id="coponGen"><input type="text" name="couponCode" id="m__couponCode" value="<?=$_POST['couponCode']?$_POST['couponCode']:$result->couponCode?>" readonly="true"></span><input type="button" name="back" id="back" value="Generate Code" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;find_randomNumber();"/>
				<?=$arr_error[couponCode]?>
			  </div>	
			</section> 				
		<!-- START:COUPON Name -->						
			<section>
			  <label for="Name">Coupon Name </label>
			  
			  <div>			  
			  <?=$genObj->getLanguageEditTextBox('couponName','m__Coupon_Name',TBL_COUPON_DESCRIPTION,base64_decode($_GET['id']),"couponId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid?>

			  </div>	
			 
			</section> 
		<!-- START: DISCOUNT VALUE -->	
			<section>
			  <label for="DiscountValue">Discount Value</label>
			  <div>
			  <input type="text" name="discountValue" id="m__discount_Value"  class="wel" value="<?=$_POST['discountValue']?$_POST['discountValue']:$result->couponDiscountAmount?>" />
			  <?=$arr_error[discountValue]?>
			  </div>	
					 
			</section> 

               <!-- START: DISCOUNT Type -->
            <section>
               <label for="DiscountType">Discount Type</label>
               <div>
                <?=$couponObj->getDiscountType($result->couponDiscountType)?>
               </div>

            </section>
		<!-- Calendar Scrip --->
			<!-- <script src="js/jquery_DateTime/jquery.min.js"></script> -->

		<script type="text/javascript" src="js/jquery_DateTime/jquery.dynDateTime.js"></script>

		<script type="text/javascript" src="js/jquery_DateTime/lang/calendar-en.js"></script>

		<link rel="stylesheet" type="text/css" media="all" href="js/jquery_DateTime/css/calendar-win2k-1.css"  />
		<script type="text/javascript">

		jQuery(document).ready(function() {

				jQuery("#dateTimeCustom").dynDateTime({

					showsTime: true,

					ifFormat: "%Y-%m-%d ",

					daFormat: "%l;%M %p, %e %m,  %Y",

					align: "TL",

					electric: true,

					singleClick: true

				});

				

			jQuery("#datepicker").dynDateTime({

					showsTime: true,

					ifFormat: "%Y-%m-%d ",

					daFormat: "%l;%M %p, %e %m,  %Y",

					align: "TL",

					electric: true,

					singleClick: true

				});	
			
			});

			</script>
		
		<!-- START: Start Date -->	
			<section>
			  <label for="StartDate">Start Date </label>
			  <div>
			  <input type="text" name="startDate" id="dateTimeCustom"  readonly="true" value="<?=$result->startDate?>" />Leave blank if not applicable</span>
			  </div>	
			 
			</section> 
		
		<!-- START: End Date -->	
			<section>
			  <label for="EndDate">End Date </label>
			  <div>
			  <input type="text" name="endDate" id="datepicker"  readonly="true" value="<?=$result->endDate?>" /> <span class="normal">Leave blank if not applicable</span>
			  </div>	
			 
		</section> 
		    
		
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
          </section>
        </fieldset>
</div>
	</form>
</section>
   <div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>

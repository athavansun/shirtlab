<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCurrency.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$catObj = new Currency();
$cid  = $_GET['id']?$_GET['id']:0;
$result = $catObj->getResult(base64_decode($_GET['id']));
    
   $addHadding  = "Currency";
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
</head>
<body>
         <section id="content-detail">
     <form>
 	<fieldset>
    <label><?=$addHadding ?> Detail</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>
        <section>
       	  <label for="categoryName"><?=$addHadding ?>Name:</label>
             <div><?=$result->currencyName?>
            </div>
        </section>
		
        <section>
       	  <label for="categoryName"><?=$addHadding ?>Code:</label>
             <div><?=$result->currencyCode?>
            </div>
        </section>
          
        <section>
       	  <label for="categoryName"><?=$addHadding ?>Sign:</label>
             <div><?=html_entity_decode($result->sign);?>
            </div>
        </section>
	
     </fieldset>
     </form>
    </section>
      
<? include_once('includes/footer.php');?>

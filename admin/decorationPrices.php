<?
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("decorationPrices.php","add_record");
$decorationObj = new Decoration();
$generalObj = new GeneralFunctions();
$isEmbPriceTable = $generalObj->haveRow(TBL_DECORATION_PRICE_TABLE,"id");
$isScrPriceTable = $generalObj->haveRow(TBL_DECORATION_PRICE_TABLE_SCREEN,"id");

require_once('validation_class.php');
$obj = new validationclass();

if(isset($_POST['submit'])) {

	if(isset($_POST['digitalPrinting'])){
		$obj->fnAdd('white',$_POST['white'], 'req', 'Please enter digital printing price for white.');
		$obj->fnAdd('white',$_POST['white'], 'float', 'Please enter correct value for digital printing price for white.');
	
		$obj->fnAdd('light',$_POST['light'], 'req', 'Please enter digital printing price for light.');
		$obj->fnAdd('light',$_POST['light'], 'float', 'Please enter correct value for digital printing price for light.');
	
		$obj->fnAdd('dark',$_POST['dark'], 'req', 'Please enter digital printing price for dark.');
		$obj->fnAdd('dark',$_POST['dark'], 'float', 'Please enter correct value for digital printing price for dark.');
	}

	if(isset($_POST['screenPrinting'])){
		$obj->fnAdd('single',$_POST['single'], 'req', 'Please select default single price.');
	}
	
	if(isset($_POST['embroideryPrice'])){
		$obj->fnAdd('priceTable',$_POST['priceTable'], 'req', 'Please select price table.');

		$obj->fnAdd('stitchCount',$_POST['stitchCount'], 'req', 'Please enter default stitch count.');
		$obj->fnAdd('stitchCount',$_POST['stitchCount'], 'num', 'Please enter correct value for default stitch count.');

		$obj->fnAdd('digiFee',$_POST['digiFee'], 'req', 'Please enter digitizing fee.');
		$obj->fnAdd('digiFee',$_POST['digiFee'], 'float', 'Please enter correct value for digitizing fee.');
	}

	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1; 
	
	$arr_error['white'] = $obj->fnGetErr($arr_error['white']);
	$arr_error['light'] = $obj->fnGetErr($arr_error['light']);
	$arr_error['dark'] = $obj->fnGetErr($arr_error['dark']);
	$arr_error['single'] = $obj->fnGetErr($arr_error['single']);
	$arr_error['priceTableId'] = $obj->fnGetErr($arr_error['priceTableId']);
	$arr_error['stitchCount'] = $obj->fnGetErr($arr_error['stitchCount']);
	$arr_error['digiFee'] = $obj->fnGetErr($arr_error['digiFee']);
	
	$_SESSION['SESS_MSG'] = $arr_error['white'].$arr_error['light'].$arr_error['dark'].$arr_error['single'].$arr_error['priceTableId'].$arr_error['stitchCount'].								$arr_error['digiFee'];
	if($str_validate){
		$_POST = postwithoutspace($_POST);
		$decorationObj->saveRecord($_POST);
	}
}

	$decProIdDigital = 1;
	$decoPriMetIdDigital = 1;

	$decProIdScreen = 2;
	$decoPriMetIdScreen = 2;

	$decProIdEmbroidery = 3;
	$decoPriMetIdEmbroidery = 3;

	$row1 = $decorationObj->getDigitalPrinting($decProIdDigital, $decoPriMetIdDigital);
	$row2 = $decorationObj->getScreenPrinting($decProIdScreen, $decoPriMetIdScreen);
	$row3 = $decorationObj->getEmbroidery($decProIdEmbroidery, $decoPriMetIdEmbroidery);
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- Light Box Starts -->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->
<!--          *****************            DRAG AND DROP  - START        ***************   -->
<!--<script type="text/javascript" src="js/file/jquery-ui-1.7.2.custom.min.js" ></script>
<script type="text/javascript" src="js/file/drag-drop.js" ></script>-->
<!--          *****************            DRAG AND DROP  - END          ***************   -->
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
	 <h1>Decoration Processes</h1>
	 
	<?php if($isEmbPriceTable == 0 || $isScrPriceTable == 0) {?>
		<?=msgSuccessFail("fail","Please First manage embroidery and screen printing price table."); ?>
	<?php }else{ ?>
	  
	<fieldset>
	<form name="decoration" id="decoration" method="post"  enctype="multipart/form-data">
	 <label><?=LANG_DEFAULT_DECORATION_PRICES?></label>
	 
	<input type="hidden" name="digital" value="1">
	<input type="hidden" name="screen" value="2">
	<input type="hidden" name="embroidery" value="3">
	<input type="hidden" name="white-light-dark-price" value="1">
	<input type="hidden" name="single-price" value="2">
	<input type="hidden" name="price-table" value="3">
	
	<div class="top-filter bg02">
	</div>
	<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	<table class="documentation">
	   <tr><th>Active</th>
		   <th>Decoration process</th>
		   <th>Pricing method</th>
		   <th>Decoration pricing ( <?=$_SESSION['DEFAULTCURRENCYSIGN'] ?> )</th>
	   </tr>
	   <tr >
			<td><input type="checkbox" <?php if(isset($_POST['digitalPrinting']) || ($row1->id)){?> checked="checked" <?php }?> name="digitalPrinting" id="digitalPrinting" /></td>
			<td><?=LANG_DIGITAL_PRINTING?></td>
			<td>white/light/dark price</td>
			<td>
			<?php
			   $digiPrintPrice = explode("-", $row1->price);
			?>
				  <input type="text" style="width: 40px;" size="3" name="white" id="white" value="<?=$digiPrintPrice[0] ? $digiPrintPrice[0] : $_POST['white'] ;?>">
				/ <input type="text" style="width: 40px;" size="3" name="light" id="light" value="<?=$digiPrintPrice[1] ? $digiPrintPrice[1]: $_POST['light'];?>">
				/ <input type="text" style="width: 40px;" size="3" name="dark" id="dark" value="<?=$digiPrintPrice[2] ? $digiPrintPrice[2] : $_POST['dark'];?>">&nbsp;
			</td>
	   </tr>
	   
	   <tr >
			<td><input type="checkbox" <?php if(isset($_POST['screenPrinting']) || ($row2->id)){?> checked="checked" <?php }?> name="screenPrinting" id="screenPrinting" /></td>
			<td><?=LANG_SCREEN_PRINTING?></td>
			<td><?=LANG_DEFAULT_SINGLE_PRICE?></td>
			<td>
				<select name="single" id="single">
					<?=$decorationObj->getSinglePriceTable($row2->price_table_id ? $row2->price_table_id : $_POST['single']); ?>
				</select>
			</td>
	   </tr>       
	
	   <tr >
			<td><input type="checkbox" <?php if((isset($_POST['embroideryPrice'])) || ($row3->id)){?> checked="checked" <?php }?>  name="embroideryPrice" id="embroideryPrice" /></td>
			<td><?=LANG_EMBROIDERY?></td>
			<td><?=LANG_PRICE_TABLE?></td>
			<td>
				<select name="priceTable" id="priceTable" style="width:150px; margin:0 10 10 10;">
					<?=$decorationObj->getPriceTable($row3->price_table_id ? $row3->price_table_id : $_POST['priceTable']); ?>
				</select>&nbsp;<?=LANG_DEFAULT_STITCH_COUNT ?>
				<input type="text" style="width: 40px;" value="<?=$row3->stitch_count ? $row3->stitch_count : $_POST['stitchCount']  ?>" size="05" name="stitchCount">
				&nbsp;<?=LANG_DIGITIZING_FEE ?>
				<input type="text" style="width: 40px;" value="<?=$row3->digitizing_fee ? $row3->digitizing_fee : $_POST['digiFee'] ?>" size="05" name="digiFee">
			</td>
	   </tr>  
	   <tr>
	   </tr>     
	</table>
	<table>
	<tr><td colspan="3"><input type="submit" name="submit"  style="cursor:pointer;" value="<?=LANG_SAVE?>" /></td></tr>
	</table>
	</form>
	</fieldset>
	<? } ?>	
</section>

</body>
</html>
<?php
/*---Basic for Each Page Starts----*/
session_id();
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
if(isset($_POST['submitLogin'])) {
	$loginObj->adminLogin($_POST);
}
/*---Basic for Each Page Ends----*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="utf-8">
 	<title>Login <?=SITENAME?></title>
 	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/main/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" class="theme">
 	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
 <script type="text/javascript">
	function focusTxtBox()
	{
		document.getElementById("userName").focus();
	}
</script>
</head>


<body id="login" onload="javascript:focusTxtBox();">
	<header>
		<div id="logo">
			<a href="../" target="_blank"><?=SITENAME?></a>
		</div>
	</header>


	<section id="content">
		<form name="frmLogin" method="post" action="" onsubmit="return validationLogin()">
			<div id="login-box">
				<?php if($_SESSION['SESS_MSG']){?>
				<div class="alert warning" id="alert"><?=$_SESSION['SESS_MSG']?></div>
				<?php }?>
				<fieldset>
					<section>
						<label for="username">Username</label>
						<div><input type="text" id="userName" name="userName" value="<?=$_POST['userName']?>"></div>
					</section>
					<section>
						<label for="password">Password </label>
						<div><input type="password" id="userPassword" name="userPassword"></div>
						<div><input type="checkbox" id="remember" name="remember">
						<label for="remember" class="checkbox">remember me</label></div>
					</section>
					<section>
						<div><button class="fr" type="submit" name="submitLogin">Login</button></div>
					</section>
					
				</fieldset>
		</form>
		<div id="lost-password"><a href="lost-password.php">Lost your password?</a></div>
	</section>
	<footer>&copy;Copyright by <?=SITENAME?></footer>
	<? unset($_SESSION['SESS_MSG']); ?>
</body>
</html>

<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
//error_reporting(0);
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageNewsletter.php", "edit_record");

/* ---Basic for Each Page Ends---- */
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();
$id = base64_decode($_GET['id']);
$row = $newsletterObj->newsLetterToEdit($id);
if (isset($_POST['submit'])) {
   //echo "<pre>"; print_r($_POST);echo "</pre>";exit;
   require_once('validation_class.php');
   $obj = new validationclass();

   $obj->fnAdd("mailsubject", $_POST["mailsubject"], "req", "Please enter  Mail Subject.");
   $obj->fnAdd('email', $_POST['email'], 'req', 'Please enter Email Address.');
   $obj->fnAdd("email", $_POST["email"], "email", "Please enter valid Email Address.");
   $obj->fnAdd("message", strip_tags($_POST["message"]), "req", "Please enter Mail Containt.");



   $arr_error = $obj->fnValidate();
   $str_validate = (count($arr_error)) ? 0 : 1;

   $arr_error[mailsubject] = $obj->fnGetErr($arr_error[mailsubject]);
   $arr_error[email] = $obj->fnGetErr($arr_error[email]);
   $arr_error[message] = $obj->fnGetErr($arr_error[message]);

   if ($str_validate) {
      $newsletterObj->sendNewsletterToUsers($_POST);
   }
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
   function hrefBack1(){
      window.location='manageNewsletter.php';
   }
</script>
<script type="text/javascript">
   function show_date_box(valu){
      if(valu == 1){
         document.getElementById('datedisplaybox').style.display = 'none';
         document.getElementById('dateTimeCust').value = '';
      }
      if(valu == 2){
         document.getElementById('datedisplaybox').style.display = '';
      }
   }



   function showusertypediv(optionVal){
      if(optionVal == 1){
         document.getElementById('allusertype').style.display = '';
         document.getElementById('uploadxls').style.display = 'none';
         document.getElementById('userlist').style.display = 'none';
         document.getElementById('manualusers').style.display = 'none';
      }
	
      if(optionVal == 2){
         document.getElementById('allusertype').style.display = 'none';
         document.getElementById('uploadxls').style.display = '';
         document.getElementById('userlist').style.display = 'none';
         document.getElementById('manualusers').style.display = 'none';
      }
	
      if(optionVal == 3){
         document.getElementById('allusertype').style.display = 'none';
         document.getElementById('uploadxls').style.display = 'none';
         document.getElementById('userlist').style.display = '';
         document.getElementById('manualusers').style.display = 'none';
      }
	
      if(optionVal == 4){
         document.getElementById('allusertype').style.display = 'none';
         document.getElementById('uploadxls').style.display = 'none';
         document.getElementById('userlist').style.display = 'none';
         document.getElementById('manualusers').style.display = '';
      }
	
   }



   <!-- Check XLS File ------------------->
   function isXLS(){
      var chk=$('#usertyperadio2').is(':checked');
      //alert(chk);
		
      if(chk){
         xls =$('#db_xlsfile').val();
         start=xls.lastIndexOf(".");
         argvalue = xls.substring(xls.length-3, xls.length);
         if (argvalue.toLowerCase() != "xls" ) {
            $("#db_xlsfileInfo").addClass("alert-red alert-icon");
            $("#db_xlsfileInfo").html('Please Upload .xls file.');
            return false;
         }else{
            $("#db_xlsfileInfo").removeClass("alert-red alert-icon");
            $("#db_xlsfileInfo").html('');
            return true;
         }
      }
   }


</script>
</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Send Newsletter</h1>
      <form name="sendNewsletterForm" id="sendNewsletterForm" method="post" onSubmit="return (validateFrm(this) && isXLS());"  enctype="multipart/form-data">
         <fieldset>

            <label>Send Newsletter</label>
              <?= $_SESSION['SESS_MSG'] ?>
            <!-- Start : Subject -------------------->
            <section>
               <label for="Subject">Subject</label>
               <div>
                   <? $subject = $_POST['mailsubject'] ? $_POST['mailsubject'] : $row['newsletterSubject']; ?>
                  <textarea name="mailsubject" id="m__Mail_Subject"><?= nl2br(stripslashes($subject)) ?></textarea>
                  <?= $arr_error[mailsubject] ?>
               </div>
            </section>

            <!-- Start : From Mail ID -------------------->
            <section>
               <label for="From Mail ID">From MailId</label>
               <div>
               <? $email = $_POST['email'] ? $_POST['email'] : $row['senderEmailId']; ?>
                  <input type="text" name="email" id="m__email" size="3"  value="<?= stripslashes($email) ?>" />
                  <?= $arr_error[email] ?>
               </div>
            </section>

            <!-- Start : Send Newsletter: Now-Schedule -------------------->
            <section>
               <label for="SendNewsletter">Send Newsletter</label>
               <div>
                  <input type="radio" name="sendnewsletternow" id="sendnewsletternow" value="1" checked="checked" onclick="show_date_box(1);" />Now
                  <!--<input type="radio" name="sendnewsletternow" id="sendnewsletternow" value="2"	onclick="show_date_box(2);"  />Schedule
                  <span style="display:none;" id="datedisplaybox">
                  <?php include_once("datetime.php"); ?>-->
                  </span>
               </div>
            </section>

            <!-- Start : Attachment-------------------->
<?php
if ($row['attachedFile'] && $row['attachmentPath']) {
   $attachment = "<a href='" . __NEWSLETTERPATH__ . $row[attachmentPath] . "' style='cursor:pointer;' target='new'>" .
           $row['attachedFile'] .
           "</a>";
   ?>
               <section>
                  <label for="Attachment">Attachment</label>
                  <div><?= $attachment ?></div>
               </section>
               <input type="hidden" name="attachmentfile" value="<?= $row[attachmentPath] ?>" />
               <input type="hidden" name="attachmentname" value="<?= $row[attachedFile] ?>" />
            <?php } ?>


            <!-- Start : Newsletter Content -------------------->
            <section>
               <label for="Newsletter Content">Newsletter Content</label>
               <div>
                  <?php
                  $message = $_POST['message'] ? $_POST['message'] : $row[newsletterContaint];
                  $oFCKeditor = new FCKeditor('message');
                  $oFCKeditor->BasePath = 'fckeditor/';
                  $oFCKeditor->Height = 400;
                  $oFCKeditor->Width = 740;
                  $oFCKeditor->Value = stripslashes($message);
                  $oFCKeditor->Create();
                  ?>
                  <?= $arr_error[message] ?>
               </div>
            </section>


            <!-- Start : Send Newsletter To -------------------->
            <section>
               <label for="SendNewsletterTo">Send Newsletter To</label>
               <div>
                  <div id="usertypediv1" style="clear:both;margin:30px 0 0 0;">
<!--                     <input type="radio" name="usertyperadio" id="usertyperadio1" value="1" checked="checked" onclick="showusertypediv(1);" />User Type-->
                  </div>

                  <div id="allusertype" style="padding-left: 20px;">
                     <?php //echo  $newsletterObj->newsletterUsersChkBox($_POST['usertypeId']); ?>
                  </div>


                  <div id="usertypediv2" style="clear:both;margin:10px 0 0 0;">
                      <input type="radio" name="usertyperadio" id="usertyperadio" value="2" onclick="showusertypediv(2);" /> Upload XLS
                  </div>

                  <div id="uploadxls" style="display:none;">
                     <input type="file" name="db_xlsfile" id="db_xlsfile" />

                  </div>

                  <div id="usertypediv3" style="clear:both;margin:10px 0 0 0;">
                     <input type="radio" name="usertyperadio" id="usertyperadio3" value="3" onclick="showusertypediv(3);" />User List</div>

                  <div id="userlist" style="display:none;">
                     <div id="check" style="padding-left: 20px;display:none;">
                        <a href="javascript:void(NULL)" class="buttontext"
                           onclick='javascript:checkAllCheckboxes(document.sendNewsletterForm);'><?= LANG_SELECT_ALL ?></a>
                     </div>
                     <div id="uncheck" style="padding-left: 20px; display:none;">
                        <a href="javascript:void(NULL)" class="buttontext"
                           onclick='javascript:uncheckAllCheckboxes(document.sendNewsletterForm);'><?= LANG_UNSELECT_ALL ?></a>
                     </div>

                     <?php echo  $newsletterObj->getAllUserList(); ?>
                  </div>


                  <div id="usertypediv4" style="clear:both;margin:10px 0 0 0;">
                     <input type="radio" name="usertyperadio" id="usertyperadio4" value="4" onClick="showusertypediv(4);"/>
							Emails
                  </div>

                  <div id="manualusers" style="display:none;">
                     <textarea name='manualemailids'></textarea>
                     <br> You can enter multiple emailId by comma (,) seperated.
                  </div>

               </div>
            </section>
         </fieldset>

         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">

                  <input type="submit" name="submit"   value="Send Newsletter Now" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
         <input type="hidden" name="newsletterId" value="<?= $id ?>" />
         <input type="hidden" name="page" value="<?= $_GET['page'] ?>" />
      </form>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>
   <? include_once('includes/footer.php');?>

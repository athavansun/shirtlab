<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageView.php","edit_record");
/*---Basic for Each Page Starts----*/
$viewObj = new View();
$generalFunctionObj = new GeneralFunctions();

if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $viewObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' ORDER BY languageName asc","","");		
	$num = $viewObj->getTotalRow($rst);	
	if($num > 0){
		$langIdArr = array();		
		while($line = $viewObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('viewName_'.$value,$_POST['viewName_'.$value], 'req', 'Please Enter View Name');
		}
		$obj->fnAdd("price", $_POST["price"], "req", "Please enter the Price.");
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		$arr_error[price]=$obj->fnGetErr($arr_error[price]);
		foreach($langIdArr as $key=>$value) {
			$arr_error['viewName_'.$value]=$obj->fnGetErr($arr_error['viewName_'.$value]);
		}

		foreach($langIdArr as $key=>$value) {
			if($viewObj->isViewExist($_POST['viewName_'.$value],$value,$_POST[id]))
			{ 
				$arr_error['viewName_'.$value] = 'View Name Already Exists';
			}
		}	
	}
	if($str_validate){
		$viewObj->editRecord($_POST);		
	}
}
	$result = $viewObj->getResult(base64_decode($_GET['id']));
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageView.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php');  ?>

  <section id="content">
  		<h1>View</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">	
  		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		 <fieldset>  
            <label>Edit View</label>
			<?=$_SESSION['SESS_MSG']?>
            <section>
                  <label>Name</label>
                  <div>
			<?=$generalFunctionObj->getLanguageEditTextBox('viewName','m__Name',TBL_VIEWDESC,base64_decode($_GET['id']),"viewId",$arr_error) ?>
			</div>
            </section>
		<section style="display:none;">
		  <label>Price</label>
			 <div>
		<input type="text" name="price" id="m__price" maxlength="6" value="<?=$result->price ?>" />
		<p><?=$arr_error[price] ?></p>
		</div>
	 </section>
		</fieldset>
        <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
              </div>
                </section>
        </fieldset>
	</form>
</section>
<? unset($_SESSION['SESS_MSG']); ?>

<?php

session_start();

require_once("includes/JSON.php");

class WelCommit extends MySqlDriver{
	var $productColorArr = array();
	function __construct() {
	  $this->obj = new MySqlDriver;
    }

	function valDetail($status) {

		if($status)
		{
			$dateStatus="commitDate";
			$commintUncommit="";
		}
		else
		{
			$dateStatus="addDate";
			$commintUncommit="Un";
		}

		$cond = "1 and status = '".$status."' group by panelId";
	 	$query = "select *,COUNT(identification) AS num from ".TBL_COMMITLOG." where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {

		//-------------------------Paging------------------------------------------------

			$paging = $this->paging($query);
			$this->setLimit($_GET[limit]);
			$recordsPerPage = $this->getLimit();
			$offset = $this->getOffset($_GET["page"]);
			$this->setStyle("redheading");
			$this->setActiveStyle("smallheading");
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();
			//-------------------------Paging------------------------------------------------

			$orderby = $_GET[orderby] ? $_GET[orderby] : $dateStatus;
		    $order = $_GET[order]? $_GET[order]:"DESC";
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;

			$rst = $this->executeQry($query);
			$row = $this->getTotalRow($rst);

			$strQuery = "SELECT ".$dateStatus." FROM ".TBL_COMMITLOG." WHERE status='".$status."' GROUP BY ".$dateStatus." DESC LIMIT 2";
			$DateRst = $this->executeQry($strQuery);

			$NoOfRowsInDate = $this->getTotalRow($DateRst);
			
			$genTable='<table cellpadding="2" cellspacing="2" width="50%" align="center" border="0">';
			
			if($NoOfRowsInDate >= 1)
			{
				while($DataDate=$this->getResultObject($DateRst))
				{
					$dateToDisplay[]=$DataDate->$dateStatus;
				}				
				if(count($dateToDisplay)==2)
				{
					$startDate = date("d M,Y",strtotime($dateToDisplay[0]));
					$endDate = date("d M,Y",strtotime($dateToDisplay[1]));
				}
				else if(count($dateToDisplay)==1)
				{
					$startDate = date("d M,Y",strtotime($dateToDisplay[0]));
					$endDate = "";
				}
			}			
			$genTable .='<tr height="50" align="left" bgcolor="#DDDDDD">
                         <td style="vertical-align:middle;text-align:left;" colspan="3">
                         <h3>&nbsp;'.$commintUncommit.'Committed Record(s)
                         &nbsp;&nbsp;&nbsp;&nbsp;Date From:'.$startDate.' To '.$endDate.'';
                        if($status==0) {
							$genTable .= '<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;float:right;" value="&nbsp;Commit&nbsp;" />';
                        }

               $genTable .= '</h3></td>';
               
               $genTable .='</tr>
               <tr bgcolor="#F7F7F7">
               <td style="width:30px;">SN.</td>
               <td style="width:10px">Panel</td>
               <td style="width:10px">Section</td>
               </tr>';
			if($row > 0) {

				$i = 1;

				while($line = $this->getResultObject($rst)) {

					$div_id = "status".$line->id;

					if ($status==0){						
						 $color = "color:#FF0000;";
					}
					else{
						$color = "color:#006633;";
					}

					$menuName = $this->fetchValue(TBL_MENU,"menuName","1 and menuId = '$line->panelId'");

					$genTable .= '<tr>
								 	<td style="width:70px;'.$color.'">'.$i.'</td>
									<td style="width:200px;'.$color.'">'.$menuName.'</td>
									<td style="width:100px;'.$color.'">'.$line->section.'('.$line->num.')</td>
								</tr>';
					$i++;
				}

				

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}

				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
		}

		} else {
			$genTable = '<div style="float:right;display:inline;">
							<input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Commit Again" /></div>
							<div class="Error-Msg">Sorry, No '.$commintUncommit.'commited record found !!! </div>';
		}

		  if ($status==0){
					//$genTable .='<tr><td colspan="3"><input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Commit" /></td></tr>';
					}

		
		$genTable.='</table';
		return $genTable;

	}



	function updateCommit()
	{
		$query = mysql_query("select id from ".TBL_COMMITLOG." where status='0'");
		while($sd=mysql_fetch_object($query))
		{
		    $date=date(ymdhis);
			$statusquery=mysql_query("UPDATE ".TBL_COMMITLOG." SET status = '1', commitDate=".$date." WHERE id=".$sd->id." ");
		}
	}



	function generateXML($get = NULL){
		$this->updateCommit();
		$query = "select * from ".TBL_LANGUAGE." where 	isDeleted = '0' and status = '1'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0)
		{
			$i = 1;
			while($line = $this->getResultObject($sql)) {
				$dirPath = ABSOLUTEPATH."php";
				$this->createDirectory($dirPath);
				$xmlfileArr = $this->getXmlDataArr($line->id);
				
				foreach($xmlfileArr as $key=>$value){
					$xmlFilePath = ABSOLUTEPATH."php"."/".$key."_".$line->languageCode.".xml";
					@unlink($xmlFilePath);
					$xmlFileHandle = fopen($xmlFilePath, 'w') or die("can't open file");
					fwrite($xmlFileHandle, $value);
					fclose($xmlFileHandle);
				}
				$i++;
			}
		}
		$_SESSION['SESS_MSG'] =  msgSuccessFail("success","xml has been generated successfully.");
		header("Location:adminArea.php");
		exit;
	}



	function getXmlDataArr($langId){
		$data = $this->getData($langId);// exit;
		$sendArr = array('content'=>$data);
		$sendArr = $sendArr;
		return $sendArr;
	}

	function getData($langId){
		$tbl = '';
		$tbl .= '<?xml version="1.0" encoding="utf-8"?>'."\n".
		'<content>'."\n";

		$contentQuery = "SELECT TBL1.*, TBL2.toolName FROM ".TBL_TOOL." AS TBL1	INNER JOIN ".TBL_TOOLDESC." AS TBL2 ON (TBL1.id = TBL2.Id) WHERE TBL1.status = '1' AND TBL1.isDeleted = '0' AND TBL2.langId = '".$langId."' ";

	    $contentSql = $this->executeQry($contentQuery);
	    $contentNum = $this->getTotalRow($contentSql);
		if($contentNum)
		{
			while($tagData = $this->getResultObject($contentSql)) {

				$tbl .='<'.$tagData->variableName.' value="'.html_entity_decode(stripslashes($tagData->toolName)).'"/>'."\n";
				//$tbl .= '<'.$tagData->variableName.'>'.htmlspecialchars(stripslashes("".$tagData->toolName."")).'</'.$tagData->variableName.'>'."\n";
			}

		}
		$tbl .='</content>';
		
		return $tbl;
	}

	function createDirectory($dirName)
	{
		if(!is_dir($dirName)) {
			@mkdir($dirName);
			@chmod($dirName,0777);
		}
	} // End of Function

}

?>

<?php 
session_start();
class CountrySetting extends MySqlDriver{
	function __construct() {
		 $this->obj = new MySqlDriver; 
	}

        function val() {        								
            //$cond = "1 and cs.countryId = cou.countryId and cur.id = cs.currencyId and cou.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
            
            $rs = $this->fetchValue(TBL_SYSTEMCONFIG, 'systemVal', "systemName = 'DEFCOUNTRY'");
                        
            $cond = "1 and cs.id = csd.settingId and csd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and cs.countryId = cd.countryId and cur.id = cs.currencyId ";    
            if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
                    $searchtxt = $_REQUEST['searchtxt'];
                    $cond .= " AND (cou.countryName LIKE '%$searchtxt%' OR cur.currencyName LIKE  '%$searchtxt%')";
            }

//            echo $query = "select cs.* cou.countryName, cur.sign from ".TBL_COUNRTYSETTING." as cs inner join ".TBL_COUNTRY_DESCRIPTION." as cou inner join ".TBL_CURRENCY_DETAIL." as cur on ".$cond;
            $query = "select cs.*, csd.message, cd.countryName, cur.sign from ".TBL_COUNRTYSETTING." as cs inner join ".TBL_COUNRTYSETTINGDESC." as csd inner join ".TBL_COUNTRY_DESCRIPTION." as cd inner join ".TBL_CURRENCY_DETAIL." as cur on ".$cond; 
            $sql = $this->executeQry($query);
            $num = $this->getTotalRow($sql);
            $menuObj = new Menu();
            $page =  $_REQUEST['page']?$_REQUEST['page']:1;
            if($num > 0) {

                    //-------------------------Paging------------------------------------------------			
                    $paging = $this->paging($query); 
                    $this->setLimit($_GET['limit']); 
                    $recordsPerPage = $this->getLimit(); 
                    $offset = $this->getOffset($_GET["page"]); 
                    $this->setStyle("redheading"); 
                    $this->setActiveStyle("smallheading"); 
                    $this->setButtonStyle("boldcolor");
                    $currQueryString = $this->getQueryString();
                    $this->setParameter($currQueryString);
                    $totalrecords = $this->numrows;
                    $currpage = $this->getPage();
                    $totalpage = $this->getNoOfPages();
                    $pagenumbers = $this->getPageNo();		
                    //-------------------------Paging------------------------------------------------

                    $orderby = $_GET[orderby]?$_GET[orderby]:"countryName";
                    $order = $_GET[order]?$_GET[order]:"ASC";

                    $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
                    $rst = $this->executeQry($query); 
                    $row = $this->getTotalRow($rst);
                    $genTable = '';
                    if($row > 0) {			
                            $i = 1;			
                            while($line = $this->getResultObject($rst)) {
                                    //~ echo "<pre>";
                                    //~ print_r($line);
                                    //~ echo "</pre>";exit;
                                    
                                
                                $highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
                                $div_id = "status".$line->id;
                                $status=($line->status)?"Active":"Inactive";
                                $eoriNo = $line->eoriNo == 1?"Yes":"No";
                                $genTable .= '<tr>';
                                if($line->countryId == $rs) {
                                    $genTable .='<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox" disabled="disabled"></th>';
                                } else{
                                    $genTable .='<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>';    
                                }
                               $genTable .='<td>'.$i.'</td>
                                        <td>'.$line->countryName.'</td>
                                        <td>'.html_entity_decode($line->sign).'</td>
                                        <td>'.$line->vat.'</td>
                                        <td>'.$line->customDuty.'</td>
                                        <td>'.$eoriNo.'</td>';
            $genTable.='<td>';                    
                                if($menuObj->checkEditPermission()) {							
                                    if($line->countryId == $rs) {
                                        $genTable .='Active';
                                    }else {                                  
                                $genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'countrysetting\')" >'.$status.'</div>';
                                    }
                                }	
                                $genTable .= '</td>';                                        

            $genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->fabricName).'" href="viewCountrySetting.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';                    
            $genTable.='<td>';
                                if($menuObj->checkEditPermission()) {					
                                        $genTable .= '<a class="i_pencil edit" href="editCountrySetting.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
                                }	
                                $genTable .= '</td>';

            $genTable.='<td>';
                                if($menuObj->checkDeletePermission()) {					
                                    if($line->countryId == $rs) {
                                        $genTable .= '';
                                    }else {                                    
                                        $genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=countrysetting&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
                                    }
                                }
                                $genTable .= '</td>';

            $genTable.='</tr>';
                                $i++;	
                        }
                        switch($recordsPerPage){
                                 case 10:
                                  $sel1 = "selected='selected'";
                                  break;
                                 case 20:
                                  $sel2 = "selected='selected'";
                                  break;
                                 case 30:
                                  $sel3 = "selected='selected'";
                                  break;
                                 case $this->numrows:
                                  $sel4 = "selected='selected'";
                                  break;
                        }
                        $currQueryString = $this->getQueryString();
                        $limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
                        $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
                                 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
                                 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                                 <option value='10' $sel1>10</option>
                                 <option value='20' $sel2>20</option>
                                 <option value='30' $sel3>30</option> 
                                 <option value='".$totalrecords."' $sel4>All</option>  
                                   </select> Records Per Page
                                </td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
                    }					
            } else {
                    $genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
            }	
		return $genTable;
	}	 
	
	//==================View Settings ==================
    function getResult($id) {
		$query= "select cs.*, cou.countryName, cur.sign from ".TBL_COUNRTYSETTING." as cs inner join ".TBL_COUNTRY_DESCRIPTION." as cou inner join ".TBL_CURRENCY_DETAIL." as cur on 1 and cs.countryId = cou.countryId and cur.id = cs.currencyId and cs.id = '".$id."' and cou.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {				
			return $line = $this->getResultRow($sql);	
		} else {
			redirect('manageCountrySetting.php');	
		}
	}
	
	//=============Add Country Setting====================
    function addCountrySetting($post){
		//echo "<pre>";print_r($post); exit;
		$query = "Select * from ".TBL_COUNRTYSETTING." where countryId = '".$post[country]."'";
		$result = $this->executeQry($query);
		$number = mysql_num_rows($result);
		
		if($number > 0) {
			$_SESSION['SESS_MSG'] = msgSuccessFail("fail","Record already exists.");			
			header("Location:addCountrySetting.php?id=".$post[country]);exit;			
		} else {
			$xmlArr = array();
			$xm = 1;
            
            $query = "insert into ".TBL_COUNRTYSETTING." set countryId = '".$post[country]."', currencyId = '".$post[currency]."', langId = '".implode(',',$post['lang'])."', vat = '".$post[vat]."', customDuty ='".$post[cDuty]."',eoriNo ='".$post['eori']."', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."'";
            $this->executeQry($query);
            $inserted_id = mysql_insert_id();   
            
            $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
            $num = $this->getTotalRow($rst);
            if ($num) {
                while ($line = $this->getResultObject($rst)) {                    
                   $message = 'message_'.$line->id;
                   $query = "insert into ".TBL_COUNRTYSETTINGDESC." set settingId ='".$inserted_id."', langId ='".$line->id."', message = '".$post[$message]."'";
                    
                    $res = $this->executeQry($query);                    

                    $xmlArr[$xm]['query'] = addslashes($query);
                    $xmlArr[$xm]['identification'] = $inserted_id;
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;        		

                    if($res) 
                            $this->logSuccessFail('1',$query);		
                    else 	
                            $this->logSuccessFail('0',$query);

                    $xmlInfoObj = new XmlInfo();
                    $xmlInfoObj->addXmlData($xmlArr);		
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been added successfully.");			
                    //header("Location:addCountrySetting.php");exit;
                }
            }
                        
		}
	}
	
	function editCountrySetting($post) {
//		echo "<pre>";
//		print_r($post);
  
            $query = "update ".TBL_COUNRTYSETTING." set countryId = '".$post[countryId]."', currencyId = '".$post[currencyId]."', langId = '".implode(',',$post['lang'])."', vat = '".$post[vat]."', customDuty ='".$post[customDuty]."',eoriNo ='".$post[eoriNo]."', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."' where id = '".$post['id']."'";
        $this->executeQry($query);        

        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {                   
               $message = 'message_'.$line->id;
               $sql = $this->selectQry(TBL_COUNRTYSETTINGDESC, '1 and settingId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) { 
					$query = "insert into ".TBL_COUNRTYSETTINGDESC." set settingId = '$post[id]', langId = '".$line->id."', message = '".addslashes($post[$message])."' ";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					 $query = "update ".TBL_COUNRTYSETTINGDESC." set  message = '".addslashes($post[$message])."'  where 1 and settingId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}
               
                $_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been updated successfully.");			         
            }
        }                        
		header("Location:manageCountrySetting.php");exit;
	}
	 	
	//Start : Delete Single Value=================================
    function deleteSetting($get) {
    	$xmlArr = array();
        $xm = 1;		
		$sql = "DELETE FROM  ".TBL_COUNRTYSETTING." where id = '$get[id]'";        
		$rst = $this->executeQry($sql);
        
        $sql = "DELETE FROM  ".TBL_COUNRTYSETTINGDESC." where settingId = '$get[id]'";        
		$rst = $this->executeQry($sql);
        
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;	
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}						
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageCountrySetting.php?page=$post[page]&limit=$post[limit]';</script>";
	}
	 	
    
	//==================Status====================	
	function changeStatus($get) {
		$xmlArr = array();
        $xm = 1;	
		$status=$this->fetchValue(TBL_COUNRTYSETTING,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive, 0";
		} else 	{
			$stat= 1;
			$status="Active, 1";
		}
		$sql = "UPDATE ".TBL_COUNRTYSETTING." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "update";
        $xm++;	
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
	
    
	//Start : Mulitple Operation=========================================
	function deleteAllSetting($post){		
		//~ echo "<pre>";
		//~ print_r($post);exit;
		$xmlArr = array();
        $xm = 1;	
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageCountrySetting.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){										
					$sql = "DELETE FROM ".TBL_COUNRTYSETTING." where id = '$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}										
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COUNRTYSETTING." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}

		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COUNRTYSETTING." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageCountrySetting.php?page=$post[page]';</script>";exit;
	}	
		
	
    
	//==============Get list of Country============
 	function getCountry($id = '') { 		 		
		$sql = $this->executeQry("select * from ".TBL_COUNTRY_DESCRIPTION." where 1 and langId = '".$_SESSION['DEFAULTLANGUAGE']."' order by countryName");
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {				
				($id == $line->countryId) ? $sel ="selected='selected'" : $sel ="";				
				$genTable .= '<option value="'.$line->countryId.'" '.$sel.'>'.$line->countryName.'</option>';	
			}
		}		
        return $genTable;
	}	
		
	
	//==============Get list of Currency============
 	function getCurrency($id = '') { 				
		$sql = $this->executeQry("select cd.* from ".TBL_CURRENCY_DETAIL." as cd inner join ".TBL_CURRENCY." as c on cd.id = c.currencyDetailId and c.status = '1' order by currencyName");
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {				
				($id == $line->id) ? $sel ="selected='selected'" : $sel ="";				
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->currencyName.'</option>';	
			}
		}		
        return $genTable;
	}			
	
	
	function getCountryLang($id)
	{
		$langId = $this->fetchValue(TBL_COUNRTYSETTING,'langId',"id = '".$id."'");
		if($langId != '')
		{
			$langIdArr = explode(',',$langId);
		}
		return $langIdArr;
	}
	
	function getlangbox($langIdArr = ''){
		
		$sql = $this->executeQry("select id,languageName,languageCode,languageFlag,isDefault from ".TBL_LANGUAGE." where 1 and status = '1' order by isDefault desc");
		$num = $this->getTotalRow($sql);		
		$gen='';
		if($num > 0) {
			$gen.="<div class='langcontainer'>";
			while($line = $this->getResultObject($sql))
			{
				if($langIdArr != '')
				{
					if(in_array($line->id,$langIdArr))
						$chk='checked="checked"';
					else
						$chk = '';
				}				
				$gen.='<section><input name="lang[]" type="checkbox" '.$chk.' value="'.$line->id.'" class="checklang" checklang="'.$line->languageName.'" />&nbsp;<img src="../files/flag/flagthumb/'.$line->languageFlag.'"/>&nbsp;'.$line->languageName.'</section>';
			}
			$gen.="</div>";
		}
		echo $gen;
	}
	
	
 }
?>	

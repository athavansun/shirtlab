<?php

session_start();

require_once "includes/JSON.php";

class Order extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function orderInformation() {
        $generalObj = new GeneralFunctions();
        $menuObj = new Menu;
        $cond = " 1=1 and paymentMessage = 'Completed'";
        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (" . TBL_ORDER . ".`customerName` LIKE '%$searchtxt%'  OR " . TBL_ORDER . ".`ordReceiptId` LIKE '%$searchtxt%')  ";
        }
        //$query = " SELECT * FROM ".TBL_ORDER." WHERE $cond AND paymentType = 'paypal' ";
        $query = " SELECT * FROM " . TBL_ORDER . " WHERE " . $cond;
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $this->setLimit($_GET[limit]);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : "id";
            $order = $_GET[order] ? $_GET[order] : "DESC";
            $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;
            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);
            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    $user = $this->executeQry(" select firstName, lastName from " . TBL_USER . " where id = '" . $line->userId . "'");
                    $sign = $this->fetchValue(TBL_CURRENCY_DETAIL, "sign", "id = '" . $line->currencyId . "'");
                    $userDetail = $this->fetch_object($user);
                    $amount = html_entity_decode($sign) . " " . $line->ordTotal;

                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    if ($line->orderStatus == 0)
                        $status = "InActive";
                    elseif ($line->orderStatus == 1)
                        $status = "Active";

                    $genTable .= '<tr class="' . $highlight . '" id="' . $line->id . '" >';
                    $genTable .= '<th>';
                    $genTable .= '<input name="chk[]" value="' . $line->id . '" type="checkbox">';
                    $genTable .= '</th>';
                    $genTable .= '<td>' . $i . '</td>';
                    $genTable .= '<td>' . $line->ordReceiptId . '</td>';
                    //$genTable .= '<td>'.stripslashes($userDetail->firstName).'</td>';
                    $genTable .= '<td>' . date(DEFAULTDATEFORMAT, strtotime($line->orderDate)) . '</td>';
                    $genTable .= '<td>' . $this->getPriceWithSign($line->ordTotal, $line->currencyId) . '</td>';
                    //$genTable .= '<td>'.$line->paymentType.'</td>';
                    $genTable .= '<td>' . $line->paymentType . '</td>';
                    $genTable .= '<td>' . $this->getPaymentStatus($line->paymentStatus, $line->id) . '</td>';
                    //$genTable .= '<td>Pending</td>';
                    $genTable .= '<td>' . $this->getOrderStatus($line->orderStatus, $line->id) . '</td>';
                    $genTable .= '<td><a rel="shadowbox;width=1015;height=625" title="" href="viewOrder.php?id=' . base64_encode($line->id) . '"><img src="images/view.png" border="0"></a></td>';
                    if ($menuObj->checkDeletePermission()) {
                        $genTable .= '<td><a href="javascript:void(0);" onClick="if(confirm(\'Are you sure to delete this Resord?\')){window.location.href=\'pass.php?action=manageorder&type=delete&id=' . $line->id . '&page=$page\'}else{}" ><img src="images/drop.png" height="16" width="16" border="0" title="Delete" /></a></td>';
                    } else {
                        $genTable .= '<td>N/A</td>';
                    }
                    if ($line->pdf_generated == 1) {
                        $genTable .= '<td><div style="width:90px;" id="' . $div_id . '"><a id="pdfDown' . $line->ordReceiptId . '" style="display: block;" title="Order Detail" href="download.php?fileName=' . $line->ordReceiptId . '.zip">Download PDF</a></div></td>';
//                                $genTable .= '<td><div style="width:90px;" id="'.$div_id.'"><a id="pdfDown'.$line->ordReceiptId.'" style="display: block;" title="Order Detail" href="download.php?f=a.zip">Download PDF</a></div></td>';
                    } else {
                        $genTable .= '<td><div style="width:90px;" id="' . $div_id . '"><a style="cursor:pointer;" onClick="javascript:generatePDF(\'' . $div_id . '\',\'' . $line->id . '\',\'PDF\')">Generate PDF</a></div></td>';
                    }
                    if ($line->pdf_generated == 1) {
                        $genTable .= '<td><div style="width:30px;" id="' . $div_id . '_d"><a style="cursor:pointer;" onClick="javascript:deleteGenPdf(\'' . $div_id . '\',\'' . $line->id . '\',\'deletePDF\')"><img src="images/drop.png" height="16" width="16" border="0" title="Delete" /></a></div></td>';
                    } else {
                        $genTable .= '<td>N/A</td>';
                    }
                    $genTable.='</tr>';
                    $i++;
                }
            }
            switch ($recordsPerPage) {
                case 10:
                    $sel1 = "selected='selected'";
                    break;
                case 20:
                    $sel2 = "selected='selected'";
                    break;
                case 30:
                    $sel3 = "selected='selected'";
                    break;
                case $this->numrows:
                    $sel4 = "selected='selected'";
                    break;
            }
            $currQueryString = $this->getQueryString();
            $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
            $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='" . $totalrecords . "' $sel4>All</option>  
					   </select> Records Per Page
					   <input type='hidden' name='page' value='" . $currpage . "'>
					</td><td class='page_info' align='center' width='200'>Total Records Found " . $totalrecords . "</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table></div>";
        } else {
            $genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
        }
        return $genTable;
    }

    function getOrderStatus($id, $orderId) {
        $sql = "SELECT tbl1.statusId,tbl2.orderStatus  FROM " . TBL_ORDERSTATUS . " AS tbl1 INNER JOIN " . TBL_ORDERSTATUSDESC . " AS tbl2 ON (tbl1.statusId = tbl2.orderStatusId) WHERE tbl2.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' AND tbl1.isDeleted = '0' GROUP BY tbl1.statusId";
        $query = $this->executeQry($sql);
        $tabel = '';
        $tabel .= '<select name="order_status" id="order_status" onchange="change_orderstatus(' . $orderId . ',this.value);">';
        while ($line = $this->getResultObject($query)) {
            $sel = ($id == $line->statusId) ? "selected" : "";
            $tabel .= '<option value="' . $line->statusId . '" ' . $sel . '>' . $line->orderStatus . '</option>';
        }
        $tabel .= '</select>';
        return $tabel;
    }

    function getPaymentStatus($id, $orderId) {
        $sql = "SELECT tbl1.statusId,tbl2.orderStatus  FROM " . TBL_PAYMENTSTATUS . " AS tbl1 INNER JOIN " . TBL_PAYMENTSTATUSDESC . " AS tbl2 ON (tbl1.statusId = tbl2.orderStatusId) WHERE tbl2.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' AND tbl1.isDeleted = '0' GROUP BY tbl1.statusId";
        $query = $this->executeQry($sql);
        $tabel = '';
        $tabel .= '<select name="payment_status" id="payment_status" onchange="change_paymentstatus(' . $orderId . ',this.value);">';
        while ($line = $this->getResultObject($query)) {
            $sel = ($id == $line->statusId) ? "selected" : "";
            $tabel .= '<option value="' . $line->statusId . '" ' . $sel . '>' . $line->orderStatus . '</option>';
        }
        $tabel .= '</select>';
        return $tabel;
    }

    function deleteValue($get) {
        $sql = " DELETE FROM  " . TBL_ORDER . " WHERE id = '$get[id]' ";
        $rst = $this->executeQry($sql);
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manage-order.php?page=$get[page]&limit=$get[limit]';</script>";
    }

    function changeValueStatus($get) {
        $status = $this->fetchValue(TBL_ORDER, "orderStatus", "1 and id = '$get[id]'");

        if ($status == 1) {
            $stat = 0;
            $status = "InActive";
        } else {
            $stat = 1;
            $status = "Active";
        }

        $query = "update " . TBL_ORDER . " set orderStatus = '$stat' where id = '$get[id]'";
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);
        echo $status;
    }

    function deleteAllValues($post) {
        if (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the action or records, And then submit!!!");
            echo "<script language=javascript>window.location.href='manage-order.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }
        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "DELETE FROM " . TBL_ORDER . " where id = '$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your all selected information has been deleted successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'enableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "UPDATE " . TBL_ORDER . " set orderStatus ='1' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Enable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'disableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "UPDATE " . TBL_ORDER . " set orderStatus ='0' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Disable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        echo "<script language=javascript>window.location.href='manage-order.php?page=$post[page]&limit=$post[limit]';</script>";
    }

    function orderDetail($id) {
        $sitename = SITENAME;
        $generalObj = new GeneralFunctions();
        $id = base64_decode($id);
        $menuObj = new Menu;
        $cond = " 1=1 ";
        $query = " SELECT ORD.* FROM " . TBL_ORDER . " as ORD WHERE $cond AND ORD.id = $id";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            if ($line = $this->getResultObject($sql)) {
                
            }
        }
        return $genTable;
    }

//  orderDetail	END

    function productDetails($orderId) {
        $generalObj = new GeneralFunctions();
        $query = "SELECT ORD.*,p.imageName FROM " . TBL_ORDERDETAIL . " as ORD inner join " . TBL_MAINPRODUCT . " as p on (ORD.productId = p.id) WHERE ORD.orderId = '$orderId' AND p.isDeleted = '0' ORDER BY ORD.id DESC";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            $genTable .= '<ul>';
            while ($line = $this->getResultObject($sql)) {
                $queryImage = "select tmpv.imageName from " . TBL_MAIN_PRODUCT_COLOR . " as tmpc left join " . TBL_MAIN_PRODUCT_VIEW . " as tmpv on tmpc.id=tmpv.color_id and tmpc.product_id=tmpv.product_id and tmpv.product_id='" . $line->productId . "' where tmpv.isDefault='1' order by tmpv.id  asc limit 0,1";
                $rstImage = $this->executeQry($queryImage);
                $lineImage = $this->getResultObject($rstImage);
                $image = __MAINPRODUCTTHUMBPATH__ . $lineImage->imageName;
                $image1 = __MAINPRODUCTTHUMBPATHORIGINAL__ . $lineImage->imageName;
                $IMAGE = getImagePath($image, '', '', __NOIMAGEPATH__, '50', '50');
                $imgOriginalPath = getImagePath($image1, '', '', __NOIMAGEPATH__, '100', '100');
                //print_r($IMAGE);
                //$imgPath   	 = __PRODUCTTHUMB__.$line->imageName;
                //$imgOriginalPath = __PRODUCTPATH__.$line->imageName;
                $total = ($line->unit_price) * ($line->quantity);
                $divLightBox = "divLightBox" . $line->id;
                $genTable .= "<li class='summary-border1' style='clear:both;'>
			 <a rel=\"shadowbox;\" title='' href=\"" . $image1 . "\"><img src=\"" . $image . "\" width='" . $IMAGE[1] . "' height='" . $IMAGE[2] . "' alt='Image' border='0' /></a><br>" . $this->getProductName($line->productId) . "</li><li 
	class='summary-border4'>" . $this->getAttributeName($line->id) . "</li><li 
	class='summary-border4'>" . $generalObj->displayPrice($line->unit_price) . "</li><li class='summary-border7'>" . $line->quantity . "</li><li
	 class='summary-border6'>" . $generalObj->displayPrice($total) . "</li>";
            }
            $genTable .= '</ul>';
        }
        return $genTable;
    }

    function getStateName($id) {
        $statename = $this->fetchValue(TBL_STATE_DESCRIPTION, "stateName", "stateId='" . $id . "' and langId='" . $_SESSION['DEFAULTLANGUAGE'] . "'");
        return $statename;
    }

    function getCountryName($id) {
        $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "countryId='$id' and langId='" . $_SESSION['DEFAULTLANGUAGE'] . "'");
        return $countryName;
    }

    function getEmailId($id) {
        $countryName = $this->fetchValue(TBL_USER, "email", "id='$id'");
        return $countryName;
    }

    function getPhone($id) {
        $countryName = $this->fetchValue(TBL_USER, "phoneNo", "id='$id'");
        return $countryName;
    }

    function getProductName($productId) {
        $productName = $this->fetchValue(TBL_MAIN_PRODUCT_DESCRIPTION, "productName", "productId='$productId' and langId='" . $_SESSION['DEFAULTLANGUAGE'] . "'");
        return $productName;
    }

    function getAttributeName($id) {

        $query = "select * from " . TBL_ORDERDETAIL_ATTRIBUTES . " where orderdetailId = '" . $id . "'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            while ($line = $this->getResultObject($sql)) {
                $queryName = "select attributeValuesName from " . TBL_ATTRIBUTE_VALUES_DESCRIPTION . " where attributeValuesId='" . $line->attributeValueId . "' and langId='" . $_SESSION['DEFAULTLANGUAGE'] . "'";
                $sqlName = $this->executeQry($queryName);
                while ($lineName = $this->getResultObject($sqlName)) {
                    $name .= $lineName->attributeValuesName . "<br>";
                }
            }
            return $name;
        } else {
            return $name = "N/A";
        }
    }

    function changeorderstatus($get) {
        $status = $this->fetchValue(TBL_ORDERSTATUSDESC, "orderStatus", " orderStatusId = '" . $get[order_status] . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
        $userId = $this->fetchValue(TBL_ORDER, "userId", " id ='" . $get[id] . "'");
        $recId = $this->fetchValue(TBL_ORDER, "ordReceiptId", " id ='" . $get[id] . "'");
        $sql = "UPDATE " . TBL_ORDER . " SET orderStatus = '$get[order_status]' WHERE id ='$get[id]' ";
        $query = $this->executeQry($sql);
        $mailObj = new MailFunction();

        $genObj = new GeneralFunctions();
        $langId = $genObj->getLanguageIdUser($userId);
        $mailObj->mailValue("14", $langId, $userId, $recId, $status);
    }

    function changePaymentStatus($get) {
        $status = $this->fetchValue(TBL_PAYMENTSTATUSDESC, "orderStatus", " orderStatusId = '" . $get[paymentstatus] . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
        $userId = $this->fetchValue(TBL_ORDER, "userId", " id ='" . $get[id] . "'");
        $recId = $this->fetchValue(TBL_ORDER, "ordReceiptId", " id ='" . $get[id] . "'");
        $sql = "UPDATE " . TBL_ORDER . " SET paymentStatus = '$get[paymentstatus]' WHERE id ='$get[id]' ";
        $query = $this->executeQry($sql);

        $mailObj = new MailFunction();

        $genObj = new GeneralFunctions();
        $langId = $genObj->getLanguageIdUser($userId);

        $mailObj->mailValue("19", $langId, $userId, $recId, $status);
    }

    function getFinalProductImage($orderId) {
        $imageArr = array();
        $quantity = array();
        $sizeArr = array();
        $unitCost = array();
        $amount = array();
        $ctr = 0;

        $result = $this->executeQry("select id, productId, unit_price from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "'");
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                $imageName = $this->fetchValue(TBL_MAINPRODUCT_VIEW, "viewImage", " mainProdId = '" . $line->productId . "' order by viewId");
                $imageArr[] = '<img src="' . SITE_URL . __MAINPRODTHUMB__ . $imageName . '" alt="Front Image" title="front Image">';

                $unitCost[] = $line->unit_price;

                $sizeResultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $line->id . "'");
                $size_num = $this->num_rows($sizeResultSet);
                if ($size_num > 0) {
                    while ($sizeResult = $this->fetch_object($sizeResultSet)) {
                        $sizeArr[$ctr][] = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $sizeResult->sizeId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");

                        $quantity[$ctr][] = $sizeResult->quantity;
                        $amountArr[$ctr][] = $sizeResult->quantity * $unitCost[$ctr];
                    }
                }
                $ctr++;
            }
        }

        $html = '<table style="width:100%; font-size:12px;">';
        for ($i = 0; $i < $num; $i++) {
            $rowspan = count($sizeArr[$i]);
            //$img ='<td style="width:42%;" rowspan="'.$rowspan.'">'.$imageArr[$i].'</td>';
            for ($j = 0; $j < $rowspan; $j++) {
                $html .= '<tr>';
                if ($j == 0) {
                    $html .='<td style="width:42%;" rowspan="' . $rowspan . '">' . $imageArr[$i] . '</td>';
                }
                $html .='<td style="width:16%;">' . $sizeArr[$i][$j] . '</td>';
                $html .='<td style="width:15%;">' . $quantity[$i][$j] . '</td>';
                if ($j == 0) {
                    $html .='<td style="width:16%;" rowspan="' . $rowspan . '">' . $unitCost[$i] . '</td>';
                }
                $html .='<td style="width:11%;">' . $amountArr[$i][$j] . '</td>';
                $html .= '</tr>';
            }
        }
        $html .= '</table>';
        return $html;
    }

    function getPriceWithSign($valuePoint, $curId) {
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        $finalPrice = $leftPlace . "" . $price . "" . $rightPlace;
        return $finalPrice;
    }

//    function getAddressDetail($oId, $uId, $inId, $deId, $grId, $message) {               
//        $html = $message;
//        $inResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$inId."'");
//        $deResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$deId."'");
//        $grResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$grId."'");
//        $invoice = $this->fetch_object($inResultSet);
//        $delivery = $this->fetch_object($deResultSet);
//        $grafic = $this->fetch_object($grResultSet);        
//        $uresult = $this->executeQry("select firstName, lastName from ".TBL_USER." where id = '".$uId."'");
//        $user = $this->fetch_object($uresult);        
//        $orderResultSet = $this->executeQry("select orderDate, ordReceiptId, paymentType, currencyId, ordSubTotal, discount_ammount, ordTotal  from ".TBL_ORDER." where id ='".$oId."'");
//        $order = $this->fetch_object($orderResultSet);
//        
//        $orderReceipt = $order->ordReceiptId;
//        $orderDate = $order->orderDate;
//        $userName = stripslashes($user->firstName)." ".stripslashes($user->lastName);
//        $inName = stripslashes($invoice->fName)." ".stripslashes($invoice->lName);
//        $deName = stripslashes($delivery->fName)." ".stripslashes($delivery->lName);
//        $grName = stripslashes($grafic->fName)." ".stripslashes($grafic->lName);
//        $orderData = $this->getFinalProductImage($oId);
//        
//        $html = str_replace("[username]",$userName, $html);
//        $html = str_replace("[ORDERID]",$orderReceipt, $html);
//        $html = str_replace("[ORDERDATE]", $orderDate, $html);
//        $html = str_replace("[IN_NAME]", $inName, $html);
//        $html = str_replace("[IN_EMAIL]", stripslashes($invoice->email), $html);
//        $html = str_replace("[IN_ADDRESS]", stripslashes($invoice->address), $html);
//        $html = str_replace("[IN_CITY]", stripslashes($invoice->city), $html);
//        $html = str_replace("[IN_ZIP]", stripslashes($invoice->zip), $html);
//        $html = str_replace("[IN_MOBILE]", stripslashes($invoice->mobile), $html);        
//        $html = str_replace("[DE_NAME]", $deName, $html);
//        $html = str_replace("[DE_EMAIL]", stripslashes($delivery->email), $html);
//        $html = str_replace("[DE_ADDRESS]", stripslashes($delivery->address), $html);
//        $html = str_replace("[DE_CITY]", stripslashes($delivery->city), $html);
//        $html = str_replace("[DE_ZIP]", stripslashes($delivery->zip), $html);
//        $html = str_replace("[DE_MOBILE]", stripslashes($delivery->mobile), $html);        
//        $html = str_replace("[GR_NAME]", $grName, $html);
//        $html = str_replace("[GR_EMAIL]", stripslashes($grafic->email), $html);
//        $html = str_replace("[GR_ADDRESS]", stripslashes($grafic->address), $html);
//        $html = str_replace("[GR_CITY]", stripslashes($grafic->city), $html);
//        $html = str_replace("[GR_ZIP]", stripslashes($grafic->zip), $html);
//        $html = str_replace("[GR_MOBILE]", stripslashes($grafic->mobile), $html);
//        
//        $html = str_replace("[PAYMENTTYPE]", stripslashes($order->paymentType), $html);
//        $html = str_replace("[SUBTOTAL]", stripslashes($this->getPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);
//        $html = str_replace("[COUPANVALUE]", stripslashes($this->getPriceWithSign($order->discount_ammount, $order->currencyId)), $html);
//        $html = str_replace("[GRANDTOTAL]", stripslashes($this->getPriceWithSign($order->ordTotal, $order->currencyId)), $html);
//        $html = str_replace("[LOGOPATH]", SITE_URL."images/logo.jpg", $html);        
//        $html = str_replace("[ITEMS]", $orderData, $html);        
//        
//        return $html;
//    }

    function getDecoTypeById($id) {
        $type = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " id = '" . $id . "'");
        return $type;
    }

    function getProductColorById($id, $viewId) {
        $colorCode = '';
        $resultSet = $this->executeQry("select id, colorCode from " . TBL_ORDER_COLOR . " where orderDetailId = '" . $id . "' and viewId = '" . $viewId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 1;
            while ($line = $this->fetch_object($resultSet)) {
                if ($ctr == $num) {
                    $colorCode .= $line->colorCode;
                } else {
                    $colorCode .= $line->colorCode . ", ";
                }
                $ctr++;
            }
        }
        return $colorCode;
    }

    function getProductComment($orderId) {
        $result = $this->executeQry("select id, productCode, comment from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table>';
            while ($line = $this->fetch_object($result)) {
                $html .= '<tr>';
                $html .= '<td style="width:40px; text-align:center; padding-right:12px;">' . $ctr . '</td>';
                $html .= '<td style="width:170px; padding-right:12px;">' . $line->productCode . '</td>';
                $html .= '<td style="width:550px;">' . $line->comment . '</td>';
                $html .= '</tr>';
                $ctr++;
            }
            $html .='</table>';
        }
        return $html;
    }

    function getProductViewComment($orderId) {
        $result = $this->executeQry("select id, productCode from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table>';
            while ($line = $this->fetch_object($result)) {
                $rs = $this->executeQry("select comment, viewId from " . TBL_ORDER_DECO . " where orderDetailId = '" . $line->id . "' order by viewId");
                $viewNum = $this->num_rows($rs);
                if ($viewNum > 0) {
                    while ($row = $this->fetch_object($rs)) {
                        $viewName = $this->fetchValue(TBL_VIEWDESC, "viewName", " viewId = '" . $row->viewId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
                        $html .= '<tr>';
                        $html .= '<td style="width:40px; text-align:center; padding-right:12px;">' . $ctr . '</td>';
                        $html .= '<td style="width:170px; padding-right:12px;">' . $line->productCode . '</td>';
                        $html .= '<td style="width:105px; padding-right:12px;">' . $viewName . '</td>';
                        $html .= '<td style="width:470px;">' . $row->comment . '</td>';
                        $html .= '</tr>';
                        $ctr++;
                    }
                }
            }
            $html .='</table>';
        }
        return $html;
    }

    function getCouponHtml($couponId) {
        $html = '';
        $couponQuery = $this->executeQry("select * from " . TBL_GIFT_SEND . " where id = '" . $couponId . "'");
        if ($this->num_rows($couponQuery) > 0) {
            $couponResult = $this->fetch_object($couponQuery);
            $html = '<table style="width:100%; font-size:12px;"><tr>';
            $html .='<td style="width:19%; text-align:left; padding-right:20px;"> ' . $couponResult->giftCertificateCode . ' </td>';
            $html .='<td style="width:17%; text-align:left; padding-right:20px;"> ' . $couponResult->couponExpiryDate . ' </td>';
            $html .='<td style="width:33%; text-align:left; padding-right:20px;">' . $couponResult->toEmail . '</td>';
            $html .= '</tr></table>';
        } else {
            $html = '<table style="width:100%; font-size:12px;"><tr>';
            $html .='<td style="width:19%; text-align:left; padding-right:20px;"> NA </td>';
            $html .='<td style="width:17%; text-align:left; padding-right:20px;"> NA </td>';
            $html .='<td style="width:33%; text-align:left; padding-right:20px;"> NA </td>';
            $html .= '</tr></table>';
        }

        return $html;
    }

//    function getAddressDetail($oId, $uId, $inId, $deId, $grId, $message) {        
//        $html = $message;
//        $inResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$inId."'");
//        $deResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$deId."'");
//        $grResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$grId."'");
//        $invoice = $this->fetch_object($inResultSet);
//        $delivery = $this->fetch_object($deResultSet);
//        $grafic = $this->fetch_object($grResultSet);
//        
//        $uresult = $this->executeQry("select firstName, lastName from ".TBL_USER." where id = '".$uId."'");
//        $user = $this->fetch_object($uresult);        
//        $orderResultSet = $this->executeQry("select orderDate, countryId, deliveryId, ordReceiptId, paymentType, currencyId, ordSubTotal, discount_ammount, ordTotal, vat, custom_duty, couponId from ".TBL_ORDER." where id ='".$oId."'");
//        $order = $this->fetch_object($orderResultSet);
//        $orderReceipt = $order->ordReceiptId;
//        
//        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '".$order->countryId."'");
//        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '".$order->countryId."'");
//        $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$order->countryId."'");
//        $subTotalH = "Total of the order excluding VAT : ";
//        $vatH = $vat."% VAT (".$countryName."): ";
//        $customH = "Custom Duty ".$customDuty."% in (".$countryName."): ";
//        $grandTotalH = "Grand Total :";
//        
//        $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id = '".$order->deliveryId."'");
//        if($deliveryTime == 1) {
//            $deliveryTime = $deliveryTime." Day";
//        } else {
//            $deliveryTime = $deliveryTime." Days";
//        }
//        $orderDate = $order->orderDate;
//        $userName = stripslashes($user->firstName)." ".stripslashes($user->lastName);
//        
//        $inTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$invoice->title."' and langId = 1");
//        $inName = $invoice->firstName." ".$invoice->lastName;
//        $inCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$invoice->countryId."' and langId = 1");
//        $inLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$invoice->language."'");
//        
//        $deTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$delivery->title."' and langId = 1");
//        $deName = $delivery->firstName." ".$delivery->lastName;
//        $deCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$delivery->countryId."' and langId = 1");
//        $deLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$delivery->language."'");
//        
//        $grTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$grafic->title."' and langId = 1");
//        $grName = $grafic->firstName." ".$grafic->lastName;
//        $grCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$grafic->countryId."' and langId = 1");
//        $grLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$grafic->language."'");
//        
//        $productComment = $this->getProductComment($oId);        
//        $viewComment = $this->getProductViewComment($oId);
//        $fabricDetail = $this->getFabricDetailById($oId, $order->currencyId, $order->countryId);        
//        $decoData = $this->getDecoDetailById($oId, $order->currencyId, $order->countryId);        
//        $orderData = $this->getFinalProductInfo($oId);
//        $orderTotal = $order->ordTotal + $order->vat + $order->custom_duty;
//        $couponData = $this->getCouponHtml($order->couponId);
//        $ineori = $deeori = $greori = 'NA';
//        
//        if($invoice->eoriNo) {
//            $ineori = $invoice->eoriNo;    
//        }
//        if($delivery->eoriNo) {
//           $deeori = $delivery->eoriNo;
//        }
//        if($grafic->eoriNo) {
//            $greori = $grafic->eoriNo;    
//        }
//        
//        $html = str_replace("[LOGING_YOUR_ACCOUNT]", LANG_LOGING_YOUR_ACCOUNT, $html);
//        $html = str_replace("[username]",$userName, $html);
//        $html = str_replace("[ORDERID]",$orderReceipt, $html);
//        $html = str_replace("[ORDERDATE]", $orderDate, $html);
//        $html = str_replace("[DELIVERY_TIME]", $deliveryTime, $html);
//        
//        $html = str_replace("[IN_FIRMA]", stripslashes($invoice->firma), $html);
//        $html = str_replace("[IN_TITLE]", $inTitle, $html);
//        $html = str_replace("[IN_NAME]", stripslashes($inName), $html);
//        $html = str_replace("[IN_EMAIL]", stripslashes($invoice->email), $html);
//        $html = str_replace("[IN_ADDRESS]", stripslashes($invoice->address), $html);        
//        if($invoice->address1) {            
//            $html = str_replace("[IN_ADDRESS1]", stripslashes($invoice->address1), $html);
//        }
//        $html = str_replace("[IN_ZIP]", stripslashes($invoice->zip), $html);
//        $html = str_replace("[IN_CITY]", stripslashes($invoice->city), $html);
//        $html = str_replace("[IN_COUNTRY]", stripslashes($inCountry), $html);
//        $html = str_replace("[IN_PHONE]", stripslashes($invoice->phoneNo), $html); 
//        $html = str_replace("[IN_MOBILE]", stripslashes($invoice->mobile), $html);
//        $html = str_replace("[IN_EORI]", stripslashes($ineori), $html);
//        $html = str_replace("[IN_LANGUAGE]", stripslashes($inLang), $html);        
//                
//        $html = str_replace("[DE_FIRMA]", stripslashes($delivery->firma), $html);
//        $html = str_replace("[DE_TITLE]", $deTitle, $html);
//        $html = str_replace("[DE_NAME]", stripslashes($deName), $html);        
//        $html = str_replace("[DE_EMAIL]", stripslashes($delivery->email), $html);
//        $html = str_replace("[DE_ADDRESS]", stripslashes($delivery->address), $html);
//        if($delivery->address1) {            
//            $html = str_replace("[DE_ADDRESS1]", stripslashes($delivery->address1), $html);
//        }
//        $html = str_replace("[DE_ZIP]", stripslashes($delivery->zip), $html);
//        $html = str_replace("[DE_CITY]", stripslashes($delivery->city), $html);
//        $html = str_replace("[DE_COUNTRY]", stripslashes($deCountry), $html);
//        $html = str_replace("[DE_PHONE]", stripslashes($delivery->phoneNo), $html);        
//        $html = str_replace("[DE_MOBILE]", stripslashes($delivery->mobile), $html);        
//        $html = str_replace("[DE_EORI]", stripslashes($deeori), $html);
//        $html = str_replace("[DE_LANGUAGE]", stripslashes($deLang), $html); 
//                
//        $html = str_replace("[GR_FIRMA]", stripslashes($grafic->firma), $html);
//        $html = str_replace("[GR_TITLE]", $grTitle, $html);
//        $html = str_replace("[GR_NAME]", stripslashes($grName), $html);   
//        $html = str_replace("[GR_EMAIL]", stripslashes($grafic->email), $html);
//        $html = str_replace("[GR_ADDRESS]", stripslashes($grafic->address), $html);
//        if($grafic->address1) {            
//            $html = str_replace("[GR_ADDRESS1]", stripslashes($grafic->address1), $html);
//        }
//        $html = str_replace("[GR_ZIP]", stripslashes($grafic->zip), $html);
//        $html = str_replace("[GR_CITY]", stripslashes($grafic->city), $html);
//        $html = str_replace("[GR_COUNTRY]", stripslashes($grCountry), $html);
//        $html = str_replace("[GR_PHONE]", stripslashes($grafic->phoneNo), $html);
//        $html = str_replace("[GR_MOBILE]", stripslashes($grafic->mobile), $html);        
//        $html = str_replace("[GR_EORI]", stripslashes($greori), $html);
//        $html = str_replace("[GR_LANGUAGE]", stripslashes($grLang), $html);
//        
//        $html = str_replace("[PAYMENTTYPE]", stripslashes($order->paymentType), $html);
//        $html = str_replace("[SUBTOTAL]", stripslashes($this->getPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);
//        $html = str_replace("[SUBTOTAL_H]", stripslashes($subTotalH), $html);
//        $html = str_replace("[VAT]", stripslashes($this->getPriceWithSign($order->vat, $order->currencyId)), $html);
//        $html = str_replace("[VAT_H]", stripslashes($vatH), $html);
//        $html = str_replace("[CUSTOMDUTY]", stripslashes($this->getPriceWithSign($order->custom_duty, $order->currencyId)), $html);
//        $html = str_replace("[CUSTOMDUTY_H]", stripslashes($customH), $html);
//        $html = str_replace("[COUPANVALUE]", stripslashes($this->getPriceWithSign($order->discount_ammount, $order->currencyId)), $html);
//        $html = str_replace("[GRANDTOTAL]", stripslashes($this->getPriceWithSign($orderTotal, $order->currencyId)), $html);
//        $html = str_replace("[GRANDTOTAL_H]", stripslashes($grandTotalH), $html);
//        $html = str_replace("[LOGOPATH]", SITE_URL."images/logo.jpg", $html);
//        
//        $html = str_replace("[PRODUCT_COMMENT]", $productComment, $html);
//        $html = str_replace("[VIEW_COMMENT]", $viewComment, $html);
//        $html = str_replace("[FABRIC_DETAIL]", $fabricDetail, $html);
//        $html = str_replace("[DECO_DETAIL]", $decoData, $html);        
//        $html = str_replace("[ITEMS]", $orderData, $html);
//        $html = str_replace("[COUPON_DETAIL]", $couponData, $html);
//        $html = str_replace("[TOTAL_AFTER_DISCOUNT]", 'Order Amount After Discount', $html);//TOTAL_AFTER_DISCOUNT
//        $html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getPriceWithSign($order->ordTotal, $order->currencyId)), $html);
//        
//        return $html;        
//    }
   
    function getAddressDetail($oId, $uId, $inId, $deId, $grId, $message) {
        $html = $message;
        $inResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$inId."'");
        $deResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$deId."'");
        $grResultSet = $this->executeQry("select * from ".TBL_ADDRESS." where id = '".$grId."'");
        $invoice = $this->fetch_object($inResultSet);
        $delivery = $this->fetch_object($deResultSet);
        $grafic = $this->fetch_object($grResultSet);
        
        $uresult = $this->executeQry("select firstName, lastName from ".TBL_USER." where id = '".$uId."'");
        $user = $this->fetch_object($uresult);        
        $orderResultSet = $this->executeQry("select orderDate, countryId, deliveryId, ordReceiptId, paymentType, currencyId, ordSubTotal, discount_ammount, ordTotal, vat, custom_duty, couponId from ".TBL_ORDER." where id ='".$oId."'");
        $order = $this->fetch_object($orderResultSet);
        $orderReceipt = $order->ordReceiptId;
        
        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '".$order->countryId."'");
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '".$order->countryId."'");
        $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$order->countryId."'");
        $subTotalH = "Total of the order excluding VAT : ";
        $vatH = $vat."% VAT (".$countryName."): ";
        $customH = "Custom Duty ".$customDuty."% in (".$countryName."): ";
        $grandTotalH = "Grand Total :";
        
        $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id = '".$order->deliveryId."'");
        if($deliveryTime == 1) {
            $deliveryTime = $deliveryTime." Day";
        } else {
            $deliveryTime = $deliveryTime." Days";
        }
        $orderDate = $order->orderDate;
        $userName = stripslashes($user->firstName)." ".stripslashes($user->lastName);
        
        $inTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$invoice->title."' and langId = 1");
        $inName = $invoice->firstName." ".$invoice->lastName;
        $inCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$invoice->countryId."' and langId = 1");
        $inLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$invoice->language."'");
        
        $deTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$delivery->title."' and langId = 1");
        $deName = $delivery->firstName." ".$delivery->lastName;
        $deCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$delivery->countryId."' and langId = 1");
        $deLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$delivery->language."'");
        
        $grTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '".$grafic->title."' and langId = 1");
        $grName = $grafic->firstName." ".$grafic->lastName;
        $grCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '".$grafic->countryId."' and langId = 1");
        $grLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '".$grafic->language."'");
        
        $productComment = $this->getProductComment($oId);        
        $viewComment = $this->getProductViewComment($oId);
        $fabricDetail = $this->getFabricDetailById($oId, $order->currencyId, $order->countryId);        
        $decoData = $this->getDecoDetailById($oId, $order->currencyId, $order->countryId);        
        $orderData = $this->getFinalProductInfo($oId);
        $orderTotal = $order->ordTotal + $order->vat + $order->custom_duty;
        $couponData = $this->getCouponHtml($order->couponId);
        $ineori = $deeori = $greori = 'NA';
        
        if($invoice->eoriNo) {
            $ineori = $invoice->eoriNo;    
        }
        if($delivery->eoriNo) {
           $deeori = $delivery->eoriNo;
        }
        if($grafic->eoriNo) {
            $greori = $grafic->eoriNo;    
        }
        
        
        $html = str_replace("[LOGING_YOUR_ACCOUNT]", LANG_LOGING_YOUR_ACCOUNT, $html);
        $html = str_replace("[username]",$userName, $html);
        $html = str_replace("[ORDERID]",$orderReceipt, $html);
        $html = str_replace("[ORDERDATE]", $orderDate, $html);
        $html = str_replace("[DELIVERY_TIME]", $deliveryTime, $html);
        
        $html = str_replace("[IN_FIRMA]", stripslashes($invoice->firma), $html);
        $html = str_replace("[IN_TITLE]", $inTitle, $html);
        $html = str_replace("[IN_NAME]", stripslashes($inName), $html);
        $html = str_replace("[IN_EMAIL]", stripslashes($invoice->email), $html);
        $html = str_replace("[IN_ADDRESS]", stripslashes($invoice->address), $html);        
        if($invoice->address1) {            
            $html = str_replace("[IN_ADDRESS1]", stripslashes($invoice->address1), $html);
        }
        $html = str_replace("[IN_ZIP]", stripslashes($invoice->zip), $html);
        $html = str_replace("[IN_CITY]", stripslashes($invoice->city), $html);
        $html = str_replace("[IN_COUNTRY]", stripslashes($inCountry), $html);
        $html = str_replace("[IN_PHONE]", stripslashes($invoice->phoneNo), $html); 
        $html = str_replace("[IN_MOBILE]", stripslashes($invoice->mobile), $html);         
        $html = str_replace("[IN_EORI]", stripslashes($ineori), $html);
        $html = str_replace("[IN_LANGUAGE]", stripslashes($inLang), $html);        
                
        $html = str_replace("[DE_FIRMA]", stripslashes($delivery->firma), $html);
        $html = str_replace("[DE_TITLE]", $deTitle, $html);
        $html = str_replace("[DE_NAME]", stripslashes($deName), $html);        
        $html = str_replace("[DE_EMAIL]", stripslashes($delivery->email), $html);
        $html = str_replace("[DE_ADDRESS]", stripslashes($delivery->address), $html);
        if($delivery->address1) {            
            $html = str_replace("[DE_ADDRESS1]", stripslashes($delivery->address1), $html);
        }
        $html = str_replace("[DE_ZIP]", stripslashes($delivery->zip), $html);
        $html = str_replace("[DE_CITY]", stripslashes($delivery->city), $html);
        $html = str_replace("[DE_COUNTRY]", stripslashes($deCountry), $html);
        $html = str_replace("[DE_PHONE]", stripslashes($delivery->phoneNo), $html);        
        $html = str_replace("[DE_MOBILE]", stripslashes($delivery->mobile), $html);        
        $html = str_replace("[DE_EORI]", stripslashes($deeori), $html);
        $html = str_replace("[DE_LANGUAGE]", stripslashes($deLang), $html); 
                
        $html = str_replace("[GR_FIRMA]", stripslashes($grafic->firma), $html);
        $html = str_replace("[GR_TITLE]", $grTitle, $html);
        $html = str_replace("[GR_NAME]", stripslashes($grName), $html);   
        $html = str_replace("[GR_EMAIL]", stripslashes($grafic->email), $html);
        $html = str_replace("[GR_ADDRESS]", stripslashes($grafic->address), $html);
        if($grafic->address1) {            
            $html = str_replace("[GR_ADDRESS1]", stripslashes($grafic->address1), $html);
        }
        $html = str_replace("[GR_ZIP]", stripslashes($grafic->zip), $html);
        $html = str_replace("[GR_CITY]", stripslashes($grafic->city), $html);
        $html = str_replace("[GR_COUNTRY]", stripslashes($grCountry), $html);
        $html = str_replace("[GR_PHONE]", stripslashes($grafic->phoneNo), $html);
        $html = str_replace("[GR_MOBILE]", stripslashes($grafic->mobile), $html);
        $html = str_replace("[GR_EORI]", stripslashes($greori), $html);
        $html = str_replace("[GR_LANGUAGE]", stripslashes($grLang), $html);
        
        $html = str_replace("[PAYMENTTYPE]", stripslashes($order->paymentType), $html);
        $html = str_replace("[SUBTOTAL]", stripslashes($this->getPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);
        $html = str_replace("[SUBTOTAL_H]", stripslashes($subTotalH), $html);
        $html = str_replace("[VAT]", stripslashes($this->getPriceWithSign($order->vat, $order->currencyId)), $html);
        $html = str_replace("[VAT_H]", stripslashes($vatH), $html);
        $html = str_replace("[CUSTOMDUTY]", stripslashes($this->getPriceWithSign($order->custom_duty, $order->currencyId)), $html);
        $html = str_replace("[CUSTOMDUTY_H]", stripslashes($customH), $html);
        //$html = str_replace("[COUPANVALUE]", stripslashes($this->getCoupanPriceWithSign($order->discount_ammount, $order->currencyId)), $html);
        $html = str_replace("[COUPANVALUE]", stripslashes($this->getPriceSign($order->discount_ammount, $order->currencyId)), $html);
        $html = str_replace("[GRANDTOTAL]", stripslashes($this->getPriceWithSign($orderTotal, $order->currencyId)), $html);
        $html = str_replace("[GRANDTOTAL_H]", stripslashes($grandTotalH), $html);
        $html = str_replace("[LOGOPATH]", SITE_URL."images/logo.jpg", $html);
        
        $html = str_replace("[PRODUCT_COMMENT]", $productComment, $html);
        $html = str_replace("[VIEW_COMMENT]", $viewComment, $html);
        $html = str_replace("[FABRIC_DETAIL]", $fabricDetail, $html);
        $html = str_replace("[DECO_DETAIL]", $decoData, $html);        
        $html = str_replace("[ITEMS]", $orderData, $html);
        $html = str_replace("[COUPON_DETAIL]", $couponData, $html);
        $html = str_replace("[TOTAL_AFTER_DISCOUNT]", 'Order Amount After Discount', $html);//TOTAL_AFTER_DISCOUNT
        $html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getPriceWithSign($order->ordTotal, $order->currencyId)), $html);
        
        return $html;        
    }
    
    function getPriceSign($price, $curId) {
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";        
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        $final_price = $leftPlace . "" . $price . "" . $rightPlace;
        return $final_price;
    }
    
    function getCoupanPriceWithSign($valuePoint, $curId) {
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        if ($curId == 1) {
            $price = $valuePoint;
        } else {
            $price = $valuePoint * $currencyValue;
        }

        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        $finalPrice = $leftPlace . "" . $price . "" . $rightPlace;
        return $finalPrice;
    }

    function getFabricDetailById($orderId, $curId, $countryId) {
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", "countryId = '" . $countryId . "'");
        $query = "select rawProductId, productCode, productId, quantity, base_price, quantitySurcharge, deliverySurcharge, deliverySign from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table>';
            while ($line = $this->fetch_object($resultSet)) {
                $qualityId = $this->fetchValue(TBL_MAINPRODUCT, "qualityId", " id = '" . $line->productId . "'");
                $quality = $this->fetchValue(TBL_FABRICCHARGE, "fQuality", " id = '" . $qualityId . "'");

                $price = $line->base_price + $line->plainSurcharge;
                $cDuty = ($price * $customDuty) / 100;
                $basePrice = $price + $cDuty;
                $fabric_price = $this->getVPPrice($basePrice, $curId);

                $deSurcharge = $this->getVPPrice($line->deliverySurcharge, $curId);
                $qtySurcharge = $this->getVPPrice($line->quantitySurcharge, $curId);
                $totalVP = $line->base_price + $line->quantitySurcharge;
                $total = $this->getVPPrice($totalVP, $curId);
                $total = $fabric_price + $qtySurcharge;
                if ($line->deliverySign == 0) {
                    $total = $total - $deSurcharge;
                } else if ($line->deliverySign == 2) {
                    $total = $total + $deSurcharge;
                }
                $total = $total * $line->quantity;

                $html .= '<tr>';
                $html .= '<td style="width:40px; text-align:center; padding-right:12px;">' . $ctr . '</td>';
                $html .= '<td style="width:170px; padding-right:12px;">' . $line->productCode . '</td>';
                $html .= '<td style="width:295px; padding-right:12px;">' . $quality . '</td>';
                $html .= '<td style="width:165px; padding-right:12px;">' . $line->quantity . '</td>';
                $html .= '<td style="width:108px; padding-right:12px;">' . $fabric_price . '</td>';
                $html .= '<td style="width:108px; padding-right:12px;">' . $deSurcharge . '</td>';
                $html .= '<td style="width:108px; padding-right:12px; color:#ffffff;">' . $qtySurcharge . '</td>';
                $html .= '<td style="width:108px;">' . $total . '</td>';
                $html .= '</tr>';
                $ctr++;
            }
            $html .='</table>';
        }
        return $html;
    }
    

    function getDecoDetailById($orderId, $currencyId, $countryId) {
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", "countryId = '" . $countryId . "'");
        $jsonObj = new JSON();
        $decoPrice = array();
        $html = '';
        $query = "select id, productCode, productId from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 0;
            while ($line = $this->fetch_object($resultSet)) {
                $decoImage = array();
                $dataArr = $this->fetchValue(TBL_MAINPRODUCT, "dataArr", "id = {$line->productId}");
                $deco = $jsonObj->decode($dataArr);
                foreach ($deco as $obj) {
                    $viewId = $this->fetchValue(TBL_VIEWDESC, "viewId", "viewName = '" . $obj->ViewName . "'");
                    foreach ($obj->contentArray as $img) {
                        $arr = explode("http://", $img);
                        $decoImg = str_replace("original", "thumb", $arr[1]);
                        $decoImage[$viewId][] = "http://" . strip_tags($decoImg);
                    }
                }
                $html .= $this->getPantoneNo($line->id, $decoImage, $ctr, $currencyId, $line->productCode, $customDuty);
            }
        }
        return $html;
    }

//    function getDecoPriceInfo($orderDetailId, $currencyId) {
//        $price = (object)$price;
//        $query = "select sum(price) as price, sum(surcharge) as surcharge from ".TBL_ORDER_DECO." where orderDetailId = '".$orderDetailId."' group by viewId";
//        $resultSet = $this->executeQry($query);
//        $num = $this->num_rows($resultSet);
//        if($num > 0) {
//            while($line = $this->fetch_object($resultSet)) {
//                $price->unitPrice = $this->getVPPrice($line->price, $currencyId);
//                $price->surcharge = $this->getVPPrice($line->surcharge, $currencyId);
//                $price->total = $this->getVPPrice(($line->price + $line->surcharge), $currencyId);
//            }
//        }
//        return $price;
//    }

    function getDecoPriceInfo($orderDetailId, $viewId, $currencyId, $customDuty = 0) {
        $obj = (object) $obj;
        $price = 0;
        $surcharge = 0;

        $query = "select price, surcharge, sign from " . TBL_ORDER_DECO . " where orderDetailId = '" . $orderDetailId . "' and viewId = '" . $viewId . "'";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($line = $this->fetch_object($resultSet)) {
                $price += $line->price;
                if ($line->sign == 0) {
                    $surcharge -= $line->surcharge;
                } else if ($line->sign == 2) {
                    $surcharge += $line->surcharge;
                }
            }
        }

        $cPrice = ($price * $customDuty) / 100;
        $price = $price + $cPrice;

        $cSurcharge = ($surcharge * $customDuty) / 100;
        $surcharge = $surcharge + $cSurcharge;

        $obj->unitPrice = $this->getVPPrice($price, $currencyId);
        $obj->surcharge = $this->getVPPrice($surcharge, $currencyId);
        $obj->total = $this->getVPPrice(($price + $surcharge), $currencyId);

        return $obj;
    }

    private $sNumber = 0;

    function getPantoneNo($orderDetailId, $decoImage, $sNo, $currencyId, $productCode, $customDuty = 0) {
        $html = '';
        $formatArr = array();
        $checkId = 0;

        $result = $this->executeQry("select * from " . TBL_ORDER_DECO . " where orderDetailId = {$orderDetailId} order by viewId");
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                if ($checkId != $line->viewId) {
                    $checkId = $line->viewId;
                    $this->sNumber++;
                    $imgHtml = '';
                    foreach ($decoImage[$line->viewId] as $img) {
                        $imgHtml .= '<img src="' . $img . '" alt="Deco Image"></img>';
                        $imgHtml .= ' ';
                    }
                    //$priceObj = $this->getDecoPriceInfo($orderDetailId, $currencyId);
                    $priceObj = $this->getDecoPriceInfo($orderDetailId, $line->viewId, $currencyId, $customDuty);
                    $html .='<tr>';
                    $html .='<td style="width:55px; text-align:center; padding-right:12px;">' . $this->sNumber . '</td>';
                    $html .='<td style="width:170px; text-align:left; padding-right:12px;">' . $productCode . '</td>';
                    $html .='<td style="width:130px; padding-right:12px;">' . $imgHtml . '</td>';
                    $html .='<td style="width:55px; padding-right:12px;">' . $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = {$line->viewId}") . '</td>';
                    $html .='<td style="width:80px; padding-right:12px;">' . $this->getDecoTypeById($line->type) . '</td>';
                    $html .= '<td style="width:273px; padding-right:12px;">' . $this->getProductColorById($line->orderDetailId, $line->viewId) . '</td>';
                    $html .='<td style="width:108px; text-align:left; padding-right:12px;">' . $priceObj->unitPrice . '</td>';
                    $html .='<td style="width:108px; text-align:left; padding-right:12px;">' . $priceObj->surcharge . '</td>';
                    $html .='<td style="width:108px; text-align:left; padding-right:12px;">' . $priceObj->total . '</td>';
                }
            }
        }
        return $html;
    }

    function getSizeQuantityById($orderDetailId) {
        $obj = (object) $obj;
        $str = '';
        $quantity = 0;
        //echo "select sizeId, quantity from ".TBL_ORDER_SIZE." where orderDetailId = '".$orderDetailId."'"."<br />";
        $resultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $orderDetailId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 1;
            while ($line = $this->fetch_object($resultSet)) {
                $quantity += $line->quantity;
                $sizeName = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $line->sizeId . "'");
                if ($num != $ctr) {
                    $str .= $sizeName . ":" . $line->quantity;
                } else {
                    $str .= $sizeName . ":" . $line->quantity;
                }
                $ctr++;
            }
        }
        $obj->quantity = $quantity;
        $obj->size = $str;

        return $obj;
    }

    function getViewImage($id) {
        $image = '';
        $imageResult = $this->executeQry("select id, viewImage from " . TBL_MAINPRODUCT_VIEW . " where mainProdId = '" . $id . "' order by viewId");
        $imageNum = $this->num_rows($imageResult);
        if ($imageNum > 0) {
            while ($imageRow = $this->fetch_object($imageResult)) {
                $image .= '<img height="70px"; width = "55px"; src="' . SITE_URL . __MAINPRODTHUMB__ . $imageRow->viewImage . '" alt="Front Image" title="front Image">';
            }
        }
        return $image;
    }

    function getFinalProductInfo($orderId) {
        $imageArr = array();
        $productCode = array();
        $quantity = array();
        $sizeArr = array();
        $unitCost = array();
        $finalCost = array();
        $amount = array();
        $ctr = 0;

        $currencyId = $this->fetchValue(TBL_ORDER, "currencyId", " id = '" . $orderId . "'");
        $result = $this->executeQry("select id, productCode, productId, unit_price, final_price from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id asc");
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                $productCode[] = $line->productCode;
                $imageName = $this->fetchValue(TBL_MAINPRODUCT_VIEW, "viewImage", " mainProdId = '" . $line->productId . "' order by viewId");
                //$imageArr[] = '<img src="'.SITE_URL.__MAINPRODTHUMB__.$imageName.'" alt="Front Image" title="front Image">';                
                $imageArr[] = $this->getViewImage($line->productId);

                $sizeObj[] = $this->getSizeQuantityById($line->id);

                $unitCost[] = $this->getVPPrice($line->unit_price, $currencyId);
                $finalCost[] = $this->getVPPrice($line->final_price, $currencyId);
                $sizeResultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $line->id . "'");
                $size_num = $this->num_rows($sizeResultSet);
                if ($size_num > 0) {
                    while ($sizeResult = $this->fetch_object($sizeResultSet)) {
                        $sizeArr[$ctr][] = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $sizeResult->sizeId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");

                        $quantity[$ctr][] = $sizeResult->quantity;
                        $amountArr[$ctr][] = $finalCost[$ctr];
                    }
                }
                $ctr++;
            }
        }

        $html = '<table style="width:100%; font-size:12px;" class="deco_detail">';
        for ($i = 0; $i < $num; $i++) {
            $rowspan = count($sizeArr[$i]);
            for ($j = 0; $j < $rowspan; $j++) {
                $html .= '<tr>';
                if ($j == 0) {
                    $html .='<td style="width:4%; text-align:right; padding-right:25px;" rowspan="' . $rowspan . '">' . ($i + 1) . '</td>';
                    $html .='<td style="width:15%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $productCode[$i] . '</td>';
                    $html .='<td style="width:35%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $imageArr[$i] . '</td>';
                    $html .='<td style="width:16%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $sizeObj[$i]->size . '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $sizeObj[$i]->quantity . '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $unitCost[$i] . '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $amountArr[$i][$j] . '</td>';
                }

                $html .= '</tr>';
            }
        }
        $html .= '</table>';

        return $html;
    }

    //delete this code
    function getDataArr($orderId) {
        $jsonObj = new JSON();
        $result = $this->executeQry("select id, productId from " . TBL_ORDERDETAIL . " where orderId = {$orderId}");
        $num = $this->num_rows($result);
        if ($num > 0) {
            //echo "<pre>";
            $ctr = 0;
            while ($line = $this->fetch_object($result)) {
                //echo $line->productId;
                $ctr++;
                $decoImage = array();
                $dataArr = $this->fetchValue(TBL_MAINPRODUCT, "dataArr", "id = {$line->productId}");
                $deco = $jsonObj->decode($dataArr);
                foreach ($deco as $obj) {
                    $viewId = $this->fetchValue(TBL_VIEWDESC, "viewId", "viewName = '" . $obj->ViewName . "'");
                    foreach ($obj->contentArray as $img) {
                        $arr = explode("http://", $img);
                        //$decoImage[$viewId][] = "http://".strip_tags($arr[1]);

                        $decoImg = str_replace("original", "thumb", $arr[1]);
                        $decoImage[$viewId][] = "http://" . strip_tags($decoImg);
                    }
                }
                $html .= $this->getPantoneNo($line->id, $decoImage, $ctr);
            }
            return $html;
        }
    }

    private static $ctr = 1;

    //===========END   testing=================


    function getOrderDetail($orderId) {
        $id = base64_decode($orderId);
        $langId = $this->fetchValue(TBL_LANGUAGE, "id", "isDefault = '1'");
        //$rst = $this->selectQry(TBL_MAILSETTING,"mailTypeId='11' AND langId='".$_SESSION[DEFAULTLANGUAGEID]."'",'0','0');
        $rst = $this->selectQry(TBL_MAILSETTING, "mailTypeId='11' AND langId='" . $langId . "'", '0', '0');
        $query = $this->getResultRow($rst);

        $mailContent = stripslashes($query['mailContaint']);
        $resultSet = $this->selectQry(TBL_ORDER, "id='" . $id . "'", '0', '0');
        $result = $this->getResultObject($resultSet);
        $orderDetail = $this->getAddressDetail($result->id, $result->userId, $result->invoiceAddId, $result->deliveryAddId, $result->graficAddId, $mailContent);

        return $orderDetail;
    }

    function getVPPrice($valuePoint, $curId) {
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        return $total = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
    }

}

?>	
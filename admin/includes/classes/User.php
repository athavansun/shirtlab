<?php 
session_start();
class User extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }



	function userFullInformation() {
		$menuObj =  new Menu;
		$cond = " 1=1 "; 
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (".TBL_USER.".`firstName` LIKE '%$searchtxt%'  OR ".TBL_USER.".`lastName` LIKE '%$searchtxt%' OR ".TBL_USER.".`email` LIKE '%$searchtxt%')  ";
		}
		$query = " SELECT `id`, firma, `firstName`, `lastName`, `countryId`, `status`, `registerDate` FROM ".TBL_USER." WHERE $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"lastName";
		    $order = $_GET[order]? $_GET[order]:"DESC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
/*					echo "<pre>";
					print_r($line);
					echo "</pre>";*/
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "InActive";
					elseif($line->status==1)
						$status = "Active";
					else
						$status = "Banned";
						
					$genTable .= '<tr>
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.stripslashes($line->firma).'</td>
									<td>'.stripslashes($line->lastName == ''? $line->firstName:$line->lastName).'</td>
									<td>'.$this->fetchValue(TBL_COUNTRY_DESCRIPTION,'countryName',"countryId = '".$line->countryId."'").'</td>
									<td>'.substr(date('ymd\'hisA', strtotime($line->registerDate)), 0, -1).'</td>
									<td>'.date(DEFAULTDATEFORMAT,strtotime($line->registerDate)).'</td>
									<td><div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'manageuser\')">'.$status.'</div></td>
									<td>';
					$genTable .= '<a  rel="shadowbox;width=705;height=490" title="'.stripslashes($line->fullName).'" href="viewUser.php?uid='.base64_encode($line->id).'"><img src="images/view.png" alt="View" width="16" height="16" border="0" /></a>';	
					$genTable .= '</td><td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit"  href="editUser.php?uid='.base64_encode($line->id).'&page='.$page.'" title="Edit">Edit</a>';
					}	
					$genTable .= '</td><td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=manageuser&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
		}	
		return $genTable;
	}
	


	function getUserById($id){
		$rst = $this->selectQry(TBL_USER,"id='$id'","","");		
		if($this->getTotalRow($rst)){
			return $this->getResultRow($rst);	
		}else{
			header("Location:manageUser.php");exit;
		}
	}
	
 function getStateName1($id){
		 $statename = $this->fetchValue(TBL_STATE,"Region","RegionID='".$id."'");
		return $statename;
	}

	function getCountryName1($id){
		$countryName = $this->fetchValue(TBL_COUNTRY,"Country","CountryID='$id'");
		return $countryName;
	}
	
	function getStateName($id){
		$statename = $this->fetchValue(TBL_STATE_DESCRIPTION,"stateName"," stateId='$id' and langId = '1'");
		return $statename;
	}
	
	function getCountryName($id){
		$countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION,"countryName","countryId='$id' and langId = '1'");
		return $countryName;
	}

	function isUserEmailExists($email){
		$email = trim($email);
		$rst = $this->selectQry(TBL_USER,"email='$email' ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}


	function getTitleInDropdown($tid = ''){		
		$rst = $this->executeQry("Select t.*, td.titleName from ".TBL_TITLE." as t, ".TBL_TITLEDESC." as td where t.id = td.Id and t.status = '1' and t.isDeleted = '0' and td.langId = '".$_SESSION[DEFAULTLANGUAGEID]."'");	
		$preTable = "<option value=''> Select Title </option>";
		while($row = $this->getResultObject($rst)){
			if($tid == $row->id){ $sel = "selected = 'selected'";}else{ $sel="";}
			$preTable .= "<option value='$row->id' $sel>".$row->titleName."</option>";			
		}
		return $preTable;
	}
	
	///// Add New User ////////

	function addNewUser($post){
	  /*echo "<pre>";
	    print_r($post);
	    echo "<pre>";exit;	*/
		
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_USER;	
		$this->field_values['firma'] = $post['firma'];
		$this->field_values['title'] = $post['title'];
		$this->field_values['firstName'] = $post['firstName'];
		$this->field_values['lastName'] = $post['lastName'];
		$this->field_values['language'] = $post['language'];
		$this->field_values['email'] = $post['email'] ;
		$this->field_values['address'] = $post['address'] ;
		$this->field_values['address1'] = $post['address1'] ;
		$this->field_values['zip'] = $post['postCode'] ;
		$this->field_values['city'] = $post['city'] ;
		$this->field_values['countryId'] = $post['countryId'];
		$this->field_values['phoneNo'] = $post['phoneNo'] ;
		$this->field_values['mobile'] = $post['mobile'] ;
		$this->field_values['birthday'] = $post['birthday'] ;
		$this->field_values['language'] = $post['language'] ;
		$this->field_values['userPassword'] = $this->encrypt_password($post['password']);
		$this->field_values['hash'] = md5($post['email'].":".$post['password']);
		$this->field_values['status'] = $post['status'] ;
		
//		$this->field_values['stateId'] = $post['stateId'] ;		
//		$this->field_values['faxNo'] = $post['faxNo'] ;
//		$this->field_values['streetAdd'] = $post['streetAdd'] ;	
//		$this->field_values['houseNo'] = $post['houseNo'] ;

		$this->field_values['registerDate'] = date("Y-m-d H:i:s");
		$this->field_values['registerIP'] = $_SERVER['REMOTE_ADDR'];
		$res = $this->insertQry();
		$insertId = $res[1];
		if($insertId){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add record.");
		}	
		header("Location:addUser.php");exit;
	}

		///// Add New User ////////

	function editRecord($post){
		//echo "<pre>";  print_r($post);exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_USER;	
		$this->field_values['firma'] = $post['firma'];
		$this->field_values['title'] = $post['title'];
		$this->field_values['firstName'] = $post['firstName'];
		$this->field_values['lastName'] = $post['lastName'];
		$this->field_values['language'] = $post['language'];
		$this->field_values['email'] = $post['email'] ;
		$this->field_values['address'] = $post['address'] ;
		$this->field_values['address1'] = $post['address1'] ;
		$this->field_values['zip'] = $post['postCode'] ;
		$this->field_values['city'] = $post['city'] ;
		$this->field_values['countryId'] = $post['countryId'];
		$this->field_values['phoneNo'] = $post['phoneNo'] ;
		$this->field_values['mobile'] = $post['mobile'] ;
		$this->field_values['birthday'] = $post['birthday'] ;
		$this->field_values['language'] = $post['language'] ;
		$this->field_values['status'] = $post['status'] ;		
		if($post['password']){
		$this->field_values['userPassword'] = $this->encrypt_password($post['password']);
		}
		$this->condition  = "id = '$post[id]'";
		
		$res = $this->updateQry();
		if($res){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
		}
		header("Location:manageUser.php");exit;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_USER,"status","1 and id = '$get[id]'");

		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}

		$query = "update ".TBL_USER." set status = '$stat', activationCode='', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query))
			$this->logSuccessFail('1',$query);
		else
			$this->logSuccessFail('0',$query);
		echo $status;
	}

	function deleteValue($get){
			$sql = " DELETE FROM  ".TBL_USER." WHERE id = '$get[id]' ";
			$rst = $this->executeQry($sql);
			if($rst){
					$this->logSuccessFail('1',$query);		
				}else{ 	
					$this->logSuccessFail('0',$query);
				}
			$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
			echo "<script language=javascript>window.location.href='manageUser.php?page=$get[page]&limit=$get[limit]';</script>";
	}



	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records, And then submit!!!");
			echo "<script language=javascript>window.location.href='manageUser.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql = "DELETE FROM ".TBL_USER." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="UPDATE ".TBL_USER." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="UPDATE ".TBL_USER." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'bannedall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="UPDATE ".TBL_USER." set status ='2', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageUser.php?page=$post[page]&limit=$post[limit]';</script>";
	}	
	
	
	
	function encrypt_password($plain) {
		$password = '';
		for ($i=0; $i<10; $i++) {
			$password .= $this->tep_rand();
		}
		$salt = substr(md5($password), 0, 2);
		$password = md5($salt . $plain) . ':' . $salt;
		return $password;
	}
	
	function tep_rand($min = null, $max = null) {
	    static $seeded;
    	if (!$seeded) {
	      mt_srand((double)microtime()*1000000);
    	  $seeded = true;
    	}
	}

	



}// End Class
?>	

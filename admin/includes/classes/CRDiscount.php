<?php

session_start();

class CRDiscount extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    //=====================================================================
    //FABRIC PAGE 
    //=====================================================================
    //=============View Fabric=============================================

    function valCRDiscount() {
        $cond = "1 and f.id = fd.fabricId and fd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {

            $searchtxt = $_REQUEST['searchtxt'];

            $cond .= " AND fd.fabricName LIKE '%$searchtxt%' OR f.type LIKE  '%$searchtxt%'";
        }
        $query = "select f.*, fd.fabricName from " . TBL_FABRIC . " as f inner join " . TBL_FABRICDESC . " as fd on " . $cond;

        $sql = $this->executeQry($query);

        $num = $this->getTotalRow($sql);

        $menuObj = new Menu();

        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;

        if ($num > 0) {



            //-------------------------Paging------------------------------------------------			

            $paging = $this->paging($query);

            $this->setLimit($_GET['limit']);


            $this->setStyle("redheading");

            $this->setActiveStyle("smallheading");

            $this->setButtonStyle("boldcolor");

            $currQueryString = $this->getQueryString();

            $this->setParameter($currQueryString);

            $totalrecords = $this->numrows;

            $currpage = $this->getPage();

            $totalpage = $this->getNoOfPages();

            $pagenumbers = $this->getPageNo();

            $orderby = $_GET[orderby] ? $_GET[orderby] : "id";

            $order = $_GET[order] ? $_GET[order] : "ASC";



            $query .= " ORDER BY $orderby $order ";


            $rst = $this->executeQry($query);

            $row = $this->getTotalRow($rst);

            $genTable = '';

            if ($row > 0) {

                $i = 1;

                while ($line = $this->getResultObject($rst)) {

                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";

                    $div_id = "status" . $line->id;

                    $status = ($line->status) ? "Active" : "Inactive";

                    $genTable .= '<tr>
						<td>' . stripslashes($line->fabricName) . '</td>';


                    $arr = isset($_POST['errors']["arr_" . $line->id]) ? $_POST['errors']["arr_" . $line->id] : "";
                    $value = isset($_POST['cr_' . $line->id]) ? $_POST['cr_' . $line->id] : $this->fetchValue(TBL_CRDISCOUNT, 'value', "fabric_gruop_id=" . $line->id);

                    $genTable .='<td> <input type="text" name = "cr_' . $line->id . '" value="' . $value . '" placeholder="0.00"/><span class ="error">' . $arr . '</span></td>';
                    $genTable.='</tr>';


                    $i++;
                }



                $currQueryString = $this->getQueryString();

                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
            }
        } else {

            $genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
        }

        return $genTable;
    }

    //Update CR Discount Ratio

    function updateCRDiscount() {
        foreach ($_POST as $key => $value) {
            list($cnst, $fab_group_id) = explode("_", $key);
            if ($this->fetchValue(TBL_CRDISCOUNT, "id", 'fabric_gruop_id = ' . $fab_group_id)) {
                $query = "update " . TBL_CRDISCOUNT . " set  value = " . $value . " , update_date ='" . date('Y-m-d H:i:s') . "' where fabric_gruop_id = '$fab_group_id'";
            } else {
                $query = "insert into  " . TBL_CRDISCOUNT . " set fabric_gruop_id = " . $fab_group_id . " , value = " . $value;
            }
           
            $this->executeQry($query);
        }
        
        redirect('manageCRDiscount.php');
    }

    //End 
    //=============Edit Fabric================

    function editFabric($post) {

        //~ echo "<pre>";
        //~ print_r($post);
        //~ echo "</pre>";
        //~ exit;
        //~ 

        $xmlArr = array();

        $xm = 1;



        $post = postwithoutspace($post);

        $_SESSION['SESS_MSG'] == "";



        $query = "update " . TBL_FABRIC . " set modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$post[id]'";

        $this->executeQry($query);



        $xmlArr[$xm]['query'] = addslashes($query);

        $xmlArr[$xm]['identification'] = $post['id'];

        $xmlArr[$xm]['section'] = "update";

        $xm++;



        if ($_SESSION['SESS_MSG'] == "") {

            $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");

            $num = $this->getTotalRow($rst);

            if ($num) {

                while ($line = $this->getResultObject($rst)) {

                    $fabricName = 'fabricName_' . $line->id;

                    $sql = $this->selectQry(TBL_FABRICDESC, ' fabricId = "' . $post[id] . '" and langId = "' . $line->id . '"', '', '');

                    $numrows = $this->getTotalRow($sql);

                    if ($numrows == 0) {

                        $query = "insert into " . TBL_FABRICDESC . " set fabricId = '" . $post[id] . "', fabricName = '$post[$fabricName]', langId = '" . $line->id . "'";



                        $xmlArr[$xm]['query'] = addslashes($query);

                        $xmlArr[$xm]['identification'] = $line->id;

                        $xmlArr[$xm]['section'] = "insert";

                        $xm++;



                        if ($this->executeQry($query))
                            $this->logSuccessFail('1', $query);
                        else
                            $this->logSuccessFail('0', $query);
                    }else {

                        $query = "update " . TBL_FABRICDESC . " set fabricName= '" . $post[$fabricName] . "' where 1 and fabricId = '" . $post[id] . "' and langId = '" . $line->id . "'";



                        $xmlArr[$xm]['query'] = addslashes($query);

                        $xmlArr[$xm]['identification'] = $post[id];

                        $xmlArr[$xm]['section'] = "update";

                        $xm++;



                        if ($this->executeQry($query)) {

                            $this->logSuccessFail('1', $query);
                        } else {

                            $this->logSuccessFail('0', $query);
                        }
                    }
                }
            }
        }

        $xmlInfoObj = new XmlInfo();

        $xmlInfoObj->addXmlData($xmlArr);

        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Information has been updated successfully.!!!");

        header('Location:manageFabric.php');
        exit;

        //echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]';</script>";exit;
    }

}

// End Class
?>	


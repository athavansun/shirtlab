<?php 
session_start();
class TotalQuantity extends MySqlDriver{
    
    function __construct() {
        $this->obj = new MySqlDriver;       
    }
        
    function editQuantitySurcharges($post) {        
        $_SESSION['SESS_MSG'] = "";    
        $sql = $this->executeQry("select * from " .TBL_QUANTITY_SURCHARGE. " where 1 ");
        $num = $this->getTotalRow($sql);
        if($num > 0) {
            while ($row = $this->getResultObject($sql)) {
                $query = "update " . TBL_QUANTITY_SURCHARGE . " set charge = '" . $post['charge_' . $row->id] . "' WHERE id = '" . $row->id . "'";
                $res = $this->executeQry($query);
                if ($res) {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
                } else {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
                }
            }
        }
    }

    function addQuantitySurcharge($post) {        	
    	$query   = "INSERT INTO ".TBL_QUANTITY_SURCHARGE." SET quantity='".$post['pcs']."', charge = '".$post[charges]."'";
            
        $insertId = $this->executeQry($query);
        if($insertId){
            $_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
        }else{
            $_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add record.");
        }	
        header("location:addQuantityDiscount.php");
        exit;
    }
        
    function deleteQuantitySurcharge($get) {
    	$sql = " DELETE FROM  ".TBL_QUANTITY_SURCHARGE." WHERE id = '".$get['id']."'";
        $rst = $this->executeQry($sql);
        if($rst){
                $this->logSuccessFail('1',$query);		
        }else{ 	
                $this->logSuccessFail('0',$query);
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageQuantityDiscount.php?page=$get[page]&limit=$get[limit]';</script>";
    }        
}
    
    
	
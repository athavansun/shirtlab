<?php
class Common {	

	function dateDiff($submitdate)
	{ 
		$endtime = $submitdate;
		$starttime = time();
		$endtime = strtotime($endtime);
		$timediff = $endtime-$starttime;
		$days=intval($timediff/86400);
		$remain=$timediff%86400;
		$hours=intval($remain/3600);
		$remain=$remain%3600;
		$mins=intval($remain/60);
		$secs=$remain%60;
		if($days > 1)
			$timediff = $days.' days ';
		elseif($days == 1)
			$timediff = $days.' day ';
		elseif($days >= 7)
		{ 
			$weeks = int($days/7);
			if($weeks == 1)
				$timediff = '1 week';
			else 
				$timediff = $weeks.' weeks';
		}    
		elseif($days < 1 && $hours < 1) 
			$timediff = $mins.' mins';
		elseif($days < 1 && $hours >= 1)
			$timediff = $hours.' hours ';
		return $timediff;
	}			
} 
?>
<?php 
session_start();
class Banner extends MySqlDriver{
	function __construct() { $this->obj = new MySqlDriver; }
        
        //Start : Show Grid Details=============================================        
        function valDetail() {						
		$cond = "1 and b.id = bd.bannerId and bd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND bd.bannerTitle LIKE '%$searchtxt%' OR b.bannerGroup LIKE  '%$searchtxt%'";
		}
		
		$query = "select b.*,bd.bannerTitle from ".TBL_BANNER." as b , ".TBL_BANNER_DESCRIPTION." as bd where $cond ";			
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"Inactive";
					
					
					$thumbImage = checkImgAvailable(__BANNERTHUMB__.$line->bannerImage);
					$originalImage=checkImgAvailable(__BANNERORIGINAL__.$line->bannerImage);
				   
					list($w,$h)=getimagesize($originalImage);
                                        
					/*** Banner Click Count & Show*********
					$sqlDisplayClick = $this->executeQry("select sum(banners_shown) as banners_shown, sum(banners_clicked) as banners_clicked from ".TBL_BANNER_HISTORY." where 1 and banner_id = '".$line->id."'");
					$lineDisplayClick = $this->getResultObject($sqlDisplayClick);
					$banners_shown = $lineDisplayClick->banners_shown?$lineDisplayClick->banners_shown:0;
					$banners_clicked = $lineDisplayClick->banners_clicked?$lineDisplayClick->banners_clicked:0;					
					$display = $banners_shown.' / '.$banners_clicked;
                                         ********************************/
                                         
					$genTable .= '<tr>
					<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
					<td>'.$i.'</td>
					<td>'.stripslashes($line->bannerTitle).'</td>
                                        <td><a rel="shadowbox;width='.$w.';height='.$h.'" href="'.$originalImage.'"><img src="'.$thumbImage.'"></a></td>
					<td>'.stripslashes($line->bannerGroup).'</td>';
					
                                        $genTable.='<td>';
					if($menuObj->checkEditPermission()) {							
					$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'banner\')" >'.$status.'</div>';
					}	
					$genTable .= '</td>';                                       
                                        
					$genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->bannerTitle).'" href="viewBanner.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';
					
					$genTable.='<td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editBanner.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td>';
                                        
                    $genTable.='<td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=banner&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</td>';
                                        
                    $genTable.='</tr>';
					$i++;	
				}
				switch($recordsPerPage){
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}
	
        
        //Start : Add New Record===============================================
        function addNew($post,$file){
		//echo "<pre>";print_r($_POST);print_r($_FILES);echo "</pre>";exit;       
		$post = postwithoutspace($post);	
                
                //Upload Banner=======================
		if($file['bannerImage']['name']){
  			$filename = stripslashes($file['bannerImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = ABSOLUTEPATH . __BANNERORIGINAL__.$image_name;
			if($this->checkExtensions($extension)) {	
				$filestatus = 	move_uploaded_file($file['bannerImage']['tmp_name'], $target);
				chmod($target, 0777);
                                if($filestatus){
                                    $ThumbImage = ABSOLUTEPATH . __BANNERTHUMB__.$image_name;
                                    exec(' "'.IMAGEMAGICPATH.'" "'.$target.'" -resize 96x40 "'.$ThumbImage.'"');          
                                }else{
                                    $_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Image upload error.");
                                }
        
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Uploaded image extension not allowed.");
			} 	
		}
                
                //Insert Data if banner upload successfull==============================
		if($_SESSION['SESS_MSG'] == "") {
			$bannerGroup = $post[bannerGroup]?$post[bannerGroup]:$post[inbuiltBannerGroup];
                        $post[bannerSize]=($post[bannerSize])?$post[bannerSize]:$this->fetchValue(TBL_BANNER,"bannerSize","bannerGroup='".$bannerGroup."'");
			$bannerUrl = substr($post[bannerUrl],0,7) == "http://"?$post[bannerUrl]:"http://".$post[bannerUrl];
			$sql = "INSERT INTO ".TBL_BANNER." SET `bannerUrl`='$bannerUrl', `bannerImage`='$image_name',`bannerSize`='".$post[bannerSize]."', `bannerGroup`='$bannerGroup',  openIn = '$post[openIn]', expires_impressions = '$post[expires_impressions]', startDate = '$post[startDate]', endDate = '$post[endDate]', `addDate`='".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', `status`='1'";			
			$rst = $this->executeQry($sql);
			$inserted_id = mysql_insert_id();
			
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);			}
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$bannerTitle = 'bannerTitle_'.$line->id;
					$bannerHtmlText='bannerHtmlText_'.$line->id;
					$query = "insert into ".TBL_BANNER_DESCRIPTION." set bannerId = '$inserted_id',bannerHtmlText='".addslashes($post[$bannerHtmlText])."', langId = '".$line->id."', bannerTitle = '".addslashes($post[$bannerTitle])."', bannerSize='".$post[bannerSize]."'";	
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}	
			
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
			header('Location:addBanner.php');
			echo "<script language=javascript>window.location.href='manageBanner.php';</script>";exit;
		}
	}


        //Start : Edit New Record===================================================
        function editRecord($post,$file){
		//echo "<pre>";print_r($post); print_r($file);echo "</pre>";exit;
		$post = postwithoutspace($post);	
        
        $sql = "update ".TBL_BANNER." SET `bannerUrl`='".$post['bannerUrl']."', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";
        //exit();
		$rst = $this->executeQry($sql);
              
                //Upload Banner======================
		if($file['bannerImage']['name']){
  			$filename = stripslashes($file['bannerImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = ABSOLUTEPATH . __BANNERORIGINAL__.$image_name;
			if($this->checkExtensions($extension)) {	
				$filestatus = 	move_uploaded_file($file['bannerImage']['tmp_name'], $target);
				@chmod($target, 0777);	
				if($filestatus){
					$ThumbImage = ABSOLUTEPATH . __BANNERTHUMB__.$image_name;
					exec(' "'.IMAGEMAGICPATH.'" "'.$target.'" -resize  96x40 "'.$ThumbImage.'"');
					$sql = "update ".TBL_BANNER." SET `bannerImage`='$image_name', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";			
					$rst = $this->executeQry($sql);			

					if($rst){
							$this->logSuccessFail("1",$sql);
					}else{
							$this->logSuccessFail("0",$sql);
					}	
				}else{
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Image upload error.");
				}
									
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"Uploaded image extension not allowed.");
			} 	
		}
                
                
		if($_SESSION['SESS_MSG'] == "") {
			//$bannerGroup = $post[bannerGroup]?$post[bannerGroup]:$post[inbuiltBannerGroup];
			$bannerUrl = substr($post[bannerUrl],0,7) == "http://"?$post[bannerUrl]:"http://".$post[bannerUrl];
			$sql = "update ".TBL_BANNER." SET expires_impressions = '$post[expires_impressions]', openIn = '$post[openIn]', startDate = '$post[startDate]', endDate = '$post[endDate]' where id = '$post[id]'";			
			$rst = $this->executeQry($sql);			
			
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$bannerTitle = 'bannerTitle_'.$line->id;
					$bannerHtmlText = 'bannerHtmlText_'.$line->id;
					$sql = $this->selectQry(TBL_BANNER_DESCRIPTION,'1 and bannerId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
					$numrows = $this->getTotalRow($sql);
					if($numrows == 0) {
						$query = "insert into ".TBL_BANNER_DESCRIPTION." set bannerId = '$post[id]', langId = '".$line->id."', bannerTitle = '".$post[$bannerTitle]."',bannerHtmlText='".$post[bannerHtmlText]."'";
						if($this->executeQry($query)) 
							$this->logSuccessFail('1',$query);		
						else 	
							$this->logSuccessFail('0',$query);	
					} else {
						$query = "update ".TBL_BANNER_DESCRIPTION." set bannerTitle = '".$post[$bannerTitle]."',bannerHtmlText='".$post[$bannerHtmlText]."' where 1 and bannerId = '$post[id]' and langId = '".$line->id."'";
						if($this->executeQry($query)) 
							$this->logSuccessFail('1',$query);		
						else 	
							$this->logSuccessFail('0',$query);	
					}	
				}	
			}
			
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
                        header('Location:manageBanner.php');
			echo "<script language=javascript>window.location.href='manageBanner.php?page=$post[page]';</script>";exit;
		}
	}

        //Start :In-Built Banner Group    ======================================
        function getInbuiltBannerGroup($bannerGroup) {               
		$genTable = '<select id="inbuiltBannerGroup" name="inbuiltBannerGroup"  onChange="showBannerSize();" >';
		$sql = $this->executeQry("select  distinct bannerGroup as bannerGroup  from ".TBL_BANNER." WHERE 1");	
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {
                        $genTable.='<option value="">-Select Group-</option>';
			while($line = $this->getResultObject($sql)) {
                                if($bannerGroup==$line->bannerGroup){
                                    $sel ="selected='selected'";   
                                    $selectedBannerGroup=$line->bannerGroup." ( ".$line->bannerSize." )";
                                }else{
                                    $sel ="";
                                }
				$sel = $bannerGroup==$line->bannerGroup?"selected='selected'":"";          
                                $bannerSize=$this->fetchValue(TBL_BANNER,'bannerSize','bannerGroup="'.$line->bannerGroup.'"');
				$genTable .= '<option value="'.$line->bannerGroup.'" '.$sel.'>'.$line->bannerGroup.' ( '.$bannerSize.' )</option>';	
			}
		}
		$genTable .= '</select>';
                return $genTable;
	}
        //Start : Get Result ================================================
        function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_BANNER." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {				
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageBanner.php');	
		}
	}
        //Start : Change Value Status===========================================
        function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_BANNER,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "UPDATE ".TBL_BANNER." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
        //Start : Delete Single Value=================================
        function deleteValue($get) {
		$filename = $this->fetchValue(TBL_BANNER,"bannerImage","id='$get[id]'");
		if($filename){
			if(file_exists(ABSOLUTEPATH.__BANNERORIGINAL__.$filename)){ unlink(ABSOLUTEPATH .__BANNERPATH__.$filename);}
		}
		$sql = "DELETE FROM  ".TBL_BANNER." where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$sql = "DELETE FROM  ".TBL_BANNER_DESCRIPTION." where bannerid = '$get[id]'";		
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$sql = "DELETE FROM  ".TBL_BANNER_HISTORY." where banner_id = '$get[id]'";		
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}		
				
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageBanner.php?page=$post[page]&limit=$post[limit]';</script>";
	}
        
        
        //Start : Mulitple Operation=========================================
	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageBanner.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$filename = $this->fetchValue(TBL_BANNER,"bannerImage","id='$val'");
					if($filename){
						if(file_exists(ABSOLUTEPATH . __BANNERORIGINAL__.$filename)){ unlink(ABSOLUTEPATH .__BANNERORIGINAL__.$filename);}
					}
					$sql = "DELETE FROM ".TBL_BANNER." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
					
					$sql = "DELETE FROM ".TBL_BANNER_DESCRIPTION." where bannerId = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
					
					$sql = "DELETE FROM ".TBL_BANNER_HISTORY." where banner_id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_BANNER." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_BANNER." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageBanner.php?page=$post[page]';</script>";exit;
	}		

	


}// End Class
?>	

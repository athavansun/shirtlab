<?php
session_start();

class Size extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function valDetail() {

        //$cond = "1 and ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
        $cond = " and sd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (sd.sizeName LIKE '%$searchtxt%' ) ";
        }
        //$query = "select ft.*,ftd.sizeName from ".TBL_SIZE." as ft , ".TBL_SIZEDESC." as ftd where $cond ";
        $query = "select s.*, sd.sizeName from " . TBL_SIZE . " as s inner join " . TBL_SIZEDESC . " as sd on s.id = sd.Id " . $cond;
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $this->setLimit($_GET[limit]);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : " s.groupId";
            $order = $_GET[order] ? $_GET[order] : "ASC";
            $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;
            //$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);
            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    $currentOrder .= $line->id . ",";
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    if ($line->status == 0)
                        $status = "Inactive";
                    else
                        $status = "Active";
                    if ($line->isDefault == 1) {
                        $isDefault = "checked='checked'";
                        $onclickstatus = '';
                        $chkbox = '--';
                        $statusfun = '';
                    } else {
                        $isDefault = "";
                        $statusfun = 'style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'Size\')"';
                        $onclickstatus = ' onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'language\')"';
                        $chkbox = '<input name="chk[]" value="' . $line->id . '" type="checkbox">';
                    }

                    $genTable .= '<tr class="dragbox-content ' . $highlight . '" id="' . $line->id . '" >
						<th>' . $chkbox . '</th>
						<td>' . $i . '</td>
						<td>' . substr($line->sizeName, 0, 40) . '</td>
						<td>' . $this->fetchValue(TBL_SIZE_GROUPDESC, 'groupName', "Id = '" . $line->groupId . "' and langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'") . '</td>						
                                                    
						<td><input type="checkbox" ' . $isDefault . ' disabled=disabled class="welcheckbox"></td>
                                                <td><input type="text" name="seq[' . $line->id . ']" style="width:30px;" value="' . $line->sequence . '"></td>
						<td>';
                    if ($menuObj->checkEditPermission())
                        $genTable .= '<div id="' . $div_id . '" ' . $statusfun . '>' . $status . '</div>';

                    $genTable .= '</td><td>';

                    if ($menuObj->checkEditPermission())
                        $genTable .= '<a class="i_pencil edit" href="editSize.php?id=' . base64_encode($line->id) . '&page=' . $page . '">Edit</a>';

                    $genTable .= '</td><td>';

                    if ($menuObj->checkDeletePermission())
                        if ($line->isDefault == 1) {
                            $genTable .='--';
                        } else {
                            $genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=Size&type=delete&id=" . $line->id . "&page=$page'}else{}\" >Delete</a>";
                        }

                    $genTable .= '</td></tr>';

                    $i++;
                }

                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="' . $currentOrder . '" /></div>';

                switch ($recordsPerPage) {
                    case 10:
                        $sel1 = "selected='selected'";
                        break;
                    case 20:
                        $sel2 = "selected='selected'";
                        break;
                    case 30:
                        $sel3 = "selected='selected'";
                        break;
                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='" . $totalrecords . "' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='" . $currpage . "'></td><td class='page_info' align='center' width='200'>Total " . $totalrecords . " records found</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table></div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
        }
        return $genTable;
    }

    /// For Add New Forum Topic
    function addRecord($post) {
        //~ echo "<pre>";
        //~ print_r($post);exit;
        $query = "insert into " . TBL_SIZE . " set groupId = '" . $post[groupId] . "', sequence = '" . $post['sequence'] . "' ,status  = '1', addDate = '" . date('Y-m-d') . "', addedBy = '" . $_SESSION['ADMIN_ID'] . "'";
        $res = $this->executeQry($query);
        $inserted_id = mysql_insert_id();
        if ($res)
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);

        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $sizeName = 'sizeName_' . $line->id;
                $query = "insert into " . TBL_SIZEDESC . " set  Id  = '$inserted_id', langId = '" . $line->id . "' , sizeName  = '" . addslashes($post[$sizeName]) . "' ";
                if ($this->executeQry($query))
                    $this->logSuccessFail('1', $query);
                else
                    $this->logSuccessFail('0', $query);
            }
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Information has been added successfully.");
        }
        header("Location:addSize.php");
        exit;
    }

    /// For Change Topic Status
    function changeValueStatus($get) {
        $status = $this->fetchValue(TBL_SIZE, "status", "1 and id = '$get[id]'");
        if ($status == 1) {
            $stat = 0;
            $status = "Inactive, 0";
        } else {
            $stat = 1;
            $status = "Active, 1";
        }
        $query = "update " . TBL_SIZE . " set status = '$stat', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);
        echo $status;
    }

    function deleteAllValues($post) {

        if (isset($post['sort'])) {
            foreach ($post['seq'] as $key => $val) {
                $this->executeQry("UPDATE " . TBL_SIZE . " SET sequence =" . $val . " where id=" . $key);
            }
        } elseif (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the action or records , And then submit!!!");
            echo "<script language=javascript>window.location.href='manageSize.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }
        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $result = $this->deleteRec(TBL_SIZE, "id='" . $val . "'");
                    $result1 = $this->deleteRec(TBL_SIZEDESC, "id='" . $val . "'");
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your all selected information has been deleted successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'enableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
                    $sql = "update " . TBL_SIZE . " set status ='1', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Enable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'disableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "update " . TBL_SIZE . " set status ='0', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Disable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        echo "<script language=javascript>window.location.href='manageSize.php?page=$post[page]';</script>";
    }

    /// For Delete Single Forum Topic

    function deleteValue($get) {
        $result = $this->deleteRec(TBL_SIZE, "id='" . $get['id'] . "'");
        $result1 = $this->deleteRec(TBL_SIZEDESC, "id='" . $get['id'] . "'");
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageSize.php?page=$get[page]&limit=$get[limit]';</script>";
    }

    /// Get Information About Existing color
    function getResult($id) {
        $sql = $this->executeQry("select * from " . TBL_SIZE . " where id = '$id'");
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            return $line = $this->getResultObject($sql);
        } else {
            redirect("manageSize.php");
        }
    }

    // Edit color

    function editRecord($post) {
        //print_r($post);exit;
        $isdefault = $post[isDefault] ? 1 : 0;
        if ($isdefault) {
            $this->executeQry("update " . TBL_SIZE . " set isDefault='0' where id != '$post[id]'");
            $this->executeQry("update " . TBL_SIZE . " set groupId = '" . $post[groupId] . "' isDefault='1',status ='1' where id  = '$post[id]'");
        } else {
            $this->executeQry("update " . TBL_SIZE . " set groupId = '" . $post[groupId] . "', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION[ADMIN_ID] . "' where id  = '$post[id]'");
        }
        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $sizeName = 'sizeName_' . $line->id;
                $sql = $this->selectQry(TBL_SIZEDESC, '1 and id = "' . $post[id] . '" and langId = "' . $line->id . '"', '', '');
                $numrows = $this->getTotalRow($sql);
                if ($numrows == 0) {
                    $query = "insert into " . TBL_SIZEDESC . " set id = '$post[id]', langId = '" . $line->id . "', sizeName = '" . addslashes($post[$sizeName]) . "' ";
                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                } else {
                    $query = "update " . TBL_SIZEDESC . " set  sizeName = '" . addslashes($post[$sizeName]) . "'  where 1 and id = '$post[id]' and langId = '" . $line->id . "'";
                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                }
            }
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record Updated successfully.");
        header("location:manageSize.php");
        exit;
        //echo "<script>window.location.href='manageSize.php?page=$post[page]';</script>";
    }

    function SortSequence($get) {
        //print_r($get);
        $sortedArr = explode("=", $get['url']);
        $sortedIdArr = $sortedArr[1];
        $Sorted_Order_Arr = explode(",", $sortedIdArr);
        $Current_Order_Arr = explode(",", substr_replace($get['Current_Order'], "", -1));

        $i = 0;
        $sorted_key_arr = array();
        foreach ($Sorted_Order_Arr as $Sorted_Order_Val) {
            $current_id = $Current_Order_Arr[$i];
            $sql_string = "SELECT sequence FROM " . TBL_SIZE . " WHERE id = '$current_id'";
            $query = mysql_query($sql_string);
            if ($line = mysql_fetch_object($query)) {
                $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
            }
            $i++;
        }
        //print_r($sorted_key_arr);
        foreach ($sorted_key_arr as $key => $val) {
            $sql_string = "UPDATE " . TBL_SIZE . " SET sequence = '$val' WHERE id = '" . $key . "'";
            mysql_query($sql_string);
        }

        /* 		for($i=30;$i<60;$i++){
          $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
          $n_row = mysql_num_rows($Qry);
          if($n_row > 1){
          $k = 1;
          while($data = mysql_fetch_object($Qry)){
          mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
          $k++;
          }
          }
          } */
        $cond = "1 and ft.id = ftd.Id  and ftd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
        //$cond = "1  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
        $query = "select ft.*,ftd.sizeName from " . TBL_SIZE . " as ft , " . TBL_SIZEDESC . " as ftd where $cond ";


        //-------------------------Paging------------------------------------------------			
        $paging = $this->paging($query);
        $this->setLimit($get['limit']);
        $recordsPerPage = $this->getLimit();
        $offset = $this->getOffset($get['page']);
        $this->setStyle("redheading");
        $this->setActiveStyle("smallheading");
        $this->setButtonStyle("boldcolor");
        $currQueryString = $this->getQueryString();
        $this->setParameter($currQueryString);
        $totalrecords = $this->numrows;
        $currpage = $this->getPage();
        $totalpage = $this->getNoOfPages();
        $pagenumbers = $this->getPageNo();
        //-------------------------Paging------------------------------------------------
        $orderby = $_GET[orderby] ? $_GET[orderby] : "sequence";
        $order = $_GET[order] ? $_GET[order] : "ASC";
        //   $query .=  " ORDER BY cd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
        $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;

        $order_query = mysql_query($query);
        while ($line = mysql_fetch_object($order_query)) {
            $currentOrder .= $line->id . ",";
        }
        ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?= $currentOrder ?>" /><?php
    }

    function findMaxSequence() {
        $query = "SELECT max(sequence) as maxsequence FROM " . TBL_SIZE . " ";
        $res = $this->executeQry($query);
        $line = $this->getResultObject($res);
        return $line->maxsequence + 1;
    }

    function checkSizeExists($sizeName, $langId, $id = '', $groupId) {
        $sizeName = trim($sizeName);
        if ($id != '') {
            $query = "select s.*, sd.sizeName from " . TBL_SIZE . " as s, " . TBL_SIZEDESC . " as sd where s.id = sd.Id and s.isDeleted = '0' and sd.langId = '" . $langId . "' and sizeName = '" . addslashes($sizeName) . "' and s.groupId = '" . $groupId . "' and s.id != '" . $id . "'";
        } else {
            $query = "select s.*, sd.sizeName from " . TBL_SIZE . " as s, " . TBL_SIZEDESC . " as sd where s.id = sd.Id and s.isDeleted = '0' and sd.langId = '" . $langId . "' and sd.sizeName = '" . addslashes($sizeName) . "' and s.groupId ='" . $groupId . "'";
        }
        $sql = $this->executeQry($query);
        $row = $this->getTotalRow($sql);
        if ($row > 0) {
            return true;
        } else {
            return false;
        }
    }

    //========================================================================
    //==============================Size Group================================
    //========================================================================

    function valSizeGroup() {

        $cond = "1 and g.id = gd.id  and gd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (ftd.sizeName LIKE '%$searchtxt%' ) ";
        }
        $query = "select g.*, gd.groupName from " . TBL_SIZE_GROUP . " as g , " . TBL_SIZE_GROUPDESC . " gd where $cond ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $this->setLimit($_GET[limit]);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : " groupName ";
            $order = $_GET[order] ? $_GET[order] : "ASC";
            $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;
            //$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);
            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    //~ echo "<pre>";
                    //~ print_r($line);
                    //~ echo "</pre>";exit;	
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    $status = ($line->status) ? "Active" : "Inactive";

                    $genTable .= '<tr>
						<th><input name="chk[]" value="' . $line->id . '" type="checkbox" class="checkbox"></th>						
						<td>' . $i . '</td>						
						<td>' . stripslashes($line->groupName) . '</td>';
                    $genTable.='<td>';
                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<div id="' . $div_id . '" style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'manageSizeGroup\')" >' . $status . '</div>';
                    }
                    $genTable .= '</td>';

                    $genTable.='<td>';
                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<a class="i_pencil edit" href="editSizeGroup.php?id=' . base64_encode($line->id) . '&page=' . $page . '">Edit</a>';
                    }
                    $genTable .= '</td>';

                    $genTable.='<td>';
                    if ($menuObj->checkDeletePermission()) {
                        $genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageSizeGroup&type=delete&id=" . $line->id . "&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
                    }
                    $genTable .= '</td>';

                    $genTable.='</tr>';
                    $i++;
                }
                switch ($recordsPerPage) {
                    case 10:
                        $sel1 = "selected='selected'";
                        break;
                    case 20:
                        $sel2 = "selected='selected'";
                        break;
                    case 30:
                        $sel3 = "selected='selected'";
                        break;
                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='" . $totalrecords . "' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='" . $currpage . "'></td><td class='page_info' align='center' width='200'>Total " . $totalrecords . " records found</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table></div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
        }
        return $genTable;
    }

    function addGroupName($post) {
        //~ echo "<pre>";
        //~ print_r($post);exit;
        $query = "insert into " . TBL_SIZE_GROUP . " set status  = '1', addDate = '" . date('Y-m-d') . "', addedBy = '" . $_SESSION['ADMIN_ID'] . "'";
        $res = $this->executeQry($query);
        $inserted_id = mysql_insert_id();
        if ($res)
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);

        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $groupName = 'groupName_' . $line->id;
                $query = "insert into " . TBL_SIZE_GROUPDESC . " set  Id  = '$inserted_id', langId = '" . $line->id . "' , groupName  = '" . addslashes($post[$groupName]) . "' ";
                if ($this->executeQry($query))
                    $this->logSuccessFail('1', $query);
                else
                    $this->logSuccessFail('0', $query);
            }
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Information has been added successfully.");
        }
        header("Location:addSizeGroup.php");
        exit;
    }

    function editGroupName($post) {
        //~ echo "<pre>";
        //~ print_r($post);
        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $groupName = 'groupName_' . $line->id;
                $sql = $this->selectQry(TBL_SIZE_GROUPDESC, '1 and id = "' . $post[id] . '" and langId = "' . $line->id . '"', '', '');
                $numrows = $this->getTotalRow($sql);
                if ($numrows == 0) {
                    $query = "insert into " . TBL_SIZE_GROUPDESC . " set Id = '$post[id]', langId = '" . $line->id . "', groupName = '" . addslashes($post[$groupName]) . "' ";
                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                } else {
                    $query = "update " . TBL_SIZE_GROUPDESC . " set  groupName = '" . addslashes($post[$groupName]) . "'  where 1 and Id = '$post[id]' and langId = '" . $line->id . "'";
                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                }
            }
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Information has been updated successfully.!!!");
        header('Location:manageSizeGroup.php');
        //echo "<script>window.location.href='manageSizeGroup.php?page=$post[page]';</script>";
        exit();
    }

    function checkGroupNameExists($groupName, $langId, $id = '') {
        $groupName = trim($groupName);
        if ($id != '') {
            $query = "select g.*, gd.groupName from " . TBL_SIZE_GROUP . " as g, " . TBL_SIZE_GROUPDESC . " as gd where g.id = gd.Id and g.isDeleted = '0' and gd.langId = '" . $langId . "' and gd.groupName = '" . addslashes($groupName) . "' and g.id != '" . $id . "'";
        } else {
            $query = "select g.*, gd.groupName from " . TBL_SIZE_GROUP . " as g, " . TBL_SIZE_GROUPDESC . " as gd where g.id = gd.Id and g.isDeleted = '0' and gd.langId = '" . $langId . "' and gd.groupName = '" . addslashes($groupName) . "'";
        }
        $sql = $this->executeQry($query);
        $row = $this->getTotalRow($sql);
        if ($row > 0) {
            return true;
        } else {
            return false;
        }
    }

    function changeGroupStatus($get) {
        $status = $this->fetchValue(TBL_SIZE_GROUP, "status", "1 and id = '$get[id]'");
        if ($status == 1) {
            $stat = 0;
            $status = "Inactive, 0";
        } else {
            $stat = 1;
            $status = "Active, 1";
        }
        $query = "update " . TBL_SIZE_GROUP . " set status = '$stat', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);
        echo $status;
    }

    function deleteGroup($get) {
        $result = $this->deleteRec(TBL_SIZE_GROUP, "id='" . $get['id'] . "'");
        $result1 = $this->deleteRec(TBL_SIZE_GROUPDESC, "id='" . $get['id'] . "'");
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageSizeGroup.php?page=$get[page]&limit=$get[limit]';</script>";
    }

    function deleteAllGroup($post) {
        //~ echo "<pre>";
        //~ print_r($post);exit;
        if (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the action or records , And then submit!!!");
            echo "<script language=javascript>window.location.href='manageSizeGroup.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }
        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $result = $this->deleteRec(TBL_SIZE_GROUP, "id='" . $val . "'");
                    $result1 = $this->deleteRec(TBL_SIZE_GROUPDESC, "id='" . $val . "'");
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your all selected information has been deleted successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'enableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
                    $sql = "update " . TBL_SIZE_GROUP . " set status ='1', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Enable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'disableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "update " . TBL_SIZE_GROUP . " set status ='0', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Disable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        echo "<script language=javascript>window.location.href='manageSizeGroup.php?page=$post[page]';</script>";
    }

    function getSizeGroup($id) {
        $sql = $this->executeQry("select * from " . TBL_SIZE_GROUP . " where id = '$id'");
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            return $line = $this->getResultObject($sql);
        } else {
            redirect("manageSizeGroup.php");
        }
    }

    function getGroupName($id = '') {
        $sql = $this->executeQry("select * from " . TBL_SIZE_GROUPDESC . " WHERE langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'");
        $num = $this->getTotalRow($sql);
        $sel = "";
        if ($num > 0) {
            while ($line = $this->getResultObject($sql)) {
                ($id == $line->Id) ? $sel = "selected='selected'" : $sel = "";
                $genTable .= '<option value="' . $line->Id . '" ' . $sel . '>' . $line->groupName . '</option>';
            }
        }
        return $genTable;
    }

}

// End Class
?>	

<?php 
session_start();
class SystemConfig extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function addConfiguration($post,$file)
	{
		$_SESSION['SESS_MSG'] = "";
		$sql = $this->executeQry("select * from ".TBL_SYSTEMCONFIG." where 1");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)){
                if($line->systemName == 'MAX_AMOUNT') {                    
                    $query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' WHERE systemName = '".$line->systemName."'"; 
                }
                
				if ($line->systemName=='PAYMENT_TYPE')
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' WHERE systemName = '".$line->systemName."'"; 

				if ($line->systemName=='PAYPAL_ID')
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' WHERE systemName = '".$line->systemName."'"; 
					
				if ($line->systemName=='PAYPAL_SANDBOX_ID')
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' WHERE systemName = '".$line->systemName."'"; 
							
				if($line->systemName != 'SITE_LOGO' && $line->systemName != 'SITE_FAVICON' && $line->systemName != 'BACKGROUND_IMAGE' && $line->systemName != 'SWF_TTF' && in_array($line->systemName,array_keys($post))) {
					
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".addslashes($post[$line->systemName])."' WHERE systemName = '".$line->systemName."'"; 
					
				
					
					if($this->executeQry($query)) 
				 		$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
						
						
				}else if($line->systemName == 'SITE_FAVICON') {
					if($file['SITE_FAVICON']['name']){
						$filename = stripslashes($file['SITE_FAVICON']['name']);
						$extension = "ico";
						$extension = strtolower($extension);
				
						$image_name = date("Ymdhis").time().rand().'.'.$extension;
						$target    = __SITELOGOORIGINAL__.$image_name;
						if($this->checkExtensions($extension)) {	
							$prevLogo = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'SITE_FAVICON'");
							@unlink(__SITELOGOORIGINAL__.$prevLogo);
							$filestatus = 	move_uploaded_file($file['SITE_FAVICON']['tmp_name'], $target);
							chmod($target, 0777);									
							$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".$image_name."' where systemName = 'SITE_FAVICON'"; 
							if($this->executeQry($query)) 
						 		$this->logSuccessFail('1',$query);		
							else 	
								$this->logSuccessFail('0',$query);
						} else {
							$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
						} 	
					}	
				} else if($line->systemName == 'DEFCOUNTRY') {
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".$post['DEFCOUNTRY']."' where systemName = '".$line->systemName."'"; 
					if($this->executeQry($query)) 
				 		$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);		
										
				}else if(trim($line->systemName) == 'DEFLANGUAGE'){				   
					$query = "update ".TBL_SYSTEMCONFIG." set systemVal = '".$post['DEFLANGUAGE']."' where systemName = '".$line->systemName."'"; 
					if($this->executeQry($query)) 
				 		$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}   

		}
                $_SESSION['SESS_MSG'] = msgSuccessFail('success',"Configuration saved successfully.");
	}	

	function getLanguage($countryId, $lang)
	{               
            $html = '';
            $langlanguage = $this->fetchValue(TBL_COUNRTYSETTING,'langId',"countryId = '".$countryId."'");
            $langArr = explode(',',$langlanguage);
            foreach($langArr as $langId)
            {
                if($lang == $langId) {
                    $check = 'selected = "selected"';
                } else {
                    $check = '';
                }
                $langTitle = $this->fetchValue(TBL_LANGUAGE,'languageName',"id = '".$langId."'");
                $html .='<option '.$check.' value="'.$langId.'">'.$langTitle.'</option>';
            }
            echo $html;
	}

}// End Class
?>	

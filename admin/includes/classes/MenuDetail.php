<?php 
session_start();
class MenuDetail extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function valDetail(){
		//$cond = "1 and ft.id = ftd.id  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		//if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			//$searchtxt = $_REQUEST['searchtxt'];
			//$cond .= " AND (ftd.colorName LIKE '%$searchtxt%' )";
		//}
		$query = "SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"menuId";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;	
				$j = 1;		
				while($line = $this->getResultObject($rst)) {
					
					$sqlSubMenu   = $this->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId='$line->menuId' AND parentId!=0 ");	
					$genTable .= '<div><div style="float:left;">'.$line->menuName.'</div>';
					$genTable .= '<div class="column" id="'.$line->menuId.'"><input type="hidden" name="parentid'.$line->menuId.'" value="'.$line->menuId.'"/>';
					$row = $this->getTotalRow($sqlSubMenu);
					if($row == 0){
				//	$genTable .= '<div class="dragbox-content"></div>';
					}
					$highlight = $j%2==0?"main-body-bynic":"main-body-bynic2";
					//$genTable .= '<span id="'.$line->menuId.'"><div class="'.$highlight.'" id= "'.$line->menuId.'"><ul><li ><li style="width:140px;">'.$line->menuName.'</li></ul></div></span>';
					$genTable .= '<span id="'.$line->menuId.'">&nbsp;</span>';
					
					while ($line1 = $this->getResultObject($sqlSubMenu)){
						$currentOrder	.=	$line1->menuId.",";
						$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
						$div_id = "status".$line1->menuId;
						if ($line1->status==0)
							$status = "Inactive";
						else
							$status = "Active";
						$genTable .= '<div class="'.$highlight.'" id="'.$line1->menuId.'"><div class="dragbox-content"><ul><li style="width:50px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line1->menuId.'" type="checkbox"></li><li style="width:60px;">'.$i.'</li><li style="width:140px;">'.$line1->menuName.'</li><li style="width:250px;">';
					
						$genTable .= $line1->menuUrl.'</li><li style="width:110px;">';
									
						if($menuObj->checkEditPermission()) 							
							$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line1->menuId.'\',\'menu\')">'.$status.'</div>';
									
																											
						$genTable .= '</li>
					
						<li style="width:75px;">';
									
						if($menuObj->checkEditPermission()) 					
							$genTable .= '<a href="editMenu.php?id='.base64_encode($line1->menuId).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
						$genTable .= '</li><li>';
				    
						if($menuObj->checkDeletePermission()) 					
							$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Forum Topic?')){window.location.href='pass.php?action=menu&type=delete&id=".$line1->menuId."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
						$genTable .= '</li></ul></div></div>';
						$i++;
					}
					$j++;	
					$i = 1;
					$genTable .= '</div></div>';
				}
				//$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	
	}
     
	function getAllParentMenu($id){
		$query = "SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 ORDER BY menuId";
		$sqlMenu = $this->executeQry($query);	
		$table .= '<select name="mainMenu" id="m__Main  Menu" >';
			while($rowMenu = $this->getResultObject($sqlMenu)){
				if($rowMenu->menuId == $id )
					$sel = "selected = 'selected'";
				else
					$sel = "";
				$table .= "<option value='".$rowMenu->menuId."' $sel>".$rowMenu->menuName."</option>";
			}
		$table .= "</select>";
		echo $table;
	}
	
	function addRecord($post){
	
		$query = "Insert INTO ".TBL_MENU." set menuName = '".$post['menuName']."' , menuUrl = '".$post['menuUrl']."' , status = '1' , parentId = '0' , menu_type = '1'  ";
		$sqlMenu = $this->executeQry($query);
		$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		header("Location:addMenu.php");exit;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_MENU,"status","1 and menuId = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
		$query = "update ".TBL_MENU." set status = '$stat' where menuId = '$get[id]'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageMenu.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$queryFind = "select * from ".TBL_COLOR." where id='".$val."'";
					$sqlFind = $this->executeQry($queryFind);
					$lineFind = $this->getResultObject($sqlFind);
					if($lineFind->isDefault != 1){
				    	$result=$this->deleteRec(TBL_COLORDESC,"id='".$val."'");	
						$result1=$this->deleteRec(TBL_COLOR,"id='".$val."'");
					}	
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_MENU." set status ='1' where menuId ='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_MENU." set status ='0' where menuId ='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageMenu.php?page=$post[page]';</script>";
	}
	
	/// Get Information About Existing Menu
	function getResult($id) {
	    
		$sql = $this->executeQry("select * from ".TBL_MENU." where menuId  = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageMenu.php");
		}	
	}
	
	function editRecord($post){
		$query = "update ".TBL_MENU." set menuName = '".$post['menuName']."', parentId = '".$post['mainMenu']."' where menuId = '".$post['id']."'";
		$sqlMenu = $this->executeQry($query);
		$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been edited successfully.");
		header("Location:editMenu.php?id='".base64_encode($post[id])."'");exit;
	
	}
	
	
	function SortSequence($get){
	  //print_r($get);
	  $sortedArr 	=   explode("=",$get['url']);
	  $sortedIdArr	=	$sortedArr[1];
	  $Sorted_Order_Arr = explode(",",$sortedIdArr);
	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	 
	  $i = 0;
	  $sorted_key_arr	=	array();	 
	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){
	     $current_id	=	$Current_Order_Arr[$i].',';
		 $sql_string	=	"SELECT parentId FROM ".TBL_MENU." WHERE menuId = '$current_id' ";
         $query 	 	=	mysql_query($sql_string);
		 if($line = mysql_fetch_object($query)){
		    $sorted_key_arr[$Sorted_Order_Val] = $line->menuId;
		 }
		 $i++;
       }
	  //print_r($sorted_key_arr);
	  foreach($sorted_key_arr as $key=>$val){  
	        echo $sql_string		=	"UPDATE ".TBL_MENU." SET parentId = '$val' WHERE menuId = '".$key."'";
		   	//mysql_query($sql_string);   
	   }
	    
/*		for($i=30;$i<60;$i++){
		     $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
			 $n_row = mysql_num_rows($Qry);
			 if($n_row > 1){
			   $k = 1;
			   while($data = mysql_fetch_object($Qry)){
			     mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
				 $k++;
			   }
			 }
		}*/
		$query = "SELECT * FROM ".TBL_MENU." WHERE parentId=0 AND status=1 ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"menuId";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;	
				$j = 1;		
				while($line = $this->getResultObject($rst)) {
					
					$sqlSubMenu   = $this->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId='$line->menuId' AND parentId!=0 ");	
					$genTable .= '<div class="column" id="column1">';
					$highlight = $j%2==0?"main-body-bynic":"main-body-bynic2";
					$genTable .= '<div class="'.$highlight.'"><ul><li ><li style="width:140px;">'.$line->menuName.'</li></ul></div>';
					while ($line1 = $this->getResultObject($sqlSubMenu)){
						$currentOrder	.=	$line1->menuId.",";
					}
			    }
			}
			
		}
	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php


	}
	
	function setmenuorder($get){
		$menuOrder =  substr_replace($get[menuorder],"",0,1);
		$menuArr = explode(",",$menuOrder);
		foreach($menuArr as $key=>$value){
			$rst = $this->executeQry("select menuId from ".TBL_MENU." WHERE menuId = '$value' AND parentId = '0' ");
			$row = $this->getTotalRow($rst);
			if($row > 0 ){
				$parentId = $this->fetchValue(TBL_MENU,"menuId","1 and menuId = '".$value."' and parentId = '0'");
				$parentKey = $key;
			}
		}
		unset($menuArr[$parentKey]);
		foreach($menuArr as $key=>$value){
			if($parentId != 0)
			 $sqlSubMenu   = $this->executeQry("UPDATE ".TBL_MENU." SET parentId = '$parentId' WHERE menuId = '$value' AND parentId != '0' ");
		} // end foreach
    }	
	
	
}// End Class
?>	
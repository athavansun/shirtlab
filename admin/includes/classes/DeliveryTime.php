<?php 
session_start();
class DeliveryTime extends MySqlDriver{
	function __construct() {
	    $this->obj = new MySqlDriver;       
    }
    
        function editDeliveryTime($post) {   			    	
            $_SESSION['SESS_MSG'] = "";
            $sql = $this->executeQry("select * from ".TBL_DELIVERY_TIME." where 1");
            $num = $this->getTotalRow($sql);
            if($num > 0) {                        
                while($line = $this->getResultObject($sql)){                                                
                    $query = "update ".TBL_DELIVERY_TIME." set days = '".$post['day_'.$line->id]."', pcs = '".$post['pcs_'.$line->id]."' WHERE id = '".$line->id."' ";                         
                    if($this->executeQry($query)) {
                    	$this->logSuccessFail('1', $query);
						$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
					}else{
						$this->logSuccessFail('0', $query);						
					}
                }  
            }
        }
        
        function editDeliveryCharges($post) {			
            $_SESSION['SESS_MSG'] = "";                        
            $sql = $this->executeQry("select * from ".TBL_DELIVERY_TIME_CHARGES." where 1");
            $num = $this->getTotalRow($sql);
            if($num > 0) {                        
                while($line = $this->getResultObject($sql)){                                  
                    $query = "update ".TBL_DELIVERY_TIME_CHARGES." set charge = '".$post['charge_'.$line->id]."' WHERE id = '".$line->id."' ";
                    if($this->executeQry($query)) {                                                       
                        $this->logSuccessFail('1', $query);
                        $_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
                    } else {                                                                            
                        $this->logSuccessFail('0', $query);
                    }                                 
                }      

            }
        }
        
        function editDeliveryCost($post) {   
            //echo "<pre>"; print_r($post);echo "</pre>"; exit;
            $_SESSION['SESS_MSG'] = "";                        
            $sql = $this->executeQry("select * from ".TBL_DELIVERY_TIME_CHARGES." where 1");
            $num = $this->getTotalRow($sql);
            if($num > 0) {                        
                while($line = $this->getResultObject($sql)){                                                      
                    $query = "update ".TBL_DELIVERY_TIME_CHARGES." set sign = '".$post['charges_'.$line->id]."' WHERE id = '".$line->id."'";
                    $this->executeQry($query);
                    $_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
                }      
                    
            }
        }
        
        function addDeliveryTime($post) {
            $query   = "INSERT INTO ".TBL_DELIVERY_TIME." SET days='".$post['days']."', pcs='".$post['pcs']."'";             
            $result = $this->executeQry($query);
            $curInsId = mysql_insert_id();
            
            $query   = "INSERT INTO ".TBL_DELIVERY_TIME_CHARGES." SET d_id='".$curInsId."', sign='".$post[sign]."'";             
            $result = $this->executeQry($query);
            $_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
            header("location:manageDeliveryTime.php");
            exit;
        }
        
		function deleteDeliveryTime($get) {
        	$sql = " DELETE FROM  ".TBL_DELIVERY_TIME." WHERE id = '$get[id]' ";
			$rst = $this->executeQry($sql);
			
			$sql = " DELETE FROM  ".TBL_DELIVERY_TIME_CHARGES." WHERE d_id = '$get[id]' ";
			$rst = $this->executeQry($sql);
			
			if($rst){
					$this->logSuccessFail('1',$query);		
				}else{ 	
					$this->logSuccessFail('0',$query);
				}
			$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
			echo "<script language=javascript>window.location.href='manageDeliveryTime.php?page=$get[page]&limit=$get[limit]';</script>";
        }
}
    
    
	

<?php 
session_start();
class Color extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;
    }

	function addColorPallet($post)
	{
//		print_r($post);exit;
		$xmlArr = array();
		$xm = 1;
		$query = "insert into ".TBL_PALLET." set  status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."', isDeleted = '0' ";
		
		$res = $this->executeQry($query);
		$inserted_id = mysql_insert_id();
		
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $inserted_id;
		$xmlArr[$xm]['section'] = "insert";
		$xm ++;
		
		if($res) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
			
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$palletName = 'palletName_'.$line->id;
				$query = "insert into ".TBL_PALLETDESC." set palletId  = '$inserted_id', langId = '".$line->id."', palletName = '".addslashes($post[$palletName])."' ";	
				if($this->executeQry($query)) 
					$this->logSuccessFail('1',$query);		
				else 	
					$this->logSuccessFail('0',$query);
			}	
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		
		$queryVal = Array();
		for($i = 0; $i < 30; $i++)
		{
			$queryVal[] = "('".$inserted_id."')";			
		}
		$queryField = implode(', ',$queryVal);
		
		$palletQuery = "Insert into ".TBL_PALLETCOLOR." (palletId) values ".$queryField;
		$this->executeQry($palletQuery);
		
		
							
		$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		header("Location:addColor.php?palletId=$inserted_id");
		exit;
	}
	
	function getColorBox($palletId)
	{
		$output = '';
		$ctr = 1;
		$query = "select * from ".TBL_PALLETCOLOR." where palletId = '".$palletId."'";
		$result = $this->executeQry($query);
		$num = $this->getTotalRow($result);
		if($num > 0)
		{
			$output .='<tr>';
			while($line = $this->getResultObject($result))
			{                            
				$image = ''; $class = '';
				if($line->colorImage != '')
					$image = '<img width="50" height="45" src="'.SITE_URL.__PALLETIMGTHUMB__.$line->colorImage.'" />';
				else
					$class = 'style="background-color:'.$line->colorCode.';"';
					
				$output .='<td>
							<a href="#" onclick="return showLoadImage(\''.$line->id.'\');" >
								<span '.$class.' id="imgClass" class="imgClass">								
									'.$image.'
								</span>
							</a>
							<input type="hidden" name="oldImage'.$line->id.'" id="oldImage'.$line->id.'" value="'.$line->colorImage.'" />
							<input type="hidden" name="pCode'.$line->id.'" id="pCode'.$line->id.'" value="'.$line->code.'" />
							<input type="hidden" name="pColorCode'.$line->id.'" id="pColorCode'.$line->id.'" value="'.$line->colorCode.'" />
							<br>'.$line->code.'
						</td>';
						
				if($ctr % 6 == 0 && $ctr != $num)
					$output .='</tr><tr>';
					
				$ctr ++;
			}
			$output .='</tr>';
		}
		else
			$output = '<tr><td colspan="6">Invalid Data</td></tr>';
		
		return $output;
	}
	
	function updatePalletImage($post,$file)
	{
            //print_r($file);
//            print_r($post);exit;
            #-------for image-----------
            $xmlArr = array();
            $xm = 1;
		

            $query = "Update ".TBL_PALLETCOLOR." set code = '".$post['code']."', colorCode = '".$post['colorCode']."' where id = '".$post['palletBoxId']."'";
          //  $query = "Update ".TBL_PALLETCOLOR." set colorCode = '".$post['colorCode']."' where id = '".$post['palletBoxId']."'";
    $this->executeQry($query);
		  
            $xmlArr[$xm]['query'] = addslashes($query);
            $xmlArr[$xm]['identification'] = $post['palletBoxId'];
            $xmlArr[$xm]['section'] = "update";
            $xm ++;

            $xmlInfoObj = new XmlInfo();
            $xmlInfoObj->addXmlData($xmlArr);
		
            if($file['colorImage']['name']){
                $filename = stripslashes($file['colorImage']['name']);
                $extension = findexts($filename);
                $extension = strtolower($extension);

                $image_name = date("Ymdhis").time().rand().'.'.$extension;
                $target    =ABSOLUTEPATH. __PALLETIMGORIG__.$image_name;
                if($this->checkExtensions($extension)) {
                $filestatus = move_uploaded_file($file['colorImage']['tmp_name'], $target);

                if($filestatus){
                $imgSource = $target;
                $ThumbImage = ABSOLUTEPATH.__PALLETIMGTHUMB__.$image_name;

                $query = "Update ".TBL_PALLETCOLOR." set colorImage = '".$image_name."' where id = '".$post['palletBoxId']."'";
                $this->executeQry($query);

                $fileSize = '35x30!';
                exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");
                $_SESSION['SESS_MSG'] .= msgSuccessFail('success',"Operation successfull");
                }
                else
                {
                  $_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
                }
                } else {
                $_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
                }
            } 
		
	}
	
	function getPalletName($pid=''){	    
	    
	    $output = '';
		$query = "Select P.id, PD.palletName from ".TBL_PALLET." as P INNER JOIN ".TBL_PALLETDESC." as PD ON (P.id = PD.palletId) where P.isDeleted = '0' and PD.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$rst = $this->executeQry($query);
		$row = $this->getTotalRow($rst);
		if($row > 0)
		{
                    while($line = $this->getResultObject($rst))	{
                        if($pid == $line->id){ $sel = "selected = 'selected'";}else{ $sel="";}
                        $output .='<option value="'.$line->id.'"'.$sel.' >'.$line->palletName.'</option>';
                    }
		}
		
		return $output;		
	}
	
	function showSinglePallet($palletId)
	{
            $output = '';
            $ctr = 1;
            $query = "select * from ".TBL_PALLETCOLOR." where palletId = '".$palletId."'";
            $result = $this->executeQry($query);
            $num = $this->getTotalRow($result);
            $output .='<table style="width:600px;">';
            if($num > 0)
            {
                    $output .='<tr>';
                    while($line = $this->getResultObject($result))
                    {
                        $image = ''; $class = '';
                        if($line->colorImage != '')
                                $image = '<img width="50" height="45" src="'.SITE_URL.__PALLETIMGTHUMB__.$line->colorImage.'" />';
                        else
                                $class = 'style="background-color:'.$line->colorCode.';"';

                        $output .='<td>							
                                        <span '.$class.' id="imgClass" class="imgClass">								
                                                '.$image.'
                                        </span>
                                        <br>'.$line->code.'
                                    </td>';

                        if($ctr % 6 == 0 && $ctr != $num)
                                $output .='</tr><tr>';

                        $ctr ++;
                    }
                    $output .='</tr>';
                    $output .= '<tr><td colspan="6"><input type="button" onclick="return removeColorPallet(\''.$palletId.'\');" value="Delete" /></td></tr>';
            }
            else
                    $output .= '<tr><td colspan="6">No enough Data</td></tr>';

            $output .='</table>';

            echo $output;
	}
	
	function isPalletNameExist($palletName, $langId, $id){            
	    if($id != 0)
                    $cond = " AND palletId !='$id' ";
		else
                    $cond = "";
		$palletName = trim($palletName);
                //$query = "Select id from ".TBL_PALLETDESC." where palletName = '".$palletName."' and langId = '".$langId."' $cond";
		$query = "Select id from ".TBL_PALLETDESC." where palletName = '".$palletName."' and langId = '".$langId."' $cond";
		$rst = $this->executeQry($query);
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
	function updateColorPallet($post)
	{
		$xmlArr = array();
		$xm = 1;
		//print_r($post); exit;
		$rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $categoryName = 'palletName_' . $line->id;
                $sql = $this->selectQry(TBL_PALLETDESC, '1 and palletId = "' .$post[id]. '" and langId = "' . $line->id . '"', '', '');
                $numrows = $this->getTotalRow($sql);
                if ($numrows == 0) {
                    $query = "insert into " . TBL_PALLETDESC . " set palletId = '$post[id]', langId = '" . $line->id . "', palletName = '" .$post[$categoryName] . "'";
					$insert_id = mysql_insert_id();
                    
                    $xmlArr[$xm]['query'] = addslashes($query);
                    $xmlArr[$xm]['identification'] = $insert_id;
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;

                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                } else {
                    $query = "update " . TBL_PALLETDESC . " set palletName = '" . addslashes($post[$categoryName]) . "' where 1 and palletId = '$post[id]' and langId = '" . $line->id . "'";

                    $xmlArr[$xm]['query'] = addslashes($query);
                    $xmlArr[$xm]['identification'] = $post['id'];
                    $xmlArr[$xm]['section'] = "update";
                    $xm++;

                    $xmlInfoObj = new XmlInfo();
					$xmlInfoObj->addXmlData($xmlArr);
					
                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                }
            }
        }
	}
	
	function removePallet($palletId)
	{
		$xmlArr = array();
		$xm = 1;
		
		if($palletId != '')
		{
			$query = "update ".TBL_PALLET ." set isDeleted = '1' where id = '".$palletId."'";
			$conf = $this->executeQry($query);
			
			$xmlArr[$xm]['query'] = addslashes($query);
            $xmlArr[$xm]['identification'] = $palletId;
            $xmlArr[$xm]['section'] = "update";
            $xm++;

			$xmlInfoObj = new XmlInfo();
			$xmlInfoObj->addXmlData($xmlArr);
			if($conf)
				echo "success";
			else
				echo "fail";			
		}
	}
	
	function valDetail() {
        $cond = "1 and ft.id = ftd.id  and ftd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (ftd.colorName LIKE '%$searchtxt%' )";
        }

        $query = "select ft.*,ftd.colorName from " . TBL_COLOR . " as ft , " . TBL_COLORDESC . " as ftd where $cond ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;

        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $this->setLimit($_GET[limit]);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------

            $orderby = $_GET[orderby] ? $_GET[orderby] : "id";
            $order = $_GET[order] ? $_GET[order] : "DESC";

            $query .= " ORDER BY ftd.$orderby $order LIMIT " . $offset . ", " . $recordsPerPage;
            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);

            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    $div_id1 = "default" . $line->id;

                    if ($line->status == 0)
                        $status = 'Inactive';
                    else
                        $status = 'Active';

                    if ($line->isDefault == 1) {
                        $isDefault = '<input type="radio" checked="checked" class="welcheckbox" name="default[]" >';
                        $onclickstatus = '';
                        $onclickdefault = '';
                        $chkbox = '';
                    } else {
                        $isDefault = '<input type="radio" class="welcheckbox" name="default[]" >';
                        $onclickstatus = ' onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'Color\')"';
                        $onclickdefault = ' onClick="javascript:changeDefault(\'' . $div_id1 . '\',\'' . $line->id . '\',\'Color\')"';
                        $chkbox = '<input name="chk[]" value="' . $line->id . '" type="checkbox" class="checkbox">';
                    }


//                    $genTable .= '<div  style="width:25px;background-color:' . '#' . $line->colorCode . ';" >&nbsp;</div>(' . '#' . $line->colorCode . ')</td>';
                    $genTable .= '<tr class="' . $highlight . '"><th>&nbsp;&nbsp;<input name="chk[]" value="' . $line->id . '" type="checkbox"></th><td>' . $i . '</td><td>' . substr($line->colorCode, 0, 40) . '</td>';
                    $genTable .='<td><img src="../'.__COLORTHUMB__.$line->colorImage.'" border = "0" alt="'.stripslashes($line->name).'"></td>';

                    if ($menuObj->checkEditPermission()){
                        $genTable .='<td><div id="' . $div_id . '" style="cursor:pointer;" ' . $onclickstatus . '>' . $status . '</div></td>';
                    }
                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<td><a class="i_pencil edit" href="editColor.php?id=' . base64_encode($line->id) . '&page=' . $page . '"></a></td>';
                    }
                    //$genTable .= '<td><a rel="shadowbox;width=705;height=325" title="'.$line->colorName.'" href="viewColor.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';

                    $genTable .= '<td>';
                    if ($menuObj->checkDeletePermission() && $line->isDefault != 1)
                        $genTable .= "<div id=\"" . $div_id1 . "_del\"><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this record!')){window.location.href='pass.php?action=Color&type=delete&id=" . $line->id . "&page=$page'}else{}\" ></a><div>";
                    $genTable .= '</td>';

                    $genTable .= '</tr>';

                    $i++;
                }

                switch ($recordsPerPage) {
                    case 10:
                        $sel1 = "selected='selected'";
                        break;

                    case 20:
                        $sel2 = "selected='selected'";
                        break;

                    case 30:
                        $sel3 = "selected='selected'";
                        break;

                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }

                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
			<table border='0' width='88%' height='50'>
			<tr>
			<td align='left' width='300' class='page_info' 'style=margin-left=20px;'>Display
			<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
			<option value='10' $sel1>10</option>
			<option value='20' $sel2>20</option>
			<option value='30' $sel3>30</option> 
			<option value='" . $totalrecords . "' $sel4>All</option>
			</select>Records Per Page
			</td>
			<td align='center' class='page_info'><inputtype='hidden' name='page' value='" . $currpage . "'></td>
			<td class='page_info' align='center' width='200'>Total Records Found : " . $totalrecords . "</td>
			<td width='0' align='right'>" . $pagenumbers . "</td>
			</tr>
			</table>
			</div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry! No Record Found.</div>';
        }

        return $genTable;
    }
	
 
	function addRecord($post) {
			$xmlArr = array();
			$xm = 1;
		   			  
		    $query = "insert into ".TBL_COLOR." set  status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."', colorCode = '".substr_replace($post['colorCode'], '', 0, 1)."'";
			$res = $this->executeQry($query);
			$inserted_id = mysql_insert_id();
			
			$xmlArr[$xm]['query'] = addslashes($query);
			$xmlArr[$xm]['identification'] = $inserted_id;
			$xmlArr[$xm]['section'] = "insert";
			$xm ++;
			
			if($res) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$colorName = 'colorName_'.$line->id;
					$query = "insert into ".TBL_COLORDESC." set  Id  = '$inserted_id', langId = '".$line->id."', colorName = '".addslashes($post[$colorName])."' ";	
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}
			$xmlInfoObj = new XmlInfo();
			$xmlInfoObj->addXmlData($xmlArr);					
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
			header("Location:addColor.php");exit;
				
			
	}
 
	 
	function changeValueStatus($get) {
	
		$xmlArr = array();
		$xm = 1;
		
		$status=$this->fetchValue(TBL_COLOR,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$query = "update ".TBL_COLOR." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		
		
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $get[id];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;
			
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
 /// For Change Color Status
    function changeValueDefault($get) {
        $status = $this->fetchValue(TBL_COLOR, "isDefault", "1 and id = '$get[id]'");

        if ($status == 0) {
            $stat = 1;
            $status = '<input type="radio" checked="checked" class="welcheckbox" name="default[]" >';
        }

        $query = "update " . TBL_COLOR . " set isDefault = '0', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where 1";
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);

        $query = "update " . TBL_COLOR . " set isDefault = '$stat', status='1', modDate = '" . date('Y-m-d') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);

        //echo $status;	
        echo 'manageColor.php'; //?page=$post[page]';
    }
	
	function deleteAllValues($post){
	
		$xmlArr = array();
		$xm = 1;
			
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageColor.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$queryFind = "select * from ".TBL_COLOR." where id='".$val."'";
					$sqlFind = $this->executeQry($queryFind);
					$lineFind = $this->getResultObject($sqlFind);
					if($lineFind->isDefault != 1){
				    	$result=$this->deleteRec(TBL_COLORDESC,"id='".$val."'");	
						$result1=$this->deleteRec(TBL_COLOR,"id='".$val."'");
						$xmlArr[$xm]['query'] = addslashes($result);
						$xmlArr[$xm]['identification'] = $val;
						$xmlArr[$xm]['section'] = "delete";
						$xm ++;
					}	
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
 
					$sql="update ".TBL_COLOR." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COLOR." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;
			
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		
		echo "<script language=javascript>window.location.href='manageColor.php?page=$post[page]';</script>";
	}
	
	
	/// For Delete Single Forum Topic
	
	function deleteValue($get) {
		$sql = TBL_COLOR.", id='".$get['id']."'";
		$result=$this->deleteRec($sql);	
		$xmlArr[$xm]['query'] = addslashes($sql);
		$xmlArr[$xm]['identification'] = $get[id];
		$xmlArr[$xm]['section'] = "delete";
		$xm ++;
		$sql = TBL_COLORDESC.",id='".$get['id']."'";
		$result1=$this->deleteRec($sql);
		$xmlArr[$xm]['query'] = addslashes($sql);
		$xmlArr[$xm]['identification'] = $get[id];
		$xmlArr[$xm]['section'] = "delete";
		$xm ++;
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);		
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageColor.php?page=$get[page]&limit=$get[limit]';</script>";
	}
	
	/// Get Information About Existing color
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_COLOR." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageColor.php");
		}	
	}
	
	// Edit color
		
	function editRecord($post) {
	
		$xmlArr = array();
		$xm = 1;
	   
	    $isdefault = $post[isDefault]?1:0;
	    if($isdefault){
	    	$this->executeQry("update ".TBL_COLOR." set isDefault='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id != '$post[id]'");
		 	$con =", isDefault = '1'";
		}else{
		 	$con="";
		}
		
		$queryUpdate = "update ".TBL_COLOR." set colorCode = '".substr_replace($post[colorCode],'', 0, 1)."' ".$con." where 1 and id = '$post[id]' ";
		
		$xmlArr[$xm]['query'] = addslashes($queryUpdate);
		$xmlArr[$xm]['identification'] = $inserted_id;
		$xmlArr[$xm]['section'] = "insert";
		$xm ++;
		
			if($this->executeQry($queryUpdate)) 
				$this->logSuccessFail('1',$queryUpdate);		
			else 	
				$this->logSuccessFail('0',$queryUpdate);
				
				
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$colorName = 'colorName_'.$line->id;
				$sql = $this->selectQry(TBL_COLORDESC,'1 and id = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) { 
					$query = "insert into ".TBL_COLORDESC." set id = '$post[id]', langId = '".$line->id."', colorName = '".$post[$colorName]."' ";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_COLORDESC." set colorName = '".addslashes($post[$colorName])."' where 1 and id = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		

					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
		}
			$xmlInfoObj = new XmlInfo();
			$xmlInfoObj->addXmlData($xmlArr);	
		echo "<script>window.location.href='manageColor.php?page=$post[page]';</script>";
	}
        
        function deletePalletColor($get_data) {
            $flag = false;
            $res = $this->executeQry(" update ".TBL_PALLETCOLOR." set code='', colorCode='', colorImage='' where id = '".$_GET['id']."'");
            if($res) {
                $flag = true;
            }
            echo $flag;
        }
	
}// End Class
?>	

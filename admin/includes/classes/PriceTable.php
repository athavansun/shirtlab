<?php 
session_start();
class PriceTable extends MySqlDriver{

	function __construct() {
	  $this->obj = new MySqlDriver;  
	    
    }

// EMBROIDERY : DETAIL ON MANAGE PAGE===================================================================

	function valDetail() {	

		$menuObj =  new Menu;

		$cond = " 1=1 "; 
		$cond .=" AND ".TBL_DECORATION_PRICE_TABLE.".fulfillment_id='".$_SESSION['FULFILLMENTID']."'";
		

		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){

			$searchtxt = $_REQUEST['searchtxt'];

			$cond .= " AND (".TBL_DECORATION_PRICE_TABLE.".`name` LIKE '%$searchtxt%') ";

		}

		
		$query= " SELECT * FROM  `".TBL_DECORATION_PRICE_TABLE."` WHERE ".$cond;
		//echo $query;exit;
		
		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		$page =  $_REQUEST['page']?$_REQUEST['page']:1;

		if($num > 0) {			

			//-------------------------Paging------------------------------------------------			

			$paging = $this->paging($query); 

			$this->setLimit($_GET[limit]); 

			$recordsPerPage = $this->getLimit(); 

			$offset = $this->getOffset($_GET["page"]); 

			$this->setStyle("redheading"); 

			$this->setActiveStyle("smallheading"); 

			$this->setButtonStyle("boldcolor");

			$currQueryString = $this->getQueryString();

   			$this->setParameter($currQueryString);

			$totalrecords = $this->numrows;

			$currpage = $this->getPage();

			$totalpage = $this->getNoOfPages();

			$pagenumbers = $this->getPageNo();		

			//-------------------------Paging------------------------------------------------

			$orderby = $_GET[orderby]? $_GET[orderby]:"name";

		    $order = $_GET[order]? $_GET[order]:"ASC";   

            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			

			$rst = $this->executeQry($query); 

			$row = $this->getTotalRow($rst);

			if($row > 0) {			

				$i = 1;			

				while($line = $this->getResultObject($rst)) {
					//echo "<pre>"; print_r($line);continue;exit;

					$currentOrder	.=	$line->id.","; 
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					/// Name Section ====================
					$genTable .= '<tr class="'.$highlight.'" id="'.$line->id.'" >
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$line->id.'</td>
									<td>'.stripslashes($line->name).'</td>';
						
					/// Status Section : Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'EPT\')">'.$status.'</div>';
					$genTable .= '</td>';
					
					
					/// View  Section ====================				
					$genTable .= '<td><a rel="shadowbox;width=705;height=325" title="'.stripslashes($line->fullName).'" href="viewEmbroideryPriceTable.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0" alt="View"></a></td>';
					
					
					/// Edit Section : Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a class="i_pencil edit" href="editEmbroideryPriceTable.php?id='.base64_encode($line->id).'&page='.$page.'" title="Edit" alt="Edit">edit</a>';
					$genTable .= '</td>';
					
					
					/// Delete Section: Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=EPT&type=delete&id=".$line->id."&page=$page'}else{}\" > delete </a>";
					$genTable.='</td>'; 
					// Close Row========================
					$genTable.='</tr>';
					$i++;	

				}
				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}

				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;

				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td> <td class='page_info' align='center' width='200'><inputtype='hidden' name='page' value='".$currpage."'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
		}	
		return $genTable;

	}
	
/// EMBROIDERY : ADD RECORD=============================================================================

	function addRecord($post){
		//echo "<pre>";print_r($_REQUEST);exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_DECORATION_PRICE_TABLE;
		$this->field_values['name'] = $post['embroideryName'];
		$this->field_values['currency_id'] = $_SESSION['DEFAULTCURRENCYID'];
		$this->field_values['fulfillment_id'] = $_SESSION['FULFILLMENTID'];
		$this->field_values['status'] = '1';
		$this->field_values['row'] = $post['row_val'];
		$this->field_values['col'] = $post['col_val'];
		$res = $this->insertQry();
		$insertId = $res[1];
		
		if($insertId){
			for($i=0;$i<$post['row_val'];$i++){
				for($j=0;$j<$post['col_val'];$j++){
				
					$SF=$post["SF_".$i];
					$ST=$post["ST_".$i];
					$QF=$post["QF_".$j];
					$QT=$post["QT_".$j];
					$STP=$post["STP_".$i.$j];				
					/// Unable to use above used method for insert query//// ??
					$query="insert into ".TBL_DECORATION_PRICE_TABLE_DATA." set price_table_id='".$insertId."',stitch_from='".$SF."' , stitch_to='".$ST."' ,quantity_from='".$QF."' , quantity_to='".$QT."' ,price='".$STP."' , rowId='".$i."', colId='".$j."'";
					$rst = $this->executeQry($query);
							
				}
			
				
			}
			
		}
		
		if($insertId){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add record.");
		}	
			
		
		header("Location:addEmbroideryPriceTable.php");exit;
		
	}	

/// EMBROIDERY : EDIT RECORD============================================================================
	
	function editRecord($post){
	//echo "<pre>"; print_r($post); echo "</pre>";exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_DECORATION_PRICE_TABLE;
		$this->field_values['name'] = $post['embroideryName'];
		$this->field_values['currency_id'] = $_SESSION['DEFAULTCURRENCYID'];
		$this->field_values['fulfillment_id'] = $_SESSION['FULFILLMENTID'];		
		$this->field_values['row'] = $post['row_val'];
		$this->field_values['col'] = $post['col_val'];
		$this->condition  = "id = '$post[id]'";
		
		$res = $this->updateQry();
		
		
		/// Delete Prices related particular id from price_table_data
			
		$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA,"price_table_id = '$post[id]'");
		// insert new values in price_table_data
		if($rst){
			for($i=0;$i<$post['row_val'];$i++){
				for($j=0;$j<$post['col_val'];$j++){
				
					$SF=$post["SF_".$i];
					$ST=$post["ST_".$i];
					$QF=$post["QF_".$j];
					$QT=$post["QT_".$j];
					$STP=$post["STP_".$i.$j];				
					/// Unable to use above used method for insert query//// ??
					$query="insert into ".TBL_DECORATION_PRICE_TABLE_DATA." set price_table_id='".$post[id]."',stitch_from='".$SF."' , stitch_to='".$ST."' ,quantity_from='".$QF."' , quantity_to='".$QT."' ,price='".$STP."' , rowId='".$i."', colId='".$j."'";
					$rst = $this->executeQry($query);
							
				}
			
				
			}
			
		}
		if($rst){

			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");

		}else{

			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");

		}	

		header("Location:manageEmbroideryPriceTable.php");exit;
	}		
	
///EMBROIDERY : Add Price Table=========================================================================
	function showTable($get){
				//echo "<pre>"; print_r($_POST);exit;
				$row=$get[row];
				$col=$get[col];
				$Tbl="";
				if($row !='' && $row !='0' && $col !='' &&  $col !=''){
	
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From</td>';
					  for($i=0; $i<$col; $i++){
					  	$QF="QF_".$i;
			if($i==0)
			{
			
			$Tbl.='<td><span id="m__QF_'.$i.'" >1</span><input name="QF_'.$i.'" type="hidden"  id="m__QFH_'.$i.'"   value="1" /></td>';
			}
			else
			{
			$Tbl.='<td><input type="text" value="'.($i*5).'" name="QF_'.$i.'" id="m__QF_'.$i.'" class="quanbalance"   /></td>';
			}
									  
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  for($i=0; $i<$col; $i++){
				if($i==0)
			{
			
			$Tbl.='<td><span id="m__QT_'.$i.'" >4</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="4" /></td>';
			}
			else
			{
			if($i==($col-1))
			{
			$vs='>';
			}
			$Tbl.='<td >'.$vs.'<span id="m__QT_'.$i.'" >'.(($i*5)+4).'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.(($i*5)+4).'" /></td>';
			}
					
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'"> Stitiches </td></tr>
					  <tr>
					  <td>From</td>
					  <td>To</td>
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					 for($i=0; $i<$row; $i++){
					 $stclass='';
					 $Tbl.='<tr>';
					
					 for($j=0; $j<($col+2); $j++){
					 if($j==0){
					 $name="SF_".$i;	
					  $stclass='stbalance';
					   $disables='';	
					    if($i==0)
						{
						$val=0;
						}
						else{
						$val=$val+1;
						}
				$Tbl.='<td>
				<span id="m__'.$name.'" >'.$val.'</span><input  type="hidden"  id="m__SFH_'.$i.'" name="'.$name.'"  value="'.$val.'" />
				</td>';
					 }
					 else if($j==1){$name="ST_".$i;	
					  $val=($i+1)*1000;
						
					
				$Tbl.='<td><input type="text" name="'.$name.'" id="m__'.$name.'" class="stbalance"  value="'.$val.'"   /></td>';
					  }
					 		 
					 else {
					 
					 $name="STP_".($i).($j-2);
					 	
			$Tbl.='<td><input type="text" name="'.$name.'" id="m__'.$name.'" class="stprice"   /></td>';
					  }
					  
					 
					 
					 }
					  
					 $Tbl.='</tr>';
					 
					 }
					  
					  
				  $Tbl.='</table>';
			  }
			  
			  
		echo $Tbl;
	
	}
	
///EMBROIDERY : Edit Price Table=========================================================================

	function showTableEdit($get){
				$row=$get[row];
				$col=$get[col];
				$id=$get[id];
				$rst = $this->executeQry("select DPT.name,DPT.row, DPT.col, DPTD.* from ".TBL_DECORATION_PRICE_TABLE." as DPT inner join ".TBL_DECORATION_PRICE_TABLE_DATA." as DPTD  on (DPT.id=DPTD.price_table_id) where DPT.id = '$id'");

				$num = $this->getTotalRow($rst);
				while($line=$this->getResultObject($rst)){
				$priceArr[]=$line;	
				$SF[$line->rowId]=$line->stitch_from;	
				$ST[$line->rowId]=$line->stitch_to;	
				$QF[$line->colId]=$line->quantity_from ;	
				$QT[$line->colId]=$line->quantity_to;		
				$STP[$line->rowId][$line->colId]=$line->price;			
				}
				//echo "<pre>";print_r($priceArr);print_r($QT);
				//echo $pk;
				
				
				//print_r($priceArr);
				$Tbl="";
				$preQFarr=array();
				if($row !='' && $row !='0' && $col !='' &&  $col !=''){
				
				
				
	
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From</td>';
					  $perval='';
					  $isone=1;
					  for($i=0; $i<$col; $i++){
					  	//$QF="QF_".$i;
					  $valQF=$QF[$i];
					  if($valQF=='')
					  {
					  if($isone==1)
					  {
						 $isone=0;
					
					   $valQF= $QT[$i-1]+1;
					   $preQFarr[$i]= $QT[$i-1]+1;
					  }
					  else
					  {
					
					   $valQF=$perval+5;
					   $preQFarr[$i]=$perval+5;
					   }
					  }
			if($i==0)
			{
			
			$Tbl.='<td><span id="m__QF_'.$i.'" >'.$valQF.'</span><input name="QF_'.$i.'" type="hidden"  id="m__QFH_'.$i.'"   value="'.$valQF.'" /></td>';
			}
			else
			{
			$Tbl.='<td><input type="text" value="'.$valQF.'" name="QF_'.$i.'" id="m__QF_'.$i.'" class="quanbalance"   /></td>';
			}
							    $perval=$valQF	;	  
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  for($i=0; $i<$col; $i++){
					  	  $valQT=$QT[$i];
					  if($valQT=='')
					  {
					  $valQT=$preQFarr[$i]+4;
					  }
				if($i==0)
			{
			
			$Tbl.='<td><span id="m__QT_'.$i.'" >'.$valQT.'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.$valQT.'" /></td>';
			}
			else
			{
			if($i==($col-1))
			{
			$vs='>';
			}
			$Tbl.='<td >'.$vs.'<span id="m__QT_'.$i.'" >'.$valQT.'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.$valQT.'" /></td>';
			}
					
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'"> Stitiches </td></tr>
					  <tr>
					  <td>From</td>
					  <td>To</td>
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					   $perval='';
					 for($i=0; $i<$row; $i++){
					 $stclass='';
					 $Tbl.='<tr>';
					 
					 
					 for($j=0; $j<($col+2); $j++){
					 if($j==0){
					 $name="SF_".$i;	
					  $stclass='stbalance';
					   $disables='';	
					 
						$valSF=$SF[$i];
						if($valSF=='')
						{
						$valSF=$perval+1;
						}
					  
				$Tbl.='<td>
				<span id="m__'.$name.'" >'.$valSF.'</span><input  type="hidden"  id="m__SFH_'.$i.'" name="'.$name.'"  value="'.$valSF.'" />
				</td>';
				$perval=$valSF;
					 }
					 else if($j==1){$name="ST_".$i;	
					  $val=($i+1)*1000;
						$valST=$ST[$i];
						if($valST=='')
						{
						$valST=$perval+999;
						}
					
				$Tbl.='<td><input type="text" name="'.$name.'" id="m__'.$name.'" class="stbalance"  value="'.$valST.'"   /></td>';
				$perval=$valST;
					  }
					 		 
					 else {
					 
					 $name="STP_".($i).($j-2);
					 	
			$Tbl.='<td><input type="text" name="'.$name.'" id="m__'.$name.'" class="stprice" value="'.$STP[$i][$j-2].'"   /></td>';
					  }
					  
					 
					 
					 }
					  
					 $Tbl.='</tr>';
					 
					 }
					  
					  
				  $Tbl.='</table>';
			  
				
				//////////////////////////ffffffffffffffff/////////////////
				
			
				}
			  
			  
		echo $Tbl;
				
	
	}	
	
///EMBROIDERY : View Price Table=========================================================================
	function showTableView($get){
				$row=$get[row];
				$col=$get[col];
				$id=$get[id];
				$rst = $this->executeQry("select DPT.name,DPT.row, DPT.col, DPTD.* from ".TBL_DECORATION_PRICE_TABLE." as DPT inner join ".TBL_DECORATION_PRICE_TABLE_DATA." as DPTD  on (DPT.id=DPTD.price_table_id) where DPT.id = '$id'");

				$num = $this->getTotalRow($rst);
				while($line=$this->getResultObject($rst)){
				$priceArr[]=$line;	
				$SF[$line->rowId]=$line->stitch_from;	
				$ST[$line->rowId]=$line->stitch_to;	
				$QF[$line->colId]=$line->quantity_from ;	
				$QT[$line->colId]=$line->quantity_to;		
				$STP[$line->rowId][$line->colId]=$line->price;			
				}
				//echo "<pre>";print_r($priceArr);print_r($QT);
				//echo $pk;
				
				
				//print_r($priceArr);
				$Tbl="";
				if($row !='' && $row !='0' && $col !='' &&  $col !=''){
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From</td>';
					  for($i=0; $i<$col; $i++){
					  //$val="SF_".$i;
					  //$$val='5';
					  
					$Tbl.='<td>'.$QF[$i].'</td>';				  
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  for($i=0; $i<$col; $i++){
					  $Tbl.='<td>'.$QT[$i].'</td>';
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'">Stitiches</td></tr>
					  <tr>
					  <td>From</td>
					  <td>To</td>
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					 for($i=0; $i<$row; $i++){
					 $Tbl.='<tr>';
					 for($j=0; $j<($col+2); $j++){
					 if($j==0){$name="SF_".$i;$val=$SF[$i];	}
					 else if($j==1){$name="ST_".$i;$val=$ST[$i];}		 
					 else {$name="STP_".($i).($j-2);$val=$STP[$i][$j-2];}
					  
					 
					 $Tbl.='<td>'.$val.'</td>';
					 }
					  
					 $Tbl.='</tr>';
					 
					 }
					$Tbl.='</table>';  
				}
			  
			  
		echo $Tbl;
				
	
	}	
	
///EMBROIDERY : Delete Single Value=======================================================================
		function deleteValue($get){
		
			$rst=$this->deleteRec(TBL_DECORATION_PRICE_TABLE,"id = '$get[id]'");
			if($rst){
					$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA,"price_table_id = '$get[id]'");
							

				}else{ 	

					$this->logSuccessFail('0',$sql);

				}

			$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");

			echo "<script language=javascript>window.location.href='manageEmbroideryPriceTable.php?page=$get[page]&limit=$get[limit]';</script>";exit;

	}	
	
// EMBROIDERY : Delete, Active/Inactive Multiple Values ==================================================
	function deleteAllValues($post){
	//echo"<pre>"; print_r($post);exit;

		if(($post[action] == '')){

		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records, And then submit!!!");
			echo "<script language=javascript>window.location.href='manageEmbroideryPriceTable.php?page=$post[page]&limit=$post[limit]';</script>";exit;

		}				

		if($post[action] == 'deleteselected'){
		//echo "yes";exit;

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){

					
					$rst=$this->deleteRec(TBL_DECORATION_PRICE_TABLE,"id = '$val'");
					
					if($rst){
						$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA,"price_table_id = '$val'");
						

					}else{

						$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");

			}else{

			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		if($post[action] == 'enableall'){

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){
				
					$sql="UPDATE ".TBL_DECORATION_PRICE_TABLE." set status ='1'  where id='$val'";
					
					$rst = $this->executeQry($sql);

					if($rst){

					$this->logSuccessFail("1",$sql);

					}else{

					$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");

			}else{

			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		if($post[action] == 'disableall'){

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){

					$sql="UPDATE ".TBL_DECORATION_PRICE_TABLE." set status ='0' where id='$val'";

					$rst = $this->executeQry($sql);

					if($rst){

					$this->logSuccessFail("1",$sql);

					}else{

					$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");

			}else{

				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		echo "<script language=javascript>window.location.href='manageEmbroideryPriceTable.php?page=$post[page]&limit=$post[limit]';</script>";exit;

	}	
	
	
// EMBROIDERY : GET RESULT BY ID===========================================================================
	function getResult($id) {

		$sql = $this->executeQry("select * from ".TBL_DECORATION_PRICE_TABLE." where id = '$id'");
		
		$num = $this->getTotalRow($sql);

		if($num > 0) {				

			return $line = $this->getResultObject($sql);	

		} 

	}
	
// EMBROIDERY : Change Value Status========================================================================
	function changeValueStatus($get) {

		

		$status=$this->fetchValue(TBL_DECORATION_PRICE_TABLE,"status","1 and id = '$get[id]'");

		

		if($status==1) {

			$stat= 0;

			$status="Inactive";

		} else 	{

			$stat= 1;

			$status="Active";

		}

		$query = "update ".TBL_DECORATION_PRICE_TABLE." set status = '$stat'  where id = '$get[id]'";
		
		if($this->executeQry($query)) 

			$this->logSuccessFail('1',$query);		

		else 	

			$this->logSuccessFail('0',$query);

		echo $status;		

	}
	
	
	
	
//// ************************************** SCREEN PRINTING PRICE TABLE FUNCTTIONS****************************************//////



	
// SCREEN PRINTING : DETAIL ON MANAGE PAGE===================================================================

	function valDetailSP() {	

		$menuObj =  new Menu;

		$cond = " 1=1 "; 
		$cond .=" AND ".TBL_DECORATION_PRICE_TABLE_SCREEN.".fulfillment_id='".$_SESSION['FULFILLMENTID']."'";
		

		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){

			$searchtxt = $_REQUEST['searchtxt'];

			$cond .= " AND (".TBL_DECORATION_PRICE_TABLE_SCREEN.".`name` LIKE '%$searchtxt%') ";

		}

		
		$query= " SELECT * FROM  `".TBL_DECORATION_PRICE_TABLE_SCREEN."` WHERE ".$cond;
		//echo $query;exit;
		
		$sql = $this->executeQry($query);

		$num = $this->getTotalRow($sql);

		$page =  $_REQUEST['page']?$_REQUEST['page']:1;

		if($num > 0) {			

			//-------------------------Paging------------------------------------------------			

			$paging = $this->paging($query); 

			$this->setLimit($_GET[limit]); 

			$recordsPerPage = $this->getLimit(); 

			$offset = $this->getOffset($_GET["page"]); 

			$this->setStyle("redheading"); 

			$this->setActiveStyle("smallheading"); 

			$this->setButtonStyle("boldcolor");

			$currQueryString = $this->getQueryString();

   			$this->setParameter($currQueryString);

			$totalrecords = $this->numrows;

			$currpage = $this->getPage();

			$totalpage = $this->getNoOfPages();

			$pagenumbers = $this->getPageNo();		

			//-------------------------Paging------------------------------------------------

			$orderby = $_GET[orderby]? $_GET[orderby]:"name";

		    $order = $_GET[order]? $_GET[order]:"ASC";   

            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			

			$rst = $this->executeQry($query); 

			$row = $this->getTotalRow($rst);

			if($row > 0) {			

				$i = 1;			

				while($line = $this->getResultObject($rst)) {
					//echo "<pre>"; print_r($line);continue;exit;

					$currentOrder	.=	$line->id.","; 
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					/// Name Section ====================
					$genTable .= '<tr class="'.$highlight.'" id="'.$line->id.'" >
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$line->id.'</td>
									<td>'.stripslashes($line->name).'</td>';
						
					/// Status Section : Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'SPPT\')">'.$status.'</div>';
					$genTable .= '</td>';
					
					
					/// View  Section ====================				
					$genTable .= '<td><a rel="shadowbox;width=705;height=325" title="'.stripslashes($line->fullName).'" href="viewSPPTable.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0" alt="View"></a></td>';
					
					
					/// Edit Section : Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a class="i_pencil edit" href="editSPPTable.php?id='.base64_encode($line->id).'&page='.$page.'" title="Edit" alt="Edit">edit</a>';
					$genTable .= '</td>';
					
					
					/// Delete Section: Permissions ====================
					$genTable.='<td>';
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=SPPT&type=delete&id=".$line->id."&page=$page'}else{}\" > delete </a>";
					$genTable.='</td>'; 
					// Close Row========================
					$genTable.='</tr>';
					$i++;	

				}
				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}

				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;

				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td> <td class='page_info' align='center' width='200'><inputtype='hidden' name='page' value='".$currpage."'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
		}	
		return $genTable;

	}	
	
	
/// SCREEN PRINTING  : ADD RECORD=============================================================================

	function addRecordSP($post){
		//echo "<pre>";print_r($_REQUEST);exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_DECORATION_PRICE_TABLE_SCREEN;
		$this->field_values['name'] = $post['SPName'];
		$this->field_values['currency_id'] = $_SESSION['DEFAULTCURRENCYID'];
		$this->field_values['fulfillment_id'] = $_SESSION['FULFILLMENTID'];
		$this->field_values['status'] = '1';
		$this->field_values['row'] = $post['row_val'];
		$this->field_values['col'] = $post['col_val'];
		$res = $this->insertQry();
		$insertId = $res[1];
		
		if($insertId){
			for($i=0;$i<$post['row_val'];$i++){
				for($j=0;$j<$post['col_val'];$j++){
				
					$NOC=$post["NOC_".$i];
					//$ST=$post["ST_".$i];
					$QF=$post["QF_".$j];
					$QT=$post["QT_".$j];
					$CP=$post["CP_".$i.$j];	
					$SP=$post["SP_".$i.$j];				
					/// Unable to use above used method for insert query//// ??
					$query="insert into ".TBL_DECORATION_PRICE_TABLE_DATA_SCREEN." set price_table_id='".$insertId."',color_in_design='".$NOC."' ,quantity_from='".$QF."' , quantity_to='".$QT."' ,price='".$CP."', setup_price='".$SP."' , rowId='".$i."', colId='".$j."'";
					$rst = $this->executeQry($query);
							
				}
			
				
			}
			
		}
		
		if($insertId){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add record.");
		}	
			
		
		header("Location:addSPPTable.php");exit;
		
	}	

/// SCREEN PRINTING  : EDIT RECORD============================================================================
	
	function editRecordSP($post){
	//echo "<pre>"; print_r($post); echo "</pre>";exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_DECORATION_PRICE_TABLE_SCREEN;
		$this->field_values['name'] = $post['SPName'];
		$this->field_values['currency_id'] = $_SESSION['DEFAULTCURRENCYID'];
		$this->field_values['fulfillment_id'] = $_SESSION['FULFILLMENTID'];
		$this->field_values['status'] ='1';		
		$this->field_values['row'] = $post['row_val'];
		$this->field_values['col'] = $post['col_val'];
		$this->condition  = "id = '$post[id]'";
		
		$res = $this->updateQry();
		
		
			/// Delete Prices related particular id from price_table_data
			$rst=$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA_SCREEN,"price_table_id = '$post[id]'");			
		
		// insert new values in price_table_data
		if($rst){
			for($i=0;$i<$post['row_val'];$i++){
				for($j=0;$j<$post['col_val'];$j++){
				
					$NOC=$post["NOC_".$i];
					//$ST=$post["ST_".$i];
					$QF=$post["QF_".$j];
					$QT=$post["QT_".$j];
					$CP=$post["CP_".$i.$j];
					$SP=$post["SP_".$i.$j];					
					/// Unable to use above used method for insert query//// ??
					$query="insert into ".TBL_DECORATION_PRICE_TABLE_DATA_SCREEN." set price_table_id='".$post[id]."',color_in_design='".$NOC."' ,quantity_from='".$QF."' , quantity_to='".$QT."' ,price='".$CP."' , setup_price='".$SP."', rowId='".$i."', colId='".$j."'";
					$rst = $this->executeQry($query);
							
				}
			
				
			}
			
		}
		if($rst){

			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");

		}else{

			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");

		}	

		header("Location:manageSPPTable.php");exit;
	}		
	
///SCREEN PRINTING  : Add Price Table=========================================================================

	function showTableSP($get){
				//echo "<pre>"; print_r($_POST);exit;
				$row=$get[row];
				$col=$get[col];
				$Tbl="";
				if($row !='' && $row !='0' && $col !='0' &&  $col !=''){					
	
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From</td>';
					  for($i=0; $i<$col; $i++){
					  
					  
			$QF="QF_".$i;
			if($i==0)
			{
			
			$Tbl.='<td><span id="m__QF_'.$i.'" >1</span><input name="QF_'.$i.'" type="hidden"  id="m__QFH_'.$i.'"   value="1" /></td>';
			}
			else
			{
			$Tbl.='<td><input type="text" value="'.($i*5).'" name="QF_'.$i.'" id="m__QF_'.$i.'" class="quanbalance"   /></td>';
			}
									  
					   
					  	//$QF="QF_".$i;
						//$Tbl.='<td><input type="text" value="" name="QF_'.$i.'" id="m__QF_'.$i.'"  /></td>';				  
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  for($i=0; $i<$col; $i++){
					  
					  
					if($i==0)
					{
					
					$Tbl.='<td><span id="m__QT_'.$i.'" >4</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="4" /></td>';
					}
					else
					{
					if($i==($col-1))
					{
					$vs='>';
					}
					$Tbl.='<td >'.$vs.'<span id="m__QT_'.$i.'" >'.(($i*5)+4).'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.(($i*5)+4).'" /></td>';
					}
					
					   
					  
					  
					  
					  
					//  $Tbl.='<td><input type="text" name="QT_'.$i.'" id="m__QT_'.$i.'"  /></td>';
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'"> Color In Design </td></tr>
					  <tr> <td colspan="2">No Of Colors</td>  <td colspan="'.($col).'"></td>  </tr>';
					 for($i=0; $i<$row; $i++){
						 $Tbl.='<tr>';
							 for($j=0; $j<($col+1); $j++){
							 if($j==0){
								 $name="NOC_".$i;
								 $Tbl.='<td colspan="2">'.($i+1).'<input type="hidden" name="'.$name.'" value="'.($i+1).'" id="'.$name.'" onchange="changeValue(\''.$i.'\',this.value);" /></td>';
							 }							 
							 else {
								 $name="CP_".($i).($j-1);
								 $Tbl.='<td><input type="text" name="'.$name.'" id="'.$name.'" class="stprice"  /></td>';
							 }
											 
						 }
						  
						 $Tbl.='</tr>';
					 
					 }
					$Tbl.='<tr><td colspan="'.($col+2).'">Set-up cost</td></tr>
					  <tr> <td colspan="2">No Of Colors</td>  <td colspan="'.($col).'"></td>  </tr>';
					 for($i=0; $i<$row; $i++){
					 $Tbl.='<tr>';
					 for($j=0; $j<($col+1); $j++){
					 if($j==0){$name="NOC2_".$i;	$Tbl.='<td colspan="2">'.($i+1).'<input type="hidden" name="'.$name.'" value="'.($i+1).'" id="'.$name.'"  /></td>';}							 
					 else {$name="SP_".($i).($j-1);$Tbl.='<td><input class="stprice"  type="text" name="'.$name.'" id="'.$name.'"  /></td>';}
					  					 
					 }
					  
					 $Tbl.='</tr>';
					 
					 }
				
					  					  
				  $Tbl.='</table>';
				  
				  
			  }
			  
			  
		echo $Tbl;
	
	}	
	
///SCREEN PRINTING  : Edit Price Table=========================================================================

	function showTableEditSP($get){
				$row=$get[row];
				$col=$get[col];
				$id=$get[id];
				$rst = $this->executeQry("select DPT.name,DPT.row, DPT.col, DPTD.* from ".TBL_DECORATION_PRICE_TABLE_SCREEN." as DPT inner join ".TBL_DECORATION_PRICE_TABLE_DATA_SCREEN." as DPTD  on (DPT.id=DPTD.price_table_id) where DPT.id = '$id'");

				$num = $this->getTotalRow($rst);
				while($line=$this->getResultObject($rst)){
				$priceArr[]=$line;	
				$NOC[$line->rowId]=$line->color_in_design;	
				//$ST[$line->rowId]=$line->stitch_to;	
				$QF[$line->colId]=$line->quantity_from ;	
				$QT[$line->colId]=$line->quantity_to;		
				$CP[$line->rowId][$line->colId]=$line->price;	
				$SP[$line->rowId][$line->colId]=$line->setup_price;
						
				}
				//echo "<pre>";print_r($priceArr);print_r($QT);
				//echo $pk;
				
				
				//print_r($priceArr);
				$Tbl="";
				$preQFarr=array();
				if($row !='' && $row !='0' && $col !='' &&  $col !=''){
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From </td>';
					  $perval='';
					  $isone=1;
					  for($i=0; $i<$col; $i++){
					  //$val="SF_".$i;
					  //$$val='5';
					  $valQF=$QF[$i];
					  if($valQF=='')
					  {
					  
					  if($isone==1)
					  {
						 $isone=0;
					
					   $valQF= $QT[$i-1]+1;
					   $preQFarr[$i]= $QT[$i-1]+1;
					  }
					  else
					  {
					
					   $valQF=$perval+5;
					   $preQFarr[$i]=$perval+5;
					   }
					  
					  
					
					  }
			if($i==0)
			{
			
			$Tbl.='<td><span id="m__QF_'.$i.'" >'.$valQF.'</span><input name="QF_'.$i.'" type="hidden"  id="m__QFH_'.$i.'"   value="'.$valQF.'" /></td>';
			}
			else
			{
			$Tbl.='<td><input type="text" value="'.$valQF.'" name="QF_'.$i.'" id="m__QF_'.$i.'" class="quanbalance"   /></td>';
			}
					  
					  
					  
				//	$Tbl.='<td><input type="text" name="QF_'.$i.'" value="'.$QF[$i].'"  /></td>';				  
					     $perval=$valQF;
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  
					  for($i=0; $i<$col; $i++){
					  $valQT=$QT[$i];
					  if($valQT=='')
					  {
					  $valQT=$preQFarr[$i]+4;
					  }
					  
					  
					  	if($i==0)
					{
					
					$Tbl.='<td><span id="m__QT_'.$i.'" >'.$valQT.'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.$valQT.'" /></td>';
					}
					else
					{
					if($i==($col-1))
					{
					$vs='>';
					}
					$Tbl.='<td >'.$vs.'<span id="m__QT_'.$i.'" >'.$valQT.'</span><input  type="hidden"  id="m__QTH_'.$i.'" name="QT_'.$i.'"  value="'.$valQT.'" /></td>';
					}
					
					  
					  
					 // $Tbl.='<td><input type="text" name="QT_'.$i.'" value="'.$QT[$i].'"  /></td>';
					 // $perval=$valQF;
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'"> Color In Design</td></tr>
					  <tr>
					  <td colspan="2">No Of Colors</td>					  
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					 for($i=0; $i<$row; $i++){
						 $Tbl.='<tr>';
						 for($j=0; $j<($col+1); $j++){
						 if($j==0){
							$name="NOC_".$i;
							$val=$NOC[$i];	
								if($val=='')
								{
								$val=$i+1;
								}
							$Tbl.='<td colspan="2">'.$val.'<input type="hidden" id="'.$name.'" name="'.$name.'" value="'.$val.'" onchange="changeValue(\''.$i.'\',this.value);" /></td>';
						 }						 
						 else {
							$name="CP_".($i).($j-1);
							$val=$CP[$i][$j-1];
							$Tbl.='<td><input type="text" class="stprice" name="'.$name.'" value="'.$val.'"  /></td>';
						 }
						  
						 
						 
						 }
						  
						 $Tbl.='</tr>';
					}	 
						 
						 $Tbl.=' <tr><td colspan="'.($col+2).'">Set-up cost</td></tr>';
						 $Tbl.=' <tr>
					  				<td colspan="2">No Of Colors</td>					  
					 		    	<td colspan="'.($col).'"></td>
					  
					  			</tr>';
					 			for($i=0; $i<$row; $i++){
									$Tbl.='<tr>';
										for($j=0; $j<($col+1); $j++){
											if($j==0){
												$name="NOC2_".$i;
												$val=$NOC[$i];
												if($val=='')
												{
													$val=$i+1;
												}
												$Tbl.='<td colspan="2">'.$val.'<input type="hidden" id="'.$name.'" name="'.$name.'" value="'.$val.'" />
												</td>';
											}else {
												$name="SP_".($i).($j-1);
												$val=$SP[$i][$j-1];
												$Tbl.='<td><input type="text" class="stprice" name="'.$name.'" value="'.$val.'"  /></td>';
											}
										}
							  
									$Tbl.='</tr>';
					 
								}
					 
					 
					$Tbl.='</table>';  
				}
			  
			  
		echo $Tbl;
				
	
	}	
	
///SCREEN PRINTING  : View Price Table=========================================================================
	function showTableViewSP($get){
				$row=$get[row];
				$col=$get[col];
				$id=$get[id];
				$rst = $this->executeQry("select DPT.name,DPT.row, DPT.col, DPTD.* from ".TBL_DECORATION_PRICE_TABLE_SCREEN." as DPT inner join ".TBL_DECORATION_PRICE_TABLE_DATA_SCREEN." as DPTD  on (DPT.id=DPTD.price_table_id) where DPT.id = '$id'");

				$num = $this->getTotalRow($rst);
				while($line=$this->getResultObject($rst)){
				$priceArr[]=$line;	
				$NOC[$line->rowId]=$line->color_in_design;	
				//$ST[$line->rowId]=$line->stitch_to;	
				$QF[$line->colId]=$line->quantity_from ;	
				$QT[$line->colId]=$line->quantity_to;		
				$CP[$line->rowId][$line->colId]=$line->price;
				$SP[$line->rowId][$line->colId]=$line->setup_price;				
				}
				//echo "<pre>";print_r($priceArr);print_r($QT);
				//echo $pk;
				
				
				//print_r($priceArr);
				$Tbl="";
				if($row !='' && $row !='0' && $col !='' &&  $col !=''){
					$Tbl.='<table   border="1" rules="all" align="center" cellpadding="5" cellspacing="5">
					  <tr>
					  <td colspan="2" align="right">Quantity From</td>';
					  for($i=0; $i<$col; $i++){
					  //$val="SF_".$i;
					  //$$val='5';
					  
					$Tbl.='<td>'.$QF[$i].'</td>';				  
					   }
					  $Tbl.='</tr>		  
					  </tr>
					 
					  <tr>
					  <td colspan="2" align="right">Quantity To </td>';
					  for($i=0; $i<$col; $i++){
					  $Tbl.='<td>'.$QT[$i].'</td>';
					   }
					 $Tbl.=' </tr>
					   <tr><td colspan="'.($col+2).'">Color In Design</td></tr>
					  <tr>
					  <td colspan="2">No Of Colors</td>					  
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					 for($i=0; $i<$row; $i++){
						 $Tbl.='<tr>';
						 for($j=0; $j<($col+1); $j++){
							 if($j==0){
								 $name="NOC_".$i;
								 $val=$NOC[$i];	
								 $Tbl.='<td colspan="2">'.$val.'</td>';
							 }							 
							 else {
								 $name="CP_".($i).($j-1);
								 $val=$CP[$i][$j-1];
								  $Tbl.='<td>'.$val.'</td>';
							 }
						  
						 
						
						 }
						  
						 $Tbl.='</tr>';
						 
					 }
					 
					$Tbl.='<tr><td colspan="'.($col+2).'">Set-up cost</td></tr>'; 
					$Tbl.='<tr>
					  <td colspan="2">No Of Colors</td>					  
					  <td colspan="'.($col).'"></td>
					  
					  </tr>';
					 for($i=0; $i<$row; $i++){
						 $Tbl.='<tr>';
						 for($j=0; $j<($col+1); $j++){
							 if($j==0){
								 $name="NOC_".$i;
								 $val=$NOC[$i];	
								 $Tbl.='<td colspan="2">'.$val.'</td>';
							 }							 
							 else {
								 $name="SP_".($i).($j-1);
								 $val=$SP[$i][$j-1];
								  $Tbl.='<td>'.$val.'</td>';
							 }
						  
						 
						
						 }
						  
						 $Tbl.='</tr>';
						 
					 }
					 
					 
					$Tbl.='</table>';  
				}
			  
			  
		echo $Tbl;
				
	
	}	
	
	
	
// SCREEN PRINTING : GET RESULT BY ID===========================================================================
	function getResultSP($id) {

		$sql = $this->executeQry("select * from ".TBL_DECORATION_PRICE_TABLE_SCREEN." where id = '$id' and fulfillment_id='".$_SESSION['FULFILLMENTID']."'");
		
		$num = $this->getTotalRow($sql);

		if($num > 0) {				

			return $line = $this->getResultObject($sql);	

		} 
		else{
		header("location:manageSPPTable.php");exit;
		}

	}	

	
//  SCREEN PRINTING : Delete, Active/Inactive Multiple Values ==================================================
	function deleteAllValuesSP($post){
	//echo"<pre>"; print_r($post);exit;

		if(($post[action] == '')){

		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records, And then submit!!!");
			echo "<script language=javascript>window.location.href='manageSPPTable.php?page=$post[page]&limit=$post[limit]';</script>";exit;

		}				

		if($post[action] == 'deleteselected'){
		//echo "yes";exit;

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){
					$rst=$this->deleteRec(TBL_DECORATION_PRICE_TABLE_SCREEN,"id = '$val'");				

					if($rst){
						$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA_SCREEN,"price_table_id = '$val'");	
						
					}else{

						$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");

			}else{

			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		if($post[action] == 'enableall'){

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){
				
					$sql="UPDATE ".TBL_DECORATION_PRICE_TABLE_SCREEN." set status ='1'  where id='$val'";
					
					$rst = $this->executeQry($sql);

					if($rst){

					$this->logSuccessFail("1",$sql);

					}else{

					$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");

			}else{

			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		if($post[action] == 'disableall'){

			$delres = $post[chk];

			$numrec = count($delres);

			if($numrec>0){

				foreach($delres as $key => $val){

					$sql="UPDATE ".TBL_DECORATION_PRICE_TABLE_SCREEN." set status ='0' where id='$val'";

					$rst = $this->executeQry($sql);

					if($rst){

					$this->logSuccessFail("1",$sql);

					}else{

					$this->logSuccessFail("0",$sql);

					}

				}

				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");

			}else{

				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");

			}

		}

		echo "<script language=javascript>window.location.href='manageSPPTable.php?page=$post[page]&limit=$post[limit]';</script>";exit;

	}		

///SCREEN PRINTING  : Delete Single Value=======================================================================
		function deleteValueSP($get){
		
			$rst=$this->deleteRec(TBL_DECORATION_PRICE_TABLE_SCREEN,"id = '$get[id]'");
			if($rst){
					$this->deleteRec(TBL_DECORATION_PRICE_TABLE_DATA_SCREEN,"price_table_id = '$get[id]'");
							

				}else{ 	

					$this->logSuccessFail('0',$sql);

				}

			$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");

			echo "<script language=javascript>window.location.href='manageSPPTable.php?page=$get[page]&limit=$get[limit]';</script>";exit;

	}	

// SCREEN PRINTING  : Change Value Status========================================================================
	function changeValueStatusSP($get) {

		

		$status=$this->fetchValue(TBL_DECORATION_PRICE_TABLE_SCREEN,"status","1 and id = '$get[id]'");
		
		if($status==1) {

			$stat= 0;

			$status="Inactive";

		} else 	{

			$stat= 1;

			$status="Active";

		}

		$query = "update ".TBL_DECORATION_PRICE_TABLE_SCREEN." set status = '$stat'  where id = '$get[id]'";
		
		
		if($this->executeQry($query)) 

			$this->logSuccessFail('1',$query);		

		else 	

			$this->logSuccessFail('0',$query);

		echo $status;		

	}

		
/// SCREEN PRINTING : GET PRICE TABLE SCREEN ========================================================================
	
	function getPriceTable_screen($tableId){
	
		$option = "";
		$query = "SELECT id, name FROM ".TBL_DECORATION_PRICE_TABLE_SCREEN." WHERE status = '1' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		while($data = $this->getResultObject($sql)){
			if($tableId == $data->id){ $selected = 'selected="selected"';}else{  $selected = '';}
				$option .= '<option value="'.$data->id.'" '.$selected.' > '.$data->name.' </option>';
		}
		return $option;
	}
	
}// End Class
?>	

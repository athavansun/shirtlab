<?php 
session_start();
class Coupon extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function getDiscountType($id) {
		$genTable = '<select name="couponDiscountType" id="couponDiscountType">';
		$sel1 = ""; $sel2 = ""; $sel3 = "";
		
		if($id != "") {
			if($id == "Percent") { $sel1 = "selected='selected'"; }
			//if($id == "Amount") { $sel2 = "selected='selected'"; }
			//if($id == "Shipping") { $sel3 = "selected='selected'"; }
		}
		
		$genTable .= '<option value="Percent" '.$sel1.'>Discount On Order (%)</option>';
		//$genTable .= '<option value="Amount" '.$sel2.'>Discount On Order (Amount)</option>';
		//$genTable .= '<option value="Shipping" '.$sel3.'>Free Shipping</option>';
		return $genTable .= '</select>';
	}

	function getMinOrderType($id) {
		$genTable = '<select name="minOrderType" id="minOrderType">';
		
		$sel1 = ""; $sel2 = "";
		if($id != "") {
			if($id == "Price") { $sel1 = "selected='selected'"; }
			if($id == "Quantity") { $sel2 = "selected='selected'"; }
		}
		
		$genTable .= '<option value="Price" '.$sel1.'>Price Total</option>';
		$genTable .= '<option value="Quantity" '.$sel2.'>Product Quantity</option>';
		return $genTable .= '</select>';
	}
	
	function getAllProducts($ids,$keyword,$retType) {
		$genTable = '<select name="productId[]" id="productId[]" size="4" multiple="multiple">';				
		$cond = " 1 AND p.status = '1' and p.isDeleted = '0' and p.id = pd.productId";
		$sel = "";
		$idArr = array();
		if($ids != "") {
			$idArr = explode(',',$ids);
		}
		
		if($keyword != "") {
			$cond .= " AND p.isDeleted = '0' AND pd.productName like '%".addslashes($keyword)."%'";
		} else {
			if(count($idArr) > 0) {
				if(in_array("0",$idArr)) {
					$sel = "selected='selected'";
				}			
			} else {
				$sel = "selected='selected'";
			}				
			$genTable .= '<option value="0" '.$sel.'>All Product</option>';
		}
		$query = "select p.id,pd.productName from ".TBL_MAINPRODUCT." as p left join ".TBL_MAIN_PRODUCT_DESCRIPTION." as pd on p.id=pd.productId  where $cond and langId='".$_SESSION['DEFAULTLANGUAGE']."'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {
				$sel2 = "";
				foreach($idArr as $key=>$value) { if($value == $line->id) { $sel2 = "selected='selected'"; } }
				$genTable .= '<option value="'.$line->id.'" '.$sel2.'>'.stripslashes($line->productName).'</option>';
			}
		}
		$genTable .= '</select>';	
		if($retType == 0) 
			return $genTable;
		if($retType == 1) 
			echo $genTable;	
	}
	
	function getAllCustomers($ids,$keyword,$retType) {
		$genTable = '<select name="customerId[]" id="customerId[]" size="4" multiple="multiple">';						
		$cond = " 1 AND status = '1'";
		$sel = "";
		$idArr = array();
		if($ids != "") {
			$idArr = explode(',',$ids);
		}
		
		if($keyword != "") {
			$cond .= " AND email like '%".$keyword."%'";
		} else {
			if(count($idArr) > 0) {
				if(in_array("0",$idArr)) {
					$sel = "selected='selected'";
				}			
			} else {
				$sel = "selected='selected'";
			}
			
			$genTable .= '<option value="0" '.$sel.'>All Customers</option>';
		}
		$query = "select * from ".TBL_USER." where $cond";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			while($line = $this->getResultObject($sql)) {
				$sel2 = "";
				foreach($idArr as $key=>$value) { if($value == $line->id) { $sel2 = "selected='selected'"; } }				
				$genTable .= '<option value="'.$line->id.'" '.$sel2.'>'.$line->email.' ('.$line->firstName.' '.$line->lastName.')</option>';
			}
		}
		$genTable .= '</select>';	
		if($retType == 0) 
			return $genTable;
		if($retType == 1) 
			echo $genTable;	
	}
	
	function isCouponCodeExist($code) {
		$sql = $this->executeQry("select * from ".TBL_COUPON." where 1 and couponCode = '".$code."'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	function addNew($post) {
		
		$sql = "INSERT INTO ".TBL_COUPON." SET `couponCode`='$post[couponCode]', `couponDiscountAmount`='$post[discountValue]', `couponDiscountType`='$post[couponDiscountType]', `minOrderValue`='0', minOrderType = '$post[minOrderType]', startDate = '$post[startDate]', endDate = '$post[endDate]', qtyPerUser = '$post[qty_each_user]', productExclusive = '$productId', customerExclusive = '$customerId', productAllId = '".$productAllID."', customerAllId = '0' ,`addDate`='".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', `status`='1'";		
		$rst = $this->executeQry($sql);
		$inserted_id = mysql_insert_id();
			
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$couponName = 'couponName_'.$line->id;
				$query = "insert into ".TBL_COUPON_DESCRIPTION." set couponId = '$inserted_id', langId = '".$line->id."', couponName = '".addslashes($post[$couponName])."'";
				if($this->executeQry($query)) 
					$this->logSuccessFail('1',$query);		
				else 	
					$this->logSuccessFail('0',$query);
			}	
		}	
		
		/*if($productId == 1) {
			foreach($post[productId] as $key=>$value) {
				if($value != "" && $value > 0) {
					$query = "insert into ".TBL_COUPON_TO_PRODUCT." set couponId = '$inserted_id', productId = '$value'";
					$this->executeQry($query);
				}
			}
		}	
		
		if($customerId == 1) {			
			foreach($post[customerId] as $key1=>$value1) {
				if($value1 != "" && $value1 > 0) {
					$query = "insert into ".TBL_COUPON_TO_CUSTOMER." set couponId = '$inserted_id', customerId = '$value1'";
					$this->executeQry($query);
				}	
			}
		}
		*/	
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
		header("Location:addCoupon.php");
		exit;	
	}
	
	
	function editRecord($post) {
	   /*
		if($post[productId])
			$productId = in_array("0",$post[productId])?0:1;
		if($post[customerId])
			$customerId = in_array("0",$post[customerId])?0:1;
		if($post[productId])
			$productAllID = ($productId == 1)?implode(',',$post[productId]):0;
		if($post[customerId])
		$customerAllID = ($customerId == 1)?implode(',',$post[customerId]):0;
		
		$sql = "update ".TBL_COUPON." SET `couponCode`='$post[couponCode]', `couponDiscountAmount`='$post[discountValue]', `couponDiscountType`='$post[couponDiscountType]', `minOrderValue`='$post[minOrderValue]', minOrderType = '$post[minOrderType]', startDate = '$post[startDate]', endDate = '$post[endDate]', qtyPerUser = '$post[qty_each_user]', productExclusive = '$productId', customerExclusive = '$customerId', productAllId = '".$productAllID."', customerAllId = '".$customerAllID."', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$post[id]."'";	
		*/
		
		$sql = "update ".TBL_COUPON." SET `couponCode`='$post[couponCode]', `couponDiscountAmount`='$post[discountValue]', `couponDiscountType`='$post[couponDiscountType]', `minOrderValue`='0', minOrderType = '$post[minOrderType]', startDate = '$post[startDate]', endDate = '$post[endDate]', qtyPerUser = '$post[qty_each_user]', productExclusive = '$productId', customerExclusive = '$customerId', productAllId = '".$productAllID."', customerAllId = '0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$post[id]."'";
		$rst = $this->executeQry($sql);			
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}

		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$couponName = 'couponName_'.$line->id;
				$sql = $this->selectQry(TBL_COUPON_DESCRIPTION,'1 and couponId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_COUPON_DESCRIPTION." set couponId = '".$post[id]."', langId = '".$line->id."', couponName = '".addslashes($post[$couponName])."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				} else {
					$query = "update ".TBL_COUPON_DESCRIPTION." set couponName = '".addslashes($post[$couponName])."' where 1 and couponId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
		}	
		/*
		$this->executeQry("delete from ".TBL_COUPON_TO_PRODUCT." where couponId = '".$post[id]."'");
		if($productId == 1) {
			foreach($post[productId] as $key=>$value) {
				if($value != "" && $value > 0) {
					$query = "insert into ".TBL_COUPON_TO_PRODUCT." set couponId = '".$post[id]."', productId = '$value'";
					$this->executeQry($query);
				}
			}
		}	
		
		$this->executeQry("delete from ".TBL_COUPON_TO_CUSTOMER." where couponId = '".$post[id]."'");
		if($customerId == 1) {			
			foreach($post[customerId] as $key1=>$value1) {
				if($value1 != "" && $value1 > 0) {
					$query = "insert into ".TBL_COUPON_TO_CUSTOMER." set couponId = '".$post[id]."', customerId = '$value1'";
					$this->executeQry($query);
				}	
			}
		}	
		*/		
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
		echo "<script language=javascript>window.location.href='manageCoupon.php?page=$post[page]';</script>";
		exit;		
	}	

	
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_COUPON." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageCoupon.php');
		}	
	}


	function valDetail() {						
		$cond = "1 ";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND couponCode LIKE '%$searchtxt%'";
		}
		
		$query = "select * from ".TBL_COUPON." where $cond ";			
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					if($line->couponDiscountType == "Percent") {
						$discount = $line->couponDiscountAmount." %";
					} elseif($line->couponDiscountType == "Amount") {
						$discount = "$ ".$line->couponDiscountAmount;
					} elseif($line->couponDiscountType == "Shipping") {
						$discount = "Free Shipping";
					}
					
					$startDate = $line->startDate != "0000-00-00"?date(DEFAULTDATEFORMAT,strtotime($line->startDate)):"Unlimited";
					$endDate = $line->endDate != "0000-00-00"?date(DEFAULTDATEFORMAT,strtotime($line->endDate)):"Unlimited";
					
					if($line->minOrderType == "Price") {
						$minOrder = "$ ".$line->minOrderValue;
					} else {
						$minOrder = $line->minOrderValue;
					}
					//<li style="width:80px;">'.$line->qtyPerUser.'</li> <li style="width:60px;">'.$minOrder.'</li>
					$genTable .= '<tr>
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$line->couponCode.'</td>
									<td>'.$discount.'</td>
									<td>'.$startDate.'</td>
									<td>'.$endDate.'</td>
																		
									
									<td>';
									if($menuObj->checkEditPermission()) {							
										$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'coupon\')" >'.$status.'</div>';
									}				
					$genTable .= '</td><td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->couponCode).'" href="viewCoupon.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td><td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editCoupon.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td><td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=coupon&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	
	////// change status////

	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_COUPON,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
		$sql = "UPDATE ".TBL_COUPON." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
	///// Delete Newsletter
	function deleteValue($get) {
		$sql = "DELETE FROM  ".TBL_COUPON." where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$sql = "DELETE FROM  ".TBL_COUPON_DESCRIPTION." where couponId = '$get[id]'";		
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$sql = "DELETE FROM  ".TBL_COUPON_TO_PRODUCT." where couponId = '$get[id]'";		
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}		
		
		$sql = "DELETE FROM  ".TBL_COUPON_TO_CUSTOMER." where couponId = '$get[id]'";		
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}				
				
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageCoupon.php?page=$post[page]&limit=$post[limit]';</script>";
	}


	///// Enable All/ Disable All // Delete All 

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageCoupon.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql = "DELETE FROM ".TBL_COUPON." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
					
					$sql = "DELETE FROM ".TBL_COUPON_DESCRIPTION." where couponId = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
					
					$sql = "DELETE FROM ".TBL_COUPON_TO_PRODUCT." where couponId = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
					
					$sql = "DELETE FROM ".TBL_COUPON_TO_CUSTOMER." where couponId = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}					
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COUPON." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_COUPON." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageCoupon.php?page=$post[page]';</script>";
	}
	
	function generateNumber(){
		$password = substr (MD5(uniqid(rand(),1)), 3, 8);
		echo '<input type="text" name="couponCode" id="m__couponCode" value="'.$password.'">';
	}	


}// End Class
?>	

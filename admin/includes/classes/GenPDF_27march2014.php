<?php

session_start();

require_once("includes/JSON.php");
require_once("includes/classes/pdf.php");
require_once("includes/classes/zip.class.php");
require_once("includes/classes/MySqlDriver.php");

class GenPDF extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function deletePDF($id) {
        $query = "select * from " . TBL_ORDER . " where 1 and id = '" . $id . "'";
        $sql = $this->executeQry($query);
        $line = mysql_fetch_object($sql);
        $ordReceiptId = $line->ordReceiptId;
        $this->executeQry("update " . TBL_ORDER . " set pdf_generated = '0' where 1 and id = '" . $id . "'");
        @unlink('pdf/' . $ordReceiptId . ".zip");
        $this->DELETE_RECURSIVE_DIRS('pdf/' . $ordReceiptId);
        $div_id = "status" . $id;
        echo '<a style="cursor:pointer;" onClick="javascript:generatePDF(\'' . $div_id . '\',\'' . $id . '\',\'PDF\')">Generate PDF</a>';
    }

    function DELETE_RECURSIVE_DIRS($dirname) {
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname . "/" . $file)) {
                        unlink($dirname . "/" . $file);
                    } else {
                        $this->DELETE_RECURSIVE_DIRS($dirname . "/" . $file);
                        @rmdir($dirname . "/" . $file);
                    }
                }
            }
            closedir($dir_handle);
            @rmdir($dirname);
            return true;
        } else
            return false;
    }

    function createDirectory($dirName) {
        if (!is_dir($dirName)) {
            mkdir($dirName);
            @chmod($dirName, 0777);
        }
    }

    function generatePDF($id) {
        //echo "<pre>"; print_r($_SERVER);  exit;
        //echo 	$id; echo"<br>";
        $query = "select * from " . TBL_ORDER . " where 1 and id = '" . $id . "'";
        $sql = $this->executeQry($query);
        $line = mysql_fetch_object($sql);
        $imageMagickPath = '/usr/bin/convert';

        $ordReceiptId = $line->ordReceiptId;
        $ordZipFileName = $ordReceiptId . ".zip";
        $directoryName = "pdf/" . $ordZipFileName;

        if (!is_file($directoryName)) {
            $query2 = "SELECT ORD.*, p.dataArr FROM " . TBL_ORDERDETAIL . " as ORD inner join " . TBL_MAINPRODUCT . " as p on (ORD.productId = p.id) WHERE ORD.orderId = '$id' AND p.isDeleted = '0' ORDER BY ORD.id DESC";
            $sql2 = $this->executeQry($query2);
            $num2 = $this->getTotalRow($sql2);
            if ($num2 > 0) {
                $zipArr = array();
                $image_array = array();
                while ($line2 = mysql_fetch_object($sql2)) {
                    $jsonClass = new JSON;
                    $jsonEncodedData = $jsonClass->decode($line2->dataArr);
					//$jsonEncodedData = $jsonClass->decode($jsonEncodedData->contentArray);
                    //echo "<pre>"; print_r($jsonEncodedData);  exit;
                    //$viewId = 1;
                    foreach ($jsonEncodedData as $fvalue) {
                        $viewId = $this->fetchValue(TBL_VIEWDESC, 'viewId', 'viewName = "' . $fvalue->ViewName . '"');
                        $rowProdDetSql = $this->executeQry("select * from  ecart_mainproduct_view  where  mainProdId = '" . $line2->productId . "' and viewId = '" . $viewId . "'");
                        $rowProdDet = mysql_fetch_object($rowProdDetSql);
                        //$viewId = $viewId+1;
                        $profImage = $rowProdDet->viewImage;
                        $profImage = "../files/mainproduct/original/" . $profImage;
                        list($printW, $printH) = getimagesize($profImage);

                        $size = array($printW, $printH);
                        $pdf = new PDF("P", "pt", $size);
                        $pdf->AddPage();
                        $viewName = $fvalue->ViewName;
                        $dirName = "pdf/" . $ordReceiptId;
                        $this->createDirectory($dirName); // Create First Level Directory								
                        $dirName = "pdf/" . $ordReceiptId . "/" . strtolower($viewName);
                        $this->createDirectory($dirName);  // Create Second Level Directory 
                        $pdfFileName = $ordReceiptId . "-" . $line2->productId . "-" . strtolower($viewName) . ".pdf";
                        $pdf->RotatedImage($profImage, 0, 0, $printW, $printH, 0);
                        $zipArr[] = $dirName . "/" . $pdfFileName;
                        $pdf->Output($dirName . "/" . $pdfFileName);
                        $dirName = "pdf/" . $ordReceiptId . "/designImage";
                        $this->createDirectory($dirName);  // Create third Level Directory 
                        foreach ($fvalue->contentArray as $value) {
                            $value = strip_tags($value);
                            $siteUrl = explode('://', SITE_URL);
                            //echo'<pre>'; print_r($siteUrl); exit;
                            $deco = explode($siteUrl[0] . "://", $value);
                            $decoImg = $deco[1];
                            //echo'<pre>'; print_r($decoImg); exit;
                            $imageArray = explode("/", $decoImg);

                            $position = count($imageArray) - 1;
                            $imageRequied = $imageArray[$position];
                            
                            //change start here                            
                            $originalImageName = $this->fetchValue(TBL_USERIMAGE, "originalname", "imageName ='".$imageRequied."'");                            
                            $originalExt = array_pop(explode('.', $originalImageName));
                            $fileType = '.'.$originalExt;
                            $pathArr = explode('/', $decoImg);
                            array_pop($pathArr);                            
                            array_push($pathArr, $originalImageName);
                            $path = implode("/", $pathArr);
                            $filePath = $siteUrl[0]."://".$path;
                            $path_parts = pathinfo($filePath);
                            $getExt = $path_parts['extension'];
                            
                            //change end
                            
                            
                            
                            
                            //$filePath = $siteUrl[0] . "://" . $deco[1];
//                            $path_parts = pathinfo($filePath);
//                            $getExt = $path_parts['extension'];
                            
//                            $fileType = '';
//                            if (strtolower($getExt) == 'png') {
//                                $filePath = '../' . str_replace(SITE_URL, '', $filePath);
//                                if (file_exists(str_replace('.png', '.ai', $filePath))) {
//                                    $filePath = str_replace('.png', '.ai', $filePath);
//                                    $fileType = '.ai';
//                                } else if (file_exists(str_replace('.png', '.pdf', $filePath))) {
//                                    $filePath = str_replace('.png', '.pdf', $filePath);
//                                    $fileType = '.pdf';
//                                } else if (file_exists(str_replace('.png', '.psd', $filePath))) {
//                                    $filePath = str_replace('.png', '.psd', $filePath);
//                                    $fileType = '.psd';
//                                }
//                                else if (file_exists(str_replace('.png', '.eps', $filePath))) {
//                                    $filePath = str_replace('.png', '.eps', $filePath);
//                                    $fileType = '.eps';
//                                } else {
//                                    
//                                }
//                            } else {
//                            }
                            
                            if (!empty($fileType)) {
                                $imageRequied = str_replace('.png', $fileType, $imageRequied);
                            }

                            if (!is_file("pdf/" . $ordReceiptId . "/designImage/" . $imageRequied))
                                $zipArr[] = "pdf/" . $ordReceiptId . "/designImage/" . $imageRequied;


                            
                            copy($filePath, "pdf/" . $ordReceiptId . "/designImage/" . $imageRequied);
							//echo'<pre>'; print_r("pdf/" . $ordReceiptId . "/designImage/" . $imageRequied); exit;
                            //copy("http://".$deco[1] , "pdf/".$ordReceiptId."/".strtolower($viewName)."/".$imageRequied);
                        }
                    }
                }

                $fileName = "pdf/" . $ordReceiptId . ".zip";
                $fd = fopen($fileName, "wb");
                $createZip = new ZipFile($fd);



                foreach ($zipArr as $filzip) {
                    $zipfileName = substr($filzip, strrpos($filzip, "/") + 1);

                    $createZip->addFile($filzip, $zipfileName, true);
                }

                $createZip->close();

                $this->executeQry("update " . TBL_ORDER . " set pdf_generated = '1' where 1 and id = '" . $id . "'");
                echo '<a id="pdfDown000041" style="display: block;" title="Order Detail" href="download.php?fileName=' . $ordReceiptId . '.zip">Download PDF</a>
';
            } else {
                echo "Sorry there is no order!!";
            }
        } else {
            echo "PDF already generated!!";
        }
    }

}
?>	

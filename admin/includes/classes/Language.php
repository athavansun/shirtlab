<?php 
session_start();
class Language extends MySqlDriver{
	function __construct() { $this->obj = new MySqlDriver;  }

        //Start : Show Details in Grid==================================      
	function valDetail() {
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$con = " AND languageName LIKE '%$searchtxt%' OR languageCode LIKE  '%$searchtxt%'";
		}
		
		$query = "select * from ".TBL_LANGUAGE." where 1=1 and isDeleted='0' $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"languageName";
			$order = $_GET[order]? $_GET[order]:"ASC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);                        
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
                                        $status=($line->status)?"Active":"Inactive";					
					if ($line->isDefault==1){
						$isDefault = "checked='checked'";
                                                $onclickstatus = '';
                                                $onclickdefault = '';
                                                $chkbox = '';
					}else{
						 $isDefault = "";
                                                $onclickstatus = ' onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'language\')"';
                                                $chkbox = '<input name="chk[]" value="' . $line->id . '" type="checkbox" class="checkbox">';
					}
                                        
                                        $genTable .= '<tr class="' . $highlight . '">';
                                        $genTable.='<th>'.$chkbox.'</th>';
                                        $genTable.='<td>'.$i .'</td>';
                                        $genTable.='<td>'.$line->id.'</td>';
                                        $genTable.='<td>'.stripslashes($line->languageName).'</td>';
                                        $genTable.='<td>'.$line->languageCode.'</td>';
                                        $genTable.='<td><img src="'.FLAGPATH.$line->languageFlag.'" /></td>';
                                        $genTable.='<td><input type="checkbox" ' . $isDefault . ' disabled=disabled class="welcheckbox"></td>';
                                        
                                        //Status===============
                                        $genTable.= '<td>';
                                        if ($menuObj->checkEditPermission()){
                                            $genTable.='<div id="' . $div_id . '" style="cursor:pointer;" ' . $onclickstatus . '>' . $status . '</div>';
                                        }                                      
                                        $genTable.='</td>';
                                        
                                        //Edit======================
                                        $genTable.='<td>';
                                        if ($menuObj->checkEditPermission()){
                                            $genTable.='<a class="i_pencil edit" href="editLanguage.php?id=' . base64_encode($line->id) . '&page=' . $page . '"></a>';
                                        }
                                        $genTable.='</td>';
                                        
                                        //Delete======================
                                        $genTable.='<td>';
                                        if ($menuObj->checkEditPermission()  && (!$line->isDefault)){
                                            $genTable .= "<div id=\"" . $div_id1 . "_del\"><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this record!')){window.location.href='pass.php?action=language&type=delete&id=" . $line->id . "&page=$page'}else{}\" ></a><div>";
                                        }
                                        $genTable.='</td>';	
                                        $genTable .= '</tr>';
                                        $i++;
				}
                                switch ($recordsPerPage) {
                                    case 10:
                                    $sel1 = "selected='selected'";
                                    break;
                                    case 20:
                                    $sel2 = "selected='selected'";
                                    break;
                                    case 30:
                                    $sel3 = "selected='selected'";
                                    break;
                                    case $this->numrows:
                                    $sel4 = "selected='selected'";
                                    break;
                                }
                                
				$currQueryString = $this->getQueryString();
                                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
                                <table border='0' width='88%' height='50'>
                                <tr>
                                <td align='left' width='300' class='page_info' 'style=margin-left=20px;'>Display
                                <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
                                <option value='10' $sel1>10</option>
                                <option value='20' $sel2>20</option>
                                <option value='30' $sel3>30</option> 
                                <option value='" . $totalrecords . "' $sel4>All</option>
                                </select>Records Per Page
                                </td>
                                <td align='center' class='page_info'><inputtype='hidden' name='page' value='" . $currpage . "'></td>
                                <td class='page_info' align='center' width='200'>Total Records Found : " . $totalrecords . "</td>
                                <td width='0' align='right'>" . $pagenumbers . "</td>
                                </tr>
                                </table>
                                </div>";
			}					
		} else {
			 $genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no records found.</span>';
		}	
               
		return $genTable;
	}
	
	
	function addConstant($post){
	
		//~ if($post['frontBack'] == 'back'){
			//~ $viewTable = TBL_LANGUAGEATTRIBUTEADMIN ;
		//~ }
		//~ else if($post['frontBack'] == 'front'){
			//~ $viewTable = TBL_LANGATTRIBUTE;
		//~ }
		//print_r($post);
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		
		if($num){
			while($line = $this->getResultObject($rst)) {					
				$constantValue = 'constantVal_'.$line->id;
				$query = " INSERT INTO ".TBL_LANGATTRIBUTE." SET langId = '".$line->id."', lang_type = '".strtoupper($post['constant'])."' , lang_value = '".$post[$constantValue]."' ";
				$status[] = $this->executeQry($query);
			}
			if(count($status) == $num )
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
			else
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail", "Unable to insert constant");
		}
		echo "<script language=javascript>window.location.href='setattribute.php';</script>"; exit;
	}
	
	function isConstantExits($constant, $view = "front"){
	
		$upperCaseConstant = strtoupper($constant);
		if($view == "back"){
			$viewTable = TBL_LANGUAGEATTRIBUTEADMIN;
		}
		else if($view == "front"){
			$viewTable = TBL_LANGATTRIBUTE;
		}
		$rst = $this->selectQry($viewTable," lang_type = '".$upperCaseConstant."' " ,"","");
		$row = $this->getTotalRow($rst);
	
		return $row;
	}
        
        //Start : Change Status==========================
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_LANGUAGE,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status = "Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "update ".TBL_LANGUAGE." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		echo $status;		
	}
	
        
        //Start : Delete Value=============================
	function deleteValue($get){		
                $date = date("Y-m-d h:i:s");                
                $sql="update ".TBL_LANGUAGE." set isDeleted='1', modDate='".$date."', modBy='".$_SESSION['ADMIN_ID']."' where id='$get[id]'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
                echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";exit;
        }
	function addNewLanguageNew($post,$file) {
		$date = date("Y-m-d h:i:s");
		$_SESSION['SESS_MSG'] = "";
		if($file['languageFlag']['name']){
  			$filename = stripslashes($file['languageFlag']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = FLAGORIGINAL.$image_name;
			$filestatus = 	move_uploaded_file($file['languageFlag']['tmp_name'], $target);
			chmod($target, 0777);
			if($filestatus){
				$imgSource = $target;	 
				$LargeImage = FLAGORIGINAL.$image_name;
				$ThumbImage = FLAGTHUMB.$image_name;
				chmod(FLAGORIGINAL,0777);
				chmod(FLAGTHUMB,0777);
				exec(IMAGEMAGICPATH." $imgSource -thumbnail 20x20 $ThumbImage");
				$sql = "INSERT INTO ".TBL_LANGUAGE." SET `languageName`='".$post['languageName']."', `languageCode`='".strtolower($post['languageCode'])."', `languageFlag`='$image_name', `status`='1', `addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."'";
				$rst = $this->executeQry($sql);
				if($rst){
					$this->logSuccessFail("1",$sql);
				}else{
					$this->logSuccessFail("0",$sql);
				}
				unlink($target);
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
				header('Location:addLanguage.php?page=$post[page]');
				echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]';</script>";exit;	
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","There is some error to upload flag.!!!");
			}
		}else{
			$_SESSION['SESS_MSG'] =msgSuccessFail("fail","Please upload flag.!!!");
		}
	}
	
                
                
	function addNewLanguage($post , $file){
                //echo "<pre>";print_r($_POST);print_r($_FILES); print_r($arr_error);echo "</pre>";exit;
		$date = date("Y-m-d h:i:s");
		$_SESSION['SESS_MSG'] = "";
		if($file['languageFlag']['name']){
  			$filename = stripslashes($file['languageFlag']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = FLAGORIGINAL.$image_name;                       
			$filestatus = 	move_uploaded_file($file['languageFlag']['tmp_name'], $target);
			chmod($target, 0777);
			if($filestatus){
				$imgSource = $target;	 
				$LargeImage = FLAGORIGINAL.$image_name;
				$ThumbImage = FLAGTHUMB.$image_name;
				@chmod(FLAGORIGINAL,0777);
				@chmod(FLAGTHUMB,0777);
				exec(IMAGEMAGICPATH." $imgSource -thumbnail 20x20 $ThumbImage");
				$sql = "INSERT INTO ".TBL_LANGUAGE." SET `languageName`='".$post['languageName']."', `languageCode`='".strtolower($post['languageCode'])."', `languageFlag`='$image_name', `status`='1', `addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."'";		
                                $rst = $this->executeQry($sql);	    

				if($rst){
					$this->logSuccessFail("1",$sql);
				}else{
					$this->logSuccessFail("0",$sql);
				}
				@unlink($target);                              
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
				header('Location:addLanguage.php?page=$post[page]');
				echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]';</script>";exit;	
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","There is some error to upload flag.!!!");
			}
		}else{
			$_SESSION['SESS_MSG'] =msgSuccessFail("fail","Please upload flag.!!!");
		}
	}
	
	
	function copyRecord($langId){
		$rowLang   = $this->getDefaultLanuage();	
		$defLangId = $rowLang->id;
		$sql    =  "SELECT * FROM ".TBL_LANGATTRIBUTE." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){			
			$sql = $this->executeQry("INSERT INTO ".TBL_LANGATTRIBUTE." SET `welcome`='".$row->welcome."', `langId`='".$langId."', `Login`='".$row->Login."', `logout`='".$row->logout."', `Signup`='".$row->Signup."', `Signup_Page_Main_Message`='".$row->Signup_Page_Main_Message."', `Login_information`='".$row->Login_information."', `EmailId`='".$row->EmailId."', `Password`='".$row->Password."', `Confirm_Password`='".$row->Confirm_Password."', `Invalid_Email_Id_Msg`='".$row->Invalid_Email_Id_Msg."', `Email_Already_Exists_Msg`='".$row->Email_Already_Exists_Msg."', `Email_does_not_Exists_Msg`='".$row->Email_does_not_Exists_Msg."', `Password_Length_Msg`='".$row->Password_Length_Msg."', `Empty_Password_Msg`='".$row->Empty_Password_Msg."', `Confirm_Password_Msg`='".$row->Confirm_Password_Msg."', `Personal_Information`='".$row->Personal_Information."', `Title_Error_Msg`='".$row->Title_Error_Msg."', `First_Name_Error_Msg`='".$row->First_Name_Error_Msg."', `Last_Name_Error_Msg`='".$row->Last_Name_Error_Msg."', `Street_Address_Error_Msg`='".$row->Street_Address_Error_Msg."', `Suit_Error_Msg`='".$row->Suit_Error_Msg."', `City_Error_Msg`='".$row->City_Error_Msg."', `State_Error_Msg`='".$row->State_Error_Msg."', `Zip_Code_Error_Msg`='".$row->Zip_Code_Error_Msg."', `Country_Error_Msg`='".$row->Country_Error_Msg."', `County_Error_Msg`='".$row->County_Error_Msg."', `Contact_Number_Error_Msg`='".$row->Contact_Number_Error_Msg."', `Fax_Number_Error_Msg`='".$row->Fax_Number_Error_Msg."', `Verification_Code_Error_Msg`='".$row->Verification_Code_Error_Msg."', `Signup_Check_Terms_Text`='".$row->Signup_Check_Terms_Text."', `Terms_and_Conditions`='".$row->Terms_and_Conditions."', `Submit_Button_Text`='".$row->Submit_Button_Text."', `Please_Select`='".$row->Please_Select."', `White_Spaces_Not_Allowed`='".$row->White_Spaces_Not_Allowed."', `Check_Agreement_Box_Msg`='".$row->Check_Agreement_Box_Msg."', `Login_Page_Main_Message`='".$row->Login_Page_Main_Message."', `Dont_have_an_account`='".$row->Dont_have_an_account."', `No_Worries`='".$row->No_Worries."', `Create_one_now`='".$row->Create_one_now."', `It_is_easy`='".$row->It_is_easy."', `Remember_Me`='".$row->Remember_Me."', `Forget_your_password`='".$row->Forget_your_password."', `Resend_Activation_mail`='".$row->Resend_Activation_mail."', `Invalid_login_details_message`='".$row->Invalid_login_details_message."', `account_not_activated_Msg`='".$row->account_not_activated_Msg."', `account_banned_Msg`='".$row->account_banned_Msg."', `your_Account`='".$row->your_Account."', `forget_password_page_text`='".$row->forget_password_page_text."', `reset_password`='".$row->reset_password."', `reset_password_page_text`='".$row->reset_password_page_text."', `password_change_success_msg`='".$row->password_change_success_msg."', `password_change_fail_msg`='".$row->password_change_fail_msg."', `resend_activation_code_text`='".$row->resend_activation_code_text."', `account_already_activated_Msg`='".$row->account_already_activated_Msg."', `account_activated_successfully_Msg`='".$row->account_activated_successfully_Msg."', `problem_to_activate_account_Msg`='".$row->problem_to_activate_account_Msg."', `Testimonials`='".$row->Testimonials."', `Information_Add_Successfully`='".$row->Information_Add_Successfully."', `Forum`='".$row->Forum."', `New_Thread`='".$row->New_Thread."', `Sent_For_Admin_Approval`='".$row->Sent_For_Admin_Approval."', `Special_Characters_not_allowed_Msg`='".$row->Special_Characters_not_allowed_Msg."', `Thread_Title`='".$row->Thread_Title."', `Thread_Description`='".$row->Thread_Description."', `New_Post`='".$row->New_Post."', `This_field_is_required`='".$row->This_field_is_required."', `Forum_Topic`='".$row->Forum_Topic."', `Posted_By`='".$row->Posted_By."', `Posted_Date`='".$row->Posted_Date."', `Title`='".$row->Title."', `No_Of_Thread`='".$row->No_Of_Thread."', `No_Of_Post`='".$row->No_Of_Post."', `Last_Post`='".$row->Last_Post."', `Forum_Post`='".$row->Forum_Post."', `Name`='".$row->Name."', `Price`='".$row->Price."', `BUY_NOW`='".$row->BUY_NOW."', `Customize`='".$row->Customize."', `No_Record_Found`='".$row->No_Record_Found."', `Categories`='".$row->Categories."', `Quantity`='".$row->Quantity."', `Product_Info`='".$row->Product_Info."', `Availability_In_stock`='".$row->Availability_In_stock."', `Add_To_Your_Basket`='".$row->Add_To_Your_Basket."', `Product_Name`='".$row->Product_Name."', `Total_Price`='".$row->Total_Price."', `Edit_This_Product`='".$row->Edit_This_Product."', `Delete_basket`='".$row->Delete_basket."', `Cart`='".$row->Cart."', `Not_Available_In_Stock`='".$row->Not_Available_In_Stock."', `Continue_Shopping`='".$row->Continue_Shopping."', `Proceed_To_Purchase`='".$row->Proceed_To_Purchase."', `Shopping_Cart`='".$row->Shopping_Cart."', `Address`='".$row->Address."', `Payment_And_Shipping`='".$row->Payment_And_Shipping."', `Verify_Order_And_Make_Payment`='".$row->Verify_Order_And_Make_Payment."', `Check_Order_Information`='".$row->Check_Order_Information."', `Already_Registered`='".$row->Already_Registered."', `Create_Your_Account`='".$row->Create_Your_Account."', `Please_Enter_Your_Email_Id_For_Your_Registration`='".$row->Please_Enter_Your_Email_Id_For_Your_Registration."', `Check_If_Shipping_Address_Is_Not_Same_As_Billing_Address`='".$row->Check_If_Shipping_Address_Is_Not_Same_As_Billing_Address."', `First_Name`='".$row->First_Name."', `Street_Address`='".$row->Street_Address."', `Town_City`='".$row->Town_City."', `Country`='".$row->Country."', `State`='".$row->State."', `Last_Name`='".$row->Last_Name."', `Suite_APT_Floor`='".$row->Suite_APT_Floor."', `Fax_No`='".$row->Fax_No."', `Zip_Postal_Code`='".$row->Zip_Postal_Code."', `Phone_No`='".$row->Phone_No."', `Your_Billing_Address`='".$row->Your_Billing_Address."', `Your_Shipping_Address`='".$row->Your_Shipping_Address."', `User_Identification`='".$row->User_Identification."', `My_Account`='".$row->My_Account."', `View_user_details`='".$row->View_user_details."', `Your_Shopping_Cart_Is_Empty`='".$row->Your_Shopping_Cart_Is_Empty."', `Billing_Address`='".$row->Billing_Address."', `Shipping_Address`='".$row->Shipping_Address."', `Ch_ange`='".$row->Ch_ange."', `Paypal`='".$row->Paypal."', `I_would_Like_To_Pay_By_Paypal`='".$row->I_would_Like_To_Pay_By_Paypal."', `Edit_User_Account`='".$row->Edit_User_Account."', `Change_Password`='".$row->Change_Password."', `Changepassword_Page_Main_Message`='".$row->Changepassword_Page_Main_Message."', `Old_Password`='".$row->Old_Password."', `New_Password`='".$row->New_Password."', `Retype_New_Password`='".$row->Retype_New_Password."', `Enter_Correct_Old_Password_Msg`='".$row->Enter_Correct_Old_Password_Msg."', `Changepassword_Successfully_Msg`='".$row->Changepassword_Successfully_Msg."', `First_Name_Error_Msg_Shipping`='".$row->First_Name_Error_Msg_Shipping."', `Last_Name_Error_Msg_Shipping`='".$row->Last_Name_Error_Msg_Shipping."', `Information_Updated_Successfully`='".$row->Information_Updated_Successfully."', `Payment_Cancelled`='".$row->Payment_Cancelled."', `Payment_Cancelled_Message`='".$row->Payment_Cancelled_Message."', `Testimonial_Image_Upload_Error_Message1`='".$row->Testimonial_Image_Upload_Error_Message1."', `Testimonial_Image_Upload_Error_Message`='".$row->Testimonial_Image_Upload_Error_Message."', `Order_History`='".$row->Order_History."', `Payment_Status_Type`='".$row->Payment_Status_Type."', `Order_No`='".$row->Order_No."', `Order_Date`='".$row->Order_Date."', `Status`='".$row->Status."', `Amount`='".$row->Amount."', `Detail`='".$row->Detail."', `Print`='".$row->Print."', `Shipping`='".$row->Shipping."', `Total`='".$row->Total."', `View_Change`='".$row->View_Change."', `Checkout`='".$row->Checkout."', `ZIP_CODE_ERROR_MSG_VALID`='".$row->ZIP_CODE_ERROR_MSG_VALID."', `Delivery_Costs`='".$row->Delivery_Costs."', `Coupon_Costs`='".$row->Coupon_Costs."', `Do_you_want_to_delete_this_product_from_you_cart`='".$row->Do_you_want_to_delete_this_product_from_you_cart."', `Choice_Your_Available_Color`='".$row->Choice_Your_Available_Color."', `Sign_up_for_our_email_newsletter`='".$row->Sign_up_for_our_email_newsletter."', `Receive_updates_and_exclusive_offers`='".$row->Receive_updates_and_exclusive_offers."', `Sign_me_Up`='".$row->Sign_me_Up."', `Customer_Photos`='".$row->Customer_Photos."', `See_our_customer_in`='".$row->See_our_customer_in."', `action`='".$row->action."', `Hot_Designs`='".$row->Hot_Designs."', `view_all`='".$row->view_all."', `Choose_the_one_made_just_for_you`='".$row->Choose_the_one_made_just_for_you."', `Get_Ready_to_give_your_friends_a_big_surprise`='".$row->Get_Ready_to_give_your_friends_a_big_surprise."', `Please_Enter_Name`='".$row->Please_Enter_Name."', `Please_Enter_Message`='".$row->Please_Enter_Message."', `Your_Query_Sent_To_Admin`='".$row->Your_Query_Sent_To_Admin."', `other`='".$row->other."', `Links`='".$row->Links."', `About_Us`='".$row->About_Us."', `Privacy_Policy`='".$row->Privacy_Policy."', `Terms_of_Use`='".$row->Terms_of_Use."', `FAQ`='".$row->FAQ."', `Contact_Us`='".$row->Contact_Us."', `Cat_Main_Page`='".$row->Cat_Main_Page."', `Cat_Create_Your_Own`='".$row->Cat_Create_Your_Own."', `Cat_Faq`='".$row->Cat_Faq."', `Cat_Support`='".$row->Cat_Support."', `Follow_us_on`='".$row->Follow_us_on."', `Subscribe_to_blog`='".$row->Subscribe_to_blog."', `Follow_us_on_Twitter`='".$row->Follow_us_on_Twitter."', `Be_A_Fan_On_Facebook`='".$row->Be_A_Fan_On_Facebook."', `Join_Us_On_LinkedIn`='".$row->Join_Us_On_LinkedIn."', `Header_Contact`='".$row->Header_Contact."', `Header_Sitemap`='".$row->Header_Sitemap."', `Header_Bookmark`='".$row->Header_Bookmark."', `All_rights_reserved`='".$row->All_rights_reserved."', `Header_PERSONALIZE_YOUR_PRODUCTS`='".$row->Header_PERSONALIZE_YOUR_PRODUCTS."', `Email_Id`='".$row->Email_Id."', `About_You`='".$row->About_You."', `Location`='".$row->Location."', `Website`='".$row->Website."', `Image`='".$row->Image."', `Message`='".$row->Message."', `Verification_Code`='".$row->Verification_Code."', `Welcome_to_Presonalize_Your_Products`='".$row->Welcome_to_Presonalize_Your_Products."', `COUPON_NUMBER`='".$row->COUPON_NUMBER."', `COUPON_APPLY`='".$row->COUPON_APPLY."', `Enter_Coupon_Code`='".$row->Enter_Coupon_Code."', `Your_Coupon_Value_Is`='".$row->Your_Coupon_Value_Is."', `Wrong_Coupon_Code`='".$row->Wrong_Coupon_Code."', `You_Have_Already_Used_This_Coupon`='".$row->You_Have_Already_Used_This_Coupon."', `My_Design`='".$row->My_Design."', `CONFIRM_EMAILID`='".$row->CONFIRM_EMAILID."', `CONFIRM_EMAIL_MSG`='".$row->CONFIRM_EMAIL_MSG."', `PROCEED_TO_PAY`='".$row->PROCEED_TO_PAY."', `Color`='".$row->Color."', `WANT_THIS_DESIGN_ON_YOUR_T_SHIRT`='".$row->WANT_THIS_DESIGN_ON_YOUR_T_SHIRT."', `START_CREATING_NOW`='".$row->START_CREATING_NOW."', `HOT_PRODUCT`='".$row->HOT_PRODUCT."', `Size`='".$row->Size."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/*			Language Attribute End			*/
		
		
		
		/*			Banner Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_BANNER_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_BANNER_DESCRIPTION." SET `bannerId`='".$row->bannerId."', `langId`='".$langId."', `bannerTitle`='".$row->bannerTitle."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/*			Banner End			*/
		
		/*			Category Start		*/
		
		$sql  	  =  "SELECT * FROM ".TBL_CATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query 	  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_CATEGORY_DESCRIPTION." SET `catId`='".$row->catId."', `langId`='".$langId."', `categoryName`='".$row->categoryName."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Category End		*/
		
		/*			Color Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_COLORDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_COLORDESC." SET `Id`='".$row->Id."', `langId`='".$langId."', `colorName`='".$row->colorName."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Color End		*/
		
		
		/*			Country Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_COUNTRY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_COUNTRY_DESCRIPTION." SET `countryId`='".$row->countryId."', `langId`='".$langId."', `countryName`='".$row->countryName."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Country End		*/
		
		
		/*			County Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_COUNTY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_COUNTY_DESCRIPTION." SET `countyId`='".$row->countyId."', `langId`='".$langId."', `countyName`='".addslashes($row->countyName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			County End		*/
		
		
		/*			Coupon Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_COUPON_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_COUPON_DESCRIPTION." SET `couponId`='".$row->couponId."', `langId`='".$langId."', `description`='".addslashes($row->description)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Coupon End		*/
		
		
		/*			Design Category Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_DESIGNCATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_DESIGNCATEGORY_DESCRIPTION." SET `catId`='".$row->catId."', `langId`='".$langId."', `categoryName`='".addslashes($row->categoryName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Design Category End		*/
		
		
		/*			Font Category Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_FONTCATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_FONTCATEGORY_DESCRIPTION." SET `catId`='".$row->catId."', `langId`='".$langId."', `categoryName`='".addslashes($row->categoryName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Font Category End		*/
		
		/*			Gift Cirtificate Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_GIFTCERTIFICATEDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_GIFTCERTIFICATEDESC." SET `giftCertificateId`='".$row->giftCertificateId."', `langId`='".$langId."', `giftTitle`='".addslashes($row->giftTitle)."' , `giftDescription`='".addslashes($row->giftDescription)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Gift Cirtificate End		*/
		
		
		/*			Email Setting Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_MAILSETTING." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query); 
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			//echo "INSERT INTO ".TBL_MAILSETTING." SET `mailTypeId`='".$row->mailTypeId."', `langId`='".$langId."', `mailSubject`='".$row->mailSubject."' , `mailContaint`='".$row->mailContaint."', `emailid`='".$row->emailid."'".'<br/>';
			$sql = $this->executeQry("INSERT INTO ".TBL_MAILSETTING." SET `mailTypeId`='".$row->mailTypeId."', `langId`='".$langId."', `mailSubject`='".addslashes($row->mailSubject)."' , `mailContaint`='".addslashes($row->mailContaint)."', `emailid`='".$row->emailid."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Email Setting End		*/
	
	
		
		/*			Main Product Color Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){					
			$sql = $this->executeQry("INSERT INTO ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." SET `product_color_id`='".$row->product_color_id."', `langId`='".$langId."', `colorTitle`='".addslashes($row->colorTitle)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Main Product Color Description End		*/


	/*			Main Product Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_MAIN_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_MAIN_PRODUCT_DESCRIPTION." SET `productId`='".$row->productId."', `langId`='".$langId."', `productName`='".addslashes($row->productName)."', `productDesc`='".addslashes($row->productDesc)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Main Product Description End		*/
		





		/*			Main Product Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_PRODUCT_DESCRIPTION." SET `productId`='".$row->productId."', `langId`='".$langId."', `productName`='".addslashes($row->productName)."', `productDesc`='".addslashes($row->productDesc)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Main Product Description End		*/
		
		/*			Meta Data Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_METADATADESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_METADATADESC." SET `metaDataId`='".$row->metaDataId."', `langId`='".$langId."', `metaTitle`='".addslashes($row->metaTitle)."', `metaKeyword`='".addslashes($row->metaKeyword)."', `metaDescription`='".addslashes($row->metaDescription)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Meta Data Description End		*/
		
		/*			Newslatter Start		*/		
		$sql    =  "SELECT * FROM ".TBL_NEWSLETTER." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_NEWSLETTER." SET `senderEmailId`='".$row->senderEmailId."', `langId`='".$langId."', `newsletterSubject`='".addslashes($row->newsletterSubject)."', `newsletterContaint`='".addslashes($row->newsletterContaint)."', `attachedFile`='".addslashes($row->attachedFile)."', `attachmentPath`='".addslashes($row->attachmentPath)."', `status`='".$row->status."'");
			}	// While Loop End
		}	//	If Loop End
		
		/* 			Newslatter End		*/
		
		
		/*			Order Status Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_ORDERSTATUSDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_ORDERSTATUSDESC." SET `orderStatusId`='".$row->orderStatusId."', `langId`='".$langId."', `orderStatus`='".addslashes($row->orderStatus)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Order Status End		*/
		
		/*			Payment Structure Desc Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_PAYMENTSTRDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){			
			$sql = $this->executeQry("INSERT INTO ".TBL_PAYMENTSTRDESC." SET `viewId`='".$row->viewId."', `langId`='".$langId."', `viewName`='".addslashes($row->viewName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Payment Structure Desc  End		*/
		
		/*			Product Color Desc Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_PRODUCT_COLOR_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){			
			$sql = $this->executeQry("INSERT INTO ".TBL_PRODUCT_COLOR_DESCRIPTION." SET `product_color_id`='".$row->product_color_id."', `langId`='".$langId."', `colorTitle`='".addslashes($row->colorTitle)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Product Color Desc  End		*/
		
		
		/*			Product Desciption Start		*/		
		$sql    =  "SELECT * FROM ".TBL_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_PRODUCT_DESCRIPTION." SET `productId`='".$row->productId."', `langId`='".$langId."', `productName`='".addslashes($row->productName)."', `productDesc`='".addslashes($row->productDesc)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Product Desciption End		*/

		/*			Sign Up Desciption Start		*/		
		$sql    =  "SELECT * FROM ".TBL_SIGNUPFIELDS_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_SIGNUPFIELDS_DESCRIPTION." SET `fieldId`='".$row->fieldId."', `langId`='".$langId."', `fieldDesc`='".addslashes($row->fieldDesc)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Sign Up Desciption End		*/		
		
		/*			Size Desciption Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_SIZEDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_SIZEDESC." SET `Id`='".$row->Id."', `langId`='".$langId."', `sizeName`='".addslashes($row->sizeName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Size Desciption End		*/		
		
		/*			State Desciption Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_STATE_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_STATE_DESCRIPTION." SET `stateId`='".$row->stateId."', `langId`='".$langId."', `stateName`='".addslashes($row->stateName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			State Desciption End		*/	
		
		/*			Static Pages Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_STATICPAGESETTING." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_STATICPAGESETTING." SET `staticPageTypeId`='".$row->staticPageTypeId."', `langId`='".$langId."', `pageTitle`='".addslashes($row->pageTitle)."', `pageContaint`='".addslashes($row->pageContaint)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Static Pages End		*/
		
		/*			Tool Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_TOOLDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_TOOLDESC." SET `Id`='".$row->Id."', `langId`='".$langId."', `toolName`='".addslashes($row->toolName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Tool Description End		*/
		
		/*			View Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_VIEWDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_VIEWDESC." SET `viewId`='".$row->viewId."', `langId`='".$langId."', `viewName`='".addslashes($row->viewName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			View Description End		*/		
		
		/*			Zone Description Start		*/
		
		$sql    =  "SELECT * FROM ".TBL_ZONE_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		$totRow = $this->getTotalRow($query);
	
		if($totRow > 0) {				
			while($row = $this->getResultObject($query)){
			$sql = $this->executeQry("INSERT INTO ".TBL_ZONE_DESCRIPTION." SET `zoneId`='".$row->zoneId."', `langId`='".$langId."', `zoneName`='".addslashes($row->zoneName)."'");
			}	// While Loop End
		}	//	If Loop End	
		
		/* 			Zone Description End		*/		
		
		}
		
	
	private function deleteLanguage($defLangId)		{
		$sql = $this->executeQry("DELETE FROM ".TBL_LANGATTRIBUTE." WHERE 1 AND `langId`='".$defLangId."'");
		/*			Language Attribute End			*/

		/*			Banner Start		*/		
		$sql    =  "DELETE FROM ".TBL_BANNER_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 		
		/*			Banner End			*/
		
		
		/*			Category Start		*/
		
		$sql  	  =  "DELETE FROM ".TBL_CATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query 	  = $this->executeQry($sql); 
		
		/* 			Category End		*/
		
		/*			Color Start		*/
		
		$sql    =  "DELETE FROM ".TBL_COLORDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 	
		/* 			Color End		*/
		
		
		/*			Country Start		*/
		
		$sql    =  "DELETE FROM ".TBL_COUNTRY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Country End		*/
		
		
		/*			County Start		*/
		
		$sql    =  "DELETE FROM ".TBL_COUNTY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		/* 			County End		*/
		
		
		/*			Coupon Start		*/
		
		$sql    =  "DELETE FROM ".TBL_COUPON_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		
		/* 			Coupon End		*/
		
		
		/*			Design Category Start		*/
		
		$sql    =  "DELETE FROM ".TBL_DESIGNCATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Design Category End		*/
		
		
		/*			Font Category Start		*/
		
		$sql    =  "DELETE FROM ".TBL_FONTCATEGORY_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Font Category End		*/
		
		/*			Gift Cirtificate Start		*/
		
		$sql    =  "DELETE FROM ".TBL_GIFTCERTIFICATEDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
	
		/* 			Gift Cirtificate End		*/
		
		
		/*			Email Setting Start		*/
		
		$sql    =  "DELETE FROM ".TBL_MAILSETTING." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
	
		
		/* 			Email Setting End		*/
	
	
		
		/*			Main Product Color Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_MAIN_PRODUCT_COLOR_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Main Product Color Description End		*/


		/*			Main Product Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_MAIN_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 		
		/* 			Main Product Description End		*/

		/*			Main Product Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Main Product Description End		*/
		
		/*			Meta Data Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_METADATADESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		
		/* 			Meta Data Description End		*/
		
		/*			Newslatter Start		*/		
		$sql    =  "DELETE FROM ".TBL_NEWSLETTER." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		
		/* 			Newslatter End		*/
		
		
		/*			Order Status Start		*/
		
		$sql    =  "DELETE FROM ".TBL_ORDERSTATUSDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		
		/* 			Order Status End		*/
		
		/*			Payment Structure Desc Start		*/
		
		$sql    =  "DELETE FROM ".TBL_PAYMENTSTRDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		/* 			Payment Structure Desc  End		*/
		
		/*			Product Color Desc Start		*/
		
		$sql    =  "DELETE FROM ".TBL_PRODUCT_COLOR_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Product Color Desc  End		*/
		
		
		/*			Product Desciption Start		*/		
		$sql    =  "DELETE FROM ".TBL_PRODUCT_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
	
		/* 			Product Desciption End		*/

		/*			Sign Up Desciption Start		*/		
		$sql    =  "DELETE FROM ".TBL_SIGNUPFIELDS_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		
		/* 			Sign Up Desciption End		*/		
		
		/*			Size Desciption Start		*/
		
		$sql    =  "DELETE FROM ".TBL_SIZEDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
	
		/* 			Size Desciption End		*/		
		
		/*			State Desciption Start		*/
		
		$sql    =  "DELETE FROM ".TBL_STATE_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		/* 			State Desciption End		*/	
		
		/*			Static Pages Start		*/
		
		$sql    =  "DELETE FROM ".TBL_STATICPAGESETTING." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 

		/* 			Static Pages End		*/
		
		/*			Tool Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_TOOLDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		/* 			Tool Description End		*/
		
		/*			View Description Start		*/
		
		$sql    =  "DELETE FROM ".TBL_VIEWDESC." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql); 
		/* 			View Description End		*/		
		
		/*			Zone Description Start		*/		
		$sql    =  "DELETE FROM ".TBL_ZONE_DESCRIPTION." WHERE 1 AND langId='".$defLangId."'";
		$query  = $this->executeQry($sql);
		/* 			Zone Description End		*/		
		
		}
		
	
	function getDefaultLanuage(){
		$query = mysql_query("SELECT L.* from ".TBL_LANGUAGE." AS L Left Join ".TBL_LANGATTRIBUTE." AS LA on L.id = LA.langId WHERE L.status='1' AND L.isDefault='1' AND LA.langId > 0");
		$rowLang  = mysql_fetch_object($query);
		return $rowLang;
	}
	
	
	
	function editLanguage($post , $file) {
                //echo "<pre>";print_r($post);echo "</pre>";exit;
                $date = date("Y-m-d h:i:s");
                $_SESSION['SESS_MSG'] = "";
                $isdefault = $post[isDefault]?1:0;             
                $sql = "UPDATE ".TBL_LANGUAGE." SET `languageName`='".$post['languageName']."',isDefault='$isdefault', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."'";
                if($file['languageFlag']['name']){
                        $filename = stripslashes($file['languageFlag']['name']);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);
                        $image_name = date("Ymdhis").time().rand().'.'.$extension;
                        $target    = FLAGORIGINAL.$image_name;                        
                        $filestatus = 	move_uploaded_file($file['languageFlag']['tmp_name'], $target);
                        @chmod($target, 0777);
                        if($filestatus){
                                $imgSource = $target;	 
                                $LargeImage = FLAGORIGINAL.$image_name;
                                $ThumbImage = FLAGTHUMB.$image_name;
                                @chmod(FLAGORIGINAL,0777);
                                @chmod(FLAGTHUMB,0777);
                                exec(IMAGEMAGICPATH." $imgSource -thumbnail 20x20 $ThumbImage");
                                @unlink($target);
                                $preImg = $this->fetchValue(TBL_LANGUAGE,"languageFlag","id='$post[id]'");                                
                                if(file_exists(FLAGTHUMB.$preImg)){
                                    @unlink(FLAGTHUMB.$preImg);
                                }
                                $sql .= ", `languageFlag`='$image_name' ";
                        }else{
                                $_SESSION['SESS_MSG'] =msgSuccessFail("fail","There is some error to upload flag.!!!");
                        }
                }
                if($isdefault == '1'){
                        $_SESSION['DEFAULTLANGUAGE']  = $post[id];
                        $this->executeQry("update ".TBL_LANGUAGE." set isDefault='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id != '$post[id]'");
                        $sql .= " ,status = '1' ";
                }	
                $sql .= "  WHERE id='$post[id]'"; 
                $rst = $this->executeQry($sql);
                if($rst){
                    $this->logSuccessFail("1",$sql);
                }else{
                    $this->logSuccessFail("0",$sql);
                }	
                echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";exit;
	}

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]&limit=$post[limit]';</script>";exit;
		}		
	
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				$sql = "update ".TBL_LANGUAGE." set isDeleted = '1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'";
				$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_LANGUAGE." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_LANGUAGE." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageLanguage.php?page=$post[page]';</script>";exit;
	}	
	
	function getResult($id) {
		$sql_lang = $this->executeQry("select * from ".TBL_LANGUAGE." where id = '$id'");
		$num_lang = $this->getTotalRow($sql_lang);
		if($num_lang > 0) {
			return $line_lang = $this->getResultRow($sql_lang);	
		} else {
			redirect('manageLanguage.php');
		}	
	}

	function fetattribute($lan,$frontBack="front"){ 
	
		echo "<form name='languageform' action='' method='post'><fieldset>
			<input type='hidden' name='langId'  value='$lan'>
			<input type='hidden' name='frontBack'  value='$frontBack'>";

		if($frontBack=="front"){
			$sql1="select lag1.* FROM ".TBL_LANGATTRIBUTE." as lag1 where lag1.langId='".$lan."' ";
			$rst1=$this->executeQry($sql1);
			$num=$this->getTotalRow($rst1); 
			if($num<=0){
				$sql1="select lag1.* FROM ".TBL_LANGATTRIBUTE." as lag1 where lag1.langId='1' ";
				$rst1=$this->executeQry($sql1);
			}

		while($row=$this->getResultRow($rst1)){
			echo '<section><label >'.ucwords(strtolower(str_replace("_"," ",$row['lang_type']))).'</label><div><textarea  name="'.$row['lang_type'].'"  cols="35" rows="2">'.stripslashes($row['lang_value']).'</textarea> </div></section>';
				}
			}

		/*if($frontBack=="back"){
			$sql1="select lag1.* FROM ".TBL_LANGUAGEATTRIBUTEADMIN." as lag1 where lag1.langId='".$lan."' ";
			$rst1=$this->executeQry($sql1);
			$num=$this->getTotalRow($rst1); 
			if($num<=0){
			$sql1="select lag1.* FROM ".TBL_LANGUAGEATTRIBUTEADMIN." as lag1 where lag1.langId='1' ";
			$rst1=$this->executeQry($sql1);
			}

			while($row=$this->getResultRow($rst1))
				{
				echo '<section><label >'.ucwords(strtolower(str_replace("_"," ",$row['lang_type']))).'</label><div><textarea  name="'.$row['lang_type'].'"  cols="35" rows="2">'.utf8_encode(stripslashes($row['lang_value'])).'</textarea></div></section>';
			}
		} */
		echo "<section><label ></label><div><input type='submit' name='submit' value='SAVE' class=''></div></section></fieldset></form>";
	}
	
	function addlanguage()
	{    
		//print_r($_POST);
		$frontBack=$_POST['frontBack'];
		if($frontBack == "front"){
			$rst=$this->selectQry(TBL_LANGATTRIBUTE,"langId='$_POST[langId]' ",'','');
			$num=$this->getTotalRow($rst);
			
			if($num){
				while($row1=$this->getResultRow($rst)){
					$fiels=addslashes($row1[lang_type]);
					$sql="update  ".TBL_LANGATTRIBUTE." set  lang_value = '".addslashes($_POST[$fiels])."'  where lang_type='".$row1[lang_type]."' and   langId='".$_POST[langId]."'"; 
					 //exit;
					 $this->executeQry($sql); 
				}
			}else{
				$rst=$this->selectQry(TBL_LANGATTRIBUTE,"langId='1' ",'','');
				$num=$this->getTotalRow($rst);
				if($num){
					while($row1=$this->getResultRow($rst)){
						$fiels=addslashes($row1[lang_type]);
						$sql="INSERT INTO ".TBL_LANGATTRIBUTE." (`langId`, `lang_type`, `lang_value`) VALUES ('".$_POST['langId']."','".$row1['lang_type']."','".addslashes($_POST[$fiels])."')";
						$this->executeQry($sql);
					}
				}
			}
			foreach ($_POST as $key => $value) {
				if($value != "" && $key != 'submit' ) {
					$key=strtoupper(str_replace("_","_",$key));
					$value=stripslashes($value);
					$define=$define."define('LANG_$key','".addslashes($value)."');\n";
				}
			}
			$rst=$this->selectQry(TBL_LANGUAGE," status='1' and id='$_POST[langId]' ",'','');
			$row=$this->getResultRow($rst);
			$ourFileName = __DIR_LANGUAGES__.strtolower($row[languageCode]).".inc.php";
			if (file_exists($ourFileName)) {
				@unlink ($ourFileName);
			} 
			$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
			$somecontent="<?php\n$define?>";
			fwrite($ourFileHandle, $somecontent);
			fclose($ourFileHandle);
		}
		/*
		if($frontBack == "back"){
			$rst=$this->selectQry(TBL_LANGUAGEATTRIBUTEADMIN,"langId='$_POST[langId]' ",'','');
			$num=$this->getTotalRow($rst);
			if($num){
				while($row1=$this->getResultRow($rst)){
					$fiels=addslashes($row1[lang_type]);
					$sql="update  ".TBL_LANGUAGEATTRIBUTEADMIN." set  lang_value = '".addslashes($_POST[$fiels])."'  where lang_type='".$row1[lang_type]."' and   langId='".$_POST[langId]."'"; 
					 $this->executeQry($sql); 
				}
			}else{
				$rst=$this->selectQry(TBL_LANGUAGEATTRIBUTEADMIN,"langId='1' ",'','');
				$num=$this->getTotalRow($rst);
				if($num){
					while($row1=$this->getResultRow($rst)){
						$fiels=addslashes($row1[lang_type]);
						$sql="INSERT INTO ".TBL_LANGUAGEATTRIBUTEADMIN." (`langId`, `lang_type`, `lang_value`) VALUES ('".$_POST[langId]."','".$row1[lang_type]."','".addslashes($_POST[$fiels])."')";
						$this->executeQry($sql);
					}
				}
			}
			foreach ($_POST as $key => $value) {
				if($value != "" && $key != 'submit' ) {
					$key=strtoupper(str_replace("_","_",$key));
					$value=stripslashes($value);
					$define=$define."define('LANG_$key','".addslashes($value)."');\n";
				}
			}
			$rst=$this->selectQry(TBL_LANGUAGE," status='1' and id='$_POST[langId]' ",'','');
			$row=$this->getResultRow($rst);
			$ourFileName = __DIR_LANGUAGES_ADMIN__.strtolower($row[languageCode]).".inc.php";
			if (file_exists($ourFileName)) {
				@unlink ($ourFileName);
			} 
			$ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
			$somecontent="<?php\n$define?>";
			fwrite($ourFileHandle, $somecontent);
			fclose($ourFileHandle);
		} */
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been save successfully!!!");
	}
	
	
	function choselanguage($id) {
	
		$rst=$this->selectQry(TBL_LANGUAGE," status='1' and id='$id' ",'','');
		$row=$this->getResultRow($rst);
		return  $row;
	}
	/////// Language Name Exists ////////
	function islanguageNameExit($langname,$id=''){ 
		$langname = trim($langname);
		$rst = $this->selectQry(TBL_LANGUAGE,"languageName='$langname' AND id!='$id' AND isDeleted='0'","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	/////// Language Name Exists ////////
	function islanguageCodeExit($languageCode,$id=''){
		$languageCode = trim($languageCode);
		$rst = $this->selectQry(TBL_LANGUAGE,"languageCode='$languageCode' AND id!='$id'  AND isDeleted='0' ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
}// End Class
?>

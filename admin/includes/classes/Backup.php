<?php 
require("includes/phpzip.inc.php");
session_start();
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
class Backup extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {	
		$query = "select * from ".TBL_BACKUP." where 1 ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"id";
			$order = $_GET[order]? $_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					
					
					$chkbox = '<input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox">';
					
					$genTable .= '<div class="'.$highlight.'">
								 <ul>
								 	<li style="width:50px;">&nbsp;&nbsp;'.$chkbox.'</li>
									<li style="width:90px;">'.$i.'</li>
									<li style="width:250px;">'.$line->fileName.'</li>';
																											
					$genTable .= '<li style="width:120px;">';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a href="pass.php?action=backup&type=downloadBackup&id='.$line->id.'" )"><img src="images/download.png" alt="Download" title="Download" width="20" height="20" border="0" /></a> | <a href="pass.php?action=backup&type=restore&id='.$line->id.'" )"><img src="images/restore.png" alt="Restore" title="Restore" width="20" height="20" border="0" /></a>';
					}	
					$genTable .= '</li><li style="width:55px;">';
				
					if($menuObj->checkDeletePermission() && (!$line->isDefault)) {					
						$genTable .= "<a href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=backup&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</li></ul></div>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	
	

	
	function deleteValue($get) {	
		$dbFile = $this->fetchValue(TBL_BACKUP,"fileName","1 and id = '".$get[id]."'");
		@unlink(__DIR_BACKUPS__.$dbFile);
		$sql = "delete from ".TBL_BACKUP." WHERE id = '".$get[id]."'";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageBackup.php?page=$post[page]&limit=$post[limit]';</script>";
	}
	
	function takeBackup() {	
		global $config;
		
		$temp=$config['database'] . '-' . date('YmdHis');
		$backup_file =  $temp.'.sql';
		mkdir(__DIR_BACKUPS__.$temp,0777);
		@chmod(__DIR_BACKUPS__.$temp,0777);
		$fp = fopen(__DIR_BACKUPS__ .$temp.'/'.$backup_file, 'w');
		@chmod(__DIR_BACKUPS__ .$temp.'/'.$backup_file,0777);
		
		$dt = date('Y-m-d H:i:s');
		$sql = "insert into ".TBL_BACKUP."(fileName,addDate) values('$backup_file','$dt')";
		$rs = $this->query($sql);	
		$schema = '# Database Backup for '.SITENAME.' ' . "\n" .
                  '# Database Name: ' . $config['database'] . "\n" .
                  '# Database Server: ' . $config['server'] . "\n" .
				  '# ' . "\n" .
                  '# Backup Date: ' . date('Y-m-d :: H:i:s') . "\n\n";
		fputs($fp, $schema);
		$sql = "show tables from ". $config['database'];  
		$rs = $this->executeQry($sql);
		$num = mysql_num_rows($rs);
		for($j=1;$j<=$num;$j++){
			$tables = mysql_fetch_array($rs);
			$tbl = $tables[0];
			$schema = 'drop table if exists ' . $tbl . ';' . "\n" .
                    'create table ' . $tbl . ' (' . "\n";
			$table_list = array();
			$sql = "show fields from ".$tbl;
			$field_query = $this->executeQry($sql);
			while ($fields = mysql_fetch_array($field_query, MYSQL_ASSOC)) {
				$table_list[] = $fields['Field'];
				$schema .= '  ' . $fields['Field'] . ' ' . $fields['Type'];
				if (strlen($fields['Default']) > 0) $schema .= ' default \'' . $fields['Default'] . '\'';
				if ($fields['Null'] != 'YES') $schema .= ' not null';
				if (isset($fields['Extra'])) $schema .= ' ' . $fields['Extra'];
				$schema .= ',' . "\n";
			} 
			$schema = ereg_replace(",\n$", '', $schema);

			$index = array();
			$sql = "show keys from ". $tbl;
			$keys_query = $this->executeQry("show keys from " . $tbl);
			while($keys = mysql_fetch_array($keys_query)) {
				$kname = $keys['Key_name'];
				if (!isset($index[$kname])) {
					$index[$kname] = array('unique' => !$keys['Non_unique'],
                                     'columns' => array());
				}
				$index[$kname]['columns'][] = $keys['Column_name'];
			}

			while (list($kname, $info) = each($index)) {
				$schema .= ',' . "\n";
				$columns = implode($info['columns'], ', ');
				if ($kname == 'PRIMARY') {
					$schema .= '  PRIMARY KEY (' . $columns . ')';
				} elseif ($info['unique']) {
					$schema .= '  UNIQUE ' . $kname . ' (' . $columns . ')';
				} else {
					$schema .= '  KEY ' . $kname . ' (' . $columns . ')';
				}
			}

			$schema .= "\n" . ');' . "\n\n";
			fputs($fp, $schema);
	
			$rows_query = mysql_query("select " . implode(',', $table_list) . " from " . $tbl);
			while (@$rows = mysql_fetch_array($rows_query)) {
				$schema = 'insert into ' . $tbl . ' (' . implode(', ', $table_list) . ') values (';

				reset($table_list);
				$i=1;
				while (list(,$i) = each($table_list)) {
					if (!isset($rows[$i])) {
						$schema .= 'NULL, ';
					} elseif (isset($rows[$i])) {
						$row = addslashes($rows[$i]);
						$row = ereg_replace("\n#", "\n".'\#', $row);
						$schema .= '\'' . $row . '\', ';
					} else {
						$schema .= '\'\', ';
					}
				}
				$schema = ereg_replace(', $', '', $schema) . ');' . "\n";
				fputs($fp, $schema);
			}
		}// end for loop
		fclose($fp);		  	
		//$zipObj = new PHPZip;		
		//$fineZipName = __DIR_BACKUPS__.$temp.".zip";	
		//$sqlfile = file_get_contents('db_backups/'.$temp.'/'.$backup_file);
		//$zipObj -> addFile($sqlfile, 'wholetshirt.sql');
		//$phpfile = $zipObj ->GetFileList('../php/');
		//for($j=0;$j<count($phpfile);$j++){
		//$phpfilecnt = file_get_contents('../php/'.$phpfile[$j]);		
		//$zipObj -> addFile($phpfilecnt,$phpfile[$j]);
		//}	
		//$xmlfile = file_get_contents('../php/1.xml');
		//$zipObj -> addFile($xmlfile, '1.xml');	
		//$zipObj -> addFile($phpfile, 'php/data.php/');						
		//$zipObj->myZip('../files', $fineZipName);
		mkdir(__DIR_BACKUPS__.$temp.'files',0777);
		chmod(__DIR_BACKUPS__.$temp.'files',0777);
		mkdir(__DIR_BACKUPS__.$temp.'php',0777);
		chmod(__DIR_BACKUPS__.$temp.'php',0777);
		$this->MAKE_RECURSIVE_DIRS("../files/",'db_backups/'.$temp.'files/');
	    $this->MAKE_RECURSIVE_DIRS("../php/",'db_backups/'.$temp.'php/');
		$this->DELETE_RECURSIVE_DIRS("../files/");
	    $this->DELETE_RECURSIVE_DIRS("../php/");
		
		$this->TruncateDatabase();
		$Secure_Login = new Secure_Login();
		$Secure_Login->Logout();
		echo "All data has been successfully cleaned!";					
	}
		function MAKE_RECURSIVE_DIRS($dirname,$srcname) 
	{ 
		if(is_dir($dirname)){
			$dir_handle=opendir($dirname); 
			while($file=readdir($dir_handle)) 
			{ 
				if($file!="." && $file!="..") 
				{  
					if(!is_dir($dirname."/".$file)) {					    
						//unlink ($dirname."/".$file);
						 copy($dirname."/".$file,$srcname."/".$file);
						
					}
					else
					{ 
					    mkdir($srcname."/".$file);
						@chmod($srcname."/".$file,0777);
						$this->MAKE_RECURSIVE_DIRS($dirname."/".$file,$srcname."/".$file);
						//@rmdir($dirname."/".$file);
					}
				}
			}
			closedir($dir_handle); 
			//@rmdir($dirname);
			return true;
		} else
		return false; 
	}
	
	
		function DELETE_RECURSIVE_DIRS($dirname) 
	{ 
		if(is_dir($dirname)){
			$dir_handle=opendir($dirname); 
			while($file=readdir($dir_handle)) 
			{ 
				if($file!="." && $file!="..") 
				{  
					if(!is_dir($dirname."/".$file)) {
					    if($dirname."/".$file == 'data.php'){
						
						}else{
						unlink($dirname."/".$file);
						}
					}
					else
					{ 
						$this->DELETE_RECURSIVE_DIRS($dirname."/".$file);
						//@rmdir($dirname."/".$file);
					}
				}
			}
			closedir($dir_handle); 
			//@rmdir($dirname);
			return true;
		} else
		return false; 
	}
	


    function restoreDB($id)  {
	 	global $config;		
		$dbfile = $this->fetchValue(TBL_BACKUP,"fileName", "id = $id");	
		
		$expfilename = explode('.',$dbfile);	
		if ($dbfile) {
			$query = "DB Backup";
			$this->logSuccessFail('1',$query);
			$filePath = __DIR_BACKUPS__.$expfilename[0].'/'.$dbfile;
			if(file_exists($filePath)) {		 
				$lines = @file($filePath);
				if(!is_array($lines)) {
					$_SESSION['SESS_MSG'] = msgSuccessFail("fail","SQL File is empty!!!");
			       	} else	{			
					foreach($lines as $line)
					{
						$sql .= trim($line);
						if(empty($sql))
						{
							$sql = '';
							continue;
						}
						elseif(preg_match("/^[#-].*+\r?\n?/i",trim($line)))
						{
							$sql = '';
							continue;
						}
						elseif(!preg_match("/;[\r\n]+/",$line))
							continue;
						$sql;
					
						@mysql_query($sql);
						if(mysql_error() != '')
						{
							$this->error .= '<br>'.mysql_error();
						}
			
						$sql = '';
					}
					if(!empty($this->error)) {
						$_SESSION['SESS_MSG'] = msgSuccessFail("fail",$this->error);
				      
					} else {
					     $this->MAKE_RECURSIVE_DIRS('db_backups/'.$expfilename[0].'files/',"../files/");
	                     $this->MAKE_RECURSIVE_DIRS('db_backups/'.$expfilename[0].'php/',"../php/"); 
				         
						$_SESSION['SESS_MSG'] = msgSuccessFail("success","DB Restore Successfully!!!");
						
				       	
					}						
				}				
			} else {
				$_SESSION['SESS_MSG'] = msgSuccessFail("fail","Sorry no db file exists!!!");
		      	
			}			 
		} else {
			$_SESSION['SESS_MSG'] = msgSuccessFail("fail","Sorry no db file exists!!!");
	       
		}
		 $Secure_Login = new Secure_Login();
		 $Secure_Login->Logout();
		echo $_SESSION['SESS_MSG'];			
	}	
	
	
	function downloadBackup($get) {
		$fileName = $this->fetchValue(TBL_BACKUP,"fileName","1 and id = '".$get[id]."'");
		$fileNameArr = explode('.',$fileName);
		if(file_exists(__DIR_BACKUPS__.$fileName)) {
			$fileArr[] = __DIR_BACKUPS__.$fileName; 
			$z = new PHPZip();
			$z -> Zip($fileArr, __DIR_BACKUPS__.$fileNameArr[0].".zip");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-type: application/zip');
			header('Content-Disposition: attachment; filename="'.$fileName.'.zip"');
			readfile(__DIR_BACKUPS__.$fileNameArr[0].'.zip');			
			@unlink(__DIR_BACKUPS__.$fileNameArr[0].".zip");	
			exit;
		} else {
			echo $_SESSION['SESS_MSG'] = msgSuccessFail("fail","Sorry no db file exists!!!");
		}	
	}
	
	
function clean() {						
		
	
					$genTable .="<div id='cleandata' style='margin-top:100px; border:2px;' align='center'></div><div  style='margin-top:100px; border:2px;' align='center'>
    				<input type='button' name='submit' class='main-body-sub-submit' style='cursor:pointer;' value='Clean' onclick='cleandata();'/>
    	 			</div>";
					
					return $genTable;
	}
	
	
	function restore(){
		$sql = "SELECT * FROM ".TBL_BACKUP;
		$rst = $this->executeQry($sql);
		$num = $this->getTotalRow($rst);
		if($num>0){
		  $i = 0;
			while($line = $this->getResultObject($rst)) {
			if($i=='0'){
			$checked = 'checked';
			$genTable .= '<input type="hidden" value="'.$line->id.'" id="databasevalue" name="databasevalue" />';
			}
			else
			{
			$checked = '';
			}			
			$genTable .= '<table cellpadding="0" cellspacing="0" width="400" border="0" align="center"><tr style="height:10px;">
	        <td style="padding-top:10px;">'.$line->addDate.'</td> 
	       <td><input type="radio" onclick="databaseval(this.value);" name="databasename" '.$checked.' value="'.$line->id.'"/></td>
	       </tr></table>';	
		   $i++;		
			}
		}						
		return $genTable;
	
	}
function TruncateDatabase()
{
		$this->TrunCate(TBL_CATEGORY); 
		
		$this->TrunCate(TBL_CATEGORY_DESCRIPTION); 
		
		$this->TrunCate(TBL_SESSIONDETAIL); 
		
		$this->TrunCate(TBL_LOGDETAIL); 
		
		$this->TrunCate(TBL_FONTCATEGORY); 
		
		$this->TrunCate(TBL_FONTCATEGORY_DESCRIPTION); 
		
		$this->TrunCate(TBL_DESIGNCATEGORY); 
		
		$this->TrunCate(TBL_DESIGNCATEGORY_DESCRIPTION); 
		
		$this->TrunCate(TBL_NEWSLETTER);
		
		$this->TrunCate(TBL_NEWSLETTERTBL_USERTYPE);
		
		$this->TrunCate(TBL_NEWSLETTERSUBSCRIBER);
		
		$this->TrunCate(TBL_NEWSLETTERTEMPLATE);  
		
		$this->TrunCate(TBL_BANNER); 
		
		$this->TrunCate(TBL_BANNER_DESCRIPTION); 
		
		$this->TrunCate(TBL_BANNER_HISTORY); 
		
		$this->TrunCate(TBL_USER);
		
		$this->TrunCate(TBL_GIFTCERTIFICATE);
		
		$this->TrunCate(TBL_GIFTCERTIFICATEDESC);
		
		$this->TrunCate(TBL_PRODUCT);
		
		$this->TrunCate(TBL_PRODUCT_DESCRIPTION);
		
		$this->TrunCate(TBL_COUPON); 
		
		$this->TrunCate(TBL_COUPON_DESCRIPTION);
		
		$this->TrunCate(TBL_COUPON_TO_PRODUCT); 
		
		$this->TrunCate(TBL_COUPON_TO_CUSTOMER);
		
		$this->TrunCate(TBL_CONTACTUSTYPE);
		
		$this->TrunCate(TBL_CONTACTUS);
		
		$this->TrunCate(TBL_ZONE);
		
		$this->TrunCate(TBL_ZONE_DESCRIPTION);
		
		$this->TrunCate(TBL_COUNTRY);
		
		$this->TrunCate(TBL_COUNTRY_DESCRIPTION);
		
		$this->TrunCate(TBL_STATE);
		
		$this->TrunCate(TBL_STATE_DESCRIPTION);
		
		$this->TrunCate(TBL_COUNTY);
		
		$this->TrunCate(TBL_COUNTY_DESCRIPTION);
		
		$this->TrunCate(TBL_USERTITLE);
		
		$this->TrunCate(TBL_ATTRIBUTE);
		
		$this->TrunCate(TBL_ATTRIBUTE_DESCRIPTION);
		
		$this->TrunCate(TBL_ATTRIBUTE_VALUES);
		
		$this->TrunCate(TBL_ATTRIBUTE_VALUES_DESCRIPTION);
		
		$this->TrunCate(TBL_TESTIMONIALFIELDS);
		
		$this->TrunCate(TBL_TESTIMONIALFIELDS_DESCRIPTION);
		
		$this->TrunCate(TBL_TESTIMONIALS_DESC);
		
		$this->TrunCate(TBL_TESTIMONIALS);
		
		$this->TrunCate(TBL_EMAILSCHEDULE);
		
		$this->TrunCate(TBL_FORUMTOPIC);
		
		$this->TrunCate(TBL_FORUMTOPICDESC);
		
		$this->TrunCate(TBL_FORUMTHREAD);
		
		$this->TrunCate(TBL_FORUMTHREADDESC);
		
		$this->TrunCate(TBL_FORUMPOST);
		
		$this->TrunCate(TBL_FORUMPOSTDESC);
		
		$this->TrunCate(TBL_COUNTYDESC);
		
		$this->TrunCate(TBL_ORDER);
		
		$this->TrunCate(TBL_ORDERDETAIL);
		
		$this->TrunCate(TBL_ORDERDETAIL_ATTRIBUTES);
		
		$this->TrunCate(TBL_COLOR);
		
		$this->TrunCate(TBL_COLORDESC);
		
		$this->TrunCate(TBL_FONT);
		
		$this->TrunCate(TBL_FONTDESC);
		
		$this->TrunCate(TBL_VIEW);
		
		$this->TrunCate(TBL_VIEWDESC);
		
		$this->TrunCate(TBL_MAINPRODUCT);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_DESCRIPTION);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_ATTRIBUTE);
		
		$this->TrunCate(TBL_PRODUCT_VIEW);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_VIEW);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_COLOR);
		
		$this->TrunCate(TBL_PRODUCT_COLOR_DESCRIPTION);
		
		$this->TrunCate(TBL_PRODUCT_COLOR);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_COLOR_DESCRIPTION);
		
		$this->TrunCate(TBL_SIZE);
		
		$this->TrunCate(TBL_SIZEDESC);
		
		$this->TrunCate(TBL_SIZEPRICE);
		
		$this->TrunCate(TBL_SIZEATTRIBUTE);
		
		$this->TrunCate(TBL_MESURMENT);
		
		$this->TrunCate(TBL_MAINSIZEPRICE);
		
		$this->TrunCate(TBL_MAINSIZEATTRIBUTE);
		
		$this->TrunCate(TBL_MAINMESURMENT);
		
		$this->TrunCate(TBL_DESIGNDETAIL);
		
		$this->TrunCate(TBL_DESIGNDETAILDESC);
		
		$this->TrunCate(TBL_PRODUCT_ATTRIBUTE);
		
		$this->TrunCate(TBL_SIZEATTRIBUTEVALUE);
		
		$this->TrunCate(TBL_DESIGN);
		
		$this->TrunCate(TBL_DESIGNDETAIL);
		
		$this->TrunCate(TBL_USERIMAGE);
		
		$this->TrunCate(TBL_ORDERSTATUS);
		
		$this->TrunCate(TBL_ORDERSTATUSDESC);
		
		$this->TrunCate(TBL_DESIGNCOUNT);
		
		$this->TrunCate(TBL_SAVEASSIGNHOTDESIGN);
		
		$this->TrunCate(TBL_UPLOADPDF);
		
		$this->TrunCate(TBL_PAYMENTSTRUCTURE);
		
		$this->TrunCate(TBL_PAYMENTSTRUCTUREDESC);
		
		$this->TrunCate(TBL_SIZEGROUP);
		
		$this->TrunCate(TBL_SIZEGROUPDESC);
		
		$this->TrunCate(TBL_ADMIN_DESIGNPRODUCT);
		
		$this->TrunCate(TBL_ADMIN_PRODUCT_VIEW);
		
		$this->TrunCate(TBL_HOTDESIGN_DETAIL);
		
		$this->TrunCate(TBL_BASKET);
		
		$this->TrunCate(TBL_DISCOUNT);
		
		$this->TrunCate(TBL_MAIN_PRODUCT_QUANTITY);
		
		$this->TrunCate(TBL_SIZE_IMAGE);
		
		$this->TrunCate(TBL_PRODUCT_DETAIL_IMAGE);
		
		$this->TrunCate(TBL_COMMITLOG);
}	
	
}// End Class
?>	
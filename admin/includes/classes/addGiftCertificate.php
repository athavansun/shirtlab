<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageGift.php","");
/*---Basic for Each Page Ends----*/
$giftObj = new GiftCertificate();
$genObj = new GeneralFunctions();
$defCurrency = '$';//$_SESSION['DEFAULTCURRENCYSIGN'];

$defGiftTemplate = (object) $defGiftTemplate;
$defGiftTemplate = $giftObj->getDefaultGiftTemplate();

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$rst = $giftObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND isDefault = '1' ","","");		
	$num = $giftObj->getTotalRow($rst);	
	$error = 0;
	if($num){
		$langIdArr = array();		
		while($line = $giftObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
			
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('giftName_'.$value,$_POST['giftName_'.$value], 'req', "Please Enter Gift Certificate Name");			
		}
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('description_'.$value,$_POST['description_'.$value], 'req', "Please Enter Description.");			
		}
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('termsAndCond_'.$value,$_POST['termsAndCond_'.$value], 'req', "Please Enter Term & Conditions.");			
		}
		$obj->fnAdd("default",$_POST["default"], "req", "Please Check Default language.");
		$obj->fnAdd("cost_0",$_POST["cost_0"], "req", "Please Enter Cost.");
		$obj->fnAdd("cost_0",$_POST["cost_0"], "float", "Please Enter Cost.");
		$obj->fnAdd("value_0",$_POST["value_0"], "req", "Please Enter Value.");
		$obj->fnAdd("value_0",$_POST["value_0"], "float", "Please Enter Value.");
		
		$obj->fnAdd("expire",$_POST["expire"], "req", "Please Enter Expire Month.");
		$obj->fnAdd("expire",$_POST["expire"], "num", "Please Enter Expire Month.");

		$obj->fnAdd("status",$_POST["status"], "req", "Please Select Status.");
		
		if($_POST["templateFontColor"] == ""){
			$error = 1;
		}

		if($_POST["templateBgImage"] == ""){
			$error = 1;
		}

		$arr_error = $obj->fnValidate();
		
		$str_validate = (count($arr_error)) ? 0 : 1; 
	
		foreach($langIdArr as $key=>$value) {
			$arr_error['giftName_'.$value]=$obj->fnGetErr($arr_error['giftName_'.$value]);
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['description_'.$value]=$obj->fnGetErr($arr_error['description_'.$value]);
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['termsAndCond_'.$value]=$obj->fnGetErr($arr_error['termsAndCond_'.$value]);
		}
		$arr_error['default']=$obj->fnGetErr($arr_error['default']);
		$arr_error['cost_0']=$obj->fnGetErr($arr_error['cost_0']);
		$arr_error['value_0']=$obj->fnGetErr($arr_error['value_0']);
		$arr_error['status']=$obj->fnGetErr($arr_error['status']);
		$arr_error['expire']=$obj->fnGetErr($arr_error['expire']);
	}
	
	if(($error == 0) && $str_validate){
		$_POST = postwithoutspace($_POST);
		$giftObj->addGiftCertificate($_POST);
	}
		
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!--<script src="js/file/custom-form-elements.js"></script>-->
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<!--Light Box Starts-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--Light Box Ends -->
<script language="javascript" language="javascript">
var dogift=1; // For onChange event in color picker
</script>
<!--				Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)		-->

<script type="text/javascript">
	function addNewRow(){
		var addm=$("#addMore").val();		
		var id="row_"+$("#addMore").val();
		var htmls='<tr id="'+id+'"><td width="20%"><input type="radio" name="default" id="default" value = "'+addm+'" ></td><td><b><?=$defCurrency?></b>&nbsp;<input type="text" name="cost_'+addm+'" id="__cost_'+addm+'" style="width:50px; height:10px;" ></td><td><b><?=$defCurrency?></b>&nbsp;<input type="text" name="value_'+addm+'" id="__value_'+addm+'" style="width:50px; height:10px;" ></td><td width="15%"><a href="javascript:void();" onClick="$(\'#'+id+'\').remove();"><img src="images/close_small.png"></a></td></tr>';
		$("#addMore").val(parseInt(addm)+parseInt(1));
		$("#denomination").append(htmls) ;
	}
	
	function changefont(code){
		$("#fontColor").css("background-color",code);
		$(".font_color").css("color",code);
	}
	
	function changeFontTexts(fontName){
		$(".font_color").css("font-family",fontName);
	}
	
	var TEMPLATE_IMG_LARGE = '<?=SITE_URL.__GIFTLARGEIMAGE__?>';
	
	function hrefBack1(){
		window.location='manageGift.php';
	}
	
	function changeTemplate(templateImage){
		$("#templateBg").attr("src", TEMPLATE_IMG_LARGE+templateImage);
		$("#templateBgImage").val(templateImage);
	}
</script>
<style type="text/css">
*{
 padding:0px;
 margin:0px;
}
li{ list-style:none;}
.template{ margin:0 auto; width:510px; position:relative; }
.template_inner{ position:absolute; width:100%; left:0; top:0px; padding-bottom:22px;}
.template_inner h2{ padding:10px; margin-top:10px; font-weight:normal; font-size:33px; line-height:60px;}
.template_inner h2 span{ font-weight:normal; font-size:33px; line-height:60px; width:90px; margin-right:59px; background:url(images/prise_bg.png) left top no-repeat; padding:15px 0; text-align:center; display:inline-block; }
.template_inner div.left_box{ float:left; width:150px; margin-top:5px; height:130px;}
.template_inner div.left_box ul{ overflow:hidden; float:right; padding-right:5px; margin-top:9px;}
.template_inner div.left_box ul li{ list-style:none; text-align:right; font-weight:normal; font-size:13px; line-height:16px; padding-bottom:5px;}
.template_inner div.right_box{ float:left; width:285px;  margin-top:5px; height:130px;}
.template_inner div.right_box div.r_top{ background:url(images/t-top.png) left top no-repeat; display:inline-block; padding-top:9px;}
.template_inner div.right_box div.r_mid{ background:url(images/t-mid.png) left top repeat-y; overflow:hidden; padding-left:10px;}
.template_inner div.right_box div.r_bot{ background:url(images/t-bot.png) left bottom no-repeat; display:inline-block; padding-bottom:9px;}
.template_inner div.right_box div.r_mid ul{ overflow:hidden; width:259px; min-height:112px;}
.template_inner div.right_box div.r_mid ul li{ list-style:none; text-align:left; font-weight:normal; font-size:13px; line-height:16px;  padding-bottom:5px;}
.template_inner h3{ width:100%; text-align:center; margin-top:33px; font-weight:normal; font-size:13px; line-height:18px; text-decoration:none; display:inline-block;}
.font_color{color:<?=$_POST['templateFontColor'] ? $_POST['templateFontColor'] :$defGiftTemplate->font_color ?>; font-family:<?=$_POST['templateFont'] ? $_POST['templateFont'] : 'Verdana, Arial, Helvetica, sans-serif;' ?>}
</style>
</head>
<html>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Miscellaneous</h1>
        <form name="giftCertificate" id="giftCertificate" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<input type="hidden" name="addMore" id="addMore" value="1">
		 <fieldset>  
            <label>Add Gift Voucher</label>
			<?=$_SESSION['SESS_MSG']?>
			<p id="tempDelMsg"></p>
            <section>
			<label><b>Large Preview</b></label>
			<?php if($defGiftTemplate) {?>
			<div>
			<div class="template">
			<img id="templateBg" src="<?="../".__GIFTLARGEIMAGE__."/".($_POST['templateBgImage'] ? $_POST['templateBgImage'] :$defGiftTemplate->bg_image)?>" />
				<div class="template_inner">
				<h2 class="font_color"><span class="font_color">25</span></h2>
					<div class="left_box">
						<ul>
						<li class="font_color">To :</li>
						<li class="font_color">From :</li>
						<li class="font_color">Message :</li>
						</ul>
					</div>
				<div class="right_box">
					<div class="r_bot">
						<div class="r_top">
							<div class="r_mid">
								<ul>
								<li class="font_color">Customer</li>
								<li class="font_color">Shirtlab</li>
								<li class="font_color">Test message</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<h3 class="font_color">http://www.shirtlabweb.com</h3>
				</div>
			</div>
			<!-- Change font text and color starts -->
			
			<div  class="change_data">
			<input type="hidden" name="templateFontColor" id="templateFontColor" value="<?=$_POST['templateFontColor']?$_POST['templateFontColor']:$defGiftTemplate->font_color ?>">
			<input type="hidden" name="templateBgImage" id="templateBgImage" value="<?=$_POST['templateBgImage']?$_POST['templateBgImage']:$defGiftTemplate->bg_image ?>">			
			</div>
			
			
			<!-- Change font text and color ends -->
			</div>
			<? }else{ ?> 
			
			<div><span class="alert-red alert-icon">Please upload gift template first</span></div>
			<input type="hidden" name="templateFontColor" id="templateFontColor" value="">
			<input type="hidden" name="templateBgImage" id="templateBgImage" value="">
			
			<? } ?>
			</section>
            <section>
			<label><b>Template</b></label>
			<div>				
				<a class="btn small fr" href="uploadTemplate.php" rel="shadowbox;width=705;height=325">Upload Template</a>
			<?=$giftObj->getTemplateThumb(); ?>
			</div>
			</section>
            <section>
			<label><b>Gift Voucher Name</b></label><div>
			<?=$genObj->getTextBoxFrmSessionEngValidated('giftName','m__giftName',$arr_error); ?>
			</div>
			</section>
			<section>
			<label><b>Denomination</b></label>
			<div>
				<table>
					<tr>
						<thead>
							<th>Default</th>
							<th>Cost</th>
							<th>Value</th>
							<th width="15%">
								<input type="button" name="addmore" value="Add More" onclick="return addNewRow();" />
							</th>
						</thead>
					</tr>
					<tr>
						<td><input type="radio" name="default" id="default" value="0" checked="checked"></td>
						<td><b><?=$defCurrency?></b>&nbsp;<input type="text" name="cost_0" id="__cost_0" value="<?=$_POST['cost_0'] ?>"  style="width:50px; height:10px;" ></td>
						<td><b><?=$defCurrency?></b>&nbsp;<input type="text" name="value_0" id="__value_0" value="<?=$_POST['value_0'] ?>" style="width:50px; height:10px;" ></td>						
					</tr>
				</table>
				<table id="denomination" style="padding-left:40px;">
				</table>
				<?=$arr_error['default_0'].$arr_error['cost_0'].$arr_error['value_0']?>
			</div>
            </section>
			<section>
			<label><b>Details</b></label>
			<div>
				<label><b>Expires</b></label><input type="text" style="width:50px; height:10px;" name="expire" id="__expires" value="<?=$_POST['expire'] ?>" > Month after purchase  
				<?=$arr_error['expire']?><hr>
				
				<label><b>Description</b></label><br><br>
				<?=$genObj->getLangTextareaFrmSessionEngValidated('description','m__description',$arr_error);?>
				<hr>
				
				<label><b>Terms & Conditions</b></label><br><br>
				<?=$genObj->getLangTextareaFrmSessionEngValidated('termsAndCond','m__termsAndCond',$arr_error);?>
				<hr>
				
				<label><b>Status</b></label>
				<select name="status" id="__status" >
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				</select>
				<?=$arr_error['status']?>
			</div>
            </section>
		</fieldset>
             <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
              </div>
                </section>             
        </fieldset>
        </form>
	</section>
	</body>
    </html>
	<? unset($_SESSION['SESS_MSG']); ?>

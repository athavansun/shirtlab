<?php 
session_start();
class Currency extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {
		$cond = "1 and ".TBL_CURRENCY.".currencyDetailId = ".TBL_CURRENCY_DETAIL.".id";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (".TBL_CURRENCY_DETAIL.".currencyName LIKE '%$searchtxt%' or ".TBL_CURRENCY_DETAIL.".currencyCode LIKE '%$searchtxt%')";
		}
		$query = "select ".TBL_CURRENCY.".*,".TBL_CURRENCY_DETAIL.".currencyName,".TBL_CURRENCY_DETAIL.".sign,".TBL_CURRENCY_DETAIL.".currencyCode from ".TBL_CURRENCY." , ".TBL_CURRENCY_DETAIL." where $cond ";
				
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"currencyName";
			$order = $_GET[order]? $_GET[order]:"asc";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;
				while($line = $this->getResultObject($rst)) {									
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					if ($line->isDefault==1){
						$isDefault = "checked='checked'";
						$onclickstatus = '';
						$chkbox = '';
					}else{
						$isDefault = "";
						$onclickstatus = ' onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'currency\')"';
						$chkbox = '<input name="chk[]" value="'.$line->currencyDetailId.'" type="checkbox" class="checkbox">';
					}
					
					$currencyValue = ($line->showIn==0)?$line->sign." ":"";
					$currencyValue .= $line->currencyValue;
					$currencyValue .= ($line->showIn==1)?" ".$line->sign:"";										
					$genTable .= '<div class="'.$highlight.'">
						 <tr>
							<td style="width:50px;">&nbsp;&nbsp;'.$chkbox.'</td>
							<td style="width:70px;">'.$i.'</td>
							<td style="width:125px;">'.$line->currencyName.'</td>
							<td style="width:125px;">'.html_entity_decode($line->sign).'</td>
							<td style="width:90px;"><input type="checkbox" '.$isDefault.' disabled=disabled class="welcheckbox"></td>
							<td style="width:100px;">';
			
					if($menuObj->checkEditPermission()) {		                                                
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" '.$onclickstatus.' >'.$status.'</div>';
					}				
																											
					$genTable .= '</td><td style="width:90px;"><a rel="shadowbox;width=705;height=325" title="'.$line->currencyName.'" href="viewCurrency.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td><td style="width:90px;">';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editCurrency.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
					}	
					$genTable .= '</td><td style="width:55px;">';		
					
			
					if($menuObj->checkDeletePermission() && (!$line->isDefault)) {					
							$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=currency&type=delete&id=".$line->currencyDetailId."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</td></tr></div>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_CURRENCY,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "update ".TBL_CURRENCY." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}
		echo $status;		
	}
	

	
	function deleteValue($get) {
			$sql = "delete from ".TBL_CURRENCY."  where currencyDetailId = '$get[id]'";			
			$rst = $this->executeQry($sql);
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}
			
			$sql = "delete from ".TBL_CURRENCY_DETAIL."  where id = '$get[id]'";			
			$rst = $this->executeQry($sql);
                        
                        $sql = "delete from ".TBL_VALUE_POINTS." where cur_id = '".$get[id]."'";
                        $rst = $this->executeQry($sql);
                        
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}			
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script currency=javascript>window.location.href='manageCurrency.php?page=$post[page]&limit=$post[limit]';</script>";
	}
	
	function addRecord($post) {		
		$sqlCur   = "INSERT INTO ".TBL_CURRENCY_DETAIL." SET currencyName='".$post['currencyName']."' ,currencyCode='".$post['currencyCode']."' , sign='".$post['currencySign']."'";
		$resCur   = $this->executeQry($sqlCur);
		$curInsId = mysql_insert_id();
			
		$date = date("Y-m-d h:i:s");		
		$sql = "INSERT INTO ".TBL_CURRENCY." SET `currencyDetailId`='".$curInsId."', `showIn`='$post[showIn]', decimalPlace = '$post[decimalPlace]', `status`='1', `addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."', currencyCode = '$post[currencyCode]', currencyCodeT = '$post[currencyCode]' , SeparatorFrom='".$post[currencySeprator]."'";		
                $rst = $this->executeQry($sql);
		$inserted_id = mysql_insert_id();
                
                $sql = "INSERT INTO ". TBL_VALUE_POINTS." SET `cur_id`='".$curInsId."'";
                $rst = $this->executeQry($sql);
                
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		if($post[isDefault]) {
			$query = "update ".TBL_CURRENCY." set isDefault = '0' where 1";
			if($this->executeQry($query)){
				$this->logSuccessFail("1",$sql);
			} else {
				$this->logSuccessFail("0",$sql);
			}
			
			$query = "update ".TBL_CURRENCY." SET `isDefault`='".$post['isDefault']."' where id = '".$inserted_id."'";
			$rst = $this->executeQry($query);			
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}
		}
		
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
		header("Location:addCurrency.php");
		exit;			
	}
	
	function editRecord($post) {        
		$date = date("Y-m-d h:i:s");	
		
		$curDetId = $this->fetchValue(TBL_CURRENCY,"currencyDetailId","1 and currencyDetailId = '$post[id]'");
		
		$sqlCur   = "UPDATE ".TBL_CURRENCY_DETAIL." SET currencyName='".$post['currencyName']."' , currencyCode='".$post['currencyCode']."', sign='".$post['currencySign']."' WHERE id =".$curDetId."";
		$resCur   = $this->executeQry($sqlCur);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Record updated successfully.");
			
		$date = date("Y-m-d h:i:s");		
		$sql = "UPDATE ".TBL_CURRENCY." SET showIn ='".$post[showIn]."', decimalPlace = '$post[decimalPlace]', `status`='1', `addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."', currencyCode = '$post[currencyCode]', currencyCodeT = '$post[currencyCode]' , SeparatorFrom='".$post[currencySeprator]."' WHERE  currencyDetailId = '$post[id]'";
		$rst = $this->executeQry($sql);		
		if($rst){
			$this->logSuccessFail("1",$sql);
			$_SESSION['SESS_MSG'] = msgSuccessFail("success","Record updated successfully.");
		}else{
			$this->logSuccessFail("0",$sql);
			$_SESSION['SESS_MSG'] = msgSuccessFail("fail","Fail to updat record.");
		}
		
		if($post[isDefault]) {
            $query = "update ".TBL_CURRENCY." set isDefault = '0' where 1";
			if($this->executeQry($query)){
				$this->logSuccessFail("1",$sql);
			} else {
				$this->logSuccessFail("0",$sql);
			}
			
			$query = "update ".TBL_CURRENCY." SET `isDefault`='".$post['isDefault']."' where currencyDetailId = '".$post[id]."'";
			$rst = $this->executeQry($query);			
			if($rst){
				$this->logSuccessFail("1",$sql);
			}else{
				$this->logSuccessFail("0",$sql);
			}
		}
		echo "<script currency=javascript>window.location.href='manageCurrency.php?page=$post[page]&limit=$post[limit]';</script>";
		exit;
	}

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script currency=javascript>window.location.href='manageCurrency.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}		
	
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				$sql = "delete from ".TBL_CURRENCY." where currencyDetailId = '$val'";
				$rst = $this->executeQry($sql);
                                
                                $sql = "delete from ".TBL_CURRENCY_DETAIL." where id = '$val'";
                                $rst = $this->executeQry($sql);
                                
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_CURRENCY." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where currencyDetailId ='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_CURRENCY." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where currencyDetailId='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script currency=javascript>window.location.href='manageCurrency.php?page=$post[page]';</script>";
	}	
	
	function getResult($id) {
		$sql_lang = $this->executeQry("SELECT TC.* , TCD.* FROM ".TBL_CURRENCY." AS TC, ".TBL_CURRENCY_DETAIL." AS TCD WHERE TC.id = '$id' AND TCD.id=TC.currencyDetailId");
		$num_lang = $this->getTotalRow($sql_lang);
		if($num_lang > 0) {
			return $line_lang = $this->getResultObject($sql_lang);	
		} else {
			redirect('manageCurrency.php');exit;
		}	
	}
	
}// End Class
?>	

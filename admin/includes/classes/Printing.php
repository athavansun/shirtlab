<?php

session_start();

class Printing extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function editPrintingDiscount($post) {
        $_SESSION['SESS_MSG'] = "";

        $sql = $this->executeQry("select * from " . TBL_PRINTING_COLOR . " where 1 ");
        $num = $this->getTotalRow($sql);
        while ($row = $this->getResultObject($sql)) {
            $query = "update " . TBL_PRINTING_COLOR . " set valuePoint = '" . $post['valuePoint_' . $row->id] . "' WHERE id = '" . $row->id . "'";
            if ($this->executeQry($query)) {
                $this->logSuccessFail('1', $query);
            } else {
                $this->logSuccessFail('0', $query);
            }
        }

        $sql = $this->executeQry("select * from " . TBL_EMBROIDERY_RATE . " where 1 and type = '1'");
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            $line = $this->getResultObject($sql);
            $query = "update " . TBL_EMBROIDERY_RATE . " set min_cm = '" . $post[cm2] . "' where id = '" . $line->id . "'";
            if ($this->executeQry($query)) {
                $this->logSuccessFail('1', $query);
            } else {
                $this->logSuccessFail('0', $query);
            }

            $sql = $this->executeQry("select * from " . TBL_EMBROIDERY_DISCOUNT . " where 1 and type = '1' ");
            $num = $this->getTotalRow($sql);
            while ($row = $this->getResultObject($sql)) {
                $query = "update " . TBL_EMBROIDERY_DISCOUNT . " set charge = '" . $post['charge_' . $row->id] . "' WHERE id = '" . $row->id . "'";
                $res = $this->executeQry($query);
                if ($res) {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
                } else {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
                }
            }
        }
    }

    function addPrintingDiscount($post) {
        $query = "INSERT INTO " . TBL_EMBROIDERY_DISCOUNT . " SET pcs='" . $post['pcs'] . "', sign='" . $post['sign'] . "', charge = '" . $post[charges] . "', type = '1'";

        $insertId = $this->executeQry($query);
        if ($insertId) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been added successfully.");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "There is some problem to add record.");
        }
        header("location:managePrintingDiscount.php");
        exit;
    }

    function addColorQuantity($post) {
        $query = "INSERT INTO " . TBL_PRINTING_COLOR . " SET color='" . $post['color'] . "', valuePoint = '" . $post[charges] . "'";

        $insertId = $this->executeQry($query);
        if ($insertId) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been added successfully.");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "There is some problem to add record.");
        }
        header("location:managePrintingDiscount.php");
        exit;
    }

    function deleteColorValue($get) {
        $sql = " DELETE FROM  " . TBL_PRINTING_COLOR . " WHERE id = '$get[id]' ";
        $rst = $this->executeQry($sql);
        if ($rst) {
            $this->logSuccessFail('1', $query);
        } else {
            $this->logSuccessFail('0', $query);
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='managePrintingDiscount.php?page=$get[page]&limit=$get[limit]';</script>";
    }

    function deletePrintingDiscount($get) {
        $sql = " DELETE FROM  " . TBL_EMBROIDERY_DISCOUNT . " WHERE id = '$get[id]' ";
        $rst = $this->executeQry($sql);
        if ($rst) {
            $this->logSuccessFail('1', $query);
        } else {
            $this->logSuccessFail('0', $query);
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='managePrintingDiscount.php?page=$get[page]&limit=$get[limit]';</script>";
    }

}


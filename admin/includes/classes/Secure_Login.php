<?php 
session_start();
class Secure_Login extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function adminLogin($post,$pagename) {
	
		$username = mysql_real_escape_string($post[userName]);
		$password = mysql_real_escape_string($post[userPassword]); 
		$sql = $this->executeQry("select * from ".TBL_ADMINLOGIN." where username = '$username' and adminLevelId='-2' ");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
			if($this->validate_password($password,$line->password)) {
				$defaultLangCode = $this->fetchValue(TBL_LANGUAGE,"id","1 and isDefault='1'");
				$_SESSION['SECURE_ADMIN_ID'] = $line->id;
				$_SESSION['SECURE_ADMINNNAME'] = $line->username;
				$_SESSION['SECURE_ADMINTBL_USERHASH'] = $line->hash;
				$_SESSION['SECURE_USERLEVELID'] = $line->adminLevelId;
				$_SESSION['SECURE_DEFAULTLANGUAGE'] = $defaultLangCode;
				$_SESSION['SECURE_PHPSESSIONID'] = session_id();
				$_SESSION['SECURE_ADMIN_LAST_LOGIN'] = date("d M Y h:i a",strtotime($line->lastLogin));
				$this->executeQry("update ".TBL_ADMINLOGIN." set lastLogin = '".date('Y-m-d H:i:s')."' where id = '".$line->id."'");
				$this->executeQry("insert into ".TBL_SESSIONDETAIL." set sessionId = '".$_SESSION['SECURE_PHPSESSIONID']."', adminId = '".$_SESSION['SECURE_ADMIN_ID']."', ipAddress = '".$_SERVER['REMOTE_ADDR']."', signInDateTime = '".date('Y-m-d H:i:s')."', signDate = '".date('Y-m-d')."'");
				
				$_SESSION['SECURE_SESSIONID'] = mysql_insert_id();
				
				
				echo "<script>window.location.href='$pagename'</script>";			
			} else {						
				$_SESSION['SESS_MSG'] = "Login Authentication Failed";
			}	
		} else {
			$_SESSION['SESS_MSG'] = "Login Authentication Failed";
		}
	}
	
	
	
	function validate_password($plain, $encrypted) {
		if (!is_null($plain) && !is_null($encrypted)) {
			// split apart the hash / salt
			$stack = explode(':', $encrypted);
	
			if (sizeof($stack) != 2) 
				return false;
			if (md5($stack[1] . $plain) == $stack[0]) {
				return true;
			}
		}
		return false;
	}
	
	function checkSession($pageName) {
	
	    if ( !$_SESSION['SECURE_ADMIN_ID'] ) {
			$_SESSION['SESS_MSG'] = 'OOPS! your session has been expired!';
			redirect('securelogin.php?pagename='.$pageName.'');
			exit;
		} else {
			$adminUserHash = $this->fetchValue(TBL_ADMINLOGIN,"hash","1 and id = '".$_SESSION['SECURE_ADMIN_ID']."'");
			if($adminUserHash != $_SESSION['SECURE_ADMINTBL_USERHASH']) {
				$_SESSION['SESS_MSG'] = 'OOPS! your session has been expired!';
				redirect('securelogin.php?pagename='.$pageName.'');
				exit;
			}
 		}
	}
	
	function Logout() {
		$this->executeQry("update ".TBL_SESSIONDETAIL." set signOutDateTime = '".date('Y-m-d H:i:s')."' where sessionId = '".$_SESSION['PHPSESSIONID']."' and adminId = '".$_SESSION['ADMIN_ID']."'");
		unset($_SESSION['SECURE_ADMIN_ID']);
		unset($_SESSION['SECURE_ADMINNNAME']);
		unset($_SESSION['SECURE_ADMINTBL_USERHASH']);
		unset($_SESSION['SECURE_USERLEVELID']);
		unset($_SESSION['SECURE_DEFAULTLANGUAGE']);
		unset($_SESSION['SECURE_PHPSESSIONID']);
		unset($_SESSION['SECURE_SESSIONID']);
		//session_destroy();		
	}
	
	function isExistsAdminEmailId($emailId){
		$sql = $this->executeQry("select * from ".TBL_ADMINLOGIN." where 1 and emailId = '$emailId'");
		$num = $this->getTotalRow($sql);
        if($num > 0)
		 return true;
		else
		 return false; 	
	}
	
	
	function adminResendPassword($post){
		$mailFunctionObj = new MailFunction;
		$generalDataObj = new GeneralFunctions();
		
		$sql = $this->executeQry("select * from ".TBL_ADMINLOGIN." where 1 and emailId = '$post[email]'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			$line = $this->getResultObject($sql);
	        $uid = $line->id;
			$possible = '012dfds3456789bcdfghjkmnpq454rstvwx54yzABCDEFG5HIJ5L45MNOP352QRSTU5VW5Y5Z';
     		$newPassword = substr($possible, mt_rand(0, strlen($possible)-10), 6);
			//$newPassword = "123456";
			$Password = $this->encrypt_password($newPassword); 
			$updateqry = "UPDATE ".TBL_ADMINLOGIN." SET `password` = '$Password' WHERE `id` ='$uid'";
		    $this->executeQry($updateqry);
			$_SESSION['SESS_MSG'] = "Your Password has been send successfully to your email Id.";
			//$mailFunctionObj->mailValue("5","1",$uid,$newPassword);
			$to = $line->emailId;
			$subject = "Forget Password";
			$from = $line->emailId;
			$message = '<table cellspacing="0" cellpadding="0" align="center" width="100%">
    <tbody>
        <tr>
            <td valign="top" height="81" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td height="10">&nbsp;</td>
        </tr>
        <tr>
            <td height="30" style="font-family: tahoma; text-decoration: none; font-weight: bold; font-size: 11px; color: rgb(55, 64, 73);" colspan="3">&nbsp;Hello '.$line->username.',</td>
        </tr>
        <tr>
            <td height="30" width="76%" style="font-family: Tahoma; font-size: 12px; font-weight: bold; text-decoration: none; color: rgb(83, 91, 97);" colspan="3">&nbsp;Your Password is given bellow.</td>
        </tr>
        <tr>
            <td height="30" style="font-family: Tahoma; font-size: 12px; font-weight: normal; text-decoration: none; color: rgb(83, 91, 97);" colspan="4">&nbsp;'.$newPassword.'</td>
        </tr>

        <tr>
        </tr>
        <tr>
        </tr>
        <tr>
            <td height="30" colspan="4">&nbsp;</td>
        </tr>
    </tbody>
</table>';

			@mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());
			//header("Location:lost-password.php");
		    echo "<script language=javascript>window.location.href='lost-password.php';< /script>";
		}	
	}
	
	function encrypt_password($plain) {
		$password = '';
		for ($i=0; $i<10; $i++) {
			$password .= $this->tep_rand();
		}
		$salt = substr(md5($password), 0, 2);
		$password = md5($salt . $plain) . ':' . $salt;
		return $password;
	}
	
	function tep_rand($min = null, $max = null) {
	    static $seeded;
    	if (!$seeded) {
	      mt_srand((double)microtime()*1000000);
    	  $seeded = true;
    	}
	}			
}// End Class
?>	
<?php

session_start();

class SiteAccessUser extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }
/*----------------Add New Site User Function----------------*/
    function addSiteAcessUser($post) {
       
        $_SESSION['SESS_MSG'] = "";
        $this->tablename = TBL_SITEACCESS_USER;
        $this->field_values['userName'] = $post['userName'];
        $this->field_values['password'] = $post['password'];
        $this->field_values['created_at'] = time();
        $this->field_values['status'] = '1';
        $res = $this->insertQry();
        $insertId = $res[1];
        if ($insertId) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been added successfully.");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "There is some problem to add record.");
        }
        $this->getAllActiveUser();
        header("Location:addSiteAccessUser.php");
        exit;
    }
/*-----------------------End--------------------------------*/
/*--------------Check Unique Site User Name Function--------------*/    
    function isUserNameExists($userName, $id = "") {
        $userName = trim($userName);
        if ($id != "") {
            $rst = $this->selectQry(TBL_SITEACCESS_USER, "userName='$userName' and id!='$id' ", "", "");
        } else {
            $rst = $this->selectQry(TBL_SITEACCESS_USER, "userName='$userName'", "", "");
        }
        $row = $this->getTotalRow($rst);
        return $row;
    }
/*----------------End-------------------*/
/*----------------Show Site User Detail Function-----------------*/    
    function userFullInformation() {
        $menuObj = new Menu;
        $cond = " 1=1 ";
        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT) {
            $searchtxt = $_REQUEST['searchtxt'];
            $cond .= " AND (" . TBL_SITEACCESS_USER . ".`userName` LIKE '%$searchtxt%')  ";
        }
        $query = " SELECT `id`, `userName`,`password`,`status` FROM " . TBL_SITEACCESS_USER . " WHERE $cond ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $this->setLimit($_GET[limit]);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : "userName";
            $order = $_GET[order] ? $_GET[order] : "DESC";
            $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;
            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);
            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    if ($line->status == 0)
                        $status = "InActive";
                    else
                        $status = "Active";
                    $genTable .= '<tr>
								 	<th><input name="chk[]" value="' . $line->id . '" type="checkbox" class="checkbox"></th>
									<td>' . $i . '</td>
									<td>' . stripslashes($line->userName) . '</td>
                                                                        <td>' . stripslashes($line->password) . '</td>    
									<td><div id="' . $div_id . '" style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'managesiteuser\')">' . $status . '</div></td>
									<td>';
                    $genTable .= '<a  rel="shadowbox;width=705;height=490" title="' . stripslashes($line->userName) . '" href="viewSiteAccessUser.php?uid=' . base64_encode($line->id) . '"><img src="images/view.png" alt="View" width="16" height="16" border="0" /></a>';
                    $genTable .= '</td><td>';
                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<a class="i_pencil edit"  href="editSiteAccessUser.php?uid=' . base64_encode($line->id) . '&page=' . $page . '" title="Edit">Edit</a>';
                    }
                    $genTable .= '</td><td>';
                    if ($menuObj->checkDeletePermission()) {
                        $genTable .= "<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=managesiteuser&type=delete&id=" . $line->id . "&page=$page'}else{}\" >Delete</a>";
                    }
                    $genTable .= '</td></tr>';
                    $i++;
                }
                switch ($recordsPerPage) {
                    case 10:
                        $sel1 = "selected='selected'";
                        break;
                    case 20:
                        $sel2 = "selected='selected'";
                        break;
                    case 30:
                        $sel3 = "selected='selected'";
                        break;
                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='" . $totalrecords . "' $sel4>All</option>  

					   </select> Records Per Page

					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='" . $currpage . "'></td><td class='page_info' align='center' width='200'>Total " . $totalrecords . " records found</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table></div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
        }
        return $genTable;
    }
/*-------------End----------------------*/
/*----------------View Site User Function-------------------*/
    function getUserById($id) {
        $rst = $this->selectQry(TBL_SITEACCESS_USER, "id='$id'", "", "");
        if ($this->getTotalRow($rst)) {

            return $this->getResultRow($rst);
        } else {

            header("Location:manageSiteAccessUser.php");
            exit;
        }
    }
/*----------------End----------------------*/
/*----------------Edit Site User Function----------------------*/
    function editRecord($post) {
        $_SESSION['SESS_MSG'] = "";
        $this->tablename = TBL_SITEACCESS_USER;
        $this->field_values['userName'] = $post['userName'];
        if ($post['password']) {
            $this->field_values['password'] = $post['password'];
        }
        $this->condition = "id = '$post[id]'";
        $res = $this->updateQry();
        if ($res) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully.");
        }
        $this->getAllActiveUser();
        header("Location:manageSiteAccessUser.php");
        exit;
    }
/*----------------End----------------------*/
/*----------------Status Site User Function--------------------*/    
    function changeValueStatus($get) {
        $status = $this->fetchValue(TBL_SITEACCESS_USER, "status", "1 and id = '$get[id]'");
        if ($status == 1) {
            $stat = 0;
            $status = "Inactive,0";
        } else {
            $stat = 1;
            $status = "Active,1";
        }
        $query = "update " . TBL_SITEACCESS_USER . " set status = '$stat', updated_at = '" . time() . "' where id = '$get[id]'";        
        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);		
        echo $status;
        $this->getAllActiveUser();
    }
/*----------------End----------------------*/    
/*----------------Delete Site User Function---------------*/
    function deleteValue($get) {
        $sql = " DELETE FROM  " . TBL_SITEACCESS_USER . " WHERE id = '$get[id]' ";
        $rst = $this->executeQry($sql);
        if ($rst) {
            $this->logSuccessFail('1', $query);
        } else {
            $this->logSuccessFail('0', $query);
        }
        $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        $this->getAllActiveUser();
        echo "<script language=javascript>window.location.href='manageSiteAccessUser.php?page=$get[page]&limit=$get[limit]';</script>";
    }
/*----------------End----------------------*/
/*-------------Enable,Disable and Delete Site User Function--------------*/    
    function deleteAllValues($post) {
        if (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the action or records, And then submit!!!");
            echo "<script language=javascript>window.location.href='manageSiteAccessUser.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }
        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "DELETE FROM " . TBL_SITEACCESS_USER . " where id = '$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your all selected information has been deleted successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
             $this->getAllActiveUser();
        }
        if ($post[action] == 'enableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "UPDATE " . TBL_SITEACCESS_USER . " set status ='1', updated_at = '" . time() . "' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Enable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
             $this->getAllActiveUser();
        }
        if ($post[action] == 'disableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "UPDATE " . TBL_SITEACCESS_USER . " set status ='0', updated_at = '" . time() . "' where id='$val'";
                    $rst = $this->executeQry($sql);
                    if ($rst) {
                        $this->logSuccessFail("1", $sql);
                    } else {
                        $this->logSuccessFail("0", $sql);
                    }
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Disable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
             $this->getAllActiveUser();
        }
        echo "<script language=javascript>window.location.href='manageSiteAccessUser.php?page=$post[page]&limit=$post[limit]';</script>";
    }
/*----------------End----------------------*/
/*----------------Active Site User Function-----------------*/    
    function getAllActiveUser(){
        $query = " SELECT  `userName`,`password` FROM " . TBL_SITEACCESS_USER . " WHERE status='1' order by id desc";
        $sql = $this->executeQry($query);
        $myfile = fopen(ABSOLUTEPATH.".htpasswd", "w") or die("Unable to open file!");
        while( $line = mysql_fetch_object($sql))
        {	  
		    fwrite($myfile, $line->userName.":".crypt($line->password, base64_encode($line->password))."\n");
	}       
        fclose($myfile);       
    }
/*----------------End---------------------*/    
}

// End Class
?>	


<?php 
session_start();
class Newsletter extends MySqlDriver{
	function __construct() { $this->obj = new MySqlDriver; }


	function allNewsletterInformation() {
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
		$searchtxt = $_REQUEST['searchtxt'];
		$con = " AND senderEmailId LIKE '%$searchtxt%' OR newsletterSubject LIKE '%$searchtxt%' OR languageName  LIKE '%$searchtxt%' ";
		}
		$query = "select ".TBL_NEWSLETTER.".`id`,".TBL_NEWSLETTER.".`addDate`, ".TBL_NEWSLETTER.".`newsletterSubject`, ".TBL_NEWSLETTER.".`attachedFile`, ".TBL_NEWSLETTER.".`attachmentPath`, ".TBL_NEWSLETTER.".`status`,".TBL_LANGUAGE.".languageName from ".TBL_NEWSLETTER." INNER JOIN ".TBL_LANGUAGE." ON (".TBL_NEWSLETTER.".langId=".TBL_LANGUAGE.".id) where 1=1 $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "InActive";
					else
						$status = "Active";
					if($line->attachedFile){
						$attachment = "<a href='".__NEWSLETTERPATH__.$line->attachmentPath."' style='cursor:pointer;' target='new'>".$line->attachedFile."</a>";
					}else{
					$attachment = " -- ";
					}
					$genTable .= '<tr>';
					$genTable .= '<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>';
					$genTable .= '<td>'.$i.'</td>';
                                        $genTable .= '<td>'.$line->id.'</td>';
					$genTable .= '<td>'.$line->languageName.'</td>';
					$genTable .= '<td>'.substr(stripslashes($line->newsletterSubject),0,100).'</td>';
					$genTable .= '<td>'.$attachment.'</td>';
                                         $genTable.='<td>';
                                        if($menuObj->checkEditPermission()) {							
                                                $genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'newsletter\')" >'.$status.'</div>';
                                        }
					$genTable .= '</td>';
                                        $genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.$line->languageName.'" href="viewNewsletter.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';
                                         $genTable.='<td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editNewsletter.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td>';
                                        
                                        $genTable.='<td>';                                        
                                        if($menuObj->checkEditPermission()) {					
						$genTable .= '<a href="sendNewsletter.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/sendMail.png" alt="Send" width="16" height="16" border="0" /></a>';
					}
                                        $genTable.='</td>';
                                        
                                         $genTable.='<td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a  class='i_trashcan edit'  href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Newsletter?')){window.location.href='pass.php?action=newsletter&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td>';
                                        $genTable.='</tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}
	
	


	
	////// Add New Newsletter//////
	
	function addNewNewsletter($post,$file){
                //echo "<pre>";print_r($_POST);print_r($file);echo "</pre>";exit;
                $newslettermsg = mysql_real_escape_string($post[message]);		;
		$post = postwithoutspace($post);                
		$date = date("Y-m-d h:i:s");
                
		$sql = "INSERT INTO ".TBL_NEWSLETTER." SET `langId`='$post[language]', `senderEmailId`='$post[email]', `newsletterSubject`='$post[mailsubject]', `newsletterContaint`='$newslettermsg',`addDate`='$date', addedBy = '".$_SESSION['ADMIN_ID']."', `status`='1'";	
                if($file['upload']['name']){
                        $filename = stripslashes($file['upload']['name']);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);
                        if($this->checkExtensions($extension)) {
                            $image_name = date("Ymdhis").time().rand().'.'.$extension;
                            $target    = __NEWSLETTERORIGINAL__.$image_name;
                            $filestatus = 	move_uploaded_file($file['upload']['tmp_name'], $target);
                            chmod($target, 0777);
                            if($filestatus){
                                    chmod(__NEWSLETTERORIGINAL__,0777);
                                    $sql .= "  ,`attachedFile`='".$filename."', `attachmentPath`='$image_name' ";
                            }else{
                                 $_SESSION['SESS_MSG']=msgSuccessFail("fail","Attachment Upload Error!!!");
                            }
                        }else{
                            $_SESSION['SESS_MSG']=msgSuccessFail("fail","Attachment Extension Not Allowed!!!");
                        }
                }          
                
                if($_SESSION['SESS_MSG']==""){
                    $rst = $this->executeQry($sql);                
                    if($rst){
                        $this->logSuccessFail("1",$sql);
                    }else{
                        $this->logSuccessFail("0",$sql);
                    }
                    $_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.!!!");	
                }
		
		header('Location:addNewsletter.php?page=$post[page]');
		echo "<script language=javascript>window.location.href='addNewsletter.php?page=$post[page]';</script>";exit;
	}


	
	////// Edit Newsletter//////
	
	function editNewNewsletter($post,$file){
                //echo "<pre>";print_r($file);echo "</pre>";exit;
		$newslettermsg = mysql_real_escape_string($post[message]);
		$post = postwithoutspace($post);
		$date = date("Y-m-d h:i:s");
		$sql = "UPDATE ".TBL_NEWSLETTER." SET `langId`='$post[languageName]', `senderEmailId`='$post[email]', `newsletterSubject`='$post[mailsubject]', `newsletterContaint`='$newslettermsg', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' ";
			if($file['upload']['name']){
				$filename = stripslashes($file['upload']['name']);
				$extension = findexts($filename);
				$extension = strtolower($extension);                                
                                if($this->checkExtensions($extension)) {
                                        $image_name = date("Ymdhis").time().rand().'.'.$extension;
                                        $target    = __NEWSLETTERORIGINAL__.$image_name;                                        
                                        $filestatus = 	move_uploaded_file($file['upload']['tmp_name'], $target);                                        
                                        chmod($target, 0777);
                                        if($filestatus){                                            
                                                chmod(__NEWSLETTERORIGINAL__,0777);
                                                $sql .= "  ,`attachedFile`='$filename', `attachmentPath`='$image_name' ";
                                        }else{
                                            $_SESSION['SESS_MSG']=msgSuccessFail("fail","Attachment Upload Error!!!");
                                        }
                                }else{
                                    $_SESSION['SESS_MSG']=msgSuccessFail("fail","Attachment Extension Not Allowed!!!");
                                }
			}
		$sql .= " WHERE id='$post[newsletterId]'";
                if($_SESSION['SESS_MSG']==""){
                    $rst = $this->executeQry($sql);
                    if($rst){
                    $this->logSuccessFail("1",$sql);
                    }else{
                    $this->logSuccessFail("0",$sql);
                    }
                    $_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been updated successfully.!!!");
                    
                }
                echo "<script language=javascript>window.location.href='manageNewsletter.php?page=$post[page]';</script>";exit;
		
	}

	////// change status////

	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_NEWSLETTER,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "UPDATE ".TBL_NEWSLETTER." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		echo $status;		
	}
	
	///// Delete Newsletter
	function deleteValue($get) {
			$filename = $this->fetchValue(TBL_NEWSLETTER,"attachmentPath","id='$get[id]'");
			if($filename){
				if(file_exists(__NEWSLETTERORIGINAL__.$filename)){ unlink(__NEWSLETTERORIGINAL__.$filename);}
			}
			$sql = "DELETE FROM  ".TBL_NEWSLETTER." where id = '$get[id]'";
			$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
      echo "<script language=javascript>window.location.href='manageNewsletter.php?page=$post[page]&limit=$post[limit]';</script>";
	}


	///// Enable All/ Disable All // Delete All 

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageNewsletter.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$filename = $this->fetchValue(TBL_NEWSLETTER,"attachmentPath","id='$val'");
					$sql = "DELETE FROM ".TBL_NEWSLETTER." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						if($filename){
							if(file_exists(__NEWSLETTERORIGINAL__.$filename)){ unlink(__NEWSLETTERORIGINAL__.$filename);}
						}
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_NEWSLETTER." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_NEWSLETTER." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageNewsletter.php?page=$post[page]';</script>";
	}	

	/////// NewsLetter to edit ///////
	function newsLetterToEdit($id)
	{
		$rst=$this->selectQry(TBL_NEWSLETTER," id='$id' ",'','');
		$num=$this->getTotalRow($rst);
			if($num){
			$row=$this->getResultRow($rst);
			return $row;
			}else{
			echo "<script language=javascript>window.location.href='manageNewsletter.php';</script>";	
			}
	}
	/////////////////////////////////////////////////////////////////// Sent Newsletter Functions /////////////////////////////////
	function allSentNewsletterInformation() {
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$con = " AND senderEmailId LIKE '%$searchtxt%' OR newsletterSubject LIKE '%$searchtxt%' OR languageName  LIKE '%$searchtxt%' ";
		}
		$query = "select ".TBL_NEWSLETTER.".`id`,".TBL_NEWSLETTER.".`addDate`, ".TBL_NEWSLETTER.".`newsletterSubject`, ".TBL_NEWSLETTER.".`attachedFile`, ".TBL_NEWSLETTER.".`attachmentPath`, ".TBL_NEWSLETTER.".`status`,".TBL_LANGUAGE.".languageName from ".TBL_NEWSLETTER." INNER JOIN ".TBL_LANGUAGE." ON (".TBL_NEWSLETTER.".langId=".TBL_LANGUAGE.".id) where 1=1 $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "InActive";
					else
						$status = "Active";
					if($line->attachedFile){
						$attachment = "<a href='".__NEWSLETTERPATH__.$line->attachmentPath."' style='cursor:pointer;' target='new'>".$line->attachedFile."</a>";
					}else{
						$attachment = " -- ";
					}
					$genTable .= '<tr>
									<th>&nbsp;'.$i.'</th>
									<td>'.$line->languageName.'</td>
									<td>'.substr(stripslashes($line->newsletterSubject),0,100).'</td>
									<td>'.$attachment.'</td>
									<td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a href="sendNewsletter.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/sendMail.png" alt="Send" width="16" height="16" border="0" /></a>';
					}	
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}



	//////////////// Newsletter Users Type //////////
	function newsletterUsersChkBox($radioid){
		$rst = $this->selectQry(TBL_NEWSLETTERTBL_USERTYPE,"status='1' ORDER BY newsletterUserType ","","");                
		$preTable = "<ul>";
		while($row = $this->getResultObject($rst)){
			if($radioid == $row->id){ $sel = "checked=checked";}else{ $sel = "";}
			$preTable .= "<li style='clear:both;'><input name='usertypeId[]' type='checkbox' value='$row->id' $sel  class='send-news' />&nbsp;$row->newsletterUserType</li>";
		}
		$preTable .= "</ul>";
		return $preTable;	
	}

	function sendNewsletterToUsers($post){
                //echo "<pre>";print_r($post);echo "</pre>";exit;	  
		require("class.phpmailer-lite.php");
		$mail = new PHPMailerLite();
		
                //Start : User Type==========================			
                if($post['usertyperadio'] == "1"){	
                    
                        //Registered Users===================================    
                        if(in_array('1',$post['usertypeId'])){ 
                                $sql = "SELECT email FROM ".TBL_USER." WHERE status='1'";
                                $rst = $this->executeQry($sql);
                                $row = $this->getTotalRow($rst);
                                if($row){
                                        while($line = $this->getResultObject($rst)){
                                                $emailarray[] = $line->email;
                                        }
                                }
                        }
                        //Wait Confirm================================
                        if(in_array('2',$post['usertypeId'])){ 
                                $sql = "SELECT email FROM ".TBL_USER." WHERE status='0' AND activationCode !=''";
                                $rst = $this->executeQry($sql);
                                $row = $this->getTotalRow($rst);
                                if($row){
                                        while($line = $this->getResultObject($rst)){
                                                $emailarray[] = $line->email;
                                        }
                                }
                        }

                        //Inactive Users================================
                        if(in_array('3',$post['usertypeId'])){

                            $sql = "SELECT email FROM ".TBL_USER." WHERE status !='1'";
                                $rst = $this->executeQry($sql);
                                $row = $this->getTotalRow($rst);
                                if($row){
                                        while($line = $this->getResultObject($rst)){
                                                $emailarray[] = $line->email;
                                        }
                                }
                        }


                        //Subscriber====================================
                        if(in_array('4',$post['usertypeId'])){  // subscriber
                                $sql = "SELECT emailId FROM ".TBL_NEWSLETTERSUBSCRIBER." WHERE status='1'";
                                $rst = $this->executeQry($sql);
                                $row = $this->getTotalRow($rst);
                                if($row){
                                        while($line = $this->getResultObject($rst)){
                                                $emailarray[] = $line->emailId;
                                        }
                                }
                        }              

                        //Unsubscriber=====================================
                        if(in_array('5',$post['usertypeId'])){

                                $sql = "SELECT emailId FROM ".TBL_NEWSLETTERSUBSCRIBER." WHERE status='0'";
                                $rst = $this->executeQry($sql);
                                $row = $this->getTotalRow($rst);
                                if($row){
                                        while($line = $this->getResultObject($rst)){
                                                $emailarray[] = $line->email;
                                        }
                                }
                        }
                }
                
                //End : User Type==================================================
	
                //Start : Upload XLS FILES=============================================

                if($post['usertyperadio'] == "2"){	

                $importFileName1 = basename($_FILES['db_xlsfile']['name']);
                $target_path = __EMAILFILE__.$importFileName1;
                if(move_uploaded_file($_FILES['db_xlsfile']['tmp_name'], $target_path)) 
                        {
                        //echo "The file has been uploaded";
                } else{
                        //echo "There was an error uploading the file, please try again!";
                }

                include("excel_reader.php");
                $data = new Spreadsheet_Excel_Reader();
                if(basename($_FILES['db_xlsfile']['name']))
                        {
                                $extension  = explode('.',$importFileName1);	
                        }
                $data->setUTFEncoder('iconv');
                $data->setOutputEncoding('CP1251');
                $data->read($target_path);
                $totRows = $data->sheets[0]['numRows'];
                $numCols = $data->sheets[0]['numCols'];
                for ($i = 1; $i <= $totRows; $i++)
                                                { 	
                                                        if($data->sheets[0]['cells'][$i][1] != "") {
                                                                $emailarray[] = $data->sheets[0]['cells'][$i][1];	
                                                        }
                                                }	
                }	

                //End : Upload XLS FILES======================================
	
                
                //Start :User List : All User From User List===============================        
                if($post['usertyperadio'] == "3"){                    
                        if($cntUser = count($post[chk])){
                                $emailarray = $post[chk];
                        }
                }	
                //End : User List : All User From User List===============================
                
                //Start : Manual Emails===================================
                if($post['usertyperadio'] == "4"){	
                        if(trim($post['manualemailids']) != ""){
                                        $explodeemail = explode(",",$post['manualemailids']);
                                        for($i=0;$i<count($explodeemail);$i++){
                                                $emailarray[] = $explodeemail[$i];
                                        }
                                }	
                }		
                
                //End : Manual Email========================================
                
                
                //Start : Send Newsletter Now====================================
		if($post['sendnewsletternow'] == 1){                       
			//echo '<pre>';print_r($emailarray);echo'</pre>';exit;
			$mail->SetFrom($post[email], '');
			$mail->WordWrap = 50; 
			if($post[attachmentfile] && $post[attachmentname]){ 
				$mail->AddAttachment(__NEWSLETTERORIGINAL__.$post[attachmentfile]);         // add attachments
				}
			$mail->IsHTML(true);                                  // set email format to HTML
			$mail->Subject = $post[mailsubject];
			$mail->Body    = $post[message];
			$mail->AltBody = "This is the body in plain text for non-HTML mail clients";
                        if(count($emailarray)){
                            $sendtime= date("Y-m-d h:i:s")  ;
                            foreach($emailarray as $email){
                                $mail->AddAddress($email);
                                $mail->Send();	
                                $sql = "INSERT INTO ".TBL_EMAILSCHEDULE." SET `emailid`='".$email."', `newsletterid`='$post[newsletterId]', `sentdatetime`='".$sendtime."', `usertype`='".$post['usertyperadio']."',sendStatus='1'";	
                                $rst = $this->executeQry($sql);
                            }
                            $_SESSION['SESS_MSG'] =msgSuccessFail("success","Newsletter has been sent out successfully.");
                            header("Location:manageNewsletter.php");
                            echo "<script language=javascript>window.location.href='manageNewsletter.php';</script>";exit;	
                        }else{
                            $_SESSION['SESS_MSG'] = msgSuccessFail("fail","No email address to sent newsletter.");
                            header("Location:sendNewsletter.php?id=".base64_encode($post['newsletterId'])."'");
                            echo "<script language=javascript>window.location.href='sendNewsletter.php?id=".base64_encode($post[newsletterId])."';</script>";exit;
                        }
		}
                
                //End : Send Newsletter Now========================================================
                
                //Start: Scheduled Send Newsletter====================================
	
		if($post['sendnewsletternow'] == 2){
                    //print_r($post);
                    $sendtime=$post['dateTimeCust']  ;
                    foreach($emailarray as $email){
                        $sql = "INSERT INTO ".TBL_EMAILSCHEDULE." SET `emailid`='".$email."', `newsletterid`='$post[newsletterId]', `sentdatetime`='".$sendtime."', `usertype`='".$post['usertyperadio']."',sendStatus='0'";	
                        $rst = $this->executeQry($sql);
                    }
                    if(count($emailarray)){
                        $_SESSION['SESS_MSG'] =msgSuccessFail("success","Newsletter has been Scheduled successfully.");
                        header("Location:manageNewsletter.php");
                        echo "<script language=javascript>window.location.href='manageNewsletter.php';</script>"; exit;	
                    }else{
                        $_SESSION['SESS_MSG'] = msgSuccessFail("fail","No email address to Scheduled newsletter.");
                        header("Location:sendNewsletter.php?id=".base64_encode($post['newsletterId'])."'");
                        echo "<script language=javascript>window.location.href='sendNewsletter.php?id=".base64_encode($post[newsletterId])."';</script>";exit;
                    }
		}	
                //End : Scheduled Send Newsletter=====================================
	}



////////////////////////////////////////////////// Newsletter Template Functions ///////////////////////////////////////////////


	function allNewsletterTemplate() {
		
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
		$searchtxt = $_REQUEST['searchtxt'];
		$con = " AND templateName LIKE '%$searchtxt%'  ";
		}
		$query = "select * from ".TBL_NEWSLETTERTEMPLATE." where 1=1 $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;					
					$status=($line->status)?"Active":"Inactive";
					
                                        
                                        if ($line->isDefault == 1) {
                                            $isDefault = "Yes";	                                            
                                            $onclickstatus = '';
                                            $chkbox = '';
                                        } else {
                                            $isDefault = "No";                                           
                                            $onclickstatus = 'style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'newsletterTemplate\')"';
                                            $chkbox = '<input name="chk[]" value="' . $line->id . '" type="checkbox" class="checkbox">';
                                        }
						

					$genTable .= '<tr>
								 	<th>'.$chkbox.'</th>
									<td>'.$i.'</td>
									<td>'.$line->templateName.'</td>
									<td>';
									if($menuObj->checkEditPermission()) {							
										$genTable .= '<a href="pass.php?'.$_SERVER['QUERY_STRING'].'&id='.base64_encode($line->id).'&action=newsletterTemplate&type=defaultTemp">'.$isDefault.'</a>';
									}				
					$genTable .= '</td><td>';
									if($menuObj->checkEditPermission()) {							
										$genTable .= '<div id="'.$div_id.'" '.$onclickstatus.' >'.$status.'</div>';
									}				

					$genTable .= '</td><td><a rel="shadowbox;width=705;height=425" title="'.$line->languageName.'" href="viewNewsletterTemp.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td><td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editNewsletterTemp.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td><td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Newsletter Template?')){window.location.href='pass.php?action=newsletterTemplate&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}
	

//////////////////// Change Default Template Status //////////////////////////////

	function changeDefaultStatus($get){
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_NEWSLETTERTEMPLATE;		
		$this->field_values['isDefault'] = "1" ;
		$this->condition  = "id = '".base64_decode($get[id])."' ";
		$res = $this->updateQry();
		
		$this->field_values['isDefault'] = "0" ;
		$this->condition  = "id != '".base64_decode($get[id])."' ";
		$res = $this->updateQry();
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Default status has been changed successfully.");
		header("Location:manageNewsletterTemp.php?page=".$get[page]."&limit=".$get[limit]."&searchtxt=".$get[searchtxt]);
		exit;		
	}

//////////////////// Change Active Template Status //////////////////////////////

function changeTemplateStatus($get) {
		$status=$this->fetchValue(TBL_NEWSLETTERTEMPLATE,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "UPDATE ".TBL_NEWSLETTERTEMPLATE." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
                if($rst){
                    $this->logSuccessFail("1",$sql);
                }else{
                    $this->logSuccessFail("0",$sql);
                }
		echo $status;		
	}
	

	/////// NewsLetter Template to edit ///////
	function newsLetterTempToEdit($id)
	{
		$rst=$this->selectQry(TBL_NEWSLETTERTEMPLATE," id='$id' ",'','');
		$num=$this->getTotalRow($rst);
			if($num){
			$row=$this->getResultRow($rst);
			return $row;
			}else{
			echo "<script language=javascript>window.location.href='manageNewsletterTemp.php';</script>";	
			}
	}



//////////////////// Edit Newsletter Template  //////////////////////////////

	function addNewsletterTemplate($post){
                //echo "<pre>";print_r($post);echo "</pre>";exit;
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_NEWSLETTERTEMPLATE;		
		$this->field_values['status'] = $post['status'] ;
		$this->field_values['templateContaint'] = addslashes($post['message']) ;
		$this->field_values['templateName'] = addslashes($post['tempname']) ;
		$this->field_values['addDate'] = date("Y-m-d h:i:s") ;
                $this->field_values['addedBy'] = $_SESSION['ADMIN_ID'];
		$res = $this->insertQry();
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.");		
		header("Location:addNewsletterTemp.php");exit;		
	}



//////////////////// Edit Newsletter Template  //////////////////////////////

	function editNewsletterTemplate($post){
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_NEWSLETTERTEMPLATE;		
		$this->field_values['status'] = $post['status'] ;
		$this->field_values['templateContaint'] = addslashes($post['message']) ;
		$this->field_values['templateName'] = addslashes($post['tempname']) ;
		$this->condition  = "id = '".$post['newsletterId']."' ";
		$res = $this->updateQry();
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been updated successfully.");
		header("Location:manageNewsletterTemp.php?page=".$post[page]."&limit=".$post[limit]);
		exit;		
	}

	
	///// Delete Newsletter Template
	function deleteNewsletterTemp($get) {
			$sql = "DELETE FROM  ".TBL_NEWSLETTERTEMPLATE." where id = '$get[id]'";
			$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
      echo "<script language=javascript>window.location.href='manageNewsletterTemp.php?page=$post[page]&limit=$post[limit]';</script>";
	}


///// Enable All/ Disable All // Delete All 

	function deleteAllTemplateValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageNewsletterTemp.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql = "DELETE FROM ".TBL_NEWSLETTERTEMPLATE." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_NEWSLETTERTEMPLATE." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_NEWSLETTERTEMPLATE." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageNewsletterTemp.php?page=$post[page]';</script>";
	}	
	


	function getTemplateInDropDown($tempId){
		$sql = "SELECT id,templateName,isDefault FROM  ".TBL_NEWSLETTERTEMPLATE." WHERE status='1' ORDER BY templateName ";
		$rst = $this->executeQry($sql);
		$preTable .= "<option value=''>Please Select Template </option>";
		while($row = mysql_fetch_array($rst)){
			if($tempId == $row[id]) $selected = 'selected=selected';else $selected = "";
			$preTable .= "<option value='$row[id]' $selected>".stripslashes($row['templateName'])."</option>";
		}
		return $preTable;
	}


	function getTemplateContaints($tempId){
		if(!$tempId){
		$tempId = $this->fetchValue(TBL_NEWSLETTERTEMPLATE,"id","isDefault ='1'" );
		}
		return $this->fetchValue(TBL_NEWSLETTERTEMPLATE,"templateContaint","id='$tempId'" );
	}


	function getAllUserList(){
		$sql = "SELECT id, firstName,lastName,email FROM ".TBL_USER;               
		$rst = $this->executeQry($sql);
		$preTable = "<ul>";
		while($row = $this->getResultRow($rst)){
                        $username=$row[firstName]." ".$row[lastName];
                        $email=$row[email];
			$preTable .= "<li style='clear:both;'><input type='checkbox' name='chk[]' value='$row[email]'>&nbsp;".$username."&nbsp;(&nbsp;".$email."&nbsp;)&nbsp;</li>";
			
					
		}
		$preTable .= "</ul>";
		return $preTable;
		}
		
	////////////////////////////////////////////////// Newsletter Subscriber Functions ///////////////////////////////////////////////


	function allNewsletterSubscriber() {		
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
		$searchtxt = $_REQUEST['searchtxt'];
		$con = " AND emailId LIKE '%$searchtxt%'  ";
		}
		$query = "select * from ".TBL_NEWSLETTERSUBSCRIBER." where 1=1 $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "InActive";
					else
						$status = "Active";
					
					if ($line->isDefault==0)
						$isDefault = "No";
					else
						$isDefault = "Yes";	

					$genTable .= '<tr>
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$this->fetchValue(TBL_LANGUAGE,'languageName',"id = '".$line->langId."'").'</td>
									<td>'.$line->emailId.'</td>';
												
					$genTable .= '<td>';
									if($menuObj->checkEditPermission()) {							
										$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'newsletterSubscriber\')" >'.$status.'</div>';
									}				

					$genTable .= '</td><td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Newsletter Template?')){window.location.href='pass.php?action=newsletterSubscriber&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
				{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				echo $currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}


//////////////////// Change Active Subscriber Status //////////////////////////////

function changeSubscriberStatus($get) {
		$status=$this->fetchValue(TBL_NEWSLETTERSUBSCRIBER,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$sql = "UPDATE ".TBL_NEWSLETTERSUBSCRIBER." set status = '$stat', addDate = '".date('Y-m-d H:i:s')."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		echo $status;		
	}
	
///// Enable All/ Disable All // Delete All 

	function deleteAllSubscriberValues($post){
	
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='NewsletterSubscriber.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql = "DELETE FROM ".TBL_NEWSLETTERSUBSCRIBER." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
		
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_NEWSLETTERSUBSCRIBER." set status ='1', addDate = '".date('Y-m-d H:i:s')."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
		//echo newsletterSubscriber;die;
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					 $sql="update ".TBL_NEWSLETTERSUBSCRIBER." set status ='0', addDate = '".date('Y-m-d H:i:s')."' where id='$val'";
					$rst = $this->executeQry($sql);
					if($rst){
					$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='NewsletterSubscriber.php?page=$post[page]';</script>";
	}	
	
	
///// Delete Newsletter Template
	function deleteNewsletterSubscriber($get) {
			$sql = "DELETE FROM  ".TBL_NEWSLETTERSUBSCRIBER." where id = '$get[id]'";
			$rst = $this->executeQry($sql);
			if($rst){
			$this->logSuccessFail("1",$sql);
			}else{
			$this->logSuccessFail("0",$sql);
			}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
      echo "<script language=javascript>window.location.href='NewsletterSubscriber.php?page=$post[page]&limit=$post[limit]';</script>";
	}	
		


}// End Class
?>	

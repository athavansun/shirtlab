<?php 
session_start();
class Decoration extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function saveRecord($post){
	
/*	echo"<pre>";
		print_r($post);
	echo"</pre>";
	exit;
*/	
	if( isset($post['digitalPrinting']) || isset($post['screenPrinting']) || isset($post['embroideryPrice']) ){
		
		$genFunObj = new GeneralFunctions();

		
		$this->deleteRec(TBL_DECORATION_PROCESS_PRICING_RELATION,' fulfillment_id = "'.$_SESSION['FULFILLMENTID'].'" ');
	
		if(isset($post['digitalPrinting'])){
			$query1 = "INSERT INTO ".TBL_DECORATION_PROCESS_PRICING_RELATION." SET decoration_process_id = '".$post['digital']."',decoration_pricing_methods_id = '".$post['white-light-dark-price']."',price = '".$post['white']."-".$post['light']."-".$post['dark']."',fulfillment_id = '".$_SESSION['FULFILLMENTID']."', currency_id = '".$_SESSION['DEFAULTCURRENCYID']."', price_in_dollar = '".$genFunObj->calculatePriceInDollar($post['white'], $_SESSION['DEFAULTCURRENCYID'] )."-".$genFunObj->calculatePriceInDollar($post['light'], $_SESSION['DEFAULTCURRENCYID'])."-".$genFunObj->calculatePriceInDollar($post['dark'], $_SESSION['DEFAULTCURRENCYID'])."' ";
			$this->executeQry($query1);
		}
		if(isset($post['screenPrinting'])){
			$query2 = "INSERT INTO ".TBL_DECORATION_PROCESS_PRICING_RELATION." SET decoration_process_id = '".$post['screen']."',decoration_pricing_methods_id = '".$post['single-price']."',price_table_id = '".$post['single']."',fulfillment_id = '".$_SESSION['FULFILLMENTID']."',currency_id = '".$_SESSION['DEFAULTCURRENCYID']."' ";
		$this->executeQry($query2);
		}
		
		if(isset($post['embroideryPrice'])){
			$query3 = "INSERT INTO ".TBL_DECORATION_PROCESS_PRICING_RELATION." SET decoration_process_id = '".$post['embroidery']."',decoration_pricing_methods_id = '".$post['price-table']."', price_table_id = '".$post['priceTable']."',fulfillment_id = '".$_SESSION['FULFILLMENTID']."',currency_id = '".$_SESSION['DEFAULTCURRENCYID']."', stitch_count = '".$post['stitchCount']."', digitizing_fee = '".$post['digiFee']."', digitizing_fee_dollar = '".$genFunObj->calculatePriceInDollar($post['digiFee'], $_SESSION['DEFAULTCURRENCYID'])."' ";
		$this->executeQry($query3);
		}
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your information has been added successfully.");
		header("location:decorationPrices.php");exit;
	}
	else{
		$_SESSION['SESS_MSG'] = msgSuccessFail("fail","You must choose a decoration processes.");
		header("location:decorationPrices.php");exit;
	}
}
	
	
	function getDigitalPrinting($decProId, $decoPriMetId){
	
		$query = " SELECT * FROM ".TBL_DECORATION_PROCESS_PRICING_RELATION." WHERE decoration_process_id = '".$decProId."' AND 
						decoration_pricing_methods_id = '".$decoPriMetId."' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		$data  = $this->getResultObject($sql);
		return $data;
	
	}
	
	function getScreenPrinting($decProId, $decoPriMetId){
	
		$query = " SELECT * FROM ".TBL_DECORATION_PROCESS_PRICING_RELATION." WHERE decoration_process_id = '".$decProId."' AND 
						decoration_pricing_methods_id = '".$decoPriMetId."' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		$data  = $this->getResultObject($sql);
		return $data;
	
	}

	function getEmbroidery($decProId, $decoPriMetId){
	
		$query = " SELECT * FROM ".TBL_DECORATION_PROCESS_PRICING_RELATION." WHERE decoration_process_id = '".$decProId."' AND 
						decoration_pricing_methods_id = '".$decoPriMetId."' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		$data  = $this->getResultObject($sql);
		return $data;
	
	}
	
	function getPriceTable($tableId){
	
		$option = "";
		$query = "SELECT id, name FROM ".TBL_DECORATION_PRICE_TABLE." WHERE status = '1' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		while($data = $this->getResultObject($sql)){
			if($tableId == $data->id){ $selected = 'selected="selected"';}else{  $selected = '';}
				$option .= '<option value="'.$data->id.'" '.$selected.' > '.$data->name.' </option>';
		}
		return $option;
	}
	
	function getSinglePriceTable($tableId){
	
		$option = "";
		$query = "SELECT id, name FROM ".TBL_DECORATION_PRICE_TABLE_SCREEN." WHERE status = '1' AND fulfillment_id = '".$_SESSION['FULFILLMENTID']."' ";
		$sql = $this->executeQry($query);
		while($data = $this->getResultObject($sql)){
			if($tableId == $data->id){ $selected = 'selected="selected"';}else{  $selected = '';}
				$option .= '<option value="'.$data->id.'" '.$selected.' > '.$data->name.' </option>';
		}
		return $option;
	}




}// End Class

?>	
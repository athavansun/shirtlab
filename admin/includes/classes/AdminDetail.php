<?php
session_start();
class AdminDetail extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail()
	{		
		//echo "select t1.* from ".TBL_ADMINLOGIN." AS t1,".TBL_ADMINLOGIN." 
		//AS t2 WHERE 1 AND t1.id=t2.id and (FIND_IN_SET($_SESSION[ADMIN_ID],t1.acl) OR t2.id='".$_SESSION['ADMIN_ID']."')";
		//var_dump($_SESSION);//ADMIN_ID
		//echo $_SESSION[ADMIN_ID];
		if($_SESSION['USERLEVELID'] == '-1') $sql = $this->executeQry("select * from ".TBL_ADMINLOGIN." where 1");
		else $sql = $this->executeQry("select t1.* from ".TBL_ADMINLOGIN." AS t1,".TBL_ADMINLOGIN." AS t2 WHERE 1 AND t1.id=t2.id and (FIND_IN_SET($_SESSION[ADMIN_ID],t1.acl) OR t2.id='".$_SESSION['ADMIN_ID']."')");
		
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		if($num > 0)
		{
			$genTable = '<thead>
                                        <tr>
                                         <th width="8%">SL.No</th>
                                         <th width="23%">User Name</th>
                                         <th width="30%">Email Id</th>
                                         <th width="10%">Status</th>
                                         <th width="10%">Edit</th>
                    			 <th width="10%">Delete</th>
                                        </tr>
                                    </thead>';
			$i = 1;		
			while($line = $this->getResultObject($sql)) {
				$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
				$div_id = "status".$line->id;
				if ($line->status==0)
					$status = "InActive";
				else
					$status = "Active";
         if( $_SESSION['ADMIN_ID']==$line->id)
					$highlight='bydefault';
					
				$genTable .= '<tr class="'.$highlight.'">
									<th>'.$i.'</th>
									<td>'.$line->username.'</td>
									<td>'.$line->emailId.'</td>';
				if($line->adminLevelId!=-1)
				{
                     if($menuObj->checkEditPermission() && $line->id!=$_SESSION['ADMIN_ID']) {							
					 	$genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'admin\')">'.$status.'</div></td>';
					}
					else {$genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" >'.$status.'</div></td>';}
				}
				else {
                   $genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" >'.$status.'</div></td>';
                }
				
				$genTable .= '<td>';
									
				if($menuObj->checkEditPermission()) {					
					$genTable .= '<a href="editAdmin.php?id='.base64_encode($line->id).'" class="i_pencil edit" ></a>';
				}	
				$genTable .= '</td><td>';
				if($line->adminLevelId!=-1)
				{
    				if($menuObj->checkDeletePermission() && $line->id!=$_SESSION['ADMIN_ID']) {					
						$genTable .= "<a href='javascript:void(NULL);'  class='i_trashcan edit' onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=admin&type=delete&id=".$line->id."&page=$page'}else{}\" ></a>";
					}
				}
				$genTable .= '</td></tr>';
				$i++;	
			}			
         return $genTable;
		}	
	}
	function changStatus($get) {
		$status=$this->fetchValue(TBL_ADMINLOGIN,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
	
		$query = "update ".TBL_ADMINLOGIN." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query)) echo $status;
			//$this->logSuccessFail('1',$query);		
		//else 	
			//$this->logSuccessFail('0',$query);
		//echo $status;
	}
	
	function deleteRecord($get) {
		$adminLevelId = $this->fetchValue(TBL_ADMINLOGIN,"adminLevelId"," id = '$get[id]'");
		$this->deleteval(TBL_ADMINLOGIN," id = '$get[id]'");
		$this->deleteval(TBL_ADMINLEVEL," id = '$adminLevelId'");
		$this->deleteval(TBL_ADMINPERMISSION," adminLevelId = '$adminLevelId'");		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='administrator.php';</script>";
	}
	
	function getResult($id) {
		$sql_user = $this->executeQry("select * from ".TBL_ADMINLOGIN." where id = '$id'");
		$num_user = $this->getTotalRow($sql_user);
		if($num_user > 0) {
			return $line_user = $this->getResultObject($sql_user);	
		} else {
			redirect("administrator.php");
		}	
	}	
	
	function encrypt_password($plain) {
		$password = '';
		for ($i=0; $i<10; $i++) {
			$password .= $this->tep_rand();
		}
		$salt = substr(md5($password), 0, 2);
		$password = md5($salt . $plain) . ':' . $salt;
		return $password;
	}
	
	function tep_rand($min = null, $max = null) {
	    static $seeded;
    	if (!$seeded) {
	      mt_srand((double)microtime()*1000000);
    	  $seeded = true;
    	}
	}
	
	function addNewAdmin($post)
	{
		$_SESSION['SESS_MSG'] = "";
		if($post[userName] == "") {
			$_SESSION['SESS_MSG'] .= "Enter username. <br> ";
		}
		if($post[userEmail] == "") {
			$_SESSION['SESS_MSG'] .= "Enter Email ID. <br> ";
		} else {
			if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $post[userEmail])){
				$_SESSION['SESS_MSG'] .= "Enter Valid Email ID. <br> ";
			}
		} 
		if($post[password] == "") {
			$_SESSION['SESS_MSG'] .= "Enter Password. <br> ";
		} else {
			if (strlen($post[password]) < 6){
				$_SESSION['SESS_MSG'] .= "Enter password of alteast 6 charactors. <br> ";
			}
		}
		
		if($_SESSION['SESS_MSG'] == "")
		{
			//=============== implemented by Rashid ====================
			$sql = "SELECT id, acl FROM ".TBL_ADMINLOGIN." WHERE id = '".$_SESSION['ADMIN_ID']."'";
			$rst = $this->executeQry($sql);
			if($this->getTotalRow($rst) > 0)
			{
				$line =  $this->getResultObject($rst);
				$acl = $line->acl?$line->acl.','.$line->id:$line->id;
			}
			//==========================================================
			$sql1 = $this->executeQry("insert into ".TBL_ADMINLEVEL." set name = '$post[userName]', status = '1'");
			$inserted_id = mysql_insert_id();
			$password = $this->encrypt_password($post[password]);
			$hash = md5($post[userName].":".$password);
			"insert into ".TBL_ADMINLOGIN." set username = '$post[userName]', 
			password = '$password', emailId = '$post[userEmail]', hash = '$hash', adminLevelId = '$inserted_id', 
			addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', status = '1',acl='".$acl."'";
			$query = $this->executeQry("insert into ".TBL_ADMINLOGIN." set username = '$post[userName]', 
			password = '$password', emailId = '$post[userEmail]', hash = '$hash', adminLevelId = '$inserted_id', 
			addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."', status = '1',acl='".$acl."'");
			
			if(count($post[menuCheck]) > 0)
			{
				foreach($post[menuCheck] as $key=>$value)
				{	
					$add = "menuCheck_".$value."_add";
					$edit = "menuCheck_".$value."_edit";
					$del = "menuCheck_".$value."_del";
					$query = $this->executeQry("insert into ".TBL_ADMINPERMISSION." set adminLevelId = '$inserted_id', menuid = '$value', 
					add_record = '".trim($post[$add])."',edit_record = '".trim($post[$edit])."',delete_record = '".trim($post[$del])."'");
				}
			}
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been added successfully.");	
		}
		header("Location:addAdmin.php");exit;
	}

	function editAdminDetail($post)
	{	
		$_SESSION['SESS_MSG'] = "";
		if($post[userName] == "") {
			$_SESSION['SESS_MSG'] .= "Enter username. <br> ";
		}
		if($post[userEmail] == "") {
			$_SESSION['SESS_MSG'] .= "Enter Email ID. <br> ";
		} else {
			if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $post[userEmail])){
				$_SESSION['SESS_MSG'] .= "Enter Valid Email ID. <br> ";
			}
		} 
		
		if ($post[password] != "" && strlen($post[password]) < 6){
			$_SESSION['SESS_MSG'] .= "Enter password of alteast 6 charactors. <br> ";		
		}
		
		if($_SESSION['SESS_MSG'] == "")
		{
			$adminLevelId = $post[adminLevelId];
			
			$query = $this->executeQry("update ".TBL_ADMINLOGIN." set username = '$post[userName]', emailId = '$post[userEmail]', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[admin_id]'");
			if($post[password] != "") {
				$password = $this->encrypt_password($post[password]);
				$query = $this->executeQry("update ".TBL_ADMINLOGIN." set password = '$password' where id = '$post[admin_id]'");
			}	
			if(($_SESSION['ADMIN_ID'] != $post[admin_id]) || ($_SESSION['USERLEVELID'] == '-1' ))
			{
				$this->deleteval(TBL_ADMINPERMISSION,"1 and adminLevelId = '$adminLevelId'");
			
				if(count($post[menuCheck]) > 0)
				{
					foreach($post[menuCheck] as $key=>$value)
					{
						$add = "menuCheck_".$value."_add";
						$edit = "menuCheck_".$value."_edit";
						$del = "menuCheck_".$value."_del";
						//if($_SESSION['ADMIN_ID'] != $post['admin_id']) {
						//	if(!empty($post[$add]) || !empty($post[$edit]) || !empty($post[$del])) 
						//	{
								$query = $this->executeQry("insert into ".TBL_ADMINPERMISSION." set adminLevelId = '$adminLevelId', menuid = '$value', 
								add_record = '".trim($post[$add])."', edit_record = '".trim($post[$edit])."', delete_record = '".trim($post[$del])."'");
						//	}
						/*}
						else {
							$query = $this->executeQry("insert into ".TBL_ADMINPERMISSION." set adminLevelId = '$adminLevelId', menuid = '$value', 
							add_record = '".trim($post[$add])."', edit_record = '".trim($post[$edit])."', delete_record = '".trim($post[$del])."'");
						}*/
					}
       				//$_SESSION['SESS_MSG'] = msgSuccessFail("success","Record has been Updated successfully.");
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Record has been Updated successfully.");
			}
      		echo header("Location:administrator.php");exit;
			echo "<script>window.location.href='administrator.php';</script>";exit;
		}
	}
}// End Class
?>	
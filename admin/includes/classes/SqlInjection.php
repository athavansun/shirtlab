<?php
class SqlInjection
{
    function secureSuperGlobalGET(&$value, $key)
    {
	
        $_GET[$key] = htmlspecialchars(stripslashes($_GET[$key]));
        $_GET[$key] = str_ireplace("script", "blocked", $_GET[$key]);
        $_GET[$key] = mysql_escape_string($_GET[$key]);
        return $_GET[$key];
    }
   
    function secureSuperGlobalPOST(&$value, $key)
    {
        $_POST[$key] = htmlspecialchars(stripslashes($_POST[$key]));
        $_POST[$key] = str_ireplace("script", "blocked", $_POST[$key]);
        $_POST[$key] = mysql_escape_string($_POST[$key]);
        return $_POST[$key];
    } 
	
    
	
	function secureGlobals()
    {
        array_walk($_GET, array($this, 'secureSuperGlobalGET'));
        array_walk($_POST, array($this, 'secureSuperGlobalPOST'));
    }
	
	  /*------------   For SQL Injection  ----------------------*/  

	
	 function secureSuperGlobalGETValidation(&$value, $key,$validate)
    {
	      $filteration = explode(",",$validate);
		   $search = explode(" ",$_GET[$key]);
		   foreach ($filteration as $word )
           {		   
	        if(in_array("$word",$search))
		     {
		     $_POST["Total_Errors"].=$word.",";
		     }
		   }       
           return $_POST["Total_Errors"];
    }
       
     function secureSuperGlobalPOSTValidation(&$value, $key,$validate)
    {	 	
	       $filteration = explode(",",$validate);
		   $search = explode(" ",$_POST[$key]);
		    foreach ($filteration as $word )
            {		   
	        if(in_array("$word",$search))
		     {
		     $_POST["Total_Errors"].=$word.",";
		     }
		   }       
          return $_POST["Total_Errors"];
    }
	
/*------------   For Server side validation
	              Usage :  simple include  file and create  object  of class then call  validateGlobal() function , $_POST["Total_Errors"]  this variable  will contail all invalid  characters  */
	
	function validateGlobal()
	{
        $validate="<,>,script,alert(,document.write";
	    array_walk($_GET, array($this, 'secureSuperGlobalGETValidation'),$validate);
        array_walk($_POST, array($this, 'secureSuperGlobalPOSTValidation'),$validate);
	}
	
	
}

?>
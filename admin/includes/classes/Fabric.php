<?php 
session_start();
class Fabric extends MySqlDriver{
	function __construct() { $this->obj = new MySqlDriver; }
        
	//=====================================================================
	//FABRIC PAGE 
	//=====================================================================
    //=============View Fabric=============================================
    function valFabric() {        						
		
		$cond = "1 and f.id = fd.fabricId and fd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";		
		//$cond = " f.id = fd.fabricId and fq.fId = f.id and fd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND fd.fabricName LIKE '%$searchtxt%' OR f.type LIKE  '%$searchtxt%'";
		}			
		
		$query = "select f.*, fd.fabricName from ".TBL_FABRIC." as f inner join ".TBL_FABRICDESC." as fd on " . $cond;
		//$query = "select fq.*, fd.fabricName, fq.fquality from ".TBL_FABRIC." as f inner join ".TBL_FABRICDESC." as fd inner join (select fdetails.*, fq.fquality from ecart_fabric_details as fdetails left outer join ecart_fabric_quality fq on fdetails.fQualityId = fq.id) as fq on ".$cond;			
		
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			$genTable = '';
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					//~ echo "<pre>";
					//~ print_r($line);
					//~ echo "</pre>";exit;	
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"Inactive";

					$genTable .= '<tr>
						<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>						
						<td>'.$i.'</td>						
						<td>'.stripslashes($line->fabricName).'</td>';
                    $genTable.='<td>';                    
					if($menuObj->checkEditPermission()) {							
					$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'manageFabric\')" >'.$status.'</div>';
					}	
					$genTable .= '</td>';                                        
                                        
                    $genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->fabricName).'" href="viewFabric.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';                    
                    $genTable.='<td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editFabric.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td>';
                                        
                    $genTable.='<td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageFabric&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</td>';
                                        
                    $genTable.='</tr>';
					$i++;	
				}
				switch($recordsPerPage){
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}
	
		
	//=============Edit Fabric================
	function editFabric($post){
		//~ echo "<pre>";
		//~ print_r($post);
		//~ echo "</pre>";
		//~ exit;
				//~ 
		$xmlArr = array();
        $xm = 1;
                
		$post = postwithoutspace($post);                
		$_SESSION['SESS_MSG'] == "";
						
		$query = "update ".TBL_FABRIC." set modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";
		$this->executeQry($query);
		
		$xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $post['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;
        
		if($_SESSION['SESS_MSG'] == "") {					
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {
					$fabricName = 'fabricName_'.$line->id;					
					$sql = $this->selectQry(TBL_FABRICDESC,' fabricId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
					$numrows = $this->getTotalRow($sql);
					if($numrows == 0) {						
						$query = "insert into ".TBL_FABRICDESC." set fabricId = '".$post[id]."', fabricName = '$post[$fabricName]', langId = '".$line->id."'";
						
						$xmlArr[$xm]['query'] = addslashes($query);
				        $xmlArr[$xm]['identification'] = $line->id;
				        $xmlArr[$xm]['section'] = "insert";
				        $xm++;
				        
						if($this->executeQry($query)) 
							$this->logSuccessFail('1',$query);		
						else 	
							$this->logSuccessFail('0',$query);	
					}else {
						$query = "update ".TBL_FABRICDESC." set fabricName= '".$post[$fabricName]."' where 1 and fabricId = '".$post[id]."' and langId = '".$line->id."'"; 
						
						$xmlArr[$xm]['query'] = addslashes($query);
	                    $xmlArr[$xm]['identification'] = $post[id];
	                    $xmlArr[$xm]['section'] = "update";
	                    $xm++;
						
						if($this->executeQry($query)) {
							$this->logSuccessFail('1',$query);		
						}else {
							$this->logSuccessFail('0',$query);	
						}
					}
				}	
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);	
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been updated successfully.!!!");	
		header('Location:manageFabric.php');exit;
		//echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]';</script>";exit;
	}
	
	//=============Add Fabric====================
    function addFabric($post){
		//echo "<pre>";print_r($_POST);echo "</pre>";exit;
		$xmlArr = array();
        $xm = 1;
        $inserted_fQualityId = 0;
        
		$query = "insert into ".TBL_FABRIC." set status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."'";
		$res = $this->executeQry($query);
		$inserted_Id = mysql_insert_id();
		
		if($res) {
			$this->logSuccessFail('1',$query);		
		} else {
			$this->logSuccessFail('0',$query);
		}	
		
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $inserted_Id;
        $xmlArr[$xm]['section'] = "insert";
        $xm++;
		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
			   	$fabricName = 'fabricName_'.$line->id;
				$query = "insert into ".TBL_FABRICDESC." set  fabricId  = '$inserted_Id', langId = '".$line->id."' , fabricName = '".$post[$fabricName]."'";
				   
				if($this->executeQry($query)) {
					$this->logSuccessFail('1',$query);		
				} else {
					$this->logSuccessFail('0',$query);
				}
				
				$xmlArr[$xm]['query'] = addslashes($query);
                $xmlArr[$xm]['identification'] = $inserted_Id;
                $xmlArr[$xm]['section'] = "insert";
                $xm++;                 
			}	
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);   
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been added successfully.");
		header("Location:addFabric.php");exit;	
		
	}

    //Start : Get Result ================================================
    function getResult($id) {		
		$query = "Select * from ".TBL_FABRICDESC." where fabricId = '".$id."'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {				
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageFabric.php');	
		}
	}
	
    //Start : Change Value Status===========================================
    function changeFabricValueStatus($get) {
    	$xmlArr = array();
        $xm = 1;
		$status=$this->fetchValue(TBL_FABRIC,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive, 0";
		} else 	{
			$stat= 1;
			$status="Active, 1";
		}
						
		$sql = "UPDATE ".TBL_FABRIC." set status = '$stat' where id = '$get[id]'";
		
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
	
	
    //Start : Delete Single Value=================================
    function deleteFabricValue($get) {	
    	$xmlArr = array();
        $xm = 1;	
   
        $sql = "DELETE FROM  ".TBL_FABRICDESC." where fabricId = '$get[id]'";		
		$rst = $this->executeQry($sql);
		
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;        
        
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
        
		$sql = "DELETE FROM  ".TBL_FABRIC." where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		
        $xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;
        		
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
        $xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]&limit=$post[limit]';</script>";
	}
        
        
    //Start : Mulitple Operation=========================================
	function deleteAllFabricValues($post){							
		$xmlArr = array();
        $xm = 1;
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		
		if($post[action] == 'deleteselected'){						
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){										
					$sql = "DELETE FROM ".TBL_FABRICDESC." where fabricId = '$val'";
					$rst = $this->executeQry($sql);
										
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}					
						
					$sql = "DELETE FROM ".TBL_FABRIC." where id = '$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}			
		}		
	
		if($post[action] == 'enableall'){			
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRIC." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}

		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRIC." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]';</script>";exit;
	}	
	
	//=====================================================================
	// Fabric Quality 
	//=====================================================================
	//=============View Fabric Quality=============================================
    function valFabricQuality() {        								
		$cond = "1 and fq.fId = fd.fabricId and fd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";						
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND fd.fabricName LIKE '%$searchtxt%' OR f.type LIKE  '%$searchtxt%'";
		}		
							
		$query = "select fq.*, fd.fabricName from ".TBL_FABRIC_QUALITY." as fq left outer join ".TBL_FABRICDESC." as fd on " . $cond;
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"fabricName";
			$order = $_GET[order]?$_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			$genTable = '';
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					//~ echo "<pre>";
					//~ print_r($line);
					//~ echo "</pre>";exit;	
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"Inactive";

					$genTable .= '<tr>
						<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>						
						<td>'.$i.'</td>						
						<td>'.stripslashes($line->fabricName).'</td>
						<td>'.stripslashes($line->fQuality).'</td>';
                    $genTable.='<td>';                    
					if($menuObj->checkEditPermission()) {							
					$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'manageFabricQuality\')" >'.$status.'</div>';
					}	
					$genTable .= '</td>';                                       
                                        
                    $genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->fabricName).'" href="viewFabricQuality.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';                    
                    $genTable.='<td>';
                    
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editFabricQuality.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td>';
                                        
                    $genTable.='<td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageFabricQuality&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</td>';
                                        
                    $genTable.='</tr>';
					$i++;	
				}
				switch($recordsPerPage){
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}
	
	
	//=============Edit Fabric Quality================
	function editFabricQuality($post){
		// "<pre>";print_r($post); echo "</pre>";exit;
				
		$xmlArr = array();
        $xm = 1;
                
		$post = postwithoutspace($post);                
		$_SESSION['SESS_MSG'] == "";
						
		$query = "update ".TBL_FABRIC." set modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";
		$this->executeQry($query);
		
		$xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $post['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;
        
		if($_SESSION['SESS_MSG'] == "") {					
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {
					$fabricName = 'fabricName_'.$line->id;					
					$sql = $this->selectQry(TBL_FABRICDESC,' fabricId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
					$numrows = $this->getTotalRow($sql);
					if($numrows == 0) {						
						$query = "insert into ".TBL_FABRICDESC." set fabricName = '$post[$fabricName]', langId = '".$line->id."'";
						
						$xmlArr[$xm]['query'] = addslashes($query);
				        $xmlArr[$xm]['identification'] = $line->id;
				        $xmlArr[$xm]['section'] = "insert";
				        $xm++;
				        
						if($this->executeQry($query)) 
							$this->logSuccessFail('1',$query);		
						else 	
							$this->logSuccessFail('0',$query);	
					}else {
						$query = "update ".TBL_FABRICDESC." set fabricName= '".$post[$fabricName]."' where 1 and fabricId = '".$post[id]."' and langId = '".$line->id."'"; 
						
						$xmlArr[$xm]['query'] = addslashes($query);
	                    $xmlArr[$xm]['identification'] = $post[id];
	                    $xmlArr[$xm]['section'] = "update";
	                    $xm++;
						
						if($this->executeQry($query)) {
							$this->logSuccessFail('1',$query);		
						}else {
							$this->logSuccessFail('0',$query);	
						}
					}
				}	
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);	
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been updated successfully.!!!");	
		header('Location:manageFabric.php');exit;
		//echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]';</script>";exit;
	}
	
	//=============Add Fabric Quality====================
    function addFabricQuality($post){
		//echo "<pre>";print_r($_POST);echo "</pre>";exit;
		$xmlArr = array();
        $xm = 1;
        $inserted_fQualityId = 0;
        
		$query = "insert into ".TBL_FABRIC_QUALITY." set fId = '".$post[fabricType]."', fQuality  = '".$post[fQuality]."'";
		$res = $this->executeQry($query);
		$inserted_Id = mysql_insert_id();
		
		if($res) {
			$this->logSuccessFail('1',$query);		
		} else {
			$this->logSuccessFail('0',$query);
		}	
		
		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $inserted_Id;
        $xmlArr[$xm]['section'] = "insert";
        $xm++;
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);   
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been added successfully.");
		header("Location:addFabricQuality.php");exit;	
		
	}
	
	    //Start : Get Result ================================================
    function getResultQuality($id) {				
		$query = "Select fq.*, fd.fabricName from ".TBL_FABRIC_QUALITY." as fq inner join ".TBL_FABRICDESC." as fd on 1 and fq.fId = fd.fabricId and id = '".$id."'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {				
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageFabricQuality.php');	
		}
	}
	
	 function changeQualityStatus($get) {
    	$xmlArr = array();
        $xm = 1;
		$status=$this->fetchValue(TBL_FABRIC_QUALITY,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive, 0";
		} else 	{
			$stat= 1;
			$status="Active, 1";
		}
						
		$sql = "UPDATE ".TBL_FABRIC_QUALITY." set status = '$stat' where id = '$get[id]'";
		
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
	
	
    //Start : Delete Single Value=================================
    function deleteQuality($get) {	
    	$xmlArr = array();
        $xm = 1;	
        
		$sql = "DELETE FROM  ".TBL_FABRIC_QUALITY." where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		
        $xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;
        		
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageFabricQuality.php?page=$post[page]&limit=$post[limit]';</script>";
	}
        
        
    //Start : Mulitple Operation=========================================
	function deleteAllQuality($post){							
		$xmlArr = array();
        $xm = 1;
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		
		if($post[action] == 'deleteselected'){					
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){				
					$sql = "DELETE FROM ".TBL_FABRIC_QUALITY." where id = '$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}			
		}		
	
		if($post[action] == 'enableall'){			
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRIC_QUALITY." set status ='1' where id='$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}

		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRIC_QUALITY." set status ='0' where id='$val'";
					$rst = $this->executeQry($sql);
					
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageFabricQuality.php?page=$post[page]';</script>";exit;
	}	
	
	
	
	
	
	
	
	
	
	
			
	//=====================================================================
	// Fabric Price Page 
	//=====================================================================
	//==========================View Fabric Price==========================        
    function valFabricPrice() {        						
		$cond = "1 and f.fId = fd.fabricId and fd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		//$cond = 1;
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND fd.fabricName LIKE '%$searchtxt%' OR f.type LIKE  '%$searchtxt%'";
		}
		
		$query = "select f.*,fd.fabricName from ".TBL_FABRICCHARGE." as f , ".TBL_FABRICDESC." as fd where $cond ";
		//$query = "select * from ".TBL_FABRICCHARGE." where ".$cond;
					
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]?$_GET[orderby]:"addDate";
			$order = $_GET[order]?$_GET[order]:"DESC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			$genTable = '';
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
				/*	echo "<pre>";
					print_r($line);
					echo "</pre>";exit;	*/				
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					$status=($line->status)?"Active":"Inactive";

					$genTable .= '<tr>
						<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
						<td>'.$i.'</td>
						<td>'.$this->fetchValue(TBL_FABRICDESC, "fabricName", "fabricId = ".$line->fId).'</td>
						<td>'.$this->fetchValue(TBL_FABRIC, "type", "id = ".$line->fId).'</td>
						<td>'.$this->fetchValue(TBL_PALLETDESC, "palletName", "palletId = ".$line->palletId).'</td>
						<td>'.$line->percent.'</td>';					
                    $genTable.='<td>';                    
					if($menuObj->checkEditPermission()) {							
					$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'manageFabricPrice\')" >'.$status.'</div>';
					}	
					$genTable .= '</td>';                                        
                                        
                    $genTable.='<td><a rel="shadowbox;width=705;height=425" title="'.stripslashes($line->fabricName).'" href="viewFabricPrice.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>';                    
                    $genTable.='<td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editFabricPrice.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td>';
                                        
                    $genTable.='<td>';
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageFabricPrice&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					}
					$genTable .= '</td>';
                                        
                    $genTable.='</tr>';
					$i++;	
				}
				switch($recordsPerPage){
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><span class="alert-red alert-icon">Sorry no record found.</span>';
		}	
		return $genTable;
	}	 
	
	//==================View Fabric Price===================
	function viewFabric($type) {		
		$fabric = '';		
		$query = "Select fc.*, fd.fabricName from ".TBL_FABRICCHARGE." as fc inner join ".TBL_FABRICDESC." as fd where 1 and fc.fId = fd.fabricId and fd.fabricId = '".$type."' and fd.langId = '".$_SESSION[DEFAULTLANGUAGE]."' order by fc.id";
		//$query = "Select f.id, f.isMain, f.type, fd.fabricName from ".TBL_FABRIC." as f, ".TBL_FABRICDESC." as fd where 1 and f.id = fd.fabricId and f.type = '".$type."' ";
    	$result = $this->executeQry($query);
    	$num = $this->getTotalRow($result);
    	$output = "";    	    	
    	if($num > 0) {    		
    		$output .= '<label>'.$this->fetchValue(TBL_FABRICDESC, "fabricName", " fabricId = '".$type."'").'</label><section>
		            		<div  style="width:37%; float:left;"><span style="font-size: 13px; margin-left: 25px;"><strong>Fabric Quality</strong></span>		            			
		            		</div>
		            		<div  style="width:10%; float:left;"><strong>Main-Fabric</strong>
		            		</div>
		            		<div  style="width:13%; float:left;"><strong>Percent</strong>
		            		</div>
		            		<div  style="width:23%; float:left;"><strong>Color Pallet of Particular fabric</strong>
		            		</div>
		            		</div>
		            		<div  style="width:5%; float:left;"><strong>Operation</strong>
		            		</div>
            			</section>';
    		while($row = $this->getResultObject($result)) {
    			// "<pre>";print_r($row); echo "</pre>"; exit;	    			    			
    			if($row->isMain == 1){
                	$status = "MAIN - Fabric ";
				} else {
                	$status = "";
                }   				
    		   	$output .="<section id='".$row->id."'><div style='width:37%; float:left;'>";
    			$output	.="<input type='text' name='fQuality_".$row->id."' id='fQuality".$row->id."' value='".$row->fQuality."' class='wel' style='width:250px;' /></div>";
    			$output .="<div style='width:10%; float:left;'>";
    			//$output .="<span style='font-size: 13px; margin-left:11px;'>".$status."</span></div>";
    			if($status) {
    				$output .="<span>".$status."</span></div>";    				
    			}else {
    				$output .="<input type='radio' name='default' value='".$row->id."' />".$status."</div>";
    			}
    			$output .="<div style='width:13%; float:left;'>";
    			if($row->isMain != 1) {
    				$output	.="<input type='text' name='percent_".$row->id."' id='percent_".$row->id."' value='".$row->percent."' class='wel' style='width:75px;' />";
    			} else {
    				$output .="<span style='font-size:13px'>100</span>";
    				$output	.="<input type='hidden' name='percent_".$row->id."' id='percent_".$row->id."' value='".$row->percent."' class='wel' style='width:75px;' />";
    			}
    			$output .="<span style='font-size:13px'> %</span></div>";
    			$output .="<div style='width:23%; float:left;'>";
    			$output .="<select name='colorPallet_".$row->id."' style='width:100%;'>";
				$output .="<option value=''>Select Color Pallet</option>";
				$output .= $this->getPallet($row->palletId);
				$output .="</select></div>";
				$output .="<div style='width:5%; float:left;'>";
				$output .="<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageFabricPrice&type=delete&id=".$row->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a></div></section>";
	   			//$output .="<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){deleteFabric('".$row->fId."');}else{}\" >Delete</a></div></section>";    			
    		}	    							
    	}
    	echo $output;
	} 
	
	//=============Add Fabric Price====================
    function addFabricPrice($post){
        //echo "<pre>";print_r($post); echo "</pre>";exit;
        $xmlArr = array();
        $xm = 1;
        
        $query = "insert into ".TBL_FABRICCHARGE." set fId = '".$post[fabricId]."', fQuality = '".$post[fQuality]."', palletId = '".$post[pallet]."', percent ='".$post[percent]."', status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."'";
        $res = $this->executeQry($query);
        $inserted_id = mysql_insert_id();

        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $inserted_id;
        $xmlArr[$xm]['section'] = "insert";
        $xm++;        		
		
		if($res) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
			
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Information has been added successfully.");			
		header("Location:addFabricPrice.php");exit;			
	}
	
	function editFabricCharges($post) {
		//~ echo "<pre>";
		//~ print_r($post);
		//~ echo "</pre>";exit;				
		
		if(isset($post['default'])) {
			$id = $post['default'];
			$result = $this->executeQry("Update ".TBL_FABRICCHARGE." set isMain = '0' where fId ='".$post[fabricId]."'");
			$result = $this->executeQry("Update ".TBL_FABRICCHARGE." set isMain = '1' where id = '".$id."'");
		}
		
		$_SESSION['SESS_MSG'] = "";				
		$sql = $this->executeQry("select * from ".TBL_FABRICCHARGE." where 1 and fId = '".$post[fabricId]."'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {                        
			while($line = $this->getResultObject($sql)){                                                
				//$query = "update ".TBL_DELIVERY_TIME." set days = '".$post['day_'.$line->id]."', pcs = '".$post['pcs_'.$line->id]."' WHERE id = '".$line->id."' ";                         
				
				 $query = "Update ".TBL_FABRICCHARGE." set fQuality ='".$post['fQuality_'.$line->id]."', palletId ='".$post['colorPallet_'.$line->id]."', percent = '".$post['percent_'.$line->id]."', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$line->id."'";
				if($this->executeQry($query)) {
					$this->logSuccessFail('1', $query);
					$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
				}else{
					$this->logSuccessFail('0', $query);						
				}
			}  
		}
	}
	
	//=============Edit Fabric Price================
	function editFabricPrice($post){
		//echo "<pre>";print_r($post); echo "</pre>";exit;
		$xmlArr = array();
        $xm = 1;
        
		$post = postwithoutspace($post);                
		
		if($_SESSION['SESS_MSG'] == "") {
			$query = "update ".TBL_FABRICCHARGE." set fId ='".$post['fabricName']."', palletId ='".$post[palletId]."', percent = '".$post[percent]."', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."'  where id = '$post[id]'";
			$xmlArr[$xm]['query'] = addslashes($query);
	        $xmlArr[$xm]['identification'] = $post[id];
	        $xmlArr[$xm]['section'] = "update";
	        $xm++;		
			if($this->executeQry($query)) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			}	
			$xmlInfoObj = new XmlInfo();
        	$xmlInfoObj->addXmlData($xmlArr);	
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Information has been updated successfully.!!!");	
            header('Location:manageFabricPrice.php');exit;
			//echo "<script language=javascript>window.location.href='manageFabric.php?page=$post[page]';</script>";exit;
	}

	function deleteFabricDiscount($id) {
		$query = "Delete from ".TBL_FABRICCHARGE." where fId = '".$id."'";
    	if($this->executeQry($query)) {
    		echo "success";
		}else{
			echo "fail";
    	}    	
    }  

	
    //Start : Delete Single Value=================================
    function deleteFabricPriceValue($get) {
    	$xmlArr = array();
        $xm = 1;		
		$sql = "DELETE FROM  ".TBL_FABRICCHARGE." where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;	
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}						
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
    	echo "<script language=javascript>window.location.href='manageFabricPrice.php?page=$post[page]&limit=$post[limit]';</script>";
	}
    
	//Start : Mulitple Operation=========================================
	function deleteAllFabricPriceValues($post){		
		$xmlArr = array();
        $xm = 1;	
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageFabricPrice.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}		
		
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){										
					$sql = "DELETE FROM ".TBL_FABRICCHARGE." where id = '$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}										
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRICCHARGE." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "update";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}

		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_FABRICCHARGE." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					$rst = $this->executeQry($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
			        $xmlArr[$xm]['identification'] = $val;
			        $xmlArr[$xm]['section'] = "delete";
			        $xm++;	
					if($rst){
						$this->logSuccessFail("1",$sql);
					}else{
						$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		echo "<script language=javascript>window.location.href='manageFabricPrice.php?page=$post[page]';</script>";exit;
	}	
	
	//==================Status====================	
	function changeFabricPriceValueStatus($get) {	
		$xmlArr = array();
        $xm = 1;	
		$status=$this->fetchValue(TBL_FABRICCHARGE,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive, 0";
		} else 	{
			$stat= 1;
			$status="Active, 1";
		}
		$sql = "UPDATE ".TBL_FABRICCHARGE." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		$rst = $this->executeQry($sql);
		$xmlArr[$xm]['query'] = addslashes($sql);
        $xmlArr[$xm]['identification'] = $get[id];
        $xmlArr[$xm]['section'] = "update";
        $xm++;	
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
		if($rst){
			$this->logSuccessFail("1",$sql);
		}else{
			$this->logSuccessFail("0",$sql);
		}
		echo $status;		
	}
	
	//==================Get Result ==================
    function getFabricPriceResult($id) {
		$query= "select * from ".TBL_FABRICCHARGE." where id = '".$id."'";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		if($num > 0) {				
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageFabricPrice.php');	
		}
	}
    
	//==============Get list of Fabric Type============
 	function getFabricType($id = '') { 		 		
		$sql = $this->executeQry("select f.*, fd.fabricName from ".TBL_FABRIC." as f, ".TBL_FABRICDESC." as fd where 1 and f.id = fd.fabricId and fd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {
				($id == $line->id) ? $sel ="selected='selected'" : $sel ="";				
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->fabricName.'</option>';	
			}
		}		
        return $genTable;
	}	
		
	//==============Get Fabric Quality============
 	function getFabricQuality($id = '') {
		$sql = $this->executeQry("select * from ".TBL_FABRIC_QUALITY." WHERE 1");	
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {
				($id == $line->id) ? $sel ="selected='selected'" : $sel ="";				
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->fQuality.'</option>';	
			}
		}		
        return $genTable;
	}	
 	
	//==============Get list of Pallet Name============
	function getPallet($palletId = '') {
		$sql = $this->executeQry("select p.*, pd.palletName from ".TBL_PALLET." as p, ".TBL_PALLETDESC." as pd where 1 and p.id = pd.palletId and status = '1' and isDeleted = '0' and pd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
		$num = $this->getTotalRow($sql);
		$sel = "";
		if($num > 0) {			
			while($line = $this->getResultObject($sql)) {
				($palletId == $line->id) ? $sel ="selected='selected'" : $sel ="";				
				$genTable .= '<option value="'.$line->id.'" '.$sel.'>'.$line->palletName.'</option>';	
			}
		}		
        return $genTable;	
	}
	
	//==============Show Type on Name change==========
	function getFabricTypeOnNameChange($fId){
		$sql = "SELECT type FROM ".TBL_FABRIC." WHERE 1 and id =".$fId; 
		$rst = $this->executeQry($sql);		
		$row = $this->getResultObject($rst);
		$type = $row->type;
		echo $type;
	}
	
}// End Class
?>	

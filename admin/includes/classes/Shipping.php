<?php
session_start();
class Shipping extends MySqlDriver{
	function __construct() {
		$this->obj = new MySqlDriver;
	}

	function valDetail()
	{
		//$cond = "1 and ft.id = ftd.styleId  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and ftd.name!=''";

		//if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != 'searchtext'){
			//$searchtxt = $_REQUEST['searchtxt'];
			//$cond .= " AND (ftd.name LIKE '%$searchtxt%' ) ";
		//}
 
		$query = "SELECT * FROM ".TBL_SHIPPING_METHOD." AS SM INNER JOIN ".TBL_COUNTRY_DESCRIPTION." AS CD ON(SM.countryId=CD.countryId) WHERE 1 $cond";

		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			//-------------------------Paging----------------------- -------------------------
			$paging = $this->paging($query);
			$this->setLimit($_GET[limit]);
			$recordsPerPage = $this->getLimit();
			$offset = $this->getOffset($_GET["page"]);
			$this->setStyle("redheading");
			$this->setActiveStyle("smallheading");
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"id";
			$order = $_GET[order]? $_GET[order]:"ASC";
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query);
			$row = $this->getTotalRow($rst);
			if($row > 0) {
				$i = 1;
				while($line = $this->getResultObject($rst)) {
					$currentOrder .=$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
					$status = 'Inactive';
					else
					$status = 'Active';
					if($line->minPairsForFreeShipping) $freeShipVal = $line->minPairsForFreeShipping;
					else {$freeShipVal= 'n/a';} 
					$genTable .= '<tr class="dragbox-content '.$highlight.'" id="'.$line->id.'" >
						<th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>
						<td>'.$i.'</td>
						<td>'.$line->id.'</td>
						<td>'.$line->countryName.'</td>
						<td>'.$line->firstPairCost.'</td>
						<td>'.$line->additionalPairCost.'</td>
						<td>'.$freeShipVal.'</td>';
						
					if($menuObj->checkEditPermission())
					$genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'Shipping\')">'.$status.'</div></td>';

					if($menuObj->checkEditPermission())
					$genTable .= '<td><a class="i_pencil edit" href="editShipping.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a></td>';

					if($menuObj->checkDeletePermission())
					$genTable .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'onClick=\"if(confirm('Are you sure to delete this record.')){window.location.href='pass.php?action=Shipping&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete </a>";
					$genTable .= '</td>
					</tr>';
					$i++;
				}
				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display&nbsp;<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select>Records Per Page
				</td> <td class='page_info' align='center' width='200'><input type='hidden' name='page' value='".$currpage."'>Total Records Found : ".$totalrecords."</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry! No Record Found</div>';
		}
		return $genTable;
	}

	function isCountryExists($countryId)
	{
		if($countryId) {
			$sql= "SELECT id FROM ".TBL_SHIPPING_METHOD." WHERE countryId='".$countryId."'";
			$res = $this->executeQry($sql);
			return $this->getTotalRow($res);
		}
	}
	function isCountryExistsEdit($newCntryId, $oldCntryId)
	{
		//echo $newCntryId.'ras'.$oldCntryId;
		if(!empty($newCntryId) && !empty($oldCntryId)) {
			$sql= "SELECT id FROM ".TBL_SHIPPING_METHOD." WHERE countryId='".$newCntryId."' AND countryId!='".$oldCntryId."'";
			$res = $this->executeQry($sql);
			return $this->getTotalRow($res);
		}
		return 1;
	}
	/// For Add New Forum Topic

	function addRecord($post)
	{
		//var_dump($post);die;
		if($post['freeShipping'] == 1 ) $freeShipVal = $post['minPairsForFreeShipping'];
		else $freeShipVal = $post['freeShipping'];

		$query = "insert into ".TBL_SHIPPING_METHOD." set  countryId='".$post['countryId']."',firstPairCost='".$post['firstPairCost']."',additionalPairCost='".$post['additionalPairCost']."',minPairsForFreeShipping='".$freeShipVal."',status= '1',addDate='NOW()', addedBy='".$_SESSION['ADMIN_ID']."'";
		$res = $this->executeQry($query);
		//$inserted_id = mysql_insert_id();

		if($res) {
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
			$this->logSuccessFail('1',$query);
		}
		else {
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","Opps! Problem saving your information,try again later.");
			$this->logSuccessFail('0',$query);
		}
			
		/*if($inserted_id)	$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		 else $_SESSION['SESS_MSG'] =  msgSuccessFail("fail","Opps! Problem saving your information,try again later.");*/
		header("Location:addShipping.php");exit;
	}

	//edit style
	function editRecord($post)
	{
		//var_dump($post);die;
		if($post['freeShipping'] == 1 ) $freeShipVal = $post['minPairsForFreeShipping'];
		else $freeShipVal = $post['freeShipping'];

		echo $query = "update ".TBL_SHIPPING_METHOD." set countryId='".$post['countryId']."',firstPairCost='".$post['firstPairCost']."',additionalPairCost='".$post['additionalPairCost']."',minPairsForFreeShipping='".$freeShipVal."',status= '1',modDate='NOW()', modBy='".$_SESSION['ADMIN_ID']."' WHERE id='".$post['id']."'";
		$res = $this->executeQry($query);
		//$inserted_id = mysql_insert_id();

		if($res) {
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
			$this->logSuccessFail('1',$query);
		}
		else {
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","Opps! Problem saving your information,try again later.");
			$this->logSuccessFail('0',$query);
		}
			
		/*if($inserted_id)	$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		 else $_SESSION['SESS_MSG'] =  msgSuccessFail("fail","Opps! Problem saving your information,try again later.");*/
		header("Location:manageShipping.php");
		echo "<script>window.location.href='manageShipping.php?page=$post[page]';</script>";
		exit;
	}

	/// For Change Topic Status
	function changeValueStatus($get)
	{
		$status=$this->fetchValue(TBL_SHIPPING_METHOD,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$query = "update ".TBL_SHIPPING_METHOD." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";

		if($this->executeQry($query)) $this->logSuccessFail('1',$query);
		else 	$this->logSuccessFail('0',$query);
		echo $status;
	}

	function findMaxSequence()
	{
		$query="SELECT max(sequence) as maxsequence FROM ".TBL_STYLE." ";
		$res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}

	function deleteAllValues($post)
	{
		if(($post[action] == ''))
		{
			$_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageShipping.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec > 0){
				foreach($delres as $key => $val){
					$queryXml = "DELETE FROM ".TBL_SHIPPING_METHOD." WHERE id='".$val."' ";
					$this->executeQry($queryXml);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall')
		{
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0)
			{
				foreach($delres as $key => $val)
				{
					$sql="update ".TBL_SHIPPING_METHOD." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall')
		{
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0)
			{
				foreach($delres as $key => $val)
				{
					$sql="update ".TBL_SHIPPING_METHOD." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageShipping.php?page=$post[page]';</script>";
	}


	/// For Delete Single Forum Topic

	function deleteValue($get)
	{
		$queryXml = "DELETE FROM ".TBL_SHIPPING_METHOD." WHERE id='".$get['id']."' ";
		$this->executeQry($queryXml);
		//$result1=$this->deleteRec(TBL_STYLEDESC,"styleId='".$get['id']."'");
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
		echo "<script language=javascript>window.location.href='manageShipping.php?page=$get[page]&limit=$get[limit]';</script>";
	}

	/// Get Information About Existing color
	function getResult($id)
	{
		$sql = $this->executeQry("select * from ".TBL_SHIPPING_METHOD." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0)
		{
			return $line = $this->getResultObject($sql);
		}
		else
		{
			redirect("manageShipping.php");
		}
	}

	/*
	 function checkCategoryExist($cid) {
		if($cid) {
		if($cid >= 0 && is_numeric($cid)) {
		if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_TBL_CATEGORY'") == 1) {
		$getCid = $this->fetchValue(TBL_CATEGORY,"id","1 and id = ".(int)$cid."");
		if(!$getCid)
		redirect('manageCategory.php');
		} else {
		redirect('manageCategory.php');
		}
		} else {
		redirect('manageCategory.php');
		}
		}
		}

		*/

}// End Class
?>
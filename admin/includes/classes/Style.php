<?php 
session_start();
class Style extends MySqlDriver{
	private $styleType = array("single layer sole"=>"Single Layer Sole","triple layer sole"=>"Triple Layer Sole");
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	
	function valDetail() {
	
		$cond = "1 and ft.id = ftd.styleId  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and ftd.name!=''";
		
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != 'searchtext'){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (ftd.name LIKE '%$searchtxt%' ) ";
		}
		
		$query = "select ft.*,ftd.name from ".TBL_STYLE." as ft , ".TBL_STYLEDESC." as ftd where $cond ";

		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging----------------------- -------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
		    $orderby = $_GET[orderby]? $_GET[orderby]:"id";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
		    $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;	
				while($line = $this->getResultObject($rst)) {
				   	$currentOrder .=$line->id.","; 
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0) $status = 'Inactive';
					else $status = 'Active';
					$chk = '';
					if($line->isDefault == '1') $chk = 'checked';
						
					$genTable .= '<tr class="dragbox-content '.$highlight.'" id="'.$line->id.'" >';
					$genTable .= '<th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>';
					$genTable .= '<td>'.$i.'</td>';
					$genTable .= '<td>'.$line->id.'</td>';
					$genTable .= '<td>'.stripslashes($line->name).'</td>';
					$genTable .= '<td>'.stripslashes($line->addPricePerPair).'</td>';
					$genTable .= '<td><input type="radio" name="isDefault" value="'.$line->id.'" '.$chk.' onClick="updateStyleDefault(\''.$line->id.'\');" /></td>';
									
					if($menuObj->checkEditPermission()) {
						if($chk) {
							$genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" onClick="alert(\'Sorry! you can not alter a default record!\')">'.$status.'</div></td>';
						}
						else {
							$genTable .= '<td><div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'Style\')">'.$status.'</div></td>';
						}
					}
						
					if($menuObj->checkEditPermission()) 
						$genTable .= '<td><a class="i_pencil edit" href="editStyle.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a></td>';
						
					if($menuObj->checkDeletePermission()) {
						if($chk) {
							$genTable .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);' onClick=\"alert('Sorry! you can not alter a default record!');\" >Delete </a></td>";
						}
						else {
							$genTable .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'onClick=\"if(confirm('Are you sure to delete this record.')){window.location.href='pass.php?action=Style&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete </a></td>";
					    }
					}
					$genTable .= '</tr>';
					$i++;	
				}
					$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display&nbsp;<select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select>Records Per Page
				</td> <td class='page_info' align='center' width='200'><input type='hidden' name='page' value='".$currpage."'>Total Records Found : ".$totalrecords."</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry! No Record Found</div>';
		}	
		return $genTable;
	}
	
	/// For Add New Forum Topic
	function updateDefault($get)
	{
		if($get['sid']) {
			if($this->executeQry("UPDATE ".TBL_STYLE." SET isDefault='0'")) {
				$this->executeQry("UPDATE ".TBL_STYLE." SET isDefault='1',status='1' WHERE id='".$get['sid']."'");
				echo '';
			}
		}	
	}
	
	function getStyleType($st=0)
	{
		$tbl = '';
		foreach($this->styleType as $key=>$val)
		{
			$sel ='';
			if($st == $key)  $sel = 'selected';
			$tbl .= '<option value="'.$key.'" '.$sel.'>'.$val.'</option>';
		}
		return $tbl;
	}
	
	function addRecord($post)
	{
		
		$query = "insert into ".TBL_STYLE." set type='".$post['styleType']."',status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',addPricePerPair='".$post['addPrice']."'";
		$res = $this->executeQry($query);
		$inserted_id = mysql_insert_id();
		
		if($res) $this->logSuccessFail('1',$query);		
		else 	$this->logSuccessFail('0',$query);
			
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num)
		{			
			while($line = $this->getResultObject($rst))
			{					
				$styleName = 'styleName_'.$line->id;
				$query = "insert into ".TBL_STYLEDESC." set styleId  = '$inserted_id', langId = '".$line->id."',name = '".addslashes($post[$styleName])."'";
	
				if($this->executeQry($query))	$this->logSuccessFail('1',$query);		
				else 	$this->logSuccessFail('0',$query);
			}	
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		}	
		header("Location:addStyle.php");exit;							
	}
	
	//edit style	
	function editRecord($post)
	{
		$query = "UPDATE ".TBL_STYLE." SET type='".$post['styleType']."',modDate = '".date('Y-m-d')."',modBy = '".$_SESSION['ADMIN_ID']."',addPricePerPair='".$post['addPrice']."' WHERE id='".$post['id']."'";
		$res = $this->executeQry($query);
		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num)
		{			
			while($line = $this->getResultObject($rst))
			{					
				$styleName = 'name_'.$line->id;
				$sql = $this->selectQry(TBL_STYLEDESC,'1 and styleId = "'.$post['id'].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0)
				{ 
					$query = "insert into ".TBL_STYLEDESC." set styleId = '".$post[id]."', langId = '".$line->id."', name = '".addslashes($post[$styleName])."'";
					
					if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);	
				}
				else 
				{
					$query = "update ".TBL_STYLEDESC." set  name = '".addslashes($post[$styleName])."'  where 1 and styleId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query))	$this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);	
				}	
			}	
		}
		echo "<script>window.location.href='manageStyle.php?page=$post[page]';</script>";
		exit();
	}
	
	/// For Change Topic Status
	function changeValueStatus($get)
	{
		$status=$this->fetchValue(TBL_STYLE,"status","1 and id = '$get[id]'");
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
		$query = "update ".TBL_STYLE." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		
		if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
		else 	$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function findMaxSequence()
	{
		$query="SELECT max(sequence) as maxsequence FROM ".TBL_STYLE." ";
                $res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}
	
	function deleteAllValues($post)
	{
		if(($post[action] == ''))
		{
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			 echo "<script language=javascript>window.location.href='manageStyle.php?page=$post[page]&limit=$post[limit]';</script>";
			 exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec > 0){
				foreach($delres as $key => $val){
				
					$queryXml = "DELETE FROM ".TBL_STYLE." WHERE id='".$val."' ";
					$this->executeQry($queryXml);
					
					$result1=$this->deleteRec(TBL_STYLEDESC,"styleId='".$val."'");	
					
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First se		lect the record!!!");
			}
		}
		if($post[action] == 'enableall')
		{
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0)
			{
				foreach($delres as $key => $val)
				{
					$sql="update ".TBL_STYLE." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall')
		{
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0)
			{
				foreach($delres as $key => $val)
				{
					$sql="update ".TBL_STYLE." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageStyle.php?page=$post[page]';</script>";
	}
	
	
	/// For Delete Single Forum Topic
	
	function deleteValue($get)
	{
		$queryXml = "DELETE FROM ".TBL_STYLE." WHERE id='".$get['id']."' ";
		$this->executeQry($queryXml);
		//$result=$this->deleteRec(TBL_SIZE,"id='".$get['id']."'");	
		$result1=$this->deleteRec(TBL_STYLEDESC,"styleId='".$get['id']."'");
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageStyle.php?page=$get[page]&limit=$get[limit]';</script>";
	}
	
	/// Get Information About Existing color
	function getResult($id)
	{
		$sql = $this->executeQry("select * from ".TBL_STYLE." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0)
		{
			return $line = $this->getResultObject($sql);	
		}
		else
		{
			redirect("manageStyle.php");
		}	
	}

	/*
	function checkCategoryExist($cid) {
		if($cid) {
			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_TBL_CATEGORY'") == 1) {
					$getCid = $this->fetchValue(TBL_CATEGORY,"id","1 and id = ".(int)$cid."");
					if(!$getCid)
						redirect('manageCategory.php');		
				} else {
					redirect('manageCategory.php');					
				}
			} else {
				redirect('manageCategory.php');
			}
		}
	}
	
	*/
	
}// End Class
?>	
<?php

session_start();

class Country extends MySqlDriver {

    function __construct() {

        $this->obj = new MySqlDriver;
    }

// / SHOW DETAIL ON MANAGE PAGE==================done

    function valDetail() {

        $cond = "1 and " . TBL_COUNTRY . ".id = " . TBL_COUNTRY_DESCRIPTION . ".countryId and " . TBL_COUNTRY . ".isDeleted = '0' and " . TBL_COUNTRY_DESCRIPTION . ".langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != 'searchtext') {

            $searchtxt = $_REQUEST['searchtxt'];

            $cond .= " AND (" . TBL_COUNTRY_DESCRIPTION . ".countryName LIKE '%$searchtxt%' or " . TBL_COUNTRY . ".countryCode LIKE '%$searchtxt%')";
        }

        $query = "select " . TBL_COUNTRY . ".*," . TBL_COUNTRY_DESCRIPTION . ".countryName from " . TBL_COUNTRY . " , " . TBL_COUNTRY_DESCRIPTION . " where $cond ";

        //echo $query;exit;

        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging----------------------- -------------------------			
            $paging = $this->paging($query);
            $limit = $_GET[limit]?$_GET[limit]:25;
            $this->setLimit($limit);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : "" . TBL_COUNTRY . ".id";
            $order = $_GET[order] ? $_GET[order] : "desc";
            $query .= " ORDER BY $orderby $order LIMIT " . $offset . ", " . $recordsPerPage;

            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);

            if ($row > 0) {
                $i = 1;
                while ($line = $this->getResultObject($rst)) {
                    //echo "<pre>"; print_r($line);exit;
                    $currentOrder .= $line->id . ",";
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    if ($line->status == 0)
                        $status = 'Inavtive';
                    else
                        $status = 'Active';

                    $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "1 and countryId = '" . $line->id . "' and langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'");

                    $genTable .= '<tr class="' . $highlight . '" id="' . $line->id . '" >
								 	<th><input name="chk[]" value="' . $line->id . '" type="checkbox"></th>
									<td>' . $i . '</td>
									<td>' . $line->id . '</td>
									<td>' . stripslashes($countryName) . '</td>
									<td>' . stripslashes($line->country_2_code) . '</td>	
									<td><img src="' . FLAGPATH . $line->flag . '"</td>';


                    $genTable.='<td>';
                    if ($menuObj->checkEditPermission())
                        $genTable .= '<div id="' . $div_id . '" style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'country\')">' . $status . '</div>';
                    $genTable .= '</td>';

                    $genTable .= '<td><a rel="shadowbox;width=705;height=325" title="' . $categoryTitle . '" href="viewCountry.php?id=' . base64_encode($line->id) . '"><img src="images/view.png" border="0"></a></td>';

                    $genTable.='<td>';
                    if ($menuObj->checkEditPermission())
                        $genTable .= '<a class="i_pencil edit" href="editCountry.php?id=' . base64_encode($line->id) . '&page=' . $page . '">' . LANG_EDIT . '</a>';
                    $genTable .= '</td>';

                    $genTable.='<td>';
                    if ($menuObj->checkDeletePermission())
                        $genTable .= "<a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are You Sure To Delete This Record?')){window.location.href='pass.php?action=country&type=delete&cid=" . $cid . "&id=" . $line->id . "&page=$page'}else{}\" > " . LANG_DELETE . " </a>";
                    $genTable .= '</td> 
					
					</tr>';
                    $i++;
                }
                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="' . $currentOrder . '" /></div>';

                switch ($recordsPerPage) {
                    case 25:
                        $sel1 = "selected='selected'";
                        break;
                    case 50:
                        $sel2 = "selected='selected'";
                        break;
                    case 75:
                        $sel3 = "selected='selected'";
                        break;
                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display&nbsp; <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='25' $sel1>25</option>
					<option value='50' $sel2>50</option>
					<option value='75' $sel3>75</option> 
					<option value='" . $totalrecords . "' $sel4>All</option>  
					  </select> Records per page
				</td> <td class='page_info' align='center' width='200'><input type='hidden' name='page' value='" . $currpage . "'>Total Records Found :" . $totalrecords . "</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table></div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry! No Record Found.</div>';
        }
        return $genTable;
    }

/// CHANGE VALUE STATUS=================================================================================done

    function changeValueStatus($get) {

        /*if ($this->checkOperation($get[id])) {
            echo"Operation Aborted :Data is being used by another process.";
            exit;
        } else {*/
            $status = $this->fetchValue(TBL_COUNTRY, "status", "1 and id = '$get[id]'");
            if ($status == 1) {
                $stat = 0;
                $status = "Inactive,0";
            } else {
                $stat = 1;
                $status = "Active,1";
            }
            $query = "update " . TBL_COUNTRY . " set status = '$stat', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";
            $this->executeQry($query);
            echo $status;
        //}
    }

/// DELETE VALUE : SINGLE==============================================================================Done	

    function deleteValue($get) {

        /*if ($this->checkOperation($get['id'])) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Operation Aborted :Data is being used by another process.");
        } else {*/
			if($get['id']){        
            $this->executeQry("update " . TBL_COUNTRY . " set isDeleted = '1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'");
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Your information has been deleted successfully!');
        }
        echo "<script language=javascript>window.location.href='manageCountry.php?page=$get[page]&limit=$get[limit]';</script>";
        exit;
    }

///  ADD RECORD=======================================================================================Done	

    function addRecord($post, $file) {
        //echo"<pre>"; print_r($post); print_r($file);echo "</pre>"; exit;

        $_SESSION['SESS_MSG'] = "";
        if ($file['flag']['name']) {
            $filename = stripslashes($file['flag']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);
            $image_name = date("Ymdhis") . time() . rand() . '.' . $extension;
            $target = FLAGORIGINAL . $image_name; 
            $filestatus = move_uploaded_file($file['flag']['tmp_name'], $target);
            //echo $filestatus;exit;
            chmod($target, 0777);
            if ($filestatus) {
                $imgSource = $target;
                $LargeImage = FLAGORIGINAL . $image_name;
                $ThumbImage = FLAGTHUMB . $image_name;
                chmod(FLAGORIGINAL, 0777);
                chmod(FLAGTHUMB, 0777);
                exec(IMAGEMAGICPATH . " $imgSource -thumbnail 16x16 $ThumbImage");
                unlink($target);
                //$oldimg = $this->fetchValue(TBL_COUNTRY,"flag","id='$post[id]'");
                //if(file_exists(FLAGTHUMB.$oldimg)){
                //unlink(FLAGTHUMB.$oldimg);
                //}				
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'Opps! Some error uploading flag.');
            }
        }

        if ($_SESSION['SESS_MSG'] == "") {
        	//echo 'rashd';die;

            $query = "insert into " . TBL_COUNTRY . " set countryCode = '$post[countryCode]',	country_2_code = '$post[countryCode2]',  flag='$image_name' ,status = '1', addDate = '" . date('Y-m-d H:i:s') . "', addedBy = '" . $_SESSION['ADMIN_ID'] . "'";
            $sql = $this->executeQry($query);
            $inserted_id = mysql_insert_id();
            if ($inserted_id)
                $this->logSuccessFail('1', $query);
            else
                $this->logSuccessFail('0', $query);

            $query = "insert into " . TBL_COUNTRY_DESCRIPTION . " set countryId = '$inserted_id', langId = '" .$_SESSION['DEFAULTLANGUAGE']. "', countryName = '" . addslashes($post['countryName']) . "'";

            if ($this->executeQry($query))
                $this->logSuccessFail('1', $query);
            else
                $this->logSuccessFail('0', $query);

            $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Information has been updated successfully!');
        }

        //header("Location:addCountry.php");exit;	
        echo "<script>window.location.href='addCountry.php';</script>";
        exit;
    }

/// EDIT RECORD ========================================================================================Done	


    function editRecord($post, $file) {
        //echo"<pre>"; print_r($post);print_r($file); echo "</pre>"; exit;

        $_SESSION['SESS_MSG'] = "";
        $image_name = "";
        if ($file['flag']['name']) {
            $filename = stripslashes($file['flag']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);
            $image_name = date("Ymdhis") . time() . rand() . '.' . $extension;

            $target = FLAGORIGINAL . $image_name;
            //echo $target;exit;

            $filestatus = move_uploaded_file($file['flag']['tmp_name'], $target);
            $filestatus;
            @chmod($target, 0777);
            if ($filestatus) {
                $imgSource = $target;
                $LargeImage = FLAGORIGINAL . $image_name;
                $ThumbImage = FLAGTHUMB . $image_name;

                @chmod(FLAGORIGINAL, 0777);
                @chmod(FLAGTHUMB, 0777);
                exec(IMAGEMAGICPATH . " $imgSource -thumbnail 16x16 $ThumbImage");

                @unlink($target);
                $oldimg = $this->fetchValue(TBL_COUNTRY, "flag", "id='$post[id]'");
                if (file_exists(FLAGTHUMB . $oldimg)) {
                    @unlink(FLAGTHUMB . $oldimg);
                }
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'Opps! Some error uploading flag');
            }
        }

        if ($_SESSION['SESS_MSG'] == "") {
            $query = "update " . TBL_COUNTRY_DESCRIPTION . " set countryName = '" . addslashes($post['countryName']) . "' where 1 and countryId = '$post[id]'";
            if ($this->executeQry($query))
                $this->logSuccessFail('1', $query);
            else
                $this->logSuccessFail('0', $query);


            if ($image_name == "") {
                $query = "update " . TBL_COUNTRY . " set countryCode = '$post[countryCode]', country_2_code = '$post[countryCode2]', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '" . $post[id] . "'";
            } else {
                $query = "update " . TBL_COUNTRY . " set countryCode = '$post[countryCode]', country_2_code = '$post[countryCode2]', flag='" . $image_name . "' ,modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '" . $post[id] . "'";
            }



            //echo $query;exit;
            if ($this->executeQry($query))
                $this->logSuccessFail('1', $query);
            else
                $this->logSuccessFail('0', $query);

            $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Record has been updated successfully!');
        }


        echo "<script>window.location.href='manageCountry.php?page=$post[page]';</script>";
        exit;
    }

///  OPTERATION : DELETE/ACTIVE/INACTIVE ON MULTIPLE VALUEWS============================================Done

    function deleteAllValues($post) {

        if (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'First select action or record and then submit.');
            echo "<script language=javascript>window.location.href='manageCountry.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }

        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                   /* if ($this->checkOperation($val)) {
                        $_SESSION['SESS_MSG'] = msgSuccessFail("fail", " All checked record(s) are not deleted because some of them are used by other processes.");
                    } else {*/
                        $this->executeQry("update ".TBL_COUNTRY." set isDeleted = '1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$val'");
//                    }
                }
                if($_SESSION['SESS_MSG'] == ""){
                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Your all selected information has been deleted successfully');
                }
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'First select record');
            }
        }

        if ($post[action] == 'enableall') {

            $delres = $post[chk];

            $numrec = count($delres);

            if ($numrec > 0) {

                foreach ($delres as $key => $val) {

                    //$result=$this->deleteRec(LOANTBL_COUNTRY,"cat_id='$val'");	

                    $sql = "update " . TBL_COUNTRY . " set status ='1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";

                    mysql_query($sql);
                }

                $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Selected record(s) have been enabled successfully!');
            } else {

                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'First select record(s).');
            }
        }

        if ($post[action] == 'disableall') {

            $delres = $post[chk];

            $numrec = count($delres);

            if ($numrec > 0) {

                foreach ($delres as $key => $val) {

               /*if($this->checkOperation($val)){
                    $_SESSION['SESS_MSG'] =msgSuccessFail("fail"," All checked record(s) are not disableall because some of them are used by other processes.");
               }*/
               //else{    	  
                    $sql = "update " . TBL_COUNTRY . " set status ='0', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    mysql_query($sql);
                    //}
                }
           if($_SESSION['SESS_MSG']==""){
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", 'Selected record(s) have been disabled successfully!');
           }
            }else{
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", 'First select record(s)');
            }
        }
        echo "<script language=javascript>window.location.href='manageCountry.php?cid=$post[cid]&page=$post[page]';</script>";
        exit;
    }

///  GET RESULT BY ID==================================================================================Done
    function getResult($id) {
        $query = "Select c.*,cd.countryName from " . TBL_COUNTRY . " as c inner join " . TBL_COUNTRY_DESCRIPTION . " as cd on (c.id=cd.countryId) where c.id='$id'";

        $sql = $this->executeQry($query);

        $num = $this->getTotalRow($sql);

        if ($num > 0) {

            return $line = $this->getResultObject($sql);
        } else {

            redirect("manageCountry.php");
        }
    }

///  CHECK COUNTRY NAME EXISTANCE========================================================================


    function isCountryNameExist($countryname, $langId, $id='') {

        $countryname = trim($countryname);

        if ($id)
            $con = "  and c.id!='" . $id . "' and c.isDeleted='0'";
        else
            $con = " and c.isdeleted='0'";

        $query = "select c.*,cd.countryName from " . TBL_COUNTRY . " as c left join " . TBL_COUNTRY_DESCRIPTION . " as cd on cd.langId = '" . $langId . "' and cd.countryName='$countryname' where  cd.countryId=c.id $con ";

        $rst = $this->executeQry($query);

        $row = $this->getTotalRow($rst);

        return $row;
    }

    function checkOperation($id) {      
       $query = "SELECT C.id FROM ".TBL_COUNTRY." AS C, ".TBL_ORDER." AS O, ".TBL_FULFILLMENT." AS F, ".TBL_USER." AS U, ".TBL_STATE." AS S WHERE 
                    (((O.billCountryId=$id) OR (O.shipCountryId=$id)) OR (F.countryId=$id) OR ( (U.countryId=$id) OR (U.shipCountryId=$id)) OR 
                        (S.countryId=$id)) AND F.isdeleted='0' AND S.isDeleted='0' LIMIT 0,1 ";
        $rst = $this->executeQry($query);
        $row = $this->getTotalRow($rst);
        if ($row > 0) {
            return 1;
        }
        return 0;
    }

}

// End Class
?>	

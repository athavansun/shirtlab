<?php
session_start();
class View extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {
		$genObj = new GeneralFunctions();
		$cond = "1 and ft.id = ftd.viewId  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select ft.*,ftd.viewName from ".TBL_VIEW." as ft , ".TBL_VIEWDESC." as ftd where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			
			if($row > 0) {			
				$i = 1;	
				while($line = $this->getResultObject($rst)) {
				    $currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
												
						$genTable .= '<tr class="dragbox-content '.$highlight.'" id="'.$line->id.'" >
					                        <th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>
					                        <td>'.$i.'</td>
					                         <td>'.$line->id.'</td>
									         <td>'.substr($line->viewName, 0,40 ).'</td>
									         <!--<td>'.$genObj->displayPrice($line->price).'</td>--><td>';
									
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'view\')">'.$status.'</div>';
									
																											
					$genTable .= '</td>
					<td><a rel="shadowbox;width=705;height=325" title="'.$line->viewName.'" href="viewView.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>
					<td>';
									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a class="i_pencil edit" href="editView.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
						
					$genTable .= '</td><td>';
				
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Forum Topic?')){window.location.href='pass.php?action=view&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					
							
					$genTable .= '</td></tr>';
				
		
					$i++;	
				}

					$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	
	
	
	/// For Add New Forum Topic
	
	
	function addRecord($post) {		
  	    $query = "insert into ".TBL_VIEW." set status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."', price = '".$post[price]."' , sequence = '".$post['maxsequence']."'";
			$res = $this->executeQry($query);
			$inserted_id = mysql_insert_id();
			
			if($res) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$viewName = 'viewName_'.$line->id;
					$query = "insert into ".TBL_VIEWDESC." set viewId = '$inserted_id', langId = '".$line->id."', viewName = '".addslashes($post[$viewName])."'";	
					$this->executeQry($query);
				}
			}
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
			header("Location:addView.php");exit;
		}
	
	//// check for existing topic
	
	function isViewExist($view,$langId,$id=''){
	   
		$view = trim($view);
		$rst = $this->selectQry(TBL_VIEWDESC,"langId = '".$langId."' and viewName ='".addslashes($view)."' AND viewId!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
	/// For Change Topic Status
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_VIEW,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
	
		$query = "update ".TBL_VIEW." set status = '$stat', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageView.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sequence = $this->fetchValue(TBL_VIEW,'sequence',"id='".$val."'");
				    $result=$this->deleteRec(TBL_VIEW,"id='".$val."'");	
					$result1=$this->deleteRec(TBL_VIEWDESC,"viewId='".$val."'");	
					$query = "update ".TBL_VIEW." set sequence = sequence - 1 where sequence > '".$sequence."'";
					$res = $this->executeQry($query);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_VIEW." set status ='1', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_VIEW." set status ='0', modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageView.php?page=$post[page]';</script>";
	}
	
	
	/// For Delete Single view
	
	function deleteValue($get) {
	    $sequence = $this->fetchValue(TBL_VIEW,'sequence',"id='".$get['id']."'");
		$result=$this->deleteRec(TBL_VIEW,"id='".$get['id']."'");	
		$result1=$this->deleteRec(TBL_VIEWDESC,"viewId='".$get['id']."'");
		$query = "update ".TBL_VIEW." set sequence = sequence - 1 where sequence > '".$sequence."'";
        $res = $this->executeQry($query)	;	
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageView.php?page=$get[page]&limit=$get[limit]';</script>";
	}
	
	/// Get Information About Existing Forum Topic
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_VIEW." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageView.php");
		}	
	}
	
	// Edit Forum Topic
		
		function editRecord($post) {
		
		$query = "update ".TBL_VIEW." set price = '".$post[price]."' , modDate = '".date('Y-m-d')."', modBy = '".$_SESSION['ADMIN_ID']."' where 1 and id = '$post[id]' ";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$viewName = 'viewName_'.$line->id;
				$sql = $this->selectQry(TBL_VIEWDESC,'1 and viewId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) { 
					$query = "insert into ".TBL_VIEWDESC." set viewId = '$post[id]', langId = '".$line->id."', viewName = '".addslashes($post[$viewName])."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}
				else
				{
					$query = "update ".TBL_VIEWDESC." set viewName = '".addslashes($post[$viewName])."' where 1 and viewId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) {									
						$this->logSuccessFail('1',$query);		
					}else {	
						$this->logSuccessFail('0',$query);
						}	
				}	
			}	
		}
		echo "<script>window.location.href='manageView.php?page=$post[page]';</script>";
	}
	
	function SortSequence($get){
	  //print_r($get);
	  $sortedArr 	=   explode("=",$get['url']);
	  $sortedIdArr	=	$sortedArr[1];
	  $Sorted_Order_Arr = explode(",",$sortedIdArr);
	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	 
	  $i = 0;
	  $sorted_key_arr	=	array();	 
	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){
	     $current_id	=	$Current_Order_Arr[$i];
		 $sql_string	=	"SELECT sequence FROM ".TBL_VIEW." WHERE id = '$current_id'";
         $query 	 	=	mysql_query($sql_string);
		 if($line = mysql_fetch_object($query)){
		    $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
		 }
		 $i++;
       }
	  //print_r($sorted_key_arr);
	  foreach($sorted_key_arr as $key=>$val){  
	        $sql_string		=	"UPDATE ".TBL_VIEW." SET sequence = '$val' WHERE id = '".$key."'";
		   	mysql_query($sql_string);   
	   }
	    
/*		for($i=30;$i<60;$i++){
		     $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
			 $n_row = mysql_num_rows($Qry);
			 if($n_row > 1){
			   $k = 1;
			   while($data = mysql_fetch_object($Qry)){
			     mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
				 $k++;
			   }
			 }
		}*/
		$cond = "1 and ft.id = ftd.viewId  and ftd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select ft.*,ftd.viewName from ".TBL_VIEW." as ft , ".TBL_VIEWDESC." as ftd where $cond ";
		
		
				//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($get['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($get['page']); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
		     $orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
		     $order = $_GET[order]? $_GET[order]:"ASC";   
         //   $query .=  " ORDER BY cd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
		     $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			
	      $order_query = mysql_query($query);
		  while($line = mysql_fetch_object($order_query)){
		     $currentOrder .= $line->id.",";
		  }
	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php


	}
	
	function findMaxSequence(){
		$query	=	"SELECT max(sequence) as maxsequence FROM ".TBL_VIEW." ";
	    $res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}
	/*
	function checkCategoryExist($cid) {
		if($cid) {
			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_TBL_CATEGORY'") == 1) {
					$getCid = $this->fetchValue(TBL_CATEGORY,"id","1 and id = ".(int)$cid."");
					if(!$getCid)
						redirect('manageCategory.php');		
				} else {
					redirect('manageCategory.php');					
				}
			} else {
				redirect('manageCategory.php');
			}
		}
	}
	
	*/
	
}// End Class
?>	

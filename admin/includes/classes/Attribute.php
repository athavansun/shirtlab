<?php 
session_start();
class Attribute extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail() {
		$cond = "1 and ".TBL_ATTRIBUTE.".id = ".TBL_ATTRIBUTE_DESCRIPTION.".attributeId and ".TBL_ATTRIBUTE.".isDeleted = '0' and ".TBL_ATTRIBUTE.".showAttribute = '0' and ".TBL_ATTRIBUTE_DESCRIPTION.".langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (".TBL_ATTRIBUTE_DESCRIPTION.".attributeName LIKE '%$searchtxt%')";
		}
		$query = "select ".TBL_ATTRIBUTE.".*,".TBL_ATTRIBUTE_DESCRIPTION.".attributeName from ".TBL_ATTRIBUTE." , ".TBL_ATTRIBUTE_DESCRIPTION." where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:TBL_ATTRIBUTE_DESCRIPTION.".attributeName";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					$attributeName = $this->fetchValue(TBL_ATTRIBUTE_DESCRIPTION,"attributeName","1 and attributeId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
												
					$genTable .= '<div class="'.$highlight.'">
								 <ul>
								 	<li style="width:50px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>
									<li style="width:100px;">'.$i.'</li>
									<li style="width:180px;">'.stripslashes($attributeName).'</li><li style="width:100px;">';
									
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'attribute\')">'.$status.'</div>';
									
																											
					$genTable .= '</li>
					<li style="width:110px;"><a rel="shadowbox;width=705;height=325" title="'.stripslashes($attributeName).'" href="viewAttribute.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li>
					<li style="width:75px;">';
									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a href="editAttribute.php?id='.base64_encode($line->id).'&page='.$page.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
					$genTable .= '</li><li>';
				
					if($menuObj->checkDeletePermission() && $line->attributeType == 0)
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=attribute&type=delete&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					
							
					$genTable .= '</li></ul></div>';
				
		
					$i++;	
				}

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_ATTRIBUTE,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive";
		} else 	{
			$stat= 1;
			$status="Active";
		}
	
		$query = "update ".TBL_ATTRIBUTE." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteValue($get) {
		$this->executeQry("update ".TBL_ATTRIBUTE." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
		
		$this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where attributeId = '$get[id]'");
		
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageAttribute.php?page=$get[page]&limit=$get[limit]';</script>";
	}
		
	
	
	
	function addRecord($post) {
		
		$_SESSION['SESS_MSG'] = "";		
		if($_SESSION['SESS_MSG'] == "") {
			$query = "insert into ".TBL_ATTRIBUTE." set status = '1', addDate = '".date('Y-m-d H:i:s')."', addedBy = '".$_SESSION['ADMIN_ID']."'";
			$sql = $this->executeQry($query);
			$inserted_id = mysql_insert_id();

			if($inserted_id) 
				$this->logSuccessFail('1',$query);		
			else 	
				$this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){			
				while($line = $this->getResultObject($rst)) {					
					$attributeName = 'attributeName_'.$line->id;
					$query = "insert into ".TBL_ATTRIBUTE_DESCRIPTION." set attributeId = '$inserted_id', langId = '".$line->id."', attributeName = '".addslashes($post[$attributeName])."'";	
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);
				}	
			}					
			$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
			//@header('Location:addAttribute.php');	
		} 
			
	}
	
	function editRecord($post) {
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$attributeName = 'attributeName_'.$line->id;
				$sql = $this->selectQry(TBL_ATTRIBUTE_DESCRIPTION,'1 and attributeId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_ATTRIBUTE_DESCRIPTION." set attributeId = '$post[id]', langId = '".$line->id."', attributeName = '".$post[$attributeName]."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				} else {
					$query = "update ".TBL_ATTRIBUTE_DESCRIPTION." set attributeName = '".addslashes($post[$attributeName])."' where 1 and attributeId = '$post[id]' and langId = '".$line->id."'";
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}	
			}	
		}					
		
		$query = "update ".TBL_ATTRIBUTE." set modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$post[id]."'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);	
			
		echo "<script>window.location.href='manageAttribute.php?page=$post[page]';</script>";
	}

	function deleteAllValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageAttribute.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_ATTRIBUTE," path like '%-$val-%'");	
					$this->executeQry("update ".TBL_ATTRIBUTE." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
				    $this->executeQry("update ".TBL_ATTRIBUTE_VALUES." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where attributeId = '$val'");	
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_ATTRIBUTE." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql="update ".TBL_ATTRIBUTE." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageAttribute.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_ATTRIBUTE." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageAttribute.php");
		}	
	}		
	
	function isAttributeNameExist($attributename,$langId,$id=''){
	    
		$catname = trim($attributename);
		$rst = $this->selectQry(TBL_ATTRIBUTE_DESCRIPTION,"langId = '".$langId."' and attributeName='".addslashes($attributename)."' AND attributeId!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		$line = $this->getResultObject($rst);
		if($this->fetchValue(TBL_ATTRIBUTE,'isDeleted',"id='".$line->attributeId."'")){
		    if($id)
			$rstFind = $this->executeQry("update ".TBL_ATTRIBUTE." set isDeleted='1' where id='".$id."'");
			$rstFind = $this->executeQry("update ".TBL_ATTRIBUTE." set isDeleted='0' where id='".$line->attributeId."'");
			echo "<script>window.location.href='manageAttribute.php';</script>";
			exit;
		}else{
			return $row;
		}
	}		
	
}// End Class
?>	
<?php
session_start();

class Products extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function valDetail() {
        $cond = "1 and P.id=PD.pId and P.isDeleted = '0' and PD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        if ($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != 'searchtext') {
            $searchtxt = addslashes($_REQUEST['searchtxt']);
            $cond .= " AND (PD.productName LIKE '%$searchtxt%')";
        }
        //$cond .= " group by P.styleId, P.sequence";

        $query = "select P.*,PD.productName from " . TBL_PRODUCT . " AS P ," . TBL_PRODUCT_DESCRIPTION . " AS PD WHERE $cond ";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $menuObj = new Menu();
        $page = $_REQUEST['page'] ? $_REQUEST['page'] : 1;
        if ($num > 0) {
            //-------------------------Paging------------------------------------------------			
            $paging = $this->paging($query);
            $limit = $_GET[limit] ? $_GET[limit] : 20;
            $this->setLimit($limit);
            $recordsPerPage = $this->getLimit();
            $offset = $this->getOffset($_GET["page"]);
            $this->setStyle("redheading");
            $this->setActiveStyle("smallheading");
            $this->setButtonStyle("boldcolor");
            $currQueryString = $this->getQueryString();
            $this->setParameter($currQueryString);
            $totalrecords = $this->numrows;
            $currpage = $this->getPage();
            $totalpage = $this->getNoOfPages();
            $pagenumbers = $this->getPageNo();
            //-------------------------Paging------------------------------------------------
            $orderby = $_GET[orderby] ? $_GET[orderby] : " PD.productName ";
            $orderby = $_GET[orderby] ? $_GET[orderby] : " P.catId";
            $order = $_GET[order] ? $_GET[order] : " ASC";
            $query .= " order by " . $orderby . " " . $order . " LIMIT " . $offset . ", " . $recordsPerPage;

            $rst = $this->executeQry($query);
            $row = $this->getTotalRow($rst);
            if ($row > 0) {
                $i = 1;
                //$genTable .= '<div class="column" id="column1">';		
                while ($line = $this->getResultObject($rst)) {
                    $currentOrder .= $line->id . ",";
                    $highlight = $i % 2 == 0 ? "main-body-bynic" : "main-body-bynic2";
                    $div_id = "status" . $line->id;
                    if ($line->status == 0)
                        $status = "Inactive";
                    else
                        $status = "Active";
                    //$productName = $this->fetchValue(TBL_PRODUCT_DESCRIPTION,"productName","1 and productId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");

                    $genTable .= '<tr class="dragbox-content ' . $highlight . '" id="' . $line->id . '" >';
                    $genTable .= '<th><input name="chk[]" value="' . $line->id . '" type="checkbox"></th>';
                    $genTable .= '<td>' . $i . '</td>';
                    //$genTable .= '<td>'.$line->id.'</td>';
                    //$genTable .= '<td>'.$line->id.'</td>';
                    $genTable .= '<td>' . stripslashes(stripslashes($this->fetchValue(TBL_CATEGORY_DESCRIPTION, 'categoryName', "catId='" . $line->catId . "' and langId='" . $_SESSION['DEFAULTLANGUAGE'] . "'"))) . '</td>';
                    $genTable .= '<td>' . stripslashes($line->productName) . '</td>';
                    $image = ABSOLUTEPATH . __PRODUCTTHUMB__ . $line->proImage;
                    if (file_exists($image))
                        $image = SITE_URL . __PRODUCTTHUMB__ . $line->proImage;
                    else
                        $image = NOIMAGE;

                    $genTable .= '<td><img src="' . $image . '" /></td>';
                    //getDefaultColorProductImage
                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<td><div id="' . $div_id . '" style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'product\')">' . $status . '</div></td>';
                    }else
                        $genTable .= '<td>' . $status . '</td>';
                    $genTable .= '<td><a rel="shadowbox;width=705;height=325;" title="test' . $productName . '" href="viewProduct.php?id=' . base64_encode($line->id) . '"><img src="images/view.png" border="0"></a></td>';

                    if ($menuObj->checkEditPermission()) {
                        $genTable .= '<td><a class="i_pencil edit" href="editProduct.php?id=' . base64_encode($line->id) . '&page=' . $page . '"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a></td>';
                    }else
                        $genTable .= '<td>xxxx</td>';

                    if ($menuObj->checkDeletePermission()) {
                        $genTable .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=product&type=delete&id=" . $line->id . "&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a></td>";
                    }else
                        $genTable .= '<td>xxxxx</td>';
                    $genTable .= '</tr>';
                    $i++;
                }
                $genTable .= '</div>';
                $genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="' . $currentOrder . '" /></div>';

                switch ($recordsPerPage) {
                    case 20:
                        $sel1 = "selected='selected'";
                        break;
                    case 30:
                        $sel2 = "selected='selected'";
                        break;
                    case 40:
                        $sel3 = "selected='selected'";
                        break;
                    case $this->numrows:
                        $sel4 = "selected='selected'";
                        break;
                }
                $currQueryString = $this->getQueryString();
                $limit = basename($_SERVER['PHP_SELF']) . "?" . $currQueryString;
                $genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'>
				<table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='20' $sel1>20</option>
					<option value='30' $sel2>30</option>
					<option value='40' $sel3>40</option> 
					<option value='" . $totalrecords . "' $sel4>All</option>  
					  </select> Records Per Page
				</td><td class='page_info' align='center' width='200'>Total " . $totalrecords . " records found</td><td width='0' align='right'>" . $pagenumbers . "</td></tr></table><inputtype='hidden' name='page' value='" . $currpage . "'></div>";
            }
        } else {
            $genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry no records found</div>';
        }
        return $genTable;
    }

    function changeValueStatus($get) {
        $xmlArr = array();
        $xm = 1;
        $status = $this->fetchValue(TBL_PRODUCT, "status", "1 and id = '$get[id]'");
        if ($status == 1) {
            $stat = 0;
            $status = "Inactive,0";
        } else {
            $stat = 1;
            $status = "Active,1";
        }

        $query = "update " . TBL_PRODUCT . " set status = '$stat', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";

        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;

        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        echo $status;
    }

    function deleteValue($get) {
        $xmlArr = array();
        $xm = 1;
        $query = "update " . TBL_PRODUCT . " set isDeleted = '1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$get[id]'";
        $rst = $this->executeQry($query);

        //$result=$this->deleteRec(TBL_PRODUCT_ATTRIBUTE,"productId='$get[id]'");
        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;

        if ($rst) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully!!!");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Opps! somthing went wrong,try later again.");
        }

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        echo "<script language=javascript>window.location.href='manageProducts.php?page=$get[page]&limit=$get[limit]';</script>";
    }

    function deleteAllValues($post) {
        $xmlArr = array();
        $xm = 1;
        if (($post[action] == '')) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the action or records , And then submit!!!");
            echo "<script language=javascript>window.location.href='manageProducts.php?page=$post[page]&limit=$post[limit]';</script>";
            exit;
        }
        if ($post[action] == 'deleteselected') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $this->executeQry("update " . TBL_PRODUCT . " set isDeleted = '1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$val'");

                    $xmlArr[$xm]['query'] = addslashes("update " . TBL_PRODUCT . " set isDeleted = '1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id = '$val'");
                    $xmlArr[$xm]['identification'] = $val;
                    $xmlArr[$xm]['section'] = "delete";
                    $xm++;
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your all selected information has been deleted successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'enableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "update " . TBL_PRODUCT . " set status ='1', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";

                    $xmlArr[$xm]['query'] = addslashes($sql);
                    $xmlArr[$xm]['identification'] = $val;
                    $xmlArr[$xm]['section'] = "update";
                    $xm++;

                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Enable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }
        if ($post[action] == 'disableall') {
            $delres = $post[chk];
            $numrec = count($delres);
            if ($numrec > 0) {
                foreach ($delres as $key => $val) {
                    $sql = "update " . TBL_PRODUCT . " set status ='0', modDate = '" . date('Y-m-d H:i:s') . "', modBy = '" . $_SESSION['ADMIN_ID'] . "' where id='$val'";
                    $xmlArr[$xm]['query'] = addslashes($sql);
                    $xmlArr[$xm]['identification'] = $val;
                    $xmlArr[$xm]['section'] = "update";
                    $xm++;

                    mysql_query($sql);
                }
                $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Disable selected successfully!!!");
            } else {
                $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "First select the record!!!");
            }
        }

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo "<script language=javascript>window.location.href='manageProducts.php?cid=$post[cid]&page=$post[page]';</script>";
    }

    function getProductInfo($id) {
        $sql = $this->executeQry("select * from " . TBL_PRODUCT . " where id = '$id'");
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            return $line = $this->getResultObject($sql);
        } else {
            redirect("manageProducts.php");
        }
    }

    function addRecord($post, $file) {
        $xmlArr = array();
        $xm = 1;
        $_SESSION['SESS_MSG'] = "";
        if ($_SESSION['SESS_MSG'] == "") {
            #-----------adding general images---------

            if ($file['proImage']['name']) {
                $filename = stripslashes($file['proImage']['name']);
                $extension = findexts($filename);
                $extension = strtolower($extension);

                $proImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                $target = ABSOLUTEPATH . __PRODUCTORIGINAL__ . $proImgName;
                $filestatus = move_uploaded_file($file['proImage']['tmp_name'], $target);
                if ($filestatus) {
                    $ThumbImage = ABSOLUTEPATH . __PRODUCTTHUMB__ . $proImgName;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 60x60! $ThumbImage");

                    $ThumbImage1 = ABSOLUTEPATH . __PRODUCTTHUMB_125x160_ . $proImgName;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 125x160! $ThumbImage1");

                    $ThumbImage2 = ABSOLUTEPATH . __PRODUCTTHUMB_150x265_ . $proImgName;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 214x265! $ThumbImage2");

                    $ThumbImage3 = ABSOLUTEPATH . __PRODUCTLARGE__ . $proImgName;
                    //exec(IMAGEMAGICPATH." $target -thumbnail 275x464! $ThumbImage3");
                    exec(IMAGEMAGICPATH . " $target -thumbnail 293x380! $ThumbImage3");
                    exec(IMAGEMAGICPATH . " $target -thumbnail 293x380! $ThumbImage3");
                }
            }

            $query = "insert into " . TBL_PRODUCT . " set catId = '" . $post['prodStyle'] . "', productPrice = '$post[productPrice]', proImage = '" . $proImgName . "', status = '1', isDeleted = '0', addDate = '" . date('Y-m-d H:i:s') . "',addedBy = '" . $_SESSION['ADMIN_ID'] . "', sequence  = '" . $sequence . "'";
            $sql = $this->executeQry($query);
            $prodId = mysql_insert_id();

            $xmlArr[$xm]['query'] = addslashes($query);
            $xmlArr[$xm]['identification'] = $prodId;
            $xmlArr[$xm]['section'] = "insert";
            $xm++;

            if ($prodId) {
                $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
                $num = $this->getTotalRow($rst);
                if ($num) {
                    while ($line = $this->getResultObject($rst)) {
                        $productName = 'productName_' . $line->id;
                        $productDesc = 'productDesc_' . $line->id;

                        $query = "insert into " . TBL_PRODUCT_DESCRIPTION . " set pId = '$prodId', langId = '" . $line->id . "', productName = '" . addslashes($post[$productName]) . "', productDesc = '" . mysql_real_escape_string($post[$productDesc]) . "'";

                        $xmlArr[$xm]['query'] = addslashes($query);
                        $xmlArr[$xm]['identification'] = $prodId;
                        $xmlArr[$xm]['section'] = "insert";
                        $xm++;

                        if ($this->executeQry($query))
                            $this->logSuccessFail('1', $query);
                        else
                            $this->logSuccessFail('0', $query);
                    }
                }

                #------product pdf start---------
                if ($file['proPdf']['name']) {
                    $filename = stripslashes($file['proPdf']['name']);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);

                    $proPdf = date("Ymdhis") . time() . rand() . '.' . $extension;
                    $target = ABSOLUTEPATH . __PRODUCTPDF__ . $proPdf;
                    $filestatus = move_uploaded_file($file['proPdf']['tmp_name'], $target);
                    if ($filestatus) {
                        $this->executeQry("update " . TBL_PRODUCT . " set proPdf = '" . addslashes($proPdf) . "' where id = '" . $prodId . "'");
                    }
                }

                #------product pdf end---------
                #----------- product size ----------------
                if (count($post['size']) > 0) {
                    $query = Array();
                    foreach ($post['size'] as $sizeid) {
                        $query[] = " ('" . $prodId . "', '" . $sizeid . "', '1', '0')";
                    }
                    $queryVal = implode(' ,', $query);
                    $querySize = "Insert into " . TBL_PRODUCTSIZE . " (pId, sizeId, status, isDeleted) values " . $queryVal;
                    $sql = $this->executeQry($querySize);

                    $xmlArr[$xm]['query'] = addslashes($querySize);
                    $xmlArr[$xm]['identification'] = $prodId;
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;
                }
                #---------------Product tool & view Image Upload---------------

                foreach ($post['viewName'] as $viewId) {
                    #---------tool image start----------
                    $imgName = "toolImage_" . $viewId;
                    $printW = "pWidth_" . $viewId;
                    $printH = "pHeight_" . $viewId;
                    if ($file[$imgName]['name'] != '') {
                        $filename = stripslashes($file[$imgName]['name']);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);

                        $toolImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __PRODUCTTOOLORIGINAL__ . $toolImage;

                        if ($this->checkExtensions($extension)) {
                            $filestatus = move_uploaded_file($file[$imgName]['tmp_name'], $target);
                            @chmod($target, 0777);

                            if ($filestatus) {
                                $printWidth = $post[$printW];
                                $printHeight = $post[$printH];

                                $ThumbImage = ABSOLUTEPATH . __PRODUCTTOOLTHUMB__ . $toolImage;
                                $fileSize = "60x60";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                                $Thumblarge = ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $toolImage;
                                $fileSize = "350x450";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $Thumblarge");

                                $queryTool = "insert into " . TBL_PRODUCT_TOOL . " set pId = '$prodId', viewId = '" . $viewId . "', toolImage = '" . addslashes($toolImage) . "', printWidth = '" . $printWidth . "', printHeight = '" . $printHeight . "'";
                                $this->executeQry($queryTool);

                                $xmlArr[$xm]['query'] = addslashes($queryTool);
                                $xmlArr[$xm]['identification'] = $prodId;
                                $xmlArr[$xm]['section'] = "insert";
                                $xm++;
                            }
                        }
                    }
                    #---------tool image end-------
                    #---------view image start----------
                    $imageName = "viewImage_" . $viewId;
                    if ($file[$imageName]['name'] != '') {
                        $filename = stripslashes($file[$imageName]['name']);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);

                        $viewImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __PRODUCTVIEWORIGINAL__ . $viewImgName;

                        if ($this->checkExtensions($extension)) {
                            $filestatus = move_uploaded_file($file[$imageName]['tmp_name'], $target);
                            @chmod($target, 0777);

                            if ($filestatus) {
                                //$imgAtt = getimagesize($target);
                                $ThumbImage = ABSOLUTEPATH . __PRODUCTVIEWTHUMB__ . $viewImgName;
                                $fileSize = "72x71";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                                $queryView = "insert into " . TBL_PRODUCT_VIEW . " set pId = '$prodId', viewId = '" . $viewId . "', imageName = '" . addslashes($viewImgName) . "'";
                                $this->executeQry($queryView);

                                $xmlArr[$xm]['query'] = addslashes($queryView);
                                $xmlArr[$xm]['identification'] = $prodId;
                                $xmlArr[$xm]['section'] = "insert";
                                $xm++;
                            }
                        }
                    }
                    #---------view image end-------
                }
            } else {
                $this->logSuccessFail('0', $query);
            }

            $xmlInfoObj = new XmlInfo();
            $xmlInfoObj->addXmlData($xmlArr);
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been added successfully");
        }
        echo "<script language=javascript>window.location.href='editProduct.php?id=" . base64_encode($prodId) . "';</script>";
        exit;
    }

    function editRecord($post, $file) { // <TAB 1 UPDATION>
        $xmlArr = array();
        $xm = 1;
        $_SESSION['SESS_MSG'] = "";
        $query = "UPDATE " . TBL_PRODUCT . " set productPrice = '$post[productPrice]' WHERE id ='$post[id]'";

        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $post['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;

        $sql = $this->executeQry($query);
        if ($sql) {
            $this->logSuccessFail('1', $query);
        } else {
            $this->logSuccessFail('0', $query);
        }
        #---------product image----------
        if ($file['proImage']['name']) {
            $filename = stripslashes($file['proImage']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);

            $proImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
            $target = ABSOLUTEPATH . __PRODUCTORIGINAL__ . $proImgName;
            $filestatus = move_uploaded_file($file['proImage']['tmp_name'], $target);
            if ($filestatus) {
                #-------removing old iamges------
                $oldImgName = $this->fetchValue(TBL_PRODUCT, "proImage", " id = '" . $post[id] . "'");

                @unlink(ABSOLUTEPATH . __PRODUCTTHUMB__ . $oldImgName);
                @unlink(ABSOLUTEPATH . __PRODUCTTHUMB_125x160_ . $oldImgName);
                @unlink(ABSOLUTEPATH . __PRODUCTTHUMB_150x265_ . $oldImgName);
                @unlink(ABSOLUTEPATH . __PRODUCTLARGE__ . $oldImgName);
                @unlink(ABSOLUTEPATH . __PRODUCTORIGINAL__ . $oldImgName);
                #------updating recodr---------
                $query = "UPDATE " . TBL_PRODUCT . " set proImage = '" . $proImgName . "' WHERE id ='$post[id]'";
                $this->executeQry($query);

                $xmlArr[$xm]['query'] = addslashes($query);
                $xmlArr[$xm]['identification'] = $post['id'];
                $xmlArr[$xm]['section'] = "update";
                $xm++;

                $ThumbImage = ABSOLUTEPATH . __PRODUCTTHUMB__ . $proImgName;
//				exec(IMAGEMAGICPATH." $target -thumbnail 60x60! $ThumbImage");
                exec(IMAGEMAGICPATH . " $target -thumbnail 46x60! $ThumbImage");

                $ThumbImage1 = ABSOLUTEPATH . __PRODUCTTHUMB_125x160_ . $proImgName;
                exec(IMAGEMAGICPATH . " $target -thumbnail 125x160! $ThumbImage1");

                $ThumbImage2 = ABSOLUTEPATH . __PRODUCTTHUMB_150x265_ . $proImgName;
                exec(IMAGEMAGICPATH . " $target -thumbnail 214x265! $ThumbImage2");

                $ThumbImage3 = ABSOLUTEPATH . __PRODUCTLARGE__ . $proImgName;
                exec(IMAGEMAGICPATH . " $target -thumbnail 293x380! $ThumbImage3");
            }
        }

        if ($file['proPdf']['name']) {
            $filename = stripslashes($file['proPdf']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);
            $proPdf = date("Ymdhis") . time() . rand() . '.' . $extension;
            $target = ABSOLUTEPATH . __PRODUCTPDF__ . $proPdf;
            $filestatus = move_uploaded_file($file['proPdf']['tmp_name'], $target);
            if ($filestatus) {
                #-------removing old pdf------
                $oldproPdf = $this->fetchValue(TBL_PRODUCT, "proPdf", " id = '" . $post[id] . "'");

                @unlink(ABSOLUTEPATH . __PRODUCTPDF__ . $oldproPdf);

                #------updating record---------
                $query = "UPDATE " . TBL_PRODUCT . " set proPdf = '" . $proPdf . "' WHERE id ='$post[id]'";
                $this->executeQry($query);

                $xmlArr[$xm]['query'] = addslashes($query);
                $xmlArr[$xm]['identification'] = $post['id'];
                $xmlArr[$xm]['section'] = "update";
                $xm++;
            }
        }

        $rst = $this->selectQry(TBL_LANGUAGE, "status='1' and isDeleted = '0' order by id asc", "", "");
        $num = $this->getTotalRow($rst);
        if ($num) {
            while ($line = $this->getResultObject($rst)) {
                $productName = 'productName_' . $line->id;
                $productDesc = 'productDesc_' . $line->id;
                $sql_pro = $this->selectQry(TBL_PRODUCT_DESCRIPTION, '1 and pId = "' . $post[id] . '" and langId = "' . $line->id . '"', '', '');
                $numrows_pro = $this->getTotalRow($sql_pro);
                if ($numrows_pro == 0) {
                    $query = "insert into " . TBL_PRODUCT_DESCRIPTION . " set productName = '" . addslashes($post[$productName]) . "', langId = '" . $line->id . "', productDesc = '" . mysql_real_escape_string($post[$productDesc]) . "' , pId = '" . $post[id] . "'";
                } else {
                    $query = "UPDATE " . TBL_PRODUCT_DESCRIPTION . " set productName = '" . addslashes($post[$productName]) . "', productDesc = '" . mysql_real_escape_string($post[$productDesc]) . "' WHERE pId = '$post[id]' AND langId = '" . $line->id . "'";
                    $xmlArr[$xm]['query'] = addslashes($query);
                    $xmlArr[$xm]['identification'] = $post['id'];
                    $xmlArr[$xm]['section'] = "update";
                    $xm++;
                }
                if ($this->executeQry($query))
                    $this->logSuccessFail('1', $query);
                else
                    $this->logSuccessFail('0', $query);
            }
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");
        }
        else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Opps! something went wrong, please try later again.");
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        header('location:editProduct.php?id=' . base64_encode($post[id]));
        exit();
    }

    function updateProductSize($post) {
        $xmlArr = array();
        $xm = 1;
        if ($post['id'] != '') {
            $this->executeQry("Delete from " . TBL_PRODUCTSIZE . " where pId = '" . $post['id'] . "'");

            $query = Array();
            foreach ($post['size'] as $sizeid) {
                $query[] = " ('" . $post['id'] . "', '" . $sizeid . "', '1', '0')";
            }
            $queryVal = implode(' ,', $query);
            $querySize = "Insert into " . TBL_PRODUCTSIZE . " (pId, sizeId, status, isDeleted) values " . $queryVal;
            $sql = $this->executeQry($querySize);
            $xmlArr[$xm]['query'] = addslashes($querySize);
            $xmlArr[$xm]['identification'] = $post['id'];
            $xmlArr[$xm]['section'] = "insert";
            $xm++;
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        header("location:editProduct.php?id=" . base64_encode($post[id]) . "&tab=3");
        exit;
    }

    function isProductNameExist($productname, $langId, $id = '') {
        $catname = addslashes(trim($productname));
        $query = "Select P.id from " . TBL_PRODUCT . " as P INNER JOIN " . TBL_PRODUCT_DESCRIPTION . " PD ON (P.id = PD.pId) where P.id != '" . $id . "' and P.isDeleted = '0' and PD.productName = '" . $catname . "' and PD.langId = '" . $langId . "'";

        $rst = $this->executeQry($query);
        $row = $this->getTotalRow($rst);
        if ($row > 0)
            return true;
        else
            return false;
    }

    function getProductViewEdit($pId) {
        $genTable = '';
        $query = "select v.id, pt.toolImage, pt.printWidth, pt.printHeight, vd.viewName from " . TBL_VIEW . " as v left join " . TBL_PRODUCT_TOOL . " as pt on v.id = pt.viewId and pt.pId = '" . $pId . "' inner join " . TBL_VIEWDESC . " as vd on v.id=vd.viewId and vd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        //echo $query;// exit;
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $genTable .= '<table style="width:100%">';
        $genTable .= '<tr>';
        $genTable .= '<th>View Name</th>';
        $genTable .= '<th>Print Size (W x H) in cm</th>';
        $genTable .= '<th style="font-size:18px;">1 Image for Tool & 2 View Image</th>';
        $genTable .= '</tr>';
        if ($num > 0) {
            while ($line = $this->getResultObject($result)) {
                $check = '';
                $viewImageSrc = '';
                $toolImageSrc = '';
                if ($line->toolImage != '') {
                    $check = 'checked="checked"';
                    $toolImage = checkImgAvailable(__PRODUCTTOOLTHUMB__ . $line->toolImage);
                    $toolImageSrc = '<img width="50" src="' . $toolImage . '" /><br />';
                    $viewImage = $this->fetchValue(TBL_PRODUCT_VIEW, 'imageName', " pId = '" . $pId . "' and viewId = '" . $line->id . "'");
                    $viewImageHid = $viewImage;
                    $viewImage = $viewImage == '' ? NOIMAGE : checkImgAvailable(__PRODUCTVIEWTHUMB__ . $viewImage);
                    $viewImageSrc = '<img width="50" src="' . $viewImage . '" /><br />';
                }

                $genTable .= '<input type="hidden" name="view_' . $line->id . '" value="' . $this->getOtherValue($pId, $line->id) . '" />';
                $genTable .= '<tr>';
                $genTable .= '<th>' . $line->viewName . ' <input ' . $check . ' type="checkbox" name="viewName[]" value="' . $line->id . '" /></th>';
                $genTable .= '<td>
				<input maxLength="5" value="' . $line->printWidth . '" type="text" style="width:35px;" name="pWidth_' . $line->id . '" /> x 
				<input type="text" value="' . $line->printHeight . '" maxLength="5" style="width:35px;" name="pHeight_' . $line->id . '" /></td>';
                $genTable .= '<td style="text-align:left;">
				' . $toolImageSrc . '
				<input type="hidden" name="oldToolImg' . $line->id . '" value="' . $line->toolImage . '" />
				<input type="file" name="toolImage_' . $line->id . '" class="file browse-button" /><br /><br />
				' . $viewImageSrc . '
				<input type="hidden" name="oldViewImg' . $line->id . '" value="' . $viewImageHid . '" />
				<input type="file" name="viewImage_' . $line->id . '" class="file browse-button" />
				</td>';
                $genTable .= '</tr>';
            }
        } else {
            $genTable .= '<tr><th colspan="2">Oops, no view(s) available.</th></tr>';
        }
        $genTable .= '</table>';
        return $genTable;
    }

    function getOtherValue($pId, $viewId) {
        $query = "select imageX, imageY, imageW, imageH, wAreaX, wAreaY, wAreaW, wAreaH from " . TBL_PRODUCT_TOOL . " where pId = '" . $pId . "' and viewId = '" . $viewId . "'";
        $result = $this->executeQry($query);
        $valArr = $this->fetch_assoc($result);
        $val = implode(',', $valArr);
        return $val;
    }

    function saveProdView($post, $file) {
        $xmlArr = array();
        $xm = 1;

        #---------------Product tool & view Image Upload---------------
        if (count($post['viewName']) > 0) {
            #-------removing from table------
            $this->executeQry("delete from " . TBL_PRODUCT_VIEW . " where pId = '" . $post['pId'] . "'");
            $this->executeQry("delete from " . TBL_PRODUCT_TOOL . " where pId = '" . $post['pId'] . "'");

            #------restoring record-------
            foreach ($post['viewName'] as $viewId) {
                #------other values start--------
                if (count($post['view_' . $viewId]) > 0) {
                    $valArr = explode(',', $post['view_' . $viewId]);
                    list($imageX, $imageY, $imageW, $imageH, $wAreaX, $wAreaY, $wAreaW, $wAreaH) = $valArr;

                    $otherFields = " , imageX = '" . $imageX . "', imageY = '" . $imageY . "', imageW = '" . $imageW . "', imageH = '" . $imageH . "', wAreaX = '" . $wAreaX . "', wAreaY = '" . $wAreaY . "', wAreaW = '" . $wAreaW . "', wAreaH = '" . $wAreaH . "'";
                }
                #------other values start--------
                #---------tool image start----------
                $imgName = "toolImage_" . $viewId;
                $printW = "pWidth_" . $viewId;
                $printH = "pHeight_" . $viewId;
                $printWidth = $post[$printW];
                $printHeight = $post[$printH];
                if ($file[$imgName]['name'] != '') {
                    $filename = stripslashes($file[$imgName]['name']);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);
                    $toolImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                    $target = ABSOLUTEPATH . __PRODUCTTOOLORIGINAL__ . $toolImage;
                    $filestatus = move_uploaded_file($file[$imgName]['tmp_name'], $target);
                    @chmod($target, 0777);
                    if ($filestatus) {
                        $ThumbImage = ABSOLUTEPATH . __PRODUCTTOOLTHUMB__ . $toolImage;
                        $fileSize = "60x60";
                        exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                        $ThumbImage1 = ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $toolImage;
                        $fileSize = "350x450";
                        exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage1");

                        $queryTool = "insert into " . TBL_PRODUCT_TOOL . " set pId = '" . $post['pId'] . "', viewId = '" . $viewId . "', toolImage = '" . addslashes($toolImage) . "', printWidth = '" . $printWidth . "', printHeight = '" . $printHeight . "'" . $otherFields;
                        $this->executeQry($queryTool);
                        $xmlArr[$xm]['query'] = addslashes($queryTool);
                        $xmlArr[$xm]['identification'] = $post['pId'];
                        $xmlArr[$xm]['section'] = "insert";
                        $xm++;

                        #--------removing old images-------
                        //@unlink(ABSOLUTEPATH.__PRODUCTTOOLORIGINAL__.$post['oldToolImg'.$viewId]);
                        //@unlink(ABSOLUTEPATH.__PRODUCTTOOLTHUMB__.$post['oldToolImg'.$viewId]);
                    }
                } else {
                    $queryTool = "insert into " . TBL_PRODUCT_TOOL . " set pId = '" . $post['pId'] . "', viewId = '" . $viewId . "', toolImage = '" . addslashes($post['oldToolImg' . $viewId]) . "', printWidth = '" . $printWidth . "', printHeight = '" . $printHeight . "'" . $otherFields;
                    $this->executeQry($queryTool);
                    $xmlArr[$xm]['query'] = addslashes($queryTool);
                    $xmlArr[$xm]['identification'] = $post['pId'];
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;
                }
                #---------tool image end-------
                #---------view image start----------
                $imageName = "viewImage_" . $viewId;

                if ($file[$imageName]['name'] != '') {
                    $filename = stripslashes($file[$imageName]['name']);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);
                    $viewImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                    $target = ABSOLUTEPATH . __PRODUCTVIEWORIGINAL__ . $viewImgName;
                    $filestatus = move_uploaded_file($file[$imageName]['tmp_name'], $target);
                    @chmod($target, 0777);
                    if ($filestatus) {
                        $ThumbImage = ABSOLUTEPATH . __PRODUCTVIEWTHUMB__ . $viewImgName;
                        $fileSize = "72x71";
                        exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                        $queryView = "insert into " . TBL_PRODUCT_VIEW . " set pId = '" . $post['pId'] . "', viewId = '" . $viewId . "', imageName = '" . addslashes($viewImgName) . "'";
                        $this->executeQry($queryView);

                        $xmlArr[$xm]['query'] = addslashes($queryView);
                        $xmlArr[$xm]['identification'] = $post['pId'];
                        $xmlArr[$xm]['section'] = "insert";
                        $xm++;

                        @unlink(ABSOLUTEPATH . __PRODUCTVIEWORIGINAL__ . $post['oldViewImg' . $viewId]);
                        @unlink(ABSOLUTEPATH . __PRODUCTVIEWTHUMB__ . $post['oldViewImg' . $viewId]);
                    }
                } else {
                    $queryView = "insert into " . TBL_PRODUCT_VIEW . " set pId = '" . $post['pId'] . "', viewId = '" . $viewId . "', imageName = '" . addslashes($post['oldViewImg' . $viewId]) . "'";
                    $this->executeQry($queryView);
                    $xmlArr[$xm]['query'] = addslashes($queryView);
                    $xmlArr[$xm]['identification'] = $post['pId'];
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;
                }
                #---------view image end-------
            }
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        header('location:editProduct.php?id=' . base64_encode($post['pId']) . '&tab=4');
        exit();
    }

    function getProductViewImageUpload($arr = '') {
        $genTable = '';
        $query = "select V.id, VD.viewName from " . TBL_VIEW . " as V INNER JOIN " . TBL_VIEWDESC . " as VD ON (V.id = VD.viewId) where V.status = '1' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' ";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $genTable .= '<table style="width:100%">';
        $genTable .= '<tr>';
        $genTable .= '<th>View Name</th>';
        $genTable .= '<th>Print Size (W x H)in cm</th>';
        $genTable .= '<th style="font-size:18px;">1 Image for Tool & 2 View Image</th>';
        $genTable .= '</tr>';
        if ($num > 0) {
            while ($line = $this->getResultObject($result)) {
                $check = '';
                if ($arr) {
                    if (in_array($line->id, $arr))
                        $check = 'checked="checked"';
                }
                $genTable .= '<tr>';
                $genTable .= '<th>' . $line->viewName . ' <input ' . $check . ' type="checkbox" name="viewName[]" value="' . $line->id . '" /></th>';
                $genTable .= '<td><input maxLength="5" value="' . $_POST['pWidth_' . $line->id] . '" type="text" style="width:35px;" name="pWidth_' . $line->id . '" /> x <input type="text" value="' . $_POST['pHeight_' . $line->id] . '" maxLength="5" style="width:35px;" name="pHeight_' . $line->id . '" /></td>';
                $genTable .= '<td style="text-align:left;">
				<input type="file" name="toolImage_' . $line->id . '" /><br /><br />
				<input type="file" name="viewImage_' . $line->id . '" />
				</td>';
                $genTable .= '</tr>';
            }
        }
        else {
            $genTable .= '<tr><th colspan="2">Oops, no view(s) available.</th></tr>';
        }
        $genTable .= '</table>';
        return $genTable;
    }

    function editStylePart($post, $file) {
        $xmlArr = array();
        $xm = 1;
        #------reset basic style----------
        if ($post['isBasic'] == '1') {
            $this->executeQry("update " . TBL_PRODUCT_STYLE . " set isBasic = '0' where pId = '" . $post['pId'] . "'");
            $this->executeQry("update " . TBL_PRODUCT_STYLE . " set isBasic = '1' where pId = '" . $post['pId'] . "' and id = '" . $post['sId'] . "'");
        }

        $query1 = "update " . TBL_PRODUCT_STYLE . " set styleCode = '" . $post['styleCode'] . "', sign = '" . $post['sign'] . "', percent = '" . $post['percent'] . "' where pId = '" . $post['pId'] . "' and id = '" . $post['sId'] . "'";
        $_SESSION['query'] = $query1;
        $this->executeQry($query1);

        $xmlArr[$xm]['query'] = addslashes($query1);
        $xmlArr[$xm]['identification'] = $post['pId'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;

        if ($file['styleImage']['name'] != '') {
            $filename = stripslashes($file['styleImage']['name']);
            $extension = findexts($filename);
            $extension = strtolower($extension);

            if ($this->checkExtensions($extension)) {
                $styleImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                $target = ABSOLUTEPATH . __PRODUCTBASICORIGINAL__ . $styleImage;
                $filestatus = move_uploaded_file($file['styleImage']['tmp_name'], $target);

                if ($filestatus) {
                    $ThumbImage = ABSOLUTEPATH . __PRODUCTBASICTHUMB__ . $styleImage;
                    $fileSize = "120x98";
                    exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                    $queryStyle = "update " . TBL_PRODUCT_STYLE . " set styleImage = '" . addslashes($styleImage) . "' where id = '" . $post['sId'] . "'";
                    $this->executeQry($queryStyle);

                    $xmlArr[$xm]['query'] = addslashes($queryStyle);
                    $xmlArr[$xm]['identification'] = $post['pId'];
                    $xmlArr[$xm]['section'] = "update";
                    $xm++;

                    #----remove old basic image------
                    @unlink(ABSOLUTEPATH . __PRODUCTBASICTHUMB__ . $post['oldStyleImage']);
                    @unlink(ABSOLUTEPATH . __PRODUCTBASICORIGINAL__ . $post['oldStyleImage']);
                }
            }
        }

        #-------removing old part images-------
        $delQuery = "Delete from " . TBL_PRODUCT_PART . " where sId = '" . $post['sId'] . "'";
        //$_SESSION['delQuery'] = $delQuery;
        $this->executeQry($delQuery);


        #---------updating product part-------
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $post[pId] . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
        $result = $this->executeQry($query);
        $this->getTotalRow($result);

        while ($line = $this->getResultObject($result)) {

            $ctr = 0;
            $imageName = $line->viewName . $line->viewId;
            $imageName = str_replace(" ", "_", $imageName);
            $oldImg = 'oldImg' . $line->viewId;
            $partTitle = 'partTitle' . $line->viewId;
            $colorable = 'colorable' . $line->viewId;
            
            $visible = "visible" . $line->viewId;
            $sequence = "sequence" . $line->viewId;
            
            foreach ($file[$imageName]['name'] as $imgName) {
                if ($imgName != '') {
                    $filename = stripslashes($imgName);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);

                    if ($this->checkExtensions($extension)) {
                        $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $partImgName;
                        $filestatus = move_uploaded_file($file[$imageName]['tmp_name'][$ctr], $target);

                        if ($filestatus) {
                            $ThumbImage = ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $partImgName;
                            $fileSize = "70x70";
                            exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                            $ThumbImage1 = ABSOLUTEPATH . __PRODUCTPARTLARGE__ . $partImgName;
                            $fileSize = "350x450";
                            exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage1");

                            $queryPart = "insert into " . TBL_PRODUCT_PART . " set sId = '" . $post['sId'] . "', viewId = '" . $line->viewId . "', titleId = '" . $post[$partTitle][$ctr] . "', isColorable = '" . $post[$colorable][$ctr] . "', is_visible = '" . $post[$visible][$ctr] . "',sequence = '" . $post[$sequence][$ctr] . "', partImage = '" . addslashes($partImgName) . "', status = '1'";
                            $this->executeQry($queryPart);

                            $xmlArr[$xm]['query'] = addslashes($queryPart);
                            $xmlArr[$xm]['identification'] = $post['pId'];
                            $xmlArr[$xm]['section'] = "insert";
                            $xm++;

                            #----remove old images------
                            @unlink(ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $post[$oldImg][$ctr]);
                            @unlink(ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $post[$oldImg][$ctr]);
                        }
                    }
                } else { //-------incase of no image change---------
                    $queryPart = "insert into " . TBL_PRODUCT_PART . " set sId = '" . $post['sId'] . "', viewId = '" . $line->viewId . "', titleId = '" . $post[$partTitle][$ctr] . "', isColorable = '" . $post[$colorable][$ctr] . "', is_visible = '" . $post[$visible][$ctr] . "',sequence = '" . $post[$sequence][$ctr] . "', partImage = '" . addslashes($post[$oldImg][$ctr]) . "', status = '1'";

                    $this->executeQry($queryPart);
                    $xmlArr[$xm]['query'] = addslashes($queryPart);
                    $xmlArr[$xm]['identification'] = $post['pId'];
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;
                }
                $ctr++;
            }
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo '<script>window.parent.location.reload();</script>';
        //header("location:editProduct.php?id=".base64_encode($post[pId])."&tab=2");
        exit;
    }

    function updateSpecialColor($post, $file) {
        $xmlArr = array();
        $xm = 1;
        #------Pallet color image----------		
        
        if(!empty($file['colorImage']['name'])) {
               $filename = $file['colorImage']['name'];
               $filename = stripslashes($filename);
               $extension = findexts($filename);
               $extension = strtolower($extension);
               	
               if ($this->checkExtensions($extension)){
                        $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __COLORTHUMB__ . $partImgName;
                        @chmod($target, 0777);
                        $filestatus = move_uploaded_file($file['colorImage']['tmp_name'], $target);
                        $addColorImage = "colorImage='".$partImgName."', ";
                            }			
 				}
        
        
        $query = "update " . TBL_SPECIAL_COLOR . " set $addColorImage colorCode = '#ffffff', code = '" . $post['colorCode'] . "' where id = '" . $post['colorId'] . "'";
        
        
        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $post['colorId'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;
        $this->executeQry($query);
        $this->executeQry("delete from " . TBL_PRODUCT_SPECIAL_PART . " where sId = '" . $post['sId'] . "' and colorId = '" . $post['colorId'] . "'");
        #---------adding special part after delete--------
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $post[pId] . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

        $this->executeQry($query);
        $result = $this->executeQry($query);

        while ($line = $this->getResultObject($result)) {
            $ctr = 0;
            $viewName = preg_replace('!\s+!', '_', trim($line->viewName));
            $imageName = $viewName . $line->viewId;
            $title = $viewName;
            $oldPartImg = 'img' . $viewName;
            foreach ($file[$imageName]['name'] as $imgName) {
                if ($imgName != '') {
                    $filename = stripslashes($imgName);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);

                    if ($this->checkExtensions($extension)) {
                        $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $partImgName;
                        $filestatus = move_uploaded_file($file[$imageName]['tmp_name'][$ctr], $target);

                        if ($filestatus) {
                            $queryPart = "insert into " . TBL_PRODUCT_SPECIAL_PART . " set sId = '" . $post['sId'] . "', colorId = '" . $post['colorId'] . "', viewId = '" . $line->viewId . "', titleId = '" . $post[$title][$ctr] . "', partImage = '" . addslashes($partImgName) . "', status = '1'";
                            $this->executeQry($queryPart);

                            $ThumbImage = ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $partImgName;
                            $fileSize = "70x70";
                            exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                            $ThumbImage1 = ABSOLUTEPATH . __PRODUCTPARTLARGE__ . $partImgName;
                            $fileSize = "350x450";
                            exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage1");

                            #-----removing old image-----
                            @unlink(ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $post[$oldPartImg][$ctr]);
                            @unlink(ABSOLUTEPATH . __PRODUCTPARTLARGE__ . $post[$oldPartImg][$ctr]);
                            @unlink(ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $post[$oldPartImg][$ctr]);

                            $xmlArr[$xm]['query'] = addslashes($queryPart);
                            $xmlArr[$xm]['identification'] = $post['sId'];
                            $xmlArr[$xm]['section'] = "insert";
                            $xm++;
                        }
                    }
                } else {
                    $queryPart = "insert into " . TBL_PRODUCT_SPECIAL_PART . " set sId = '" . $post['sId'] . "', colorId = '" . $post['colorId'] . "', viewId = '" . $line->viewId . "', titleId = '" . $post[$title][$ctr] . "', partImage = '" . addslashes($post[$oldPartImg][$ctr]) . "', status = '1'";
                    $this->executeQry($queryPart);

                    $xmlArr[$xm]['query'] = addslashes($queryPart);
                    $xmlArr[$xm]['identification'] = $post['sId'];
                    $xmlArr[$xm]['section'] = "insert";
                    $xm++;
                }
                $ctr++;
            }
        }

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo '<script>window.parent.location="editProduct.php?id=' . base64_encode($post[pId]) . '&tab=2";</script>';
        exit;
    }

    function saveSpecialColor($post, $file) {
        $_SESSION['special_color_post_data'] = $post;
        $_SESSION['special_color_data'] = $file;

        $flag = true;
        $xmlArr = array();
        $xm = 1;
        
        if(!empty($file['colorImage']['name'])) {
               $filename = $file['colorImage']['name'];
               $filename = stripslashes($filename);
               $extension = findexts($filename);
               $extension = strtolower($extension);
               	
               if ($this->checkExtensions($extension)){
                        $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                        $target = ABSOLUTEPATH . __COLORTHUMB__ . $partImgName;
                        @chmod($target, 0777);
                        $filestatus = move_uploaded_file($file['colorImage']['tmp_name'], $target);
                        $addColorImage = "colorImage='".$partImgName."',";
                            }			
 				}
        
        
        $query = "Insert into " . TBL_SPECIAL_COLOR . " set $addColorImage colorCode = '#ffffff', code = '" . $post['colorCode'] . "'";
        $this->executeQry($query);
        $colorId = mysql_insert_id(); //----getting the color id
        #---------adding special product part--------		
        if ($flag) {
            $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $post[pId] . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
            $result = $this->executeQry($query);
            while ($line = $this->getResultObject($result)) {
                $ctr = 0;
                $viewName = preg_replace('!\s+!', '_', trim($line->viewName));
                $imageName = $viewName . $line->viewId;
                //$title = $line->viewName;
                foreach ($file[$imageName]['name'] as $imgName) {
                    if ($imgName != '') {
                        $filename = stripslashes($imgName);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);

                        if ($this->checkExtensions($extension)) {
                            $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                            $target = ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $partImgName;
                            $filestatus = move_uploaded_file($file[$imageName]['tmp_name'][$ctr], $target);

                            if ($filestatus) {
                                $ThumbImage = ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $partImgName;
                                $fileSize = "70x70";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                                $ThumbImage1 = ABSOLUTEPATH . __PRODUCTPARTLARGE__ . $partImgName;
                                $fileSize = "350x450";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage1");

                                $queryPart = "insert into " . TBL_PRODUCT_SPECIAL_PART . " set sId = '" . $post['sId'] . "', colorId = '" . $colorId . "', viewId = '" . $line->viewId . "', titleId = '" . $post[$viewName][$ctr] . "', partImage = '" . addslashes($partImgName) . "', status = '1'";
                                $this->executeQry($queryPart);
                                $xmlArr[$xm]['query'] = addslashes($queryPart);
                                $xmlArr[$xm]['identification'] = $styleId;
                                $xmlArr[$xm]['section'] = "insert";
                                $xm++;
                            }
                        }
                    }
                    $ctr++;
                }
            }
        }

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo '<script>window.parent.location="editProduct.php?id=' . base64_encode($post[pId]) . '&tab=2";</script>';
        exit;
    }

    function addNewProductStyle($post, $file) {

        $xmlArr = array();
        $xm = 1;
        #------reset basic style----------
        if ($post['isBasic'] == '1') {
            $this->executeQry("update " . TBL_PRODUCT_STYLE . " set isBasic = '0' where pId = '" . $post['pId'] . "'");
        }


        $queryStyle = "Insert into " . TBL_PRODUCT_STYLE . " set 
		pId = '" . $post['pId'] . "',
		styleCode = '" . $post['productCode'] . "',
                sign = '" . $post['sign'] . "',
                percent = '" . $post['percent'] . "',
		isBasic = '" . $post['isBasic'] . "',
		status = '1',
		isDeleted = '0'";

        $xmlArr[$xm]['query'] = addslashes($queryStyle);
        $xmlArr[$xm]['identification'] = $post['pId'];
        $xmlArr[$xm]['section'] = "insert";
        $xm++;

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        $conf = $this->executeQry($queryStyle);
        $styleId = mysql_insert_id();
        if ($conf) {
            if ($file['proStyleImage']['name']) {
                $filename = stripslashes($file['proStyleImage']['name']);
                $extension = findexts($filename);
                $extension = strtolower($extension);

                $proStyleImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                $target = ABSOLUTEPATH . __PRODUCTBASICORIGINAL__ . $proStyleImage;
                $filestatus = move_uploaded_file($file['proStyleImage']['tmp_name'], $target);
                if ($filestatus) {

                    #------updating record---------
                    $query = "UPDATE " . TBL_PRODUCT_STYLE . " set styleImage = '" . $proStyleImage . "' WHERE id ='$styleId'";
                    $this->executeQry($query);
                    $ThumbImage = ABSOLUTEPATH . __PRODUCTBASICTHUMB__ . $proStyleImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 120x98 $ThumbImage");
                }
            }

            $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $post[pId] . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
            $result = $this->executeQry($query);


            while ($line = $this->getResultObject($result)) {
                //~ echo "<pre>";
                //~ print_r($line);

                $ctr = 0;
                $imageName = str_replace(' ', '_', $line->viewName) . $line->viewId;
                $title = str_replace(' ', '_', $line->viewName) . 'Part';

                foreach ($file[$imageName]['name'] as $imgName) {
                    $colorable = "colorable" . $line->viewId;
                    $visible = "visible" . $line->viewId;
                    $sequence = "sequence" . $line->viewId;
                    if ($imgName != '') {
                        $filename = stripslashes($imgName);
                        $extension = findexts($filename);
                        $extension = strtolower($extension);

                        if ($this->checkExtensions($extension)) {
                            $partImgName = date("Ymdhis") . time() . rand() . '.' . $extension;
                            $target = ABSOLUTEPATH . __PRODUCTPARTORIGINAL__ . $partImgName;
                            $filestatus = move_uploaded_file($file[$imageName]['tmp_name'][$ctr], $target);

                            if ($filestatus) {
                                $ThumbImage = ABSOLUTEPATH . __PRODUCTPARTTHUMB__ . $partImgName;
                                $fileSize = "70x70";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage");

                                $ThumbImage1 = ABSOLUTEPATH . __PRODUCTPARTLARGE__ . $partImgName;
                                $fileSize = "350x450";
                                exec(IMAGEMAGICPATH . " $target -thumbnail $fileSize $ThumbImage1");

                                $queryPart = "insert into " . TBL_PRODUCT_PART . " set sId = '$styleId', viewId = '" . $line->viewId . "', titleId = '" . $post[$title][$ctr] . "', isColorable = '" . $post[$colorable][$ctr] . "', is_visible = '" . $post[$visible][$ctr] . "',sequence = '" . $post[$sequence][$ctr] . "', partImage = '" . addslashes($partImgName) . "', status = '1'";
                                $this->executeQry($queryPart);
                                $xmlArr[$xm]['query'] = addslashes($queryPart);
                                $xmlArr[$xm]['identification'] = $styleId;
                                $xmlArr[$xm]['section'] = "insert";
                                $xm++;
                            }
                        }
                    }
                    $ctr++;
                }
            }
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        //header("location:editProduct.php?id=".base64_encode($post[pId])."&tab=2");
        echo '<script>window.parent.location.reload();</script>';
        exit;
    }

    function getAllProductStyle($pId) {
        $query = "select * from " . TBL_PRODUCT_STYLE . " where pId = '" . $pId . "' and isDeleted = '0'";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $outPut = '';
        $ctr = 1;
        if ($num > 0) {
            while ($line = $this->getResultObject($result)) {
                $image = checkImgAvailable(__PRODUCTBASICTHUMB__ . $line->styleImage);
                if ($line->status == 1)
                    $status = "Active";
                else
                    $status = "Inactive";

                $div_id = "status" . $line->id;
                $outPut .= '<tr>';
                $outPut .= '<td>' . $ctr . '</td>';
                $outPut .= '<td>' . $line->styleCode . '</td>';
                $outPut .= '<td><img width="60" src="' . $image . '" /></td>';
                $outPut .= '<td style="vertical-align:top;">';
                $outPut .= $this->getAllSpecialColor($line->id, $pId);
                $outPut .='</td>';
                $outPut .= '<td><a href="addProductFabric.php?sId=' . $line->id . '&pId=' . $pId . '" rel="shadowbox;width=800;height=450;">Fabric Quality</a></td>';
                $outPut .= '<td><div id="' . $div_id . '" style="cursor:pointer;" onClick="javascript:changeStatus(\'' . $div_id . '\',\'' . $line->id . '\',\'productStyle\')">' . $status . '</div></td>';
                $outPut .= '<td><a href="viewProductStyle.php?sId=' . base64_encode($line->id) . '&pId=' . base64_encode($pId) . '" rel="shadowbox;width=780;height=425;"><img border="0" src="images/view.png"></a></td>';
                $outPut .= '<td><a href="editProductStyle.php?sId=' . base64_encode($line->id) . '&pId=' . base64_encode($pId) . '" rel="shadowbox;width=780;height=425;" class="i_pencil edit">EDIT</a></td>';
                $outPut .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=productStyle&type=delete&id=" . $line->id . "&pId=$pId'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a></td>";
                $outPut .='</tr>';
                $ctr++;
            }
        }
        else {
            $outPut .= '<tr>';
            $outPut .= '<th colspan="7"><span style="font-size:18px;color:#A52A2A;">No style found!</span></th>';
            $outPut .='</tr>';
        }
        return $outPut;
    }

    function getSpColorEdit($sId, $colorId) {
        $html = '';
        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $sId . "'");
        $html .='<input type="hidden" name="pId" value="' . $pId . '" />';
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $pId . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by PV.viewId";
        $result = $this->executeQry($query);
        while ($line = $this->getResultObject($result)) {
            $viewName = preg_replace('!\s+!', '_', trim($line->viewName));
            $html .= '<section>
						<label>Product Image (' . $line->viewName . ')</label>
						<div>';
            $queryPart = "Select PSP.id, PSP.titleId, PSP.partImage, PD.partTitle from " . TBL_PRODUCT_SPECIAL_PART . " as PSP INNER JOIN " . TBL_PARTDESC . " as PD ON (PSP.titleId = PD.partId) and PSP.sId = '" . $sId . "' and PSP.colorId = '" . $colorId . "' and PSP.viewId = '" . $line->viewId . "' and PD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";

            $res = $this->executeQry($queryPart);
            $html .='<table id="' . $line->viewName . '">';
            if ($this->getTotalRow($res) > 0) {
                $html .='<tr>
							<th>Part Title</th>
							<th>Part Image</th>
						</tr>';
                while ($row = $this->getResultObject($res)) {
                    $html .='<tr id="partTr' . $row->id . '">
									<th>' . $row->partTitle . '
									<input type="hidden" name="' . $line->viewName . '[]" value="' . $row->titleId . '" />
									</th>';
                    $image = checkImgAvailable(__PRODUCTPARTTHUMB__ . $row->partImage);
                    $html .='<td>
									<img style="float:left;" width="40" src="' . $image . '" />
									<input type="hidden" name="img' . $viewName . '[]" value="' . $row->partImage . '" />
									<input type="file" name="' . $viewName . $line->viewId . '[]" />
									</td>
								</tr>';
                }
            }
            $html .='</table>';


            $html .='</div></section>';
        }
        return $html;
    }

    function getAllSpecialColor($sId, $pId = '') {
        $html = '';
        $queryPart = "select * from " . TBL_PRODUCT_SPECIAL_PART . " where sId = '" . $sId . "' and isDeleted = '0' group by colorId";
        $result = $this->executeQry($queryPart);
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
                $image = SITE_URL . __PALLETIMGTHUMB__ . $this->fetchValue(TBL_SPECIAL_COLOR, "colorImage", "id = '" . $row->colorId . "'");

                $html .='<a rel="shadowbox;width=800;height=500" href="addSpecialColor.php?sId=' . base64_encode($sId) . '&colorId=' . $row->colorId . '&action=edit" /><img width="20" src="' . $image . '" /></a>
				 <a rel="shadowbox;width=800;height=500" href="addSpecialColor.php?sId=' . base64_encode($sId) . '&colorId=' . $row->colorId . '&action=edit">Edit</a> | 
				 <a href="javascript:void(NULL);" onclick="if(confirm(\'Are you sure to delete this Record?\')) {window.location.href=\'pass.php?action=product&type=deleteSpecialColor&sId=' . $sId . '&colorId=' . $row->colorId . '\'}else{}">
				 <img src="images/drop.png" height="13" width="13" border="0" title="Delete" />
				 </a><br />';
            }
        }

        $html .= '<hr />';
        $html .= '<a rel="shadowbox;width=800;height=500" href="addSpecialColor.php?sId=' . base64_encode($sId) . '&pId=' . base64_encode($pId) . '">Add More</a>';
        return $html;
    }

    function deleteSpecialColor($get) {
        $xmlArr = array();
        $xm = 1;
        //print_r($get);
        $queryCol = "update " . TBL_SPECIAL_COLOR . " set isDeleted = '1' where id = '" . $get['colorId'] . "'";
        $this->executeQry($queryCol);

        $xmlArr[$xm]['query'] = addslashes($queryCol);
        $xmlArr[$xm]['identification'] = $get['colorId'];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;

        $queryPart = "update " . TBL_PRODUCT_SPECIAL_PART . " set isDeleted = '1' where sId = '" . $get['sId'] . "' and colorId = '" . $get['colorId'] . "'";
        $this->executeQry($queryPart);

        $xmlArr[$xm]['query'] = addslashes($queryPart);
        $xmlArr[$xm]['identification'] = $get['sId'];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $get['sId'] . "'");
        header('Location:editProduct.php?id=' . base64_encode($pId) . '&tab=2');
        exit;
    }

    function getAllProductStyleForView($pId) {
        $query = "select * from " . TBL_PRODUCT_STYLE . " where pId = '" . $pId . "' and isDeleted = '0'";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $outPut = '';
        $ctr = 1;
        if ($num > 0) {
            while ($line = $this->getResultObject($result)) {
                $image = checkImgAvailable(__PRODUCTBASICTHUMB__ . $line->styleImage);
                if ($line->status == 1)
                    $status = "Active";
                else
                    $status = "Inactive";

                $div_id = "status" . $line->id;
                $outPut .= '<tr>';
                $outPut .= '<td>' . $ctr . '</td>';
                $outPut .= '<td>' . $line->styleCode . '</td>';
                $outPut .= '<td><img width="70" src="' . $image . '" /></td>';
                $outPut .= '<td>' . $status . '</td>';
                //$outPut .= '<td><a href="viewProductStyle.php?sId='.base64_encode($line->id).'&pId='.base64_encode($pId).'" rel="shadowbox;width=715;height=325;"><img border="0" src="images/view.png"></a></td>';
                //$outPut .= '<td><a href="editProductStyle.php?sId='.base64_encode($line->id).'&pId='.base64_encode($pId).'" rel="shadowbox;width=705;height=325onClose=sayHello();" class="i_pencil edit">EDIT</a></td>';
                //$outPut .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=productStyle&type=delete&id=".$line->id."&pId=$pId'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a></td>";
                $outPut .='</tr>';
            }
        }
        else {
            $outPut .= '<tr>';
            $outPut .= '<th colspan="7"><span style="font-size:18px;color:#A52A2A;">No style found!</span></th>';
            $outPut .='</tr>';
        }
        return $outPut;
    }

    function getSpecialStylePart($pId, $sId) {

        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $pId . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by PV.viewId";
        $result = $this->executeQry($query);
        $html = '';
        while ($line = $this->getResultObject($result)) {
            $html .= '<section>
                                    <label>Product Image (' . $line->viewName . ')</label>
                                    <div>';
            $queryPart = "Select PP.id, PP.titleId, PD.partTitle from " . TBL_PRODUCT_PART . " as PP INNER JOIN " . TBL_PARTDESC . " as PD ON (PP.titleId = PD.partId) and PP.sId = '" . $sId . "' and PP.viewId = '" . $line->viewId . "' and PD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by sequence asc";
            $res = $this->executeQry($queryPart);
            $html .='<table id="' . $line->viewName . '">';
            if ($this->getTotalRow($res) > 0) {
                $html .='<tr>
							<th>Part Title</th>
							<th>Part Image</th>
						</tr>';
                while ($row = $this->getResultObject($res)) {
                    $viewName = preg_replace('!\s+!', '_', trim($line->viewName));
                    $html .='<tr id="partTr' . $row->id . '">
									<th>' . $row->partTitle . '
									<input type="hidden" name="' . $line->viewName . '[]" value="' . $row->titleId . '" />
									</th>
									<td>
									<input type="file" class="validate[required] text-input" name="' . $viewName . $line->viewId . '[]" />
									</td>
								</tr>';
                }
            }
            $html .='</table>';


            $html .='</div></section>';
        }
        return $html;
    }

    function editProductStylePart($pId, $sId) {
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $pId . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by PV.viewId";
        $result = $this->executeQry($query);
        $html = '';
        while ($line = $this->getResultObject($result)) {
            $html .= '<section>
						<label>Product Image (' . $line->viewName . ')</label>
						<div class="pro">
						<!--<input type="button" onclick="return addMorePart(\'' . $line->viewName . '\',\'' . $line->viewId . '\',\'partTitle' . $line->viewId . '\');" style="float:right;margin:10px;" name="removeView" value="Add More Part" />--><br />';
            $queryPart = "Select PP.id, PP.isColorable, PP.titleId, PP.partImage,PP.sequence,PP.is_visible from " . TBL_PRODUCT_PART . " as PP where PP.sId = '" . $sId . "' and PP.viewId = '" . $line->viewId . "'";
            $res = $this->executeQry($queryPart);
            $html .='<table id="' . $line->viewName . '">';
            $html .='<tr>
                                                <th>Sequence</th>
                                                <th>Is Visible</th>
						<th>Is Colorable</th>
						<th>Title</th>
						<th>Image</th>
				</tr>';
            if ($this->getTotalRow($res) > 0) {
                $ctr = 0;
                while ($row = $this->getResultObject($res)) {
                    $chk = '';
                    if ($row->isColorable == '1') {
                        $chk1 = 'selected = "selected"';
                        $chk2 = '';
                    } else {
                        $chk2 = 'selected = "selected"';
                        $chk1 = '';
                    }
                    
                    if ($row->is_visible == '1') {
                        $chkv1 = 'selected = "selected"';
                        $chkv2 = '';
                    } else {
                        $chkv2 = 'selected = "selected"';
                        $chkv1 = '';
                    }
                                        
                    
                    $image = checkImgAvailable(__PRODUCTPARTTHUMB__ . $row->partImage);
                    $html .='<tr id="partTr' . $row->id . '"><td style="text-align:left;">
                                                                    <input type="text" name="sequence'.$line->viewId.'[]"  class="text-input" value="' . $row->sequence . '"/>
                                                                    </td>
									<td style="text-align:left;">
										<select name="visible' . $line->viewId . '[]"><option value="1" '.$chkv1.'>Yes</option><option value="0" '.$chkv2.'>No</option></select>
									</td>
									<td style="text-align:left;">
										<select name="colorable' . $line->viewId . '[]"><option value="1"' . $chk1 . '>Yes</option><option value="0" ' . $chk2 . '>No</option></select>
									</td>
									<td style="text-align:left;">
										<select name="partTitle' . $line->viewId . '[]">' . $this->getProductPartList($row->titleId) . '</select>
									</td>
									<td>
									<img width="60" src="' . $image . '" /><br />
									<input type="file" name="' . $line->viewName . $line->viewId . '[]" />
									<input type="hidden" name="oldImg' . $line->viewId . '[]" value="' . $row->partImage . '" />
									</td>
									<!--<td><input  type="button" onclick="return removePart(\'partTr' . $row->id . '\');" value="Remove Part" />&nbsp;</td>-->
								</tr>';
                    $ctr++;
                }
            }
            $html .='</table>';

            $html .='</div></section>';
        }
        return $html;
    }

    function getProductPartList($id = '') {
        $html = '';
        $query = "select t.id,td.partTitle from " . TBL_PART . " as t , " . TBL_PARTDESC . " as td where  t.id = td.partId and t.isDeleted = '0' and t.status = '1' and td.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by td.partTitle";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            while ($row = $this->getResultObject($sql)) {
                $sel = $row->id == $id ? 'selected="selected"' : '';
                $html .='<option ' . $sel . ' value="' . $row->id . '">' . $row->partTitle . '</option>';
            }
        } else {
            $html = '<option value="">Not Available</option>';
        }
        return $html;
    }

    function addProductStyle($pId) {
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $pId . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by PV.viewId";
        $result = $this->executeQry($query);
        $html = '';
        while ($line = $this->getResultObject($result)) {
            $html .= '<section>
						<label>Product Image (' . $line->viewName . ')</label>
						<div class="pro">
							<input type="hidden" id="' . str_replace(' ', '_', $line->viewName) . 'ProPartHid" value="1" />
							<table id="' . str_replace(' ', '_', $line->viewName) . 'ProPart" >                                                            
								<tr>
                                                                        <th>Sequence</th>
                                                                        <th>Is Visible</th>
									<th>Is Colorable</th>
									<th>Title</th>
									<th>Image</th>
									<th>Operation</th>
								</tr>
								<tr>
                                                                <td style="text-align:left;">
                                                                    <input type="text" name="sequence'.$line->viewId.'[]" value="0" class="text-input" />
                                                                    </td>
									<td style="text-align:left;">
										<select name="visible' . $line->viewId . '[]"><option value="1">Yes</option><option value="0">No</option></select>
									</td>
                                                                        <td style="text-align:left;">
										<select name="colorable' . $line->viewId . '[]"><option value="1">Yes</option><option value="0">No</option></select>
									</td>
									<td style="text-align:left;">
										<select name="' . str_replace(' ', '_', $line->viewName) . 'Part[]">
											' . $this->getProductPartList() . '
										</select>
									</td>
									<td style="text-align:left;">
										<input type="file" class="validate[required] text-input" name="' . str_replace(' ', '_', $line->viewName) . $line->viewId . '[]" />									
									</td>
									<td>
										<input type="button" onclick="return addMorePart(\'' . str_replace(' ', '_', $line->viewName) . 'ProPart\',\'' . str_replace(' ', '_', $line->viewName) . $line->viewId . '\',\'' . str_replace(' ', '_', $line->viewName) . 'Part\',\'' . $line->viewId . '\');" value="Add More Part" />
									</td>
								</tr>
							</table>
						</div>
					</section>';
        }
        return $html;
    }

    function deleteStyle($get) {
        $xmlArr = array();
        $xm = 1;
        $query = "update " . TBL_PRODUCT_STYLE . " set isDeleted = '1' where id = '$get[id]'";
        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;

        $xm = 2;
        $query1 = "update " . TBL_PRODUCT_FABRIC . " set isDeleted = '1' where pId = '" . $get['pId'] . "' and sId = '" . $get['id'] . "'";
        $xmlArr[$xm]['query'] = addslashes($query1);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "delete";
        $xm++;

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);



        if ($this->executeQry($query) && $this->executeQry($query1))
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");
        else
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");

        header('Location:editProduct.php?id=' . base64_encode($get['pId']) . '&tab=2');
        exit();
    }

    function changStyleStatus($get) {
        $xmlArr = array();
        $xm = 1;

        $status = $this->fetchValue(TBL_PRODUCT_STYLE, "status", "1 and id = '$get[id]'");
        if ($status == 1) {
            $stat = 0;
            $status = "Inactive,0";
        } else {
            $stat = 1;
            $status = "Active,1";
        }

        $query = "update " . TBL_PRODUCT_STYLE . " set status = '$stat' where id = '$get[id]'";
        $xmlArr[$xm]['query'] = addslashes($query);
        $xmlArr[$xm]['identification'] = $get['id'];
        $xmlArr[$xm]['section'] = "update";
        $xm++;

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        if ($this->executeQry($query))
            $this->logSuccessFail('1', $query);
        else
            $this->logSuccessFail('0', $query);
        echo $status;
    }

    function viewProductPart($pId, $styleId) {
        $query = "Select PV.id, PV.viewId, VD.viewName from " . TBL_PRODUCT_VIEW . " as PV INNER JOIN " . TBL_VIEWDESC . " as VD ON (PV.viewId = VD.viewId) where PV.pId = '" . $pId . "' and VD.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by PV.viewId";
        $result = $this->executeQry($query);
        $outPut = '';
        $outPut .='<table>';
        //$styleId = $this->fetchValue(TBL_PRODUCT_STYLE,"id","pId = '".$pId."'");
        while ($line = $this->getResultObject($result)) {
            $outPut .='<tr><th colspan="4">' . $line->viewName . '</th></tr>';
            $queryPart = "select * from " . TBL_PRODUCT_PART . " where sId = '" . $styleId . "' and viewId = '" . $line->viewId . "' and isDeleted = '0'";
            $rec = $this->executeQry($queryPart);
            $total = $this->getTotalRow($rec);
            if ($total > 0) {
                $ctr = 1;
                $outPut .='<tr>';
                while ($row = $this->getResultObject($rec)) {
                    if ($ctr % 2 == 0 && $ctr != $total)
                        $outPut .='</tr><tr>';

                    $outPut .='<td>' . $this->fetchValue(TBL_PARTDESC, 'partTitle', "partId = '" . $row->titleId . "' and langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'") . '</td>';
                    $outPut .='<td><img width="60" src="' . SITE_URL . __PRODUCTPARTTHUMB__ . $row->partImage . '" /></td>';
                    $ctr++;
                }
                $outPut .='</tr>';
            }
            else {
                $outPut .='<tr><th colspan="2">Part(s) not available.</th></tr>';
            }
        }
        $outPut .='</table>';

        return $outPut;
    }

    function getProductSize($sizeArr = '') {
        $table = '<table style="width:300px; float:left;">';
        $sql = "Select g.id, gd.groupName from " . TBL_SIZE_GROUP . " as g INNER JOIN " . TBL_SIZE_GROUPDESC . " as gd ON (g.id = gd.Id) and g.status = '1' and gd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
        $result = $this->executeQry($sql);
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
                $table .= '<tr>';
                $table .= '<th colspan="2"><h3>' . $row->groupName . '</h3></th>';
                $table .='</tr>';

                $query = "select " . TBL_SIZE . ".*," . TBL_SIZEDESC . ".sizeName from " . TBL_SIZE . ", " . TBL_SIZEDESC . " where 1 and " . TBL_SIZE . ".status  = '1' and " . TBL_SIZE . ".id = " . TBL_SIZEDESC . ".Id and " . TBL_SIZE . ".groupId = '" . $row->id . "' and " . TBL_SIZEDESC . ".langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by " . TBL_SIZE . ".id asc";
                $sql2 = $this->executeQry($query);
                $num2 = $this->getTotalRow($sql2);

                //$table .= '<tr>';
                //$table .= '<th>&nbsp;</th>';
                //$table .= '<td>Size Title</td>';			
                //$table .='</tr>';
                if ($num2 > 0) {
                    while ($line2 = $this->getResultObject($sql2)) {
                        $chk = '';
                        if ($sizeArr != '') {
                            if (in_array($line2->id, $sizeArr)) {
                                $chk = 'checked="checked"';
                            }
                        }
                        $table .= '<tr>';
                        $table .= '<td><input type="checkbox" value="' . $line2->id . '" name="size[]" ' . $chk . ' /></td>';
                        $table .= '<td>' . $line2->sizeName . '</td>';
                        $table .='</tr>';
                    }
                } else {
                    $table .= '<tr><th colspan="2">Oops, no size.</th></tr>';
                }
            }
        } else {
            $table .= '<tr><th colspan="2">Oops, no size.</th></tr>';
        }

        $table .= '</table>';
        return $table;
    }

    function getEditProdSize($productid) {
        $query = "select sizeId from " . TBL_PRODUCTSIZE . " where pId = '$productid'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $outPut = array();
        if ($num > 0)
            ; {
            $i = 0;
            while ($result = $this->getResultObject($sql)) {
                $outPut[$i] = $result->sizeId;
                $i++;
            }
        }
        return $outPut;
    }

    function getCategoryListSingle($id) {
        $genTable = "";
        $aaaa = "select a.id,ad.categoryName from " . TBL_CATEGORY . " as a, " . TBL_CATEGORY_DESCRIPTION . " as ad where 1 and a.parent_id = 0 and a.status = '1' and a.isDeleted = '0' and a.id = ad.catId and ad.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by a.id";

        $sql = $this->executeQry($aaaa);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            while ($line = $this->getResultObject($sql)) {
                //$disable = $this->checkProductLimit($line->id);			
                $sel = ($id == $line->id) ? 'selected="selected"' : '';
                $genTable .= '<option value="' . $line->id . '" ' . $sel . '>' . stripslashes($line->categoryName) . '</option>';
                $genTable .= $this->subCategorySingle($line->id, 1, $id);
            }
        }
        return $genTable;
    }

    function subCategorySingle($pid, $dashes, $id) {
        //echo $id;		
        $dash = str_repeat('&nbsp;&nbsp;', $dashes);
        $bbb = "select a.id,ad.categoryName from " . TBL_CATEGORY . " as a, " . TBL_CATEGORY_DESCRIPTION . " as ad where 1 and a.parent_id = '" . $pid . "' and a.status = '1' and a.isDeleted = '0' and a.id = ad.catId and ad.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "' order by a.id";
        $sql = $this->executeQry($bbb);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            while ($line = $this->getResultObject($sql)) {
                //if($this->checkForCategory($line->id)){
                //$disable = $this->checkProductLimit($line->id);
                $sel = ($id == $line->id) ? 'selected="selected"' : '';
                $genTable .= '<option value="' . $line->id . '" ' . $sel . '>' . $dash . stripslashes($line->categoryName) . '</option>';
                $genTable .= $this->subCategorySingle($line->id, ($dashes + 1), $id);
                //}
            }
            return $genTable;
        } else {
            return false;
        }
    }

    function checkProductLimit($catId) {
        $disable = '';
        $query = "Select count(*) as total from " . TBL_PRODUCT . " where catId = '" . $catId . "'";
        $result = $this->executeQry($query);
        $line = $this->getResultObject($result);
        $total = $line->total;
        if ($total >= $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='NUMBER_OF_PRODUCT_IN_CATEGORY'")) {
            $disable = 'disabled="disabled"';
        }
        return $disable;
    }

    function getSpPalletDetails($colorId) {
        $query = "Select * from " . TBL_SPECIAL_COLOR . " where id = '" . $colorId . "'";
        $result = $this->executeQry($query);
        return $this->getResultObject($result);
    }

    function getFabricQuality($fabricArr = '',$pid=0) {	
        $table = '<table>';
       $sql = "select f.id, fd.fabricName from " . TBL_FABRIC . " as f INNER JOIN " . TBL_FABRICDESC . " as fd ON (f.id = fd.fabricId) and f.status = '1' and fd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'";
        $result = $this->executeQry($sql);
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
				
				$num_check=$this->checkProductFabricGroup($pid,$row->id);  /*point9 changes*/
				if($num_check){$checked='checked="checked"';}else{$checked='';}
                $table .= '<tr>';
                $table .= '<th colspan="2"><input type="checkbox" name="group_fabric" class="chb" value="'.$row->id.'" '.$checked.'><h3>' . $row->fabricName . '</h3></th>';
                $table .='</tr>';

                $query = "select * from " . TBL_FABRICCHARGE . " where status  = '1' and isDeleted = '0' and fId = '" . $row->id . "'";
                $sql2 = $this->executeQry($query);
                $num2 = $this->getTotalRow($sql2);

                if ($num2 > 0) {
                    while ($line2 = $this->getResultObject($sql2)) {
                        $chk = '';
                        if ($fabricArr != '') {
                            if (in_array($line2->id, $fabricArr)) {
                                $chk = 'checked="checked"';
                            }
                        }
                        $table .= '<tr>';
                        $table .= '<td><input type="checkbox" value="' . $line2->id . '" name="fabric[]" ' . $chk . ' /></td>';
                        $table .= '<td>' . $line2->fQuality . '</td>';
                        $table .='</tr>';
                    }
                } else {
                    $table .= '<tr><th colspan="2">Oops, fabric not available.</th></tr>';
                }
            }
        } else {
            $table .= '<tr><th colspan="2">Oops, no size.</th></tr>';
        }

        $table .= '</table>';
        return $table;
    }

    function getEditProdFabric($productid, $sId) {
        $query = "select fId from " . TBL_PRODUCT_FABRIC . " where pId = '$productid' and sId = '" . $sId . "'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        $outPut = array();
        if ($num > 0)
            ; {
            $i = 0;
            while ($result = $this->getResultObject($sql)) {
                $outPut[$i] = $result->fId;
                $i++;
            }
        }
        return $outPut;
    }

    function addProductFabric($post) {			
        $xmlArr = array();
        $xm = 1;
        if ($post['pId'] != '' && $post['sId'] != '') {
            $this->executeQry("Delete from " . TBL_PRODUCT_FABRIC . " where pId = '" . $post['pId'] . "' and sId = '" . $post['sId'] . "'");

            $query = Array();
            foreach ($post['fabric'] as $fabricId) {
                $query[] = " ('" . $post['pId'] . "', '" . $post['sId'] . "', '" . $fabricId . "')";
            }
            $queryVal = implode(' ,', $query);
            $querySize = "Insert into " . TBL_PRODUCT_FABRIC . " (pId, sId, fId) values " . $queryVal;
            $sql = $this->executeQry($querySize);
            $xmlArr[$xm]['query'] = addslashes($querySize);
            $xmlArr[$xm]['identification'] = $post['pId'];
            $xmlArr[$xm]['section'] = "insert";
            $xm++;
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your information has been updated successfully!!!");
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        
        $this->addProductFabricGroup($post);   /* For point 9 */ 
        echo '<script>window.parent.location.reload();</script>';
        //header("location:editProduct.php?id=".base64_encode($post[pId])."&tab=2");
        exit;
    }

    function getProductZoomableImage($id) {
        $result = $this->executeQry("select * from " . TBL_PRODUCT_ZOOMIMAGE . " where pId = '" . $id . "'");
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            $ctr = 1;
            while ($line = $this->getResultObject($result)) {
                $image = checkImgAvailable(__ZOOMPRODUCTTHUMB__ . $line->zoomImage);

                $div_id = "status" . $line->id;
                $outPut .= '<tr>';
                $outPut .= '<td>' . $ctr . '</td>';
                $outPut .= '<td><img width="60" src="' . $image . '" /></td>';

                $outPut .= '<td><a href="editProductZoomImage.php?id=' . base64_encode($line->id) . '&pId=' . base64_encode($id) . '" rel="shadowbox;width=780;height=425;" class="i_pencil edit">EDIT</a></td>';
                $outPut .= "<td><a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Record  ?')){window.location.href='pass.php?action=productZoomImage&type=delete&id=" . $line->id . "&pId=$id'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a></td>";
                $outPut .='</tr>';
                $ctr++;
            }
        } else {
            $outPut .= '<tr>';
            $outPut .= '<th colspan="7"><span style="font-size:18px;color:#A52A2A;">No Record found!</span></th>';
            $outPut .='</tr>';
        }
        return $outPut;
    }

    function addZoomableImage($post, $files) {
//        echo "<pre>";
//        print_r($post);
//        print_r($files);exit;

        $flag = true;
        $xmlArr = array();
        $xm = 1;

        foreach ($files as $key => $file) {

            if ($file['name']) {
                $filename = stripslashes($file['name']);
                $ext = findexts($filename);
                $extension = strtolower($ext);

                $viewImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                $target = ABSOLUTEPATH . __ZOOMPRODUCTORIGINAL__ . $viewImage;
                $filestatus = move_uploaded_file($file['tmp_name'], $target);
                if ($filestatus) {
                    $query = "Insert into " . TBL_PRODUCT_ZOOMIMAGE . " set pId = '" . $post['pId'] . "', zoomImage = '" . $viewImage . "'";
                    $this->executeQry($query);

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTTHUMB__ . $viewImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 46x60! $ThumbImage");

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTLARGE__ . $viewImage;
                    //exec(IMAGEMAGICPATH." $target -thumbnail 275x412! $ThumbImage");                  
                    exec(IMAGEMAGICPATH . " $target -thumbnail 293x380! $ThumbImage");

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTEXTRALARGE__ . $viewImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 945x1417! $ThumbImage");

                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been added successfully");
                } else {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Failed to add Record.");
                    $flag = false;
                }
            }
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo '<script>window.parent.location="editProduct.php?id=' . base64_encode($post[pId]) . '&tab=5";</script>';
        //~ header("location:editProduct.php?id=".base64_encode($post[pId])."&tab=2");
        //echo '<script>window.location.parent.reload();</script>';
        exit;
    }

    function deleteZoomableImage($get) {
        $imageName = $this->fetchValue(TBL_PRODUCT_ZOOMIMAGE, 'zoomImage', 'id = ' . $get['id']);
        $rs = $this->executeQry("Delete from " . TBL_PRODUCT_ZOOMIMAGE . " where id = '" . $get['id'] . "'");
        $rs = 1;
        if ($rs) {
            //removing old images
            @unlink(ABSOLUTEPATH . __ZOOMPRODUCTTHUMB__ . $imageName);
            @unlink(ABSOLUTEPATH . __ZOOMPRODUCTLARGE__ . $imageName);
            @unlink(ABSOLUTEPATH . __ZOOMPRODUCTORIGINAL__ . $imageName);
            @unlink(ABSOLUTEPATH . __ZOOMPRODUCTEXTRALARGE__ . $imageName);

            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Your Information has been deleted successfully");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Opps! somthing went wrong,try later again.");
        }

        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);

        header('Location:editProduct.php?id=' . base64_encode($get['pId']) . '&tab=5');
        exit;
    }

    function editZoomableImage($post, $files) {
        $flag = true;
        $xmlArr = array();
        $xm = 1;

        if ($files['zoomImg']['name'] != '') {
            if ($files['zoomImg']['name']) {
                $filename = stripslashes($files['zoomImg']['name']);
                $ext = findexts($filename);
                $extension = strtolower($ext);

                $viewImage = date("Ymdhis") . time() . rand() . '.' . $extension;
                $target = ABSOLUTEPATH . __ZOOMPRODUCTORIGINAL__ . $viewImage;
                $filestatus = move_uploaded_file($files['zoomImg']['tmp_name'], $target);

                if ($filestatus) {
                    //removing old images
                    @unlink(ABSOLUTEPATH . __ZOOMPRODUCTTHUMB__ . $post['oldStyleImage']);
                    @unlink(ABSOLUTEPATH . __ZOOMPRODUCTLARGE__ . $post['oldStyleImage']);
                    @unlink(ABSOLUTEPATH . __ZOOMPRODUCTORIGINAL__ . $post['oldStyleImage']);
                    @unlink(ABSOLUTEPATH . __ZOOMPRODUCTEXTRALARGE__ . $post['oldStyleImage']);

                    //updating images
                    $query = "update " . TBL_PRODUCT_ZOOMIMAGE . " set zoomImage = '" . $viewImage . "' where id = '" . $post['id'] . "'";
                    $this->executeQry($query);

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTTHUMB__ . $viewImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 46x60! $ThumbImage");

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTLARGE__ . $viewImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 275x412! $ThumbImage");

                    $ThumbImage = ABSOLUTEPATH . __ZOOMPRODUCTEXTRALARGE__ . $viewImage;
                    exec(IMAGEMAGICPATH . " $target -thumbnail 945x1417! $ThumbImage");

                    $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully");
                } else {
                    $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "Failed to update Record.");
                    $flag = false;
                }
            }
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been updated successfully");
        }
        $xmlInfoObj = new XmlInfo();
        $xmlInfoObj->addXmlData($xmlArr);
        echo '<script>window.parent.location="editProduct.php?id=' . base64_encode($post[pId]) . '&tab=5";</script>';
        //~ header("location:editProduct.php?id=".base64_encode($post[pId])."&tab=2");
        //echo '<script>window.location.parent.reload();</script>';
        exit;
    }
    /**
     * @function addProductFabricGroup
     * point 9
     * This is addProductFabricGroup method which is use to add fbric group belong to particular product.
     * every product belong to only one product group.
     * @access public
     */
    public function addProductFabricGroup($post)
    {
	 $this->executeQry("Delete from " . TBL_PRODUCT_FABRICGROUP . " where pId = '" . $post['pId'] . "'");
	 $date=date('Y-m-s H:i:s');
	 
	 $querySize = "Insert into " . TBL_PRODUCT_FABRICGROUP . " (pId, fabric_gruop_id,add_date,update_date) values ('".$post['pId']."','".$post['group_fabric']."','".$date."','".$date."') ";
     $sql = $this->executeQry($querySize);	 	
	}
	/**
     * @function checkProductFabricGroup
     * point 9
     * This is checkProductFabricGroup method which is check product belong to this group or not.
     * every product belong to only one product group.
     * @access public
     */
	public function checkProductFabricGroup($pid,$gid)
	{
		 $querySize = "select * from " . TBL_PRODUCT_FABRICGROUP . "  where `pid`='".$pid."' and `fabric_gruop_id`='".$gid."'";
		$sql = $this->executeQry($querySize);	
		$num = $this->getTotalRow($sql);
		return $num;
	}

}

// End Class
?>	

<?php
//ob_start();
session_start();
class Title extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	function valDetail() {
		
		$cond = "1 and t.id = td.id and t.isDeleted = '0' and td.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		if($_REQUEST['searchtxt'] != SEARCHTEXT){
			$searchtxt = $_REQUEST['searchtxt'];
			$cond .= " AND (td.titleName LIKE '%$searchtxt%' ) ";
		}
		$query = "select t.*,td.titleName from ".TBL_TITLE." as t , ".TBL_TITLEDESC." as td where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {					
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"	titleName";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			if($row > 0) {			
				$i = 1;	
				while($line = $this->getResultObject($rst)) {
					$currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";					
					$genTable .= '<tr class="dragbox-content '.$highlight.'" id="'.$line->id.'" >
						<th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
						<td>'.$i.'</td>
						<td>'.stripslashes($line->titleName).'</td>						
						<td>';
					if($menuObj->checkEditPermission()) 							
						//$genTable .= '<div id="'.$div_id.'" '.$statusfun.'>'.$status.'</div>';
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'title\')">'.$status.'</div>';																												
				  	$genTable .= '</td><td>';									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a class="i_pencil edit" href="editTitle.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';						
					$genTable .= '</td><td>';				
					if($menuObj->checkDeletePermission()) {					
						$genTable .= "<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=title&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					}
					
					$genTable .= '</td></tr>';
					$i++;	
				}

				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';
							switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	function changeValueStatus($get) {
		$status=$this->fetchValue(TBL_TITLE,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
	
		$query = "update ".TBL_TITLE." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";
		//echo $query;exit;
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteValue($get) {
		//$result=$this->deleteRec(TBL_ZONE,"path like '%-$get[id]-%'");		
		
		$this->executeQry("update ".TBL_TITLE." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'");
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageTitle.php?page=$get[page]&limit=$get[limit]';</script>";
	}
		
	
	
	function addRecord($post) {		
     	$query = "insert into ".TBL_TITLE." set status  = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."'";
		$res = $this->executeQry($query);
		$inserted_id = mysql_insert_id();
		if($res) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
			
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$titleName = 'titleName_'.$line->id;
				$query = "insert into ".TBL_TITLEDESC." set  Id  = '$inserted_id', langId = '".$line->id."' , titleName  = '".addslashes($post[$titleName])."' ";	
				if($this->executeQry($query)) 
					$this->logSuccessFail('1',$query);		
				else 	
					$this->logSuccessFail('0',$query);
			}	
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		}	
		header("Location:addTitle.php");exit;							
	}
	
	function editRecord($post) {
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");
		$num = $this->getTotalRow($rst);		
		if($num){			
			while($line = $this->getResultObject($rst)) {					
				$title = 'titleName_'.$line->id;
				$sql = $this->selectQry(TBL_TITLEDESC, '1 and Id = "' . $post[id] . '" and langId = "' . $line->id . '"', '', '');
                $numrows = $this->getTotalRow($sql);
                if ($numrows == 0) {
                    $query = "insert into " . TBL_TITLEDESC . " set Id = '$post[id]', langId = '" . $line->id . "', titleName = '" . $post[$title] . "'";

                    if ($this->executeQry($query))
                        $this->logSuccessFail('1', $query);
                    else
                        $this->logSuccessFail('0', $query);
                }
				else
				{
					$query = "update ".TBL_TITLEDESC." set titleName = '".addslashes($post[$title])."' where 1 and id = '$post[id]' and langId = '".$line->id."'";
					//echo $query;exit;
					
					if($this->executeQry($query)) 
						$this->logSuccessFail('1',$query);		
					else 	
						$this->logSuccessFail('0',$query);	
				}
			}	
		}					
		
		$query = "update ".TBL_TITLE." set  modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '".$post[id]."'";
		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);	
			
		echo "<script>window.location.href='manageTitle.php?page=$post[page]';</script>";
	}


	function deleteAllValues($post){
	
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageTitle.php?page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_ZONE," path like '%-$val-%'");	
					$this->executeQry("update ".TBL_TITLE." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$val'");
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_ZONE,"cat_id='$val'");	
					$sql="update ".TBL_TITLE." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_ZONE,"cat_id='$val'");	
					$sql="update ".TBL_TITLE." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageTitle.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	/*
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_TITLE." where title_id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect("manageTitle.php");
		}	
	}
	*/
			
	function checkTitleExists($titleName, $langId, $id='') {
        $titleName = trim($titleName);
        if($id != ''){
       		$query = "select t.*, td.titleName from " .TBL_TITLE." as t, ". TBL_TITLEDESC . " as td where t.id = td.Id and t.isDeleted = '0' and td.langId = '" . $langId . "' and titleName = '" . addslashes($titleName) . "' and t.id != '" . $id . "'";
        }else {
     		$query = "select t.*, td.titleName from " .TBL_TITLE." as t, ". TBL_TITLEDESC . " as td where t.id = td.Id and t.isDeleted = '0' and td.langId = '" . $langId . "' and titleName = '" . addslashes($titleName) . "'";
        }
        $sql = $this->executeQry($query);
        $row = $this->getTotalRow($sql);
        if($row > 0){
        	return true;
        }else {
        	return false;
        }
    }
	
	
}// End Class
?>	

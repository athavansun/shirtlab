<?php 
session_start();
class DesignCategory extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }

	function valDetail($cid) {
	
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".
		$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where 
		$cond ";
		
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging----------------------- -------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
		    $orderby = $_GET[orderby]? $_GET[orderby]:"c.sequence";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
		   	$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			//echo $query;exit;					
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			// $MULTILEVEL_CATEGORY = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_DESIGN_CATEGORY' and fulfillment_id='".$_SESSION[FULFILLMENTID]."'");
			$MULTILEVEL_CATEGORY = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_DESIGN_CATEGORY'");
			if($row > 0) {			
				$i = 1;	
				while($line = $this->getResultObject($rst)) {
					$id1 = substr($line->path,1);
					$id2 = substr($id1,0,-1);
					if($id2){
						$id21 = explode("-", $id2);
					}
				   	$currentOrder	.=	$line->id.","; 
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = 'Inactive';
					else
						$status = 'Active';
					
					$sqlSubCat = $this->executeQry("select count(*) as cnt from ".TBL_DESIGNCATEGORY." where 1 and parent_id = '".
					$line->id."' and isDeleted = '0'");
					$lineSubCat = $this->getResultObject($sqlSubCat);
					$subCat = $lineSubCat->cnt?"(".$lineSubCat->cnt.")":"";
					//$res = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='levelDesign'  and fulfillment_id='".$_SESSION[FULFILLMENTID]."'");
					$res = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='levelDesign'");
					
					if($MULTILEVEL_CATEGORY == 1 && count($id21) < $res) {
						$categoryName = '<a href="manageDesignCategory.php?cid='.$line->id.'">'.$this->fetchValue(
						TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION[
						'DEFAULTLANGUAGE']."'").'</a>&nbsp;<a href="manageDesignCategory.php?cid='.$line->id.'">'.$subCat.'
						</a>';
						$categoryTitle = $subCat;
					} else {
						$categoryName = $this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id
						."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
						$categoryTitle = $categoryName;
					}
					$genTable .= '<tr class="'.$highlight.'" id="'.$line->id.'" >
								 	<th><input name="chk[]" value="'.$line->id.'" type="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$line->id.'</td>
									<td>'.stripslashes($categoryName).'</td>
									<td>';
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'design_category\')">'.$status.'</div>';
					$genTable .= '</td>
					<td><a rel="shadowbox;width=705;height=325" title="'.$categoryTitle.'" href="viewDesignCategory.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></td>
					<td>';
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a class="i_pencil edit" href="editDesignCategory.php?id='.base64_encode($line->id).'&page='.$page.'&cid='.$cid.'">Edit</a>';
					$genTable .= '</td><td>';
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a class='i_trashcan edit' href='javascript:void(NULL);'  onClick=\"if(confirm('Are You Sure To Delete This Record?')){window.location.href='pass.php?action=design_category&type=delete&cid=".$cid."&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					$genTable .= '</td> 
					</tr>';
					$i++;	
				}
					$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td> <td class='page_info' align='center' width='200'><inputtype='hidden' name='page' value='".$currpage."'>Total Records Found: ".$totalrecords."</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="alert i_access_denied red">Sorry! Record Not Found</div>';
		}	
		return $genTable;
	}	
	
	/*function valDetail($cid) {
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and cd.categoryName != 'other'";
		$query = "select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where $cond ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {			
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET[limit]); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			$orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
		    $order = $_GET[order]? $_GET[order]:"ASC";   
            $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
									
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
			$MULTILEVEL_TBL_CATEGORY = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='MULTILEVEL_DESIGN_CATEGORY'");
			if($row > 0) {			
				$i = 1;	
				$genTable .= '<div class="column" id="column1">';			
				while($line = $this->getResultObject($rst)) {
				    $id1 = substr($line->path,1);
					$id2 = substr($id1,0,-1);
					if($id2){
						$id21 = explode("-", $id2);
					}
					
					$currentOrder	.=	$line->id.",";
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					$div_id = "status".$line->id;
					if ($line->status==0)
						$status = "Inactive";
					else
						$status = "Active";
					
					
					$sqlSubCat = $this->executeQry("select count(*) as cnt from ".TBL_DESIGNCATEGORY." where 1 and parent_id = '".$line->id."' ");
					$lineSubCat = $this->getResultObject($sqlSubCat);
					$subCat = $lineSubCat->cnt?"(".($lineSubCat->cnt-1).")":"";
					$res = $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='levelDesign'");
					//echo count($id21); 
					if($MULTILEVEL_TBL_CATEGORY == 1 && count($id21) < $res)
						$categoryName = '<a href="manageDesignCategory.php?cid='.$line->id.'">'.$this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'").'</a>&nbsp;<a href="manageDesignCategory.php?cid='.$line->id.'">'.$subCat.'</a>';
					else 
						$categoryName = $this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
												
					$genTable .= '<div class="'.$highlight.'" id="'.$line->id.'"><div class="dragbox-content">
								 <ul>
								 	<li style="width:50px;">&nbsp;&nbsp;<input name="chk[]" value="'.$line->id.'" type="checkbox"></li>
									<li style="width:100px;">'.$i.'</li>
									<li style="width:180px;">'.stripslashes(stripslashes($categoryName)).'</li><li style="width:100px;">';
									
					if($menuObj->checkEditPermission()) 							
						$genTable .= '<div id="'.$div_id.'" style="cursor:pointer;" onClick="javascript:changeStatus(\''.$div_id.'\',\''.$line->id.'\',\'design_category\')">'.$status.'</div>';
					
					$catName = $this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '".$line->id."' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'");
					$genTable .= '</li><li style="width:100px;"><a rel="shadowbox;width=705;height=325" title="'.$catName.'" href="viewDesignCategory.php?id='.base64_encode($line->id).'"><img src="images/view.png" border="0"></a></li><li style="width:75px;">';
									
					if($menuObj->checkEditPermission()) 					
						$genTable .= '<a href="editDesignCategory.php?id='.base64_encode($line->id).'&page='.$page.'&cid='.$cid.'"><img src="images/edit.png" alt="Edit" width="16" height="16" border="0" /></a>';
						
					$genTable .= '</li><li>';
				
					if($menuObj->checkDeletePermission()) 					
						$genTable .= "<a href='javascript:void(NULL);'  onClick=\"if(confirm('Are you sure to delete this Category  ?')){window.location.href='pass.php?action=design_category&type=delete&cid=".$cid."&id=".$line->id."&page=$page'}else{}\" ><img src='images/drop.png' height='16' width='16' border='0' title='Delete' /></a>";
					
							
					$genTable .= '</li></ul></div></div>';
				
		
					$i++;	
				}
				$genTable .= '</div>';
				$genTable .= '<div id="dragndrop"><input type="hidden" name="currentOrder" id="currentOrder" value="'.$currentOrder.'" /></div>';

				switch($recordsPerPage)
				{
					case 10:
						$sel1 = "selected='selected'";
						break;
					case 20:
						$sel2 = "selected='selected'";
						break;
					case 30:
						$sel3 = "selected='selected'";
						break;
					case $this->numrows:
						$sel4 = "selected='selected'";
						break;
				}
				$currQueryString = $this->getQueryString();
				$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
				$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					<tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					<option value='10' $sel1>10</option>
					<option value='20' $sel2>20</option>
					<option value='30' $sel3>30</option> 
					<option value='".$totalrecords."' $sel4>All</option>  
					  </select> Records Per Page
				</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";	
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}*/
	
	function changeValueStatus($get) {
	
		$xmlArr = array();
		$xm = 1;
		$status=$this->fetchValue(TBL_DESIGNCATEGORY,"status","1 and id = '$get[id]'");
		
		if($status==1) {
			$stat= 0;
			$status="Inactive,0";
		} else 	{
			$stat= 1;
			$status="Active,1";
		}
	
		$query = "update ".TBL_DESIGNCATEGORY." set status = '$stat', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$get[id]'";

		$xmlArr[$xm]['query'] = addslashes($query);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;
		
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);


		if($this->executeQry($query)) 
			$this->logSuccessFail('1',$query);		
		else 	
			$this->logSuccessFail('0',$query);
		echo $status;		
	}
	
	function deleteValue($get) {

		$xmlArr = array();
		$xm = 1;

		//$result=$this->deleteRec(TBL_CATEGORY,"path like '%-$get[id]-%'");
		$queryXml = "update ".TBL_DESIGNCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$get[id]-%'";
		$this->executeQry($queryXml);
		$xmlArr[$xm]['query'] = addslashes($queryXml);
		$xmlArr[$xm]['identification'] = $get['id'];
		$xmlArr[$xm]['section'] = "update";
		$xm ++;
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);

		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$get[cid]&page=$get[page]&limit=$get[limit]';</script>";
	}
	
	
	// check for default Category Other
	function chkDefaultCategory($cid)
	{
	    //echo LANG_OTHER;
		$query = "select TDC.* from ".TBL_DESIGNCATEGORY." as TDC ,  ".TBL_DESIGNCATEGORY_DESCRIPTION." as TDD where TDC.parent_id = '".$cid."' and TDD.categoryName = '".strtolower('other')."' and  TDD.langId = '".$_SESSION['DEFAULTLANGUAGE']."' and  TDC.id = TDD.catId";
		$res = $this->executeQry($query);
		$row = $this->getTotalRow($res);
		
		$this->executeQry("insert into ".TBL_DESIGNCATEGORY." set parent_id = '$cid', categoryImage = '$image_name', status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',sequence = '".$post['maxsequence']."'");
		$inserted_id1 = mysql_insert_id();
		$path1 = "-".$cid."-".$inserted_id1."-";
		
		
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num){
		
		if($DefaultCat == 0) {
					while($line = $this->getResultObject($rst)) {
						$queryDefault = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$inserted_id1', langId = '".$line->id."', categoryName = 'other'";	
							
						if($this->executeQry($queryDefault)) 
							$this->logSuccessFail('1',$queryDefault);		
						else 	
							$this->logSuccessFail('0',$queryDefault);
					}
				}
			}
			$queryupdate = "update ".TBL_DESIGNCATEGORY." set path = '$path1' where id = '$inserted_id1'";
			$this->executeQry($queryupdate);
	}	
	
	
	
	function addRecord($post,$file)
	{
		$_SESSION['SESS_MSG'] = "";	
		if($file['categoryImage']['name'])
		{
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __CATEGORYORIGINAL__.$image_name;
			if($this->checkExtensions($extension)) {	
				$filestatus = 	move_uploaded_file($file['categoryImage']['tmp_name'], $target);
				chmod($target, 0777);
				if($filestatus){
					$imgSource = $target;	 
					$LargeImage = __CATEGORYORIGINAL__.$image_name;
					$ThumbImage = __CATEGORYTHUMB__.$image_name;
					chmod(__CATEGORYORIGINAL__,0777);
					chmod(__CATEGORYTHUMB__,0777);
					$fileSize = $this->findSize('TBL_CATEGORY_THUMB_WIDTH','TBL_CATEGORY_THUMB_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 	
		}
		
		if($_SESSION['SESS_MSG'] == "") {
			$queryXml = "insert into ".TBL_DESIGNCATEGORY." set parent_id = '$post[cid]', categoryImage = '$image_name',
			status = '1', addDate = '".date('Y-m-d')."', addedBy = '".$_SESSION['ADMIN_ID']."',sequence = '".$post['maxsequence']."'";
			$this->executeQry($queryXml);
			$inserted_id = mysql_insert_id();

			if($post[cid] == 0)	$path = "-".$inserted_id."-";
			else $path = $this->fetchValue(TBL_DESIGNCATEGORY,"path","1 and id = '$post[cid]'").$inserted_id."-";	
			$query = "update ".TBL_DESIGNCATEGORY." set path = '$path' where id = $inserted_id";

			if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
			else $this->logSuccessFail('0',$query);
			
			$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
			$num = $this->getTotalRow($rst);
			if($num)
			{			
				while($line = $this->getResultObject($rst))
				{					
					$categoryName = 'categoryName_'.$line->id;
					$query = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$inserted_id', langId = '".$line->id."',
					categoryName = '".addslashes($post[$categoryName])."'";
	
					if($this->executeQry($query))	$this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);
				}
			}					
			if($post[cid] == 0)
			echo '';
				//$DefaultCat = $this->chkDefaultCategory($inserted_id);
			//echo "<script>window.location.href='addDesignCategory.php?cid=$post[cid]';";		
		} 
		$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your information has been added successfully");
		//@header('location:addDesignCategory.php?cid=$post[cid]');		
	}
	
	function editRecord($post,$file)
	{
		$_SESSION['SESS_MSG'] = "";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' and isDeleted = '0' order by id asc","","");		
		$num = $this->getTotalRow($rst);
		if($num)
		{			
			while($line = $this->getResultObject($rst))
			{					
				$categoryName = 'categoryName_'.$line->id;
				$sql = $this->selectQry(TBL_DESIGNCATEGORY_DESCRIPTION,'1 and catId = "'.$post[id].'" and langId = "'.$line->id.'"','','');
				$numrows = $this->getTotalRow($sql);
				if($numrows == 0) {
					$query = "insert into ".TBL_DESIGNCATEGORY_DESCRIPTION." set catId = '$post[id]', langId = '".$line->id."', categoryName = '".$post[$categoryName]."'";
					
					if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);	
				}
				else {
					$query = "update ".TBL_DESIGNCATEGORY_DESCRIPTION." set categoryName = '".addslashes($post[$categoryName])."' where 1 and catId = '$post[id]' and langId = '".$line->id."'";
					
					if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);	
				}	
			}	
		}
			
		if($file['categoryImage']['name'])
		{
  			$filename = stripslashes($file['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);
	
			$image_name = date("Ymdhis").time().rand().'.'.$extension;
			$target    = __CATEGORYORIGINAL__.$image_name;	
			
			if($this->checkExtensions($extension))
			{	
				$filestatus = 	move_uploaded_file($file['categoryImage']['tmp_name'], $target);			
				chmod($target, 0777);
				if($filestatus)
				{
					$imgSource = $target;	 
					$LargeImage = __CATEGORYORIGINAL__.$image_name;
					$ThumbImage = __CATEGORYTHUMB__.$image_name;
					chmod(__CATEGORYORIGINAL__,0777);
					chmod(__CATEGORYTHUMB__,0777);
					
					$fileSize = $this->findSize('TBL_CATEGORY_THUMB_WIDTH','TBL_CATEGORY_THUMB_HEIGHT',100,100);
					exec(IMAGEMAGICPATH." $imgSource -thumbnail $fileSize $ThumbImage");
					
					$prevCategoryImage = $this->fetchValue(TBL_DESIGNCATEGORY,"categoryImage","1 and id = '$post[id]'");
					@unlink(__CATEGORYORIGINAL__.$prevCategoryImage);
					@unlink(__CATEGORYTHUMB__.$prevCategoryImage);							
					$query = "update ".TBL_DESIGNCATEGORY." set categoryImage = '$image_name', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id = '$post[id]'";
					
					if($this->executeQry($query)) $this->logSuccessFail('1',$query);		
					else $this->logSuccessFail('0',$query);
				} else {
					$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"There is some error to upload flag.!!!");
				}			
			} else {
				$_SESSION['SESS_MSG'] .= msgSuccessFail('fail',"This files are not allowed for images.!!!");
			} 	
		}
		echo "<script>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]';</script>";
	}

	function deleteAllValues($post){
	
		$xmlArr = array();
		$xm = 1;

		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]&limit=$post[limit]';</script>";
			exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(TBL_CATEGORY," path like '%-$val-%'");	
					$queryXml = "update ".TBL_DESIGNCATEGORY." set isDeleted = '1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where path like '%-$val-%'";
					$this->executeQry($queryXml);
					$xmlArr[$xm]['query'] = addslashes($queryXml);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'enableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_DESIGNCATEGORY." set status ='1', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Enable selected successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		if($post[action] == 'disableall'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
				    //$result=$this->deleteRec(LOANTBL_CATEGORY,"cat_id='$val'");	
					$sql="update ".TBL_DESIGNCATEGORY." set status ='0', modDate = '".date('Y-m-d H:i:s')."', modBy = '".$_SESSION['ADMIN_ID']."' where id='$val'";
					mysql_query($sql);
					$xmlArr[$xm]['query'] = addslashes($sql);
					$xmlArr[$xm]['identification'] = $val;
					$xmlArr[$xm]['section'] = "update";
					$xm ++;

				}
				$_SESSION['SESS_MSG'] =msgSuccessFail("success","Disable selected successfully!!!");
			}else{
				$_SESSION['SESS_MSG'] =msgSuccessFail("fail","First select the record!!!");
			}
		}
		$xmlInfoObj = new XmlInfo();
		$xmlInfoObj->addXmlData($xmlArr);

		echo "<script language=javascript>window.location.href='manageDesignCategory.php?cid=$post[cid]&page=$post[page]';</script>";
	}	
	
	function getResult($id) {
		$sql = $this->executeQry("select * from ".TBL_DESIGNCATEGORY." where id = '$id'");
		$num = $this->getTotalRow($sql);
		if($num > 0) {
			return $line = $this->getResultObject($sql);	
		} else {
			redirect('manageDesignCategory.php');
		}	
	}
	
	function getBreadCrumb($cid) {
		$pageName2   = basename($_SERVER['PHP_SELF']);		
		if($cid && $cid != '' && $cid != 0) {
			$breadcrumb = '&nbsp;&nbsp;<a href="'.$pageName2.'">Top</a>';
			$path = $this->fetchValue(TBL_DESIGNCATEGORY,"path","1 and id = $cid");
			$pathArr = explode('-',$path);
			$ctr = 0;	
			foreach($pathArr as $key=>$value) {
				if($value) {
					if(count($pathArr) - $ctr <= 2)
						$breadcrumb .= "&nbsp;->&nbsp;".stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"));
					else					
						$breadcrumb .= "&nbsp;->&nbsp;<a href='".$pageName2."?cid=$value'>".stripslashes($this->fetchValue(TBL_DESIGNCATEGORY_DESCRIPTION,"categoryName","1 and catId = '$value' and langId = '".$_SESSION['DEFAULTLANGUAGE']."'"))."</a>";
				}	
				$ctr++;
			}
		}
		return $breadcrumb;
	}
	
	function isCategoryNameExist($catname,$langId,$id=''){
		$catname = trim($catname);
		$rst = $this->selectQry(TBL_DESIGNCATEGORY_DESCRIPTION,"langId = '".$langId."' and categoryName='".addslashes($catname)."' AND catId!='$id'  ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}
	
	function checkCategoryExist($cid) {
		if($cid) {
			if($cid >= 0 && is_numeric($cid)) {	
				if($cid > 0 && $this->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName = 'MULTILEVEL_DESIGN_CATEGORY'") == 1) {
					$getCid = $this->fetchValue(TBL_DESIGNCATEGORY,"id","1 and id = ".(int)$cid."");
					if(!$getCid)
						redirect('manageDesignCategory.php');		
				} else {
					redirect('manageDesignCategory.php');					
				}
			} else {
				redirect('manageDesignCategory.php');
			}
		}
	}
	
	function SortSequence($get){
	  //print_r($get);
	  $sortedArr 	=   explode("=",$get['url']);
	  $sortedIdArr	=	$sortedArr[1];
	  $Sorted_Order_Arr = explode(",",$sortedIdArr);
	  $Current_Order_Arr = explode(",",substr_replace($get['Current_Order'],"",-1));
	 
	  $i = 0;
	  $sorted_key_arr	=	array();	 
	  foreach($Sorted_Order_Arr as $Sorted_Order_Val){
	     $current_id	=	$Current_Order_Arr[$i];
		 $sql_string	=	"SELECT sequence FROM ".TBL_DESIGNCATEGORY." WHERE id = '$current_id' ";
         $query 	 	=	mysql_query($sql_string);
		 if($line = mysql_fetch_object($query)){
		    $sorted_key_arr[$Sorted_Order_Val] = $line->sequence;
		 }
		 $i++;
       }
	  //print_r($sorted_key_arr);
	  foreach($sorted_key_arr as $key=>$val){  
	        $sql_string		=	"UPDATE ".TBL_DESIGNCATEGORY." SET sequence = '$val' WHERE id = '".$key."'";
		   	mysql_query($sql_string);   
	   }
	    
/*		for($i=30;$i<60;$i++){
		     $Qry  = mysql_query("SELECT CAT.* FROM ecart_category as CAT WHERE parent_id = '0'");
			 $n_row = mysql_num_rows($Qry);
			 if($n_row > 1){
			   $k = 1;
			   while($data = mysql_fetch_object($Qry)){
			     mysql_query("UPDATE `ecart_category` SET `sequence` = '$k' WHERE `id` ='$data->id'");
				 $k++;
			   }
			 }
		}*/
		$cid  = $cid?$cid:0;
		$cond = "1 and c.id = cd.catId and c.parent_id = $cid and c.isDeleted = '0' and cd.langId = '".$_SESSION['DEFAULTLANGUAGE']."'";
		$query = "select c.*,cd.categoryName from ".TBL_DESIGNCATEGORY." as c , ".TBL_DESIGNCATEGORY_DESCRIPTION." as cd where $cond ";
		
				//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($get['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($get['page']); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
		     $orderby = $_GET[orderby]? $_GET[orderby]:"sequence";
		     $order = $_GET[order]? $_GET[order]:"ASC";   
         //   $query .=  " ORDER BY cd.$orderby $order LIMIT ".$offset.", ". $recordsPerPage;
		     $query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			
	      $order_query = mysql_query($query);
		  while($line = mysql_fetch_object($order_query)){
		     $currentOrder .= $line->id.",";
		  }
	   	  ?><input type="hidden" name="currentOrder" id="currentOrder" value="<?=$currentOrder?>" /><?php


	}
	
	function findMaxSequence(){
		$query	=	"SELECT max(sequence) as maxsequence FROM ".TBL_DESIGNCATEGORY." ";
	    $res = $this->executeQry($query);
		$line = $this->getResultObject($res);
		return $line->maxsequence+1;
	}
	
}// End Class
?>	
<?php
ob_start(); 
session_start();
class StaticPage extends MySqlDriver{
	function __construct() {
	  $this->obj = new MySqlDriver;       
    }
	

		function allStaticPageTypeInformation() {
		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){                    
                    $searchtxt = $_REQUEST['searchtxt'];
                    $con = " AND staticPageName LIKE '%$searchtxt%' ";
		}
		$query = "select * from ".TBL_STATICPAGETYPE." where 1=1 $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"staticPageName";
			$order = $_GET[order]? $_GET[order]:"ASC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					
					$genTable .= '<tr>
						  <th><input name="chk[]" value="'.$line->id.'" type="checkbox" class="checkbox"></th>
									<td>'.$i.'</td>
									<td>'.$line->staticPageName.'</td>
									<td>'.$line->pageUrl.'</td>
									<td>';
						$genTable .= '<a href="manageStaticPageDescription.php?id='.base64_encode($line->id).'"><img src="images/view.png" alt="View" width="16" height="16" border="0" /></a>';
					$genTable .= '</td>   <td>';
					if($menuObj->checkEditPermission()) {					
						$genTable .= '<a class="i_pencil edit" href="editStaticPageType.php?id='.base64_encode($line->id).'&page='.$page.'">Edit</a>';
					}	
					$genTable .= '</td><td>';
					$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=staticpagetype&type=delete&id=".$line->id."&page=$page'}else{}\" >Delete</a>";
					$genTable .= '</td></tr>';
					$i++;	
				}
				switch($recordsPerPage)
					{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
					}
					$currQueryString = $this->getQueryString();
					$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
					$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}


///////// All Static Page Details ///////////////////

	function allStaticPageDetailsInformation($id) {

		if($_REQUEST['searchtxt'] && $_REQUEST['searchtxt'] != SEARCHTEXT){
		$searchtxt = $_REQUEST['searchtxt'];
		$con = " AND ".TBL_STATICPAGESETTING.".pageTitle LIKE '%$searchtxt%' OR ".TBL_LANGUAGE.".languageName LIKE '%$searchtxt%' ";
		}
	 $query = "select ".TBL_LANGUAGE.".languageName,".TBL_STATICPAGESETTING.".id,".TBL_STATICPAGESETTING.".pageTitle from ".TBL_LANGUAGE." INNER JOIN ".TBL_STATICPAGESETTING." ON (".TBL_LANGUAGE.".id = ".TBL_STATICPAGESETTING.".langId)  where 1=1 AND ".TBL_STATICPAGESETTING.".staticPageTypeId = '$id' $con ";
		$sql = $this->executeQry($query);
		$num = $this->getTotalRow($sql);
		$menuObj = new Menu();
		$page =  $_REQUEST['page']?$_REQUEST['page']:1;
		if($num > 0) {
			$genTable = '';
			//-------------------------Paging------------------------------------------------			
			$paging = $this->paging($query); 
			$this->setLimit($_GET['limit']); 
			$recordsPerPage = $this->getLimit(); 
			$offset = $this->getOffset($_GET["page"]); 
			$this->setStyle("redheading"); 
			$this->setActiveStyle("smallheading"); 
			$this->setButtonStyle("boldcolor");
			$currQueryString = $this->getQueryString();
   			$this->setParameter($currQueryString);
			$totalrecords = $this->numrows;
			$currpage = $this->getPage();
			$totalpage = $this->getNoOfPages();
			$pagenumbers = $this->getPageNo();		
			//-------------------------Paging------------------------------------------------
			
			$orderby = $_GET[orderby]? $_GET[orderby]:"pageTitle";
			$order = $_GET[order]? $_GET[order]:"ASC";
			
			$query .=  " ORDER BY $orderby $order LIMIT ".$offset.", ". $recordsPerPage;
			$rst = $this->executeQry($query); 
			$row = $this->getTotalRow($rst);
		
			if($row > 0) {			
				$i = 1;			
				while($line = $this->getResultObject($rst)) {
					$highlight = $i%2==0?"main-body-bynic":"main-body-bynic2";
					
					$genTable .= '<tr>
									<th>'.$i.'</th>
									<td>'.$line->languageName.'</td>
									<td>'.$line->pageTitle.'</td>';
									
					$genTable .= '<td>';
					$genTable .= '<a rel="shadowbox;width=705;height=325" href="viewStaticPage.php?id='.base64_encode($line->id).'&pageId='.base64_encode($id).'"><img src="images/view.png" alt="View" width="16" height="16" border="0" /></a>';
					$genTable .= '</td>';
					$genTable .= '<td>';
					if($menuObj->checkEditPermission("manageStaticPage.php")){
					$genTable .= '<a class="i_pencil edit" href="editStaticPage.php?id='.base64_encode($line->id).'&pageId='.base64_encode($id).'&page='.$page.'">Edit</a>';
					}
					$genTable .= '</td>';
					
					$genTable .= '<td>';
					if($menuObj->checkDeletePermission("manageStaticPage.php")){
					$genTable .= "<a class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Record Containts?')){window.location.href='pass.php?action=staticpageDescription&type=delete&id=".$line->id."&pageId=".base64_encode($id)."&page=$page'}else{}\" >Delete</a>";
					}
					$genTable .= '</td>';
					
					$genTable .= '</tr>';
					$i++;	
				}
				switch($recordsPerPage)
					{
					 case 10:
					  $sel1 = "selected='selected'";
					  break;
					 case 20:
					  $sel2 = "selected='selected'";
					  break;
					 case 30:
					  $sel3 = "selected='selected'";
					  break;
					 case $this->numrows:
					  $sel4 = "selected='selected'";
					  break;
					}
					$currQueryString = $this->getQueryString();
					$limit = basename($_SERVER['PHP_SELF'])."?".$currQueryString;
					$genTable.="<div style='overflow:hidden; margin:0px 0px 0px 50px;'><table border='0' width='88%' height='50'>
					 <tr><td align='left' width='300' class='page_info' 'style=margin-left=20px;'>
					 Display <select name='limit' id='limit' onchange='pagelimit(\"$limit\");' class='page_info'>
					 <option value='10' $sel1>10</option>
					 <option value='20' $sel2>20</option>
					 <option value='30' $sel3>30</option> 
					 <option value='".$totalrecords."' $sel4>All</option>  
					   </select> Records Per Page
					</td><td align='center' class='page_info'><inputtype='hidden' name='page' value='".$currpage."'></td><td class='page_info' align='center' width='200'>Total ".$totalrecords." records found</td><td width='0' align='right'>".$pagenumbers."</td></tr></table></div>";
			}					
		} else {
			$genTable = '<div>&nbsp;</div><div class="Error-Msg">Sorry no records found</div>';
		}	
		return $genTable;
	}
	
	
	function checkIsPageExists($pageId,$id){
		$rst = $this->selectQry(TBL_STATICPAGESETTING," staticPageTypeId = '$pageId' AND id='$id'","","");	
		$row = $this->getTotalRow($rst);
		if(!$row){
			header("Location:manageStaticPageDescription.php?id=".base64_encode($pageId));exit;
		}else{
		return $this->getResultRow($rst);
		}
	}


	//////// Function Get Remaining Language ////////
	
	function getRemainingLanguage($pageId,$langId=''){
		$rstlang = $this->selectQry(TBL_STATICPAGESETTING,"staticPageTypeId='$pageId'","","");
		$numlang = $this->getTotalRow($rstlang);
			if($numlang){
				while($rowlang = $this->getResultObject($rstlang)){
				$lang[] = '"'.$rowlang->langId.'"';
				}
				$langarr = implode(",",$lang);
			}else{
				$langarr = '0';
			}
		$preTable = "<option value=''>Please Select Language</option>";
		$rst = $this->selectQry(TBL_LANGUAGE,"status='1' AND isDeleted='0' AND id NOT IN ($langarr) order by languageName asc","","");
		$num = $this->getTotalRow($rst);
		if($num){
			while($row = $this->getResultObject($rst)){
			if($langId == $row->id){ $sel = "selected='selected'";}else{$sel = "";}
			$preTable.= "<option value='$row->id' $sel>".ucwords($row->languageName)."</option>"; 
			}
		}
		return $preTable;
	}
	


////// Add Page Containt ////////

	function addNewStaticPageContaint($post){

		$_SESSION['SESS_MSG'] = "";	
		$pageContaint = addslashes($post[message]);
		$post = postwithoutspace($post);
		$this->tablename = TBL_STATICPAGESETTING;	
		$this->field_values['staticPageTypeId'] = $post['pageTypeId'] ;
		$this->field_values['langId'] = $post['languageName'] ;
		$this->field_values['pageTitle'] = addslashes($post['pageTitle']);
		$this->field_values['pageContaint'] = $pageContaint;
		$this->field_values['addDate'] = date("Y-m-d h:i:s");
		$res = $this->insertQry();
		$insertId = $res[1];
		if($insertId){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been added successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add record.");
		}	
		header("Location:manageStaticPageDescription.php?id=".base64_encode($post['pageTypeId']));exit;
	}


////// Edit Page Containt ////////

	function editNewStaticPageContaint($post){        
		$_SESSION['SESS_MSG'] = "";	
		$pageContaint = mysql_real_escape_string($post[message]);
		$post = postwithoutspace($post);
		$this->tablename = TBL_STATICPAGESETTING;	
		$this->field_values['pageTitle'] = $post['pageTitle'] ;
		$this->field_values['pageContaint'] = $pageContaint;
		$this->condition  = " staticPageTypeId = '$post[pageTypeId]' AND  id = '$post[id]' ";
		$res1 = $this->updateQry();
		if($res1){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Record has been updated successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to update record.");
		}	
		header("Location:manageStaticPageDescription.php?id=".base64_encode($post['pageTypeId']));exit;
	}

	
	function deleteValue($get){
		$sql = " DELETE FROM  ".TBL_STATICPAGESETTING." WHERE id = '$get[id]' ";
		$rst = $this->executeQry($sql);
		if($rst){
			$this->logSuccessFail('1',$query);		
		}else{ 	
			$this->logSuccessFail('0',$query);
		}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageStaticPageDescription.php?id=$get[pageId]&page=$get[page]&limit=$get[limit]';</script>";
	}


	function deleteStaticPageValue($get){
		$sql = " DELETE FROM  ".TBL_STATICPAGETYPE." WHERE id = '$get[id]' ";
		$rst = $this->executeQry($sql);
		if($rst){
			$sql = " DELETE FROM  ".TBL_STATICPAGESETTING." WHERE staticPageTypeId = '$get[id]' ";
			$rst = $this->executeQry($sql);
			$this->logSuccessFail('1',$query);		
		}else{ 	
			$this->logSuccessFail('0',$query);
		}
		$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your Information has been deleted successfully!!!");
        echo "<script language=javascript>window.location.href='manageStaticPage.php?page=$get[page]&limit=$get[limit]';</script>";
	}


	function deleteAllStaticPagesValues($post){
		if(($post[action] == '')){
		    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the action or records , And then submit!!!");
			echo "<script language=javascript>window.location.href='manageGiftCertificate.php?page=$post[page]&limit=$post[limit]';</script>";	exit;
		}				
		if($post[action] == 'deleteselected'){
			$delres = $post[chk];
			$numrec = count($delres);
			if($numrec>0){
				foreach($delres as $key => $val){
					$sql = "DELETE FROM ".TBL_STATICPAGETYPE." where id = '$val'";
					$rst = $this->executeQry($sql);
					if($rst){
						$this->deleteval(TBL_STATICPAGESETTING,"staticPageTypeId ='$val'");
						$this->logSuccessFail("1",$sql);
					}else{
					$this->logSuccessFail("0",$sql);
					}
				}
				$_SESSION['SESS_MSG'] = msgSuccessFail("success","Your all selected information has been deleted successfully!!!");
			}else{
			    $_SESSION['SESS_MSG'] = msgSuccessFail("fail","First select the record!!!");
			}
		}
		echo "<script language=javascript>window.location.href='manageStaticPage.php?page=$post[page]&limit=$post[limit]';</script>";
	}	


	/////// Language Name Exists ////////
	function isPageNameExit($pageName,$id=''){
		$langname = trim($langname);
		$rst = $this->selectQry(TBL_STATICPAGETYPE,"staticPageName='$pageName' AND id !='$id' ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}

/////// Language Name Exists ////////
	function isPageUrlExit($pageUrl,$id=''){
		$langname = trim($langname);
		$rst = $this->selectQry(TBL_STATICPAGETYPE,"pageUrl='$pageUrl' AND id !='$id' ","","");
		$row = $this->getTotalRow($rst);
		return $row;
	}



	///////////////// Add Static Page Type ///////////////
	
	function addStaticPageType($post){
		$_SESSION['SESS_MSG'] = "";	
		$this->tablename = TBL_STATICPAGETYPE;	
		$this->field_values['staticPageName'] = $post['pageName'] ;
		$this->field_values['pageUrl'] = $post['pageUrl'] ;
		$res = $this->insertQry();
		if($res){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been added successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to add information.");
		}	
		header("Location:addStaticPageType.php");exit;
	}
	
		///////////////// Edit Static Page Type ///////////////
	
	function editStaticPageType($post){
		//print_r($post);exit;
		$_SESSION['SESS_MSG'] = "";	
		$query ="update ".TBL_STATICPAGETYPE." set staticPageName = '".$post['pageName']."', pageUrl = '".$post['pageUrl']."' where id = '".$post['id']."'";		
		$res = $this->executeQry($query);
		if($res){
			$_SESSION['SESS_MSG'] =  msgSuccessFail("success","Information has been updated successfully.");
		}else{
			$_SESSION['SESS_MSG'] =  msgSuccessFail("fail","There is some problem to update information.");
		}	
		header("Location:manageStaticPage.php");
		exit;
	}
	/////// Static Page type to edit ///////
	function staticPageTypeToEdit($id)
	{
		$rst=$this->selectQry(TBL_STATICPAGETYPE," id='$id' ",'','');
		$num=$this->getTotalRow($rst);
		if($num){
			$row=$this->getResultRow($rst);
			return $row;
		}else{
			echo "<script language=javascript>window.location.href='manageStaticPage.php';</script>";	
		}
	}

	
	
}// end Class

?>	

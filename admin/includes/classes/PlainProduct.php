<?php

session_start();

class PlainProduct extends MySqlDriver {

    function __construct() {
        $this->obj = new MySqlDriver;
    }

    function getFabricType($id = '') {
        $sql = $this->executeQry("select f.*, fd.fabricName from " . TBL_FABRIC . " as f, " . TBL_FABRICDESC . " as fd where 1 and f.id = fd.fabricId and fd.langId = '" . $_SESSION['DEFAULTLANGUAGE'] . "'");
        $num = $this->getTotalRow($sql);
        $sel = "";
        if ($num > 0) {
            while ($line = $this->getResultObject($sql)) {
                ($id == $line->id) ? $sel = "selected='selected'" : $sel = "";
                $genTable .= '<option value="' . $line->id . '" ' . $sel . '>' . $line->fabricName . '</option>';
            }
        }
        return $genTable;
    }

    function viewProduct($id) {
        $query = "Select * from " . TBL_ACCESSORIES_DISCOUNT . " where 1 and fabricId = '" . $id . "' order by pcs";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $output = "";
        if ($num > 0) {
            $output .= '<label>' . $this->fetchValue(TBL_FABRICDESC, 'fabricName', 'fabricId = ' . $id) . '</label><section>
		            		<div  style="width:55%; float:left;"><span style="font-size: 13px; margin-left: 25px;"><strong>pcs</strong></span>
		            		</div>
		            		<div  style="width:7%; float:left;"><strong>sign</strong>
		            		</div>
		            		<div  style="width:21%; float:left;"><strong>percent</strong>
		            		</div>
		            		<div  style="width:7%; float:left;"><strong>option</strong>
		            		</div>
            			</section>';
            while ($row = $this->getResultObject($result)) {
                if ($row->sign == 0) {
                    $status = "minus";
                } else if ($row->sign == 1) {
                    $status = "x";
                } else {
                    $status = "plus";
                }

                $output .="<section id='" . $row->id . "'><div style='width:55%; float:left;'>";
                $output .="<span style='font-size: 13px; margin-left: 25px;'>" . $row->pcs . "pcs</span></div>";
                $output .="<div style='width:7%; float:left;'>";
                $output .="<span style='font-size: 13px; margin-left:11px;'>" . $status . "</span></div>";

                $output .="<div style='width:21%; float:left;'>";
                if ($row->sign != 1) {
                    $output .="<input type='text' name='charge_" . $row->id . "' id='charge_" . $row->id . "' value='" . $row->charge . "' class='wel' style='width:75px;' />";
                    $output .="<span style='font-size: 17px;'>%</span></div>";
                } else {
                    $output .= "<span style='font-size: 13px; margin-left:50px;'>0</span>";
                    $output .="<span style='font-size: 17px; margin-left:25px'>%</span></div>";
                }
                $output .="<div style='width:7%; float:left;'>";
                //   			$output .="<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){window.location.href='pass.php?action=manageAccessories&type=delete&id=".$row->id."&page=$page'}else{}\" >Delete</a></div></section>";
                $output .="<a  class='i_trashcan edit' href='javascript:void(0);'  onClick=\"if(confirm('Are you sure to delete this Resord?')){deleteAccessories('" . $row->id . "', '" . $row->fabricId . "');}else{}\" >Delete</a></div></section>";
            }
        }
        echo $output;
    }

    function addFabricDiscount($post) {
        $query = "INSERT INTO " . TBL_ACCESSORIES_DISCOUNT . " SET fabricId = '" . $post[accessories] . "', pcs='" . $post['pcs'] . "', sign='" . $post['sign'] . "', charge = '" . $post[charges] . "'";
        $insertId = $this->executeQry($query);
        if ($insertId) {
            $_SESSION['SESS_MSG'] = msgSuccessFail("success", "Record has been added successfully.");
        } else {
            $_SESSION['SESS_MSG'] = msgSuccessFail("fail", "There is some problem to add record.");
        }
        header("location:addAccessoriesDiscount.php");
        exit;
    }

    function deleteAccessoriesDiscount($get) {
        $query = "Delete from " . TBL_ACCESSORIES_DISCOUNT . " where id = '" . $get[id] . "' and fabricId ='" . $get[accessoriesId] . "'";
        if ($this->executeQry($query)) {
            echo "Success";
        } else {
            echo "Fail";
        }
    }

}

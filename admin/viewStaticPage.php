<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStaticPage.php");
$staticpageObj = new StaticPage();
$pageId = base64_decode($_GET['pageId']);
$pageId = $pageId?$pageId:0;
$id =  base64_decode($_GET['id']);
$id = $id?$id:0;
$row = $staticpageObj->checkIsPageExists($pageId,$id);

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>View Static Page Content : <?=stripslashes($staticpageObj->fetchValue(TBL_STATICPAGETYPE,"staticPageName","id='$pageId'"))?></label>
 		 
          <div><?=$_SESSION['SESS_MSG']?></div>
            <section>
       	  <label>Language Name:</label>
             <div>
			 	   <?=stripslashes($staticpageObj->fetchValue(TBL_LANGUAGE,"languageName","id='".$row['langId']."' "))?>
            </div>
          </section>
		  
            <section>
       	  <label>Page Title:</label>
             <div>
			 	<?=stripslashes($row['pageTitle']);?>
            </div>
          </section>

          <section>
       	  <label>Page Content:</label>
             <div>
			 	<?=(stripslashes($row['pageContaint']));?>
            </div>
          </section>

     </fieldset>       
          </form>  
    </section>        
<? include_once('includes/footer.php');?>

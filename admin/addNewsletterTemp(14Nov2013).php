<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkEditPermission("manageNewsletterTemp.php");
/*---Basic for Each Page Ends----*/
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();



if(isset($_POST['submit'])) {	
	//echo "<pre>"; print_r($_REQUEST); echo "</pre>";exit;
	// Server Side Validation	
	require_once('validation_class.php');
	$obj = new validationclass();		
	$langIdArr = $generalFunctionObj->langIdArr();
	if(count($langIdArr)){
		
		/// Validate Category Name
		foreach($langIdArr as $key=>$value) {		
			$obj->fnAdd('name_'.$value,$_REQUEST['name_'.$value], 'req',LANG_PLEASE_ENTER_TEMP_NAME);							
		}
		
		
		
		/// Get Validation : $str_valiate=0 for ERROR; $str_validate=1 for NO ERROR
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;		
		
		/// Get ERROR ARRAY	=====
		foreach($langIdArr as $key=>$value) {			
			$obj->fnAdd('name_'.$value,$_REQUEST['name_'.$value], 'req',LANG_PLEASE_ENTER_TEMPLATE_NAME);				
		}			
		
		$obj->fnAdd("message", strip_tags($_POST["message"]), "req",LANG_PLEASE_ENTER_TEMPLATE_CONTENT);
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;	
		
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['name_'.$value]=$obj->fnGetErr($arr_error['name_'.$value]);
		}
		$arr_error[message]=$obj->fnGetErr($arr_error[message]);
		
		if($str_validate){		
			
			$newsletterObj->addNewsletterTemplate($_POST);
		}
	}	
}


//echo"<pre>"; print_r($row); echo"</pre>";exit;
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
	<h1>Newsletter Template</h1>
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>Add Newsletter Template</label>
        <div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset>  
		
		<!-- Start : Template Name --------------->
        <section>
        	<label for="TempalteName">Template Name</label>
          <? $tempname = $_POST['tempname']?$_POST['tempname']:stripslashes($row['templateName']); ?>
           	<div> <input type="text" name="tempname" id="m__Template_name" size="3" class="wel" value="<?=stripslashes($tempname)?>" />
			     <?=$arr_error[tempname]?>
			</div>
        </section>
		
		<!-- Start : Template Content --------------->
		<section>
        	<label for="TemplateContent">Template Content</label>
           	<div >
			<?						
			$oFCKeditor = new FCKeditor('message') ;			
			$oFCKeditor->BasePath = 'fckeditor/' ;			
			$oFCKeditor->Height	= 400;			
			$oFCKeditor->Width	= 560;			
			$oFCKeditor->Value = stripslashes($_POST['message']);			
			$oFCKeditor->Create();
			?>
			<?=$arr_error[message]?>
			</div>
        </section>
		
		<!-- Start : Template Status --------------->
		<section>
        	<label for="TemplateStatus">Status</label>
           	<div>
			<select name="status" >
			<option value="0" <?php if($row['status'] == '0'){ echo "Selected=selected";}  ?>>InActive</option>
			<option value="1" <?php if($row['status'] == '1'){ echo "Selected=selected";}  ?>>Active</option>
			</select>			
			
			</div>
        </section>
		
		
        
		
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack()"/>
              </div>
          </section>
        </fieldset>
		<input type="hidden" name="templateId" value="<?=$id?>" />
		<input type="hidden" name="page" value="<?=$_GET['page']?>" />
</div>
	</form>
</section>
   <div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>
<?
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();

$admObj = new AdminDetail();

if (isset($_POST['submit'])) {
   $admObj->editAdminDetail($_POST);
}

$result = $admObj->getResult(base64_decode($_GET['id']));

?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
</head>
<body>
   <? include('includes/header.php'); ?>

   <section id="content">
      <h1>Edit Admin</h1>
      <fieldset>
      <!-- left position -->
      <form name="frmUser" id="frmUser" method="post" >
         <div id="mainDiv" >
            <fieldset>
               <label>Text Fields</label>


               <div id="div_duplicateUser"></div>
               <div id="div_duplicateEmail"></div>
               <div id="divMessage"></div>
               <?= $_SESSION['SESS_MSG'] ?>
               <? unset($_SESSION['SESS_MSG']) ?>

               <section>
                  <label for="userName">User Name</label>
                  <div><input type="text" name="userName" id="userName" class="wel" value="<?=$result->username?>" />
                     <div id="chk_userName"></div>
                  </div>
               </section>

               <section>
                  <label for="userEmail">Email</label>
                  <div><input type="text" name="userEmail" id="userEmail" value="<?=$result->emailId?>" />
                     <div id="chk_userEmail"></div>
                  </div>
               </section>

               <section>
                  <label for="password">Password</label>
                  <div><input type="password" name="password" id="password"  value=""/>
                     <div id="chk_password"></div>
                  </div>
               </section>
            </fieldset>
<?php if(($_SESSION['ADMIN_ID'] != $result->id) || ($_SESSION['USERLEVELID'] == '-1'))  {?>
            <fieldset>
               <label>Select Menu</label>
               <div id="selectMenu">

                  <table width="98%" border="0" cellspacing="0" cellpadding="0" class="documentation add-menu">
                     <tr>
                        <th width="25%"><span>Page</span></th>
                        <th align="center"><span>Add Record </span></th>
                        <th align="center"><span>Edit Record</span></th>
                        <th align="center"><span>Delete Record</span></th>
                     </tr>

<?php
    		$i = 0;	$j = 0;
			if($_SESSION['USERLEVELID'] == '-1')
			{
				$sqlMenu = $admObj->executeQry("SELECT * FROM ".TBL_MENU." 	WHERE parentId=0 AND status=1 ORDER BY menuId");
			}
			else {
				$sqlMenu = $admObj->executeQry("SELECT T1.*,T2.* FROM ".TBL_MENU." AS T1 INNER JOIN ".TBL_ADMINPERMISSION." AS T2 ON (T1.menuId=T2.menuid) 
				WHERE T1.parentId=0 AND T1.status=1 AND T2.adminLevelId='".$_SESSION['USERLEVELID']."' ORDER BY T1.menuId");
			}
			while($rowMenu = $admObj->getResultObject($sqlMenu))
			{			
				if($_SESSION['USERLEVELID'] == '-1')
				{
					$sqlSubMenu = $admObj->executeQry("SELECT * FROM ".TBL_MENU." WHERE parentId='$rowMenu->menuId' AND parentId!=0 AND status=1");
				}
				else {
					$sqlSubMenu = $admObj->executeQry("SELECT T1.*,T2.* FROM ".TBL_MENU." AS T1 INNER JOIN ".TBL_ADMINPERMISSION." AS T2 ON (T1.menuId=T2.menuid) 
					WHERE parentId='$rowMenu->menuId' AND T1.parentId!=0 AND T1.status=1 AND T2.adminLevelId='".$_SESSION['USERLEVELID']."' ORDER BY T1.menuId");
				}
				$countSubMenu = $admObj->getTotalRow($sqlSubMenu);
				
				$insertMenue1=$rowMenu->menuId;
				
				if ($countSubMenu==0)
				{
					//$insertMenue1=$rowMenu->menuId;
					?>
					<tr>
						<td><input type="checkbox"  name="menuCheck[]" value="<? echo $insertMenue1;?>" id="menuCheck" onclick="checkAllSingle('<?=$rowMenu->menuId?>','<?=$rowMenu->menu_type?>')" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '$insertMenue1'") > 0) { ?> checked="checked"<? } ?>/><? echo "<b>".$menuName=$rowMenu->menuName."</b>";?></td>
		
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_add"  id="menuCheck_<?=$insertMenue1?>_add" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'") == 1) { ?> checked="checked"<? } ?> ><? } ?></td>
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_edit" id="menuCheck_<?=$insertMenue1?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'") == 1) { ?> checked="checked"<? } ?>><? } ?></td>
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$insertMenue1?>_del" id="menuCheck_<?=$insertMenue1?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'") == 1) { ?> checked="checked"<? } ?>><? } ?></td>
					</tr>
		            <?
		        }
		        else
		        {
		        	$divName = "divId".$rowMenu->menuId;
		               ?>
		    		<tr>
						<td><input type="checkbox"  name="menuCheck[]" value="<?=$rowMenu->menuId?>"  id="menuCheckA<?=$rowMenu->menuId?>"  onclick="checkAll(<?=$countSubMenu?>, '<?=$i?>','<?=$rowMenu->menuId?>','<?=$rowMenu->menu_type?>')" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'") > 0) { ?> checked="checked"<? } ?>/> <? echo "<b>".$rowMenu->menuName."</b><br />";?>
						</td>
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_add" id="menuCheck_<?=$rowMenu->menuId?>_add" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_edit" id="menuCheck_<?=$rowMenu->menuId?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"edit_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
						<td align="center"><? if($rowMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowMenu->menuId?>_del" id="menuCheck_<?=$rowMenu->menuId?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"delete_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
					</tr>		
		            <?
				while ($rowSubMenu = mysql_fetch_object($sqlSubMenu)){
					$insertMenue=$rowSubMenu->menuId;
		?>
					<tr>
						<td>&nbsp;&nbsp;<input type="checkbox" name="menuCheck[]" value="<?=$insertMenue?>" onclick="checkMenu('<?=$i?>','<?=$rowSubMenu->menu_type?>')" id="menuCheckB<?=$i?>" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"menuid","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'") > 0) { ?> checked="checked"<? } ?>/><? echo $rowSubMenu->menuName."<br>"; ?>	</td>
						<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_add" id="menuCheckB<?=$i?>_add" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"add_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
						<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_edit" id="menuCheckB<?=$i?>_edit" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"edit_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'")  == 1) { ?> checked="checked"<? } ?>><? } ?></td>
						<td align="center"><? if($rowSubMenu->menu_type == 1) { ?>-<? } else { ?><input type="checkbox" name="menuCheck_<?=$rowSubMenu->menuId?>_del"  id="menuCheckB<?=$i?>_del" value="1" <? if($admObj->fetchValue(TBL_ADMINPERMISSION,"delete_record","1 and adminLevelId = '".$result->adminLevelId."' and menuid = '".$rowSubMenu->menuId."'") == 1) { ?> checked="checked"<? } ?>><? } ?></td>
					</tr>
		<? 			$i++;
				}
		           $j++;
		     }
		 }
 ?>
    </table>
               </div>
               </fieldset>
               <?php }?>
               <fieldset>
                  <section>
                     <div>
                       <input type="hidden" name="adminLevelId" value="<?=$result->adminLevelId?>">
                        <input type="hidden" name="admin_id" value="<?=$result->id?>">
                        <button type="submit" name="submit" onClick="return validationEditAdmin()">Submit</button>
                        <button type="button" name="back" id="back" onClick="hrefBack()">Back</button>
                     </div>
                  </section>
               </fieldset>
      </form>
</fieldset>

      <div id="divTemp" style="display:none;"></div>
   </section>

   <script language="javascript">
      <!--

      function checkMenu(id,menu_type)
      {
         if(menu_type == 0) {
            if (document.getElementById("menuCheckB"+id).checked == true)
               checked = true
            else
               checked = false
		
            document.getElementById("menuCheckB"+id+"_add").checked = checked;
            document.getElementById("menuCheckB"+id+"_edit").checked = checked;
            document.getElementById("menuCheckB"+id+"_del").checked = checked;
         }
      }

      function checkAll(count, id, id1, menu_type)
      {
         if (document.getElementById("menuCheckA"+id1).checked == true)
            checked = true
         else
            checked = false

         if(menu_type == 0) {
            document.getElementById("menuCheck_"+id1+"_add").checked = checked;
            document.getElementById("menuCheck_"+id1+"_edit").checked = checked;
            document.getElementById("menuCheck_"+id1+"_del").checked = checked;
         }

         for(var i=0; i<count; i++)
         {
            document.getElementById("menuCheckB"+id).checked = checked;
		
            var ctr=0;
            var field_name = '';
            var frm = document.frmUser;

            for (ctr=0; ctr < frm.length; ctr++)
            {
               field_name = frm.elements[ctr].id;

               //if (field_name.indexOf("menuCheckB"+id+"_add") == -1)
               if ((field_name == "menuCheckB"+id+"_add") || (field_name == "menuCheckB"+id+"_edit") || (field_name == "menuCheckB"+id+"_del"))
               {
                  frm.elements[ctr].checked = checked;
               }
            }
		
            //document.getElementById("menuCheckB"+id+"_add").checked = checked;
            //document.getElementById("menuCheckB"+id+"_edit").checked = checked;
            //document.getElementById("menuCheckB"+id+"_del").checked = checked;
            id++;
         }

      }

      function checkAllSingle(id1,menu_type)
      {
         if(menu_type == 0) {
            if (document.getElementById("menuCheck").checked == true)
               checked = true
            else
               checked = false
		
            document.getElementById("menuCheck_"+id1+"_add").checked = checked;
            document.getElementById("menuCheck_"+id1+"_edit").checked = checked;
            document.getElementById("menuCheck_"+id1+"_del").checked = checked;
         }
      }

      function hrefBack()
      {
         window.location.href = "administrator.php";
      }
   </script>

<?php

session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');

$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$genObj = new GeneralFunctions();
$colorbj = new Color();

?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,Top Pageoptions ?>

<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

<script src="js/ajax.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<style type="text/css">
	.imgClass {border: 2px solid;float: left; margin-left:14px;height: 45px;width: 50px;}
</style>
</head>

<body>
<? include('includes/header.php'); ?>
<div id="fadeContent" style="background-color: #000000; display:none; opacity: 0.6; z-index:500; width:1082px; height:100%; position:absolute;"></div>
<section id="content">
	
	<h1>Color Pallets</h1>
	<fieldset>
		<form name="ecartFrm" method="post" action="pass.php?action=Color&type=deleteall"   >
			<label>Pallet Details</label>	
			
			<div class="top-filter bg02">
				<div id="check" class="seclet">&nbsp;</div>				
				<div>
			</div>
				<div id="search-main-div">
					<ul>
						<li>Select Color Pallet</li>
						<li>
							<select id="palletName" name="palletName" onchange="return showPallets(this.value);">
								<option value="">--Select Pallet--</option>
								<?=$colorbj->getPalletName();?>								
							</select>
						</li>
					<li><input id="editSelected" disabled="disabled" onclick="return goToEdit();" type="button" value="Edit Selected" /></li>
					<li><input onclick="javascript:window.location='addColor.php';" type="button" value="Create New" /></li>
					</ul>
				</div>	
			</div>
			<div class=" ">
				<? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
			</div>
				
			<section id="palletContainer"></section>				
		</form>
	</fieldset>	
</section>
<?php include_once('includes/footer.php'); ?>
<script type="text/javascript">
	function goToEdit()
	{
		var id = document.getElementById("palletName").value;
		window.location='addColor.php?palletId='+id;
	}
</script>

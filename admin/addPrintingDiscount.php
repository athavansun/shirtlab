<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("managePrintingDiscount.php","");
$colorObj = new Printing();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('pcs',$_POST['pcs'], 'req', 'Please Enter Number of pcs.');
         $obj->fnAdd('sign',$_POST['sign'], 'req', 'Please select sign.');	
	//$obj->fnAdd('charges',$_POST['charges'], 'req', 'Please Enter charges.');
        
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[pcs]=$obj->fnGetErr($arr_error[pcs]);
	$arr_error[sign]=$obj->fnGetErr($arr_error[sign]);
	//$arr_error[charges]=$obj->fnGetErr($arr_error[charges]);
	
	if($str_validate){
		$_POST = postwithoutspace($_POST);
		$colorObj->addPrintingDiscount($_POST);		                
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='managePrintingDiscount.php';
}

function checkSign(item) {
	if(item == 1) {		
		document.getElementById("m__charges").disabled = true; 
	}else {		
		document.getElementById("m__charges").disabled = false;
	}
}

</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Printing Charges</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
			<fieldset>
				
				<label>Printing Charges</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!-- Start : Currency Name ------->
				<section>
					  <label for="FirstName">Pcs</label>
					  <div>
					  	<input type="text" name="pcs" id="m__pcs"  class="wel" value="<?=stripslashes($_POST[pcs])?>" style="width:200px;" />
						<?=$arr_error[pcs]?>
					  </div>					 
				</section> 
				
				<section>
					 <label for="FirstName">Sign</label>
					 <div>
					   	<select name="sign" onchange="return checkSign(this.value);">
	                   		 <option value="">Select Sign</option>                                   
		                     <option value="0">minus</option>
		                     <option value="2">plus</option>
		                     <option value="1">x</option>                                    
		                </select>
						<?=$arr_error[sign]?>
					  </div>					 
				</section>
																
				<section>
					  <label for="FirstName">Charges</label>
					  <div>
					  	<input type="text" name="charges" id="m__charges"  class="wel" value="<?=stripslashes($_POST[charges])?>" style="width:200px;" /><span style="margin-left:3px; font-size: 17px;">%</span>
						<?=$arr_error[charges]?>
					  </div>					 
				</section>
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />					
                    <input type="button" name="back" id="back" value="Back"   onclick="javascript:hrefBack1()"/>       
				</div>
             </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
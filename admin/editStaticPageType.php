<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStaticPage.php","edit_record");
$staticpageObj = new StaticPage();
$genObj = new GeneralFunctions();

$id = base64_decode($_GET['id']);

/*---Basic for Each Page Ends----*/

if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$errorFlag = 0;
	$obj->fnAdd('pageName',$_POST['pageName'], 'req','Please enter page name');
	$obj->fnAdd('pageUrl', $_POST['pageUrl'], 'req', 'Please enter page url');
	$arr_error = $obj->fnValidate();
	
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error['pageName']=$obj->fnGetErr($arr_error['pageName']);
	$arr_error['pageUrl']=$obj->fnGetErr($arr_error['pageUrl']);

	if($staticpageObj->isPageNameExit($_POST['pageName'], $id)){ 
		   $arr_error['pageName'] = '<div><span class="alert-red alert-icon">Page name already exist.</span></div>';
		   $errorFlag = 1;
		 }
	
	if($staticpageObj->isPageUrlExit($_POST['pageUrl'], $id)){ 
		   $arr_error['pageUrl'] = '<div><span class="alert-red alert-icon">Page url already exist.</span></div>';
		   $errorFlag = 1;
		 }

	if($str_validate && ($errorFlag == 0)){
		$_POST = postwithoutspace($_POST);
		$staticpageObj->editStaticPageType($_POST);
	}
}
	$result = $staticpageObj->staticPageTypeToEdit($id);
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageStaticPage.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Miscellaneous</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<input type="hidden" name="id" id="id" value="<?=$id?>">
		 <fieldset>  
            <label>Edit static page type</label>
			<?=$_SESSION['SESS_MSG']?>
            <section>
                  <label>Page name</label>
                  <div>
                       	<input type="text" name="pageName" id="m__Page_Name" value="<?=stripslashes($_POST['pageName'] ? $_POST['pageName'] : $result['staticPageName'] )?>" />				 
					    <div><?=$arr_error['pageName']?></div>
				  </div>	
            </section>
            <section>
                  <label>Page url</label>
                  <div>
                    <input type="text" name="pageUrl" id="m__Page_Url" value="<?=stripslashes($_POST['pageUrl'] ? $_POST['pageUrl'] : $result['pageUrl'] )?>" />
					<div><?=$arr_error['pageUrl']?></div>
				  </div>	
            </section>
			
		</fieldset>
             <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
              </div>
                </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

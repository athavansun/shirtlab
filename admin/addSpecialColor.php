<?
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php", "");
/* ---Basic for Each Page Ends---- */
$genObj = new GeneralFunctions();
$proObj = new Products();
//$abc = array();
//$proObj->saveSpecialColor($abc, $_SESSION['special_color_data']);
//exit;
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
if (isset($_POST['updateSpPart'])) {
    require_once('validation_class.php');
    $obj = new validationclass();
    $errorArr = 0;

    //$obj->fnAdd('code',$_POST['code'], 'req', 'Please Enter code.');
    $obj->fnAdd('colorCode', $_POST['colorCode'], 'req', 'Please Enter colorCode code.');

    //===============================================================
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
    //===============================================================
    //$arr_error['code']=$obj->fnGetErr($arr_error['code']);
    $arr_error['colorCode'] = $obj->fnGetErr($arr_error['colorCode']);

    $permitableExt = $proObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='IMAGE_EXTENSION'");
	if($_FILES['colorImage']['name'] != '') {					
		$fileext = findexts($_FILES['colorImage']['name']);
		if($fileext) {
			if($proObj->checkExtensions($fileext) == false ) {
				$arr_error['colorImage'] = '<span class="alert-red alert-icon">Invalid Image Extention,Please Upload Image With Extention '.$permitableExt.'.</span>';
				$str_validate=0;
			}
		}
		else {
			$arr_error['colorImage'] = '<span class="alert-red alert-icon">Invalid Image Extention, Please Upload Image With Extention '.$permitableExt.'.</span>';
			$str_validate=0;
		}	
	}
    if ($str_validate) {
        
        $proObj->updateSpecialColor($_POST, $_FILES);
    }
}

if (isset($_POST['saveSpPart'])) {
    require_once('validation_class.php');
    $obj = new validationclass();
    $errorArr = 0;

    //$obj->fnAdd('code',$_POST['code'], 'req', 'Please Enter code.');
    $obj->fnAdd('colorCode', $_POST['colorCode'], 'req', 'Please Enter colorCode code.');

    //===============================================================
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
    //===============================================================
    //$arr_error['code']=$obj->fnGetErr($arr_error['code']);
    $arr_error['colorCode'] = $obj->fnGetErr($arr_error['colorCode']);

    $permitableExt = $proObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='IMAGE_EXTENSION'");
	if($_FILES['colorImage']['name'] != '') {					
		$fileext = findexts($_FILES['colorImage']['name']);
		if($fileext) {
			if($proObj->checkExtensions($fileext) == false ) {
				$arr_error['colorImage'] = '<span class="alert-red alert-icon">Invalid Image Extention,Please Upload Image With Extention '.$permitableExt.'.</span>';
				$str_validate=0;
			}
		}
		else {
			$arr_error['colorImage'] = '<span class="alert-red alert-icon">Invalid Image Extention, Please Upload Image With Extention '.$permitableExt.'.</span>';
			$str_validate=0;
		}	
	}
    if ($str_validate) {
        $proObj->saveSpecialColor($_POST, $_FILES);
    }
}
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script type="text/javascript" src="colorpicker/color_functions.js"></script>
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" type="text/css" />
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
    <section id="content-detail">
        <form id="formStylePart" name="formStylePart" method="post" enctype="multipart/form-data" action="">
            <!----------Add part section------->
            <fieldset>
                <label>Edit Product Color</label>
<?php
if ($_GET['action'] == 'edit') {
    $result = $proObj->getSpPalletDetails($_GET['colorId']);
    ?>

                    <section>
                        <label>Color Name:</label>
                        <div>
                            <input type="text" name="colorCode" class="validate[required] text-input" style="width:200px;" value="<?= $result->code; ?>" />&nbsp;
                    <?= $arr_error['name']; ?>                
                            
                        </div>
                    </section>
                
                    <section>
                        <label>Color Image:</label>
                        <div>
                            <input type="file" name="colorImage" class="text-input" style="width:200px;" value="" />&nbsp;
                            <input type="hidden" name="colorImageValue" value="<?= SITE_URL.__COLORTHUMB__.$result->colorImage;?>">
                            <br /><br />
                            <img src="<?php echo SITE_URL.__COLORTHUMB__.$result->colorImage; ?>" alt="Color Image" height="40" width="40"/>
                    <?= $arr_error['name']; ?>                
                        </div>
                    </section>
                    <section>
                        <label>Add Style Part</label>
                    </section>
                    <input type="hidden" id="hitCount" value="100" />
                            <?= $proObj->getSpColorEdit(base64_decode($_GET['sId']), $_GET['colorId']); ?>
                    <section>
                        <div>
                            <input type="hidden" name="colorId" value="<?= $_GET['colorId']; ?>" />
                            <input type="hidden" name="sId" value="<?= base64_decode($_GET['sId']); ?>" />
                            <input type="submit" name="updateSpPart" style="cursor:pointer;" value="Submit" />
                            <!--<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">-->
                        </div>
                    </section>

                    <!----------Add part section end------->
    <?php
} else {
    ?>
                    <section>
                        <label>Color Name:</label>
                        <div>
                            <input type="text" name="colorCode" class="validate[required] text-input" style="width:200px;" value="<?= $result->code; ?>" />&nbsp;
                    <?= $arr_error['name']; ?>                

                        </div>
                    </section>
                
                    <section>
                        <label>Color Image:</label>
                        <div>
                            <input type="file" name="colorImage" class="validate[required] text-input" style="width:200px;" value="" />&nbsp;
                            
                        </div>
                    </section>       
                    <section>
                        <label>Add Style Part</label>
                    </section>
                    <input type="hidden" id="hitCount" value="100" />        
    <?php echo $proObj->getSpecialStylePart(base64_decode($_GET['pId']), base64_decode($_GET['sId'])); ?>
                    <section>
                        <div>
                            <input type="hidden" name="sId" value="<?= base64_decode($_GET['sId']); ?>" />
                            <input type="hidden" name="pId" value="<?= base64_decode($_GET['pId']); ?>" />
                            <input type="submit" name="saveSpPart" style="cursor:pointer;" value="Submit" />
                            <!--<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">-->
                        </div>
                    </section>

                    <?php
                }
                ?>
            </fieldset>
        </form>
    </section>
    <script type="text/javascript">
        jQuery(document).ready( function() {
            jQuery("#formStylePart").validationEngine();
        });
    </script>    
                <? include_once('includes/footer.php'); ?>

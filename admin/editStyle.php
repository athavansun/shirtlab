<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageStyle.php","edit_record");
/*---Basic for Each Page Starts----*/
$styleObj = new Style();
$generalFunctionObj = new GeneralFunctions();

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $styleObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $styleObj->getTotalRow($rst);	
	if($num > 0){
		$langIdArr = array();		
		while($line = $styleObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('name_'.$value,$_POST['name_'.$value], 'req', 'Please Enter Name');			
		}
		$obj->fnAdd('addPrice',$_POST['addPrice'], 'req', 'Please Enter Additional Price For Per Pair');
		//==================================================		
		$arr_error = $obj->fnValidate();
	   	$str_validate = (count($arr_error)) ? 0 : 1;
	   //================================================== 
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['name_'.$value]=$obj->fnGetErr($arr_error['name_'.$value]);
		}
		$arr_error['addPrice'] = $obj->fnGetErr($arr_error['addPrice']);
	}
	//var_dump($arr_error); die;
	//echo $str_validate; die;
	if($str_validate){
		//echo 'Rashid'; die;
		$styleObj->editRecord($_POST);	
	}
}
$result = $styleObj->getResult(base64_decode($_GET['id']));?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageStyle.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Flop Style</h1></h1>
  		<fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
  		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">	
  		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		 <fieldset>  
            <label>Edit Size</label>
			<?=$_SESSION['SESS_MSG']?>
			<section>
                <label>Style Type</label>
                <div>
                	<select name="styleType" id="styleType">
					<?=$styleObj->getStyleType($result->type);?>
					</select>
			</div>
            </section>
            <section>
                  <label>Name</label>
                  <div>
					<?=$generalFunctionObj->getLanguageEditTextBox('name','m__Name',TBL_STYLEDESC,base64_decode($_GET['id']),"styleId",$arr_error) ?>
				</div>	
            </section>
             <section>
                <label>Additional Price/Pair</label>
                <div><input type="text" name="addPrice" value="<?php echo $_POST['addPrice']?$_POST['addPrice']:$result->addPricePerPair;?>" />
                <div id="chk_addPrice"><?=$arr_error['addPrice'];?></div></div>
            </section>
		</fieldset>
        <fieldset> 
               <section>  
             	 <label>&nbsp;</label>
              <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
              </div>
                </section>
        </fieldset>
	</form>
	</fieldset>
</section>
<? unset($_SESSION['SESS_MSG']); ?><table></table>
<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
/*---Basic for Each Page Ends----*/
$giftCertiObj = new GiftCertificate();
$genObj = new GeneralFunctions();
$id = base64_decode($_GET[id]);
$result = object;
$result = $giftCertiObj->viewGiftCertificate($id);
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>View Gift Voucher</label>
 		  <!-- left position -->
            <section>
       	  <label>Code</label>
             <div>
			 	<?=$result->giftCertificateCode?>
            </div>
          </section>
            <section>
       	  <label>To</label>
             <div>
			 	<?=$result->toName?>
            </div>
          </section>
            <section>
       	  <label>From</label>
             <div>
			 	<?=$result->fromName?>
            </div>
          </section>
          <section>
       	  <label>Expiry date</label>
             <div>
   				<?=$result->couponExpiryDate?>
            </div>
          </section>
		  
          <section>
       	  <label>Coupon Image</label>
             <div>
   				<img src="<?=SITE_URL.__GIFT_COUPON_IMAGE__.$result->couponImage?>">
            </div>
          </section>
     </fieldset>       
          </form>  
    </section>        
</body>
</html>

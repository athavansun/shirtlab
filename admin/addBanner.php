<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$genObj = new GeneralFunctions();
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageBanner.php", "add_record");
/* ---Basic for Each Page Ends---- */

$bannerObj = new Banner();
$generalFunctionObj = new GeneralFunctions();
if($_POST['inbuiltBannerGroup']){
    $display="display:none;";
    $sizeMsg="";
}else{
    $sizeMsg="<span id='sizeMsg'>,  or enter a new banner group below</span>";
}

if (isset($_POST['submit'])) {
        //echo "<pre>"; print_r($_POST); echo "</pre>";exit;
        require_once('validation_class.php');
        $obj = new validationclass();
        $rst = $bannerObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
        $num = $bannerObj->getTotalRow($rst);
        if ($num > 0) {
                $langIdArr = array();
                while ($line = $bannerObj->getResultObject($rst)) {
                    array_push($langIdArr, $line->id);
                }
                
                
                //Add Error===================
                foreach ($langIdArr as $key => $value) {
                    $obj->fnAdd('bannerTitle_' . $value, $_POST['bannerTitle_' . $value], 'req', 'This field is requied.');
                    //$obj->fnAdd('bannerHtmlText_' . $value, $_POST['bannerHtmlText_' . $value], 'req', 'This field is required');
                }
                if($_POST['inbuiltBannerGroup']==""){
                    $obj->fnAdd("bannerGroup", $_POST["bannerGroup"], "req", "This field is required.");
                }
                $obj->fnAdd("bannerUrl", $_POST["bannerUrl"], "req", "This field is required.");
                //$obj->fnAdd("bannerUrl", $_POST["bannerUrl"], "url", "Invalid Url.");
                $obj->fnAdd("bannerImage", $_FILES['bannerImage']['name'], "req", "This field is required.");
                
                
                //Validate===============
                $arr_error = $obj->fnValidate();
                $str_validate = (count($arr_error)) ? 0 : 1;
        
                
                //Get Error================
                foreach ($langIdArr as $key => $value) {
                    $arr_error['bannerTitle_' . $value] = $obj->fnGetErr($arr_error['bannerTitle_' . $value]); 
                    //$arr_error['bannerHtmlText_' . $value] = $obj->fnGetErr($arr_error['bannerHtmlText_' . $value]);        
                }
                if($_POST['inbuiltBannerGroup']==""){
                    $arr_error['bannerGroup'] = $obj->fnGetErr($arr_error['bannerGroup']);  
                }
                $arr_error['bannerUrl'] = $obj->fnGetErr($arr_error['bannerUrl']);  
                $arr_error['bannerImage'] = $obj->fnGetErr($arr_error['bannerImage']);  
                
                
                //Manual Error Check==============
                if ($_FILES['bannerImage']['name']) {
                    $filename = stripslashes($_FILES['bannerImage']['name']);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);
                    if (!$bannerObj->checkExtensions($extension)) {
                        $arr_error[bannerImage] = '<span class="alert-red alert-icon">Upload Only ' . $bannerObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='IMAGE_EXTENSION'") . ' image extension.</span>';
                        $str_validate=0;
                    }

                    #---check banner size-------------------Begin
                    if($_POST[inbuiltBannerGroup]){
                        $bannerSize=$bannerObj->fetchValue(TBL_BANNER,'bannerSize',"bannerGroup ='".$_POST[inbuiltBannerGroup]."'");                    
                        list($w,$h)=getimagesize($_FILES['bannerImage']['tmp_name']);   
                        $uploadedSize=$w."x".$h;                
                        if($uploadedSize!=$bannerSize){
                        $arr_error[bannerImage] = '<span class="alert-red alert-icon">Uploaded image size is '.$uploadedSize.'. Uploaded images size must be <b>'.$bannerSize.'</b>.</span>';
                        $str_validate=0;
                        }               
                    }
                    #---Check Banner Size-------------------End
                }             
                
                
                if ($str_validate) {
                    //echo "<pre>";print_r($_POST);print_r($_FILES);echo "</pre>";exit;                  
                    $bannerObj->addNew($_POST, $_FILES);
                }
        }
       
}
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions    ?>
<!--<script language="javascript" src="js/requiredValidation.js"></script>-->

<script type="text/javascript">
   function hrefBack1(){
      window.location='manageBanner.php';
   }
</script>

</head>
<body>
   <?php
   include('includes/header.php');
   if ($cid)
      $addHadding = 'Subcategory';
   else
      $addHadding = 'Category';
   ?>
   <section id="content">
      <h1>Banner</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <fieldset>              
               <label>Add Banner</label>
               <?= $_SESSION['SESS_MSG'] ?>
               <section>
                  <label for="Banner Title">Banner Title </label>
                  <div>
                     <?= $genObj->getLanguageTextBoxEngValidation('bannerTitle', 'm__bannerTitle', $arr_error); ?>
                  </div>
               </section>
               <section>
                  <label for="Banner URL">Banner URL</label>
                  <div>
                     <input type="text" name="bannerUrl" id="m__bannerUrl" class="wel" value="<?=stripslashes($_POST[bannerUrl]) ?>" />&nbsp;Ex :- http://<?= $_SERVER['HTTP_HOST'] ?>
                     <?= $arr_error['bannerUrl'] ?>
                  </div>
               </section>
               <section>
                  <label for="BannerGroup">Banner Group</label>
                  <div><?= $bannerObj->getInbuiltBannerGroup($_POST[inbuiltBannerGroup]) ?><? if($_SESSION['USERLEVELID']=='-1'){echo $sizeMsg;}?> 
                  </div>
                  <? if($_SESSION['USERLEVELID']=='-1'){?>
                  <div id="bannerGroupDiv" style="<?=$display?>">
                     <input type="text" id="bannerGroup" name="bannerGroup" class="wel" value="<?=stripslashes($_POST[bannerGroup]) ?>" />                                        
                  </div>
                  <? } ?>
                  <div><?= $arr_error['bannerGroup'] ?></div>
               </section>
               
                <? if($_SESSION['USERLEVELID']=='-1'){?>
                <section id="bannerSizeSec"  style="<?=$display?>">
                  <label for="bannerSize">Banner Size</label>
                  <div><input type="text"  name="bannerSize" id='bannerSize'  class="browse-button" value="" />
                      <?= $arr_error['bannerSize'] ?>
                  </div>
               </section>
               <? } ?>
               <section>
                  <label for="clipArtImage">Banner Image</label>
                  <div><input type="file"  name="bannerImage" id='m__banner_Image'  class="browse-button" value="" />
                      <?= $arr_error['bannerImage'] ?>
                  </div>
               </section>
               
               
<!--               <section>
                  <label for="HTMLText">HTML Text</label>
                  <div>
                     <?= $genObj->getLanguageTextarea('bannerHtmlText','m__bannerHtmlText',$arr_error); //1->type,2->name,3->id ?>
                  </div>
               </section>      
               <section>
                  <label for="ScheduledAt">Scheduled At</label>
                  <div>
                     <input type="text" name="startDate" id="startDate" class="wel" value="<? if(!$_POST[startDate]) echo date('Y-m-d H:i:s'); else echo $_POST[startDate];?>" />
                  </div>
               </section>
                 <section>
                  <label for="ScheduledAt">Expires On:</label>
                  <div>
                     <input type="text"  name="endDate" id="endDate" class="wel" value="<?=$_POST[endDate]?>" />
                  </div>
               </section>
                <section>
                  <label for="ImpressionViews">Impression/Views:</label>
                  <div>
                     <input type="text"  name="expires_impressions" class="wel" value="<?=stripslashes($_POST[expires_impressions])?>" />
                  </div>
               </section>
               -->
               
                <section>
                  <label for="OpenIn">Open In:</label>
                  <div>
                     <input type="radio" name="openIn" value="0" checked="checked" /> Same Window <input type="radio" name="openIn" value="1" /> Another Window
                  </div>
               </section>
               
               
            </fieldset>
            <fieldset>
               <section>
                  <label>&nbsp;</label>
                  <div style=" width:78%;">
                     <input type="submit" name="submit"   value="Submit" />
                     <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
                  </div>
               </section>
            </fieldset>
         </form>
      </fieldset>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>
<script>
    function showBannerSize(){
        var val=$("#inbuiltBannerGroup").val();
        if(val!=""){
            $("#bannerGroupDiv").css("display","none");
            $("#bannerSizeSec").css("display","none");
            $("#sizeMsg").css("display","none");
        }else{
            $("#bannerGroupDiv").css("display","");
            $("#bannerSizeSec").css("display","");
            $("#sizeMsg").css("display","");
        }
    }
</script>

<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//require_once('includes/classes/deliveryTime.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */
$qtyObj = new TotalQuantity();

if (isset($_POST['submit'])) {
    $_POST = postwithoutspace($_POST);
    $qtyObj->editQuantitySurcharges($_POST);    
    $rst = $qtyObj->executeQry("Select * from ".TBL_QUANTITY_SURCHARGE);
}else {
	$rst = $qtyObj->executeQry("Select * from ".TBL_QUANTITY_SURCHARGE);
        $num = $qtyObj->num_rows($rst);
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
     	  <h1>Manage Quantity Surcharge<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addQuantityDiscount.php">Add Quantity Discount</a><? } ?></h1>	   
         <!-- <h1>Embroidery</h1>	 --> 
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);">
   <?		echo $_SESSION['SESS_MSG'];                
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
                <label>Total Quantity Surcharge</label>
                <?php                
                    while($line = $qtyObj->getResultObject($rst)){ ?>                    	
                        <section>                    
                            <div style="width:55%; float:left;">                                                                                   
                                    <span style="font-size: 13px; margin-left: 25px;"><?= $line->quantity;?> % of total quantity</span>
                            </div>                           
                            
                            <div style="width:7%; float:left;">   
                                <span style="font-size: 13px; margin-left:11px;">add</span>
                            </div>
                            
                            <div style="width:22%; float:left;">
                                <input type="text" name="charge_<?= $line->id;?>" id="charge_<?= $line->id;?>" value="<?= number_format($line->charge, 2); ?>" class="wel" style="width:75px;" />     
                                <span style="font-size: 17px;"> % </span>
                            </div>
                            
                             <div style="width:5%; float:left;">
                             	<a class='i_trashcan edit' href='javascript:void(0);'  onClick="if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageQuantity&type=delete&id=<?php echo $line->id;?> &page=$page'}else{}" >Delete</a>
                             </div>                                                                 
                        </section>
                <?  
                }
                ?>   
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>

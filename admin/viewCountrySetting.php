<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabric.php", "");
/* ---Basic for Each Page Starts---- */

$countryObj = new CountrySetting();

$generalFunctionObj = new GeneralFunctions();

$result = $countryObj->getResult(base64_decode($_GET['id']));
$eoriNo = $result->eoriNo == 1?"Yes":"No";
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions   ?>

</head>
<body>
   <section id="content-detail">
      <form>
         <fieldset>
            <label>View Detail</label>
            <!-- left position -->
            <div><?= $_SESSION['SESS_MSG'] ?></div>
            <section>
               <label>Country Name</label>
               <div>
                  <?
					echo $result[countryName];
                  ?>
               </div>
            </section>
             
             <section>
               <label>Currency Sign</label>
               <div>
                  <?
					echo html_entity_decode($result[sign]);
                  ?>
               </div>
            </section>
            
            <section>
               <label>Vat</label>
               <div>
                  <?
					echo $result[vat];
                  ?>
               </div>
            </section>
            
            <section>
               <label>Custom Duty</label>
               <div>
                  <?
					echo $result[customDuty];
                  ?>
               </div>
            </section>
            
            <section>
               <label>EORI No</label>
               <div>
                  <?
					echo $eoriNo;
                  ?>
               </div>
            </section>
 
            </section>
         </fieldset>
      </form>
   </section>
</body>
</html>

<?
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
//$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$proObj = new Products();
if(isset($_POST['saveZoomImage']))
{
	require_once('validation_class.php');
	$obj = new validationclass();
	
	$permitableExt = $proObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='IMAGE_EXTENSION'");
    $str_validate = 1;
    if($_FILES['zoomImg']['name'] != '') {		
        $fileext = findexts($_FILES['zoomImg']['name']);
		if($fileext) {
			if($proObj->checkExtensions($fileext) == false ) {
				$arr_error['$zoomImg '] = '<span class="alert-red alert-icon">Invalid Image Extention,Please Upload Image With Extention '.$permitableExt.'.</span>';
				$str_validate=0;
			}
		}
		else {
			$arr_error['$zoomImg '] = '<span class="alert-red alert-icon">Invalid Image Extention, Please Upload Image With Extention '.$permitableExt.'.</span>';
			$str_validate=0;
		}	
	}
    
	if($str_validate)
	{	
		$proObj->editZoomableImage($_POST, $_FILES);
	}
}

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
 <section id="content-detail">
 <form id="formZoom" method="post" enctype="multipart/form-data" action="">
 	<fieldset>
    <label>Edit Product Zoomable Image</label>                
        <section>
       	  <label>Image</label>
             <div>
				 <?php                 
					$zoomImg = $proObj->fetchValue(TBL_PRODUCT_ZOOMIMAGE, "zoomImage"," id = '".base64_decode($_GET['id'])."'");
				
                    $image = checkImgAvailable(__ZOOMPRODUCTTHUMB__.$zoomImg);
				 ?>
				<img width="60" src="<?=$image;?>" /> <br />
				<input type="hidden" name="oldStyleImage" value="<?=$zoomImg ;?>" />
				<input type="file" name="zoomImg" />
				<?=$arr_error['$zoomImg '];?>
                                <pre>Please upload minimum size of image should be 293 * 380 (width * height)</pre>
            </div>
        </section>
        
        <section>
			<div>
                <input type="hidden" name="pId" value="<?=base64_decode($_GET['pId']);?>" />
				<input type="hidden" name="id" value="<?=base64_decode($_GET['id']);?>" />
				<input type="submit" name="saveZoomImage" style="cursor:pointer;" value="Submit" />				
			</div>
		</section>
     </fieldset>     
     </form>
    </section>   
    
<? include_once('includes/footer.php');?>

<?php
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageState.php","edit_record");
/*---Basic for Each Page Ends----*/

$stateObj = new State();
$genObj = new GeneralFunctions();


if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
	
	
		
		/// Validate Category Name
		$obj->fnAdd('stateName',$_POST['stateName'], 'req','Please enter state name');
				$obj->fnAdd('stateCode',$_POST['stateCode'], 'req', 'Please enter state code (3)');
		$obj->fnAdd('stateCode2',$_POST['stateCode2'], 'req', 'Please enter state code (2)');			
		$obj->fnAdd('countryId',$_POST['countryId'], 'req','Please select country');		
		
		/// Get Validation : $str_valiate=0 for ERROR; $str_validate=1 for NO ERROR
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		
		/// Get ERROR ARRAY	=====
		$arr_error[stateName]=$obj->fnGetErr($arr_error[stateName]);
		$arr_error[stateCode]=$obj->fnGetErr($arr_error[stateCode]);
		$arr_error[stateCode2]=$obj->fnGetErr($arr_error[stateCode2]);			
		$arr_error[countryId]=$obj->fnGetErr($arr_error[countryId]);		
		
		//// GET SPECIFIC ERROR & SET $str_validate=0 if get error START==================	
		$value= $_SESSION['DEFAULTLANGUAGEID'];//$value=__DEFAULT_ACTIVE_LANG_ID__;
		if($stateObj->isStateNameExist($_POST['stateName'],$_POST['countryId'],$value,base64_decode($_GET['id']))){
			$arr_error[stateName] = '<span class="alert-red alert-icon">State name already exists.</span>';
			$str_validate=0;				
		}
		
		/// Submit Record if Validation Succeed=====================================
		if($str_validate){
			$_POST = postwithoutspace($_POST);	
			$stateObj->editRecord($_POST,$_FILES);
		}
	
}
$result = $stateObj->getResult(base64_decode($_GET['id']));
//echo"<pre>";print_r($result); echo"</pre>";

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!-- New Drop Down menu -->

<script type="text/javascript">
function hrefBack1(){
	window.location='manageState.php';
}
</script>
</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
	<h1>Edit State</h1><fieldset>  
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>State</label>
   		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
   		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
			
        <div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset>  
      
		
		<section>
        	<label for="Country">Country</label>
			<div>
			<select name="countryId" id="m__Country" >
			<?=$genObj->getCountryInDropdown($_POST['countryId']?$_POST['countryId']:$result->countryId)?>
			</select>
			<?=$arr_error[countryId]?>
			</div>
        </section>
		
		<section>
        	<label for="Country Code">State Code (3)</label>
			<div><input type="text" name="stateCode" id="m__stateCode" class="wel" value="<?=$_POST['stateCode']?$_POST['stateCode']:$result->stateCode?>" />
			<?=$arr_error[stateCode]?>
			</div>
        </section>
        <section>
        	<label for="Country Code">State Code (2)</label>
			<div><input type="text" name="stateCode2" id="m__stateCode2" class="wel" value="<?=$_POST['stateCode2']?$_POST['stateCode2']:$result->state_2_code?>" />
			<?=$arr_error[stateCode2]?>
			</div>
        </section>
		
		<section>
        	<label for="Country Code">State Name</label>
			<div><input type="text" name="stateName" id="m__stateName" class="wel" value="<?=$_POST['stateName']?$_POST['stateName']:$result->stateName?>" />
			<?=$arr_error[stateName]?>
			</div>
        </section>
		
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
          </section>
        </fieldset>
</div>
	</form></fieldset>  
</section>
   <div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>
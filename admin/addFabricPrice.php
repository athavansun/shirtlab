<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabric.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$fabricObj = new Fabric();

$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();

if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	
	$obj->fnAdd('fabricId',$_POST['fabricId'], 'req','Please Select Fabric Name');	
	$obj->fnAdd('fQuality',$_POST['fQuality'], 'req','Please Enter Fabric Quality');		
	$obj->fnAdd('percent',$_POST['percent'], 'req','Please Enter Fabric Percent');
	$obj->fnAdd('pallet',$_POST['pallet'], 'req','Please Select Color Pallet');
	
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error['fabricId']=$obj->fnGetErr($arr_error['fabricId']);		
	$arr_error['fQuality']=$obj->fnGetErr($arr_error['fQuality']);		
	$arr_error['percent']=$obj->fnGetErr($arr_error['percent']);
	$arr_error['pallet']=$obj->fnGetErr($arr_error['pallet']);
	//print_r($arr_error);
	if($str_validate){
		$_POST = postwithoutspace($_POST);
		$fabricObj->addFabricPrice($_POST);
	}
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!-- <script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
 <script src="js/file/jquery.filestyle.js"></script>	
<script src="js/file/add-more.js"></script>		 -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageFabricPrice.php';
}

</script>
<!-- New Drop Down menu -->

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Fabric</h1><fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">		
		 <fieldset>  
            <label>Fabric</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                <label>Fabric Name</label>
                <div>
                	<select name="fabricId" onChange="return showTypeOnNameChange(this.value);">
                		<option value="">Select Fabric Name</option>
                		<?= $fabricObj->getFabricType(isset($_POST['fabricId'])?$_POST['fabricId']:$_GET['fId'])?>
                	</select> 
                	<?= $arr_error[fabricId]?>		                  	                 	
			    </div>
            </section>
            
             <section>
                  <label for="fQuality">Fabric Quality</label>
                  <div>
						<input type="text" name="fQuality" id="m__fQuality"  value="<?= $_POST['fQuality'] ?>" style="width:245px;" />
                  	   <?= $arr_error[fQuality]?>		                  	                  	  
                  </div>
             </section>
             
             <section>
                  <label>Percent </label>
                  <div>
                  	   <input type="text" name="percent" id="m__percent"  value="<?= $_POST['percent'] ?>" style="width:130px;" /><span style="font-size:13px"> %</span>	                  
                  	   <?= $arr_error[percent]?>		                  	                  	  
                  </div>
             </section>
             
            <section>
                  <label>Pallet Name</label>
                  <div>
                  	   <select name="pallet">
                  	   	<option value="">Select Pallet</option>
                  	   		<?= $fabricObj->getPallet();?>
                  	   </select>	                  
                  	   <?= $arr_error[pallet]?>		                  	                  	  
                  </div>
             </section>             
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form> </fieldset>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMailType.php","edit_record");
$mailSettingObj = new MailSetting();
$mailTemplateId = base64_decode($_GET['mailId']);
$mailTemplateId = $mailTemplateId?$mailTemplateId:0;
$id = $_GET['id'];
$row = $mailSettingObj->recordToUpdate($id,$mailTemplateId);
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd("mailsubject", $_POST["mailsubject"], "req", "Please enter  Mail Subject.");
	$obj->fnAdd('email',$_POST['email'], 'req', 'Please enter Email Address.');
	$obj->fnAdd("email", $_POST["email"], "email", "Please enter valid Email Address.");
	$obj->fnAdd("message", strip_tags($_POST["message"]), "req", "Please enter Mail Containt.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[mailsubject]=$obj->fnGetErr($arr_error[mailsubject]);
	$arr_error[email]=$obj->fnGetErr($arr_error[email]);
	$arr_error[message]=$obj->fnGetErr($arr_error[message]);
	if( empty($arr_error[mailsubject]) && empty($arr_error[email]) && empty($arr_error[message]) && isset($_POST['submit'])){
		//$_POST = postwithoutspace($_POST);
		$mailSettingObj->editMailTemplate($_POST);
	}
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageBrand.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Email Template</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			<fieldset>
				
				<label>Edit Mail Template</label>
				<?=$_SESSION['SESS_MSG']?>
				<span class="alert-red alert-icon">Must keep containt inside [] as in other templates of same type.</span>
				<!-- Start : Language Name ---- --->
				<section>
					  <label for="LanguageName">Language Name </label>
					  <div><?=$mailSettingObj->fetchValue(TBL_LANGUAGE,"languageName","id='$row->langId'");?></div>	
				</section>
				
				<!-- Start : Subject ---- --->
				<section>
					  <label for="Subject">Subject</label>
					  <div> 
					  	<textarea name="mailsubject" cols="3" class="wel" id="m__Mail_Subject"><?=$mailsubject=stripslashes($_POST[mailsubject])?stripslashes($_POST[mailsubject]):stripslashes($row->mailSubject);?></textarea>
					    <?=$arr_error[mailsubject]?>
					  </div>	
				</section>
	                              
				<!-- Start :To/From MailId ---- --->
				<section>
					  <label for="ToFromMailId">To/From MailId</label>
					  <div>
					  	<input type="text" name="email" id="m__email" size="3" class="wel" value="<?=$mailsubject = stripslashes($_POST[email])?stripslashes($_POST[email]):stripslashes($row->emailid);?>" />
						<?=$arr_error[email]?>
					  </div>	
				</section>
				
				<!-- Start : Mail Content ---- --->
				<section>
					  <label for="MailContent">Mail Content</label>
					  <div>
					  	<?php
						$oFCKeditor = new FCKeditor('message') ;

						$oFCKeditor->BasePath = 'fckeditor/' ;

						$oFCKeditor->Height	= 400;

						$oFCKeditor->Width	= 740;

						$oFCKeditor->Value = $_POST['message']?stripslashes($_POST['message']):stripslashes($row->mailContaint);

						$oFCKeditor->Create();
						
						
						?>					  
					  </div>
					  <?=$arr_error[message]?>	
				</section>
				<input type="hidden" name="mailTypeId" value="<?=$mailTemplateId?>" />
				 <input type="hidden" name="id" value="<?=$id?>" />
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack()"/>
				</div>
             </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
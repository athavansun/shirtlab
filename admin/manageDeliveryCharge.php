<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */

$sysObj = new DeliveryTime();
$result = $sysObj->executeQry("Select * from ".TBL_DELIVERY_TIME." as dt inner join ".TBL_DELIVERY_TIME_CHARGES." as dtc on dt.id = dtc.d_id order by dt.days asc");

if (isset($_POST['submit'])) {  
   $sysObj->editDeliveryCharges($_POST);
   $result = $sysObj->executeQry("Select * from ".TBL_DELIVERY_TIME." as dt inner join ".TBL_DELIVERY_TIME_CHARGES." as dtc on dt.id = dtc.d_id order by dt.days asc");
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
<!--      <h1>Delivery Charge Settings<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addDeliveryTime.php">Add Delivery Time</a><? } ?></h1> -->
          <h1>Manage Delivery Charge</h1>
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);"
               enctype="multipart/form-data">
                  <? echo $_SESSION['SESS_MSG'];                
unset($_SESSION['SESS_MSG']); ?>

            <fieldset>
                <label>Delivery Charge</label>
                <?php
                    while($line = $sysObj->getResultObject($result)){ ?>
                        <section>                    
                            <div style="width:12%; float:left;">                                                                                   
                                    <span style="font-size: 13px; margin-left: 25px;"><?= $line->days;?> Days</span>
                            </div>   
                            <div style="width:7%; float:left;">   
                            <?php
                                if($line->sign == 0){
                                    $status = "minus";
                                } else if($line->sign == 1) {
                                    $status = "x";
                                } else {
                                    $status = "plus";
                                }
                            ?>
                                    <span style="font-size: 13px; margin-left:11px;"><?= $status;?></span>
                            </div>                              
                            <div style="width:70%; float:left;"> <?php if($line->sign != 1) { ?>                                                      
                                <input type="text" name="charge_<?= $line->id;?>" id="charge_<?= $line->id;?>" value="<?= $line->charge; ?>" class="wel" style="width:75px;" />     <?php }else { echo '<span style="font-size: 13px; margin-left:75px;">0</span>';} ?>
                                <span style="font-size: 13px;"> % on the Standard Value-Point/Single-Rates of plain product</span>
                            </div>                                                                  
                        </section>                
                
                <?  }                
                ?>  
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>

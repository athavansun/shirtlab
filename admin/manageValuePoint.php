<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
require_once('includes/classes/valuePoint.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */

$sysObj = new ValuePoint();

$result = $sysObj->executeQry("Select * from ".TBL_VALUE_POINTS." as vp inner join ".TBL_CURRENCY_DETAIL." as cd on vp.cur_id= cd.id");

if (isset($_POST['submit'])) {      
    $sysObj->editValuePoints($_POST);
    $result = $sysObj->executeQry("Select * from ".TBL_VALUE_POINTS." as vp inner join ".TBL_CURRENCY_DETAIL." as cd on vp.cur_id= cd.id");
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
<!--      <h1>Delivery Charge Settings<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addDeliveryTime.php">Add Delivery Time</a><? } ?></h1> -->
          <h1>Value-Point Currency Panel</h1>
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);"
               enctype="multipart/form-data">
                  <? echo $_SESSION['SESS_MSG'];                
unset($_SESSION['SESS_MSG']); ?>

            <fieldset>
                <label>Value Point</label>
                <?php
                    while($line = $sysObj->getResultObject($result)){ ?>                            
                        <section>                    
                            <div style="width:30%; float:left;">                                                                                   
                                    <span style="font-size: medium; margin-left: 5px;"><?= $line->days;?>Value-Point to <?= $line->currencyName; ?></span>                                            
                            </div>   
                                    
                            <div style="width:10%; float:left;">
                                <span style="font-size: medium; margin-left: 5px;">factor</span>
                            </div>
                            
                            <div style="width:50%; float:left;">
                                <span style="font-size: medium; margin-left: 5px;">1 Value-Point = </span>
                                <input type="text" name="value_<?= $line->id;?>" id="value_<?= $line->id;?>" value="<?= $line->value; ?>" class="wel" style="width:200px;" />     
                                <span style="font-size: medium; margin-left: 5px;"><?= $line->currencyCode; ?></span>
                            </div>                            
                        </section>                
                
                <?  }                
                ?>
                
                
                
              <!--  
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_two" id="day_2" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 2")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                        <span style="font-size: medium; margin-left: 45px;">only Swiss // plus Germany with valid Custom number</span>
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_two" id="pcs_two" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 2")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
                
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_three" id="day_3" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 3")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_three" id="pcs_three" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 3")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>              
              
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_four" id="day_4" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 4")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_four" id="pcs_four" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 4")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
              
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_five" id="day_5" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 5")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_five" id="pcs_five" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 5")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
              
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_six" id="day_6" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 6")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_six" id="pcs_six" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 6")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>              
                
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_seven" id="day_7" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 7")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_seven" id="pcs_seven" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 7")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
              
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_eight" id="day_8" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 8")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_eight" id="pcs_eight" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 8")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
                
               <section>
                    <div style="width:20%; float:left;">                   
                            <input type="text" name="day_nine" id="day_9" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "days", "1 and id = 9")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">Days</span>
                    </div>                                       
                    <div style="float:left; width:50%; margin-top:3px;">
                
                    </div>
                    <div style="width:20%; float:left;">                   
                            <span style="font-size: medium;">up to</span>
                            <input type="text" name="pcs_nine" id="pcs_nine" value="<?= stripslashes($sysObj->fetchValue(TBL_DELIVERY_TIME, "pcs", "1 and id = 9")) ?>" class="wel" style="width:75px;"/>
                            <span style="font-size: medium;">pcs</span>
                    </div>                                        
               </section>
              
              -->
                             
                
                
                   
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>

<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabricPrice.php", "");
/* ---Basic for Each Page Starts---- */

$fabricObj = new Fabric();

$generalFunctionObj = new GeneralFunctions();

$result = $fabricObj->getFabricPriceResult(base64_decode($_GET['id']));

?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions   ?>


</head>
<body>
   <section id="content-detail">
      <form>
         <fieldset>
            <label>View Detail</label>
            <!-- left position -->
            <div><?= $_SESSION['SESS_MSG'] ?></div>
            <section>
               <label for="FabricName">Fabric Name</label>
               <div>
                  <?
                  $genObj = new GeneralFunctions();
                  echo utf8_encode($genObj->getLanguageViewTextBox('fabricName', TBL_FABRICDESC, base64_decode($_GET['id']), "fabricId")); 
                  ?>
               </div>
            </section>
            <section>
               <label for="FabricType">Fabric Type</label>
               <div>
                  <?= $fabricObj->fetchValue(TBL_FABRIC, "type", "id = ".$result->fId)?>
               </div>
            </section>	
            <section>
               <label for="PalletName">Pallet Name</label>
               <div>
                  <?= $fabricObj->fetchValue(TBL_PALLETDESC, "palletName", "palletId = ".$result->palletId)?>
               </div>
            </section>	
            <section>
               <label for="percent">Percent</label>
               <div>
                  <?= $result->percent ?>
               </div>
            </section>	
            
<!-- 		<section>
               <label for="Open">Open In :</label>
               <div><? if($result->openIn == 0) { ?> Same Window <?  } else { ?>  Another Window <? } ?></div>
            </section>
            <section>		 -->
            
            </section>
         </fieldset>
      </form>
   </section>
</body>
</html>
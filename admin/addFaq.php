<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFaq.php","add_record");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$faqObj = new Faq();

if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $faqObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $faqObj->getTotalRow($rst);	
	
	if($num){
		$langIdArr = array();		
		while($line = $faqObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('question_'.$value,$_POST['question_'.$value], 'req', 'Please enter Question.');			
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('answer_'.$value,$_POST['answer_'.$value], 'req', 'Please enter Answer.');			
		}
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['question_'.$value]=$obj->fnGetErr($arr_error['question_'.$value]);
			if($arr_error['question_'.$value]) 
				$errorArr = 1;
		}
		foreach($langIdArr as $key=>$value) {
			$arr_error['answer_'.$value]=$obj->fnGetErr($arr_error['answer_'.$value]);
			if($arr_error['answer_'.$value]) 
				$errorArr = 1;
		}
		
		/*foreach($langIdArr as $key=>$value) {
			if($faqObj->isViewExist($_POST['viewName_'.$value],$value))
			{ 
				$arr_error['viewName_'.$value] = "Name already exist. ";
				if($arr_error['viewName_'.$value]) 
					$errorArr = 1;				
			}
		}	*/
		
		if($errorArr == 0 && isset($_POST['submit'])){
			$_POST = postwithoutspace($_POST);
			$faqObj->addRecord($_POST);
		}
	}
}
$maxSequence = $faqObj->findMaxSequence();
?>

<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageFaq.php';
}
</script>
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
<section id="content">
<h1>Faq</h1><fieldset>
		<form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<fieldset>  
            <label>Add Faq</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                  <label>Question</label>
                  <div>
					<?=$genObj->getLangTextareaEngValidated('question','m__Question',$arr_error);?>
			</div>
            </section>
            <section>
                  <label>Answer</label>
                  <div>
					<?=$genObj->getLangTextareaEngValidated('answer','m__Answre',$arr_error);?>
			</div>
            </section>
         </fieldset>
         <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
	</form>
</fieldset>
</section>
<? unset($_SESSION['SESS_MSG']); ?>
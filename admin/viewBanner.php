<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageBanner.php","");
/*---Basic for Each Page Starts----*/

$bannerObj = new Banner();
$generalFunctionObj = new GeneralFunctions();

$result = $bannerObj->getResult(base64_decode($_GET['id']));
?>
<?=headcontent();?>
<!-- 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Welcome To <? //=SITENAME?> administrative panel</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<SCRIPT src="js/ajax.js" language="javascript" type="text/javascript"></SCRIPT>
<script language="javascript" src="js/requiredValidation.js"></script>
<script language="javascript" src="js/validation.js"></script>
<LINK rel="stylesheet" href="js/menu/template.css" type="text/css">
<LINK href="js/menu/ja.scriptdlmenu.css" rel="stylesheet" type="text/css">
<SCRIPT src="js/menu/mootools.js" language="javascript" type="text/javascript"></SCRIPT>
<SCRIPT src="js/menu/ja.scriptdlmenu.js" language="javascript" type="text/javascript"></SCRIPT>

<script type="text/javascript" src="datepicker/jquery-1.3.2.js"></script>
<script type="text/javascript" src="datepicker/ui.core.js"></script>
<script type="text/javascript" src="datepicker/ui.datepicker.js"></script>
<link type="text/css" href="datepicker/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="datepicker/demos.css" rel="stylesheet" />
<link type="text/css" href="datepicker/themes/base/ui.all.css" rel="stylesheet"/>  -->
<script type="text/javascript">

$(function() {
	$('#startDate').datepicker({
		showButtonPanel: true
	});
});

$(function() {
	$('#endDate').datepicker({
		showButtonPanel: true
	});
});
</script>

</head>
<body>
<section id="content-detail">
  <form>
 	<fieldset>
    <label>Banner Details</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>
		
		 <section>
       	  <label for="CountryName">Banner Title:</label>
             <div><?=stripslashes($result->bannerTitle)?></div>
        </section>
        <section>
       	  <label for="CountryName">Banner URL:</label>
             <div><?=stripslashes($result->bannerUrl)?></div>
        </section>
        <section>
       	  <label for="CountryName">Banner Group</label>
             <div><?=stripslashes($result->bannerGroup)?></div>
        </section>
        <section>
       	  <label for="CountryName">Image</label>
             <div><img width="100" src="<?=__BANNERPATH__.$result->bannerImage?>"></div>
        </section>
         <section>
       	  <label for="CountryName">Open In</label>
             <div><? if($result->openIn == 0) { ?> Same Window <?  } else { ?>  Another Window <? } ?></div>
        </section>
</fieldset>
</form>
</section>
</body>
</html>
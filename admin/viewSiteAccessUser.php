<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageSiteAccessUser.php");
/* ---Basic for Each Page Ends---- */
//$userObj = new User();
$siteuserObj = new SiteAccessUser();
$uid = $_GET['uid'] ? $_GET['uid'] : 0;
$uid = base64_decode($uid);
$result = $siteuserObj->getUserById($uid);
$result = displayWithStripslashes($result);

//$rst1 = $siteuserObj->selectQry(TBL_LANGUAGE, "id='" . $result[language] . "'", "", "");
//$rst1 = mysql_fetch_object($rst1);
?>
<?= headcontent() // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
</head>
<body>
    <section id="content-detail">
        <form>
            <fieldset> 				
                <label>View Site Access User Detail</label>
                <!-- Start : User Name ------->
                <section>
                    <label for="userName">User Name</label>
                    <div><?= $result[userName] ? $result[userName] : "--"; ?></div>					 
                </section> 
                <!-- Start : Password ------->
                <section>
                    <label for="password">Password</label>
                    <div><?= $result[password] ? $result[password] : "--"; ?></div>					 
                </section> 
                <!-- Start : Status  ------->
                <section>
                    <label for="Status">Status</label>
                    <div><?= $result[status] ? 'Active' : 'Inactive' ?></div>
                </section> 
            </fieldset>       
        </form>  
    </section>        
</body>
</html>
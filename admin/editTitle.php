<?php 
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageTitle.php", "");
/* ---Basic for Each Page Starts---- */
$titleObj = new Title();
$generalFunctionObj = new GeneralFunctions();

if (isset($_POST['submit'])) {
   	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $titleObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
	$num = $titleObj->getTotalRow($rst);
   	if ($num > 0) {
        $langIdArr = array();
      	while ($line = $titleObj->getResultObject($rst)) {
        	 array_push($langIdArr, $line->id);
      	}
      	foreach ($langIdArr as $key => $value) {      	
            $obj->fnAdd('titleName_' . $value, $_POST['titleName_' . $value], 'req', "Please enter title");
      	}
	    $arr_error = $obj->fnValidate();
	    $str_validate = (count($arr_error)) ? 0 : 1;		
	    foreach ($langIdArr as $key => $value) {
        	$arr_error['titleName_' . $value] = $obj->fnGetErr($arr_error['titleName_' . $value]);
   	  	}
   	  	foreach($langIdArr as $key=>$value) {   				
			if($titleObj->checkTitleExists($_POST['titleName_'.$value],$value, base64_decode($_GET['id']))) {
				$arr_error['titleName_'.$value] ='<span class="alert-red alert-icon">Title already exist</span>';
				if($arr_error['titleName_'.$value]){ 
					$errorArr = 1;							
				}
			}
   	  	}
   	  	if ($str_validate && $errorArr == 0) {
	    	$titleObj->editRecord($_POST);
		}   	  	
   	}
}
?>
<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
   function hrefBack1(){
      window.location='manageTitle.php';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Title</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>">
            <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
            <fieldset>
               <label>Edit Title</label>
<?= $_SESSION['SESS_MSG'] ?>
               <section>
                  <label>Title</label>
                  <div>
                 <?= $generalFunctionObj->getLanguageEditTextBox('titleName', 'm__titleName', TBL_TITLEDESC, base64_decode($_GET['id']), "id", $arr_error) ?>
                  </div>
               </section>
            </fieldset>

            <fieldset>
               <section>  
                  <label>&nbsp;</label>
                  <div style=" width:78%;">
                     <input type="submit" name="submit"   value="Submit" />
                     <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
                  </div>
               </section>
            </fieldset>
         </form>
      </fieldset>
   </section>
<? unset($_SESSION['SESS_MSG']); ?>
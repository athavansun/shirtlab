<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');

$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission('manageColor.php','');
$genObj = new GeneralFunctions();
$colorbj = new Color();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
if(isset($_POST['savePallet']))
{
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $colorbj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND isDefault = '1' order by languageName asc","","");		
	$num = $colorbj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $colorbj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
		foreach($langIdArr as $key=>$value){
			$obj->fnAdd('palletName_'.$value,$_POST['palletName_'.$value], 'req','Please enter pallet name');			
		}
				
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['palletName_'.$value]=$obj->fnGetErr($arr_error['palletName_'.$value]);
		}
		
		foreach($langIdArr as $key=>$value) {
			if($colorbj->isPalletNameExist($_POST['palletName_'.$value],$value,$_POST['id'])) {
				$arr_error['palletName_'.$value] ='<span class="alert-red alert-icon">Pallet Name already exist!</span>';				
					$str_validate = 0;				
			}
		}
		
		if($str_validate)
		{
			$colorbj->updateColorPallet($_POST);
		}
	}	
}

if(isset($_POST['loadColor']))
{	
    require_once('validation_class.php');
    $obj = new validationclass();
    $errorArr = 0;	
//    $obj->fnAdd('code',$_POST['code'], 'req','Please enter code');
    if($_FILES['colorImage']['name'] == '' && $_POST['colorCode'] == '') {
        if($_FILES['colorImage']['name'] == "") {
            $obj->fnAdd('colorImage',$_POST['colorImage'], 'req','Please upload color image');
        } else {
            $obj->fnAdd('colorCode',$_POST['colorCode'], 'req','Please enter Color code');
        }
    }

    $obj->fnAdd('colorCode',$_POST['colorCode'], 'req','Please enter Color code');
    
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;
    
//    $arr_error['colorCode']=$obj->fnGetErr($arr_error['colorCode']);    
//    $arr_error['code']=$obj->fnGetErr($arr_error['code']);
    $arr_error['colorCode']=$obj->fnGetErr($arr_error['colorCode']);
    $filename = stripslashes($_FILES['colorImage']['name']);
    if($filename != '')
    {
        $extension = findexts($filename);
        $extension = strtolower($extension);	 	 
        if(!$colorbj->checkExtensions($extension) && $filename) {
            $arr_error[colorImage] = "<span class='alert-red alert-icon'>Upload Only ".$colorbj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.</span>";
            if($arr_error[colorImage]) 
                $errorArr = 1;	
        }
    }

    //~ else
    //~ {
            //~ $arr_error['colorImage'] ='<span class="alert-red alert-icon">Select Image</span>';
            //~ $errorArr = 1;
    //~ }

    if($str_validate && $errorArr == 0)
    {		
        //echo 'hello';
        $colorbj->updatePalletImage($_POST,$_FILES);
    }
    if($_POST['colorCode'] == '') {
        echo $arr_error['colorCode'];
    }
    
//    echo $arr_error['code'];
//    if($_FILES['colorImage']['name'] == '' && $_POST['colorCode'] == '') {
//        echo $arr_error['colorCode'];
//    }
//    
//    echo $arr_error[colorImage];
}

if(isset($_POST['Input']))
{	
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $colorbj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND isDefault = '1' order by languageName asc","","");		
	$num = $colorbj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $colorbj->getResultObject($rst)) {	
                    array_push($langIdArr, $line->id);
		}
		
		foreach($langIdArr as $key=>$value){
			$obj->fnAdd('palletName_'.$value,$_POST['palletName_'.$value], 'req','Please enter pallet name');			
		}

		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['palletName_'.$value]=$obj->fnGetErr($arr_error['palletName_'.$value]);
		}
		
		foreach($langIdArr as $key=>$value) {
                    if($colorbj->isPalletNameExist($_POST['palletName_'.$value],$value, '')) {
                        $arr_error['palletName_'.$value] ='<span class="alert-red alert-icon">Pallet Name already exist!</span>';
                        $str_validate = 0;				
                    }
		}
		
		if($str_validate)
		{
                    $colorbj->addColorPallet($_POST);
		}
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,Top Pageoptions ?>

<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
    
    $(document).ready(function() {
       $("#deleteColor").click(function() {
           var palletId = $("#palletId").val();
           var id = $("#palletBoxId").val();
           
           if(id !== '') {                
                var src = 'pass.php';
                $.ajax({
                   url:src,
                   data: 'action=colorPallet&type=deleteColor&id='+id+"&pId="+palletId,
                   cache:false,
                   type:'GET',
                   success: function(data, textStatus, XMLHttpRequest) {
                       if(data == true) {
                           window.location.href="addColor.php?palletId="+palletId;
                       } 
                   },
                   error:function(XMLHttpRequest, textStatus, errorThrown) {
                       alert(textStatus);
                       alert(errorThrown);
                   }
                });
            }
       })
    });

</script>
<!--				Ligh Box Ends 	-->

<script type="text/javascript" src="colorpicker/color_functions.js"></script>
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" type="text/css" />
<script src="js/file/jquery.filestyle.js"></script>
<style type="text/css">
	.imgClass {border: 2px solid;float: left; margin-left:14px;height: 45px;width: 50px;}
</style>
</head>

<body>
<? include('includes/header.php'); ?>
<div id="fadeContent" style="background-color: #000000; display:none; opacity: 0.6; z-index:500; width:1082px; height:100%; position:absolute;"></div>
    <section id="content">
    
    <h1>Add new Color Pallet</h1>
    <fieldset>
        <form name="ecartFrm" method="post" action="">
            </div>
                            
            </div>
            <div class=" ">
            <? echo $_SESSION['SESS_MSG'];
            unset($_SESSION['SESS_MSG']); ?></div>
                                    
            <section>
                <label>Color Pallet</label>
                <div>
                    <table align="center" border="5" style="width:550px;">
                        <?php
                        if ($_GET['palletId'] != '') {
                            ?>
                            <tr>
                            <input type="hidden" name="id" value="<?= $_GET['palletId']; ?>" />
                            <td>Name of pallet</td>
                            <td style="text-align:left;" colspan="5">
                                <?= $genObj->getLanguageEditTextBox('palletName', 'm__palletName', TBL_PALLETDESC, $_GET['palletId'], "palletId", $arr_error); //1->type,2->name,3->id,4->tablename,5->tableid?>
                                                                                
                            </td>
                            </tr>
                            <?= $colorbj->getColorBox($_GET['palletId']); ?>
                            <tr>
                                <td colspan="6" width="100%" style="text-align:right;">
                                    <input type="button" value="Back" onclick="javascript:hrefBack1();">
                                    <input class="" type="submit" value=" Save " name="savePallet">
                                </td>
                            </tr>
                            <?php
                        } else {
                            echo $genObj->getLanguageTextBox('palletName', 'm__palletName', $arr_error);
                            ?>
                            <tr>
                                <td colspan="6" width="100%" style="text-align:right;">
                                    <input type="button" value="Back" onclick="javascript:hrefBack1();">
                                    <input class="" type="submit" value="Create" name="Input">
                                </td>
                            </tr>
    <?php
}
?>
                                                                    
                    </table>
                </div>	
            </section>
        </form>
    </fieldset>
            
    <section id="addnewPallet" style="z-index:1000; position:absolute; display:none; top:200px;">
        <form name="colorForm" method="post" enctype="multipart/form-data" action="">            
            <table align="center" style="width:550px;">
                <tr>
                    <td>Code</td>
                    <td style="text-align:left;">
                        <input type="text" id="code" name="code" style="width:200px;" ><br>
<?= $arr_error['code']; ?>
                    </td>
                </tr>
                <tr>
                    <td>Color Code (FFFFFF)</td>
                    <td style="text-align:left;">
                        <input type="text" name="colorCode" id="colorCode" style="width:200px;" maxlength="7" value="<?= $_POST[colorCode] ?>" />&nbsp;<img alt="col_code" src="colorpicker/color.png" onClick="showColorPicker(this,document.colorForm.colorCode)" border="0" style="cursor:pointer;">
                        <br>
<?= $arr_error['colorCode']; ?>
                    </td>
                </tr>
                <tr>
                    <td>Color Image</td>
                    <td style="text-align:left;">
<!--                        <image src="images/prise_bg.png" />
                        <span id="palletImage"></span>
                        <br><br>-->
                        
                        <input type="file" class="file browse-button" name="colorImage" value="" />
<?= $arr_error['colorImage']; ?>
                    </td>
                </tr>							
                <tr>
                    <td><input type="hidden" name="palletBoxId" value="" id="palletBoxId" />
                        <input type="hidden" name="palletId" value="<?= $_GET['palletId']; ?>" id="palletId" /></td>
                    <td><input type="button" value="Close" onclick="return closeLoadImage();" />
                        <input type="submit" name="loadColor" value="Load Color" />
                        <input type="button" id="deleteColor" onclick="deleteColor()" value="Delete Color" /></td>
                </tr>
            </table>
        </form>
    </section>
            
</section>
<?php include_once('includes/footer.php'); ?>
<script type="text/javascript">
	function showLoadImage(id)
	{
		//alert(id);
		$("#palletBoxId").val(parseInt(id));
		var ImgName = $("#oldImage"+id).val();
		var pCode = $("#pColorCode"+id).val();
		$("#code").val($("#pCode"+id).val());
		$("#colorCode").val(pCode);
		if(ImgName)		
			$("#palletImage").html('<image src="<?php echo SITE_URL.__PALLETIMGTHUMB__;?>'+ImgName+'" />');
		else
			$("#palletImage").html('');
			
		$("#fadeContent").css('display','block');
		$("#addnewPallet").css('display','block');
	}
	function closeLoadImage()
	{
		$("#fadeContent").css('display','none');
		$("#addnewPallet").css('display','none');
	}
	function hrefBack1()
	{
		window.location='manageColor.php';
	}
</script>

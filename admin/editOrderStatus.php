<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manage-order-status.php", "edit_record");
/* ---Basic for Each Page Ends---- */

$orderObj = new Orderstatus();
if (isset($_POST['submit'])) {
   $_POST = postwithoutspace($_POST);
   $orderObj->editRecord($_POST);
}
$result = $orderObj->getResult(base64_decode($_GET['id']));
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<!--				Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)		-->
<script type="text/javascript">
   function hrefBack1(){
      window.location='manage-order-status.php';
   }
</script>

</head>
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Edit Order Status</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <label>Order Status</label>
          <input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">
   				<input type="hidden" name="page" value="<?=$_GET['page']?>">
            <div id="mainDiv">
               <div><?= $_SESSION['SESS_MSG'] ?></div>
               <fieldset>
                  <section>
                     <label>Status type</label>
                     <div><?=$orderObj->fetchValue(TBL_ORDERSTATUS,"statusType","statusId='".base64_decode($_GET['id'])."'")?>
                        <?= $arr_error['colorCode'] ?>
                     </div>
                  </section>
                  <section>
                     <label>Status Name <span class="spancolor">*</span> </label>
                     <div>
                        <?	$genObj = new GeneralFunctions();
						echo $genObj->getLanguageEditTextBox('orderStatus','m__Order_Status_Name',TBL_ORDERSTATUSDESC,base64_decode($_GET['id']),"orderStatusId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
					?>
                     </div>
                  </section>

               </fieldset>
               <fieldset>
                  <section>
                     <label>&nbsp;</label>
                     <div style=" width:78%;">
                        <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                        <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
                     </div>
                  </section>
               </fieldset>
            </div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
   <? unset($_SESSION['SESS_MSG']); ?>
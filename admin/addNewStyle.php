<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
//$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
//$genObj = new GeneralFunctions();
$proObj = new Products();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
$pId = $_GET['pId'];
if(isset($_POST['addStyle']))
{
    $proObj->addNewProductStyle($_POST,$_FILES);
}
?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<section id="content-detail">
 <fieldset>
		<form id="formProdStyle" name="uploadform_style" method="post" enctype="multipart/form-data" action="" >
		<fieldset>
			<section>
				<label style="width:100%;font-weight:bold;" for="name">
					<b>Add New Product Style</b>
				</label>
			</section>
			<section>
				<label for="name">Product Code</label>
				<div>
					<input type="text" name="productCode" id="m__productCode" class="validate[required] text-input" />							
				</div>
			</section>
			<section>
				<label for="name">Product style Image <img width="60" src="../files/helpImage.png" /></label>
				<div>
					<input type="file" name="proStyleImage" class="file browse-button validate[required] text-input" />
					<?=$arr_error['proStyleImage'];?>
				</div>
			</section>
			<section>
				<label>Is Basic Style?</label>
				<div>
					Yes <input type="radio" name="isBasic" value="1" />
					No <input type="radio" name="isBasic" checked="checked" value="0" />
				</div>
			</section>
                        <section>
                            <label>Value Point</label>
                            <div>
                                <select name="sign">
                                    <option value="2"> + </option>
                                    <option value="0"> - </option>
                                </select>
                                &nbsp;&nbsp;% <input type="text" name="percent" value="0" style="width: 80px;" class="validate[required] text-input" />
                                <?=$arr_error['percent']?>
                            </div>
                        </section>
			<?=$proObj->addProductStyle($pId);?>
		</fieldset>
		<fieldset>
			<section>
				<div>
					<input type="hidden" name="pId" value="<?=$pId?>" />
					<input type="submit" name="addStyle" style="cursor:pointer;" value="Submit" />				
				</div>
			</section>
		</fieldset>
		</form>
	</fieldset>
</section>
<script type="text/javascript">
	jQuery(document).ready( function() {
		// binds form submission and fields to the validation engine
		jQuery("#formProdStyle").validationEngine();
	});
function addMorePart(tblId,fileName,partTitle, viewId)
{
	var html = "";
	var v = parseInt($("#"+tblId+"Hid").val());
	//alert(v);// = parseInt(v);
	html +='<tr id="'+tblId+'tr'+v+'">\
                                <td style="text-align:left;">\
					<input type="text" name="sequence'+viewId+'[]" value="0" class="text-input" />\
				</td>\
				<td style="text-align:left;">\
					<select name="visible'+viewId+'[]"><option value="1">Yes</option><option value="0">No</option></select>\
				</td>\<td style="text-align:left;">\
					<select name="colorable'+viewId+'[]"><option value="1">Yes</option><option value="0">No</option></select>\
				</td>\
				<td style="text-align:left;">\
					<select name="'+partTitle+'[]"><?=$proObj->getProductPartList();?></select>\
				</td>\
				<td style="text-align:left;">\
					<input type="file" name="'+fileName+'[]" class="validate[required] text-input" />\
				</td>\
				<td><input type="button" onclick="return removePartElement(\''+tblId+'tr'+v+'\')" name="removePart" value="Remove" /></td>\
			</tr>';
	
	$("#"+tblId).append(html);
	var vi = v + 1;
	$("#"+tblId+"Hid").val(vi);
}
function removePartElement(element)
{
	//alert(element);
	$("#"+element).remove();
}
</script>
<? include_once('includes/footer.php');?>

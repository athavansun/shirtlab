<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageProducts.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$catObj = new Products();
$result = $catObj->getProductInfo(base64_decode($_GET['id']));
//$colorcode = $catObj->getProductColor(base64_decode($_GET['id']));
?>
<?=headcontent()?>
</head>
<body>
<section> <!--id="content">          -->
	<!--<h1>Manage Product</h1>	-->
<!-- 	<fieldset> -->
	<form name="frmUser">
		<label>Product Details</label>
			<div id="div_duplicateUser"></div>
			<div id="div_duplicateEmail"></div>
			<div id="divMessage"></div>
			<? echo $_SESSION['SESS_MSG'] ; unset($_SESSION['SESS_MSG']); ?>
		<div id="mainDiv">
			<fieldset>
				<section>
					<label for="categoryId">Style</label>
					<div>
						<?
							echo $catObj->fetchValue(TBL_CATEGORY_DESCRIPTION,'categoryName',"catId='".$result->catId."' and langId='".$_SESSION['DEFAULTLANGUAGE']."'");
						?>
					</div>
				</section>
				<section>
					<label for="productName">Product Name</label>
					<div>
						<img src="<?=SITE_URL.__PRODUCTTHUMB__.$result->proImage;?>" />
						<?
							echo $genObj->getLanguageViewTextBox('productName',TBL_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"pId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid
						?>
					</div>
				</section>
				<section>
					<label for="price">Price</label>
					<div>
						<?
							echo $result->productPrice;
							//echo $genObj->displayPrice($result->productPrice);
						?>
					</div>
				</section>
				
				<section>
					<label for="prodDesc">Product Description</label>
					<div>
						<?
							echo $genObj->getLanguageViewTextBox('productDesc',TBL_PRODUCT_DESCRIPTION,base64_decode($_GET['id']),"pId");
						?>
					</div>
				</section>
				<section>
					<label for="color">Variations</label>
					<div>
						<table>
							<tr>
								<th>Sl. No</th>
								<th>Style Code</th>
								<th>Style Image</th>
								<th>Status</th>
							</tr>
						<?
							echo $catObj->getAllProductStyleForView(base64_decode($_GET['id']));
						?>
						</table>
					</div>
				</section>
				<section>
					<label for="size">Product Size</label>
					<div>
						<?
							//echo $catObj->getProdSize(base64_decode($_GET['id']));
						?>
						<?=$catObj->getProductSize($catObj->getEditProdSize(base64_decode($_GET['id']))); ?>
					</div>
				</section>
			</fieldset>
		</div>
	</form>
<!-- 	</fieldset> -->
</section>
<?php include_once('includes/footer.php');?>

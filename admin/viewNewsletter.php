<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageNewsletter.php","");
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();
$id = base64_decode($_GET['id']);
$row = $newsletterObj->newsLetterToEdit($id);


?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>View Detail</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>
		  <!-- Language Name ----------->
			<section>
			  <label for="Language">Language Name:</label>
				 <div><?=$newsletterObj->fetchValue(TBL_LANGUAGE,"languageName","1 and id = '".$row['langId']."'")?></div>
			</section>
			
		  <!-- Subjec ----------->
			<section>
			  <label for="Subjec">Subject:</label>
				 <div><?=nl2br(stripslashes($row['newsletterSubject']))?></div>
			</section>
		  
		  <!-- From Mail ID ----------->
			<section>
			  <label for="FromMailID">From MailId:</label>
				 <div><?=stripslashes($row['senderEmailId'])?></div>
			</section>
		  
		  <!-- Newsletter Content ----------->
			<section>
			  <label for="NewsletterContent">Newsletter Content:</label>
                          <div><?=  stripslashes($row[newsletterContaint])?></div>
			</section>
		
		
        
     </fieldset>       
     </form>  
    </section>        
</body>
</html>
<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$genObj = new GeneralFunctions();
$menuObj->checkPermission("manage-order-status.php","add_record");
/*---Basic for Each Page Ends----*/

$paymentObj = new Paymentstatus();
if(isset($_POST['submit'])) {
    $_POST = postwithoutspace($_POST);
    $paymentObj->addOrderStatusType($_POST);
}

?>
<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<? // =getjsconstant(); ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
   function hrefBack1(){
      window.location='manage-payment-status.php';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Payment Status</h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
         <input type="hidden" name="frontOrAdmin" id="frontOrAdmin" value="<?= $_REQUEST['frontOrAdmin'] ?>" >
         <fieldset>
            <label>Add Order Status</label>
            <?= $_SESSION['SESS_MSG'] ?>
            <section>
               <label>Order Status Type  </label>
               <div>
                <!--<input type="text" name="statusType" id="m__Order_Status_Type" class="wel" />-->
                   <?=$genObj->getLanguageTextBoxEngValidation('statusType','statusType',$arr_error);?>
               </div>
            </section>
         </fieldset>
         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">
                  <input type="submit" name="submit"   value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
      </form>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>
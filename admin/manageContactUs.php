<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$contactUsObj = new ContactUs();
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js,Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
    <h1 >Manage Contact Us</h1>
     <fieldset>
		<form name="ecartFrm" method="post" action="pass.php?action=contactUs&type=deleteall">
    		<fieldset>
	     		<div id="search-main-div" class="top-filter bg02">
					<div id="check" class="seclet"><a href="javascript:void(NULL)" class="buttontext"
		onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a> </div>
			 		<div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
		         	<ul>
						<li class="action">Action:</li>
						<li>
							<select name="action">
									<option value="">Select Action</option>
		            				<? if(($menuObj->checkDeletePermission())){  ?>
		            				<option value="deleteselected">Delete Selected</option>
		            				<? } ?>
							 </select>
						</li>
						<li><input name="Input" type="submit" value="Submit"  class=""/></li>
						<li><input name="searchtext" type="text" class="adminsearch" value="<?=$searchtxt=$_GET['searchtxt']?$_GET['searchtxt']:'searchtext'?>" onClick="clickclear(this,'searchtext')" onBlur="clickrecall(this,'searchtext')"/></li>
						<li><input name="GO" type="submit" value="Go"  class=""/></li>
						<li class="showall"><a href="<?=basename($_SERVER['PHP_SELF'])?>">Reset</a></li>
					</ul>
				</div>
	<? //$p = SEARCHTEXT?>
		<div><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	
	  	<table class="documentation">
            <tr>
			<thead>
				<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></th>
				<th>SN.</th>
				<th>User Type</th>
				<th>Subject</th>
				<th><?=orderBy("manageContactUs.php?searchtxt=$searchtxt","emailId","From")?></th>
				<th><?=orderBy("manageContactUs.php?searchtxt=$searchtxt","addDate","Date")?></th>
				<th style="width:100px;">Reply Status</th>
				<th>View</th>
				<th>Reply</th>
				<th>Delete</th>
				<!--<th><img width="22" src="images/attachment.png" /></th>-->
			</thead>
		</tr>	
    	<?php echo $contactUsObj->valDetail(); ?> </div>
  		</fieldset>
	</form>
    </fieldset>
	</section>
<?php include_once('includes/footer.php');?>

<?php
/*---Basic for Each Page Starts----*/
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
$xmfInfoObj = new XmlInfo();
$xmfInfoObj->setPanel();
/*---Basic for Each Page Starts----*/

$id = $_GET[id]?$_GET[id]:0;
$fabricObj = new Fabric();

if (isset($_POST['submit'])) {  
   $fabricObj->editFabricCharges($_POST);
   //$result = $sysObj->executeQry("Select * from ".TBL_DELIVERY_TIME." as dt inner join ".TBL_DELIVERY_TIME_CHARGES." as dtc on dt.id = dtc.d_id ");
}
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
 
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
 <!-- 	<h1>Fabric Discount<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addFabricDiscount.php">Add Fabric Discount</a><? } ?></h1>		 -->    	  	   
         <h1>Fabric Price Panel</h1> 
      <fieldset>
         <form name="configUser" id="configUser" method="post">
   <?		echo $_SESSION['SESS_MSG'];                
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
            	<label>Fabric</label>
                <section>
                	<label>Select Fabric</label>
                	<div>
						<select id ="fabric" name="fabricId" onchange="return viewFabric(this.value);">
							<option value="">Select Fabric Type</option>
							<?=$fabricObj->getFabricType();?>
						</select><span id="loader"></span>
						<input type="button" id="fabricPrice" value="Add Fabric Price" onclick="return addFabricPrice();" style="margin-left:33px;" />
                	</div>                  	           	
                </section>
			</fieldset>
			
			<fieldset id="addFabricDiscount" style="display:none;"></fieldset>
		            	
     		<div class="main-body-sub" id="btnSubmit" style="text-align:center; margin-left:0px; display:none;">     			
                <input type="submit" name="submit"   value="Submit" />
            </div>	     
            <div id="divTemp" style="display:none;"></div>			
         </form>
      </fieldset>
   </section>
<?php include_once('includes/footer.php'); ?>
<script type="text/javascript">
	function addFabricPrice() {		
		var fId = document.getElementById("fabric").value;		
		window.location = "addFabricPrice.php?fId="+fId;
	}
</script>

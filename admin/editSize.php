<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageSize.php", "edit_record");
/* ---Basic for Each Page Starts---- */
$sizeObj = new Size();
$generalFunctionObj = new GeneralFunctions();

if (isset($_POST['submit'])) {
   require_once('validation_class.php');
   $obj = new validationclass();
   $errorArr = 0;
   $rst = $sizeObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
   $num = $sizeObj->getTotalRow($rst);
   if ($num > 0) {
      $langIdArr = array();
      while ($line = $sizeObj->getResultObject($rst)) {
         array_push($langIdArr, $line->id);
      }
      foreach ($langIdArr as $key => $value) {
         $obj->fnAdd('sizeName_' . $value, $_POST['sizeName_' . $value], 'req', LANG_PLEASE_ENTER_NAME);
      }
      $obj->fnAdd('groupId',$_POST['groupId'], 'req','Please Select Group Name');
      
      $arr_error = $obj->fnValidate();
      $str_validate = (count($arr_error)) ? 0 : 1;

      foreach ($langIdArr as $key => $value) {
         $arr_error['sizeName_' . $value] = $obj->fnGetErr($arr_error['sizeName_' . $value]);
      }
      $arr_error['groupId']=$obj->fnGetErr($arr_error['groupId']);	
            
      foreach($langIdArr as $key=>$value) {			
		if($sizeObj->checkSizeExists($_POST['sizeName_'.$value],$value, base64_decode($_GET['id']), $_POST[groupId])) {
			$arr_error['sizeName_'.$value] ='<span class="alert-red alert-icon">Size already exist</span>';
			if($arr_error['sizeName_'.$value]) 
			$errorArr = 1;							
		}
	  }
		
	  if($str_validate and $errorArr == 0){
		$sizeObj->editRecord($_POST);
  	  }
   }
}
$result = $sizeObj->getResult(base64_decode($_GET['id']));
?>
<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>

<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
   function hrefBack1(){
      window.location='manageSize.php';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Miscellaneous</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>">
            <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
            <fieldset>
               <label>Edit Size</label>
				<?= $_SESSION['SESS_MSG'] ?>
               <section>
                  <label>Name</label>
                  <div>
                 <?= $generalFunctionObj->getLanguageEditTextBox('sizeName', 'm__Name', TBL_SIZEDESC, base64_decode($_GET['id']), "id", $arr_error) ?>
                  </div>
               </section>
               <section>
                  <label for="categoryName"> &nbsp;Make Default</label>
                  <div>
                     <input type="checkbox" name="isDefault" <?php if($result->isDefault == '1'){ echo "checked=checked"." "."disabled=disabled";} ?>>
                  </div>
               </section>
               <section>
                <label>Group Name</label>
                <div>
					<select name="groupId">
						<option value="">Select Group Name</option>
						<?= $sizeObj->getGroupName($result->groupId); ?>						
					</select>	
					<?= $arr_error[groupId] ?>
				</div>
            </section>
            </fieldset>

            <fieldset>
               <section>  
                  <label>&nbsp;</label>
                  <div style=" width:78%;">
                     <input type="submit" name="submit"   value="Submit" />
                     <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
                  </div>
               </section>
            </fieldset>
         </form>
      </fieldset>
   </section>
<? unset($_SESSION['SESS_MSG']); ?>

<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
  // echo 'fdds'.$_GET[id];die;
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMainProducts.php","edit_record");
/*---Basic for Each Page Ends----*/		
$proObj = new MainProducts();
$genObj = new GeneralFunctions();
//$promain=$proObj->getResult($_GET);

//echo $promain->product_type_id;
//die;
// if($_GET['notice']==1){
// 	$prodesc=$protyObj->getResultDesc($promain->product_type_id);		
// 	$procat=$protyObj->getResultCat($promain->product_type_id);
// }else{
// 	$prodesc=$proObj->getResultDesc(base64_decode($_GET['id']));
// 	$procat=$proObj->getResultCat(base64_decode($_GET['id']));
// }

if(isset($_POST['update'])) // <FOR TAB1>
{
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $proObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $proObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $proObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productName_'.$value,$_POST['productName_'.$value], 'req', 'Please Enter Product Name.');			
		}
		$obj->fnAdd('productPrice',$_POST['productPrice'], 'req', 'Please Enter Product Price.');
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('productDesc_'.$value,$_POST['productDesc_'.$value], 'req', 'Please enter the Product Desciption.');
		}
		
		//===================================================
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		//===================================================
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['productName_'.$value]=$obj->fnGetErr($arr_error['productName_'.$value]);
			if($arr_error['productName_'.$value]) 
				$errorArr = 1;
		}	

		foreach($langIdArr as $key=>$value) {
			$arr_error['productDesc_'.$value]=$obj->fnGetErr($arr_error['productDesc_'.$value]);
			if($arr_error['productDesc_'.$value]) 
				$errorArr = 1;
		}
		$arr_error[productPrice]=$obj->fnGetErr($arr_error[productPrice]);

		if($errorArr == 0 && isset($_POST['update']) && empty($arr_error[productPrice])){	
			$proObj->editRecord($_POST);
		}
	}	
}//</FOR TAB1>

if(isset($_POST['save'])) // <FOR TAB2>
{
	$_GET['tab'] = 2;
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $proObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $proObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $proObj->getResultObject($rst)) {
			array_push($langIdArr,$line->id);
		}		
		
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('colorName_'.$value,$_POST['colorName_'.$value], 'req', 'Please Enter Color Name.');			
		}
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['colorName_'.$value]=$obj->fnGetErr($arr_error['colorName_'.$value]);
		}
		$obj->fnAdd('colorCode',$_POST['colorCode'], 'req', 'Please Enter Color Code.');
		//$obj->fnAdd('designColorCode',$_POST['designColorCode'], 'req', 'Please Enter Design Color Code.');
		
		//========================================================
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1; 
		//========================================================
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['colorName_'.$value]=$obj->fnGetErr($arr_error['colorName_'.$value]);
			//if($arr_error['colorName_'.$value]) 
			//$errorArr = 1;
		}
		$arr_error['colorCode']=$obj->fnGetErr($arr_error['colorCode']);
		//$arr_error['designColorCode']=$obj->fnGetErr($arr_error['designColorCode']);
		
		//----------------- Image validation ----------------------------------------------------------
		$permitableExt = $proObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'");//checkExtensions(0);
		foreach($proObj->images as $key=>$val) {
			if(!empty($_FILES[$val]['name'])) {
				$fileext = findexts($_FILES[$val]['name']);
				if($fileext) {
					if($proObj->checkExtensions($fileext) == false ) {
						$arr_error['prodImage'] = '<span class="alert-red alert-icon">Invalid Image Extention For '.$key.',Please Upload Image With Extention '.$permitableExt.'.</span>';
						$str_validate=0;break;
					}
				}
				else {
					$arr_error['prodImage'] = '<span class="alert-red alert-icon">Invalid Image Extention For '.$key.',Please Upload Image With Extention '.$permitableExt.'.</span>';
					$str_validate=0;break;
				}	
			}
			else {
				$arr_error['prodImage'] = '<span class="alert-red alert-icon">Please Uploade An Image For '.$key.'</span>';
				$str_validate = 0;break;
			}
		}
		
		if($str_validate){
			$proObj->addProductNewColor($_POST,$_FILES);
		}
	}
}//</TAB2>
//<TAB 4>
if(!empty($_POST['inventory'])) {
/*	$flag = true;
	if(!empty($_POST['sizeidcsv']))
	{
		$sizeidarr= explode(',',$_POST['sizeidcsv']);
		if(sizeof($sizeidarr) > 0)
		{
			foreach($sizeidarr as $sizeid)
			{
				$sizeval = 'size'.$sizeid;
				if($_POST[$sizeval])
				{
					$flag = false; break;
				}
			}
		}
	}
	if($flag) {
		$arr_error['size'] = '<span class="alert-red alert-icon">Please Select At Least One Size.</span>';
		$str_validate = 0;
	} */
	//------------------------------------------------------------
    $proObj->saveProdQuantity($_POST);
}//</TAB 4>

if(!is_int($_GET['id'])) $_GET['id'] = base64_decode($_GET['id']);
$result1 = $proObj->getResult($_GET['id']);
//$colorcode = $proObj->getProductColor(base64_decode($_GET['id']));

//$attributeValueId = $proObj->getAttributeValueId(base64_decode($_GET['id']) , 'AttributeValueId');
//$attributeId = $proObj->getAttributeValueId(base64_decode($_GET['id']) , 'attributeId');
//$price = $proObj->getAttributeValueId(base64_decode($_GET['id']) , 'price');

//$allatributeid = $proObj->getAttributeAllId();
//$allatributevalueid = $proObj->getAllAttributeValueId();
//echo 'fafsds';
//var_dump($result);
$productid = $_GET['id'];
?>
		
<?=headcontent()//DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>


	<!--				Light Box Starts			-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
 <!--	Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--	Color Picker (END)		-->
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->
<!-- show sizing tab if redirectec from noticeInventory.php page -->

<script type="text/javascript" language="JavaScript">
	$(document).ready(function(){
	showtabvalue(<?php echo $_GET['tab']?$_GET['tab']:1; ?>); // temp
});
</script>
			
</head>
<body>
	<? include('includes/header.php'); ?>
	<section id="content">
		<h1>Edit Main Product</h1>
		<fieldset>
			<div id="mainDiv">
				<div><?=$_SESSION['SESS_MSG'];?></div>
				<div class="tab ui-tabs ui-widget ui-widget-content ui-corner-all">
					<!--  ##################################################################################  -->
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
						<li><a href="javascript:void(0);"  onClick="showtabvalue(1);" id="T1" class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">General</a></li>
						<?php if($result1->rowProductId == 0):?>
						<li><a href="javascript:void(0);"  onclick="showtabvalue(2);" id="T2" class="ui-state-default ui-corner-top">Product Colors</a></li>
						<li><a href="javascript:void(0);"  onclick="showtabvalue(3);" id="T3" class="ui-state-default ui-corner-top">Size</a></li>
						<li><a href="javascript:void(0);"  onclick="showtabvalue(4);" id="T4" class="ui-state-default ui-corner-top">Inventory</a></li>
						<?php endif;?>
					</ul>
					<!--  ##################################################################################  -->
					<div id="tab1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
					<fieldset>
						<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data"><!--javascript: return validateFrm(this);-->
							<input type="hidden" name="id" value="<?=$_GET['id']?>">
							<input type="hidden" name="page" value="<?=$_GET['page']?>">
							<input type="hidden" name="atid" id="atid" value="<?=$allatributeid?>">
							<input type="hidden" name="atvid" id="atvid" value="<?=$allatributevalueid?>">
							<fieldset>
								<section><label style="width:100%;font-weight:bold;" for="name">Manage Product General Info</label></section>
								<section>
									<label for="styleId">Style</label>
									<div>
										<select name="styleId" id="styleId" disabled>
											<?
												echo $proObj->getStyleList($result1->styleId);
											?>
										</select>
										<div id="chk_styleId"><?=$arr_error['styleId'];?></div>
									</div>
								</section>
								<section>
									<label for="Category">Category</label>
									<div>
										<select name="categoryId" id="categoryId" >
											<?
												echo $proObj->getCategoryList($result1->categoryId);
											?>
										</select>
										<div id="chk_categoryId"><?=$arr_error['categoryId'];?></div>
									</div>
								</section>
								<section>
									<label for="name">Product Name</label>
									<div>
									<?=$genObj->getLanguageEditTextBox('productName','m__productName',TBL_MAIN_PRODUCT_DESCRIPTION,$_GET['id'],"productId",$arr_error);?>
									
					<!-- 				<input type="text" name="name" id="m__name" class="wel" value="<? //=$_POST[name]?>" /> -->
					<!-- 				<div id="chk_name"><?//=$arr_error['name'];?></div> -->
									</div>
								</section>
								<section>
									<label for="name">Product Price</label>
									<div><input type="text" name="productPrice" id="m__Product_Price" class="wel" value="<?=stripslashes($result1->productPrice)?>" maxlength="8" onkeyup="return isNum12(this.value);"   />
									<div id="chk_productPrice"><?=$arr_error['productPrice'];?></div>
									</div>
								</section>
								<section>
									<label for="description">Product Description</label>
									<div>
									<?=$genObj->getLanguageEditTextarea('productDesc','m__Product_Description',TBL_MAIN_PRODUCT_DESCRIPTION,$_GET['id'],"productId",$arr_error);?>
									
									<!--<textarea name="description" id="m__description" cols="" rows=""><?=$_POST[description]?></textarea>
									<div id="chk_description"><?=$arr_error['description'];?></div>-->
									</div>
								</section>
							</fieldset>
							<fieldset> 
								<section>  
									<div>
									<!--	<input name="notice" id="notice" type="hidden" value="<? //=$_GET['notice'];?>" />
										<input name="id" type="hidden" value="<? //=$_GET['id'];?>" /> -->
										<input type="submit" name="update" value="Submit">
										<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">
									</div>
								</section>
							</fieldset>
						</form>
					</fieldset>
					</div>
<? /* ##################### End Tab1 ################################################################ */ ?>
					<div id="tab2" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
					<fieldset>
						<form name="frmupload" id="frmupload" method="post" onsubmit="javascript: return validateFrm(this);" enctype="multipart/form-data"  >
							<input type="hidden" name="id" value="<?=$_GET['id']?>">
							<input type="hidden" name="sequence" value="<?=$proObj->getMaxSequence(base64_decode($_GET['id'])) ?>" />
						<fieldset>
							<section><label style="width:100%;font-weight:bold;" for="name">Manage Product Colors and Images</label></section>
							<section>
									<? //=$genObj->getFirstProductType();
										//$fptid=($_POST['product_type_id']!='')?$_POST['product_type_id']:$genObj->getFirstProductType();?>
									<?=$proObj->get_color_view($_GET['id'])?>
									<? //=$proObj->getColorViewImageUpload(); ?>	
							</section>
						</fieldset>
						<fieldset>
							<section>
								<label for="name">Add More Color</label>
								<div>
									<a href="Javascript:void(0);" onclick="toggleDisplay1('moreCols');">Click Here!</a>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="Javascript:void(0);" onclick="hrefBack1();">&#171;Back</a>
								</div>
							</section>
						</fieldset>
						<fieldset id="moreCols" style="display:<?=$_POST['save']?$_POST['save']:'none'?>;">
							<?php /*
							<section>
								<label for="designcolorcode">Design Color Code</label>
								<div>
									<table style="padding:0px;margin:0px;"  border="0">
										<tr>
											<td><input type="text" name="designColorCode" id="m__design_color_code" maxlength="7" value="<?=$_POST[designColorCode] ?>" /></td>
											<td style="text-align:left; width:160px;"><img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmupload.designColorCode)" border="0" style="cursor:pointer;"></td>
										</tr>
									</table>
									<div id="chk_designColorCode"><?=$arr_error['designColorCode'];?></div>
								</div>
							</section> */?>
							<section>
								<label for="colorCode">Color Code</label>
								<div>
									<table border="0">
										<tr>
											<td><input type="text" name="colorCode" id="m__color__code" maxlength="7" value="<?=$_POST[colorCode] ?>" /></td>
											<td style="text-align:left; width:160px;"><img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.frmupload.colorCode)" border="0" style="cursor:pointer;"></td>
										</tr>
									</table>
									<div id="chk_colorCode"><?=$arr_error['colorCode'];?></div>
								</div>
							</section>
							<section>
								<label for="name">Color Name</label>
								<div>
								<?=$genObj->getLanguageTextBoxEngValidation('colorName','m__Color_Name',$arr_error);   ?>
								</div>
							</section>
							<?php /*<section>
								<label for="name">Color Price</label>
								<div><input type="text" name="colorPrice" value="" />
				<!-- 				<div id="chk_productPrice"><?=$arr_error['productPrice'];?></div> -->
								</div>
							</section> */ ?>
								<section>
								<label for="name">Product Images</label>
								<div> 
									<div id="product_view" >
										<?=$proObj->getColorViewImageUpload(); ?>
										<div id="chk_name"><?=$arr_error['prodImage']?></div>
									</div>
								</div>
							</section>
						<!--</fieldset>
						 <fieldset>  -->
							<section>
								<div>
									<input type="submit" name="save" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
									<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">
								</div>
							</section>
						</fieldset>
					</form>
				</fieldset>
				</div>
<!-- ################## End Tab2 ################################################################################# -->
				<div id="tab3" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"><!--ui-tabs-hide-->
				<fieldset>
					<form name="uploadform_size" method="post" action="pass.php?action=mainproduct&type=editSize" id="uploadform<?=$productid."".$colorarray->id?>" enctype="multipart/form-data">
					<fieldset>
						<section><label style="width:100%;font-weight:bold;" for="name"><b>Manage Product Size</b></label></section>
						<section>
							<label for="name">Select Size</label>
								<?php echo $proObj->getAttributeName($_GET['id']); ?>
								<input type="hidden" name="productId" value="<?=$productid?>" />
								<input type="hidden" name="page" value="<?=$_GET[page]?>" />
						</section>
					</fieldset>
					<fieldset>
						<section>
							<div>
								<input type="submit" name="save" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
								<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">
							</div>
						</section>
					</fieldset>
					</form>
				</fieldset>
				</div>
<!-- ##################  End Tab3 ####################################################################################### -->
				<div id="tab4" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide"><!--ui-tabs-hide-->
				<fieldset>
					<form name="uploadform_qty" method="post" action="" >
					<fieldset>
						<section><label style="width:100%;font-weight:bold;" for="name"><b>Manage Inventory</b></label></section>
						<section>
								<?php echo $proObj->getProdQuantity($_GET['id']);?>
								<input type="hidden" name="productId" value="<?=$productid?>" />
								<input type="hidden" name="page" value="<?=$_GET[page]?>" />
						</section>
					</fieldset>
					<fieldset>
						<section>
							<div>
								<input type="submit" name="inventory" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
								<input type="button" onClick="javascript:;hrefBack1()" value="Back" id="back" name="back">
							</div>
						</section>
					</fieldset>
					</form>
				</fieldset>
				</div>
<!-- ##################  End Tab4 ####################################################################################### -->
			</div>
		</div>
	</fieldset>
	</section>
<script language="javascript"> 
	////////////////// color size quantity start///////////
	function showtabvalue(id)
	{
		//alert(id);
		$(".ui-tabs-panel").addClass('ui-tabs-hide');
		$("#tab"+id).removeClass('ui-tabs-hide');
		
		$(".ui-state-default").removeClass('ui-tabs-selected ui-state-active');
		$("#T"+id).addClass('ui-tabs-selected ui-state-active');
		/*if($("#T"+id).attr('csquant')==1)
		{
		do_csq();
		}*/
	}
	function hrefBack1(){
		window.location='manageMainProducts.php';
	}
	function toggleDisplay1(divId)
	{
		if(divId) {
			if(document.getElementById(divId).style.display == 'none') document.getElementById(divId).style.display = 'block';
			else document.getElementById(divId).style.display =  'none';
		}
	}
</script>
<? unset($_SESSION['SESS_MSG']); ?>
<footer>&copy;copyright by Flop Happy</footer><div id="nav-under-bg"><!-- --></div>
</body>
</html>

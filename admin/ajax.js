var xmlHttp;
/// Start: Get Object==================
function getobject()
{
    var xmlHttp=null;
    try
    {
        xmlHttp=new XMLHttpRequest;
    }
    catch(e)
    {
        try
        {
            xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch(e)
        {
            xmlHttp=new ActiveXObject("Microsoft.XMLHTTP")	;
        }	
    }
    return xmlHttp;
}
	
//-----systemconfig---------
function changeConfigLanguage(countryId) {		
    xmlhttp = getobject();
    var query = "?action=systemConfig&type=getLanguage&cId="+countryId;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            var chek = xmlhttp.responseText;
            //alert(chek);				
            document.getElementById("DEFLANGUAGE").innerHTML = chek;
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}

	
//==================Accessories=================	
function viewAccessories1(id) {		
    xmlhttp = getobject();
		
    if(id == '') {			
        document.getElementById("showAccessories").style.display = 'none';
    } else {			
        document.getElementById("showAccessories").style.display = 'block';
    }
    var query = "?action=manageAccessories&type=view&id="+id;
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4) {
            if(xmlhttp.responseText == "") {					
                document.getElementById("showAccessories").style.display = 'none';		
            }else{
                document.getElementById("showHeading").style.display = 'block';					
                document.getElementById("showAccessories").innerHTML = xmlhttp.responseText;
            }
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}
 		
function deleteAccessories(id, accessoriesId) {
    xmlhttp = getobject();
    var query = "?action=manageAccessories&type=delete&id="+id+"&accessoriesId="+accessoriesId;		
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4) {
            if(xmlhttp.responseText == "Success") {
                document.getElementById(id).style.display = 'none';
            }
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}
//=================end accessories===========================
	
function showPallets(id){
    xmlhttp=getobject();
		
    if(id == '')
    {
        document.getElementById("editSelected").disabled = true;
    }
    else
    {
        document.getElementById("editSelected").disabled = false;
    }	
		
    var query="?action=colorPallet&type=showSinglePallet&id="+id;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            var chek = xmlhttp.responseText;			
            document.getElementById("palletContainer").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php"+query,true);
    xmlhttp.send(null);	
		
}

	
function removeColorPallet(id){
    var conf = confirm('Are you sure?');
    if(conf)
    {
        xmlhttp=getobject();
        var query="?action=colorPallet&type=removePallet&id="+id;
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4)
            {							
                var chek = xmlhttp.responseText;
                //alert(chek);
                window.location="manageColor.php";				
            //document.getElementById("check").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","pass.php"+query,true);
        xmlhttp.send(null);
    }
    else
        return false;
}
 
function change_orderstatus(id, order_status){    
    xmlhttp=getobject();
    var query="?action=manageorder&type=changeorderstatus&id="+id+"&order_status="+order_status;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            var chek = xmlhttp.responseText;
            alert("Order status has been changed.");
        //document.getElementById("check").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php"+query,true);
    xmlhttp.send(null);	
}

function change_paymentstatus(id, payment_status){    
    xmlhttp=getobject();    
    var query="?action=manageorder&type=changepaymentstatus&id="+id+"&paymentstatus="+payment_status;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {            
//            alert(xmlhttp.responseText);
            alert("Payment status has been changed.");			
        //document.getElementById("check").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php"+query,true);
    xmlhttp.send(null);	
}

function deleteRawProdImg(pid,colorid,viewid){
    // alert(pid);
    if(confirm('Are you sure to delete this Record  ?')){
        xmlhttp=getobject();
        var
        query="?action=delRawImg&type=deletecolorproduct&productId="+pid+"&colorId="+
        colorid+"&viewid="+viewid;
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4)
            {							
                var chek = xmlhttp.responseText;			
                document.getElementById("imgdiv"+viewid).innerHTML =
                xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","pass.php"+query,true);
        xmlhttp.send(null);	
    }else{}
}

function deleteColorProduct(divid,colorid,pid,action)
{
    if(confirm('Are you sure to delete this Record  ?')){
        xmlhttp=getobject();
  
        var query ="?action="+action+"&type=deletecolorproduct&productId="+pid+"&colorId="+colorid;
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4)
            {							
                var chek = xmlhttp.responseText;
                window.location.href="editProduct.php?id="+pid+"&tab=2";
            }
        }
        xmlhttp.open("GET","pass.php"+query,true);
        xmlhttp.send(null);	
    }else{}
}

function deleteColorMainProduct(divid,colorid,pid,action)
{
    if(confirm('Are you sure to delete this Record?'))
    {
        xmlhttp=getobject();
        var query ="?action="+action+"&type=deletecolorproduct&productId="+pid+"&colorId="+colorid;
        //alert(query);
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4)
            {							
                var chek = xmlhttp.responseText;
                //alert(chek);
                window.location.href="editMainProduct.php?id="+pid+"&tab=2";
            }
        }
        xmlhttp.open("GET","pass.php"+query,true);
        xmlhttp.send(null);	
    }else{}
}

function updateStyleDefault(sid)
{
    //alert(sid);
    xmlhttp=getobject();
    var query="sid="+sid+"&action=Style";
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {
            //alert(xmlhttp.responseText);
            window.location.href="manageStyle.php";
        }
    }
    xmlhttp.open("GET","pass.php?type=default&"+query,true);
    xmlhttp.send(null);
}

function updateColorDefault(cid,pid,action)
{
    //alert(pid);
    xmlhttp=getobject();
    var query="pid="+pid+"&cid="+cid+"&action="+action;
    //alert(query);
    //document.getElementById(showImg).innerHTML='<img src="images/loading_icon.gif">';
    //alert(xmlhttp.responseText);
    xmlhttp.onreadystatechange=function()
    {
        //alert(xmlhttp.responseText);
        if (xmlhttp.readyState==4)
        {
            //alert(xmlhttp.responseText);
            //document.getElementById('colorPrice').value = xmlhttp.responseText;
            window.location.href="editProduct.php?id="+pid+"&tab=2";
        }
    }
    xmlhttp.open("GET","pass.php?type=updateColorDefault&"+query,true);
    xmlhttp.send(null);
}

function updateMainColorDefault(cid,pid,action)
{
    //alert(pid);
    xmlhttp=getobject();
    var query="pid="+pid+"&cid="+cid+"&action="+action;
    //alert(query);
    //document.getElementById(showImg).innerHTML='<img src="images/loading_icon.gif">';
    //alert(xmlhttp.responseText);
    xmlhttp.onreadystatechange=function()
    {
        //alert(xmlhttp.responseText);
        if (xmlhttp.readyState==4)
        {
            //alert(xmlhttp.responseText);
            //document.getElementById('colorPrice').value = xmlhttp.responseText;
            window.location.href="editMainProduct.php?id="+pid+"&tab=2";
        }
    }
    xmlhttp.open("GET","pass.php?type=updateColorDefault&"+query,true);
    xmlhttp.send(null);
}

function updateColorPrice(price,pid,cid)
{
    //alert(price);
    xmlhttp=getobject();
    var query="price="+price+"&pid="+pid+"&cid="+cid+"&action=productColor";
    //document.getElementById(showImg).innerHTML='<img src="images/loading_icon.gif">';
    //alert(xmlhttp.responseText);
    xmlhttp.onreadystatechange=function()
    {
        //alert(xmlhttp.responseText);
        if (xmlhttp.readyState==4)
        {			
        //document.getElementById('colorPrice').value = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=updateColorPrice&"+query,true);
    xmlhttp.send(null);	
}

function changeViewColorStatus(divID,cid,action)
{
    xmlhttp=getobject();
    var query="cid="+cid+"&action="+action;
    document.getElementById(divID).innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if(xmlhttp.readyState==4)
        {				
            document.getElementById(divID).innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=changestatus&"+query,true);
    xmlhttp.send(null);	
}
	
/// Start: Change Status==================
function changeStatus(divID,id,action,frontOrAdmin)
{		
    //alert(frontOrAdmin);// For tool module in admin
    // exit();
    //alert(divID);
    //alert(id);
    //alert(action);
    xmlhttp=getobject();
    var query="id="+id+"&action="+action+"&frontOrAdmin="+frontOrAdmin;            
    $oldStatus = document.getElementById(divID).innerHTML;
    document.getElementById(divID).innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {
            var response = xmlhttp.responseText;
            //alert(response);
            var responseArr = response.split(",");
                            
            if(($.trim(responseArr[1])=="0")||($.trim(responseArr[1])=="1")){
                document.getElementById(divID).innerHTML=responseArr[0];
            }
            else{
                alert(responseArr[0]);
                document.getElementById(divID).innerHTML=$oldStatus;
            }
        }
    }
    xmlhttp.open("GET","pass.php?type=changestatus&"+query,true);
    xmlhttp.send(null);	
}
	
	
/// Start: Change Default==================
function changeDefault(divID,id,action)
{ 
    xmlhttp=getobject();
    var query="id="+id+"&action="+action;
	
    document.getElementById(divID).innerHTML='<img src="images/loading_icon.gif">';
		
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {	
            /*document.getElementById(divID).innerHTML=xmlhttp.responseText;
				document.getElementById(divID+"_del").innerHTML='';
				document.getElementById(divID.replace("default","status")).innerHTML='Active';*/
            window.location.href=xmlhttp.responseText;
        }
    }
    xmlhttp.open("POST","pass.php?type=changedefault&"+query,true);
    xmlhttp.send(null);	
}
	
/// Start: Take Backup==================
function takeBackup()
{
    xmlhttp=getobject();
    var query="";
    document.getElementById("showMsg").innerHTML='<img src="images/indicator.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            document.getElementById("showMsg").innerHTML = "<font size='3'>DATABASE BACKUP IS COMPLETED.</font><br><b>It is stored in -- "+xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?action=backup&type=takeBackup&"+query,true);
    xmlhttp.send(null);	
}


/// Start: Get Country List==================
function getCountryList(zoneid)
{	
    if(zoneid > 0) {
        xmlhttp=getobject();
        var query="zoneid="+zoneid+"&action=state&mode=1";
        document.getElementById("divAllCountry").innerHTML='<img src="images/loading_icon.gif">';
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4)
            {								
                document.getElementById("divAllCountry").innerHTML=xmlhttp.responseText;
            }
        }		
        xmlhttp.open("GET","pass.php?type=getCountryList&"+query,true);
        xmlhttp.send(null);	
    }
}


/// Start: Get State List=================
function getStateList(countryID)
{	
		
    xmlhttp=getobject();
    var query="countryID="+countryID+"&action=state&mode=1";
    document.getElementById("divAllState").innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {								
            document.getElementById("divAllState").innerHTML=xmlhttp.responseText;
        }
    }		
    xmlhttp.open("GET","pass.php?type=getStateList&"+query,true);
    xmlhttp.send(null);	
		
}

	

/// Start: Change =================
function change(id)
{
    if(id == 1)
    {
        document.getElementById('url').style.visibility = 'hidden';
    }
    else if(id ==2 )
    {
        document.getElementById('url').style.visibility = 'visible';
    }
}
	
/// Start: Change Drop Down Value==================
function changedropdownVal(type)
{
    var retVal = new Array();  
    retVal = type.split('&');
    var typeLen = retVal.length;
    var queryString = "";
    var ss;
    var result;
    if(typeLen >= 2)
    {
        queryString = queryString + retVal[0]; 
        for(i=1;i<=typeLen - 1;i++)
        {
            ss = retVal[i]; 
            result = ss.indexOf("imit=");   
            if(result != 1)
            {   
                queryString += "&"+ss;
            }
        }
    }
    else 
    { 
        queryString = type;
    }
    //alert(queryString); 
    var limit = document.getElementById("templateName").value;
    window.location=queryString+"?dropval="+limit;
}

/// Start: sort Order =================
function sortOrder(url)
{
    xmlhttp=getobject();
    var query="url="+url+"&action=category";
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            var chek = xmlhttp.responseText;			
            document.getElementById('dragndrop').innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=SORTORDER&"+query,true);
    xmlhttp.send(null);	
}
	
/// Start: Sort Order Product==================
function sortOrderProduct(url)
{
    xmlhttp=getobject();
    var query="url="+url+"&action=product";
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {							
            var chek = xmlhttp.responseText;			
            document.getElementById('dragndrop').innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=SORTORDER&"+query,true);
    xmlhttp.send(null);	
}
	
/// Start: Get Sub Category Drop Down =================
function getSubCatDropdown(catId)
{

    //alert(catId)

    xmlhttp=getobject();

    var query="catId="+catId+"&action=manageVendor";

    //alert(query)

    //document.getElementById("optionType").innerHTML='<img src="images/loading_icon.gif">';

    xmlhttp.onreadystatechange=function()

    {

        if (xmlhttp.readyState==4)

        {						

            var chek = xmlhttp.responseText;

            //alert(chek)

            document.getElementById("subCat").innerHTML= chek;

				

        }

    }		

    //alert(query)

    xmlhttp.open("GET","pass.php?type=getDocSubCatDropdown&"+query,true);

    xmlhttp.send(null);	

}
	
/// Start: Check Shipping Price=================
function checkShippingPrice(id)
{
    xmlhttp=getobject();
    var query="id="+id+"&action=manageShipping";
    xmlhttp.onreadystatechange=function()

    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
            if(chek == 1)
            {
                $("#shippingAmountDiv").css("display", "block");
            }else{
                $("#shippingAmountDiv").css("display", "none");
            }
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=checkShippingPrice&"+query,true);

    xmlhttp.send(null);	

}
	
/// Start: Get Sub Category In Drop Down =================
function getSubCategoryInDropdown(id)
{
    xmlhttp=getobject();
    var query="id="+id+"&action=product";
    xmlhttp.onreadystatechange=function()

    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
            if(chek == 1000)
            {
                $("#subCatDiv").css("display", "none");
                selectAllVerdict(id);
            }else{
                $("#subCatDiv").css("display", "block");
                $("#subCatLi").html(chek);
                $("#productVerdictDiv").css("display", "none");
                $("#productAttributeDiv").css("display", "none");
            }
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=getSubCategoryInDropdown&"+query,true);

    xmlhttp.send(null);		
}
	
/// Start: Get Best Item In Drop Down ==================
function getBestitemInDropdown(id)
{
    xmlhttp=getobject();
    var query="id="+id+"&action=manageBestItemConsider";
    xmlhttp.onreadystatechange=function()

    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
            if(chek == 1000)
            {
                $("#subCatDiv").css("display", "none");
                selectAllVerdict(id);
            }else{
                $("#subCatDiv").css("display", "block");
                $("#subCatLi").html(chek);
                $("#productVerdictDiv").css("display", "none");
                $("#productAttributeDiv").css("display", "none");
            }
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=getBestitemconsiderInDropdown&"+query,true);

    xmlhttp.send(null);		
}
	
/// Start: Select All Verdict==================
function selectAllVerdict(catId)
{
    xmlhttp=getobject();
    var query="catId="+catId+"&action=product";
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
            if(chek == 1000)
            {
                $("#productVerdictDiv").css("display", "none");
            }else{
                $("#productVerdictDiv").css("display", "block");
                $("#productVerdictDiv").html(chek);
            }
            //$("#productVerdictDiv").html(chek);
            selectAllAttributes(catId);
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=selectAllVerdict&"+query,true);

    xmlhttp.send(null);		
}
	
/// Start: Select All Attributes==================
function selectAllAttributes(catId)
{
    xmlhttp=getobject();
    var query="catId="+catId+"&action=product";
    xmlhttp.onreadystatechange=function()

    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
            if(chek == 1000)
            {
                $("#productAttributeDiv").css("display", "none");
            }else{
                $("#productAttributeDiv").css("display", "block");
                $("#productAttributeDiv").html(chek);
            }
        //$("#productAttributeDiv").html(chek);
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=selectAllAttributes&"+query,true);

    xmlhttp.send(null);		
}
	
/// Start: Check Vendor Url =================
function checkVendorUrl(vendorId, count)
{
    xmlhttp=getobject();
    var query="vendorId="+vendorId+"&action=product";
    xmlhttp.onreadystatechange=function()

    {
        if (xmlhttp.readyState==4)
        {						
            var chek = xmlhttp.responseText;
					
            if(chek==0)
            {
                $("#vendorDiv"+count).css("display", "block");
            }else{
                $("#vendorDiv"+count).css("display", "none");
            }
        }

    }	
		
    xmlhttp.open("GET","pass.php?type=checkVendorUrl&"+query,true);

    xmlhttp.send(null);		
}
	
function deleteRec(delId){ 
    xmlhttp=getobject();
    document.getElementById('delDivId'+delId).style.display = 'none';	
    xmlhttp.open("GET","pass.php?type=deleteDiscount&action=discount&id="+delId,true);
    xmlhttp.send(null);	
}
	
function getGiftRange(id){
		
    xmlhttp=getobject();
    var query="keyword="+keyword+"&action=coupon";
    document.getElementById("giftRange").innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {				
            document.getElementById("giftRange").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?action=gift&id="+id,true);
    xmlhttp.send(null);	
}
	
function deleteTemplate(delId){ 
    xmlhttp=getobject();
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {	
            if(xmlhttp.responseText){
                window.location.reload();
                alert("Template has been deleted sucessfully.")
            }
            else
                alert("Unable to delete template.")
        }
    }
    xmlhttp.open("GET","pass.php?type=deleteTemplate&action=giftcertificate&id="+delId,true);
    xmlhttp.send(null);	
}
	
function getGiftDenomination(id){
		
    xmlhttp=getobject();
    document.getElementById("giftDenomination").innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {
            //alert(xmlhttp.responseText);
            document.getElementById("giftDenomination").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?action=giftcertificate&type=getDenomination&id="+id,true);
    xmlhttp.send(null);	
}
	
	
	
	
///Start:=====Unused Functions====Unused Functions==Unused Functions=
	
function getAllCustomers(keyword)// Coupon Function 
{	
    xmlhttp=getobject();
    var query="keyword="+keyword+"&action=coupon";
    document.getElementById("divAllCustomers").innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {				
            document.getElementById("divAllCustomers").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=getAllCustomers&"+query,true);
    xmlhttp.send(null);	
}
	
function getAllProducts(keyword)// Coupon Function 
{	
    xmlhttp=getobject();
    var query="keyword="+keyword+"&action=coupon";
    document.getElementById("divAllProducts").innerHTML='<img src="images/loading_icon.gif">';
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {				
            document.getElementById("divAllProducts").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php?type=getAllProducts&"+query,true);
    xmlhttp.send(null);	
}
	
	///End:=====Unused Functions====Unused Functions==Unused Functions=

<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageView.php","add_record");
$genObj = new GeneralFunctions();
/*---Basic for Each Page Ends----*/
$viewObj = new View();
if(isset($_POST['submit'])) {

	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $viewObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' AND 	isDefault = '1'  order by languageName asc","","");		
	$num = $viewObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $viewObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('viewName_'.$value,$_POST['viewName_'.$value], 'req', 'Please Enter View Name');
		}
		
		$rst1 = $viewObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");
		$langIdArr1 = array();
		while($line1 = $viewObj->getResultObject($rst1)) {
			array_push($langIdArr1,$line->id);
		}
		$obj->fnAdd("price", $_POST["price"], "req", "Please enter the Price.");
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		$arr_error[price]=$obj->fnGetErr($arr_error[price]);
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['viewName_'.$value]=$obj->fnGetErr($arr_error['viewName_'.$value]);
		}
		
		foreach($langIdArr1 as $key=>$value) {
			if($viewObj->isViewExist($_POST['viewName_'.$value],$value))
			{ 
				$arr_error['viewName_'.$value] ='View Name Already Exists';
			}
		}	
		if($str_validate){
			$_POST = postwithoutspace($_POST);
			$viewObj->addRecord($_POST);
		}
	}
}
$maxSequence = $viewObj->findMaxSequence();
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageView.php';
}
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>View</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
		<input type="hidden" name="maxsequence" value="<?=$maxSequence ?>" />
		 <fieldset>  
            <label>Add View</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
                  <label>Name</label>
                  <div>
			<?=$genObj->getLanguageTextBoxEngValidation('viewName','m__Name',$arr_error);?>
		</div>
            </section>
        <section style="display:none;">
                  <label>Price</label>
		<div>
		<input type="text" name="price" value="1" maxlength="6" id="m__price" />
		<p><?=$arr_error[price] ?></p>
		</div>
		</section>
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>

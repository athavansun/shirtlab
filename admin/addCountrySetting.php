<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCountrySetting.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$countryObj = new CountrySetting();
if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();

	$rst = $countryObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' and isDefault ='1' order by languageName asc","","");		
	$num = $countryObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $countryObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		
		$obj->fnAdd('country',$_POST['country'], 'req','Please Select Country Name');
		$obj->fnAdd('currency',$_POST['currency'], 'req','Please Select Currency');
		$obj->fnAdd('vat',$_POST['vat'], 'req','Please Enter Vat in %');
		$obj->fnAdd('cDuty',$_POST['cDuty'], 'req','Please Enter Custom Duty in %');
		foreach($langIdArr as $key=>$value){                   
                    $obj->fnAdd('message_'.$value,$_POST['message_'.$value], 'req','Please Enter Buisness Rule');	
		}
				
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
				
		$arr_error['country']=$obj->fnGetErr($arr_error['country']);
		$arr_error['currency']=$obj->fnGetErr($arr_error['currency']);
		$arr_error['vat']=$obj->fnGetErr($arr_error['vat']);
		$arr_error['cDuty']=$obj->fnGetErr($arr_error['cDuty']);
		foreach($langIdArr as $key=>$value) {
            $arr_error['message_'.$value]=$obj->fnGetErr($arr_error['message_'.$value]);
		}	
                
		if(! count($_POST['lang']) > 0)
		{
			$arr_error[language] = '<span class="alert-red alert-icon">Please select Language(s).</span>';
			$str_validate = 0;
		}
		//print_r($arr_error);
		if($str_validate){
			//$_POST = postwithoutspace($_POST);
			$countryObj->addCountrySetting($_POST);
		}
	}
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!--  <script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script> 		 -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageCountrySetting.php';
}

</script>
<!-- New Drop Down menu -->

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Settings</h1><fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">		
		 <fieldset>  
            <label>Country Settting</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
               	<label>Select Country</label>
               	<div>
					<select name="country">
						<option value="">---Select Country---</option>
						<?=$countryObj->getCountry($_POST[country]);?>
					</select>
					<?=$arr_error[country]?>
               	</div>                  	           	
			</section>
			
			<section>
               	<label>Select Currency</label>
               	<div>
					<select name="currency">
						<option value="">---Select Currency---</option>
						<?=$countryObj->getCurrency($_POST['currency']);?>
					</select>
					<?=$arr_error[currency]?>
               	</div>                  	           	
			</section>
			<section>
				<label for="userEmail">Select Language's</label>
				<div>
					<?=$countryObj->getlangbox();?>
					<?=$arr_error[language]?>
				</div>
			</section>
			<section>
				  <label>Vat in %</label>
				  <div>
					<input type="text" name="vat" id="m__vat"  value="<?=stripslashes($_POST[vat])?>" style="width:155px" />
					<?=$arr_error[vat]?>
				  </div>					 
			</section> 		
			
			<section>
				  <label>Custom Duty in %</label>
				  <div>
					<input type="text" name="cDuty" id="m__cDuty"  value="<?=stripslashes($_POST[cDuty])?>" style="width:155px" />
					<?=$arr_error[cDuty]?>
				  </div>					 
			</section> 		
						
			<section>
                <label>Text Buisness Rules: </label>
                <div><?=$genObj->getLangEditorEngValidated('message','m__message',$arr_error);?></div>
            </section>  
            
			<section>
				  <label>EORI Custom-</label>
				  <div>
					<input type="radio" name="eori" id="m__eori" value="1" /><span style="margin-left:5px; font-size:13px">Yes</span>
					<input type="radio" name="eori" id="m__eori" value="0" checked="checked" style="margin-left:20px;" /><span style="margin-left:5px; font-size:13px">No</span>
				  </div>					 
			</section> 	
			
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form> </fieldset>
	</section>
	<? include_once('includes/footer.php');?>
<? unset($_SESSION['SESS_MSG']); ?>

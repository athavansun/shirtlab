<?php
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCountry.php","");
/*---Basic for Each Page Ends----*/

$countryObj = new Country();
$genObj = new GeneralFunctions();

$result = $countryObj->getResult(base64_decode($_GET['id']));

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>

</head>
<body>
<section id="content-detail">
     <form>
 	<fieldset>
    <label>Country Details</label>
 		  <!-- left position -->
          <div><?=$_SESSION['SESS_MSG']?></div>       
		
		 <section>
       	  <label for="country Code">Country Code (3)</label>
             <div><?=$result->countryCode?>
            </div>
        </section>
			 <section>
       	  <label for="country Code">Country Code (2)</label>
             <div><?=$result->country_2_code?>
            </div>
        </section>
		 <section>
       	  <label for="CountryName">Country Name</label>
             <div><?=$result->countryName?>
            </div>
        </section>
		
		<section>
       	  <label for="flag">Country Flag</label>
             <div>
   				<img src='<?=FLAGPATH.$result->flag?>'>
            </div>
          </section>
		
        
     </fieldset>       
     </form>  
    </section>        
</body>
</html>
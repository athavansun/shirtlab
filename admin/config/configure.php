<?php
$docRoot = $_SERVER['DOCUMENT_ROOT'];
$docRoot .= "/";
//$sitepath = "http://www.shirtlabweb.com/";
//$sitepath = "http://www.shirtlabweb.com/new/";
//$sitepath = "http://www.shirtlabweb.com/";
$sitepath = "http://localhost/";
$imageMagickPath  = "/usr/bin/convert";
$sitename = "Shirtlab";

define('__SITENAME__', $sitename);


// Local Database Settings 
//$config['server'] = 'localhost'; 
//$config['database'] = 'shirtl_shirtlab';
//$config['user'] ='shirtl_shirtlab';
//$config['pass'] ='shirtlab';
//
//$config['server'] = 'localhost'; 
//$config['database'] = 'shirtl_new_db';
//$config['user'] ='shirtl_shirtl';
//$config['pass'] ='wX7rbH$Z;WX{';
//no refresh 
$config['server'] = '192.168.3.64'; 
$config['database'] = 'shirtl_9';//'shirtl_shirtlab_9';
$config['user'] ='root';//'shirtl_shirtl_9';
$config['pass'] ='root';
//=================================================================

date_default_timezone_set('America/Los_Angeles');

define('CONFIGSERVER', $config['server']);
define('CONFIGDATABASE', $config['database']);
define('CONFIGUSER', $config['user']);
define('CONFIGPASS', $config['pass']);
define('CONFIGIMAGEMAGICPATH', $imageMagickPath);
define("ABSOLUTEPATH" , $docRoot);
$desOriginalImage  = "files/design/original";
$desThumbImage 	   = "files/design/thumb";
$desSwfImage 	   = "files/design/swf";
$desOrigPng 	   = "files/design/originaldesign";	//EPS
$desLargeThumb 	   = "files/design/largethumb";
define('DESORIGPNG',$desOrigPng);
$userUpdOriginalImage  	= "files/userimage/orignal";
$userUpdLargeImage 		= "files/userimage/large";
$userUpdThumbImage 		= "files/userimage/thumb";

//============pallet color image=============== 
define('__COLORTHUMB__',"files/color/thumb/");
define('__COLORORIGINAL__',"files/color/original/");

define('__GIFTLARGEIMAGE__',"files/giftCertificate/large/");
define('__GIFTTHUMBIMAGE__',"files/giftCertificate/thumb/");
define('__GIFTORIGINALIMAGE__',"files/giftCertificate/orignal/");
define('__GIFT_COUPON_IMAGE__', "files/giftCertificate/couponImage/");
define('__GIFT_IMAGE_CREATION__', "files/giftCertificate/imagesAndFontsForCouponImgCreation/");

//=====================DIR path of gift images=======================
define('__GIFT_TEMPLATE_ORIGINAL__',"files/giftCertificate/original/");
define('__GIFT_TEMPLATE_LARGE__',"files/giftCertificate/large/");
define('__GIFT_TEMPLATE_THUMB__',"files/giftCertificate/thumb/");
define('__GIFT_COUPON_IMAGE__',"files/giftCertificate/couponImage/");
define('__GIFT_IMAGE_CREATION__',"files/giftCertificate/imagesAndFontsForCouponImgCreation/");


define('__GIFT_TEMPLATE_THUMB_IMG__',$sitepath."files/giftCertificate/thumb/");

define('NOIMAGE',$sitepath.'files/noimage/noimage.jpeg');

define('IMAGEMAGICPATH', $imageMagickPath);
define('SITE_URL', $sitepath);
define('SEARCHTEXT', "Search");
define('DEFAULTDATEFORMAT', "d-m-Y");
define('__BASE_DIR_FRONT__', $docRoot); 
define('__BASE_DIR_ADMIN__', $docRoot.'admin/'); 
define('AMFPHPPATH', __BASE_DIR_ADMIN__."amfphp/services/");
define('BASEUPLOADFILE',$docRoot."files/");
define('BASEUPLOADFILEPATH',"../files/");
define('FLAGORIGINAL',BASEUPLOADFILE."flag/flagoriginal/");
define('FLAGTHUMB',BASEUPLOADFILE."flag/flagthumb/");
define('FLAGPATH',BASEUPLOADFILEPATH."flag/flagthumb/");
define('__NEWSLETTERORIGINAL__',BASEUPLOADFILE."newsletter/original/");
define('__NEWSLETTERPATH__',BASEUPLOADFILEPATH."newsletter/original/");

#---------language files---------
define('__DIR_LANGUAGES__', __BASE_DIR_FRONT__. 'languages/');

define('__EMAILFILE__',BASEUPLOADFILE."email/xls/");

define('__TESTIMONIALORIGINAL__',BASEUPLOADFILE."testimonial/original/");
define('__TESTIMONIALLARGE__',BASEUPLOADFILE."testimonial/large/");
define('__TESTIMONIALTHUMB__',BASEUPLOADFILE."testimonial/thumb/");
define('__TESTIMONIALPATH__',BASEUPLOADFILEPATH."testimonial/original/");

#--------color pallet image-------
define('__PALLETIMGORIG__',"files/color/original/");
define('__PALLETIMGTHUMB__',"files/color/thumb/");

#--------raw product image-------
define('__PRODUCTTHUMB__',"files/rawproduct/productimage/thumb60x60/");
define('__PRODUCTTHUMB_125x160_',"files/rawproduct/productimage/thumb125x160/");
define('__PRODUCTTHUMB_150x265_',"files/rawproduct/productimage/thumb150x265/");
define('__PRODUCTORIGINAL__',"files/rawproduct/productimage/original/");
define('__PRODUCTLARGE__',"files/rawproduct/productimage/large/");

#---------product zoom image------------
define('__ZOOMPRODUCTTHUMB__',"files/rawproduct/productimage/zoomImage/thumb/");
define('__ZOOMPRODUCTLARGE__',"files/rawproduct/productimage/zoomImage/large/");
define('__ZOOMPRODUCTORIGINAL__',"files/rawproduct/productimage/zoomImage/original/");
define('__ZOOMPRODUCTEXTRALARGE__',"files/rawproduct/productimage/zoomImage/extralarge/");

define('__PRODUCTTOOLTHUMB__',"files/rawproduct/toolImage/thumb/");
define('__PRODUCTTOOLORIGINAL__',"files/rawproduct/toolImage/original/");
define('__PRODUCTTOOLLARGE__',"files/rawproduct/toolImage/large/");

define('__PRODUCTBASICTHUMB__',"files/rawproduct/basicimage/thumb/");
define('__PRODUCTBASICORIGINAL__',"files/rawproduct/basicimage/original/");

define('__PRODUCTPARTTHUMB__',"files/rawproduct/productPart/thumb/");
define('__PRODUCTPARTORIGINAL__',"files/rawproduct/productPart/original/");
define('__PRODUCTPARTLARGE__',"files/rawproduct/productPart/large/");

define('__PRODUCTVIEWTHUMB__',"files/rawproduct/viewimage/thumb/");
define('__PRODUCTVIEWORIGINAL__',"files/rawproduct/viewimage/original/");

define('__PRODUCTPDF__',"files/rawproduct/pdf/");

#------page image--------
define('__PAGETHUMBIMAGE__',"files/pageimage/thumb/");
define('__PAGEORIGINALIMAGE__',"files/pageimage/original/");
define('__PAGELARGEIMAGE__',"files/pageimage/large/");
define('__PAGEEXTRALARGEIMAGE__',BASEUPLOADFILE."pageimage/extralarge/");
define('__PAGEEXTRALARGEIMAGEPATH__',BASEUPLOADFILEPATH."pageimage/extralarge/");

#------banner--------
define('__BANNERTHUMB__',"files/banner/thumb/");
define('__BANNERORIGINAL__',"files/banner/original/");
#-------deco upload-------
define('__DECOTHUMB__',"files/userdeco/thumb/");
define('__DECOORIGINAL__',"files/userdeco/original/");

#--------------define tempimage path==================
define('__TEMPIMAGE__',"files/tempImage/");


#--------------main product image path-----------
define('__MAINPRODTHUMB__',"files/mainproduct/thumb/");
define('__MAINPRODORIGINAL__',"files/mainproduct/original/");

define('__DECOPREFIX__',"org");

//~ define('__CATEGORYPATH__',BASEUPLOADFILEPATH."category/thumb/");
//~ define('__SITELOGOORIGINAL__',BASEUPLOADFILE."logo/original/");
//~ define('__SITELOGOPATH__',BASEUPLOADFILEPATH."logo/original/");
//~ define('__BACKGROUNDORIGINAL__',BASEUPLOADFILE."background/original/");
//~ define('__BACKGROUNDPATH__',BASEUPLOADFILEPATH."background/original/");
//~ define('__DIR_BACKUPS__', __BASE_DIR_ADMIN__ . 'db_backups/');
//~ define('__DIRFILE_BACKUPS__', 'db_backups/');
//~ define('__BANNERORIGINAL__',BASEUPLOADFILE."banner/original/");
//~ define('__BANNERPATH__',BASEUPLOADFILEPATH."banner/original/");
//~ define('__GIFTORIGINAL__',BASEUPLOADFILE."gift/original/");
//~ define('__GIFTTHUMB__',BASEUPLOADFILE."gift/thumb/");
//~ define('__GIFTTHUMBPATH__',BASEUPLOADFILEPATH."gift/thumb/");
//~ define('__BASEFAVICONPATH__',BASEUPLOADFILE."logo/original/");
//~ define('__BASEFAVICONURL__',SITE_URL."files/logo/original/");

?>

<?php
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');

$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manage-order.php","");
/*---Basic for Each Page Ends----*/
$orderObj = new Order();
 $id=$_REQUEST['id'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Welcome To <?=SITENAME?> administrative panel</title>
<!--				Light Box 	Start		-->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');
window.onload = function(){
	Shadowbox.init();
};
</script>
<script>
	function validate()
	{
		if(document.getElementById('file').value=="")
		{
			document.getElementById('file_error').style.display="";
			document.getElementById('file_error').innerHtml="Please Choose File";
			return false;
		}
		else
		{
			document.getElementById('file_error').style.display="none";
			}
	}
</script>
		<!--light box end-->
<?php    
    //echo $orderObj->getOrderDetail(($_GET['id']));
?> 


<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:12px;">
    <tbody>
        <tr>
            <td align="center" valign="top" style="padding:20px 0 20px 0">
            <table bgcolor="#FFFFFF" width="99%" cellspacing="0" cellpadding="10" border="0" style="border:1px solid #E0E0E0;">
                <!-- [ header starts here] -->
                <tbody>
                    <tr>
                        <td valign="top"><a href="index.php"><img border="0" style="margin-bottom:10px;" alt="" src="http://norefresh.thesparxitsolutions.com/NR193_NEW/images/logo.jpg"></a></td>
                    </tr>
                    <tr><td><?php echo $_REQUEST['show'];?></td></tr>
                    <tr>
						<td>
                    <!-- [ middle starts here] -->
                    <form action="pass.php?action=upload" enctype="multipart/form-data" method="post" onsubmit="return validate();">
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<th>Upload Image zip</th>
							<td><input type="file" name="file" id="file">(* zip file)<span id="file_error"></span></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="submit"></td>
						</tr>
					</table> 
				</form>
				</td>
				<tr>
                  
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
</div>

              
</body>
</html>

<?php

@ob_start();
@session_start();
if (!isset($_SESSION['DEFAULTLANGUAGECODE']))
{	
    $_SESSION['DEFAULTLANGUAGECODE'] = 'eng';
    $_SESSION['DEFAULTLANGUAGEID'] = 1;
    $_SESSION['DEFAULTCURRENCYID'] = 1;
}
require "../../../includes/function/autoload.php";
require "../../config/configure.php";
require "../../config/tables.php";
require "../../includes/JSON.php";

class onlineDesignerClient {

    // ========= db connection code (START) ========================
    var $host = CONFIGSERVER;
    var $usr = CONFIGUSER;
    var $pass = CONFIGPASS;
    var $db = CONFIGDATABASE;
    var $imageMagickPath = IMAGEMAGICPATH;

    function onlineDesignerClient() {
        $this->conn = mysql_connect($this->host, $this->usr, $this->pass);
        mysql_select_db($this->db, $this->conn);
        mysql_query('SET NAMES utf8');
    }

    function getProductCollections($name = '') {
        
        $xml = '';
        $xmlCl = '';
        $ctr = 1;
        $flag = true;
//        if ($_SESSION['USER_ID'] != '')
//            $cond = " userId = '" . $_SESSION['USER_ID'] . "'";
//        else
            $cond = " collectionName = '" . $name . "'";

        $query = "Select * from " . TBL_COLLECTION . " where" . $cond;
        $result = mysql_query($query);
        if (mysql_num_rows($result) > 0) { //for live
//        if ( $flag == false && mysql_num_rows($result) > 0) { //for test
            
            $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
            $xml .= '<collections>' . "\n";

            while ($line = mysql_fetch_object($result)) {

                $info = unserialize($line->prodInfo);
                if($flag == true) {
                    $xml .= '<products isDefault="' . $info->isProductDeafault . '">' . "\n";
                    $xmlCl .= '</products>' . "\n";
                    $flag = false;
                }
                
                $duplicate = ($line->isDuplicate == '0') ? 'false':'true';
                
                $xml .= '<product>
                                <sn></sn>
                                <id>' . $line->pId . '</id>
                                <rowProdId>' . $line->rawProdId . '</rowProdId>
                                <edit>true</edit>
                                <duplicate>'.$duplicate.'</duplicate>
                                <duplicateEdit>false</duplicateEdit>
                                <title>' . $this->fetchValue(TBL_PRODUCT_DESCRIPTION, 'productName', "pId = '" . $line->rawProdId . "' and langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "'") . '</title>
                                <ordercode1>' . $line->pId . '</ordercode1>
                                <ordercode2></ordercode2>
                                <thumb>' . __PRODUCTTHUMB_125x160_ . $this->fetchValue(TBL_PRODUCT, 'proImage', "id = '" . $line->rawProdId . "'") . '</thumb>';
                
                if($line->isDuplicate == '0')
                    $xml .= $this->getFabricQuality($line->rawProdId, $info);
                else
                    $xml .= $this->getFabricQualityForDuplicate($info->isQualityDefault, $info);
                
                $xml .= '</product>';
                $ctr ++;
            }
            if($ctr == 1)
            {
                $xml .= $this->appendProductToCollection('false'); // adding new product to collection
            }
            else
            {
                $xml .= $this->appendProductToCollection('true'); // append new product to collection
            }
            $xml .= $xmlCl;
            $xml .= '</collections>'."\n";
        }
        else
        {
            $xml .= $this->appendProductToCollection('false'); // adding new product to collection
        }
        return $xml;
    }
        
    function removeDecoFromAll($collection, $decoId) {
        $_SESSION['decoDel'] = 'collectionName= '. $collection .' designId= '. $decoId;
        $jsonObj = new JSON();
        $query = "select * from " . TBL_COLLECTION_DATA . " where collectionName = '" . $collection . "'";
        $result = mysql_query($query);
        if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_object($result)) {
                $dataArr = $jsonObj->decode($row->dataArr);
                if ($dataArr != '') {
                    $newDataArr = array();
                    foreach ($dataArr as $view) {
                        $viewObj = (object) $viewObj;
                        $viewObj->ViewName = $view->ViewName;
                        $viewObj->contentArray = array();
                        foreach ($view->contentArray as $deco) {
                            if ($deco != '') {
                                $newDeco = $this->removeDecoFromXml($deco, $decoId);
                                if (strlen($newDeco) > 5)
                                    $viewObj->contentArray[] = $newDeco;
                            }
                        }
                        $newDataArr[] = $viewObj;
                        unset($viewObj);
                    }
                    //----updating record---$newDataArr----
                    $strUpdate = "Update " . TBL_COLLECTION_DATA . " set dataArr = '" . $jsonObj->encode($newDataArr) . "' where id = '" . $row->id . "'";
                    $_SESSION['upQuery'][] = $strUpdate;
                    mysql_query($strUpdate);
                }
            }
            $returnTxt = 'ok';
        }

        //----updating decobasket----------
        $queryDeco = "select id, decoXml from " . TBL_COLLECTION_DECO . " where collectionName = '" . $collection . "'";
        $resultDeco = mysql_query($queryDeco);
        if (mysql_num_rows($resultDeco) > 0) {
            $rowDeco = mysql_fetch_object($resultDeco);
            if ($rowDeco != '') {
                $newDecoXml = $this->removeDecoFromXml($rowDeco->decoXml, $decoId, 'decoBasket');
                $newDecoXml = str_replace('<designs/>', '', $newDecoXml);

                //-----restoring deco data
                $decoUpdate = "update " . TBL_COLLECTION_DECO . " set decoXml = '" . mysql_real_escape_string($newDecoXml) . "' where id = '" . $rowDeco->id . "'";
                mysql_query($decoUpdate);
            }
        }
        $returnTxt = 'ok';
        return $returnTxt;
    }

    function getDecoBasket($collection) {
        $xml = '';
        $decoXml = $this->fetchValue(TBL_COLLECTION_DECO, 'decoXml', " collectionName = '" . $collection . "'");
        if ($decoXml != '') {
            $xml = $decoXml;
        } else {
            $xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
            $xml .= '<designs>' . "\n";
            $xml .= '</designs>' . "\n";
        }
        return $xml;
    }

    function getDesignById($collection, $pId, $sId) {
        $xml = '';
        $dataArr = $this->fetchValue(TBL_COLLECTION_DATA, 'dataArr', " collectionName = '" . $collection . "' and pId = '" . $pId . "' and sId = '" . $sId . "'");
        if ($dataArr != '') {
            $xml = $dataArr;
        }
        return $xml;
    }

    function saveProductCollection($encodeObj = '') {
        
        $status = (object) $status;
        $jsonObj = new JSON();
//        $_SESSION ['ecodedcolec'] = $encodeObj;
//        $encodeObj = $_SESSION['ecodedcolec'];
        $obj = $jsonObj->decode($encodeObj);
        $_SESSION ['colec'] = $obj;
        $curDate = date('Y-m-d');

        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';

        // ------removing if old collection exists--------
        $colllection = $this->fetchValue(TBL_COLLECTION, 'id', " collectionName = '" . $obj->collectionName . "'");
        if ($colllection) {
            $queryDel = "delete from " . TBL_COLLECTION . " where collectionName = '" . $obj->collectionName . "'";
            mysql_query($queryDel);
        }

        $ctr = 0;
        $pIdArr = $jsonObj->decode($obj->pidArray);
        $rIdArr = $jsonObj->decode($obj->ridArray);
        $proInfo = $jsonObj->decode($obj->infoArray);
//        echo '<pre>';
//        print_r($obj);
//        print_r($rIdArr);exit();
//        print_r($pIdArr);
        foreach ($pIdArr as $id) {
            $queryArr [] = " ('" . $userId . "', '" . mysql_real_escape_string($obj->collectionName) . "', " . $id . ", " . $rIdArr[$ctr] . ",'" . serialize($proInfo[$ctr]) . "','" . $curDate . "')";
            $ctr++;
        }
        $queryVal = implode(',', $queryArr);
        $queryCol = "insert into " . TBL_COLLECTION . " (userId, collectionName, pId, rawProdId, prodInfo, addDate) values" . $queryVal;
        //$_SESSION['coll'] = $queryCol;
        $conf = mysql_query($queryCol);
        if ($conf) {
            
            // ------removing old product Data if exists--------
            $proData = $this->fetchValue(TBL_COLLECTION_DATA, 'sId', " collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."' and  sId = '" . $obj->viewsInfo->styleId . "'");
            if ($proData) {
                $queryDel = "delete from " . TBL_COLLECTION_DATA . " where collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."' and sId = '" . $obj->viewsInfo->styleId . "'";
                mysql_query($queryDel);
            }

            $queryData = "insert into " . TBL_COLLECTION_DATA . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj->collectionName) . "', pId = '" . $obj->viewsInfo->productId . "', sId = " . $obj->viewsInfo->styleId . ", dataXml = '" . mysql_real_escape_string($obj->viewsInfo->prodViewsdata) . "', dataArr = '" . mysql_real_escape_string($obj->viewsInfo->dataArr) . "',infoArr = '" . mysql_real_escape_string($obj->viewsInfo->colorInfoArray) . "', decoInfo = '". mysql_real_escape_string($obj->viewsInfo->decoArr)."', addDate = '" . $curDate . "'";
            mysql_query($queryData);

            #------saving deco basket----------
            //------removing old deco basket-----
            $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " collectionName = '" . $obj->collectionName . "'");
            if ($deco) {
                $queryDel = "delete from " . TBL_COLLECTION_DECO . " where collectionName = '" . $obj->collectionName . "'";
                mysql_query($queryDel);
            }
            $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj->collectionName) . "', decoXml = '" . mysql_real_escape_string($obj->viewsInfo->decoBasket) . "', addDate = '" . $curDate . "'";
            mysql_query($queryDeco);
            
            //---------product's all image------
            $checkVal = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', " collectionName = '".$obj->collectionName."' and pId = '".$obj->viewsInfo->productId."'");
            if($checkVal)
            {
                $queryImage = "update ".TBL_COLLECTION_IMAGE." set prodViewImage = '". serialize($obj->viewsInfo->imgEncodeArray)."' where collectionName = '".$obj->collectionName."' and pId = '".$obj->viewsInfo->productId."'";
            }
            else
            {
                $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$obj->collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->rowProductId."', prodViewImage = '". serialize($obj->viewsInfo->imgEncodeArray)."', addDate = '".  date('Y-m-d')."'";
            }
            mysql_query($queryImage);
            
                                  
            
            $statusTxt = 'ok';
        } else
            $statusTxt = 'fail';
        
        if($obj->saveType == 'save')
            $url = 'myaccount.php?ref=saveDesign';
        else
            $url = '';
        
        $status->status = $statusTxt;
        $status->url = $url;
        
        return $status;
    }

    function getProductViews($sId, $isEdit = 'false', $collectionName = '', $colorType = 'Normal', $colorId = '',$pId = '') {
        $_SESSION ['param'] = 'sid= ' . $sId . ' edit= ' . $isEdit . ' collec= ' . $collectionName . 'colorType = ' . $colorType . 'colorId =' . $colorId.' pId='.$pId;
        $xml = '';
        if ($isEdit == 'true') {
            $xml = $this->fetchValue(TBL_COLLECTION_DATA, 'dataXml', "collectionName = '" . $collectionName . "' and pId = '".$pId."' and sId = '" . $sId . "'");
            if ($xml == '' && $colorType == 'Normal') {
                $xml = $this->getDefaultProductViews($sId); // if not then load the default views
            } else if ($xml == '' && $colorType == 'Special') {
                $xml = $this->getSpecialColProView($sId, $colorId);
            }
        } else if ($colorType == 'Normal') {
            $xml = $this->getDefaultProductViews($sId);
        }
        else
            $xml = $this->getSpecialColProView($sId, $colorId);

        return $xml;
    }

    function getSpecialColProView($sId, $colorId) {
        $xml = '';
        $xml .= '<xml>' . "\n";
        $xml .= '[sizeAttr]<price>0.00</price><views>' . "\n";
        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $sId . "'");
        $query = "select pt.*, vd.viewName from " . TBL_PRODUCT_TOOL . " as pt INNER JOIN " . TBL_VIEWDESC . " as vd ON (pt.viewId = vd.viewId) and vd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pt.pId = '" . $pId . "'";
        // echo $query; exit();
        $result = mysql_query($query);
        $flag = true;
        while ($line = mysql_fetch_object($result)) {
            $viewImage = $this->fetchValue(TBL_PRODUCT_VIEW, 'imageName', "pId = '" . $pId . "' and viewId = '" . $line->viewId . "'");
            if ($flag == true) {
                list ( $imgW, $imgH ) = getimagesize(ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $line->toolImage);
                $tmp = '<width>' . $imgW . '</width>
                                        <height>' . $imgH . '</height>';
                $flag = false;
            }
            $xml .= "\n" . '<view>
                                <title>' . $line->viewName . '</title>
                                <view>' . $line->viewName . '</view>
                                <subItemId>' . $line->viewId . '</subItemId>
                                <thumbnail>' . __PRODUCTVIEWTHUMB__ . $viewImage . '</thumbnail>						<url>' . SITE_URL . __PRODUCTTOOLLARGE__ . $line->toolImage . '</url>
                                <container>true</container>
                                <helpTip>' . $line->viewName . '</helpTip>
                                <ratio>1</ratio>
                                <price>0.00</price>
                                <x>' . $line->wAreaX . '</x>
                                <y>' . $line->wAreaY . '</y>
                                <width>' . $line->wAreaW . '</width>
                                <height>' . $line->wAreaH . '</height>
                                <images>' . "\n";
            // ---product part start--------
            $queryPart = "select psp.partImage, pd.partTitle from " . TBL_PRODUCT_SPECIAL_PART . " as psp INNER JOIN " . TBL_PARTDESC . " as pd ON (psp.titleId = pd.partId) and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and psp.sId = '" . $sId . "' and psp.viewId = '" . $line->viewId . "' and psp.colorId = '" . $colorId . "'";
            $hexVal = $this->fetchValue(TBL_SPECIAL_COLOR, 'colorCode', " id = '" . $colorId . "'");
            $resultPart = mysql_query($queryPart);
            $numPart = mysql_num_rows($resultPart);
            if ($numPart > 0) {
                while ($row = mysql_fetch_object($resultPart)) {
                    $colorable = 'false';
                    $xml .= '<image name="' . $row->partTitle . '" isColorable="' . $colorable . '" hexValue="' . substr($hexVal, 1) . '" source="' . __PRODUCTPARTLARGE__ . $row->partImage . '"/>' . "\n";
                }
            }

            // ---product part start--------
            $xml .= '</images>' . "\n";
            $xml .= '</view>' . "\n";
        }
        $xml .= '</views>' . "\n";
        $xml .= '</xml>' . "\n";

        $nXml = str_replace('[sizeAttr]', $tmp, $xml);

        return $nXml;
    }

    function getDefaultProductViews($sId) {
        $xml = '';
        $xml .= '<xml>' . "\n";
        $xml .= '[sizeAttr]<price>0.00</price><views>' . "\n";
        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $sId . "'");
        $query = "select pt.*, vd.viewName from " . TBL_PRODUCT_TOOL . " as pt INNER JOIN " . TBL_VIEWDESC . " as vd ON (pt.viewId = vd.viewId) and vd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pt.pId = '" . $pId . "'";
        // echo $query; exit();
        $result = mysql_query($query);
        $flag = true;
        while ($line = mysql_fetch_object($result)) {
            $viewImage = $this->fetchValue(TBL_PRODUCT_VIEW, 'imageName', "pId = '" . $pId . "' and viewId = '" . $line->viewId . "'");
            if ($flag == true) {
                list ( $imgW, $imgH ) = getimagesize(ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $line->toolImage);
                $tmp = '<width>' . $imgW . '</width>
					 <height>' . $imgH . '</height>';
                $flag = false;
            }
            $xml .= "\n" . '<view>
                                    <title>' . $line->viewName . '</title>
                                    <view>' . $line->viewName . '</view>
                                    <subItemId>' . $line->viewId . '</subItemId>
                                    <thumbnail>' . __PRODUCTVIEWTHUMB__ . $viewImage . '</thumbnail>                                                          <url>' . SITE_URL . __PRODUCTTOOLLARGE__ . $line->toolImage . '</url>
                                    <container>true</container>
                                    <helpTip>' . $line->viewName . '</helpTip>
                                    <ratio>1</ratio>
                                    <price>0.00</price>
                                    <x>' . $line->wAreaX . '</x>
                                    <y>' . $line->wAreaY . '</y>
                                    <width>' . $line->wAreaW . '</width>
                                    <height>' . $line->wAreaH . '</height>
                                    <printWidth>' . $line->printWidth . '</printWidth>
                                    <printHeight>' . $line->printHeight . '</printHeight>
                                    <images>' . "\n";
            // ---product part start--------
            $queryPart = "select pp.partImage, pp.isColorable, pd.partTitle from " . TBL_PRODUCT_PART . " as pp INNER JOIN " . TBL_PARTDESC . " as pd ON (pp.titleId = pd.partId) and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pp.sId = '" . $sId . "' and pp.viewId = '" . $line->viewId . "'";
            $resultPart = mysql_query($queryPart);
            $numPart = mysql_num_rows($resultPart);
            if ($numPart > 0) {
                while ($row = mysql_fetch_object($resultPart)) {
                    $colorable = $row->isColorable == '0' ? 'false' : 'true';
                    $xml .= '<image name="' . $row->partTitle . '" isColorable="' . $colorable . '" hexValue="" specialsource="' . __PRODUCTPARTLARGE__ . $row->partImage . '" source="' . __PRODUCTPARTLARGE__ . $row->partImage . '"/>' . "\n";
                }
            }

            // ---product part start--------
            $xml .= '</images>' . "\n";
            $xml .= '</view>' . "\n";
        }
        $xml .= '</views>' . "\n";
        $xml .= '</xml>' . "\n";

        $nXml = str_replace('[sizeAttr]', $tmp, $xml);

        return $nXml;
    }

    function getProductStyleById($pId, $qId, $collectionName,$duplicate = 'false') {
        
//        $_SESSION['style'] = 'pId='.$pId.' qId='.$qId.' collection='.$collectionName.' dupli='.$duplicate;
        $obj = (object) $obj;        
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $xml .= '<variants>' . "\n";
        
        if($duplicate == 'false') {
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and rawProdId = '".$pId."'");
            $query = "select ps.id, ps.styleCode, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "' and pf.fId = '" . $qId . "' order by ps.isBasic desc";

            $result = mysql_query($query);
            while ($line = mysql_fetch_object($result)) {
                $xml .= "\n" . '<variant>
                                        <id>' . $line->id . '</id>
                                        <thumb>' . __PRODUCTBASICTHUMB__ . $line->styleImage . '</thumb>
                                        <title1>' . $line->styleCode . '</title1>
                                        <title2></title2>
                                </variant>' . "\n";
            }
        }
        else
        {
            $jsonObj = new JSON();
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and pId = '".$pId."'");            
            $info = unserialize($infoArr);
            $sId = $info->isVarintDefault;
            $xml .= "\n" . '<variant>
                                <id>' . $sId . '</id>
                                <thumb>' . __PRODUCTBASICTHUMB__ . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleImage', " id = '".$sId."'") . '</thumb>
                                <title1>' . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '".$sId."'") . '</title1>
                                <title2></title2>
                        </variant>' . "\n";
        }
        $xml .= '</variants>' . "\n";
        
        
        $jsonObj = new JSON();
        if($infoArr != '')
        {   
            $infoObj = unserialize($infoArr);
            if($infoObj->sIdArray != '')
            {
                $sIdArr = explode(',',$infoObj->sIdArray);
                $resultInfo = $jsonObj->encode($sIdArr);
            }
            else
                $resultInfo = '';
        }
        else
            $resultInfo = '';
        
        
        $obj->result = $xml;
        $obj->info = $resultInfo;
        return $obj;        
    }
    
    function getColorPallet($sId, $qId, $collectionName) {
        $_SESSION['colorPallet'] = 'sId='.$sId.' qId='.$qId.' collection='.$collectionName;
        $obj = (object) $obj;
        $jsonObj = new JSON();
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $xml .= '<colors>' . "\n";
        $palletId = $this->fetchValue(TBL_FABRICCHARGE, 'palletId', "id = '" . $qId . "'");
        $queryColor = "select id, code, colorCode, colorImage from " . TBL_PALLETCOLOR . " where code !='' and colorCode !='' and palletId = '" . $palletId . "'";
        $resultColor = mysql_query($queryColor);        
        while ($row = mysql_fetch_object($resultColor)) {
            if ($row->colorImage == '')
                $image = '';
            else
                $image = __PALLETIMGTHUMB__ . $row->colorImage;
            $xml .= "\n" . '<color>
				<id>' . $row->id . '</id>
				<type>Normal</type>
				<title1>' . $row->code . '</title1>
				<value>' . str_replace('#', '', $row->colorCode) . '</value>
				<url>' . $image . '</url>
			</color>' . "\n";                       
        }
        $xml .= $this->getSpecialColor($sId);
        $xml .= '</colors>' . "\n";
        $obj->result = $xml;        
        $rowInfo = $this->fetchValue(TBL_COLLECTION_DATA, 'infoArr', " collectionName = '".$collectionName."' and sId = '".$sId."'");
//        echo "<pre>";
//        print_r($rowInfo);
        if($rowInfo == '')
        {
            $tmpArr = (object) $tmpArr;
            $tmpArr->colorId = '';
            $tmpArr->coloringStyle = '';
            $tmpArr->colorType = '';
            
            //$rowInfo = $jsonObj->encode($tmpArr);
            $obj->info = $tmpArr;
            unset($tmpArr);
        }
        else            
            $obj->info = $jsonObj->decode($rowInfo);
        
        return $obj;
    }
    
    function getSpecialColor($sId) {
        $xml = '';
        $query = "select sc.*, psp.colorId from " . TBL_SPECIAL_COLOR . " as sc INNER JOIN " . TBL_PRODUCT_SPECIAL_PART . " as psp ON (sc.id = psp.colorId) and psp.sId = '" . $sId . "' and sc.isDeleted = '0' group by psp.colorId";

        $result = mysql_query($query);
        $num = mysql_num_rows($result);
        if ($num > 0) {
            while ($row = mysql_fetch_object($result)) {
                $xml .= "\n" . '<color>
						<id>' . $row->id . '</id>
						<type>Special</type>
						<title1>' . $row->code . '</title1>
						<value>' . str_replace('#', '', $row->colorCode) . '</value>
						<url>' . __PALLETIMGTHUMB__ . $row->colorImage . '</url>
					</color>' . "\n";
            }
        }
        return $xml;
    }

    

    function appendProductToCollection($type) {

        $xml = '';
        $xml1 = '';
        $xmlCl = '';
        $flag = true;
        if(count($_SESSION['productId']) > 0) //if(1)//
        {
            if($type == 'false')
            {            
                $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
                $xml .= '<collections>' . "\n";
                $xmlCl .= '</collections>' . "\n";
            }
            $productId = implode(',',$_SESSION['productId']);//'1,2'; // 
            $query = "Select p.id, p.proImage, pd.productName from " . TBL_PRODUCT . " as p INNER JOIN " . TBL_PRODUCT_DESCRIPTION . " as pd ON (p.id = pd.pId) where p.id in (" . $productId . ") and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' order by p.id";
            $result = mysql_query($query);

            while ($line = mysql_fetch_object($result)) {
                if ($flag == true && $type == 'false') {
                    $xml .= '<products isDefault="' . $line->id . '">' . "\n";
                    $xml1 = '</products>'."\n";
                    $flag = false;
                }
                $xml .= '<product>
                                <sn></sn>
                                <id>' . $line->id . '</id>
                                <rowProdId>' . $line->id . '</rowProdId>
                                <edit>false</edit>
                                <duplicate>false</duplicate>
                                <duplicateEdit>false</duplicateEdit>
                                <title>' . $line->productName . '</title>
                                <ordercode1>' . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " pId = '" . $line->id . "' and isBasic = '1'") . '</ordercode1>
                                <ordercode2></ordercode2>
                                <thumb>' . __PRODUCTTHUMB_125x160_ . $line->proImage . '</thumb>';
                $xml .= $this->getFabricQuality($line->id, '');
                $xml .= '</product>';
//                $ctr++;
            }
            $xml .= "\n" . $xml1 . "\n";
            $xml .= $xmlCl;
            unset($_SESSION ['productId']);
        }
        return $xml;
    }
    
    function getFabricQualityForDuplicate($qId, $default) {
        
        $xml = '';
        $quality = $this->fetchValue(TBL_FABRICCHARGE, 'fQuality', "id = '".$qId."'");
        $xml .= "\n" . '<fabricquality isDefault="' . $default->isQualityDefault . '" isVarintDefault="' . $default->isVarintDefault . '" colorType="' . $default->colorType . '" colorId="' . $default->colorId . '" coloringStyle="' . $default->coloringStyle . '" sIdArray="'.$default->sIdArray.'">' . "\n";

        $xml .= "\n" . '<quality> 
                            <id>' . $qId . '</id> 
                            <value>' . $quality . '</value>
                    </quality>' . "\n";

        $xml .= '</fabricquality>'. "\n";
        
        return $xml;
    }
    
    function getFabricQuality($pId, $default = '') {
        $xml = '';
        $queryFab = "select distinct(pf.fId), fc.fQuality from " . TBL_PRODUCT_FABRIC . " as pf INNER JOIN " . TBL_FABRICCHARGE . " as fc ON(pf.fId = fc.id) and pf.pId = '" . $pId . "' order by fc.fQuality";
        $res = mysql_query($queryFab);
        $numFab = mysql_num_rows($res);
        $qflag = true;
        if ($numFab > 0) {
            while ($row = mysql_fetch_object($res)) {
                if ($qflag == true) {
                    if ($default == '') {
                        $xml .= "\n" . '<fabricquality isDefault="' . $row->fId . '" isVarintDefault="" colorType="" colorId="" coloringStyle="" sIdArray="">' . "\n";
                    } else {
                        $xml .= "\n" . '<fabricquality isDefault="' . $default->isQualityDefault . '" isVarintDefault="' . $default->isVarintDefault . '" colorType="' . $default->colorType . '" colorId="' . $default->colorId . '" coloringStyle="' . $default->coloringStyle . '" sIdArray="'.$default->sIdArray.'">' . "\n";
                    }
                    $qflag = false;
                }
                $xml .= "\n" . '<quality> 
                                        <id>' . $row->fId . '</id> 
                                        <value>' . $row->fQuality . '</value>
                                </quality>' . "\n";
            }
        }
        $xml .= '</fabricquality>';

        return $xml;
    }

    function getCollectionName() {  // -------returns colection name-----
        if (isset($_COOKIE ['collection'])) {
            $collectionName = $_COOKIE ['collection'];
        } else {
            $collectionName = time() . rand();
            setcookie('collection', $collectionName, time() + 60 * 60 * 24 * 30, '/');
        }
        return $collectionName;
    }

    function fetchValue($tbl, $field, $con) {
        $sql = "select " . $field . " from " . $tbl . " where " . $con;
        $result = mysql_query($sql) or die(mysql_error());
        $rec = mysql_fetch_object($result);
        $val = $rec->$field;
        return $val;
    }

    function removeDecoFromXml($deco, $decoId, $type = '') {
        if ($deco != '') {
            if ($type == '') {
                $xmlString = '<design><deco>' . $deco . '</deco></design>';
                $designXml = new SimpleXMLElement($xmlString);
                if ($designXml->deco->designId == $decoId) {
                    unset($designXml->deco);
                    $decoTmp = $designXml->asXML();
                } else {
                    $decoTmp = $designXml->asXML();
                }
                $find = array('<?xml version="1.0"?>', '<design>', '<deco>', '</deco>', '</design>');
                $decoXml = str_replace($find, '', $decoTmp);
            } else {
                $designXml = new SimpleXMLElement($deco);
                $decoArr = array();
                foreach ($designXml->deco as $design) {
                    if ($design->designId == $decoId)
                        unset($design->deco);
                    else
                        $decoArr[] = $design->asXML();

                    unset($design);
                }

                $decoTmp = implode('', $decoArr);
                $decoTmp = '<designs>' . $decoTmp . '</designs>';
                $decoXml = str_replace('<?xml version="1.0"?>', '', $decoTmp);
            }
        }
        $decoXml = str_replace('<design/>', '', $decoXml);
        $breaks = array("\r\n", "\n", "\r", '  ');
        $newtext = str_replace($breaks, "", $decoXml);
        return $newtext;
    }
    
    function getCollectionOverview($obj = '', $display = 'true') {
        $xml = '';
//        $_SESSION['overview'] = $obj;
//        $obj = $_SESSION['overview'];
        $jsonObj = new JSON();
        $pIdArr = $jsonObj->decode($obj['pidArray']);
        $rIdArr = $jsonObj->decode($obj['ridArray']);
        $queryColl = "select pId from ".TBL_COLLECTION_IMAGE." where collectionName = '".$obj['collectionName']."'";
        $result = mysql_query($queryColl);
        $num = mysql_num_rows($result);
        if($num == 0)
        {
            $ctr = 0;
            $queryValArr = array();
            foreach ($pIdArr as $pId)
            {
                $queryValArr[] = "('".$obj['collectionName']."','".$pId."', '".$rIdArr[$ctr]."', '".date('Y-m-d')."')";
                $ctr ++;
            }
            $queryVal = implode(', ', $queryValArr);
            $query = "insert into ".TBL_COLLECTION_IMAGE." (collectionName, pId, rId, addDate) values ".$queryVal;
            mysql_query($query);
        }
        else
        {
            $queryValArr = array();
            $ctr = 0;
            foreach ($pIdArr as $pId)
            {
                $check = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', " collectionName = '".$obj['collectionName']."' and pId = '".$pId."'");
                if(! $check)
                {
                    $queryValArr[] = "('".$obj['collectionName']."','".$pId."', '".$rIdArr[$ctr]."', '".date('Y-m-d')."')";
                }
                $ctr ++;
            }
            if(count($queryValArr) > 0)
            {
                $queryVal = implode(', ', $queryValArr);
                $query = "insert into ".TBL_COLLECTION_IMAGE." (collectionName, pId, rId, addDate) values ".$queryVal;
                mysql_query($query);
            }
        }
                       
        //------updating --------
        $queryUpdate = "update ".TBL_COLLECTION_IMAGE." set encodeString = '".mysql_real_escape_string($obj['imgInfo']['imgIncoded'])."' where collectionName = '".$obj['collectionName']."' and pId = '".$obj['imgInfo']['pId']."'";
        $conf = mysql_query($queryUpdate);
        
        
        //------fetching image for preview-------
        if($conf && $display == 'true')
        {
            //--------update sequence--------
            $snArr = $jsonObj->decode($obj['snArray']);
            foreach ($snArr as $sn) {
                $query = "update " . TBL_COLLECTION_IMAGE . " set sequence = '" . $sn->sn . "' where collectionName = '" . $obj['collectionName'] . "' and pId = '" . $sn->pid . "'";
                mysql_query($query);
            }
            
            $queryImage = "select pId, rId, encodeString, sequence from ".TBL_COLLECTION_IMAGE." where collectionName = '".$obj['collectionName']."' order by sequence";
            $resultImage = mysql_query($queryImage);
            if(mysql_num_rows($resultImage) > 0)
            {
                $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
                $xml .= '<images>' . "\n";
                while($rowImage = mysql_fetch_object($resultImage))
                {                    
                    $xml .= '<image>'."\n";                    
                    $xml .= '<id>'.$rowImage->pId.'</id>'. "\n";
                    $xml .= '<rId>'.$rowImage->rId.'</rId>'. "\n";
                    $xml .= '<sn>'.$rowImage->sequence.'</sn>'. "\n";
                    $xml .= '<url>'.$this->getImage($rowImage->rId, $rowImage->encodeString).'</url>'. "\n";
                    $xml .= '<edit>true</edit>'. "\n";
                    $xml .= '</image>'."\n";
                }
                $xml .= '</images>' . "\n";
                
            }            
        }        
        return $xml;        
    }
    
    function deleteProductPreview($collection, $pId) {
        
        //------delete product from collection--------
        $check = $this->fetchValue(TBL_COLLECTION, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if($check)
        {
            mysql_query("delete from ".TBL_COLLECTION." where collectionName = '".$collection."' and pId = '".$pId."'");
        }
        
        //---------delete record from collection data------
        $checkData = $this->fetchValue(TBL_COLLECTION_DATA, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if($checkData)
        {
            mysql_query("delete from ".TBL_COLLECTION_DATA." where collectionName = '".$collection."' and pId = '".$pId."'");
        }
        
        $val = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if ($val) {
            $query = "delete from " . TBL_COLLECTION_IMAGE . " where collectionName = '" . $collection . "' and pId = '" . $pId . "'";
            mysql_query($query);
        }
        return 'ok';
    }
    
    function getImage($rId, $image) {
        $imageName = __TEMPIMAGE__ . time() . rand() . ".png";
        if ($image != '') { //---showing from image string
            $imageStr = base64_decode($image);
            $fp = fopen(ABSOLUTEPATH . $imageName, 'w');
            fwrite($fp, $imageStr);
            fclose($fp);
            $file = $imageName;
        } else { //------showing the default view image
            $query = "select imageName from " . TBL_PRODUCT_VIEW . " where pId = '" . $rId . "'";
            $resultSet = mysql_query($query);
            if (mysql_num_rows($resultSet) > 0) {
                $imageArr = array();
                while ($line = mysql_fetch_object($resultSet)) {
                    $imageArr[] = ABSOLUTEPATH . __PRODUCTVIEWTHUMB__ . $line->imageName;
                }

                $c = count($imageArr);
                if ($c > 0) {
                    $imgArr = array_chunk($imageArr, '4');
                    $ctr = 1;
                    $tmpImgArr = array();

                    foreach ($imgArr as $image) {
                        $str = implode(' ', $image);

                        $tmpImage = ABSOLUTEPATH . __TEMPIMAGE__ . time() . rand() . $ctr . ".png";
                        $tmpImgArr[] = $tmpImage;
                        exec("convert  $str  +append -trim $tmpImage");
                        @chmod($tmpImage, 0777);
                        $ctr++;
                    }


                    $strComp = '-size 1500x1500 xc:none ';
                    if (count($tmpImgArr) > 0) {
                        $i = 0;
                        $y = 0;

                        $tHeight = 0;
                        foreach ($tmpImgArr as $tmpValue) {
                            list($width, $height) = getimagesize($tmpValue);

                            if ($i > 0) {
                                $y+=$height + 10;
                            }

                            $strComp.=' "' . $tmpValue . '" -geometry +0+' . $y . ' -composite ';
                            $i++;

                            $tHeight += $height;
                        }

                        exec('convert ' . $strComp . ' -trim "' . ABSOLUTEPATH . $imageName . '"');
                        exec('convert ' . ABSOLUTEPATH . $imageName . ' -resize 280x' . $tHeight . ' "' . ABSOLUTEPATH . $imageName . '"');
                    }
                }
            }
            $file = $imageName;
        }
        return $file;
    }
    
    function updateDecoBasket($obj) {
        $curDate = date('Y-m-d');
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '0';
        #------saving deco basket----------
        //------removing old deco basket-----
        $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " userId = '" . $userId . "' and collectionName = '" . $obj['collectionName'] . "'");
        if ($deco) {
            $queryDel = "delete from " . TBL_COLLECTION_DECO . " where userId = '" . $userId . "' and collectionName = '" . $obj['collectionName'] . "'";
            mysql_query($queryDel);
        }
        $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj['collectionName']) . "', decoXml = '" . mysql_real_escape_string($obj['decoBasket']) . "', addDate = '" . $curDate . "'";
        mysql_query($queryDeco);
    }
        
    function duplicateProduct($obj = '') {

        $jsonObj = new JSON();
        $status = (object) $status;
        $obj = $jsonObj->decode($obj);
//        $_SESSION['duplicate'] = $obj;
//        $obj = $_SESSION['duplicate'];        

        $pId = time();
        $curDate = date('Y-m-d');
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';
        
        $infoArr = $jsonObj->decode($obj->infoArray);
        //print_r($infoArr[0]);        
        
        //-----collection--------
        $query = "insert into ".TBL_COLLECTION." set userId = '".$userId."', collectionName = '".$obj->collectionName."', pId = '".$pId."', rawProdId = '".$obj->viewsInfo->productId."', prodInfo = '".serialize($infoArr[0])."', isDuplicate = '1', addDate = '".$curDate."'";
        
        $conf = mysql_query($query);
        if($conf)
        {
            //-------collection data-------
            $queryData = "insert into ".TBL_COLLECTION_DATA." set userId = '".$userId."', collectionName = '".$obj->collectionName."', pId = '".$pId."', sId = '".$obj->viewsInfo->styleId."', dataXml = '".mysql_real_escape_string($obj->viewsInfo->prodViewsdata)."', dataArr = '".mysql_real_escape_string($obj->viewsInfo->dataArr)."', infoArr = '".mysql_real_escape_string($obj->viewsInfo->colorInfoArray)."', addDate = '".$curDate."'";
            mysql_query($queryData);
            
            //---------update deco if exists---------
            if($obj->viewsInfo->decoBasket != '')
            {
                //------removing old deco basket-----
                $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " userId = '" . $userId . "' and collectionName = '" . $obj->collectionName . "'");
                if ($deco) {
                    $queryDel = "delete from " . TBL_COLLECTION_DECO . " where userId = '" . $userId . "' and collectionName = '" . $obj->collectionName . "'";
                    mysql_query($queryDel);
                }
                $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj->collectionName) . "', decoXml = '" . mysql_real_escape_string($obj->viewsInfo->decoBasket) . "', addDate = '" . $curDate . "'";
                mysql_query($queryDeco);
            }
            
            //------image code for original--------
            $checkRecord = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', "collectionName = '".$obj->collectionName."' and pId = '".$obj->viewsInfo->productId."'");
            if($checkRecord)
            {
                $queryImage = "update ".TBL_COLLECTION_IMAGE." set encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."' where collectionName = '".$obj->collectionName."' and pId = '".$obj->viewsInfo->productId."'";
                mysql_query($queryImage);
            }
            else
            {
                $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$obj->collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->productId."', encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."', addDate = '".  date('Y-m-d')."'";
                mysql_query($queryImage);
            }
            
            //------image code for duplicate--------
            $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$obj->collectionName."', pId = '".$pId."', rId = '".$obj->viewsInfo->productId."', encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."', addDate = '".  date('Y-m-d')."'";
            mysql_query($queryImage);            
            
            
            //-----making product collection--------
            $xml = '';
            $xml .='<product>
			<sn></sn>
                        <id>' . $pId . '</id>
                        <rowProdId>' . $obj->viewsInfo->productId . '</rowProdId>
                        <edit>true</edit>
                        <duplicate>true</duplicate>
                        <duplicateEdit>true</duplicateEdit>
                        <title>' . $this->fetchValue(TBL_PRODUCT_DESCRIPTION, 'productName', "pId = '" . $obj->viewsInfo->productId . "' and langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "'") . '</title>
                        <ordercode1>' . $obj->viewsInfo->productId . '</ordercode1>
                        <ordercode2></ordercode2>
                        <thumb>' . __PRODUCTTHUMB_125x160_ . $this->fetchValue(TBL_PRODUCT, 'proImage', "id = '" . $obj->viewsInfo->productId . "'") . '</thumb>';
            
            
        
            $qId = $obj->viewsInfo->qualityId;
            $infoArr = $jsonObj->decode($obj->infoArray);
            $default = $infoArr[0];
            
            $xml .= $this->getFabricQualityForDuplicate($qId, $default);
            $xml .= '</product>';
            
            $status->status = '1';
            $status->xml = $xml;
            
        }
        else
            $status->status = '0';
        
        return $status;
        
    }
    
    function sendDataToPHP($cName = '') {
        
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';
        $query = "select CI.pId, CI.rId, CI.prodViewImage, CD.sId, CD.dataArr, CD.decoInfo from ".TBL_COLLECTION_IMAGE. " as CI INNER JOIN ".TBL_COLLECTION_DATA." as CD ON (CI.collectionName = CD.collectionName) and (CI.pId = CD.pId) and CI.collectionName = '".$cName."'";
        
        $result = mysql_query($query);
        $num = mysql_num_rows($result);
        if($num > 0)
        {
            while ($row = mysql_fetch_object($result))
            {
                $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$cName."' and pId = '".$row->pId."'");
                $info = unserialize($infoArr);
                
                $categoryId = $this->fetchValue(TBL_PRODUCT, 'catId', " id = '".$row->rId."'");
                $queryProd = "insert into ".TBL_MAINPRODUCT." set rawProdId = '".$row->rId."', styleId = '".$row->sId."', qualityId = '".$info->isQualityDefault."', categoryId = '".$categoryId."', status = '1', isDeleted = '0', addDate = '".date('Y-m-d')."', addedBy = '".$userId."', dataArr = '".mysql_real_escape_string($row->dataArr)."', decoInfo = '".mysql_real_escape_string($row->decoInfo)."', isCustomizable = '0'";
                $conf = mysql_query($queryProd);
                if($conf)
                {
                    $pId = mysql_insert_id();
                    
                    //-------storing product views and image-----
                    if($row->prodViewImage != '')
                    {
                        $viewArray = unserialize($row->prodViewImage);
                        $viewVal = array();
                        foreach ($viewArray as $view)
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $fp = fopen(ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName, 'w');
                            $image = base64_decode($view->imgEncodd);
                            fwrite($fp, $image);
                            fclose($fp);

                            $target = ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName;

                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");

                            $viewVal[] = " ('".$pId."', '".$view->viewId."', '".$imageName."')";
                        }
                    }
                    else
                    {
                        $getView = "select viewId, imageName from ".TBL_PRODUCT_VIEW." where pId = '".$row->rId."'";
                        $viewResult = mysql_query($getView);
                        $viewVal = array();
                        while ($line = mysql_fetch_object($viewResult))
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $defImage = $line->imageName;
                            $target = ABSOLUTEPATH.__PRODUCTVIEWORIGINAL__.$defImage;
                            
                            copy($target, ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName);
                            
                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");
                            
                            $viewVal[] = " ('".$pId."', '".$line->viewId."', '".$imageName."')";
                        }
                    }
                    if(count($viewVal) > 0)
                    {
                        $queryVal = implode(' ,', $viewVal);
                        $queryView = "insert into ".TBL_MAINPRODUCT_VIEW." (mainProdId, viewId, viewImage) values ".$queryVal;
                        mysql_query($queryView);
                    }
                    
                    //----------adding product to basket-------
                    $this->addToBasket($pId, $row->rId, $info->isQualityDefault);                    
                }
            }
            $returnTxt = 'ok';
        }
        else            
            $returnTxt = 'fail';
        
        return $returnTxt;
    }
    
    
    function addToBasket($pId, $rId, $qId) {        
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';
        $sessionId = session_id();
        $query = "insert into ".TBL_BASKET." set sessionId = '".$sessionId."', userId = '".$userId."', rawProductId = '".$rId."',qualityId = '".$qId."', mainProdId = '".$pId."'";
        mysql_query($query);
    }
    
}
 
// $myobj = new onlineDesignerClient();
// $tmp = $myobj->sendDataToPHP('13581388581978745604');
// echo "<pre>";
// print_r($tmp);
?>
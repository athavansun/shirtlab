<?php
@ob_start();
@session_start();
if (!isset($_SESSION['DEFAULTLANGUAGECODE']))
{	
    $_SESSION['DEFAULTLANGUAGECODE'] = 'eng';
    $_SESSION['DEFAULTLANGUAGEID'] = 1;
    $_SESSION['DEFAULTCURRENCYID'] = 1;
}
require "../../../includes/function/autoload.php";
require "../../config/configure.php";
require "../../config/tables.php";
require "../../includes/JSON.php";

class onlineDesignerClient {

    // ========= db connection code (START) ========================
    var $host = CONFIGSERVER;
    var $usr = CONFIGUSER;
    var $pass = CONFIGPASS;
    var $db = CONFIGDATABASE;
    var $imageMagickPath = IMAGEMAGICPATH;

    function onlineDesignerClient() {
        
        $this->conn = mysql_connect($this->host, $this->usr, $this->pass);
        mysql_select_db($this->db, $this->conn);
        mysql_query('SET NAMES utf8');
    }

    function getProductCollections($name = '') {
        $xml = '';
        $xmlCl = '';
        $ctr = 1;
        $flag = true;
        
//        if ($_SESSION['USER_ID'] != '')
//            $cond = " userId = '" . $_SESSION['USER_ID'] . "'";
//        else
        
        $cond = ' collectionName = "' . $name . '" order by id asc';        
        $query = "Select * from " . TBL_COLLECTION . " where" . $cond;
        
        $result = mysql_query($query);
        $num = mysql_num_rows($result);

        if ($num > 0) { //for live
            $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
            $xml .= '<collections>' . "\n";            

            while ($line = mysql_fetch_object($result)) {
                $_SESSION['dupli_test_data'] = $line;
                $info = unserialize($line->prodInfo);
                if($flag == true) {
                    $xml .= '<products isDefault="' . $info->isProductDeafault . '">' . "\n";
                    $xmlCl .= '</products>' . "\n";
                    $flag = false;
                }
                
                $duplicate = ($line->isDuplicate == '0') ? 'false':'true';
                
                //23Oct2013
//                $xml .= '<product>
//                                <sn></sn>
//                                <id>' . $line->pId . '</id>
//                                <rowProdId>' . $line->rawProdId . '</rowProdId>
//                                <edit>true</edit>
//                                <duplicate>'.$duplicate.'</duplicate>
//                                <duplicateEdit>false</duplicateEdit>
//                                <title>' . $this->fetchValue(TBL_PRODUCT_DESCRIPTION, 'productName', "pId = '" . $line->rawProdId . "' and langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "'") . '</title>
//                                <ordercode1>' . $line->pId . '</ordercode1>
//                                <ordercode2></ordercode2>
//                                <thumb>' . __PRODUCTTHUMB_125x160_ . $this->fetchValue(TBL_PRODUCT, 'proImage', "id = '" . $line->rawProdId . "'") . '</thumb>';
                
                               $xml .= '<product>
                                <sn></sn>
                                <id>' . $line->pId . '</id>
                                <rowProdId>' . $line->rawProdId . '</rowProdId>
                                <edit>true</edit>
                                <duplicate>'.$duplicate.'</duplicate>
                                <duplicateEdit>false</duplicateEdit>                                
                                <ordercode1>' . $line->pId . '</ordercode1>
                                <ordercode2></ordercode2>
                                <thumb>' . __PRODUCTTHUMB_125x160_ . $this->fetchValue(TBL_PRODUCT, 'proImage', "id = '" . $line->rawProdId . "'") . '</thumb>';
 
                

//                if($line->isDuplicate == '0')
//                    $xml .= $this->getFabricQuality($line->rawProdId, $info);
//                else
//                    $xml .= $this->getFabricQualityForDuplicate($info->isQualityDefault, $info);
                if($line->isDuplicate == '0') {
                    if($line->rawProdId == $line->pId) {
                        $xml .= $this->getFabricQuality($line->rawProdId, $info);
                    } else {
                        $xml .= $this->getFabricQualityForDuplicate($info->isQualityDefault, $info);   
                    }
                } else {
                     $xml .= $this->getFabricQualityForDuplicate($info->isQualityDefault, $info);
                }
                
                $xml .= '</product>';
                $ctr ++;
            }
            if($ctr == 1)
            {
                $xml .= $this->appendProductToCollection('false'); // adding new product to collection
            }
            else
            {
                $xml .= $this->appendProductToCollection('true'); // append new product to collection
            }
            $xml .= $xmlCl;
            $_SESSION['collectionxmltest'] = $xml;
            $xml .= '</collections>'."\n";
        }
        else
        {
            $xml .= $this->appendProductToCollection('false'); // adding new product to collection
        }
        $_SESSION['collectionxmltest'] = $xml;
        return $xml;
    }
        
    function removeDecoFromAll($collection, $decoId) {
        $_SESSION['decoDel'] = 'collectionName= '. $collection .' designId= '. $decoId;
        $jsonObj = new JSON();
        $query = "select * from " . TBL_COLLECTION_DATA . " where collectionName = '" . $collection . "'";
        $result = mysql_query($query);
        if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_object($result)) {
                $dataArr = $jsonObj->decode($row->dataArr);
                if ($dataArr != '') {
                    $newDataArr = array();
                    foreach ($dataArr as $view) {
                        $viewObj = (object) $viewObj;
                        $viewObj->ViewName = $view->ViewName;
                        $viewObj->contentArray = array();
                        foreach ($view->contentArray as $deco) {
                            if ($deco != '') {
                                $newDeco = $this->removeDecoFromXml($deco, $decoId);
                                if (strlen($newDeco) > 5)
                                    $viewObj->contentArray[] = $newDeco;
                            }
                        }
                        $newDataArr[] = $viewObj;
                        unset($viewObj);
                    }
                    //----updating record---$newDataArr----
                    $strUpdate = "Update " . TBL_COLLECTION_DATA . " set dataArr = '" . $jsonObj->encode($newDataArr) . "' where id = '" . $row->id . "'";
                    $_SESSION['upQuery'][] = $strUpdate;
                    mysql_query($strUpdate);
                }
            }
            $returnTxt = 'ok';
        }

        //----updating decobasket----------
        $queryDeco = "select id, decoXml from " . TBL_COLLECTION_DECO . " where collectionName = '" . $collection . "'";
        $resultDeco = mysql_query($queryDeco);
        if (mysql_num_rows($resultDeco) > 0) {
            $rowDeco = mysql_fetch_object($resultDeco);
            if ($rowDeco != '') {
                $newDecoXml = $this->removeDecoFromXml($rowDeco->decoXml, $decoId, 'decoBasket');
                $newDecoXml = str_replace('<designs/>', '', $newDecoXml);

                //-----restoring deco data
                $decoUpdate = "update " . TBL_COLLECTION_DECO . " set decoXml = '" . mysql_real_escape_string($newDecoXml) . "' where id = '" . $rowDeco->id . "'";
                mysql_query($decoUpdate);
            }
        }
        $returnTxt = 'ok';
        return $returnTxt;
    }

    function getDecoBasket($collection) {
        $xml = '';
        $decoXml = $this->fetchValue(TBL_COLLECTION_DECO, 'decoXml', " collectionName = '" . $collection . "'");
        if ($decoXml != '') {
            $xml = $decoXml;
        } else {
            $xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
            $xml .= '<designs>' . "\n";
            $xml .= '</designs>' . "\n";
        }
        return $xml;
    }

    function getDesignById($collection, $pId, $sId) {
        $_SESSION['Design_ID_PARAMETER'] = "collelection = ".$collection.", pId = ".$pId.", sId = ".$sId;
        $xml = '';
        $dataArr = $this->fetchValue(TBL_COLLECTION_DATA, 'dataArr', " collectionName = '" . $collection . "' and pId = '" . $pId . "' and sId = '" . $sId . "'");
        if ($dataArr != '') {
            $xml = $dataArr;
        }
        return $xml;
    }

    function saveProductCollection($encodeObj = '') {        
        
         $_SESSION['save_prod_time']=date('h:is');
        $status = (object) $status;
        $jsonObj = new JSON();
//        $_SESSION ['ecodedcolec'] = $encodeObj;
//        $encodeObj = $_SESSION['ecodedcolec'];
        $obj = $jsonObj->decode($encodeObj);
      //  $_SESSION ['decoInfo'] = $obj;
        $curDate = date('Y-m-d');

        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';

        // ------removing if old collection exists--------


        $ctr = 0;
        $pIdArr = $jsonObj->decode($obj->pidArray);
        $rIdArr = $jsonObj->decode($obj->ridArray);
        $proInfo = $jsonObj->decode($obj->infoArray);
        
//        $_SESSION['decoheightwidth'] = $proInfo;
        
//        echo '<pre>';
//        print_r($obj);
//        print_r($rIdArr);exit();
//        print_r($pIdArr);
                        
        if(isset($_SESSION['ISUPDATE']) && $_SESSION['ISUPDATE']==1){
                        
                $collectionName = $this->setCollectionName();
                $_SESSION['REORDER_COLLECTION_NAME'] = $collectionName;
                unset($_SESSION['ISUPDATE']);
            
            
        }else{
        
            if($_SESSION['REORDER_COLLECTION_NAME']){
                $obj->collectionName = $_SESSION['REORDER_COLLECTION_NAME'];                
            }
            
        $colllection = $this->fetchValue(TBL_COLLECTION, 'id', " collectionName = '" . $obj->collectionName . "'");
        //16april
        if ($colllection) {
            $queryDel = "delete from " . TBL_COLLECTION . " where collectionName = '" . $obj->collectionName . "'";
            mysql_query($queryDel);
        }
           
             $proData = $this->fetchValue(TBL_COLLECTION_DATA, 'sId', " collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."'");
        $_SESSION['test_style_id'] = $proData;
            if ($proData) {
                //$queryDel = "delete from " . TBL_COLLECTION_DATA . " where collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."' and sId = '" . $obj->viewsInfo->styleId . "'";
                $queryDel = "delete from " . TBL_COLLECTION_DATA . " where collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."'";
                $_SESSION['deleted_query'] = $queryDel;
                $delrs = mysql_query($queryDel);
                $_SESSION['delted_status'] = $delrs;
            }
            
            //------removing old deco basket-----
            $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " collectionName = '" . $obj->collectionName . "'");
            if ($deco) {
                $queryDel = "delete from " . TBL_COLLECTION_DECO . " where collectionName = '" . $obj->collectionName . "'";
                mysql_query($queryDel);
            }        
            
            $collectionName = $obj->collectionName;
               
        }
        
        foreach ($pIdArr as $id) {
            if($id == $rIdArr[$ctr]) {
                $dupli = 0;
            } else {
                $dupli = 1;
            }
            $queryArr [] = " ('" . $userId . "', '" . mysql_real_escape_string($collectionName) . "', " . $id . ", " . $rIdArr[$ctr] . ",'" . serialize($proInfo[$ctr]) . "', '".$dupli."')";
            $ctr++;
        }        
        $_SESSION['collectionpId'] = $pIdArr;
        $queryVal = implode(',', $queryArr);
        $_SESSION['save_product_query'] = $queryVal;
        $queryCol = "insert into " . TBL_COLLECTION . " (userId, collectionName, pId, rawProdId, prodInfo, isDuplicate) values" . $queryVal;
        //$_SESSION['coll'] = $queryCol;
        $conf = mysql_query($queryCol);
        if ($conf) {
            // ------removing old product Data if exists--------
            //$proData = $this->fetchValue(TBL_COLLECTION_DATA, 'sId', " collectionName = '" . $obj->collectionName . "' and pId = '".$obj->viewsInfo->productId ."' and  sId = '" . $obj->viewsInfo->styleId . "'");
            
            $queryData = "insert into " . TBL_COLLECTION_DATA . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($collectionName) . "', pId = '" . $obj->viewsInfo->productId . "', sId = " . $obj->viewsInfo->styleId . ", dataXml = '" . mysql_real_escape_string($obj->viewsInfo->prodViewsdata) . "', dataArr = '" . mysql_real_escape_string($obj->viewsInfo->dataArr) . "',infoArr = '" . mysql_real_escape_string($obj->viewsInfo->colorInfoArray) . "', decoInfo = '". mysql_real_escape_string($obj->viewsInfo->decoArr)."', addDate = '" . $curDate . "'";
            mysql_query($queryData);

            $_SESSION['insert_collection'] = $queryData;
            #------saving deco basket----------
            
            $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($collectionName) . "', decoXml = '" . mysql_real_escape_string($obj->viewsInfo->decoBasket) . "', addDate = '" . $curDate . "'";
            mysql_query($queryDeco);
            
            //---------product's all image------

            //$queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->rowProductId."', prodViewImage = '". serialize($obj->viewsInfo->imgEncodeArray)."', addDate = '".  date('Y-m-d')."'";
            
            $checkVal = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', " collectionName = '".$collectionName."' and pId = '".$obj->viewsInfo->productId."'");
            if($checkVal)
            {
               $queryImage = "update ".TBL_COLLECTION_IMAGE." set prodViewImage = '". serialize($obj->viewsInfo->imgEncodeArray)."' where collectionName = '".$collectionName."' and pId = '".$obj->viewsInfo->productId."'";
            }
            else
            {
             $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->rowProductId."', prodViewImage = '". serialize($obj->viewsInfo->imgEncodeArray)."', addDate = '".  date('Y-m-d')."'";
                //$queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->rowProductId."', addDate = '".  date('Y-m-d')."'";
            }
            
            mysql_query($queryImage); 
              
             //$this->savemainproductview(serialize($obj->viewsInfo->imgEncodeArray),$collectionName);
              
                                                                     
            $statusTxt = 'ok';
        } else
            $statusTxt = 'fail';
        
        if($obj->saveType == 'save')
            $url = 'myaccount.php?ref=saveDesign';
        else
            $url = '';
        
        $status->status = $statusTxt;
        $status->url = $url;
        $status->collectionName = $collectionName;
        setcookie("saveStatus", "1", time()+3600*24, "/");
          $_SESSION['save_prod_end_time']=date('h:is');
        return $status;
    }

    function getProductViews($sId, $isEdit = 'false', $collectionName = '', $colorType = 'Normal', $colorId = '',$pId = '') {
        
        $_SESSION ['param'] = 'sid= ' . $sId . ' edit= ' . $isEdit . ' collec= ' . $collectionName . 'colorType = ' . $colorType . 'colorId =' . $colorId.' pId='.$pId;
        $xml = '';
        if ($isEdit == 'true') {            
            $xml = $this->fetchValue(TBL_COLLECTION_DATA, 'dataXml', "collectionName = '" . $collectionName . "' and pId = '".$pId."' and sId = '" . $sId . "'");            
            if ($xml == '' && $colorType == 'Normal') {                
                $xml = $this->getDefaultProductViews($sId); // if not then load the default views
            } else if ($xml == '' && $colorType == 'Special') {                
                $xml = $this->getSpecialColProView($sId, $colorId);
            }
        } else if ($colorType == 'Normal') {            
            $xml = $this->getDefaultProductViews($sId);
        }
        else{
            $xml = $this->getSpecialColProView($sId, $colorId);
        }
        $_SESSION['VIEW_TEST'] = $xml;
        return $xml;
    }

    function getSpecialColProView($sId, $colorId) {
        $xml = '';
        $xml .= '<xml>' . "\n";
        $xml .= '[sizeAttr]<price>0.00</price><views>' . "\n";
        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $sId . "'");
        $query = "select pt.*, vd.viewName from " . TBL_PRODUCT_TOOL . " as pt INNER JOIN " . TBL_VIEWDESC . " as vd ON (pt.viewId = vd.viewId) and vd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pt.pId = '" . $pId . "' order by vd.viewId";
        // echo $query; exit();
        $result = mysql_query($query);
        $flag = true;
        while ($line = mysql_fetch_object($result)) {
            $viewImage = $this->fetchValue(TBL_PRODUCT_VIEW, 'imageName', "pId = '" . $pId . "' and viewId = '" . $line->viewId . "'");
            if ($flag == true) {
                list ( $imgW, $imgH ) = getimagesize(ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $line->toolImage);
                $tmp = '<width>' . $imgW . '</width>
                                        <height>' . $imgH . '</height>';
                $flag = false;
            }
            $xml .= "\n" . '<view>
                                <title>' . $line->viewName . '</title>
                                <view>' . $line->viewName . '</view>
                                <subItemId>' . $line->viewId . '</subItemId>
                                <thumbnail>' . __PRODUCTVIEWTHUMB__ . $viewImage . '</thumbnail>						<url>' . SITE_URL . __PRODUCTTOOLLARGE__ . $line->toolImage . '</url>
                                <container>true</container>
                                <helpTip>' . $line->viewName . '</helpTip>
                                <ratio>1</ratio>
                                <price>0.00</price>
                                <x>' . $line->wAreaX . '</x>
                                <y>' . $line->wAreaY . '</y>
                                <width>' . $line->wAreaW . '</width>
                                <height>' . $line->wAreaH . '</height>
                                <images>' . "\n";
            // ---product part start--------
            $queryPart = "select psp.partImage, pd.partTitle from " . TBL_PRODUCT_SPECIAL_PART . " as psp INNER JOIN " . TBL_PARTDESC . " as pd ON (psp.titleId = pd.partId) and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and psp.sId = '" . $sId . "' and psp.viewId = '" . $line->viewId . "' and psp.colorId = '" . $colorId . "'";
            $hexVal = $this->fetchValue(TBL_SPECIAL_COLOR, 'colorCode', " id = '" . $colorId . "'");
            $resultPart = mysql_query($queryPart);
            $numPart = mysql_num_rows($resultPart);
            if ($numPart > 0) {
                while ($row = mysql_fetch_object($resultPart)) {
                    $colorable = 'false';
                    $xml .= '<image name="' . $row->partTitle . '" isColorable="' . $colorable . '" hexValue="' . substr($hexVal, 1) . '" source="' . __PRODUCTPARTLARGE__ . $row->partImage . '"/>' . "\n";
                }
            }

            // ---product part start--------
            $xml .= '</images>' . "\n";
            $xml .= '</view>' . "\n";
        }
        $xml .= '</views>' . "\n";
        $xml .= '</xml>' . "\n";

        $nXml = str_replace('[sizeAttr]', $tmp, $xml);

        return $nXml;
    }

    function getDefaultProductViews($sId) {
        $xml = '';
        $xml .= '<xml>' . "\n";
        $xml .= '[sizeAttr]<price>0.00</price><views>' . "\n";
        $pId = $this->fetchValue(TBL_PRODUCT_STYLE, 'pId', "id = '" . $sId . "'");
 //       $query = "select pt.*, vd.viewName from " . TBL_PRODUCT_TOOL . " as pt INNER JOIN " . TBL_VIEWDESC . " as vd ON (pt.viewId = vd.viewId) and vd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pt.pId = '" . $pId . "'";
        
        $query = "select pt.*, vd.viewName from " . TBL_PRODUCT_TOOL . " as pt INNER JOIN " . TBL_VIEWDESC . " as vd ON (pt.viewId = vd.viewId) where vd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pt.pId = '" . $pId . "' order by vd.viewId";
        
        // echo $query; exit();
        $result = mysql_query($query);
        $flag = true;
        while ($line = mysql_fetch_object($result)) {
            $viewImage = $this->fetchValue(TBL_PRODUCT_VIEW, 'imageName', "pId = '" . $pId . "' and viewId = '" . $line->viewId . "'");
            if ($flag == true) {
                list ( $imgW, $imgH ) = getimagesize(ABSOLUTEPATH . __PRODUCTTOOLLARGE__ . $line->toolImage);
                $tmp = '<width>' . $imgW . '</width>
					 <height>' . $imgH . '</height>';
                $flag = false;
            }
            $xml .= "\n" . '<view>
                                    <title>' . $line->viewName . '</title>
                                    <view>' . $line->viewName . '</view>
                                    <subItemId>' . $line->viewId . '</subItemId>
                                    <thumbnail>' . __PRODUCTVIEWTHUMB__ . $viewImage . '</thumbnail>                                                          <url>' . SITE_URL . __PRODUCTTOOLLARGE__ . $line->toolImage . '</url>
                                    <container>true</container>
                                    <helpTip>' . $line->viewName . '</helpTip>
                                    <ratio>1</ratio>
                                    <price>0.00</price>
                                    <x>' . $line->wAreaX . '</x>
                                    <y>' . $line->wAreaY . '</y>
                                    <width>' . $line->wAreaW . '</width>
                                    <height>' . $line->wAreaH . '</height>
                                    <printWidth>' . $line->printWidth . '</printWidth>
                                    <printHeight>' . $line->printHeight . '</printHeight>
                                    <images>' . "\n";
            // ---product part start--------
            $queryPart = "select pp.partImage, pp.isColorable, pd.partTitle,pd.partTitle,pp.is_visible,pp.sequence from " . TBL_PRODUCT_PART . " as pp INNER JOIN " . TBL_PARTDESC . " as pd ON (pp.titleId = pd.partId) and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' and pp.sId = '" . $sId . "' and pp.viewId = '" . $line->viewId . "' order by sequence asc";
            
            $_SESSION['VIEW_IMAGE_TEST'] = $queryPart;
            $resultPart = mysql_query($queryPart);
            $numPart = mysql_num_rows($resultPart);
            if ($numPart > 0) {
                while ($row = mysql_fetch_object($resultPart)) {
                    $colorable = $row->isColorable == '0' ? 'false' : 'true';
                    $visible = $row->is_visible == '0' ? 'false' : 'true';
                    $xml .= '<image name="' . $row->partTitle . '" isColorable="' . $colorable . '" isVisible="' . $visible . '" hexValue="" specialsource="' . __PRODUCTPARTLARGE__ . $row->partImage . '" source="' . __PRODUCTPARTLARGE__ . $row->partImage . '"/>' . "\n";
                }
            }

            // ---product part start--------
            $xml .= '</images>' . "\n";
            $xml .= '</view>' . "\n";
        }
        $xml .= '</views>' . "\n";
        $xml .= '</xml>' . "\n";

        $nXml = str_replace('[sizeAttr]', $tmp, $xml);

        return $nXml;
    }

    function getStyleCode($fcId, $code) {
        $query = mysql_query("select fQuality from ".TBL_FABRICCHARGE." where id = {$fcId}");
        $result = mysql_fetch_object($query);
        $fQuality = $result->fQuality;
        $quality = substr($fQuality, 0, 3);
        
        $styleCode = str_replace("xxx", $quality, $code);
        return $styleCode;
    }
/*    
    function getProductStyleById($pId, $qId, $collectionName, $duplicate = 'false') {        
        $_SESSION['stylevariable'] = 'pId='.$pId.' qId='.$qId.' collection='.$collectionName.' dupli='.$duplicate;
        $obj = (object) $obj;
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $xml .= '<variants>' . "\n";
        
        if($duplicate == 'false') {
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and rawProdId = '".$pId."'");
            //original code
            $query = "select ps.id, ps.styleCode, pf.fId as pfId, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "' and pf.fId = '" . $qId . "' order by ps.isBasic desc";          
 //testing code change it
  //$query = "select ps.id, ps.styleCode, pf.fId as pfId, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "'";
            
            $_SESSION['queryTest'] = $query;
            $result = mysql_query($query);
            while ($line = mysql_fetch_object($result)) {
                $_SESSION['productstyleinfo'] = $line;
                $styleCode = $this->getStyleCode($line->pfId, $line->styleCode);
                $_SESSION['styleCodeTest'] = $styleCode;
                
                $xml .= "\n" . '<variant>
                                        <id>' . $line->id . '</id>
                                        <thumb>' . __PRODUCTBASICTHUMB__ . $line->styleImage . '</thumb>
                                        <title1>' .$styleCode. '</title1>
                                        <title2></title2>
                                </variant>' . "\n";
            }
        }
        else
        {
            $_SESSION['here'] = 1;
            $jsonObj = new JSON();
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and pId = '".$pId."'");            
            $info = unserialize($infoArr);            
            $sId = $info->isVarintDefault;
            
            $code = $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '".$sId."'");
            $styleCode = $this->getStyleCode($qId, $code);
            
            $xml .= "\n" . '<variant>
                                <id>' . $sId . '</id>
                                <thumb>' . __PRODUCTBASICTHUMB__ . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleImage', " id = '".$sId."'") . '</thumb>
                                <title1>' . $styleCode . '</title1>
                                <title2></title2>
                        </variant>' . "\n";
        }        
        $xml .= '</variants>' . "\n";
        
        $jsonObj = new JSON();
        if($infoArr != '')
        {   
            $infoObj = unserialize($infoArr);
            if($infoObj->sIdArray != '')
            {
                $sIdArr = explode(',',$infoObj->sIdArray);
                $resultInfo = $jsonObj->encode($sIdArr);
            }
            else
                $resultInfo = '';
        }
        else
            $resultInfo = '';
        
        
        $obj->result = $xml;
        $obj->info = $resultInfo;
        return $obj;        
    }   */
    
    function getProductStyleById($pId, $rawProdId, $qId, $collectionName, $duplicate = 'false') {
        $_SESSION['stylevariable'] = 'pId='.$pId.' qId='.$qId.' collection='.$collectionName.' dupli='.$duplicate;
        $obj = (object) $obj;
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $xml .= '<variants>' . "\n";
        
//        $rawProductId = $this->fetchValue(TBL_COLLECTION, "rawProdId", " collectionName = '".$collectionName."' and pId = '".$pId."'");
        //$_SESSION['rawProdIdTest'] = $rawProductId;
        $_SESSION['rawProdIdTest'] = 'pId='.$pId.' rawProdId = '.$rawProdId.' qId='.$qId.' collection='.$collectionName.' dupli='.$duplicate;
        if($rawProdId != $pId) {
            $duplicateId = $pId;
            $pId = $rawProdId;            
        }
        
        if($duplicate == 'false') {
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and rawProdId = '".$pId."'");
            //8 Aug Code
            //$query = "select ps.id, ps.styleCode, pf.fId as pfId, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "' and pf.fId = '" . $qId . "' order by ps.isBasic desc";            
                        
            $query = "select ps.id, ps.styleCode, pf.fId as pfId, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "' and pf.fId = '" . $qId . "' order by ps.styleCode asc ";
            
 //testing code change it
  //$query = "select ps.id, ps.styleCode, pf.fId as pfId, ps.styleImage from " . TBL_PRODUCT_STYLE . " as ps INNER JOIN " . TBL_PRODUCT_FABRIC . " as pf ON (ps.id = pf.sId) and ps.status = '1' and ps.isDeleted = '0' and ps.pId = '" . $pId . "'";
            
            $_SESSION['queryTest'] = $query;
            $result = mysql_query($query);
            while ($line = mysql_fetch_object($result)) {
                $_SESSION['productstyleinfo'] = $line;
                $styleCode = $this->getStyleCode($line->pfId, $line->styleCode);
                $_SESSION['styleCodeTest'] = $styleCode;
                
                $xml .= "\n" . '<variant>
                                        <id>' . $line->id . '</id>
                                        <thumb>' . __PRODUCTBASICTHUMB__ . $line->styleImage . '</thumb>
                                        <title1>' .$styleCode. '</title1>
                                        <title2></title2>
                                </variant>' . "\n";
            }
        }
        else
        {
            $_SESSION['here'] = 1;
            $_SESSION['duplicate_variables'] = $collectionName.", ".$duplicateId;
            $jsonObj = new JSON();            
            $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$collectionName."' and pId = '".$duplicateId."'");
            $info = unserialize($infoArr);
            $_SESSION['styleIdInfo'] = $info;
            $sId = $info->isVarintDefault;
            $_SESSION['styleIdCheck'] = $sId;
            $code = $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '".$sId."'");
            $styleCode = $this->getStyleCode($qId, $code);
            
            $xml .= "\n" . '<variant>
                                <id>' . $sId . '</id>
                                <thumb>' . __PRODUCTBASICTHUMB__ . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleImage', " id = '".$sId."'") . '</thumb>
                                <title1>' . $styleCode . '</title1>
                                <title2></title2>
                        </variant>' . "\n";
        }        
        $xml .= '</variants>' . "\n";
        
        $jsonObj = new JSON();
        if($infoArr != '')
        {   
            $infoObj = unserialize($infoArr);
            if($infoObj->sIdArray != '')
            {
                $sIdArr = explode(',',$infoObj->sIdArray);
                $resultInfo = $jsonObj->encode($sIdArr);
            }
            else
                $resultInfo = '';
        }
        else
            $resultInfo = '';
        
        $_SESSION['StyleXML'] = $xml;
        $obj->result = $xml;
        $obj->info = $resultInfo;
        return $obj;        
    }
    
    
    function getColorPallet($sId, $qId, $collectionName) {
        $_SESSION['colorPallet'] = 'sId='.$sId.' qId='.$qId.' collection='.$collectionName;
        $obj = (object) $obj;
        $jsonObj = new JSON();
        $xml = '';
        $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
        $xml .= '<colors>' . "\n";
        $palletId = $this->fetchValue(TBL_FABRICCHARGE, 'palletId', "id = '" . $qId . "'");
        $_SESSION['palletIdtest'] = $palletId;
      //  $queryColor = "select id, code, colorCode, colorImage from " . TBL_PALLETCOLOR . " where code !='' and colorCode !='' and palletId = '" . $palletId . "'";
        
    //    $queryColor = "select id, colorCode from " . TBL_PALLETCOLOR . " where colorCode !='' and palletId = '" . $palletId . "'";
        
          $queryColor = "select id, code, colorCode, colorImage from " . TBL_PALLETCOLOR . " where colorCode !='' and palletId = '" . $palletId . "'";
      
        $resultColor = mysql_query($queryColor);        
        while ($row = mysql_fetch_object($resultColor)) {            
            if ($row->colorImage == '')
                $image = '';
            else
                $image = __PALLETIMGTHUMB__ . $row->colorImage;
            $xml .= "\n" . '<color>
				<id>' . $row->id . '</id>
				<type>Normal</type>
				<title1>' . $row->code . '</title1>                                
				<value>' . str_replace('#', '', $row->colorCode) . '</value>
				<url>' . $image . '</url>
			</color>' . "\n";                       
        }
        $xml .= $this->getSpecialColor($sId);
        $xml .= '</colors>' . "\n";
        $obj->result = $xml;
        
        $_SESSION['color_xml'] = $xml;
        
        $rowInfo = $this->fetchValue(TBL_COLLECTION_DATA, 'infoArr', " collectionName = '".$collectionName."' and sId = '".$sId."'");
//        echo "<pre>";
//        print_r($rowInfo);
        if($rowInfo == '')
        {
            $tmpArr = (object) $tmpArr;
            $tmpArr->colorId = '';
            $tmpArr->coloringStyle = '';
            $tmpArr->colorType = '';
            
            //$rowInfo = $jsonObj->encode($tmpArr);
            $obj->info = $tmpArr;
            unset($tmpArr);
        }
        else            
            $obj->info = $jsonObj->decode($rowInfo);
        
        return $obj;
    }
    
    function getSpecialColor($sId) {
        $xml = '';
        $query = "select sc.*, psp.colorId from " . TBL_SPECIAL_COLOR . " as sc INNER JOIN " . TBL_PRODUCT_SPECIAL_PART . " as psp ON (sc.id = psp.colorId) and psp.sId = '" . $sId . "' and sc.isDeleted = '0' group by psp.colorId";

        $result = mysql_query($query);
        $num = mysql_num_rows($result);
        
        if ($num > 0) {
            while ($row = mysql_fetch_object($result)) {
                $_SESSION['pallet_color_image'][] = $row->colorImage;
                $xml .= "\n" . '<color>
                                    <id>' . $row->id . '</id>
                                    <type>Special</type>
                                    <title1>' . $row->code . '</title1>
                                    <value>' . str_replace('#', '', $row->colorCode) . '</value>
                                    <url>' . __PALLETIMGTHUMB__ . $row->colorImage . '</url>
                                </color>' . "\n";
            }
        }
        return $xml;
    }
    
    function appendProductToCollection($type) {

        $xml = '';
        $xml1 = '';
        $xmlCl = '';
        $flag = true;
        if(count($_SESSION['productId']) > 0) //if(1)//
        {
            if($type == 'false')
            {            
                $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
                $xml .= '<collections>' . "\n";
                $xmlCl .= '</collections>' . "\n";
            }
            $productId = implode(',',$_SESSION['productId']);//'1,2'; // 
            $query = "Select p.id, p.proImage, pd.productName from " . TBL_PRODUCT . " as p INNER JOIN " . TBL_PRODUCT_DESCRIPTION . " as pd ON (p.id = pd.pId) where p.id in (" . $productId . ") and pd.langId = '" . $_SESSION ['DEFAULTLANGUAGEID'] . "' order by p.id";
            $result = mysql_query($query);

            while ($line = mysql_fetch_object($result)) {
                if ($flag == true && $type == 'false') {
                    $xml .= '<products isDefault="' . $line->id . '">' . "\n";
                    $xml1 = '</products>'."\n";
                    $flag = false;
                }
                //23Oct2013
//                $xml .= '<product>
//                            <sn></sn>
//                            <id>' . $line->id . '</id>
//                            <rowProdId>' . $line->id . '</rowProdId>
//                            <edit>false</edit>
//                            <duplicate>false</duplicate>
//                            <duplicateEdit>false</duplicateEdit>
//                            <title>' . $line->productName . '</title>
//                            <ordercode1>' . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " pId = '" . $line->id . "' and isBasic = '1'") . '</ordercode1>
//                            <ordercode2></ordercode2>
//                            <thumb>' . __PRODUCTTHUMB_125x160_ . $line->proImage . '</thumb>';
                
                $xml .= '<product>
                                <sn></sn>
                                <id>' . $line->id . '</id>
                                <rowProdId>' . $line->id . '</rowProdId>
                                <edit>false</edit>
                                <duplicate>false</duplicate>
                                <duplicateEdit>false</duplicateEdit>                                
                                <ordercode1>' . $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " pId = '" . $line->id . "' and isBasic = '1'") . '</ordercode1>
                                <ordercode2></ordercode2>
                                <thumb>' . __PRODUCTTHUMB_125x160_ . $line->proImage . '</thumb>';
                $xml .= $this->getFabricQuality($line->id, '');
                $xml .= '</product>';
//                $ctr++;
            }
            $xml .= "\n" . $xml1 . "\n";
            $xml .= $xmlCl;
            unset($_SESSION ['productId']);
        }
        return $xml;
    }
    
    function getFabricQualityForDuplicate($qId, $default) {
        
        $xml = '';
        $quality = $this->fetchValue(TBL_FABRICCHARGE, 'fQuality', "id = '".$qId."'");
        $xml .= "\n" . '<fabricquality isDefault="' . $default->isQualityDefault . '" isVarintDefault="' . $default->isVarintDefault . '" colorType="' . $default->colorType . '" colorId="' . $default->colorId . '" coloringStyle="' . $default->coloringStyle . '" sIdArray="'.$default->sIdArray.'">' . "\n";

        $xml .= "\n" . '<quality> 
                            <id>' . $qId . '</id> 
                            <value>' . $quality . '</value>
                    </quality>' . "\n";
        $xml .= '</fabricquality>'. "\n";
        
        return $xml;
    }
    
    function getFabricQuality($pId, $default = '') {
        $xml = '';
        //$queryFab = "select distinct(pf.fId), fc.fQuality from " . TBL_PRODUCT_FABRIC . " as pf INNER JOIN " . TBL_FABRICCHARGE . " as fc ON(pf.fId = fc.id) and pf.pId = '" . $pId . "' and pf.isDeleted = '0' order by fc.fQuality";
        $queryFab = "select distinct(pf.fId), fc.fQuality from " . TBL_PRODUCT_FABRIC . " as pf INNER JOIN " . TBL_FABRICCHARGE . " as fc ON(pf.fId = fc.id) and pf.pId = '" . $pId . "' and pf.isDeleted = '0' order by fc.id";
        $res = mysql_query($queryFab);
        $numFab = mysql_num_rows($res);
        $qflag = true;
        if ($numFab > 0) {
            while ($row = mysql_fetch_object($res)) {
                if ($qflag == true) {
                    if ($default == '') {
                        $xml .= "\n" . '<fabricquality isDefault="' . $row->fId . '" isVarintDefault="" colorType="" colorId="" coloringStyle="" sIdArray="">' . "\n";
                    } else {
                        $xml .= "\n" . '<fabricquality isDefault="' . $default->isQualityDefault . '" isVarintDefault="' . $default->isVarintDefault . '" colorType="' . $default->colorType . '" colorId="' . $default->colorId . '" coloringStyle="' . $default->coloringStyle . '" sIdArray="'.$default->sIdArray.'">' . "\n";
                    }
                    $qflag = false;
                }
                $xml .= "\n" . '<quality> 
                                        <id>' . $row->fId . '</id> 
                                        <value>' . $row->fQuality . '</value>
                                </quality>' . "\n";
            }
        }
        $xml .= '</fabricquality>';

        return $xml;
    }

    function getCollectionName() {  // -------returns colection name-----
        if (isset($_COOKIE ['collection'])) {
            $collectionName = $_COOKIE ['collection'];
        } else {
            $collectionName = time() . rand();
            setcookie('collection', $collectionName, time() + 60 * 60 * 24 * 30, '/');
        }
        return $collectionName;
    }
    
    function setCollectionName() {  // -------returns colection name-----  
            unset($_COOKIE ['collection']);
            $collectionName = time().rand();
            setcookie('collection', $collectionName, time() + 60 * 60 * 24 * 30, '/');                  
            return $collectionName;
    }

    function fetchValue($tbl, $field, $con) {
        $sql = "select " . $field . " from " . $tbl . " where " . $con;
        $result = mysql_query($sql) or die(mysql_error());
        $rec = mysql_fetch_object($result);
        $val = $rec->$field;
        return $val;
    }

    function removeDecoFromXml($deco, $decoId, $type = '') {
        if ($deco != '') {
            if ($type == '') {
                $xmlString = '<design><deco>' . $deco . '</deco></design>';
                $designXml = new SimpleXMLElement($xmlString);
                if ($designXml->deco->designId == $decoId) {
                    unset($designXml->deco);
                    $decoTmp = $designXml->asXML();
                } else {
                    $decoTmp = $designXml->asXML();
                }
                $find = array('<?xml version="1.0"?>', '<design>', '<deco>', '</deco>', '</design>');
                $decoXml = str_replace($find, '', $decoTmp);
            } else {
                $designXml = new SimpleXMLElement($deco);
                $decoArr = array();
                foreach ($designXml->deco as $design) {
                    if ($design->designId == $decoId)
                        unset($design->deco);
                    else
                        $decoArr[] = $design->asXML();

                    unset($design);
                }

                $decoTmp = implode('', $decoArr);
                $decoTmp = '<designs>' . $decoTmp . '</designs>';
                $decoXml = str_replace('<?xml version="1.0"?>', '', $decoTmp);
            }
        }
        $decoXml = str_replace('<design/>', '', $decoXml);
        $breaks = array("\r\n", "\n", "\r", '  ');
        $newtext = str_replace($breaks, "", $decoXml);
        return $newtext;
    }
    
    function getCollectionOverview($obj = '', $display = 'true') {
        $xml = '';
//        $_SESSION['overview'] = $obj;
//        $obj = $_SESSION['overview'];
        $jsonObj = new JSON();
        $pIdArr = $jsonObj->decode($obj['pidArray']);
        $rIdArr = $jsonObj->decode($obj['ridArray']);
        $queryColl = "select pId from ".TBL_COLLECTION_IMAGE." where collectionName = '".$obj['collectionName']."'";
       
        $result = mysql_query($queryColl);
        $num = mysql_num_rows($result);
        if($num == 0)
        {
            $ctr = 0;
            $queryValArr = array();
            foreach ($pIdArr as $pId)
            {
                $queryValArr[] = "('".$obj['collectionName']."','".$pId."', '".$rIdArr[$ctr]."', '".date('Y-m-d')."')";
                $ctr ++;
            }
            $queryVal = implode(', ', $queryValArr);
            $query = "insert into ".TBL_COLLECTION_IMAGE." (collectionName, pId, rId, addDate) values ".$queryVal;
           
            mysql_query($query);
        }
        else
        {
            $queryValArr = array();
            $ctr = 0;
           
            foreach ($pIdArr as $pId)
            {
                
                $check = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', " collectionName = '".$obj['collectionName']."' and pId = '".$pId."'");
                if(! $check)
                {
                    $queryValArr[] = "('".$obj['collectionName']."','".$pId."', '".$rIdArr[$ctr]."', '".date('Y-m-d')."')";
                }
                $ctr ++;
            }
            if(count($queryValArr) > 0)
            {
                $queryVal = implode(', ', $queryValArr);
                $query = "insert into ".TBL_COLLECTION_IMAGE." (collectionName, pId, rId, addDate) values ".$queryVal;
              
                mysql_query($query);
            }
        }
                       
        //------updating --------
        $queryUpdate = "update ".TBL_COLLECTION_IMAGE." set encodeString = '".mysql_real_escape_string($obj['imgInfo']['imgIncoded'])."' where collectionName = '".$obj['collectionName']."' and pId = '".$obj['imgInfo']['pId']."'";
        $conf = mysql_query($queryUpdate);
         $_SESSION['queryUpdate']=$queryUpdate;
        
        //------fetching image for preview-------
        if($conf && $display == 'true')
        {
            //--------update sequence--------
            $snArr = $jsonObj->decode($obj['snArray']);
            foreach ($snArr as $sn) {
                $query = "update " . TBL_COLLECTION_IMAGE . " set sequence = '" . $sn->sn . "' where collectionName = '" . $obj['collectionName'] . "' and pId = '" . $sn->pid . "'";
                mysql_query($query);
            }
            
            $queryImage = "select pId, rId, encodeString, sequence from ".TBL_COLLECTION_IMAGE." where collectionName = '".$obj['collectionName']."' order by sequence";
            $resultImage = mysql_query($queryImage);
            if(mysql_num_rows($resultImage) > 0)
            {
                $xml .= '<?xml version="1.0" encoding="utf-8"?>' . "\n";
                $xml .= '<images>' . "\n";
                while($rowImage = mysql_fetch_object($resultImage))
                {                						$_SESSION['datadata'] =   $rowImage;                  		
                    $xml .= '<image>'."\n";                    
                    $xml .= '<id>'.$rowImage->pId.'</id>'. "\n";
                    $xml .= '<rowProdId>'.$rowImage->rId.'</rowProdId>'. "\n";
                    $xml .= '<sn>'.$rowImage->sequence.'</sn>'. "\n";
                    $xml .= '<url>'.$this->getImage($rowImage->rId, $rowImage->encodeString).'</url>'. "\n";
                    $xml .= '<edit>true</edit>'. "\n";
                    $xml .= '</image>'."\n";
                }
                $xml .= '</images>' . "\n";
                $_SESSION['XMLOVERVIEW__'] = $xml;
            }            
        }        
        return $xml;        
    }
    
    function deleteProductPreview($collection, $pId) {
        
        //------delete product from collection--------
        $check = $this->fetchValue(TBL_COLLECTION, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if($check)
        {
            mysql_query("delete from ".TBL_COLLECTION." where collectionName = '".$collection."' and pId = '".$pId."'");
        }
        
        //---------delete record from collection data------
        $checkData = $this->fetchValue(TBL_COLLECTION_DATA, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if($checkData)
        {
            mysql_query("delete from ".TBL_COLLECTION_DATA." where collectionName = '".$collection."' and pId = '".$pId."'");
        }
        
        $val = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', "collectionName = '" . $collection . "' and pId = '" . $pId . "'");
        if ($val) {
            $query = "delete from " . TBL_COLLECTION_IMAGE . " where collectionName = '" . $collection . "' and pId = '" . $pId . "'";
            mysql_query($query);
        }
        return 'ok';
    }
    
    function getImage($rId, $image) {
        $imageName = __TEMPIMAGE__ . time() . rand() . ".png";
        if ($image != '') { //---showing from image string
            $imageStr = base64_decode($image);
            $fp = fopen(ABSOLUTEPATH . $imageName, 'w');
            fwrite($fp, $imageStr);
            fclose($fp);
            $file = $imageName;
        } else { //------showing the default view image
            $query = "select imageName from " . TBL_PRODUCT_VIEW . " where pId = '" . $rId . "'";
            $resultSet = mysql_query($query);
            if (mysql_num_rows($resultSet) > 0) {
                $imageArr = array();
                while ($line = mysql_fetch_object($resultSet)) {
                    $imageArr[] = ABSOLUTEPATH . __PRODUCTVIEWTHUMB__ . $line->imageName;
                }

                $c = count($imageArr);
                if ($c > 0) {
                    $imgArr = array_chunk($imageArr, '4');
                    $ctr = 1;
                    $tmpImgArr = array();

                    foreach ($imgArr as $image) {
                        $str = implode(' ', $image);

                        $tmpImage = ABSOLUTEPATH . __TEMPIMAGE__ . time() . rand() . $ctr . ".png";
                        $tmpImgArr[] = $tmpImage;
                        exec("convert  $str  +append -trim $tmpImage");
                        @chmod($tmpImage, 0777);
                        $ctr++;
                    }


                    $strComp = '-size 1500x1500 xc:none ';
                    if (count($tmpImgArr) > 0) {
                        $i = 0;
                        $y = 0;

                        $tHeight = 0;
                        foreach ($tmpImgArr as $tmpValue) {
                            list($width, $height) = getimagesize($tmpValue);

                            if ($i > 0) {
                                $y+=$height + 10;
                            }

                            $strComp.=' "' . $tmpValue . '" -geometry +0+' . $y . ' -composite ';
                            $i++;

                            $tHeight += $height;
                        }

                        exec('convert ' . $strComp . ' -trim "' . ABSOLUTEPATH . $imageName . '"');
                        exec('convert ' . ABSOLUTEPATH . $imageName . ' -resize 280x' . $tHeight . ' "' . ABSOLUTEPATH . $imageName . '"');
                    }
                }
            }
            $file = $imageName;
        }
        return $file;
    }
    
    function updateDecoBasket($obj) {
		
        $curDate = date('Y-m-d');
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '0';
        #------saving deco basket----------
        //------removing old deco basket-----
        $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " userId = '" . $userId . "' and collectionName = '" . $obj['collectionName'] . "'");
        if ($deco) {
            $queryDel = "delete from " . TBL_COLLECTION_DECO . " where userId = '" . $userId . "' and collectionName = '" . $obj['collectionName'] . "'";
            mysql_query($queryDel);
        }
        $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj['collectionName']) . "', decoXml = '" . mysql_real_escape_string($obj['decoBasket']) . "', addDate = '" . $curDate . "'";
        mysql_query($queryDeco);
    }
        
    function duplicateProduct($obj = '') {
        $jsonObj = new JSON();
        $status = (object) $status;
        $obj = $jsonObj->decode($obj);
        //$_SESSION['duplicate'] = $obj->viewsInfo;
        //$_SESSION['duplicate'] = $obj;
        //$obj = $_SESSION['duplicate'];

        $pId = time();
        $curDate = date('Y-m-d');
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';
        
        $infoArr = $jsonObj->decode($obj->infoArray);
        //print_r($infoArr[0]);
        
        //-----collection--------
        $query = "insert into ".TBL_COLLECTION." set userId = '".$userId."', collectionName = '".$obj->collectionName."', pId = '".$pId."', rawProdId = '".$obj->viewsInfo->productId."', prodInfo = '".serialize($infoArr[0])."', isDuplicate = '1', addDate = '".$curDate."'";
        $_SESSION['duplicateProductQuery'] = $query;
        $conf = mysql_query($query);
        if($conf)
        {
            //-------collection data-------
            $queryData = "insert into ".TBL_COLLECTION_DATA." set userId = '".$userId."', collectionName = '".$obj->collectionName."', pId = '".$pId."', sId = '".$obj->viewsInfo->styleId."', dataXml = '".mysql_real_escape_string($obj->viewsInfo->prodViewsdata)."', dataArr = '".mysql_real_escape_string($obj->viewsInfo->dataArr)."', infoArr = '".mysql_real_escape_string($obj->viewsInfo->colorInfoArray)."', addDate = '".$curDate."'";
            
            $_SESSION['query_data'] = $queryData;
            
            mysql_query($queryData);
            
            //---------update deco if exists---------
            if($obj->viewsInfo->decoBasket != '')
            {
                //------removing old deco basket-----
                $deco = $this->fetchValue(TBL_COLLECTION_DECO, 'collectionName', " userId = '" . $userId . "' and collectionName = '" . $obj->collectionName . "'");
                if ($deco) {
                    $queryDel = "delete from " . TBL_COLLECTION_DECO . " where userId = '" . $userId . "' and collectionName = '" . $obj->collectionName . "'";
                    mysql_query($queryDel);
                }
                $queryDeco = "insert into " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "', collectionName = '" . mysql_real_escape_string($obj->collectionName) . "', decoXml = '" . mysql_real_escape_string($obj->viewsInfo->decoBasket) . "', addDate = '" . $curDate . "'";
                mysql_query($queryDeco);
            }
            
            //------image code for original--------
            $checkRecord = $this->fetchValue(TBL_COLLECTION_IMAGE, 'pId', "collectionName = '".$obj->collectionName."' and pId = '".$infoArr[0]->pid."'");
            if($checkRecord)
            {
                $queryImage = "update ".TBL_COLLECTION_IMAGE." set encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."' where collectionName = '".$obj->collectionName."' and pId = '".$infoArr[0]->pid."'";
                mysql_query($queryImage);
               // $_SESSION['UPDATEBLOB'] = $infoArr[0]->pid;
            }
            else
            {
                $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$obj->collectionName."', pId = '".$obj->viewsInfo->productId."', rId = '".$obj->viewsInfo->productId."', encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."', addDate = '".  date('Y-m-d')."'";
                mysql_query($queryImage);
            }
            
            //------image code for duplicate--------
            $queryImage = "insert into ".TBL_COLLECTION_IMAGE." set collectionName = '".$obj->collectionName."', pId = '".$pId."', rId = '".$obj->viewsInfo->productId."', encodeString = '".mysql_real_escape_string($obj->collectionOverviewInfo->imgIncoded)."', addDate = '".  date('Y-m-d')."'";
            mysql_query($queryImage);            
            
            //-----making product code------            
            $code = $this->fetchValue(TBL_PRODUCT_STYLE, "styleCode", " id = '".$obj->viewsInfo->styleId."'");
            $styleCode = $this->getStyleCode($obj->viewsInfo->qualityId, $code);
            //-----making product collection--------
            $xml = '';
            $xml .='<product>
			<sn></sn>
                        <id>' . $pId . '</id>
                        <rowProdId>' . $obj->viewsInfo->productId . '</rowProdId>
                        <edit>true</edit>
                        <duplicate>true</duplicate>
                        <duplicateEdit>true</duplicateEdit>
                        <ordercode1>' .$styleCode. '</ordercode1>
                        <ordercode2></ordercode2>
                        <thumb>' . __PRODUCTTHUMB_125x160_ . $this->fetchValue(TBL_PRODUCT, 'proImage', "id = '" . $obj->viewsInfo->productId . "'") . '</thumb>';
            
            
        
            $qId = $obj->viewsInfo->qualityId;
            $infoArr = $jsonObj->decode($obj->infoArray);
            $default = $infoArr[0];
            
            $xml .= $this->getFabricQualityForDuplicate($qId, $default);
            $xml .= '</product>';
            $_SESSION['duplicateXML'] =$xml;
            $status->status = '1';
            $status->xml = $xml;
            
        }
        else
            $status->status = '0';            
        return $status;
        
    }
    
    private $saveBasketId = array();
    function sendDataToPHP($cName = '') {		
		$_SESSION['send_time']=date('h:is');
        $_SESSION['collectionName'] = $cName;
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';        
        $query = "select CI.pId, CI.rId, CI.prodViewImage, CD.sId, CD.dataArr, CD.decoInfo from ".TBL_COLLECTION_IMAGE. " as CI INNER JOIN ".TBL_COLLECTION_DATA." as CD ON (CI.collectionName = CD.collectionName) and (CI.pId = CD.pId) and CI.collectionName = '".$cName."'";
        
        $_SESSION['send_data_query'] = $query;
        $result = mysql_query($query);
        $num = mysql_num_rows($result);
        $_SESSION['total_number_collection'] = $num;
        if($num > 0)
        {
            while ($row = mysql_fetch_object($result))
            {
                $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$cName."' and pId = '".$row->pId."'");
                $info = unserialize($infoArr);
                
                $categoryId = $this->fetchValue(TBL_PRODUCT, 'catId', " id = '".$row->rId."'");
                $queryProd = "insert into ".TBL_MAINPRODUCT." set rawProdId = '".$row->rId."', styleId = '".$row->sId."', qualityId = '".$info->isQualityDefault."', categoryId = '".$categoryId."', status = '1', isDeleted = '0', addDate = '".date('Y-m-d')."', addedBy = '".$userId."', dataArr = '".mysql_real_escape_string($row->dataArr)."', decoInfo = '".mysql_real_escape_string($row->decoInfo)."', isCustomizable = '0'";
                $conf = mysql_query($queryProd);
                if($conf)
                {
                    $pId = mysql_insert_id();                    
                    //-------storing product views and image-----
                    if($row->prodViewImage != '')
                    {
                        $viewArray = unserialize($row->prodViewImage);
                        $viewVal = array();
                        foreach ($viewArray as $view)
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $fp = fopen(ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName, 'w');
                            $image = base64_decode($view->imgEncodd);
                            fwrite($fp, $image);
                            fclose($fp);

                            $target = ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName;

                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");

                            $viewVal[] = " ('".$pId."', '".$view->viewId."', '".$imageName."')";
                        }
                    }
                    else
                    {
                        $getView = "select viewId, imageName from ".TBL_PRODUCT_VIEW." where pId = '".$row->rId."'";
                        $viewResult = mysql_query($getView);
                        $viewVal = array();
                        while ($line = mysql_fetch_object($viewResult))
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $defImage = $line->imageName;
                            $target = ABSOLUTEPATH.__PRODUCTVIEWORIGINAL__.$defImage;
                            
                            copy($target, ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName);
                            
                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");
                            
                            $viewVal[] = " ('".$pId."', '".$line->viewId."', '".$imageName."')";
                        }
                    }
                    if(count($viewVal) > 0)
                    {
                        $queryVal = implode(' ,', $viewVal);
                        $queryView = "insert into ".TBL_MAINPRODUCT_VIEW." (mainProdId, viewId, viewImage) values ".$queryVal;
                        mysql_query($queryView);
                    }
                    
                    //----------adding product to basket-------
                    $this->saveBasketId[] = $this->addToBasket($pId, $row->rId, $info->isQualityDefault);
                }
            }
            $userId = $this->fetchValue(TBL_BASKET, "userId", " id = {$this->saveBasketId[0]}");
            $_SESSION['saveBasketId'] = $this->saveBasketId;
            $baskId = implode(",", $this->saveBasketId);
//            $query = "delete from ".TBL_BASKET." where userId = {$userId} and id NOT IN ({$baskId})";
//            $_SESSION['delete_user_basket'] = $query;
//            mysql_query($query);
            $returnTxt = 'ok';
        }
        else            
            $returnTxt = 'fail';
        $_SESSION['send_end_time']=date('h:is');
        return $returnTxt;
    }
    
    /**********************************************************ram***********************************/
    
    function savemainproductview($prodViewImage,$cName)
    {
		
         $_SESSION['save_start_time']=date('h:is');
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';        
        $query = "select CI.pId, CI.rId, CD.sId, CD.dataArr, CD.decoInfo from ".TBL_COLLECTION_IMAGE. " as CI INNER JOIN ".TBL_COLLECTION_DATA." as CD ON (CI.collectionName = CD.collectionName) and (CI.pId = CD.pId) and CI.collectionName = '".$cName."'";
             $_SESSION['$query_st']=$query;
        $result = mysql_query($query);
        $num = mysql_num_rows($result);
        $_SESSION['total_number_collection'] = $num;
        if($num > 0)
        {
            while ($row = mysql_fetch_object($result))
            {
                $infoArr = $this->fetchValue(TBL_COLLECTION, 'prodInfo', " collectionName = '".$cName."' and pId = '".$row->pId."'");
                $info = unserialize($infoArr);
                
                $categoryId = $this->fetchValue(TBL_PRODUCT, 'catId', " id = '".$row->rId."'");
                $queryProd = "insert into ".TBL_MAINPRODUCT." set rawProdId = '".$row->rId."', styleId = '".$row->sId."', qualityId = '".$info->isQualityDefault."', categoryId = '".$categoryId."', status = '1', isDeleted = '0', addDate = '".date('Y-m-d')."', addedBy = '".$userId."', dataArr = '".mysql_real_escape_string($row->dataArr)."', decoInfo = '".mysql_real_escape_string($row->decoInfo)."', isCustomizable = '0'";
                $conf = mysql_query($queryProd);
                if($conf)
                {
                    $pId = mysql_insert_id(); 
                    $_SESSION['pid']=$pId;
                    //-------storing product views and image-----
                    if($prodViewImage != '')
                    {
                        $viewArray = unserialize($prodViewImage);
                        $_SESSION['viewArray']=$viewArray;
                        $viewVal = array();
                        if(count($viewArray))
                        {
						foreach ($viewArray as $view)
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $fp = fopen(ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName, 'w');
                            $image = base64_decode($view->imgEncodd);
                            fwrite($fp, $image);
                            fclose($fp);

                            $target = ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName;

                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");

                            $viewVal[] = " ('".$pId."', '".$view->viewId."', '".$imageName."')";
                        }
                        }
                    }
                    else
                    {
                        $getView = "select viewId, imageName from ".TBL_PRODUCT_VIEW." where pId = '".$row->rId."'";
                        $viewResult = mysql_query($getView);
                        $viewVal = array();
                        while ($line = mysql_fetch_object($viewResult))
                        {
                            $imageName = date('ymdhms').time().rand().'.png';
                            $defImage = $line->imageName;
                            $target = ABSOLUTEPATH.__PRODUCTVIEWORIGINAL__.$defImage;
                            
                            copy($target, ABSOLUTEPATH.__MAINPRODORIGINAL__.$imageName);
                            
                            $ThumbImage = ABSOLUTEPATH.__MAINPRODTHUMB__.$imageName;
                            exec(IMAGEMAGICPATH." $target -thumbnail 100x140! $ThumbImage");
                            
                            $viewVal[] = " ('".$pId."', '".$line->viewId."', '".$imageName."')";
                        }
                    }
                    if(count($viewVal) > 0)
                    {
                        $queryVal = implode(' ,', $viewVal);
                        $queryView = "insert into ".TBL_MAINPRODUCT_VIEW." (mainProdId, viewId, viewImage) values ".$queryVal;
                        mysql_query($queryView);
                    }
                    
                    //----------adding product to basket-------
                    $this->saveBasketId[] = $this->addToBasket($pId, $row->rId, $info->isQualityDefault);
                }
            }
            $userId = $this->fetchValue(TBL_BASKET, "userId", " id = {$this->saveBasketId[0]}");
            $_SESSION['saveBasketId'] = $this->saveBasketId;
            $baskId = implode(",", $this->saveBasketId);
//            $query = "delete from ".TBL_BASKET." where userId = {$userId} and id NOT IN ({$baskId})";
//            $_SESSION['delete_user_basket'] = $query;
//            mysql_query($query);
            $returnTxt = 'ok';
        }
        else            
            $returnTxt = 'fail';
        $_SESSION['send_end_time']=date('h:is');
         $_SESSION['save_end_time']=date('h:is');
        /*return $returnTxt;*/
	}
    /************************************ram************************************/
    function addToBasket($pId, $rId, $qId) {
        $userId = isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] :'0';
        $sessionId = session_id();
        $query = "insert into ".TBL_BASKET." set sessionId = '".$sessionId."', userId = '".$userId."', rawProductId = '".$rId."',qualityId = '".$qId."', mainProdId = '".$pId."'";
       $_SESSION['query_basket']=$query;
        $res = mysql_query($query);
        if($res) {
            $bId = mysql_insert_id();            
        }
        return $bId;
    }
    
    function isSaveCollection($status) {
        $_SESSION['saveStatus'] = $status;        
    }

    function getCurrentDateFormat() {
        $obj = (object) $obj;
        $date1 = "2013-07-12";
        $date2 = date("Y-m-d");
        $diff = abs(strtotime($date2) - strtotime($date1));        
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $finaldate = ($years * 365) + ($months * 30) + $days;
        $currentURL = (!empty($_SERVER['HTTPS'])) ? "https://" . $_SERVER['SERVER_NAME'] : "http://" . $_SERVER['SERVER_NAME'];
        // $staticURL = "http://www.personaliseren.net";
        $_SESSION['finalDate'] = $finaldate;
        if ($finaldate > 30) {
//            $obj->status = "0";
//            $obj->licenseMsg = "Sorry, Your Tool has been expired.";
            $obj->status = "1";
            $obj->licenseMsg = "";
        } else {
            $obj->status = "1";
            $obj->licenseMsg = "";
        }
        
        return $obj;
    }

}
 

 //$myobj = new onlineDesignerClient();
// $tmp = $myobj->sendDataToPHP('13581388581978745604');
// echo "<pre>";
// print_r($tmp);
?>

<?php
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCountry.php","edit_record");
/*---Basic for Each Page Ends----*/

$countryObj = new Country();
$genObj = new GeneralFunctions();


if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
		
		/// Validate Category Name
		$obj->fnAdd('countryName',$_POST['countryName'], 'req','Please Enter Country Name');
		//~ $obj->fnAdd('countryCode',$_POST['countryCode'], 'req', 'Please Enter Country Code (3).');
		$obj->fnAdd('countryCode2',$_POST['countryCode2'], 'req', 'Please Enter Country Code.');
		
		
		/// Get Validation : $str_valiate=0 for ERROR; $str_validate=1 for NO ERROR
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		
		/// Get ERROR ARRAY	=====
		$arr_error[countryName]=$obj->fnGetErr($arr_error[countryName]);
		//~ $arr_error[countryCode]=$obj->fnGetErr($arr_error[countryCode]);
		
		$arr_error[countryCode2]=$obj->fnGetErr($arr_error[countryCode2]);
		
		//// GET SPECIFIC ERROR & SET $str_validate=0 if get error START==================	
			$value= $_SESSION['DEFAULTLANGUAGEID'];//$value=__DEFAULT_ACTIVE_LANG_ID__;		
			if($countryObj->isCountryNameExist($_POST['countryName'],$value,base64_decode($_GET['id'])))
			{ 
				$arr_error[countryName] = '<span class="alert-red alert-icon">Country Name Already Exists</span>';
				$str_validate=0;				
			}
			
		
		/// Submit Record if Validation Succeed=====================================
		if($str_validate){
			$_POST = postwithoutspace($_POST);	
			$countryObj->editRecord($_POST,$_FILES);
		}
	
}
$result = $countryObj->getResult(base64_decode($_GET['id']));
//echo"<pre>";print_r($result); echo"</pre>";

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!-- New Drop Down menu -->

<script type="text/javascript">
function hrefBack1(){
	window.location='manageCountry.php';
}
</script>
</head>
<body>
<? include('includes/header.php'); ?>
 <section id="content">
	<h1>Country</h1>
	<fieldset>
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>Country</label>
   		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
   		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
			
        <div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset> 
       
		
		<!--<section>
        	<label for="Country Code">Country Code (3)</label>
			<div><input type="text" name="countryCode" id="m__countryCode" class="wel" value="<?=$_POST['countryCode']?$_POST['countryCode']:$result->countryCode?>" />
			<?=$arr_error[countryCode]?></div>
        </section> -->
        	<section>
        	<label for="Country Code">Country Code</label>
			<div><input type="text" name="countryCode2" id="m__countryCode2" class="wel" value="<?=$_POST['countryCode2']?$_POST['countryCode2']:$result->country_2_code?>" />
			<?=$arr_error[countryCode2]?></div>
        </section>
		
		<section>
        	<label for="Country Code">Country Name</label>
			<div><input type="text" name="countryName" id="m__countryName"  value="<?=$_POST['countryName']?$_POST['countryName']:$result->countryName?>" />
			<?=$arr_error[countryName]?></div>
        </section>
		
		<section>
              <label for="countryFlag">Country Flag</label>
                <div>
                   <input type="file" name="flag" id="flag"  class="browse-button" />
              </div>
         </section>
        
		
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
              </div>
          </section>
        </fieldset>
</div>
	</form>
</section>
   <div id="divTemp" style="display:none;"></div> 
<? unset($_SESSION['SESS_MSG']); ?>

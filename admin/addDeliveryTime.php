<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');

$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageDeliveryTime.php","add_record");
$currObj = new DeliveryTime();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('days',$_POST['days'], 'req', 'Please Enter Days.');
    $obj->fnAdd('days',$_POST['days'], 'float', 'Days value is wrong.');	
	$obj->fnAdd('pcs',$_POST['pcs'], 'req', 'Please Enter Pcs.');
    $obj->fnAdd('pcs',$_POST['pcs'], 'float', 'Pcs value is wrong.');
    $obj->fnAdd('sign',$_POST['sign'], 'req', 'Please select sign.');
    
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[days]=$obj->fnGetErr($arr_error[days]);
	$arr_error[pcs]=$obj->fnGetErr($arr_error[pcs]);
	$arr_error[sign]=$obj->fnGetErr($arr_error[sign]);
	
	if(empty($arr_error[days]) && empty($arr_error[pcs])){
		$_POST = postwithoutspace($_POST);
		$currObj->addDeliveryTime($_POST);
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageDeliveryTime.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Delivery Time</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
             <fieldset>

                 <label>Add Delivery</label>
                 <?php echo $_SESSION['SESS_MSG'] ?>

                 <!-- Start : Currency Name ------->
                 <section>
                     <label for="FirstName">Days</label>
                     <div>
                         <input type="text" name="days" id="m__days"  class="wel" value="<?= stripslashes($_POST['days']) ?>" />
                         <?= $arr_error['days'] ?>
                     </div>					 
                 </section> 

                 <section>
                     <label for="FirstName">Number of pcs</label>
                     <div>
                         <input type="text" name="pcs" id="m__pcs"  class="wel" value="<?= stripslashes($_POST['pcs']) ?>" />
                         <?= $arr_error['pcs'] ?>
                     </div>					 
                 </section>

                 <section>
                     <label for="FirstName">Discount</label>
                     <div>
                         <select name="sign">
                             <option value="">Select Sign</option>                                   
                             <option value="0">minus</option>
                             <option value="2">plus</option>
                             <option value="1">x</option>                                    
                         </select>
                         <?php echo $arr_error['sign']; ?>
                     </div>					 
                 </section>		

             </fieldset> 
			          
             <fieldset> 
                 <section>  
                     <label>&nbsp;</label>
                     <div style=" width:78%;">					
                         <input type="submit" name="submit"   value="Submit" />					
                         <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>       
                     </div>
                 </section>
             </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); //2828074445?>

<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCategory.php","edit_record");
/*---Basic for Each Page Ends----*/
$catObj = new Category();
$genObj = new GeneralFunctions();

$cid  = $_GET['cid']?$_GET['cid']:0;
$catObj->checkCategoryExist($cid);
if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;
	$rst = $catObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' order by languageName asc","","");		
	$num = $catObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $catObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
                    $obj->fnAdd('categoryName_'.$value,$_POST['categoryName_'.$value], 'req', 'Please enter category name');			
		}		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
                    $arr_error['categoryName_'.$value]=$obj->fnGetErr($arr_error['categoryName_'.$value]);
		}
			
		foreach($langIdArr as $key=>$value) {                    
//                    if($catObj->isCategoryNameExist($_POST['categoryName_'.$value], $value,base64_decode($_GET['id']), $cid))
                    if($catObj->isCategoryNameExist1($_POST['categoryName_'.$value], $value,base64_decode($_GET['id']), $cid))
                    { 
                        $arr_error['categoryName_'.$value] ='Category already exist';
                        if($arr_error['categoryName_'.$value]) 
                                $errorArr = 1;				
                    }
		}
                
/*
		if($_FILES['categoryImage']['name']) {
			$filename = stripslashes($_FILES['categoryImage']['name']);
			$extension = findexts($filename);
			$extension = strtolower($extension);	 	 

			if(!$catObj->checkExtensions($extension))
			{ 
				$arr_error[categoryImage] = "Upload Only ".$catObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
				if($arr_error[categoryImage]) 
					$errorArr = 1;	
			}
		}	
*/
		if($errorArr == 0 && $str_validate){
			$_POST = postwithoutspace($_POST);	
			$catObj->editRecord($_POST);
		}
	}	
}
$result = $catObj->getResult(base64_decode($_GET['id']));
//echo "<pre>"; print_r($result); echo "</pre>"; exit;
$position = substr($result->position,1);
$position1 = substr($position,0,-1);
if($position1){
	$positionArray = explode("-", $position1);
}

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>

</head>
<body>
<? include('includes/header.php');
if($cid) $addHadding = 'Sub category';
else $addHadding  = 'Category';
?>
 <section id="content">
	<h1>Product <?=$addHadding ?></h1>
<fieldset>
  		<form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
    	<label>Edit <?=$addHadding ?></label>
   		<input type="hidden" name="id" value="<?=base64_decode($_GET['id'])?>">		
   		<input type="hidden" name="page" value="<?=$_GET['page']?>">	
		<input type="hidden" name="cid" value="<?=$cid?>">	
        <div id="mainDiv">
        <div><?=$_SESSION['SESS_MSG']?></div>
        <fieldset>        
        <section>
        	<label><?=$addHadding ?>Name</label>
			<div>
           	<?=$genObj->getLanguageEditTextBox('categoryName','m__Category_Name',TBL_CATEGORY_DESCRIPTION,base64_decode($_GET['id']),"catId",$arr_error); //1->type,2->name,3->id,4->tablename,5->tableid?> 
			</div>
        </section>
      </fieldset>
		<fieldset>	 
          <section>   
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit" class="main-body-sub-submit"
style="cursor:pointer;" value="Submit" />
                <input type="button" name="back" id="back" value="Back"
class="main-body-sub-submit" style="cursor:pointer;" 
onclick="javascript:;hrefBack()"/>
              </div>
          </section>
        </fieldset>
</div>
	</form>
</fieldset>
</section>
   <div id="divTemp" style="display:none;"></div>
<? include_once('includes/footer.php');?>   
<? unset($_SESSION['SESS_MSG']); ?>

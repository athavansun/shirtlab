<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$genObj = new GeneralFunctions();
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabricPrice.php", "");
/* ---Basic for Each Page Starts---- */

$fabricObj = new Fabric();

$generalFunctionObj = new GeneralFunctions();
$result = $fabricObj->getFabricPriceResult(base64_decode($_GET['id']));

if (isset($_POST['submit'])) {
//  echo "<pre>"; print_r($_POST); echo "</pre>";exit;
    require_once('validation_class.php');
    $obj = new validationclass();
    
	$obj->fnAdd('fabricName', $_POST['fabricName'], 'req', 'Please Select fabric name.');              
    $obj->fnAdd("percent", $_POST["percent"], "req", "Please Enter fabric Percent.");
    $obj->fnAdd("palletId", $_POST["palletId"], "req", "Please select pallet color.");                                

    //Validate===============
    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;

	//Get Error================
    
    $arr_error['fabricName'] = $obj->fnGetErr($arr_error['fabricName']);        
	$arr_error['percent'] = $obj->fnGetErr($arr_error['percent']);  
	$arr_error['palletId'] = $obj->fnGetErr($arr_error['palletId']);

    if ($str_validate){
    	$fabricObj->editFabricPrice($_POST);
    }
}
/*
echo "<pre>";
print_r($result);
echo "</pre>";exit;
*/
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions         ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
   function hrefBack1(){
      window.location='manageFabricPrice.php';
   }
</script>

</head>
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Edit </h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
         <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>" />
         <input type="hidden" name="page" value="<?= $_GET['page'] ?>" />
         <fieldset>
            <label><?= $result->type ?></label>
            <?= $_SESSION['SESS_MSG'] ?>
            <section>
                <label>Fabric Name</label>
                <div>
                	<select name="fabricName" onChange="return showTypeOnNameChange(this.value);">
                		<option value="">Select Fabric Name</option>
                		<?= $fabricObj->getFabricName($result->fId)?>
                	</select> 
                	<?= $arr_error[fabricName]?>		                  	                 	
			    </div>
            </section>
            
             <section>
                  <label for="FabricType">Fabric Type</label>
                  <div>                  
                  		<span style="font-size: 16px;" id="fabricType">
                  			<?= $fabricObj->fetchValue(TBL_FABRIC, "type", "id = ".$result->fId)?>
                  		</span>                  	                  	  
                  </div>
             </section>
             
             <section>
                  <label>Percent(%) </label>
                  <div>
                  	   <input type="text" name="percent" id="m__percent"  value="<?= $result->percent?>" style="width:130px;" />	                  
                  	   <?= $arr_error[percent]?>		                  	                  	  
                  </div>
             </section>
             
            <section>
                  <label>Pallet Name</label>
                  <div>
                  	   <select name="palletId">
                  	   	<option value="">Select Pallet</option>
                  	   		<?= $fabricObj->getPallet($result->palletId);?>
                  	   </select>	                  
                  	   <?= $arr_error[palletId]?>		                  	                  	  
                  </div>
             </section>             
          
         </fieldset>
         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">
                  <input type="submit" name="submit"   value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
      </form>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>
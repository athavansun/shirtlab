<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/*---Basic for Each Page Starts----*/

$faqObj = new Faq();
?>

<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<SCRIPT src="js/common.js" language="javascript" type="text/javascript"></SCRIPT>
<!-- Light Box Starts -->
<link rel="stylesheet" type="text/css" href="lightbox/doc/css/style.css">
<script type="text/javascript" src="lightbox/src/adapter/shadowbox-base.js"></script>
<script type="text/javascript" src="lightbox/src/shadowbox.js"></script>
<script type="text/javascript">	
Shadowbox.loadSkin('classic', 'lightbox/src/skin');
Shadowbox.loadLanguage('en', 'lightbox/src/lang');
Shadowbox.loadPlayer(['flv', 'html', 'iframe', 'img', 'qt', 'swf', 'wmp'], 'lightbox/src/player');	
window.onload = function(){
	Shadowbox.init();
};	
</script>
<!--				Ligh Box Ends 	-->

</head>
<body>

<? include('includes/header.php'); ?>
<section id="content">
   <h1>Faq <? if($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addFaq.php">Add New Faq</a><? } ?></h1>
<fieldset>
<form name="ecartFrm"  method="post" action="pass.php?action=faq&type=deleteall">
 <label>Faq Detail</label>
<input type="hidden" name="page" id="page" value="<?=$_GET['page'] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET['limit'] ?>" />

<div class="top-filter bg02">
 <div id="check" class="seclet"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" class="seclet" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
	<div id="search-main-div">
		<ul>
			<li>Action :</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission())){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>	
						<? } ?>
                  	</select>
			</li>
			<li><input type="submit" value="Submit" name="Input"></li>
			<li><input name="searchtext" type="text"  style="margin-left:0px;" value="<?=$searchtxt= $_GET['searchtxt']?$_GET['searchtxt']:SEARCHTEXT?>" onClick="clickclear(this, '<?=SEARCHTEXT?>')" onBlur="clickrecall(this,'<?=SEARCHTEXT?>')"/></li>
			<li><input name="GO" type="submit" value="Go" onKeyPress="return false;" /></li>
			<li class="showall"><a href="<?=$_SERVER['PHP_SELF']?>"><b>Reset</b></a></li>
			</ul>
</div>	
</div>
<div class=" "><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
<table class="documentation">
		<tr>
            <thead>
 			<th><input type="checkbox" name="checkall" onClick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></th>
			<th>SN</th>
            <th>Id</th>
			<th><?=orderBy("manageFaq.php","question","Question")?></th>
			<th width="55">Status</th>
			<th><input type="submit" name="sort" value="Sequence"></th>
			<th>View</th>
			<th>Edit</th>	
			<th>Delete</th>
			</thead>
		</tr>	 
	<?=$faqObj->valDetail();?>
</table>
</form>
</fieldset>
</section>
</body>
</html>


<?php /* 


<div id="nav-under-bg"><!-- --></div>
<form name="ecartFrm" method="post" action="pass.php?action=faq&type=deleteall" >
<input type="hidden" name="page" id="page" value="<?=$_GET[page] ?>" />
<input type="hidden" name="Pagelimit" id="Pagelimit" value="<?=$_GET[limit] ?>" />
<div class="main-body-div-width">
	<div class="main-body-div-header">
	<div class="main-body-header-text-top">Manage FAQ</div><span class="main-body-adduser"><b><? if($menuObj->checkAddPermission()) { ?><a href="addFaq.php">Add New Faq</a><? } ?></b></span>
	</div>
	
	<div id="search-main-div">
		<ul>
			<li class="selectall">
				<div id="check"> <a href="javascript:void(0)" class="buttontext" onclick='javascript:checkAllCheckboxes(document.ecartFrm);'>Select All</a></div>
 <div id="uncheck" style="display:none;"><a href="javascript:void(0)" class="buttontext" onclick='javascript:uncheckAllCheckboxes(document.ecartFrm);'>Unselect All</a></div>
			</li>
			<li class="action">Action:</li>
			<li>
				<select name="action">
                    	<option value="">Select Action</option>
						<? if(($menuObj->checkDeletePermission())){  ?>
                    	<option value="deleteselected">Delete Selected</option>
						<? } ?>
						<? if(($menuObj->checkEditPermission())){  ?>
                    	<option value="enableall">Enable Selected</option>	
                    	<option value="disableall">Disable Selected</option>	
						<? } ?>										
                  	</select>
			</li>
			<li><input name="Input" type="submit" value="Submit"  class=""/></li>
			
		</ul>
</div>	
<div style="margin-top:20px;"><? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?></div>
	<div class="main-body-content-text-div">
							<ul style="text-align:center;">
								<li style="width:50px;">&nbsp;&nbsp;<input type="checkbox" name="checkall" onclick="javascript:checkAllCheckboxes(document.ecartFrm)" disabled="disabled"></li>
								<li style="width:80px;">SL.No</li>
								<li style="width:180px;"><?=orderBy("manageFaqs.php","question","Question")?></li>
								<li style="width:100px;">Status</li>
								<li style="width:100px;">View</li>								
								<li style="width:125px;">Edit</li>
								<li style="width:55px;">Delete</li>
							</ul>
						</div>
	<?
		echo $viewObj->valDetail();
	?>
</div>
</div>
</form>
</body>
</html> */?>
<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$genObj = new GeneralFunctions();
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageBanner.php", "edit_record");
/* ---Basic for Each Page Starts---- */

$bannerObj = new Banner();
$generalFunctionObj = new GeneralFunctions();
$result = $bannerObj->getResult(base64_decode($_GET['id']));



if (isset($_POST['submit'])) {
        //echo "<pre>"; print_r($_POST); echo "</pre>";exit;
        require_once('validation_class.php');
        $obj = new validationclass();
        $rst = $bannerObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
        $num = $bannerObj->getTotalRow($rst);
        if ($num > 0) {
                $langIdArr = array();
                while ($line = $bannerObj->getResultObject($rst)) {
                    array_push($langIdArr, $line->id);
                }


                //Add Error===================
                foreach ($langIdArr as $key => $value) {
                    $obj->fnAdd('bannerTitle_' . $value, $_POST['bannerTitle_' . $value], 'req', 'This field is requied.');
                    //$obj->fnAdd('bannerHtmlText_' . $value, $_POST['bannerHtmlText_' . $value], 'req', 'This field is required');
                }
                if($_POST['inbuiltBannerGroup']==""){
                    $obj->fnAdd("bannerGroup", $_POST["bannerGroup"], "req", "This field is required.");
                }
                $obj->fnAdd("bannerUrl", $_POST["bannerUrl"], "req", "This field is required.");
                //$obj->fnAdd("bannerUrl", $_POST["bannerUrl"], "url", "Invalid Url.");
                

                //Validate===============
                $arr_error = $obj->fnValidate();
                $str_validate = (count($arr_error)) ? 0 : 1;


                //Get Error================
                foreach ($langIdArr as $key => $value) {
                    $arr_error['bannerTitle_' . $value] = $obj->fnGetErr($arr_error['bannerTitle_' . $value]); 
                    //$arr_error['bannerHtmlText_' . $value] = $obj->fnGetErr($arr_error['bannerHtmlText_' . $value]);        
                }
                if($_POST['inbuiltBannerGroup']==""){
                    $arr_error['bannerGroup'] = $obj->fnGetErr($arr_error['bannerGroup']);  
                }
                $arr_error['bannerUrl'] = $obj->fnGetErr($arr_error['bannerUrl']);  
                 


                //Manual Error Check==============
                if ($_FILES['bannerImage']['name']) {
                    $filename = stripslashes($_FILES['bannerImage']['name']);
                    $extension = findexts($filename);
                    $extension = strtolower($extension);
                    if (!$bannerObj->checkExtensions($extension)) {
                        $arr_error[bannerImage] = '<span class="alert-red alert-icon">Upload Only ' . $bannerObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='IMAGE_EXTENSION'") . ' image extension.</span>';
                        $str_validate=0;
                    }
                    
                    #---check banner size-------------------Begin
                    if($_POST[inbuiltBannerGroup]){
                        $bannerSize=$bannerObj->fetchValue(TBL_BANNER,'bannerSize',"bannerGroup ='".$_POST[inbuiltBannerGroup]."'");                    
                        list($w,$h)=getimagesize($_FILES['bannerImage']['tmp_name']);   
                        $uploadedSize=$w."x".$h;                
                        if($uploadedSize!=$bannerSize){
                        $arr_error[bannerImage] = '<span class="alert-red alert-icon">Uploaded image size is '.$uploadedSize.'. Uploaded images size must be <b>'.$bannerSize.'</b>.</span>';
                        $str_validate=0;
                        }               
                    }
                    #---Check Banner Size-------------------End
                }      

                
                //$str_validate==0 =>No Error => Submit Record==================
                if ($str_validate){
                    $bannerObj->editRecord($_POST, $_FILES);
                }
        }
}


?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions         ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
   function hrefBack1(){
      window.location='manageBanner.php';
   }
</script>

</head>
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Edit Banner</h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
         <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>" />
         <input type="hidden" name="page" value="<?= $_GET['page'] ?>" />
         <fieldset>
            <label>Banner</label>
            <?= $_SESSION['SESS_MSG'] ?>
            <section>
               <label for="BannerTitle">Banner Title</label>
               <div>
                  <?= $genObj->getLanguageEditTextBox('bannerTitle','m__bannerTitle',TBL_BANNER_DESCRIPTION,base64_decode($_GET['id']),"bannerId",$arr_error); ?>
               </div>
               
            </section>
            <section>
               <label for="BannerURL">Banner URL</label>
               <div>
                  <input type="text" name="bannerUrl" id="m__bannerUrl" class="wel" value="<?= stripslashes($result->bannerUrl) ?>" />
                  &nbsp;Ex :- http://<?= $_SERVER['HTTP_HOST'] ?>
                  <?= $arr_error['bannerUrl'] ?>
               </div>
               
            </section>

            <!-- Start : Banner Group  -------- -->
            <section>
               <label for="BannerGroup">Banner Group</label>
               <div><span><?php echo $result->bannerGroup." ( ".$result->bannerSize." )";?></span></div>   
               <input type="hidden" name="inbuiltBannerGroup" value="<?=$result->bannerGroup?>" >
            </section>
            
            <section>
               <label for="clipArtImage">Banner Image</label>
               <div>
                   <input type="file"  name="bannerImage" id='bannerImage'  class="browse-button" value="" />
                   <?= $arr_error['bannerImage'] ?>                  
               </div>
            </section>
            
            
            
<!--            <section>
               <label for="HTMLText">HTML Text</label>
               <div>
                  <?= $genObj->getLanguageEditTextarea('bannerHtmlText','m__bannerHtmlText',TBL_BANNER_DESCRIPTION,base64_decode($_GET['id']),"bannerId",$arr_error3); //1->type,2->name,3->id,4->tablename,5->tableid  ?>
               </div>
            </section>
            <section>
               <label for="clipArtImage">Scheduled At</label>
               <div><input  type="text" name="startDate" id="startDate"  class="wel" readonly="true" value="<?= $result->startDate ?>" /></div>
            </section>
            <section>
               <label for="clipArtImage">Expires On</label>
               <div><input  type="text" name="endDate" id="endDate" class="wel" value="<?= $result->endDate ?>" /></div>
            </section>
            <section>
               <label for="clipArtImage">Impression/Views</label>
               <div><input  type="text" name="expires_impressions" class="wel" value="<?= $result->expires_impressions ?>" /></div>
            </section>-->
            
            
            
            <section>
               <label for="OpenIn">Open In</label>
               <div><input type="radio" name="openIn" value="0" <? if ($result->openIn == 0) { ?> checked="checked"<? } ?> />
                  Same Window
                  <input type="radio" name="openIn" value="1" <? if ($result->openIn == 1) { ?> checked="checked"<? } ?> />
                  Another Window </div>
            </section>
         </fieldset>
         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">
                  <input type="submit" name="submit"   value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
               </div>
            </section>
         </fieldset>
      </form>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>

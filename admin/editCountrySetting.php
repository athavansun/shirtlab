<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCountrySetting.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$countryObj = new CountrySetting();
if(isset($_POST['submit'])) {	
	require_once('validation_class.php');
	$obj = new validationclass();
		
	$chk = '';
	if($_POST[eoriNo] == '1') {	
		$chk1 = "checked = 'checked'";					
	}else {
		$chk0 = "checked = 'checked'";
	}
		
	$obj->fnAdd('countryId',$_POST['countryId'], 'req','Please Select Country Name');
	$obj->fnAdd('currencyId',$_POST['currencyId'], 'req','Please Select Currency');
	$obj->fnAdd('vat',$_POST['vat'], 'req','Please Enter Vat in %');
	$obj->fnAdd('customDuty',$_POST['customDuty'], 'req','Please Enter Custom Duty in %');
	foreach($langIdArr as $key=>$value){                   
        $obj->fnAdd('message_'.$value,$_POST['message_'.$value], 'req','Please Enter Buisness Rule');	 }
		
				
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;

	$arr_error['countryId']=$obj->fnGetErr($arr_error['countryId']);
	$arr_error['currencyId']=$obj->fnGetErr($arr_error['currencyId']);
	$arr_error['vat']=$obj->fnGetErr($arr_error['vat']);
	$arr_error['customDuty']=$obj->fnGetErr($arr_error['customDuty']);
	foreach($langIdArr as $key=>$value) {
        $arr_error['message_'.$value]=$obj->fnGetErr($arr_error['message_'.$value]);
    }
	
	if(! count($_POST['lang']) > 0)
	{
		$arr_error[language] = '<span class="alert-red alert-icon">Please select Language(s).</span>';
		$str_validate = 0;
	}
	//print_r($arr_error);
	if($str_validate){
		//$_POST = postwithoutspace($_POST);
            $countryObj->editCountrySetting($_POST);
	}
	
} else {	
	$id = $_GET['id'] ? $_GET['id'] : 0;
	$id = base64_decode($id);
	$_POST = $countryObj->getResult($id);
	$chk = '';
	if($_POST[eoriNo] == '1') {	
		$chk1 = "checked = 'checked'";					
	}else {
		$chk0 = "checked = 'checked'";
	}
	//~ echo "<pre>";
	//~ print_r($_POST);exit;
}
?>
<?=headcontent();// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<!--  <script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script> 		 -->
<script type="text/javascript">
function hrefBack1(){
	window.location='manageCountrySetting.php';
}

</script>
<!-- New Drop Down menu -->

<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Edit Settings</h1><fieldset>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">	
        <input type="hidden" name="id" value="<?= $_POST[id] ?>" >	
		 <fieldset>  
            <label>Country Settting</label>
			<?=$_SESSION['SESS_MSG']?>
          	<section>
               	<label>Select Country</label>
               	<div>
					<select name="countryId">
						<option value="">---Select Country---</option>
						<?=$countryObj->getCountry($_POST[countryId]);?>
					</select>
					<?=$arr_error[countryId]?>
               	</div>                  	           	
			</section>
			
			<section>
               	<label>Select Currency</label>
               	<div>
					<select name="currencyId">
						<option value="">---Select Currency---</option>
						<?=$countryObj->getCurrency($_POST[currencyId]);?>
					</select>
					<?=$arr_error[currencyId]?>
               	</div>                  	           	
			</section>
			<section>
				<label for="userEmail">Select Language's</label>
				<div>
					<?=$countryObj->getlangbox($countryObj->getCountryLang($id));?>
					<?=$arr_error[language]?>
				</div>
			</section>
			
			<section>
				  <label>Vat in %</label>
				  <div>
					<input type="text" name="vat" id="m__vat"  value="<?=$_POST[vat]?>" style="width:155px" />
					<?=$arr_error[vat]?>
				  </div>					 
			</section> 		
			
			<section>
				  <label>Custom Duty in %</label>
				  <div>
					<input type="text" name="customDuty" id="m__customDuty"  value="<?=stripslashes($_POST[customDuty])?>" style="width:155px" />
					<?=$arr_error[customDuty]?>
				  </div>					 
			</section> 		
			
			<section>
                <label>Text Buisness Rules: </label>
                <div>
                <?= $genObj->getLanguageEditEditor('message','m__message',TBL_COUNRTYSETTINGDESC, base64_decode($_GET['id']),"settingId", $arr_error);?> 
                </div>
            </section>	
			
			<section>
				  <label>EORI Custom-</label>
				  <div>
					<input type="radio" name="eoriNo" id="m__eoriNo" value="1" <?= $chk1; ?> /><span style="margin-left:5px; font-size:13px">Yes</span>
					<input type="radio" name="eoriNo" id="m__eoriNo" value="0" <?= $chk0; ?> style="margin-left:20px;" /><span style="margin-left:5px; font-size:13px">No</span>
				  </div>					 
			</section> 	
			
		</fieldset>
        <fieldset> 
           <section>  
             <label>&nbsp;</label>
             <div style=" width:78%;">
                <input type="submit" name="submit"   value="Submit" />
                <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
             </div>
           </section>
        </fieldset>
        </form> </fieldset>
	</section>
	<? include_once('includes/footer.php');?>
<? unset($_SESSION['SESS_MSG']); ?>

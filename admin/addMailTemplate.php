<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageMailType.php","add_record");

/*---Basic for Each Page Ends----*/

$mailSettingObj = new MailSetting();
$mailTemplateId = base64_decode($_GET['mailId']);
$mailTemplateId = $mailTemplateId?$mailTemplateId:0;



if(isset($_POST['submit'])) {
        
        
	require_once('validation_class.php');
	$obj = new validationclass();
	$obj->fnAdd('languageName',$_POST['languageName'], 'req','*This field is requied.');
	$obj->fnAdd("mailsubject", $_POST["mailsubject"], "req",'*This field is requied.');
	$obj->fnAdd('email',$_POST['email'], 'req', '*This field is requied.');
	$obj->fnAdd("email", $_POST["email"], "email", '*Invalid Email Address.');       
	$obj->fnAdd("message", strip_tags($_POST["message"]), "req", '*This field is requied.');
	
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[mailsubject]=$obj->fnGetErr($arr_error[mailsubject]);
	$arr_error[email]=$obj->fnGetErr($arr_error[email]);
        $arr_error[mailfrom]=$obj->fnGetErr($arr_error[mailfrom]);
	$arr_error[message]=$obj->fnGetErr($arr_error[message]);
        //echo "<pre>";print_r($arr_error); echo "</pre>";exit;
	if($str_validate){
            
		//$_POST = postwithoutspace($_POST);
		$mailSettingObj->addNewMailTemplate($_POST);
	}
}

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageBrand.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Mail Template</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			<fieldset>
				
				<label>Add Mail Template</label>
				<?=$_SESSION['SESS_MSG']?>
				<span class="alert-red alert-icon">Must keep containt inside [] as in other templates of same type.</span>
				<!-- Start : Language Name ---- --->
				<section>
					  <label for="LanguageName">Language Name </label>
					  <div>					  
					  	<select name="languageName" id="m__Language"  >
							<?=$mailSettingObj->getRemainingLanguage($mailTemplateId,$_POST['languageName']);?>
						</select>
						<?=$arr_error[languageName]?>
					  </div>	
				</section>
				
				<!-- Start : Subject ---- --->
				<section>
					  <label for="Subject">Subject</label>
					  <div> 
					  	<textarea name="mailsubject" cols="3" id="m__Mail_Subject">
						<?=stripslashes($_POST[mailsubject])?>
						</textarea>
					    <?=$arr_error[mailsubject]?>
					  </div>	
				</section>	
                                
                                
				<!-- Start :To/From MailId ---- --->
				<section>
					  <label for="ToFromMailId">To/From MailId</label>
					  <div>
					  	<input type="text" name="email" id="m__email" size="3"  value="<?=stripslashes($_POST[email])?>" />
						<?=$arr_error[email]?>
					  </div>	
				</section>
				
				<!-- Start : Mail Content ---- --->
				<section>
					  <label for="MailContent">Mail Content</label>
					  <div>
					  	<?php
						$oFCKeditor = new FCKeditor('message') ;
						$oFCKeditor->BasePath = 'fckeditor/' ;
						$oFCKeditor->Height	= 400;
						$oFCKeditor->Width	= 740;
						$oFCKeditor->Value = stripslashes($_POST['message']);
						$oFCKeditor->Create();
						?>					  
					  </div>
					  <?=$arr_error[message]?>	
				</section>
				<input type="hidden" name="mailTypeId" value="<?=$mailTemplateId?>" />
				 
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack()"/>
				</div>
             </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
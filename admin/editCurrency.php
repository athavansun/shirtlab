<?php

session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCurrency.php","");
/*---Basic for Each Page Ends----*/
$genObj = new GeneralFunctions();
$catObj = new Currency();
$cid  = $_GET['id']?$_GET['id']:0;
$result = $catObj->getResult(base64_decode($_GET['id']));
/*
echo "<pre>";
print_r($result);
echo "</pre>";exit;
*/
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
    
	$obj->fnAdd("currencyName",$_POST["currencyName"], "req", "Please Enter Currency Name.");	    
	$obj->fnAdd("decimalPlace", $_POST["decimalPlace"], "req", "Please enter  Decimal Place.");
	$obj->fnAdd("currencyCode", $_POST["currencyCode"], "req", "Please enter Currency Code.");
	$obj->fnAdd("currencySign", $_POST["currencySign"], "req", "Please enter Currency Sign.");
	$obj->fnAdd("currencySeprator", $_POST["currencySeprator"], "req", "Please enter Currency Seprator.");   
	
	if(!is_numeric($_POST['decimalPlace']))
	{ 
            $obj->fnAdd('decimalPlace',$_POST['decimalPlace'], 'float', 'Decimal Place Value is wrong.');        	
	}
        
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[currencyName]=$obj->fnGetErr($arr_error[currencyName]);	
	$arr_error[decimalPlace]=$obj->fnGetErr($arr_error[decimalPlace]);
	$arr_error[currencyCode]=$obj->fnGetErr($arr_error[currencyCode]);
	$arr_error[currencySign]=$obj->fnGetErr($arr_error[currencySign]);
	$arr_error[currencySeprator]=$obj->fnGetErr($arr_error[currencySeprator]);

	
        if($str_validate) {               
   //if (empty($arr_error[firstName]) && empty($arr_error[lastName]) && empty($arr_error[postCode]) && empty($arr_error[city]) &&  empty($arr_error[countryId]) && empty($arr_error[stateId]) && empty($arr_error[phoneNo]) && empty($arr_error[status]) && isset($_POST['submit'])) {
            $_POST = postwithoutspace($_POST);
            $catObj->editRecord($_POST);
        }
    } 
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='manageCurrency.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Edit Currency</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
			<fieldset>
				
				<label>Edit Currency</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!-- Start : Currency Name ------->
				<section>
					  <label for="FirstName">Currency Name</label>
					  <div>
					  	<input type="text" name="currencyName" id="m__currencyName"  class="wel" value="<?=stripslashes($result->currencyName)?>" />
						<?=$arr_error[currencyName]?>
					  </div>					 
				</section> 
			
					<!-- Start : Symbol show in ------->
				<section>
					  <label for="FirstName">Symbol Show In</label>
					  <div>
						  <?php
								if($result->showIn == '0')
									$left = 'checked="checked"';
								else
									$right = 'checked="checked"';
						  ?>
					  	<input <?=$left;?> type="radio" name="showIn" value="0" checked="checked" /> Left 
					  	<input <?=$right;?> type="radio" name="showIn" value="1" /> Right						
					  </div>					 
				</section> 	
	
								
				<!-- Start : Decimal places ------->
				<section>
					  <label for="FirstName">Decimal Places</label>
					  <div>
					  	<input type="text" name="decimalPlace" id="m__decimalPlace"  class="wel" value="<? echo ($result->decimalPlace != "")?$result->decimalPlace:"0" ?>" />
						<?=$arr_error[decimalPlace]?>
					  </div>					 
				</section> 	
					
				<!-- Start : Make it Default------->
				<section>
					  <label for="FirstName">Make it Default</label>
					  <?php
							$sel = $result->isDefault == '0' ? '' : 'checked="checked"';									
						  ?>
					  <div>
					  	<input type="checkbox" name="isDefault" value="1" <?=$sel;?> />						
					  </div>					 
				</section> 	
				
				<!-- Start : Currency Seprator ------->
				<section>
					  <label for="FirstName">Currency Seprator</label>
					  <div>
					  	 <input type="text" name="currencySeprator" id="m__currencySeprator"  class="wel" value="<?=stripslashes($result->SeparatorFrom)?>" />
						<?=$arr_error[currencySeprator]?>
					  </div>					 
				</section> 		
				
				<!-- Start : Currency Code------->
				<section>
					  <label for="FirstName">Currency Code</label>
					  <div>
					  	<input type="text" name="currencyCode" id="m__currencyCode"  class="wel" value="<?=stripslashes($result->currencyCode)?>" />
						<?=$arr_error[currencyCode]?>
					  </div>					 
				</section>		
						
				<!-- Start : Currency Sign------->
				<section>
					  <label for="FirstName">Currency Sign</label>
					  <div>
					  	<input type="text" name="currencySign" id="m__currencySign"  class="wel" value="<?=stripslashes($result->sign)?>" />
					  	
					  	<?=$arr_error[currencySign]?>
					  </div>					 
				</section>		
						
					
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					<input type="hidden" name="id" id="id" value="<?= $result->currencyDetailId;?>" />
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
				</div>
             </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); 

<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//require_once('includes/classes/deliveryTime.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */
$product = new PlainProduct();

if(isset($_POST['deleteAccessories'])) {
	$langIdArr = array();    
	
	require_once('validation_class.php');
	$obj = new validationclass();
	
    $obj->fnAdd('accessories', $_POST['accessories'], 'req', 'Please Select Fabric Type.');    
                   
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error['accessories']=$obj->fnGetErr($arr_error['accessories']);
	
	if($str_validate) {	
		//$rst = $accObj->deleteAccessories($_POST['accessories']);				
	}
}

//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
 
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
     	  <h1>Plain Product Discount Panel</h1>	   
         <!-- <h1>Embroidery</h1>	 --> 
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onsubmit="return validateFrm(this);">
   <?		echo $_SESSION['SESS_MSG'];                
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
            	<label>Create Discount</label>
                <section>
                	<label>Select Discount</label>
                	<div>
						<select id ="accessories" name="accessories" onchange="return viewAccessories(this.value);">
							<option value="">Select Fabric Type</option>
							<?= $product->getFabricType();?>
						</select>		                        
						<input type="button" id="accDiscount" value="Add Discount" onclick="return addDiscount();" style="margin-left:33px;" />						
                		<?=$arr_error[accessories]?>
                	</div>                  	           	
                </section>
            </fieldset>
			
            <fieldset id="addAccessoriesDiscount" style="display:none;">
            </fieldset>
                                    
            <fieldset id="showHeading" style="display:none;">            	
            </fieldset>                      
		<!--<fieldset id="showAccessories" style="display:none">		 			 				 		
		 	</fieldset>		   
            	
     		<div class="main-body-sub" id="btnSubmit" style="text-align:center; margin-left:0px; display:none;">
                <button type="submit" name="submit">Submit</button>
            </div>	     
            <div id="divTemp" style="display:none;"></div>			-->	
         </form>
      </fieldset>
   </section>
<?php include_once('includes/footer.php'); ?>  
<script type="text/javascript">
	function addDiscount() {		        
		var fId = document.getElementById("accessories").value;		
		window.location = "addAccessoriesDiscount.php?fId="+fId;
	}
</script>

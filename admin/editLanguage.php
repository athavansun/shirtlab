<?
/*---Basic for Each Page Starts----*/
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageLanguage.php","edit_record");
/*---Basic for Each Page Ends----*/

$langObj = new Language();
$genObj = new GeneralFunctions();
$id  = base64_decode($_GET['id'])?base64_decode($_GET['id']):0;
$result = displayWithStripslashes($langObj->getResult($id));

require_once('validation_class.php');
$obj = new validationclass();


if(isset($_POST['submit'])) {
	
	$obj->fnAdd('languageName',$_POST['languageName'], 'req', 'Please Enter Language Name.');
	$obj->fnAdd("languageCode", $_POST["languageCode"], "req", "Please enter  Language Code.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[languageCode]=$obj->fnGetErr($arr_error[languageCode]);

	if($langObj->islanguageNameExit($_POST['languageName'],$id)){ 
            $arr_error[languageName] = '<span class="alert-red alert-icon">Language Name Already Exists.</span>';
            $str_validate=0;
        }
	if($langObj->islanguageCodeExit($_POST['languageCode'],$id)){ 
            $arr_error[languageCode] =  '<span class="alert-red alert-icon">Language Code Already Exists.</span>';
            $str_validate=0;
        }
		if($_FILES['languageFlag']['name']){
		$filename = stripslashes($_FILES['languageFlag']['name']);
		$extension = findexts($filename);
		$extension = strtolower($extension);	 	 
			if(!$langObj->checkExtensions($extension)){ 
				 $arr_error[languageFlag] = '<span class="alert-red alert-icon">Upload Only '.$langObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") .' image extension.</span>';
                                 $str_validate=0;
			}
		}
	
	if($str_validate){
            $_POST = postwithoutspace($_POST);
            $langObj->editLanguage($_POST,$_FILES);
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script src="js/file/jquery.filestyle.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script type="text/javascript">
function hrefBack1(){
	window.location='manageLanguage.php';
}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
        <section id="content">
            <h1>Language</h1>
            <fieldset>
                <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
                    <label>Edit Language</label>
                    <input type="hidden" name="id" value="<?=$id?>">		
                    <input type="hidden" name="page" value="<?=$_GET['page']?>">	
                    <div id="mainDiv">
                    <div><?=$_SESSION['SESS_MSG']?></div>
                    <fieldset>  
                        <section>
                        <label>Language Name<span class="spancolor">*</span></label>
                        <div><input type="text" name="languageName" id="m__Language__Name" value="<?=$_POST[languageName]?$_POST[languageName]:$result[languageName]?>"/>
                        <?=$arr_error['languageName'] ?>				  
                        </div>
                        </section>
                        
                        
                        <section>
                        <label>Language Code<span class="spancolor">*</span></label>
                        <div><input type="text" name="languageCode" id="m__Language_Code" readonly="true" value="<?=$result[languageCode]?>"/>
                        <?=$arr_error['languageCode'] ?>				  
                        </div>
                        </section>
                        
                        <section>
                        <label>Language Flag<span class="spancolor">*</span></label>                        
                        <div>
                        <?php if($result[languageFlag]){ echo "<img src='".FLAGPATH.$result[languageFlag]."'>";} ?>                        
                        </div>
                        <div><input class="browse-button" type="file" name="languageFlag" id="m__Language__Flag"  />
                        <?=$arr_error['languageFlag'] ?>
                        </div>
                        </section>
                        
                        <section>
                        <label>Make Default<span class="spancolor">*</span></label>
                        <div><input type="checkbox" name="isDefault" <?php if($result[isDefault] == '1'){ echo "checked=checked"; echo " readonly";} ?> />
                        </div>
                        </section>
                        

                    </fieldset>
                    
                    
                    <fieldset>	 
                        <section>   
                        <label>&nbsp;</label>
                        <div style=" width:78%;">
                        <input type="submit" name="submit" class="main-body-sub-submit" style="cursor:pointer;" value="Submit" />
                        <input type="button" name="back" id="back" value="Back" class="main-body-sub-submit" style="cursor:pointer;"  onclick="javascript:;hrefBack1()"/>
                        </div>
                        </section>
                    </fieldset>
                    </div>
                </form>
            </fieldset>
        </section>
 <div id="divTemp" style="display:none;"></div>
<? include_once('includes/footer.php');?>    
<? unset($_SESSION['SESS_MSG']); ?>

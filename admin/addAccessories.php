<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//require_once('includes/classes/deliveryTime.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageAccessoriesDiscount.php","");
$genObj = new GeneralFunctions();
/* ---Basic for Each Page Starts---- */
$accObj = new Accessories();

if (isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	
	$rst = $accObj->selectQry(TBL_LANGUAGE,"status='1'  AND isDeleted='0' and isDefault ='1' order by languageName asc","","");		
	$num = $accObj->getTotalRow($rst);	
	if($num){
		$langIdArr = array();		
		while($line = $accObj->getResultObject($rst)) {	
			array_push($langIdArr,$line->id);
		}
		foreach($langIdArr as $key=>$value) {
			$obj->fnAdd('accessories_'.$value,$_POST['accessories_'.$value], 'req','Please Enter Accessories Name.');
		}

		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
		
		foreach($langIdArr as $key=>$value) {
			$arr_error['accessories_'.$value]=$obj->fnGetErr($arr_error['accessories_'.$value]);
		}		
		
		if($str_validate) {		
			$_POST = postwithoutspace($_POST);
		    $accObj->addAccessories($_POST);		
		}
	}
}
//echo $_SESSION['CURRENTMENUID'];

?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function hrefBack1(){
		window.location='manageAccessoriesDiscount.php';
	}
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">     	  	   
         <h1>Accessories</h1> 
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);">
   <?		echo $_SESSION['SESS_MSG'];           
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
            	<label>Create Accessories</label>
                <section>
                	<label>Accessories Name</label>
                	<div>
                		<?=$genObj->getLanguageTextBoxEngValidation('accessories','m__accessories',$arr_error);?>
                	</div> 	
                </section>
			</fieldset> 
            <div class="main-body-sub" style="text-align:center; margin-left:0px">                          
               	<input type="submit" name="submit"   value="Submit" />
               <input type="button" onclick="javascript:hrefBack1()" value="Back" id="back" name="back">
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>
<?php
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageGift.php","");
$genObj = new GeneralFunctions();
$giftObj = new GiftCertificate();

if(isset($_POST['submit'])) {
	require_once('validation_class.php');
	$obj = new validationclass();
	$errorArr = 0;

	$obj->fnAdd("name",$_POST["name"], "req", "Please Enter Name");
	$obj->fnAdd("image",$_FILES["image"]["name"], "req", "Please Select Gift Image.");	
	$obj->fnAdd("color",$_POST["color"], "req", "Please Select Color.");
	
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1; 
	
	$arr_error['name']=$obj->fnGetErr($arr_error['name']);
	$arr_error['image']=$obj->fnGetErr($arr_error['image']);
	$arr_error['font']=$obj->fnGetErr($arr_error['font']);
	$arr_error['color']=$obj->fnGetErr($arr_error['color']);
	
	$filename = stripslashes($_FILES['image']['name']);
	$extension = findexts($filename);
	$extension = strtolower($extension);	 	 
	if(!$giftObj->checkExtensions($extension) && $filename) {
		$arr_error[image] = "Upload Only ".$giftObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'") ." image extension.";
		if($arr_error[categoryImage]) 
			$errorArr = 1;	
	}
	
	if($errorArr == 0 && $str_validate){
		$_POST = postwithoutspace($_POST);
		$giftObj->uploadTemplate($_POST,$_FILES);
	}
}

?>
<head>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<!--				Color Picker (START)		-->
<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)		-->
</head>
<body>
	<section id="content-detail">
     <form name="template" id="template" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data" >
 	<fieldset>
    <label>Add Template</label>
	<?=$_SESSION['SESS_MSG']?>
          <section>
       	  <label>Template Name</label>
             <div>
			 	<input type="text" name="name" id="m__name" value="<?=$_POST['name'] ?>">
				<?=$arr_error['name']?>
            </div>
          </section>
          <section>
       	  <label>Template Image</label>
             <div>
			 	<input type="file" name="image" class="browse-button" id="m__image" >
				<?=$arr_error['image']?>
            </div>
          </section>         
          <section>
       	  <label>Select Color</label>
             <div>
			 	<input type="text" name="color" id="m__color" style="width:80px; height:30px;"  value="<?=$_POST['color'] ?>">&nbsp;&nbsp;<img alt="col_code" src="colorpicker/color.gif" onClick="showColorPicker(this,document.template.color)" border="0" style="cursor:pointer;">
				<?=$arr_error['color']?>
            </div>
          </section>
		<section>  
           <label>&nbsp;</label>
           <div>
            <input type="submit" name="submit"   value="Submit" />
           </div>
        </section>
     </fieldset> 
     </form>  
    </section>        
</body>
</html>
<? unset($_SESSION['SESS_MSG']); ?>

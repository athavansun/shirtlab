<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageSizeGroup.php", "");
/* ---Basic for Each Page Starts---- */
$sizeObj = new Size();
$generalFunctionObj = new GeneralFunctions();

if (isset($_POST['submit'])) {
   require_once('validation_class.php');
   $obj = new validationclass();
   $errorArr = 0;
   $rst = $sizeObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
   $num = $sizeObj->getTotalRow($rst);
   
   if ($num > 0) {
      $langIdArr = array();
      while ($line = $sizeObj->getResultObject($rst)) {
         array_push($langIdArr, $line->id);
      }
      foreach ($langIdArr as $key => $value) {
         $obj->fnAdd('groupName_' . $value, $_POST['groupName_' . $value], 'req', "Please Enter Group Name");
      }
      $arr_error = $obj->fnValidate();
      $str_validate = (count($arr_error)) ? 0 : 1;

      foreach ($langIdArr as $key => $value) {
         $arr_error['groupName_' . $value] = $obj->fnGetErr($arr_error['groupName_' . $value]);
      }
      
      foreach($langIdArr as $key=>$value) {					  
		if($sizeObj->checkGroupNameExists($_POST['groupName_'.$value],$value, base64_decode($_GET['id']))) {				
			$arr_error['groupName_'.$value] ='<span class="alert-red alert-icon">Group Name already exist</span>';
			if($arr_error['groupName_'.$value]) 
			$errorArr = 1;							
		}
	  }
			
	  if($str_validate and $errorArr == 0){				  
		$sizeObj->editGroupName($_POST);
  	  }
   }
}
$result = $sizeObj->getSizeGroup(base64_decode($_GET['id']));
?>
<?= headcontent(); // DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>
<script src="js/file/custom-form-elements.js"></script>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script src="js/file/add-more.js"></script>
<script type="text/javascript">
   function hrefBack1(){
      window.location='manageSizeGroup.php';
   }
</script>
<!-- New Drop Down menu -->
<!-- New Drop Down menu -->
</head>
<body>
<? include('includes/header.php'); ?>
   <section id="content">
      <h1>Miscellaneous</h1>
      <fieldset>
         <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>">
            <input type="hidden" name="page" value="<?= $_GET['page'] ?>">
            <fieldset>
               <label>Edit Size</label>
<?= $_SESSION['SESS_MSG'] ?>
               <section>
                  <label>Name</label>
                  <div>
                 <?= $generalFunctionObj->getLanguageEditTextBox('groupName', 'm__groupName', TBL_SIZE_GROUPDESC, base64_decode($_GET['id']), "id", $arr_error) ?>
                  </div>
               </section>
            </fieldset>

            <fieldset>
               <section>  
                  <label>&nbsp;</label>
                  <div style=" width:78%;">
                     <input type="submit" name="submit"   value="Submit" />
                     <input type="button" name="back" id="back" value="Back" onclick="javascript:;hrefBack1()"/>
                  </div>
               </section>
            </fieldset>
         </form>
      </fieldset>
   </section>
<? unset($_SESSION['SESS_MSG']); ?>

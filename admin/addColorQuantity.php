<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("managePrintingDiscount.php","");
$colorObj = new Printing();
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('color',$_POST['color'], 'req', 'Please Enter Number of Colors.');    	
	$obj->fnAdd('charges',$_POST['charges'], 'req', 'Please Enter Value-Points.');
        
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	$arr_error[color]=$obj->fnGetErr($arr_error[color]);	
	$arr_error[charges]=$obj->fnGetErr($arr_error[charges]);
	
	if($str_validate){
		$_POST = postwithoutspace($_POST);
		$colorObj->addColorQuantity($_POST);		                
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
function hrefBack1(){
	window.location='managePrintingDiscount.php';
}

</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Add Color Quantity</h1>
         <form name="frmUser" id="frmUser" method="post" onsubmit="javascript: return validateFrm(this);" >
			<fieldset>
				
				<label>Color Quantity</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!-- Start : Currency Name ------->
				<section>
					  <label for="FirstName">Number of Colors</label>
					  <div>
					  	<input type="text" name="color" id="m__color"  class="wel" value="<?=stripslashes($_POST[color])?>" style="width:200px;" />
						<?=$arr_error[color]?>
					  </div>					 
				</section> 
																
				<section>
					  <label for="FirstName">Value-Points</label>
					  <div>
					  	<input type="text" name="charges" id="m__charges"  class="wel" value="<?=stripslashes($_POST[charges])?>" style="width:200px;" />
						<?=$arr_error[charges]?>
					  </div>					 
				</section>
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />					
                    <input type="button" name="back" id="back" value="Back"   onclick="javascript:hrefBack1()"/>       
				</div>
             </section>
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
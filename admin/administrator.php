<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */

$admObj = new AdminDetail();
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js,Top Pageoptions ?>

<? include('includes/header.php'); ?>

</head>
   <body>
      <section id="content">
         <div class="g12">
             <h1>Admin Details<? if ($menuObj->checkAddPermission()) { ?><a href="addAdmin.php" class="btn small fr">Add New Admin</a><? } ?></h1><fieldset>
              <?= $_SESSION['SESS_MSG'] ?>
            <table class="documentation">
               <? echo $admObj->valDetail(); ?>
            </table>
                 </fieldset>
         </div>
      </section>
   </body>
</html>

<?php unset($_SESSION['SESS_MSG']); ?>


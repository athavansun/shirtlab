<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageNewsletter.php","add_record");
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();

require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('language',$_POST['language'], 'req', 'Please Select Language Name.');
	$obj->fnAdd("mailsubject", $_POST["mailsubject"], "req", "Please enter  Mail Subject.");
	$obj->fnAdd('email',$_POST['email'], 'req', 'Please enter Email Address.');
	$obj->fnAdd("email", $_POST["email"], "email", "Please enter valid Email Address.");
	$obj->fnAdd("message", strip_tags($_POST["message"]), "req", "Please enter Mail Containt.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[mailsubject]=$obj->fnGetErr($arr_error[mailsubject]);
	$arr_error[email]=$obj->fnGetErr($arr_error[email]);
	$arr_error[message]=$obj->fnGetErr($arr_error[message]);
        
	if(empty($arr_error[languageName]) && empty($arr_error[mailsubject]) && empty($arr_error[email]) && empty($arr_error[message]) && isset($_POST['submit'])){
		//$_POST = postwithoutspace($_POST);
		$newsletterObj->addNewNewsletter($_POST,$_FILES);
	}
}
?>

<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageNewsletter.php';
	}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Newsletter</h1>
       
                <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			<fieldset>
				
				<label>Add Newsletter</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!--- Start : Template Name------------------>
				<section>
					<label for="TemplateName">Template Name </label>					  
					  <div>
					  	<select name="templateName" id="templateName"  onChange="changedropdownVal('addNewsletter.php')" ><?=$newsletterObj->getTemplateInDropDown($_GET['dropval']);?>
						</select>					  
					  </div>	
				</section>
				
				<!--- Start : Language------------------>
				<section>
					<label for="Language">Language </label>					  
					  <div>
					  <select name="language" id="m__Language" >
					 	 <?=$generalFunctionObj->getLanguageInDropDown($_POST['language']);?>
					  </select>
					  <?=$arr_error[language]?>
					  </div>	
				</section>
				
				<!--- Start : Subject------------------>
				<section>
					<label for="Subject">Subject </label>					  
					  <div>
					  <textarea name="mailsubject"   id="m__Mail_Subject" >
					  	<?=stripslashes($_POST[mailsubject])?>
					  </textarea>
					  <?=$arr_error[mailsubject]?>
					  </div>
				</section>
				
				<!--- Start : From Mail ID------------------>
				<section>
					<label for="FromMailID">From Mail ID</label>					  
					  <div>
					  <input type="text" name="email" id="m__email" size="3" value="<?=stripslashes($_POST[email])?>" />
					  <?=$arr_error[email]?>
					  </div>	
				</section>
				
				<!--- Start : Attachment------------------>
				<section>
					<label for="Attachment">Attachment</label>					  
					  <div><input type="file" class="browse-button" name="upload" id='upload'  value="" /></div>	
				</section>
				
				<!--- Start : Template Name------------------>
				<section>
					<label for="NewsletterContent">Newsletter Content </label>					  
					  <div>
                     <?php
						$message = $_POST['message'] ? $_POST['message']:$newsletterObj->getTemplateContaints($_GET['dropval']);
						$oFCKeditor = new FCKeditor('message') ;
						$oFCKeditor->BasePath = 'fckeditor/' ;
						$oFCKeditor->Height	= 400;
						$oFCKeditor->Width	= 740;
						$oFCKeditor->Value = stripslashes($message);
						$oFCKeditor->Create();
					?>
					  <?=$arr_error[brandName]?>
					  </div>	
				</section>
				
				
				
				 
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
				</div>
             </section>             
        </fieldset>
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */
$genObj = new GeneralFunctions();
$sysObj = new SystemConfig();

if (isset($_POST['submit'])) {
   //echo "<pre>";print_r($_POST);exit;
   $sysObj->addConfiguration($_POST, $_FILES);   
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>System Config Settings</h1>
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);"
               enctype="multipart/form-data">
                  <? echo $_SESSION['SESS_MSG'];                
unset($_SESSION['SESS_MSG']); ?>

            <fieldset>
               <label>General Configuration</label>
               <section>
                  <label>Site Name</label>
                  <div>
                     <input type="text" name="SITE_NAME" id="SITE_NAME" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'SITE_NAME'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Site Url</label>
                  <div>
                     <input type="text" name="SITE_URL" id="SITE_URL" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'SITE_URL'")) ?>" class="wel" />
                  </div>
               </section>

               <section>
                  <label>Site Email</label>
                  <div><input name="SITE_EMAIL" id="SITE_EMAIL" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'SITE_EMAIL'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
	                  <label>Image Extension Allowed</label>
	                  <div>	<input name="IMAGE_EXTENSION" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='IMAGE_EXTENSION'")?>" class="wel" /></div>	
               </section>
            </fieldset>

            <fieldset>
               <label>Front End Configuration</label>
               <section>
	                  <label>Number of Products on search Page</label>
	                  <div><input name="NUMBER_OF_PRODUCT_ON_SEARCH" type="text" value="<?=$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='NUMBER_OF_PRODUCT_ON_SEARCH'")?>" class="wel" /></div>	
               </section>
               <section>
                  <label>Default Country :</label>
                  <div>
                    <select name="DEFCOUNTRY" onchange="return changeConfigLanguage(this.value);">
                            <?=$genObj->getSetCountryInDropdown($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='DEFCOUNTRY'"));?>
                    </select>
                  </div>
               </section>
               <section>
                  <label>Default Language :</label>                  
                  <div>
                        <select id="DEFLANGUAGE" name="DEFLANGUAGE">
                                <?=$sysObj->getLanguage($sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","1 and systemName ='DEFCOUNTRY'"),$sysObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal", " systemName ='DEFLANGUAGE'"));?>
                        </select>
                  </div>
               </section>
            </fieldset>

          <fieldset>
               <label>Social Community Site Configuration</label>
               <section>
                  <label>Facebook :</label>
                  <div>
                     <input name="FACE_BOOK_URL" id="FACE_BOOK_URL" type="text"
value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1
and systemName = 'FACE_BOOK_URL'")) ?>" class="wel" />
                  </div>
                  <?php
                    $fb_status = $sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'FACE_BOOK_STATUS'");
                    if($fb_status) {
                        $fb_yes = "checked = 'checked'";
                        $fb_no  = "";
                    } else {
                        $fb_no  = "checked = 'checked'";
                        $fb_yes = "";
                    }
                  ?>
                  <label>Visibility :</label>
                  <div>
                     True <input name="FACE_BOOK_STATUS" id="FACE_BOOK_STATUS" <?=$fb_yes; ?> type="radio" value="1" />
                     False <input name="FACE_BOOK_STATUS" id="FACE_BOOK_STATUS" <?=$fb_no; ?> type="radio" value="0" />
                  </div>
               </section>

               <section>
                  <label>Twitter  :</label>
                  <div>
                     <input name="TWITTER_URL" id="TWITTER_URL" type="text"
value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1
and systemName = 'TWITTER_URL'")) ?>" class="wel" />
                  </div>
                  <?php
                    $tw_status = $sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'TWITTER_STATUS'");
                    if($tw_status) {
                        $tw_yes = "checked = 'checked'";
                        $tw_no  = "";
                    } else {
                        $tw_no  = "checked = 'checked'";
                        $tw_yes = "";
                    }
                  ?>
                  <label>Visibility :</label>
                  <div>
                     True <input name="TWITTER_STATUS" id="TWITTER_STATUS1" <?=$tw_yes; ?> type="radio" value="1" />
                     False <input name="TWITTER_STATUS" id="TWITTER_STATUS" <?=$tw_no; ?> type="radio" value="0" />
                  </div>
               </section>
               <section>
                  <label>Flickr :</label>
                  <div>
                     <input name="FLICKR_URL" id="FLICKR_URL" type="text"
value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1
and systemName = 'FLICKR_URL'")) ?>" class="wel" />
                  </div>
                  <?php
                    $fl_status = $sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'FLICKR_STATUS'");
                    if($fl_status) {
                        $fl_yes = "checked = 'checked'";
                        $fl_no  = "";
                    } else {
                        $fl_no  = "checked = 'checked'";
                        $fl_yes = "";
                    }
                  ?>
                  <label>Visibility :</label>
                  <div>
                     True <input name="FLICKR_STATUS" <?=$fl_yes; ?> type="radio" value="1" />
                     False <input name="FLICKR_STATUS" <?=$fl_no; ?> type="radio" value="0" />
                  </div>
               </section>
               <section>
                  <label>Picasa :</label>
                  <div>
                     <input name="PICASA_URL" id="PICASA_URL" type="text"
value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1
and systemName = 'PICASA_URL'")) ?>" class="wel" />
                  </div>
                  <?php
                    $pi_status = $sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'PICASA_STATUS'");
                    if($pi_status) {
                        $pi_yes = "checked = 'checked'";
                        $pi_no  = "";
                    } else {
                        $pi_no  = "checked = 'checked'";
                        $pi_yes = "";
                    }
                  ?>
                  <label>Visibility :</label>
                  <div>
                     True <input name="PICASA_STATUS" id="PICASA_STATUS1" <?=$pi_yes; ?> type="radio" value="1" />
                     False <input name="PICASA_STATUS" id="PICASA_STATUS" <?=$pi_no; ?> type="radio" value="0" />
                  </div>
               </section>
            </fieldset> 

             <fieldset>
                 <label>PayPal Payment Limit</label>
                 <section>
                     <label>Maximum Amount :</label>
                     <div>
                         <input type="text" name="MAX_AMOUNT" id="maxAmount" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " 1 and systemName = 'MAX_AMOUNT'")); ?>" class="wel" />
                     </div>
                 </section>                 
             </fieldset>

<!--             <fieldset>
                 <label>Bank Remittance</label>
                 <section>
                     <label>Bank Detail :</label>
                     <div>
                         <textarea name="BANK_DETAIL" id="BANK_DETAIL" class="wel"><?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'BANK_DETAIL'")) ?></textarea>
                     </div>
                 </section>                 
             </fieldset>             -->
             
            <fieldset>
               <label>Payment Configuration</label>
               <section>
                  <label>Payment Type :</label>
                  <div>
                     <input name="PAYMENT_TYPE" type="radio" value="Live" style="cursor:pointer;" <? if ($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYMENT_TYPE'") == "Live")
                                       echo "checked"; ?>/>
		Live&nbsp;<input name="PAYMENT_TYPE" type="radio" value="Demo" style="cursor:pointer;" <? if ($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYMENT_TYPE'") == "Demo")
                               echo "checked"; ?>/>&nbsp;Demo
                  </div>
               </section>
               
               <section>
                  <label>Paypal Live ID</label>
                  <div>
                     <input name="PAYPAL_ID" id="PAYPAL_ID" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_ID'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Live Password</label>
                  <div>
                     <input name="PAYPAL_PASS" id="PAYPAL_PASS" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_PASS'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Live Signature</label>
                  <div>
                     <input name="PAYPAL_SIG" id="PAYPAL_SIG" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_SIG'")) ?>" class="wel" />
                  </div>
               </section>
               
               <section>
                  <label>Paypal Demo ID</label>
                  <div>
                     <input name="PAYPAL_SANDBOX_ID" id="PAYPAL_SANDBOX_ID" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_SANDBOX_ID'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Demo Password</label>
                  <div>
                     <input name="PAYPAL_SANDBOX_PASS" id="PAYPAL_SANDBOX_PASS" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_SANDBOX_PASS'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Demo Signature</label>
                  <div>
                     <input name="PAYPAL_SANDBOX_SIG" id="PAYPAL_SANDBOX_SIG" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'PAYPAL_SANDBOX_SIG'")) ?>" class="wel" />
                  </div>
               </section>
            </fieldset>

            <fieldset>
               <label>Contact-Us Configuration</label>
               <section>
                  <label>Visit us</label>
                  <div>
                     <textarea name="VISIT_US" id="VISIT_US" class="wel"><?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'VISIT_US'")) ?></textarea>
                  </div>
               </section>

               <section>
                  <label>Email Us</label>
                  <div>
                     <input name="EMAIL_US" id="EMAIL_US" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'EMAIL_US'")) ?>" class="wel" />
                  </div>
               </section>
               <section>
                  <label>Talk To us</label>
                  <div>
                     <input name="TALK_TO_US" id="TALK_TO_US" type="text" value="<?= stripslashes($sysObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "1 and systemName = 'TALK_TO_US'")) ?>" class="wel" />
                  </div>
               </section>
            </fieldset>

            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
</body>
</html>

<?
/* ---Basic for Each Page Starts---- */
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
//require_once('includes/classes/deliveryTime.php');
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission();
/* ---Basic for Each Page Starts---- */
$embObj = new Embroidery();
if($_REQUEST['del_id'])
{
	$embObj->deleteEmbroideryMinRate($_REQUEST['del_id']);
}
if (isset($_POST['submit'])) {  
	 $langIdArr = array();    

	require_once('validation_class.php');
	$obj = new validationclass();
	$str_validate =1;
   /* $obj->fnAdd('valuePoint', $_POST['valuePoint'], 'req', 'Please Enter Value Point.');
    /*$obj->fnAdd('cm2', $_POST['cm2'], 'req', 'Please Enter minimum cm.');
   
		
	          
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	
	/*$arr_error['valuePoint']=$obj->fnGetErr($arr_error['valuePoint']);
	$arr_error['cm2']=$obj->fnGetErr($arr_error['cm2']);*/	

	if($str_validate) {	
		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";exit;	*/
		
				$embObj->editEmbroideryDiscount($_POST);
            $rst = $embObj->executeQry("Select * from ".TBL_EMBROIDERY_RATE." where 1 and type = '0' and id>1");
            $rate = $embObj->getResultObject($rst);
            $discount = $embObj->executeQry("Select * from ".TBL_EMBROIDERY_DISCOUNT." where 1 and type = '0' order by pcs");
		
	}
}else {
	$rst = $embObj->executeQry("Select * from ".TBL_EMBROIDERY_RATE." where 1 and type = '0'");
	$rate = $embObj->getResultObject($rst);	
	$discount = $embObj->executeQry("Select * from ".TBL_EMBROIDERY_DISCOUNT." where 1 and type = '0' order by pcs");
}
//echo $_SESSION['CURRENTMENUID'];
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<!--				Color Picker (START)		-->

<link rel="stylesheet" href="colorpicker/js_color_picker_v2.css" media="screen">
<script src="colorpicker/color_functions.js"></script>		
<script type="text/javascript" src="colorpicker/js_color_picker_v2.js"></script>
<!--				Color Picker (END)			-->
<script type="text/javascript">
   function show_value(){
      var sum1 = document.configUser.Allow_user_to_show_identification.length;
	     for (var i=0; i < sum1; i++) {
         if (document.configUser.Allow_user_to_show_identification[0].checked)
            document.getElementById("ident").style.display='';
         if (document.configUser.Allow_user_to_show_identification[1].checked)
            document.getElementById("ident").style.display='none';
			   
      }
   }
</script>
</head>
<body onload="show_value();">
   <? include('includes/header.php'); ?>
   <section id="content">
     	  <h1>Embroidery<? if ($menuObj->checkAddPermission()) { ?><a class="btn small fr" href="addEmbroideryDiscount.php">Add Embroidery Discount</a><? } ?></h1>	   
         <!-- <h1>Embroidery</h1>	 --> 
      <fieldset>
         <form name="configUser" id="configUser"   method="post" onSubmit="javascript: return validateFrm(this);">
   <?		echo $_SESSION['SESS_MSG'];                
			unset($_SESSION['SESS_MSG']); 	?>
						
            <fieldset>
            	<label>Emboidery Rate Panel</label>
            	
	<!--==============================================================================point 7 work ================================--->			<?php /*
                <section>
                	<label>1 cm2 is </label>
                	<div>
						<input type="text" maxlength="8"  name="valuePoint" id="m__valuePoint" value="<?=$rate->valuePoint; ?>" class="wel" style="width:250px;" /><span style="font-size: 13px; margin-left:5px;">Value-Points</span>	                		
                		<?=$arr_error[valuePoint]?>
                		<?php if($show_error){?>
                		<span class="alert-red alert-icon"><?php echo $show_error;?></span>
                		<?php }?>
                	</div>                	
                </section>
                <section>
                	<label>Min. Quantity in cm2</label>
                	<div>
						<input type="text" name="cm2" id="m__cm2" value="<?=$rate->min_cm; ?>" class="wel" style="width:250px;" /><span style="font-size: 13px; margin-left:5px;">cm2</span>
						<?=$arr_error[cm2]?>    		
                	</div>                	
                </section>
                <section></section>
              */?>
              <section>
				  <table style="width:50%" id="embroidary-tbl">
					  <?php
					   $rst = $embObj->executeQry("Select * from ".TBL_EMBROIDERY_RATE." where 1 and type = '0' and id>1 order by min_cm asc");
						while($rate = $embObj->getResultObject($rst))
						{						
					  ?>
					    <tr>
						  <td><input type="text"   name="cm2[]" id="m__cm2" value="<?php echo $rate ->min_cm;?>" class="wel emb_wel" style="width:150px;" placeholder="cm2" />cm2</td>
						  <td><input type="text" maxlength="8"  name="valuePoint[]" id="m__valuePoint" value="<?php echo $rate ->valuePoint;?>" class="wel emb_wel" style="width:150px;" placeholder="Value-Points"  />Value-Points</td>					   
						<td><a onclick="if(confirm('Are you sure to delete this Record?')){window.location.href='manageEmbroideryDiscount.php?del_id=<?php echo $rate->id;?>'}else{}" href="javascript:void(0);" class="i_trashcan edit">Delete</a></td>
					   </tr>
					  <?php }?>
					  <tr>
						  <td><input type="hidden" id="emb_0" class="hid"><input type="text"   name="cm2[]" id="m__cm2" value="" class="wel emb_wel" style="width:150px;" placeholder="cm2" />cm2</td>
						  <td><input type="text" maxlength="8"  name="valuePoint[]" id="m__valuePoint" value="" class="wel emb_wel" style="width:150px;" placeholder="Value-Points"  />Value-Points</td>					   
					   <td>&nbsp;</td>
					   </tr>
				  </table>	
				  <div><input type="button" name="add_more" id="add_more" value="Add More"></div>	
              </section> 
      	<!--==============================================================================point 7 work ================================--->
                <label>Embroidery Quantity-Discount</label>
                <?php
                    while($line = $embObj->getResultObject($discount)){ ?>                    	
                        <section>                    
                            <div style="width:55%; float:left;">                                                                                   
                                    <span style="font-size: 13px; margin-left: 25px;"><?= $line->pcs;?> pcs</span>
                            </div>                           
                            
                            <div style="width:7%; float:left;">   
                            <?php
                                if($line->sign == 0){
                                    $status = "minus";
                                } else if($line->sign == 1) {
                                    $status = "x";
                                } else {
                                    $status = "plus";
                                }
                            ?>
                            <span style="font-size: 13px; margin-left:11px;"><?= $status;?></span>
                            </div>                                                          
                            <div style="width:22%; float:left;"> <?php if($line->sign != 1) { ?>                                                      
                                <input type="text" name="charge_<?= $line->id;?>" id="charge_<?= $line->id;?>" value="<?= $line->charge; ?>" class="wel" style="width:75px;" />     <?php }else { echo '<span style="font-size: 13px; margin-left:75px;">0</span>';} ?>
                                <span style="font-size: 17px;"> % </span>
                            </div>
                             <div style="width:5%; float:left;">
                             	<a class='i_trashcan edit' href='javascript:void(0);'  onClick="if(confirm('Are you sure to delete this Record?')){window.location.href='pass.php?action=manageEmbroidery&type=delete&id=<?php echo $line->id;?> &page=$page'}else{}" >Delete</a>
                             </div>                                                                 
                        </section>
                <?  }                
                ?>   
            </fieldset>

     
            <div class="main-body-sub" style="text-align:center; margin-left:0px">
               <button type="submit" name="submit">Submit</button>
            </div>
            <div id="divTemp" style="display:none;"></div>
         </form>
      </fieldset>
   </section>
   <div id="divTemp" style="display:none;"></div>
   <script>
	 $(document).ready(function(){
		 //add row
		  $('#add_more').live("click", function(){           
            var $new_row=$get_lastIDproduct();            
            $('#embroidary-tbl').append($new_row);          
	    });	
	    //create new row    
			var $lastChar =0, $newRow;
			$get_lastIDproduct= function(){        
				  
			var $id = $('#embroidary-tbl tr:last-child td:first-child input[class="hid"]').attr("id");	
			$id_arr=$id.split("_"); 	          
			$lastChard = parseInt($id_arr[1]);			                           
			$lastChard = $lastChard + 1;
			$newRow = "<tr><td><input type='hidden' id='emb_"+$lastChard+"' class='hid'><input type='text'  id='cm2"+$lastChard+"' name='cm2[]'   class='wel emb_wel' style='width:150px;' placeholder='cm2'>cm2</td><td><input type='text' name='valuePoint[]' maxlength='8'  id='valuePoint"+$lastChard+"' class='wel emb_wel' style='width:150px;' placeholder='Value-Points'>Value-Points</td><td><input type='button' value='Delete' class='del_embRow' /></td></tr>";
			   
			return $newRow;
		}
		//delete row		
		$(".del_embRow").live("click", function(){ 
			$(this).closest('tr').remove();
			$lastChar = $lastChar-2;
		});	
	    
	 });
	 
	 $('.emb_wel').keypress(function(event) {
    if(event.which == 8 || event.which == 0){
        return true;
    }
    if(event.which < 46 || event.which > 59) {
        return false;
        //event.preventDefault();
    } // prevent if not number/dot
    
    if(event.which == 46 && $(this).val().indexOf('.') != -1) {
        return false;
        //event.preventDefault();
    } // prevent if already dot
	});
   </script>
   
</body>
</html>

<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageCoupon.php", "");
/* ---Basic for Each Page Starts---- */

$couponObj = new Coupon();
$genObj = new GeneralFunctions();

$result = $couponObj->getResult(base64_decode($_GET['id']));
?>
<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions  ?>

</head>
<body>
   <section id="content-detail">
      <form>
         <fieldset>
            <label>View Coupon Details</label>
            <!-- left position -->
            <div><?= $_SESSION['SESS_MSG'] ?></div>
            <!-- START:COUPON CODE -->
            <section>
               <label for="CouponCode">Coupon Code </label>
               <div><?= $result->couponCode ?></div>
            </section>
            <!-- START:COUPON Name -->
            <section>
               <label for="Name">Coupon Name</label>
               <div>
                  <? echo $genObj->getLanguageViewTextBox('couponName', TBL_COUPON_DESCRIPTION, base64_decode($_GET['id']), "couponId"); ?>
               </div>

            </section>
            <!-- START: DISCOUNT VALUE -->
            <section>
               <label for="DiscountValue">Coupon Value </label>
               <div><?= $result->couponDiscountAmount ?> </div>
            </section>

            <!-- Calendar Scrip --->
            <script src="js/jquery_DateTime/jquery.min.js"></script>

            <script type="text/javascript" src="js/jquery_DateTime/jquery.dynDateTime.js"></script>

            <script type="text/javascript" src="js/jquery_DateTime/lang/calendar-en.js"></script>

            <link rel="stylesheet" type="text/css" media="all" href="js/jquery_DateTime/css/calendar-win2k-1.css"  />
            <script type="text/javascript">

               jQuery(document).ready(function() {

                  jQuery("#dateTimeCustom").dynDateTime({

                     showsTime: true,

                     ifFormat: "%Y-%m-%d ",

                     daFormat: "%l;%M %p, %e %m,  %Y",

                     align: "TL",

                     electric: true,

                     singleClick: true

                  });

				

                  jQuery("#datepicker").dynDateTime({

                     showsTime: true,

                     ifFormat: "%Y-%m-%d ",

                     daFormat: "%l;%M %p, %e %m,  %Y",

                     align: "TL",

                     electric: true,

                     singleClick: true

                  });

				

				

				

               });

            </script>

            <!-- START: Start Date -->
            <section>
               <label for="StartDate">Coupon Type</label>
               <div><?= $result->couponDiscountType ?></div>

            </section>
            <!-- START: Start Date -->
            <section>
               <label for="StartDate">Start Date</label>
               <div><?= $result->startDate ?></div>
            </section>

            <!-- START: End Date -->
            <section>
               <label for="EndDate">End Date</label>
               <div><?= $result->endDate ?></div>
            </section>

         </fieldset>
      </form>
   </section>
<? include_once('includes/footer.php');?>

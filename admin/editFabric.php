<?php
/* ---Basic for Each Page Starts---- */
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$genObj = new GeneralFunctions();
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageFabric.php", "");
/* ---Basic for Each Page Starts---- */

$fabricObj = new Fabric();

$generalFunctionObj = new GeneralFunctions();
$result = $fabricObj->getResult(base64_decode($_GET['id']));

if (isset($_POST['submit'])) {
        //echo "<pre>"; print_r($_POST); echo "</pre>";exit;
        require_once('validation_class.php');
        $obj = new validationclass();
        $rst = $fabricObj->selectQry(TBL_LANGUAGE, "status='1'  AND isDeleted='0' order by languageName asc", "", "");
        $num = $fabricObj->getTotalRow($rst);
        if ($num > 0) {
                $langIdArr = array();
                while ($line = $fabricObj->getResultObject($rst)) {
                    array_push($langIdArr, $line->id);
                }
				
                //Add Error===================
                foreach ($langIdArr as $key => $value) {
                    $obj->fnAdd('fabricName_' . $value, $_POST['fabricName_' . $value], 'req', 'Please enter fabric name.');
                }
								
                //Validate===============
                $arr_error = $obj->fnValidate();
                $str_validate = (count($arr_error)) ? 0 : 1;


                //Get Error================
                foreach ($langIdArr as $key => $value) {
                    $arr_error['fabricName_' . $value] = $obj->fnGetErr($arr_error['fabricName_' . $value]);        
                }
				
                if ($str_validate){
                    $fabricObj->editFabric($_POST);
                }
        }
}
?>

<?= headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions         ?>
<script language="javascript" src="js/requiredValidation.js"></script>

<script type="text/javascript">
	function hrefBack1(){
		window.location='manageFabric.php';
    }   	
	
</script>

</head>
<body>
   <? include('includes/header.php'); ?>
   <section id="content">
      <h1>Edit </h1>
      <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
         <input type="hidden" name="id" value="<?= base64_decode($_GET['id']) ?>" />
         <input type="hidden" name="page" value="<?= $_GET['page'] ?>" />
         <fieldset>
            <label><?= $result->type ?></label>
            <?= $_SESSION['SESS_MSG'] ?>
            <section>
               <label for="FabricTitle">Fabric Name</label>
               <div>
                  <?= $genObj->getLanguageEditTextBox('fabricName','m__fabricName',TBL_FABRICDESC, base64_decode($_GET['id']),"fabricId",$arr_error); ?>                  		
               </div>               
            </section>          			
         </fieldset>
         <fieldset>
            <section>
               <label>&nbsp;</label>
               <div style=" width:78%;">
                  <input type="submit" name="submit" value="Submit" />
                  <input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>                                    
               </div>
            </section>
         </fieldset>
      </form>
   </section>
   <? unset($_SESSION['SESS_MSG']); ?>

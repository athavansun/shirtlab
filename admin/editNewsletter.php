<?php
ob_start();
session_start();
require_once('config/configure.php');
require_once('includes/function/autoload.php');
include_once("fckeditor/fckeditor.php");
$loginObj = new Login();
$loginObj->checkSession();
$pageName = getPageName();
$menuObj = new Menu();
$menuObj->checkPermission("manageNewsletter.php","edit_record");
$newsletterObj = new Newsletter();
$generalFunctionObj = new GeneralFunctions();
$id = base64_decode($_GET['id']);
$row = $newsletterObj->newsLetterToEdit($id);
require_once('validation_class.php');
$obj = new validationclass();
if(isset($_POST['submit'])) {
	$obj->fnAdd('languageName',$_POST['languageName'], 'req', 'Please Select Language Name.');
	$obj->fnAdd("mailsubject", $_POST["mailsubject"], "req", "Please enter  Mail Subject.");
	$obj->fnAdd('email',$_POST['email'], 'req', 'Please enter Email Address.');
	$obj->fnAdd("email", $_POST["email"], "email", "Please enter valid Email Address.");
	$obj->fnAdd("message", strip_tags($_POST["message"]), "req", "Please enter Mail Containt.");
	$arr_error = $obj->fnValidate();
	$str_validate = (count($arr_error)) ? 0 : 1;
	$arr_error[languageName]=$obj->fnGetErr($arr_error[languageName]);
	$arr_error[mailsubject]=$obj->fnGetErr($arr_error[mailsubject]);
	$arr_error[email]=$obj->fnGetErr($arr_error[email]);
	$arr_error[message]=$obj->fnGetErr($arr_error[message]);
 //echo "<pre>"; print_r($arr_error); exit;
	if(empty($arr_error[languageName]) && empty($arr_error[mailsubject]) && empty($arr_error[email]) && empty($arr_error[message]) && isset($_POST['submit'])){
		//$_POST = postwithoutspace($_POST);

		$newsletterObj->editNewNewsletter($_POST,$_FILES);
	}
}
$extensions = $newsletterObj->fetchValue(TBL_SYSTEMCONFIG,"systemVal","systemName='IMAGE_EXTENSION'");

?>
<?=headcontent()// DOCTYPE,ContentType,Title,style.css,jquery.min.js,jquery-ui.min.js,jquery_ajax.js,ajax.js, Top Pageoptions ?>
<script language="javascript" src="js/requiredValidation.js"></script>
<script src="js/file/jquery.filestyle.js"></script>
<script type="text/javascript">
	function hrefBack1(){
		window.location='manageNewsletter.php';
	}
</script>

</head>
<body>
<? include('includes/header.php'); ?>
  <section id="content">
  		<h1>Newsletter</h1>
        <form name="frmUser" id="frmUser" method="post" onSubmit="javascript: return validateFrm(this);" enctype="multipart/form-data">
			<fieldset>
				
				<label>Edit Newsletter</label>
				<?=$_SESSION['SESS_MSG']?>
				
				<!--- Start : Language------------------>
				<section>
					<label for="Language">Language</label>
					  <div>
					  <select name="languageName" id="m__language_name" >
					 	 <?=$generalFunctionObj->getLanguageInDropDown($_POST['languageName']?$_POST['languageName']:$row[langId]);?>
					  </select>
					  <?=$arr_error[language]?>
					  </div>	
				</section>
				
				<!--- Start : Subject------------------>
				<section>
					<label for="Subject">Subject </label>
					  <div>
					  <? $subject = $_POST['mailsubject']?$_POST['mailsubject']:$row['newsletterSubject']; ?>
					  <textarea name="mailsubject"   id="m__Mail_Subject"  ><?=nl2br(stripslashes($subject))?></textarea>
					  <?=$arr_error[mailsubject]?>
					  </div>	
				</section>
				
				<!--- Start : From Mail ID------------------>
				<section>
					<label for="FromMailID">From MailId</label>
					  <div>
					  <? $email = $_POST['email']?$_POST['email']:$row['senderEmailId']; ?>
					  <input type="text" name="email" id="m__email" size="3" value="<?=stripslashes($email)?>" />
					  <?=$arr_error[email]?>
					  </div>	
				</section>
				
				<!--- Start : Attachment------------------>
				<section>
					<label for="Attachment">Attachment</label>
					  <div>	
					  <div>				  
					  <?
					  if($row['attachedFile'] && $row['attachmentPath']){ 
				 		$attachment = "<a href='".__NEWSLETTERPATH__.$row[attachmentPath]."' style='cursor:pointer;' target='new'>"
										.$row['attachedFile'].
										"</a>";
					  echo $attachment;
					  } 
					 ?>	
					 </div>			
					 <input type="file" class="browse-button" name="upload" id='upload'  value="" />
					 
                                         <span style="margin-left:125px;">Allowed Extensions : (<?= $extensions; ?>)</span>
					 </div>	
				</section>
				
				<!--- Start : Template Name------------------>
				<section>
					<label for="NewsletterContent">    Newsletter Content </label>
					  <div>
                     <?php
						$message = $_POST['message']?$_POST['message']:$row[newsletterContaint];
						$oFCKeditor = new FCKeditor('message') ;
						$oFCKeditor->BasePath = 'fckeditor/' ;
						$oFCKeditor->Height	= 400;
						$oFCKeditor->Width	= 740;
						$oFCKeditor->Value = stripslashes($message);
						$oFCKeditor->Create();
					?>
					  <?=$arr_error[brandName]?>
					  </div>	
				</section>
				
				
				
				 
			 </fieldset> 
			          
             <fieldset> 
				<section>  
				<label>&nbsp;</label>
				<div style=" width:78%;">
					
					<input type="submit" name="submit"   value="Submit" />
					<input type="button" name="back" id="back" value="Back"   onclick="javascript:;hrefBack1()"/>
				</div>
             </section>             
        </fieldset>
		<input type="hidden" name="newsletterId" value="<?=$id?>" />
		<input type="hidden" name="page" value="<?=$_GET['page']?>" />
        </form>
	</section>
<? unset($_SESSION['SESS_MSG']); ?>
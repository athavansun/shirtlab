<?php include_once('includes/header.php'); ?>
<?php
	if(!empty($_SESSION['USER_HASH'])){
		$previousPage=$_SERVER['HTTP_REFERER'];    
		header("location:index.php");
		echo"<script>document.location.href='index.php'</script>";exit;
	}


	$signupObj=new Signup();
	$genObj = new GeneralFunctions();
	$row = $genObj->getStaticPageInformation();

	//Submit================================
	if(isset($_POST['submit'])){            
		require_once('validation_class.php');
		$obj = new validationclass();     
		$obj->fnAdd("email", $_POST["email"], "req","This field is required.");
		$obj->fnAdd('email',$_POST['email'], 'email', "Invalid email id.");	
		
		$arr_error = $obj->fnValidate();
		$str_validate = (count($arr_error)) ? 0 : 1;
			
		$arr_error[email] = $obj->fnGetErr($arr_error[email]);
			
			if(!$signupObj->isUserEmailExistPhp($_POST) && $str_validate){ 			
					$arr_error[email] = '<span class="alert-red alert-icon">Email does not exists.</span>';
					$str_validate=0;
			}        
			
			// $str_validate=1 => validation successfull => proceed further========
		if($str_validate){                
			$_POST = postwithoutspace($_POST);                
			$signupObj->resendActivationCode($_POST);
		}
	}
?>
<div id="content" >
	<div class="subpage-border-top">
		<div class="subpage-border-bottom">
			<div class="subpage-border-mid">
				<div class="loginpage-bottom">
					<h2><? echo utf8_decode(stripslashes($row->pageTitle));?></h2>
						<div class="loginpage-mid">
							<span><?php echo $_SESSION['ERROR_MSG']; unset($_SESSION['ERROR_MSG']); ?></span>
							<span>
								<?php echo stripslashes($row->pageContaint);?>
							</span>
							<form name="resendCodeForm" id="resendCodeForm" method="POST" action="">
							<ul>
							<li>
							<label>Email</label>
							<div class="right_input">
							<input type="text" name="email" />
							</div>
							<?=$arr_error[email]?>
							</li>							
							<li>
								<div class="remember">&nbsp;</div>
							<div class="login-button"><input name="submit" type="submit" value="Re-send" /></div>
							</li>
						  </ul>
						  </form>
						</div>
				</div>
				
			</div>
		</div>
	</div>
  <!--Content-->
</div>
<?php include_once('includes/footer.php'); ?>

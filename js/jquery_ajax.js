function validate()
{
    var sKey = $("#keyword").val();
    //var regx = /^\s*[a-zA-Z0-9,\s]+\s*$/;
    var regx = /^\s*[a-zA-Z0-9\-,\s]+\s*$/;
    
    if(regx.test(sKey))
    {        
        return true;
    }
    else
    {        
        return false;
    }
}
function changeCurrency(id) {      
    if(id !== '') {        
        var page = $("#pageName").val()+"quantitydeliverytime.php";
        var src = 'pass.php';
        $.ajax({
           url:src,
           data: 'action=changeCurrency&type=change&id='+id,
           cache:false,
           type:'GET',
           success: function(data, textStatus, XMLHttpRequest) {
               window.location = page;
               //$("#quantityPageInfo").after(data);
               //console.log(id);
           },
           error:function(XMLHttpRequest, textStatus, errorThrown) {
               alert(textStatus);
               alert(errorThrown);
           }
        });
    }
}


//function validateQuantity() {    
//    var quantityArr = new Array();
//    var flag = true;
//    var msg = '';
//    $(".quantity").each(function(index, value) {        
//        quantityArr[index] = $(this).val();
//    });
//    $.each(quantityArr, function(index, val) {
//       if(val !== '' && !isNaN(val)) {           
//           flag = true;           
//       }else if(isNaN(val)){
//           msg = "Please Enter Quantity in Number";
//           return false;
//       }else {
//           msg = "Please Enter Quantity";
//           flag = false;
//       }
//       
//    });
//    
//    if(flag === false) {        
//        $("#quantityError").css("color", "red");
//        $("#quantityError").css("font-size", "12px");
//        $("#quantityError").css("margin-left", "160px");
//        $("#quantityError").css("margin-bottom", "20px");        
//        $("#quantityError").css("display", "block");
//        $("#quantityError").text(msg);
//    }
//    
//    var i = $("#totalView").val();
//    var arr = i.split(",");
//    $.each(arr, function(index, value) {
//        var name = "#"+value+"Design option:selected";       
//        
//        var i = $(name).val();        
//        if(i == 0) {
//            $("#typeError_"+value).text("Please select embroidery type.");
//            $("#typeError_"+value).css("color", "red");
//            $("#typeError_"+value).css("font-size", "12px");
//            $("#typeError_"+value).css("margin-left", "160px");
//            $("#typeError_"+value).css("margin-bottom", "20px");        
//            $("#typeError_"+value).css("display", "block");
//            
//            flag = false;
//        } else {            
//            $("#typeError_"+value).css("display", "none");
//        }
//        
//        var colorMsg = "";
//        var quantity = $("#"+value+"ColorQty").val();
//        if(quantity !== '' && !isNaN($quantity)) {            
//        
//        }else if(isNaN(quantity)){
//           colorMsg = "Please Enter Color Quantity in Number";
//           return false;
//        }else {
//           colorMsg = "Please Enter Color Quantity";
//           flag = false;
//        }
//        
//        if(colorMsg != "") {        
//            $("#numberError_"+value).css("color", "red");
//            $("#numberError_"+value).css("font-size", "12px");
//            $("#numberError_"+value).css("margin-left", "160px");
//            $("#numberError_"+value).css("margin-bottom", "20px");        
//            $("#numberError_"+value).css("display", "block");
//            $("#numberError_"+value).text(msg);
//            flag = false;
//        } else {
//            $("#numberError_"+value).css("display", "none");
//        }
//        
//    });
//    
//    
//    
//    return flag;
//}


function validateQuantity() {   
    var quantityArr = new Array();
    var flag = true;
    var msg = '';
    
    var fabricQuantityError = $("#fabricQuantityError").val();
    var quantityNumberError = $("#quantityNumberError").val();
    var embroideryError     = $("#embroideryError").val();
    var quantityColorError  = $("#quantityColorError").val();
    var colorNumberError    = $("#colorNumberError").val();
       
    
    $(".quantity").each(function(index, value) {
        quantityArr[index] = $(this).val();
    });
        
    $.each(quantityArr, function(index, val) {       
       if(val != '' && !isNaN(val)) {           
           flag = true;           
           return false;
       }else if(isNaN(val)){
           //msg = "Please Enter Quantity in Number";           
           msg = quantityNumberError;
           flag = false;
       }else{           
           //msg = "Please Enter Quantity";
           msg = fabricQuantityError;
           flag = false;           
       }
    });    
    if(flag === false) {
        $("#quantityError").css("color", "red");
        $("#quantityError").css("font-size", "12px");
        $("#quantityError").css("margin-left", "160px");
        $("#quantityError").css("margin-bottom", "20px");        
        $("#quantityError").css("display", "block");
        $("#quantityError").text(msg);
    } else {
        $("#quantityError").css("display", "none");
    }
        
    var i = $("#totalView").val();    
    if(i !== '') {
        var arr = i.split(",");
        $.each(arr, function(index, value) {
            if($("#"+value+"PhotoPrint").attr("checked") != "checked") {
                var name = "#"+value+"Design option:selected";
                var i = $(name).val();
                if(i == 0) {
                    //$("#typeError_"+value).text("Please select embroidery type.");
                    $("#typeError_"+value).text(embroideryError);
                    $("#typeError_"+value).css("color", "red");
                    $("#typeError_"+value).css("font-size", "12px");
                    $("#typeError_"+value).css("margin-left", "160px");
                    $("#typeError_"+value).css("margin-bottom", "20px");        
                    $("#typeError_"+value).css("display", "block");

                    flag = false;
                } else {
                    $("#typeError_"+value).css("display", "none");
                }

                var colorQty = $("#"+value+"ColorQty").val();
                if(colorQty != '' && !isNaN(colorQty)) {
//                    flag = validatePantone(value, colorQty);
                    var pantone = validatePantone(value, colorQty);
                    if(pantone == false) {
                        var pantoneError = value+"PantoneError";
                        $("#"+pantoneError).text("select pantone color.");
                        flag = false;
                    } else {
                        $("#"+pantoneError).text("");
                    }
                } else if(isNaN(colorQty)){
                    //colorMsg = "Please enter color quantity in number";
                    colorMsg = colorNumberError;
                    flag = false;
                } else {
                    //colorMsg = "Please enter color quantity";
                    colorMsg = quantityColorError;
                    flag = false;
                }
                if(colorQty != '' && !isNaN(colorQty)) {
                    $("#numberError_"+value).css("display", "none");   
                }else {
                    $("#numberError_"+value).text(colorMsg);
                    $("#numberError_"+value).css("color", "red");
                    $("#numberError_"+value).css("font-size", "12px");            
                    $("#numberError_"+value).css("margin-bottom", "20px");        
                    $("#numberError_"+value).css("display", "block");
                }    
            }
        });   
    }
    return flag;
}

function validatePantone(viewName, number) {
   var flag = true;
   for(var i=1; i<=number; i++) {
       name = viewName+"Color"+i;
       var pantone = $("#"+name).val();
       if(pantone == 0 || pantone == "") {
           flag = false;           
       }        
   }
   return flag;
}


//function decoType(name, value) {    
//    var elementText = name.replace('Design', 'ColorQty');
//    var elementChk = name.replace('Design', 'PhotoPrint');
//    var viewName = name.replace('Design', '');
//    if(value != '0') {
//        $("#"+elementChk).attr("disabled", "disabled");
//        $("#"+elementChk).attr("checked", false);
//        $("#"+elementText).removeAttr("disabled");
//        this.enableEditPantone(viewName);
//    }else {        
//        $("#"+elementText).attr("disabled", "disabled");
//        $("#"+elementText).val('');
//        $("#"+elementChk).removeAttr("disabled");
//        //this.disableAllPantone(viewName);
//        this.desableEditPantone(viewName);
//    }    
//}

//function decoType(name, value) {    
//    var elementText = name.replace('Design', 'ColorQty');
//    var elementChk = name.replace('Design', 'PhotoPrint');
//    var viewName = name.replace('Design', '');
//    if(value != '0') {        
//        $("#"+elementChk).removeAttr("disabled");
//        $("#"+elementChk).attr("checked", false);
//        $("#"+elementText).removeAttr("disabled");
//        this.enableEditPantone(viewName);
//    }else {
//        $("#"+elementText).attr("disabled", "disabled");
//        $("#"+elementText).val('');
//        $("#"+elementChk).removeAttr("disabled");
//        //this.disableAllPantone(viewName);
//        this.desableEditPantone(viewName);
//    }    
//}

function decoType(name, value) {    
    var elementText = name.replace('Design', 'ColorQty');
    var elementChk = name.replace('Design', 'PhotoPrint');
    var viewName = name.replace('Design', '');
    if(value != '0') {        
        if(value == '104') {            
            $("#"+elementChk).attr("disabled", "disabled");
        } else {
            $("#"+elementChk).removeAttr("disabled");
        }
        $("#"+elementChk).attr("checked", false);
        $("#"+elementText).removeAttr("disabled");
        this.enableEditPantone(viewName);
    }else {
        $("#"+elementText).attr("disabled", "disabled");
        $("#"+elementText).val('');
        $("#"+elementChk).removeAttr("disabled");
        //this.disableAllPantone(viewName);
        this.desableEditPantone(viewName);
    }    
}

function enableEditPantone(viewName) {
    var select = viewName+"_edit_pallet";
    $("."+select).each(function(){
        $(this).children("p").remove();        
    });
}

function desableEditPantone(viewName) {    
    var select = viewName+"_edit_pallet";
    $("."+select).each(function(){
        $(this).append("<p style=\"position:absolute; width:107px;height:30px;top:0px; z-index:10000;right:0; \"></p>");
        $(this).find('.chzn-single').css('opacity','0.4');
        $("."+select).css("position","relative");
    });
}

function disableAllPantone(viewName) {    
    for(var i=1; i<=8; i++) {
        var select = viewName+"Color"+i;
        $("#"+select+"_chzn").each(function(){
            $(this).parent("li").append("<p style=\"position:absolute; width:107px;height:27px;top:0px; z-index:10000;right:0; \"></p>");
            $(this).find('.chzn-single').css('opacity','0.4');            
            $(this).parent("li").css("position","relative");     
        });
    }
}

function disableDecoType(name, value) {
    var isChecked = $("#"+name).attr("checked");
    //var isChecked = $("#"+name).is(":checked");    
    var elementText = name.replace('PhotoPrint', 'ColorQty');
    var elementSel = name.replace('PhotoPrint', 'Design');
    var color = name.replace('PhotoPrint', 'Color');
    
    if(isChecked === "checked") {
        $("#"+elementSel).attr("disabled", "disabled");
        $("#"+elementText).val('');
        $("#"+elementText).attr("disabled", "disabled");
        for(var i = 1; i <= 8; i++ ) {
            $("#"+color+i+" option:first").attr("selected", "selected");
            $("#"+color+i).attr("disabled", "disabled");
        }
        disableAllPallet('Front');
    }else {
        $("#"+elementSel).removeAttr("disabled");        
        $("#"+elementText).removeAttr("disabled");        
    }
}

//function disableDecoType(name, value) {
//    alert(name);
//    var isChecked = $("#"+name).is(":checked");
//    var elementText = name.replace('PhotoPrint', 'ColorQty');
//    var elementSel = name.replace('PhotoPrint', 'Design');
//    var color = name.replace('PhotoPrint', 'Color');
//    
//    if(isChecked === "checked") {
//        $("#"+elementSel).attr("disabled", "disabled");
//        $("#"+elementText).val('');
//        $("#"+elementText).attr("disabled", "disabled");
//        for(var i = 1; i <= 8; i++ ) {
//            $("#"+color+i+" option:first").attr("selected", "selected");
//            $("#"+color+i).attr("disabled", "disabled");
//        }
//    }else {        
//        $("#"+elementSel).removeAttr("disabled");        
//        $("#"+elementText).removeAttr("disabled");        
//    }
//}

//function changeCountry(id) {
//    if(id !== '') {
//        var src = 'pass.php';
//        $.ajax({
//           url:src,
//           data: 'action=deliveryTime&type=country&id='+id,
//           cache:false,
//           type:'GET',
//           success: function(data, textStatus, XMLHttpRequest) {
//               alert(data);
//               $("#quantityPageInfo").after(data);
////               $("#deliveryTime").remove();
////               $("#deliveryCountry").after(data);
//           },
//           error:function(XMLHttpRequest, textStatus, errorThrown) {
//               alert(textStatus);
//               alert(errorThrown);
//           }
//        });
//    }

function changeDeliveryTime(id) {	
   if(id !== '') {
       var page = $("#pageName").val()+"quantitydeliverytime.php";
        var src = 'pass.php';
        $.ajax({
           url:src,
           data: 'action=deliveryTime&type=change&id='+id,
           cache:false,
           type:'GET',
           success: function(data, textStatus, XMLHttpRequest) {
				window.location = page;
           },
           error:function(XMLHttpRequest, textStatus, errorThrown) {
               alert(textStatus);
               alert(errorThrown);
           }
        });
    }
}

function changeCountry(id) {	
    if(id !== '') {
        var page = $("#pageName").val()+"quantitydeliverytime.php";
        var src = 'pass.php';
        $.ajax({
           url:src,
           data: 'action=deliveryTime&type=country&id='+id,
           cache:false,
           type:'GET',
           success: function(data, textStatus, XMLHttpRequest) {
               //$("#quantityPageInfo").after(data);
               window.location = page;
           },
           error:function(XMLHttpRequest, textStatus, errorThrown) {
               alert(textStatus);
               alert(errorThrown);
           }
        });
    }




function viewAccessories(id)
{ 
	//alert("----"+id);
	var src = 'pass.php';	
	$.ajax({
		url: src,
		data: 'action=manageAccessories&type=view&id='+id,
		cache: false,  //don't let jquery prevent caching if cacheParam is being used dataType: 'json',
		type:'GET',
		success: function(data, textStatus, XMLHttpRequest){			
		//alert(data);			
			$("#showHeading").html("");
			$("#showHeading").css("display","block");
			$("#showHeading").append(data);
	/*		if(id != '') {
				$("#btnSubmit").css("display", "block");
			} else{
				$("#btnSubmit").css("display", "none");
			}		*/
		
		},// refresh the calendar    

		error:function(XMLHttpRequest, textStatus, errorThrown){alert(textStatus);alert(errorThrown);}
	});  
}


function changeColorPallet(fId) {    	
    var src = 'pass.php';
    $.ajax({
        url:src,
        data:'action=enterQuantity&type=changePallet&fId='+fId,
        cache:false,
        type:'GET',
        success: function(data, textStatus, XMLHttpRequest) {            
            //alert(data);
            $(".colorPallet").empty().append(data);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
            alert(errorThrown);
        }
    });
}


    
    
    
    
    
    
    
    
    
    
    
    //Testing........
    
    function testAjax(id) {
        $.ajax({
            type:"GET",
            src:"abc.php",
            data:"id="+id,
            success:function(data, textStatus, XMLHttpRequest) {
                alert(data);
            },
            error:function(XMLHttpRequest, textStatus, errorthrown){
                alert(textStatus);
            }
        })
    }
    
    
    
    
    
    
    
    
    
    
    
}

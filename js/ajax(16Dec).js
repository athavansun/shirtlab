function GetXmlHttpObject()
{
    var xmlHttp=null;
    try
    {
        // Firefox, Opera 8.0+, Safari
        xmlHttp=new XMLHttpRequest();
    }
    catch(e)
    {
        //Internet Explorer
        try
        {
            xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}

function getLanguageList(countryid){	
    xmlhttp=GetXmlHttpObject();
    document.getElementById("languageList").innerHTML= "<img src='images/loading.gif'/>";
    var query = "?action=getLanguageList&type=multiLanguage&countryid="+countryid;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {	
            document.getElementById("languageList").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","pass.php"+query, true);
    xmlhttp.send(null);
}


function reloadcaptcha(){
    xmlhttp=GetXmlHttpObject();	
    document.getElementById("captchadiv").innerHTML= "<img src='images/loading.gif'/>";
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {	
            document.getElementById("varification").value= "";
            document.getElementById("captchadiv").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","imagecode.php",true);
    xmlhttp.send(null);
}

//14Nov2013	 
//function showProductDetail(id) {        
//    xmlhttp=GetXmlHttpObject();
//    document.getElementById("jersy-tshirt-mtarr").innerHTML= "<img src='images/loading.gif'/>";	    
//	
//    var query = "?action=productDetail&type=view&id="+id;
//    xmlhttp.onreadystatechange=function()
//    {
//        if (xmlhttp.readyState==4)
//        {	
//            if(xmlhttp.responseText != "fail") {                            
//                document.getElementById("jersy-tshirt-mtarr").innerHTML= xmlhttp.responseText;                        
//                jQuery(".jersy-tshirt-mtarr").slideToggle();	
//                var settings = {
//                    zoomType: 'standard', //standard/reverse/innerzoom
//                    zoomWidth: 394,		//zoomed width default width
//                    zoomHeight: 375,		//zoomed div default width
//                    xOffset: 10,		//zoomed div default offset
//                    yOffset: 0,
//                    position: "right" ,//zoomed div default position,offset position is to the right of the image
//                    lens:true, //zooming lens over the image,by default is 1;
//                    lensReset : false,
//                    imageOpacity: 0.2,
//                    title : false,
//                    alwaysOn: false,
//                    showEffect: 'show',
//                    hideEffect: 'hide',
//                    fadeinSpeed: 'fast',
//                    fadeoutSpeed: 'slow',
//                    preloadImages :false,
//                    showPreload: true,
//                    preloadText : 'Loading zoom',
//                    preloadPosition : 'center'   //bycss
//                };
//                $(".jqzoom").jqzoom(settings);	
//            } else {
//                alert("fail");
//            }
//        }
//    }
//    xmlhttp.open("GET","pass.php"+query, true);
//    xmlhttp.send(null);
//}
	
var prevId = 0;
function showProductDetail(id) {
    xmlhttp=GetXmlHttpObject();
    document.getElementById("jersy-tshirt-mtarr").innerHTML= "<img src='images/loading.gif'/>";
    
    var res = $("#jersy-tshirt-mtarr").css("display");
    var query = "?action=productDetail&type=view&id="+id;
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4)
        {
            if(xmlhttp.responseText != "fail") {
                document.getElementById("jersy-tshirt-mtarr").innerHTML= xmlhttp.responseText;
                if(res == 'block'&& prevId == id){
                    jQuery(".jersy-tshirt-mtarr").slideUp(1000);
                }
                if(res == 'none'&& prevId == id){
                    jQuery(".jersy-tshirt-mtarr").slideDown(1000);
                }
                if(res == 'block'&& prevId != id)
                {}
                if(res == 'none'&& prevId != id){
                    jQuery(".jersy-tshirt-mtarr").slideDown(1000);
                }
                //jQuery(".jersy-tshirt-mtarr").slideToggle();                
                
                var settings = {
                    zoomType: 'standard', //standard/reverse/innerzoom
                    zoomWidth: 394,		//zoomed width default width
                    zoomHeight: 375,		//zoomed div default width
                    xOffset: 10,		//zoomed div default offset
                    yOffset: 0,
                    position: "right" ,//zoomed div default position,offset position is to the right of the image
                    lens:true, //zooming lens over the image,by default is 1;
                    lensReset : false,
                    imageOpacity: 0.2,
                    title : false,
                    alwaysOn: false,
                    showEffect: 'show',
                    hideEffect: 'hide',
                    fadeinSpeed: 'fast',
                    fadeoutSpeed: 'slow',
                    preloadImages :false,
                    showPreload: true,
                    preloadText : 'Loading zoom',
                    preloadPosition : 'center'   //bycss
                };
                $(".jqzoom").jqzoom(settings);
                prevId = id;
            } else {
                alert("fail");
            }
        }        
    }
    xmlhttp.open("GET","pass.php"+query, true);
    xmlhttp.send(null);    
}        
        
function addToCollection(pId) {
    xmlhttp = GetXmlHttpObject();
	
    var query = "?action=addToCollection&type=add&pId="+pId;
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4) {
            alert(xmlhttp.responseText);
//            if(xmlhttp.responseText == "success") {
//                alert("Product added to your collection");				
//            } else {
//                //alert(xmlhttp.responseText);
//                alert("Product already added to your collection.");
//            }
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}

function editUserInfo(fId, userId) {
    xmlhttp = GetXmlHttpObject();	
    var query = "?action=myAccount&type=editProfile&id="+userId;	
	
    xmlhttp.onreadystatechange = function() {		
        if(xmlhttp.readyState == 4) {
            $("#myAccPopup").css("display","block");
            $("#myAccPopup").html(xmlhttp.responseText);		
            $("#"+fId).css("display", "block");
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}

function addAddress(fId, heading, addressType) {
    xmlhttp = GetXmlHttpObject();
    var query = "?action=myAccount&fId="+fId+"&type=addAddress&heading="+heading+"&addressType="+addressType;
    xmlhttp.onreadystatechange = function() {		
        if(xmlhttp.readyState == 4) {            
            $("#myAccPopup").css("display","block");            
            $("#myAccPopup").html(xmlhttp.responseText);            
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}

/*
function editInvoiceAdd(fId) {
	xmlhttp = GetXmlHttpObject();
	
	var invoiceId = document.getElementById("invoiceHeading").value;
	var query = "?action=myAccount&type=editInvoiceAdd&id="+invoiceId;	
	xmlhttp.onreadystatechange = function() {
		if(xmlhttp.readyState == 4) {
			$("#myAccPopup").css("display","block");
			$("#myAccPopup").html(xmlhttp.responseText);	
			$("#"+fId).css("display", "block");
		}
	}	
	xmlhttp.open("GET", "pass.php"+query, true);
	xmlhttp.send(null);
}
*/

function editAddress(fId, heading, elementId, addressType) {
    xmlhttp = GetXmlHttpObject();
	
    var invoiceId = document.getElementById(elementId).value;
    if(invoiceId != "") {
        var query = "?action=myAccount&fId="+fId+"&type=editAddress&heading="+heading+"&id="+invoiceId+"&addressType="+addressType;
        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == 4) {
                $("#myAccPopup").css("display","block");
                $("#myAccPopup").html(xmlhttp.responseText);
                $("#"+fId).css("display", "block");
            }
        }
        xmlhttp.open("GET", "pass.php"+query, true);
        xmlhttp.send(null);
    }
}

function enterFabricQuantity(basketId, fId, pId, sId, fQId, mainProdId) {
    xmlhttp = GetXmlHttpObject();
    
	
    if(pId != "") {
        var query = "?action=enterQuantity&type=addQuantity&basketId="+basketId+"&fId="+fId+"&pId="+pId+"&sId="+sId+"&qId="+fQId+"&mainProdId="+mainProdId;               
        $("#myAccPopup").css("display","block"); 
        $("#myAccPopup").html('<div style="position:relative;left:40%;top:40%;"><img src=images/ajax-loader.gif ></div>');
        xmlhttp.onreadystatechange = function() {
                                              
            if(xmlhttp.readyState == 4) {
                
                $("#myAccPopup").html(xmlhttp.responseText);
                $("#"+fId).css("display", "block");
            }
        }
        xmlhttp.open("GET", "pass.php"+query, true);
        xmlhttp.send(null);
    //$(".select").chosen();
    }
}

function loadNewImage(imageName, original) {    
    xmlhttp = GetXmlHttpObject();
    var query = "?action=productDetail&type=loadImage&name="+imageName;
     
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4) {
            var html = '<a href="'+original+'" class="jqzoom" title="" ><img src="'+xmlhttp.responseText+'" title="" id=""></a>';            
            var html = '<a href="'+original+'" class="jqzoom" title="" ><img src="'+xmlhttp.responseText+'" title="" id=""></a>';
            
            //            var url = "convert.php?url="+xmlhttp.responseText+"&wid=330&hei=430";
            //            var html = '<a href="'+original+'" class="jqzoom" title="" ><img src="'+url+'" title="" id=""></a>';
            
            document.getElementById("loadhtm").innerHTML = html;
            loadzoom();                
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);    

}

function loadzoom()
{
    $(function() {
        var settings = {
            zoomType: 'standard', //standard/reverse/innerzoom
            zoomWidth: 394,		//zoomed width default width
            zoomHeight: 375,		//zoomed div default width
            xOffset: 10,		//zoomed div default offset
            yOffset: 0,
            position: "right" ,//zoomed div default position,offset position is to the right of the image
            lens:true, //zooming lens over the image,by default is 1;
            lensReset : false,
            imageOpacity: 0.2,
            title : false,
            alwaysOn: false,
            showEffect: 'show',
            hideEffect: 'hide',
            fadeinSpeed: 'fast',
            fadeoutSpeed: 'slow',
            preloadImages :false,
            showPreload: true,
            preloadText : 'Loading zoom',
            preloadPosition : 'center'   //bycss
        };
        $(".jqzoom").jqzoom(settings);
    });

}

//function enablePallet(viewName, num) {
//    if(!isNaN(num) && num !== '') {
//        for(var i=1; i<=8; i++) {
//            var select = viewName+"Color"+i;            
//            if(i <= num) {
//                //chzn-container chzn-container-single chzn-container-active
//                //chzn-container chzn-container-single chzn-disabled
//                alert(num);
//                $("."+chzn-container).css('border','1px solid red');
//                
////                var next = $("#"+select).next();
////                $(next).removeClass("chzn-disabled");
////                $(next).addClass("chzn-container-active");
//            } else{
//                $("."+chzn-container).css('border','1px solid blue');
//            }
//        }
//    } 
//}


//function enablePallet(viewName, num) {
//    if(!isNaN(num) && num !== "") {
//        for(var i=1; i<=8; i++) {
//            var select = viewName+"Color"+i;            
//            if(i <= num) {
//                $("#"+select+"_chzn").each(function(){
//                    $(this).parent("li").append("<p style=\"position:absolute;width:100%;height:27px;top:0px; z-index:10000; \"></p>")
//                    $(this).parent("li").css("position","relative");     
//                });
//            }
//        }
//    }
//}

function editPallet(editView, len, viewName) {
    //alert(editView+", "+len+", "+viewName);    
    this.disableAllPallet(viewName);
    this.enablePallet(editView, len);
}

function removeColor(id) {
    //    var obj = $("#"+id).parent().eq(0).remove();
    //    alert(id);
    //$(color).remove();
    
    $("#"+id).prev().prev().remove();
}
//23April 2013
//function enablePallet(viewName, num) {    
//    if(!isNaN(num) && num !== "") {
//        for(var i=1; i<=8; i++) {
//            var select = viewName+"Color"+i;            
//            if(i <= num) {
//                $("#"+select+"_chzn").each(function(){
//                    //$(this).parent("li p").remove();
//                    //$(this).find('.chzn-single > span').css('background','');
//                    $(this).find('.chzn-single').css('opacity','1');
//                    console.log($(this).parent("li").children("p").remove());
//                });
//            }
//        }
//    }
//}


function enablePallet(viewName, num) {
    //this.desableEditPantone(viewName);
    var chk = viewName+"PhotoPrint";
    num = $("#"+viewName+"ColorQty").val();
    var result = true;
    if(num == "" || num == 0) {
        num = 0;
        result = false;
        $("#"+chk).removeAttr("disabled");
    }
    if(!isNaN(num) && num !== 0) {
        $("#"+chk).attr("disabled", "disabled");
        this.disableAllPallet(viewName);        
        for(var i=1; i<=8; i++) {
            var select = viewName+"Color"+i;
            if(i <= num) {                
                $("#"+select+"_chzn").each(function(){                    
                    //$(this).parent("li p").remove();
                    //$(this).find('.chzn-single > span').css('background','');                    
                    $(this).find('.chzn-single').css('opacity','1');
                    console.log($(this).parent("li").children("p").remove());
                });
            }
        }
    }
}

function disableAllPallet(name){    
    var nameArr = name.split(',');    
    for(x=0; x<nameArr.length; x++) {
        var viewName = nameArr[x];        
        for(var i=1; i<=8; i++) {
            var select = viewName+"Color"+i;            
            $("#"+select+"_chzn").each(function(){
                var wid=$(this).width();
                //alert(wid);                
                $(this).parent("li").append("<p style=\"position:absolute; width:107px;height:27px;top:0px; z-index:10000;right:0; \"></p>");
                $(this).find('.chzn-single').css('opacity','0.4');
                // $(this).find('.chzn-single > span').css('background','#f5f5f5');
                $(this).parent("li").css("position","relative");     
            });
        }
    }
}

function isBasketEmpty() {    
    xmlhttp = GetXmlHttpObject();    
    id = $("#totalBasketId").val();
    var query = "?action=isBasketEmpty&type=check&id="+id;
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4){
            alert(xmlhttp.responseText);
            if(xmlhttp.responseText == "empty") {
                $("#isEmptyBasket").html("Basket is empty. Please enter quantity.");
                $("#isBasketStatus").val("0");
            }else {
                $("#isEmptyBasket").html("");
                $("#isBasketStatus").val("1");                
            }
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
}

function SubmitOrderform() {
    xmlhttp = GetXmlHttpObject();    
    var flag = true;
        
    var invoice = $("#selectInvoice option:selected").val();
    var delivery = $("#selectDelivery option:selected").val();
    var grafic = $("#selectGrafic option:selected").val();        
    var giftVoucher = $("#giftVoucher").val();

    $(".addressError").css("color", "#FF0000");
    $(".addressError").css("margin-top", "-15px"); 

    if(invoice == '') {
        $("#invoiceError").html("select invoice address");  
        flag = false;
    }else{
        $("#invoiceError").html("");
    }
    if(delivery == '') {
        $("#deliveryError").html("select delivery address"); 
        flag = false;
    }else {
        $("#deliveryError").html(""); 
    }
    if(grafic == '') {
        $("#graficError").html("select grafic designer address");
        flag = false;
    }else {
        $("#graficError").html("");
    }
    
    var quantity = $("#totalBasketId").val();
    var isValid = $("#isProductValid").val();
        
    if(quantity == 0) {
        $("#isEmptyBasket").html("Basket is Empty. Enter quantity.");
        flag = false;
    }
    
    if(isValid == 0) {
        $("#isEmptyBasket").html("First enter the quantity of each product.");
        flag = false;
    }   
    
    if(giftVoucher != ""){
        var query = "?action=giftVoucher&type=check&code="+giftVoucher;     
        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == 4){
                if(xmlhttp.responseText == "success") {	                     
                    if(flag == true){  
                        submitForm();
                    }
                } else {            
                    $("#giftVoucherError").html("Invalid coupan code");
                    flag = false;
                }
            }
        }
        xmlhttp.open("GET", "pass.php"+query, true);
        xmlhttp.send(null); 
    }else{
        if(flag == true){  
            submitForm();
        }
    }
}

function submitForm() {
    $("#delivery").submit();
}

function getEori(countryId){
    
    if(countryId!=""){     
    var query = "?action=eori&type=eoriadd&countryId="+countryId;
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4){
            
            if(xmlhttp.responseText == 0) {
                
                $("#eoridisp").hide(); 
                $('#eoriNo').removeClass('validate[required]');
                //$('#eoridispinfo').html('');
            }else {
                $("#eoridisp").show();
                $('#eoriNo').addClass('validate[required]');
               
            }
        }
    }
    xmlhttp.open("GET", "pass.php"+query, true);
    xmlhttp.send(null);
    }
    
}


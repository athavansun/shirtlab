<script language="javascript" type="text/javascript">

        $(document).ready(function(){            
            var form = $("#newsletter");    
            var newsletteremail = $("#newsletteremail");	
            var newsid = $("#newsId");
            var newsInfo = $("#newsInfo");   
            
            
            //Start : Newsletter Submit Button Click Function==================
            newsid.click(function(){  
               // validateEmail();
                if(validateEmail()){                   
                    return true;
                }else{                   
                    return false;
                }
            });
            
            //End : Newsletter Submit Button Click Function ==================
            
            
            //Start : Validate Email Function =================
            function validateEmail(){
                var email = $("#newsletteremail").val();               
                if(email=="" || email=="enter your email"){
                    newsletteremail.addClass("err_msg");
                    document.getElementById('newsInfo').innerHTML = "<table width='100%' class='Error-Msg' align='center' cellpadding='0' cellspacing='0'><tr><td align='center' height='15'><img src='images/error.png' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;<?=LANG_THIS_FIELD_IS_REQUIRED_MSG?></td></tr></table>";
                    newsInfo.addClass("err_msg");
                    return false;
                }else{
					 
                    //Check Space in email==============
                    for (i = 0; i < email.length; i++){
                         var c = email.charAt(i);
                         if (c == " " ) {
                             newsletteremail.addClass("err_msg");
                             document.getElementById('newsInfo').innerHTML = "<table width='100%' class='Error-Msg' align='center' cellpadding='0' cellspacing='0'><tr><td align='center' height='15'><img src='images/error.png' width='16' border='0' height='16'>&nbsp;&nbsp;<?=LANG_INVALID_EMAIL_ADDRESS_MSG?></td></tr></table>";
                             newsInfo.addClass("err_msg");
                             return false;
                         }
                        
                    }
                    
                    //Check Email Pattern==================================
                    var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    if(filter.test(email)){                        
                        $.post("pass.php?action=checkNewsletterEmail&class=IndexPage&dc=yes", {newsletteremail:newsletteremail.val()},function(data){                   
                               
                                if(data == 1) {
                                    newsletteremail.addClass("err_msg");                                   
                                    document.getElementById('newsInfo').innerHTML = "<img src='images/done.gif' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;<?=LANG_NEWSLETTER_SUCCESS_MSG?>";
                                    newsInfo.addClass("err_msg");
                                    return true;  
                                }if(data == 2){
                                    newsletteremail.addClass("err_msg");                                   
                                    document.getElementById('newsInfo').innerHTML = "<img src='images/error.png' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;<?=LANG_NEWSLETTER_FAILURE_MSG?>";
                                    newsInfo.addClass("err_msg");
                                    return true;  
                                }
                        });
                        
                    }else{
                        newsletteremail.addClass("err_msg");
                        $("#newsletteremail").val("");
                        document.getElementById('newsInfo').innerHTML = "<img src='images/error.png' width='16' border='0' height='16'>&nbsp;&nbsp;&nbsp;<?=LANG_INVALID_EMAIL_ADDRESS_MSG?>";
                        newsInfo.addClass("err_msg");
                        return false;
                        
                    }
                    
                }
                
            }
            
            //End : Validate Email Function=====================
            
        });

</script>

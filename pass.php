<?php
ob_start();
session_start();
$action = $_GET['action'];
$mode = $_GET['mode'];
$type = $_GET['type'];
$class = $_GET['class'];
/* * ************************** autoload the classes *************** */
require_once('includes/function/autoload.php');
//echo "<pre>"; print_r($_REQUEST); echo "</pre>";exit;
#-------multiLanguage------
if ($type == 'multiLanguage') {
    if ($action == 'getLanguageList') {
        $obj = new GeneralFunctions();
        $obj->getMultiLanguageList($_GET['countryid']);
    }
}


#-----user----------
if ($type == 'user') {
    $obj = new Signup();
    if ($action == 'checkEmail')
        $obj->checkUserEmail($_GET);
}

#-----Currency----------
if ($action == 'changeCurrency') {
    $obj = new QuantityDelivery();
    if ($type == 'change') {
        $obj->changeCurrency($_GET);
    }
}

#-----DeliveryTime----------
if ($action == 'deliveryTime') {
    $obj = new QuantityDelivery();
    if ($type == 'change') {
        $obj->changeDeliveryTime($_GET);
    }
    if ($type == 'country') {
        $obj->changeCountry($_GET);
    }
}

#-----DeliveryTime----------
if ($action == 'isBasketEmpty') {    
    $obj = new QuantityDelivery();
    if ($type == 'check') {
       $obj->isBasketEmpty($_GET);
    }
}


#-----Gift Voucher----------
if ($action == 'giftVoucher') {
    $obj = new QuantityDelivery();
    if ($type == 'check') {
        $obj->isValidGiftVoucher($_GET);
    }
}

#-----news letter-------
if ($action != '' && $class != '') {    
    $class1 = ucfirst($class);
    $obj = new $class1;
    if (isset($_GET['dc']) && $_GET['dc'] == 'yes') {
        $method = $action;
    } else {
        $method = $action . ucfirst($class);
    }
    $_GET = unsets($_GET, array('class', 'action', 'dc'));
    $post = (!empty($_POST)) ? $_POST : $_GET;
    $obj->$method(unsets($post, 'submit'), $_FILES);
}


//===========Product Details===============
if ($action == "productDetail") {
    $prodObj = new Product();

    if ($type == "view") {
        $prodObj->productDetail($_GET['id']);
    }

    if ($type == "loadImage") {
        $prodObj->changeImage($_GET);
    }
}

//===========Add to collection===============
if ($action == "addToCollection") {
    $prodObj = new Product();

    if ($type == "add") {
        $prodObj->adToCollection($_GET['pId']);
    }
}

//===========My account===============
if ($action == "myAccount") {
    $userObj = new User();

    if ($type == "editProfile") {
        $userObj->editUserInfo($_GET['id']);
    }

    if ($type == "addAddress") {        
        $userObj->addAddress($_GET);
    }

    if ($type == "editAddress") {
        $userObj->editAddress($_GET);
    }
}

//===========Enter quantity===============
if ($action == "enterQuantity") {
    $delivery = new QuantityDelivery();

    if ($type == "addQuantity") {
        $delivery->addQuantity($_GET);
    }
    if ($type == "changePallet") {
        $delivery->getColorPalle($_GET['fId'], 'change');
    }
}

//=============Bank Remittance==============
if ($action == "bank") {
    $deliveryObj = new QuantityDelivery();
    //echo '<pre>';print_r($_GET);exit;
    $deliveryObj->saveFinalOrder($_GET['oId'], "bank");
}
if ($action == "paypal") {
    $deliveryObj = new QuantityDelivery();
    $deliveryObj->saveFinalOrder($_GET['oId'], "paypal");
}

//=============Save Status==============
if ($action == "saveStatus") {
    if ($type == "language") {
        echo $_SESSION['saveStatus'];
    }
}

if ($action == "eori") {
    $userObj = new User();

    if ($type == "eoriadd") {
        echo $userObj->isEori($_GET['countryId']);
    }

}

if ($action == "archive") {
    $userObj = new User();

    if ($type == "deleteRecord") {
        echo $userObj->deleteRecord($_GET['id']);
    }

}

?>

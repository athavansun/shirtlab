<?php
include_once('includes/header.php'); 

$proObj = new Product();
$qString = $_SERVER['QUERY_STRING'];
$qStringArr = explode('&', $qString);
$param = array();

foreach ($qStringArr as $val) {
    $keyVal = explode('=', $val);
    $param[$keyVal[0]] = $keyVal[1];
}

?>
<style type="text/css">
    .jqZoomWindow{margin-top:-4px; margin-left:30px; border:1px solid #c7c7c7; behavior: url(PIE.htc); border-radius:5px; background-color:#eeeeee;}
    .product-style-large a{ margin:0 auto; text-align:center;}
    .product-style-large title{ display:none; text-indent:-999999px;}
    .preload{ display:none;}
</style>
<script src="js/jqzoom.pack.1.0.1.js" type="text/javascript"></script>
<script type="text/javascript">

    /*
        jQuery(".basic-image").live('click',function(){
                jQuery(".jersy-tshirt-mtarr").slideToggle();		
                $(".jqzoom").jqzoom();		
        });
     */

</script>


<div id="content">
    <?php
    if (isset($_GET['b']) && $_GET['b'] == "BL") {
        echo '<div class="message_box3"><span>'. LANG_FIRST_ADD_PRODUCT_INTO_YOUR_COLLECTION.'</span></div>';
    }
    ?>

    <div class="myaccount">	
        <?php        
        $proObj->getCategoryMenu($param);        
        ?>
    </div>

    <div class="product-style" id="loader">
        <ul>
            <?php            
            echo $result = $proObj->getProduct();
            ?>
        </ul>	
    </div>

    <div class="jersy-tshirt-mtarr" id="jersy-tshirt-mtarr">       
    </div>
    <div id="largeImgPanel" onclick="hideMe(this);">
            <img id="largeImg" style="height: 100%; margin: 0; padding: 0;" />
        </div>
</div>
<?php include_once('includes/footer.php'); ?>
<script type="text/javascript">
            function showImage(imgName) {                
                document.getElementById('largeImg').src = imgName;
                showLargeImagePanel();
                unselectAll();
            }
            function showLargeImagePanel() {
                document.getElementById('largeImgPanel').style.visibility = 'visible';
            }
            function unselectAll() {
                if(document.selection) document.selection.empty();
                if(window.getSelection) window.getSelection().removeAllRanges();
            }
            function hideMe(obj) {
                obj.style.visibility = 'hidden';
            }
        </script>
 
        <style type="text/css">
            #largeImgPanel {
                text-align: center;
                visibility: hidden;
                position: relative;
                z-index: 100;
                top: -200px; left: 20px; 
/*                width: 424px; */
                height: 300px;
                background-color: rgba(100,100,100, 0.5);
                border: 2px solid;
            }
        </style>

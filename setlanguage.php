<?php
@ob_start();
    session_start();
include("config/configure.php");
$generalDataObj = new GeneralFunctions();
$sitename = $generalDataObj->fetchValue(TBL_SYSTEMCONFIG, "systemVal","systemName='SITE_NAME'");
$defCurrId="1";
//$_SESSION[DEFAULTCURRENCYID]=$defCurrId;

if(isset($_COOKIE[$sitename."_language"]) && isset($_COOKIE[$sitename."_countryId"])) {              
    $_SESSION['DEFAULTLANGUAGECODE'] = $_COOKIE[$sitename."_language"];
    $_SESSION['DEFAULTLANGUAGEID'] = $_COOKIE[$sitename."_languageId"];
    $_SESSION['DEFAULTCOUNTRYID'] = $_COOKIE[$sitename."_countryId"];    
    
    $currencyId = $generalDataObj->fetchValue(TBL_COUNRTYSETTING, "currencyId","countryId=".$_COOKIE[$sitename."_countryId"]);
    
    if($currencyId){
        $_SESSION['DEFAULTCURRENCYID'] = $currencyId;
    }else{
       $_SESSION['DEFAULTCURRENCYID'] = 1; 
    }
    
}
elseif($_SESSION['DEFAULTLANGUAGEID'] == '' || $_SESSION['DEFAULTCOUNTRYID'] == '') {
    $defLanguageId = $generalDataObj->getSystemConfigValue('DEFLANGUAGE');//"1";
    $defCountryId = $generalDataObj->getSystemConfigValue('DEFCOUNTRY');
    $default1 = $generalDataObj->getLanuageCode($defLanguageId);//"eng"; // default will be english...
    $_SESSION['DEFAULTLANGUAGECODE'] = $default1;    
    $_SESSION['DEFAULTLANGUAGEID'] = $defLanguageId;
    $_SESSION['DEFAULTCOUNTRYID'] = $defCountryId;
    
    $currencyId = $generalDataObj->fetchValue(TBL_COUNRTYSETTING, "currencyId","countryId=".$defCountryId);
    if($currencyId){
        $_SESSION['DEFAULTCURRENCYID'] = $currencyId;
    }else{
       $_SESSION['DEFAULTCURRENCYID'] = 1; 
    }
    
    
    @setcookie(strtolower($sitename)."_language", $default1, time()+3600, '/'); 
    @setcookie(strtolower($sitename)."_languageId", $defLanguageId, time()+3600, '/');
    @setcookie(strtolower($sitename)."_countryId", $defCountryId, time()+3600, '/');
}

if(!isset($_SESSION['ORDERCOUNTRYID'])) {
    $_SESSION['ORDERCOUNTRYID'] = $_SESSION['DEFAULTCOUNTRYID'];
}

require_once ($docRoot."languages/".$_SESSION['DEFAULTLANGUAGECODE'].".inc.php");
?> 

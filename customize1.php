<?php
include_once('includes/header.php');
$fVarUrl = $_GET['ref'];
$fVar = $fVarUrl == '' ? 'product' : 'deco';
$collection = $_GET['collection'] == '' ? '' : $_GET['collection'];
?>
<style type="text/css">
    .extHeight {min-height: 870px;}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript">
    $(document).ready(function() {          
        var displayMessage = true;          
        $(window).bind('beforeunload', function(){
            //the custom message for page unload doesn't work on Firefox
            if(displayMessage){
                var message =  'This page is asking you to confirm that you want to leave - data you have entered may not be saved.';
                return message;
            } 
        });          
        $('*').hover(
        function(){displayMessage = false;},
        function(){displayMessage = true;} 
    );
    });     
</script>
<script type="text/javascript">
    //var attributes = {};
    function embedPlayer() {		
        var flashvars = {languageCode:"<?= $_SESSION['DEFAULTLANGUAGECODE'] ?>",
            selectedPage:"<?= $fVar ?>",
            collectionName:"<?= $collection?>",
            wMode:"transparent"};
        var params = {menu: "false"};	
        //swfobject.embedSWF("index.swf", "flashContent", "950", "670", "9.0",null, flashvars, params, attributes);
        swfobject.embedSWF("index1.swf", "flashContent", "959", "877", "9.0", null, flashvars , params, {name:"flashContent"});
    }
</script>
<!--Content-->
<div id="content" class="home extHeight">

    <style type="text/css" media="screen">
        object:focus { outline:none; }
        #flashContent { display:block; }		
        .home{padding:27px 0 8px 0 !important ;}
    </style>
    <script type="text/javascript">
        embedPlayer();
    </script>
    <div id="flashContent">
        <p>To view this page ensure that Adobe Flash Player version 10.0.0 or greater is installed. </p>
        <script type="text/javascript"> 
            var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
            document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
                + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
        </script>
    </div>
    <!--Content ends-->
</div>

<?php
include_once('includes/footer.php');
?>

<?php include_once('includes/header.php'); ?>
<?php
$genObj = new GeneralFunctions();
$row = $genObj->getStaticPageInformation();
if (isset($_POST['send'])) {
    require_once('validation_class.php');
    $obj = new validationclass();
    $obj->fnAdd('email', $_POST['email'], 'req', 'Please enter Email Address.');
    $obj->fnAdd('email', $_POST['email'], 'email', 'Please enter Email Address.');
    $obj->fnAdd('cemail', $_POST['cemail'], 'req', 'Please Re-enter email.');
    $obj->fnAdd('name', $_POST['name'], 'req', 'Please enter your name.');
    $obj->fnAdd('subject', $_POST['subject'], 'req', 'Please enter subject.');
    $obj->fnAdd('comment', $_POST['comment'], 'req', 'Please enter text/message.');

    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;

    $arr_error['email'] = $obj->fnGetErr($arr_error['email']);
    $arr_error['cemail'] = $obj->fnGetErr($arr_error['cemail']);
    $arr_error['name'] = $obj->fnGetErr($arr_error['name']);
    $arr_error['subject'] = $obj->fnGetErr($arr_error['subject']);
    $arr_error['comment'] = $obj->fnGetErr($arr_error['comment']);
    if ($_POST['email'] != $_POST['cemail']) {
        $arr_error['cemail'] = '<span class="alert-red alert-icon">Email mismatch.</span>';
        $str_validate = 0;
    }


    if ($str_validate) {
        $conct = new Contactus();
        $conct->insertMessage($_POST);
    }
}
?>
<div id="content" >
    <div class="subpage-border-top">
        <div class="subpage-border-bottom">
            <div class="subpage-border-mid">

                <div class="loginpage-bottom">
                    <h2><?php echo stripslashes($row->pageTitle); ?></h2>
                    <div class="loginpage-mid">
<?php echo $_SESSION['ERROR_MSG'];
unset($_SESSION['ERROR_MSG']); ?>
                        <div class="contact-us-main">
                            <span><?php echo stripslashes($row->pageContaint); ?></span>
                            <form name="contactFrm" id="contactFrm" method="post" action="">
                                <ul class="contact-us">
                                    <li>
                                        <label><?= LANG_EMAIL?></label>
                                        <div class="right_input">
                                            <input type="text" name="email" value="<?= $_POST['email']; ?>" />
                                        </div>
<?= $arr_error['email']; ?>
                                    </li>
                                    <li class="right-margin">
                                        <label><?= LANG_CONFIRM_EMAIL?></label>
                                        <div class="right_input">
                                            <input type="text" name="cemail" value="" />
                                        </div>
<?= $arr_error['cemail']; ?>
                                    </li>
                                    <li>
                                        <label><?= LANG_YOUR_NAME?></label>
                                        <div class="right_input">
                                            <input type="text" name="name" value="<?= $_POST['name']; ?>"/>
                                        </div>
<?= $arr_error['name']; ?>
                                    </li>
                                    <li class="right-margin">
                                        <label><?= LANG_YOUR_SUBJECT?></label>
                                        <div class="right_input">
                                            <input name="subject" type="text" value="<?= $_POST['subject']; ?>"/>
                                        </div>
<?= $arr_error['subject']; ?>
                                    </li>
                                    <!--<li>
                                        <label>Comment</label>
                                        <div class="right_input">
                                            <input type="text" value="" />
                                        </div>
                                    </li>
                                    <li class="right-margin">
                                        <label>Country</label>
                                        <div class="right_select">
                                            <select>
                                                <option>-Select one-</option>
                                                <option>1</option>
                                                <option>2</option>
                                            </select>
                                        </div>
                                    </li>-->
                                    <li>
                                        <label><?= LANG_TEXT?></label>
                                        <div class="myaccount-input-text" style="margin:0px 0 17px;">
                                            <textarea name="comment" rows="3" cols="8"><?= $_POST['comment']; ?></textarea>
                                        </div>
<?= $arr_error['comment']; ?>
                                    </li>

                                    <div class="login-button"><input name="send" type="submit" value="<?= LANG_SEND?>" /></div>

                                </ul>
                            </form>
                        </div> 

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Content-->
</div>
<?php include_once('includes/footer.php'); ?>

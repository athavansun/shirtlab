<?php include_once('includes/header.php'); ?>
<?php
$genObj = new GeneralFunctions();
$signupObj = new Signup();
require_once('validation_class.php');


if (isset($_POST['Register'])) {
    $obj = new validationclass();
    $obj->fnAdd('email', $_POST['email'], 'req', LANG_PLEASE_ENTER_EMAIL_ADDRESS); //'Please enter Email Address.'
    $obj->fnAdd('email', $_POST['email'], 'email', LANG_PLEASE_ENTER_VALID_EMAIL_ADDRESS);
    $obj->fnAdd('password', $_POST['password'], 'req', LANG_PLEASE_ENTER_PASSWORD);
    $obj->fnAdd('password', $_POST['password'], 'min=5', LANG_PLEASE_ENTER_PASSWORD_LENGTH_BETWEEN);
    $obj->fnAdd('password', $_POST['password'], 'max=10', LANG_PLEASE_ENTER_PASSWORD_LENGTH_BETWEEN);
    $obj->fnAdd('conpassword', $_POST['conpassword'], 'req', LANG_PLEASE_RE_ENTER_PASSWORD);
    $obj->fnAdd('firm', $_POST['firm'], 'req', LANG_FIRM);
    $obj->fnAdd('title', $_POST['title'], 'req', LANG_PLEASE_SELECT_TITLE);
    $obj->fnAdd("firstName", $_POST["firstName"], "req", LANG_PLEASE_ENTER_FIRST_NAME);    
    $obj->fnAdd('lastName', $_POST['lastName'], 'req', LANG_PLEASE_ENTER_LAST_NAME);    
    $obj->fnAdd("address1", $_POST["address1"], "req", LANG_PLEASE_ENTER_ADDRESS);
    $obj->fnAdd("country", $_POST["country"], "req", LANG_PLEASE_ENTER_COUNTRY);
    $obj->fnAdd("zipcode", $_POST["zipcode"], "req", LANG_PLEASE_ENTER_ZIP);
    $obj->fnAdd("city", $_POST["city"], "req", LANG_PLEASE_ENTER_CITY);
    $obj->fnAdd("contactnumber", $_POST["contactnumber"], "req", LANG_PLEASE_ENTER_CONTACT_NUM);
    $obj->fnAdd("mobilenumber", $_POST["mobilenumber"], "req", LANG_PLEASE_ENTER_MOBILE_NUM);
    $obj->fnAdd("birthday", $_POST["birthday"], "req", LANG_PLEASE_SELECT_BIRTHDAY);
    $obj->fnAdd("language", $_POST["language"], "req", LANG_PLEASE_SELECT_LANGUAGE);
    $obj->fnAdd("varification", $_POST["varification"], "req", LANG_PLEASE_ENTER_VERIFICATION_CODE);
    $obj->fnAdd("agreement", $_POST["agreement"], "req", LANG_PLEASE_ACCEPT_AGGREMENT);

    $arr_error = $obj->fnValidate();
    $str_validate = (count($arr_error)) ? 0 : 1;

    $arr_error['email'] = $obj->fnGetErr($arr_error['email']);
    $arr_error['password'] = $obj->fnGetErr($arr_error['password']);
    $arr_error['conpassword'] = $obj->fnGetErr($arr_error['conpassword']);
    $arr_error['firm'] = $obj->fnGetErr($arr_error['firm']);
    $arr_error['title'] = $obj->fnGetErr($arr_error['title']);
    $arr_error['firstName'] = $obj->fnGetErr($arr_error['firstName']);
    $arr_error['lastName'] = $obj->fnGetErr($arr_error['lastName']);
    $arr_error[address1] = $obj->fnGetErr($arr_error[address1]);
    $arr_error[country] = $obj->fnGetErr($arr_error[country]);
    $arr_error[city] = $obj->fnGetErr($arr_error[city]);
    $arr_error[zipcode] = $obj->fnGetErr($arr_error[zipcode]);
    $arr_error[contactnumber] = $obj->fnGetErr($arr_error[contactnumber]);
    $arr_error[mobilenumber] = $obj->fnGetErr($arr_error[mobilenumber]);
    $arr_error[birthday] = $obj->fnGetErr($arr_error[birthday]);
    $arr_error['language'] = $obj->fnGetErr($arr_error['language']);
    $arr_error['varification'] = $obj->fnGetErr($arr_error['varification']);
    $arr_error[agreement] = $obj->fnGetErr($arr_error[agreement]);

    if (!$obj->validPasswords($_POST['password'], $_POST['conpassword'])) {
        $arr_error[conpassword] = '<span class="alert-red alert-icon">' . LANG_PASSWORD_DONOT_MATCH . '</span>';
        $str_validate = 0;
    }
    if ($obj->validaSecurityCode($_POST['varification'])) {
        $arr_error['varification'] = '<span class="alert-red alert-icon">' . LANG_INVALID_VERIFICATION_CODE . '</span>';
        $str_validate = 0;
    }

    if ($signupObj->isUserEmailExistPhp($_POST['email'])) {
        $arr_error[email] = '<span class="alert-red alert-icon">' . LANG_EMAIL_ALREADY_EXIST . '</span>';
        $str_validate = 0;
    }

    if ($str_validate) {
        $_POST = postwithoutspace($_POST);
        $signupObj->registerNewUser($_POST);
        //~ echo '<pre>';
        //~ print_r($_POST);
        //~ echo '</pre>';
    }
    $queryStr = isset($_GET['ref']) ? $_GET['ref'] : $_POST['isSave'];
}
?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
    jQuery(document).ready( function() {
        jQuery("#registerForm").validationEngine();
    });
</script>
<div id="content" >
    <div class="subpage-border-top">
        <div class="subpage-border-bottom">
            <div class="subpage-border-mid">
                <form name="registerForm" id="registerForm" method="post" action="">
                    <input type="hidden" name="isSave" value="<?= $queryStr ?>" />
                    <div class="loginpage-bottom">
                        <h2><?= LANG_REGISTRATION ?></h2>
                        <div class="loginpage-mid">
                            <span><?= LANG_WELCOME_TO_THE_REGISTTRATION ?></span>
                            <ul class="registration-form">
                                <li><h3><?= LANG_LOGIN_INFORMATION ?></h3></li>
                                <li>
                                    <label><?= LANG_EMAIL ?> *</label>
                                    <div class="right_input">
                                        <input type="text" name="email" value="<?= $_POST['email']; ?>" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]] text-input" />							
                                    </div>
<?= $arr_error['email']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_PASSWORD ?> *</label>
                                    <div class="right_input">
                                        <input id="password" name="password" type="password" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['password']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_CONFIRM_YOUR_PASSWORD ?> *</label>
                                    <div class="right_input">
                                        <input type="password" name="conpassword" class="validate[required,equals[password]] text-input" />
                                    </div>
<?= $arr_error['conpassword']; ?>
                                </li>
                                <li><h3><?= LANG_PERSONAL_INFORMATION ?></h3></li>
                                <li>
                                    <label><?= wordwrap(LANG_FIRM_ORGANISATION_CLUB, 26, '<br />', true)?> *</label>
                                    <div class="right_input">
                                        <input type="text" name="firm" value="<?= stripslashes($_POST['firm']);  ?>" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['firm']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_TITLE ?> *</label>
                                    <div class="right_select">
                                        <select name="title" class="validate[required] text-input">
                                            <option value=""><?= LANG_SELECT_ONE ?></option>
<?= $signupObj->getUserTitleList($_POST['title']); ?>
                                        </select>
                                    </div>
                                            <?= $arr_error['title']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_FIRST_NAME ?> *</label>
                                    <div class="right_input">
                                        <input type="text" name="firstName" value="<?= stripslashes($_POST['firstName']); ?>" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['firstName']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_LAST_NAME ?> *</label>
                                    <div class="right_input">
                                        <input type="text" value="<?= stripslashes($_POST['lastName']); ?>" name="lastName" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['lastName']; ?>							
                                </li>
                                <li>
                                    <label><?= LANG_ADDRESS ?> 1*</label>
                                    <div class="right_input">
                                        <input type="text" value="<?= stripslashes($_POST['address1']); ?>" name="address1" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['address1']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_ADDRESS ?> 2</label>
                                    <div class="right_input">
                                        <input type="text" value="<?= stripslashes($_POST['address2']); ?>" name="address2"  />
                                    </div>
<?= $arr_error['address2']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_COUNTRY ?> *</label>
                                    <div class="right_select">
                                        <select name="country" class="validate[required] text-input">
                                            <option value=""><?= LANG_SELECT_ONE ?></option>
<?= $genObj->getMultiCountryList($_POST['country']); ?>
                                        </select>
                                    </div>
                                            <?= $arr_error['country']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_ZIP_CODE ?> *</label>
                                    <div class="right_input">
                                        <input type="text" value="<?= stripslashes($_POST['zipcode']); ?>" name="zipcode" class="validate[required,minSize[3],maxSize[30]] text-input" />
                                    </div>
<?= $arr_error['zipcode']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_CITY ?> *</label>
                                    <div class="right_input">
                                        <input name="city" type="text" value="<?= stripslashes($_POST['city']); ?>" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['city']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_PHONE_NUMBER ?> *</label>
                                    <div class="right_input">
                                        <input type="text" name="contactnumber" value="<?= stripslashes($_POST['contactnumber']); ?>" class="validate[required] text-input" />
                                    </div>
<?=$arr_error['contactnumber']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_MOBILE_NUMBER ?> *</label>
                                    <div class="right_input">
                                        <input type="text" name="mobilenumber" value="<?= stripslashes($_POST['mobilenumber']); ?>" class="validate[required] text-input" />
                                    </div>
<?=$arr_error['mobilenumber']; ?>
                                </li>

                                <li>
                                    <label><?= LANG_BIRTHDAY ?>*</label>
                                    <div class="right_input">
                                        <input type="text" style="width:150px;" id="birthday" name="birthday" value="<?= stripslashes($_POST['birthday']); ?>" class="validate[required] text-input" />
                                    </div>
<?= $arr_error['birthday']; ?>
                                </li>

                                <li>
                                    <label><?= LANG_LANGUAGE ?> *</label>
                                    <div class="right_select">
                                        <select name="language" class="validate[required] text-input">
                                            <option value=""><?= LANG_SELECT_ONE ?></option>
<?= $genObj->getMultiLanguageList($_SESSION['DEFAULTCOUNTRYID']); ?>
                                        </select>
                                    </div>
                                            <?= $arr_error['language']; ?>
                                </li>
                                <li>
                                    <label><?= LANG_VERIFICATION_CODE ?> *</label>
                                    <div class="right_input2">
                                        <input type="text" id="varification" name="varification" value="" class="validate[required] text-input" />
                                        <div class="capcha" id="captchadiv"><?php include ("imagecode.php") ?></div>
                                        <img src="images/refresh-captcha.png" border="0" onclick="reloadcaptcha();" style="cursor:pointer;float:right;vertical-align:middle; margin-top:5px;" />
                                    </div>
<?= $arr_error['varification']; ?>
                                </li>
                                <li class="terms-condition">
                                    <p><?= LANG_ACCEPT_AGGREMENT_TEXT ?> <a id="various1" href="term.php"><?= LANG_TERMS_AND_CONDITIONS ?></a> <input type="checkbox" class="validate[required] checkbox" name="agreement"></p>                             
<?= $arr_error['agreement']; ?></li>

                                <li>
                                    <label><?= LANG_SUBSCRIBE_NEWSLETTER ?></label> 

<p class="choic" style="font-size:13px; float:left;"><?= LANG_YES ?> <input type="radio" checked="checked" name="newsletter" value="1"></p>
<p class="choic" style="font-size:13px;"><?= LANG_NO ?> <input type="radio" class="validate[required] radio" name="newsletter" value="0"></p>
                                    <?= $arr_error['agreement']; ?>

                                </li>                             
                                <li>     <div class="login-button"  style="margin-left:200px;"><input name="Register" type="submit" value="<?= LANG_SEND ?>" /></div>
                                </li>
                                <li>
                                    <label>*This Fields are mandatory</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
                <div style="display: none;">
                    <div id="inline1" style="width:500px;height:200px;overflow:auto;">
<?php
$page = $genObj->getStaticPageInformation();
//echo $page->pageContaint;
?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Content-->
</div>
<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript">
    $(document).ready(function() {
        $("#various1").fancybox({
            'titlePosition'		: 'inside',
            'transitionIn'		: 'elastic',
            'transitionOut'		: 'elastic'
        });
    });
</script>
<?php include_once('includes/footer.php'); ?>
<link type="text/css" href="admin/datepicker/themes/base/ui.all.css" rel="stylesheet" />
<!--<script src="admin/js/jquery-1.6.2.js"></script>-->
<script src="admin/js/jquery.ui.core.js"></script>
<script src="admin/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(function() {
        $('#birthday').datepicker({
            dateFormat: 'yy-mm-dd',
            showButtonPanel: true,
            changeYear: true,
            yearRange: '1920:' + new Date().getFullYear().toString()
            
        });
    });
</script>

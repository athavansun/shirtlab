<?php include_once('includes/header.php'); ?>
<?php
	$genObj = new GeneralFunctions();
	$row = $genObj->getStaticPageInformation();

?>
<div id="content" >
	<div class="subpage-border-top">
		<div class="subpage-border-bottom">
			<div class="subpage-border-mid">
				<div class="loginpage-bottom">
                                    <h2><?php echo stripslashes($row->pageTitle);?></h2>
                                    <div class="loginpage-mid">
                                        <span>
                                            <?php echo stripslashes($row->pageContaint);?>
                                        </span>

                                    </div>
				</div>
				
			</div>
		</div>
	</div>
  <!--Content-->
</div>
<?php include_once('includes/footer.php'); ?>

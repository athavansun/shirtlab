<?php
session_start();

date_default_timezone_set("Asia/Kolkata");
echo date('Y-m-d H:i:s');

echo '<pre>';
print_r($_SESSION);

//print_r($_COOKIE);
//setcookie('collection', "", time() - 60 * 60 * 24 * 30, '/');
//echo $_COOKIE ['collection'].'<br>';
//echo $collectionName = time() . rand();
//setcookie('collection', $collectionName, time() + 60 * 60 * 24 * 30, '/');
//unset($_COOKIE ['collection']);
//echo $_COOKIE ['collection'];        

exit;
$fabricId = array('1'=>1, '2'=>1, '3'=>2, '4'=>3, '5'=>1, '6'=>2);
$quantity = array('1'=>10, '2'=>10, '3'=>20, '4'=>5, '5'=>10, '6'=>50);

$arr = array();
$totalQuantity = array();

function getQuantity($key, $val) {
    global $arr, $totalQuantity;    
    if(in_array($val, $arr)) {
        echo 1;
    } else {
        $arr[$key] = $val;
        echo 0;
    }
}

foreach($fabricId as $key => $val) {
    getQuantity($key, $val);
}




























exit;
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Variables extends CI_Controller {
  
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('login_model', 'login');
        $this->load->model('menu_model', 'menu');
        $this->load->model('variables_model', 'variables');
    }
    
    function index() {
        $result = $this->login->checkSession();
        $permission = true;
        if ($result && $permission) {
            if($this->input->post('input') == "Submit") {
                $this->action();
            }
            $this->load->view('template/header');
            $this->load->view('template/breadcrumb');
            $this->load->view('template/menu');
            $this->load->view('variables/manage_option');
            $this->load->view('template/footer');
        } else {
            redirect('login');
        }
    }
    
    function view() {
        $result=$this->login->checkSession();        
        if($result){
            $id = $this->uri->segment(3,0);
            $data =  $this->variables->getrecord($id);
            $this->load->view('variables/view_variables', $data);
        }else{
            redirect('login');
        }
    }
    
    function addvariable() {
        $result = $this->login->checkSession();
        $permission = true;
        if ($result && $permission) {
            if($this->input->post('input') == "Submit") {
                $this->action();
            }
            
            $data['name'] = $this->input->post('name');
            $data['value'] = $this->input->post('value');
        
            $this->load->view('template/header');
            $this->load->view('template/breadcrumb');
            $this->load->view('template/menu');
            $this->load->view('variables/addoption', $data);
            $this->load->view('template/footer');            
        } else {
            redirect('login');
        }
    }
    
    function isVariableExists($name) {
        if($this->variables->isVariableNameExists($name)) {
           return true; 
        } else {
            $this->form_validation->set_message('isVariableExists', $name.' already exists.');		
            return false;
        }
    }
    
    function add() {
        $data = $this->input->post();
        $this->form_validation->set_rules('name', $data['name'], 'is_unique['.TBL_VARIABLE.'.name]');           $this->form_validation->set_message('is_unique', "%s already exists.");
        $this->form_validation->set_rules('value', 'Variable Value', 'trim|required|xss_clean');
 
        if($this->form_validation->run() == TRUE)
        {
            $this->variables->addVariable($_POST);            
        } else{
            $this->addvariable($_POST);
            return false;
        }
    }
    
    function edit_variable() {
        $data = $this->input->post();
        $this->form_validation->set_rules('name', $data['name'], 'is_unique['.TBL_VARIABLE.'.name]');           $this->form_validation->set_message('is_unique', "%s already exists.");
        $this->form_validation->set_rules('value', 'Variable Value', 'trim|required|xss_clean');
 $this->form_validation->set_error_delimiters('<div class="alert alert-danger" style="width:40%;margin-bottom:0px;">','</div>');
        if($this->form_validation->run() == TRUE)
        {
            $this->variables->editVariable($_POST);            
        } else{
            $this->addvariable($_POST);
            return false;
        }
    }
    
    function edit() {
        if ($this->session->userdata('ADMIN_ID')) {
            $id = $this->uri->segment(3, 0);
            $data = $this->variables->getrecord($id);
            
            $this->load->view('template/header');
            $this->load->view('template/breadcrumb');
            $this->load->view('template/menu');
            $this->load->view('variables/edit_variable', $data);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('flashdata', '<div class="alert alert-block alert-danger">Oops! Session time out.</div>');
            redirect('login');
        }
    }
    
    public function action() {        
        $post = $this->input->post();
        if(isset($post['input'])) {
            $this->variables->multiAction($post);
        }
    }
}

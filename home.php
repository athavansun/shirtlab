<?php
include_once('config/configure.php');
include_once('includes/function/autoload.php');
$genObj = new GeneralFunctions();
require_once('setlanguage.php');
$indexObj = new IndexPage();
?>

<?php include_once('includes/header1.php'); ?>
<script type="text/javascript" src="js/slides.min.jquery.js"></script>
<script type="text/javascript">
    $(function(){
        $('#slides').slides({
            generateNextPrev: true,
            play: 3000,
            effect:'slide',
            slideSpeed: 900
        });
			
    });
</script>
<div id="content" class="home">
    <div class="banner"> 
        <a href="#" class="prev">Previous image</a> 
        <a href="#" class="next">Next image</a>

        <div id="slides">
            <?= $indexObj->showBanner('Home'); ?>
        </div>

    </div>
    <!-- home page content-->
    <?php
    //$page = $genObj->getStaticPageInformation();
    //echo $page->pageContaint;
    ?>
</div>
<!-- home page content ends-->
<?php include_once('includes/footer.php'); ?>

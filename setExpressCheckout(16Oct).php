<?php
include_once('includes/header.php');
$paymentType = $_GET['paymentType'];
$orderId = $_GET['id'];

$orderObj = new QuantityDelivery();
$result = $orderObj->executeQry("select * from " . TBL_ORDER . " where id = '" . $orderId . "'");
if ($orderObj->num_rows($result) > 0) {
    $order = $orderObj->fetch_object($result);
    $orderDetail = $orderObj->executeQry("select * from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "'");
    $addressRs = $orderObj->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $order->deliveryAddId . "'");
    $address = $orderObj->fetch_object($addressRs);
    $currencyCode = $orderObj->fetchValue(TBL_CURRENCY_DETAIL, "currencyCode", "id = '" . $order->currencyId . "'");
    $countryCode = $orderObj->fetchValue(TBL_COUNTRY, "country_2_code", "id = '" . $address->countryId . "'");
    ?>

    <!--<form id="checkout" action="includes/classes/ReviewOrder.php" method="POST">-->
    <form id="checkout" action="expresscheckout/ReviewOrder.php" method="POST">
        <input type="hidden" name="orderId" value="<?php echo $orderId; ?>" >
        <input type="hidden" name="paymentType" value="<?php echo $paymentType; ?>" >
        <input type="hidden" name="currencyCodeType" value="<?php echo $currencyCode; ?>" >    

        <?php
        if ($orderObj->num_rows($orderDetail) > 0) {
            $html = '';
            $i = 0;
            while ($line = $orderObj->fetch_object($orderDetail)) {
                $finalPrice = $orderObj->getVP_Price($line->final_price, $order->currencyId);
                $unitPrice = $finalPrice / $line->quantity;
                $html .= '<input type="hidden" name="L_NAME' . $i . '" value="' . $line->productCode . '" />';
                $html .= '<input type="hidden" name="L_AMT' . $i . '" value="' . $unitPrice . '" />';
                $html .= '<input type="hidden" name="L_QTY' . $i . '" value="' . $line->quantity . '" />';
                $i++;
            }
        }
        echo $html;
        ?>


        <input type="hidden" name="total" value="<?php echo $i; ?>" >
        <input type="hidden" name="discount" value="<?php echo $order->discount_ammount; ?>" >
        <input type="hidden" name="vat" value="<?php echo $orderObj->getVP_Price($order->vat, $order->currencyId); ?>" >
        <input type="hidden" name="PERSONNAME" value="<?php echo $address->firstName . " " . $address->lastName; ?>" >
        <input type="hidden" name="SHIPTOSTREET" value="<?php echo $address->address; ?>" >
        <input type="hidden" name="SHIPTOCITY" value="<?php echo $address->city; ?>" >    
        <input type="hidden" name="SHIPTOCOUNTRYCODE" value="<?php echo $countryCode; ?>" >
        <input type="hidden" name="SHIPTOZIP" value="<?php echo $address->zip; ?>" >
    </form>

    <?php
}
?>

<?php include_once('includes/footer.php'); ?>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.js"></script>

<script>
    $(document).ready(function() {
        setTimeout( $("#checkout").submit(), 1000);
    })
</script>
jQuery(document).ready(function() {


   
    jQuery('a[href=#top]').click(function(){
        jQuery('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });

});



/* Back to Top Code Begins */
jQuery(".backtotop").hide(); 
jQuery(document).scroll(function(){

if(jQuery(document).scrollTop()>15){
 
jQuery(".backtotop").fadeIn('slow'); 

}

else{
 
 jQuery(".backtotop").fadeOut('slow'); 

}

});

/* Back to Top Ends */

/* Custom scroll bar start=====*/
    (function($){
        $(window).load(function(){
            /*$(".block-layered-nav .block-content > dl > dd ol").mCustomScrollbar({
            theme:"3d-thick"
            });*/
            $.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
            $.mCustomScrollbar.defaults.axis="y"; //enable 2 axis scrollbars by default
            $(".block-layered-nav .block-content > dl > dd ol").mCustomScrollbar({theme:"3d-thick"});
        });
    })(jQuery);
/* Custom scroll bar end=====*/

/*Custom select box=================*/
jQuery(window).load(function(){
   jQuery('.shipping-form select').wrap('<span class="custom-select"></span>');
   jQuery('.form-list select').wrap('<span class="custom-select"></span>');
   jQuery('.payment_form_ccsave select').wrap('<span class="custom-select"></span>');
});

/*Custom select box=================*/
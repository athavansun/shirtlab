<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE IF EXISTS `{$installer->getTable('shopbrand/shopbrand')}`;
    CREATE TABLE `{$installer->getTable('shopbrand/shopbrand')}` (
        `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `brand_name` VARCHAR(255) NOT NULL,
        `url_key` VARCHAR(255) NOT NULL,
        `description` VARCHAR(255) NOT NULL,
        `logo` VARCHAR(255) NOT NULL, 
        `sort_order` INT NOT NULL, 
        `option_id` INT NOT NULL,
        `brand_attribute` VARCHAR(255) NOT NULL,
        `attribute_status` smallint(8) NOT NULL default '0',
        `status` TINYINT NOT NULL,
        UNIQUE (option_id) 
       )
        ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shopbrand';
");

$installer->run("
    DROP TABLE IF EXISTS `{$installer->getTable('shopbrand/shopbrandstore')}`;
    CREATE TABLE `{$installer->getTable('shopbrand/shopbrandstore')}` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `brand_id` INT NOT NULL ,
    `store_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    KEY `brand_id` (`brand_id`),
    CONSTRAINT `FK_sparx_shopbrand_store` FOREIGN KEY (`brand_id`) REFERENCES {$this->getTable('sparx_shopbrand')} (`id`) ON DELETE CASCADE ON UPDATE CASCADE
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Shopbrand Store';
");

$installer->endSetup();
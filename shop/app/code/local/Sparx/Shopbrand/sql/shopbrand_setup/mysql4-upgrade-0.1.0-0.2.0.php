<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('shopbrand/shopbrand')} ADD (
    `brand_address` varchar(255) NOT NULL default 0,
    `brand_email` varchar(255) NOT NULL default 0,
    `brand_contact` bigint NOT NULL default 0,
    `brand_timings` varchar(255) NOT NULL default 0    
    )");
$installer->endSetup();

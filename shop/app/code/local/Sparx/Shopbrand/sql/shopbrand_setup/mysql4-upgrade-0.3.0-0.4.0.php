<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('shopbrand/shopbrand')} ADD (
    `content` TEXT default ''
    )");
$installer->endSetup();

<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('shopbrand/shopbrand')} ADD (
    `is_home_page` smallint(8) NOT NULL default '0'
    )");
$installer->endSetup();

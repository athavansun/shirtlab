<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx It solution Pvt. Ltd.
 */
class Sparx_Shopbrand_Controller_Customrouter extends Mage_Core_Controller_Varien_Router_Abstract {

    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initBrandControllerRouters($observer) {
        $front = $observer->getEvent()->getFront();
        $front->addRouter('shopbrand', $this);
    }
    
    
    /**
     * Validate and Match Shopbrand Pages and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request) {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect(Mage::getUrl('install'))
                    ->sendResponse();
            exit;
        }


        $identifier = trim($request->getPathInfo(), '/');
        $brandident = Mage::helper('shopbrand')->getBrandIdentifier();
        $seosuffix = Mage::helper('shopbrand')->getBrandSeoUrlSuffix();
        $identifier = str_replace($seosuffix, '', $identifier);
        $storeid = Mage::app()->getStore()->getId();

        if ($identifier == $brandident) {
            $request->setModuleName('shopbrand')
                    ->setControllerName('index')
                    ->setActionName('index');

            return true;
        } elseif ($identifier != $brandident) {

            $brandId = Mage::getModel('shopbrand/shopbrand')->checkBrandIdentifier($identifier, $storeid);
            if (!$brandId) {
                return false;
            }
            $request->setModuleName('shopbrand')
                    ->setControllerName('index')
                    ->setActionName('view')
                    ->setParam('id', $brandId);

            $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $identifier
            );

            return true;
        } elseif (strpos($identifier, $brandident) === 0 && strlen($identifier) > strlen($brandident)) {
            $identifier = trim(substr($identifier, strlen($brandident)), '/');
            $brandId = Mage::getModel('shopbrand/shopbrand')->checkBrandIdentifier($identifier, $storeid);
            if (!$brandId) {
                return false;
            }
            $request->setModuleName('shopbrand')
                    ->setControllerName('index')
                    ->setActionName('view')
                    ->setParam('id', $brandId);

            $request->setAlias(
                    Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $identifier
            );
            return true;
        } else{
          return false;
        }
    }

}
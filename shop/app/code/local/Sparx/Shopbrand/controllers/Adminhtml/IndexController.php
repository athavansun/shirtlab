<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->_initAction();
        $this->renderLayout();
    }

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('Sparx_Shopbrand')
                ->_title($this->__('Manage Brands'));
        return $this;
    }

    /**
     * init the shopbrand
     * @access protected
     * @return Sparx_Shopbrand_Model_Shopbrand
     */
    protected function _initShopbrand() {
        $shopbrandId = (int) $this->getRequest()->getParam('id');
        $shopbrand = Mage::getModel('shopbrand/shopbrand');
        if ($shopbrandId) {
            $shopbrand->load($shopbrandId);
        }
        Mage::register('sparx_shopbrand', $shopbrand);
        return $shopbrand;
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $shopbrandId = $this->getRequest()->getParam('id');
        $shopbrand = $this->_initShopbrand();
        if ($shopbrandId && !$shopbrand->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shopbrand')->__('This shop brand no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getShopbrandData(true);
        if (!empty($data)) {
            $shopbrand->setData($data);
        }
        Mage::register('shopbrand_data', $shopbrand);
        $this->loadLayout();
        $this->_setActiveMenu('Sparx_Shopbrand');
        $this->_title(Mage::helper('shopbrand')->__('Shopbrand'))
                ->_title(Mage::helper('shopbrand')->__('Shop brand'));
        if ($shopbrand->getId()) {
            $this->_title($shopbrand->getBrandName());
        } else {
            $this->_title(Mage::helper('shopbrand')->__('Add shop brand'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }

    public function saveAction() {
        if ($postData = $this->getRequest()->getPost()) {

            if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {
                    /* Starting upload */
                    $uploader = new Varien_File_Uploader('filename');

                    // Any extention would work
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(false);

                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //	(file.jpg will go in something like /media/f/i/file.jpg)
                    $uploader->setFilesDispersion(false);

                    // We set media as the upload dir
                    $path = Mage::getBaseDir('media') . DS;
                    $uploader->save($path, $_FILES['filename']['name']);
                } catch (Exception $e) {
                    
                }

                //this way the name is saved in DB
                $postData['filename'] = $_FILES['filename']['name'];
            }
            $shopbrand = $this->_initShopbrand();
            $optionId = '';
            $products = $this->getRequest()->getPost('products', -1);
            if ($products != -1) {
                $shopbrand->setProductsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($products));
            }
            $media_path = Mage::getBaseDir('media') . DS . 'shopbrand';
            $model = Mage::getSingleton('shopbrand/shopbrand');
            $storeModel = Mage::getModel('shopbrand/shopbrandstore');
            $optionName = $postData['brand_name'];
            $sortOrder = $postData['sort_order'];
            $storeIds = $postData['store_id'];
            
            if (isset($postData['shopbrand_tabs']['images']) && !empty($postData['shopbrand_tabs']['images'])) {
                $images = Mage::helper('core')->jsonDecode($postData['shopbrand_tabs']['images'], true);
                //$images = json_decode($postData['shopbrand_tabs']['images'],true);
                $newArray = array();
                foreach ($images as $key => $image) {
                    if ($image['removed'] != 1) {
                        $newArray[] = $image;
                    }
                }
                $content = Mage::helper('core')->jsonEncode($newArray);
                $postData['content'] = $content;
                unset($postData['shopbrand_tabs']);
            }
//            echo '<pre>';print_r($postData);exit;
            try {
                if (isset($postData['url_key']) && !empty($postData['url_key'])) {
                    $model->checkUrlKey($postData);
                }
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($this->__($ex->getMessage()));
                Mage::getSingleton('adminhtml/session')->setShopbrandData($postData);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            if (isset($postData['id']) && !empty($postData['id'])) {
                if (isset($postData['logo']['delete']) && $postData['logo']['delete'] == 1) {
                    unset($postData['delete']);
                    $postData['logo'] = '';
                    $shopBrandId = $postData['id'];
                    $model->load($shopBrandId);
                    $logo = $model->getLogo();
                    Mage::helper('shopbrand')->deleteLogoImage($logo);
                } elseif (isset($_FILES['logo']) && $_FILES['logo']['error'] == 0) {
                    try {
                        $uploader = new Varien_File_Uploader($_FILES['logo']);
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $imgFilename = strtotime(date('Y-m-d H:i:s')) . '_' . strtolower($_FILES['logo']['name']);
                        $postData['logo'] = Mage::getBaseUrl('media') . 'shopbrand/' . Varien_File_Uploader::getCorrectFileName($imgFilename);
                        $uploader->save($media_path, $imgFilename);
                    } catch (Exception $e) {
                        Mage::log($e);
                        Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this record.'));
                    }
                } elseif (isset($postData['logo']['value'])) {
                    $postData['logo'] = $postData['logo']['value'];
                }
                $shopBrandId = $postData['id'];
                $model->load($shopBrandId);
                $optionId = $model->getOptionId();
                try {
                    $attrcode = $model->updateAttributeOption($optionId, $optionName, $sortOrder);
                    $postData['brand_attribute'] = $attrcode;
                } catch (Exception $ex) {
                    
                }
            } else {
                if (isset($_FILES['logo']) && $_FILES['logo']['error'] == 0) {
                    try {
                        $uploader = new Varien_File_Uploader($_FILES['logo']);
                        $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $imgFilename = strtotime(date('Y-m-d H:i:s')) . '_' . strtolower($_FILES['logo']['name']);
                        $postData['logo'] = Mage::getBaseUrl('media') . 'shopbrand/' . Varien_File_Uploader::getCorrectFileName($imgFilename);
                        $uploader->save($media_path, $imgFilename);
                    } catch (Exception $e) {
                        Mage::log($e);
                        Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this record.'));
                        Mage::getSingleton('adminhtml/session')->setShopbrandData($postData);
                        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                        return;
                    }
                }
                $brandattribute = Mage::helper('shopbrand')->getBrandAttributeCode();
                $postData['brand_attribute'] = $brandattribute;
                $postData['option_id'] = $model->addAttributeOption($optionName, $sortOrder);
            }
            $optionId = isset($postData['option_id']) ? $postData['option_id'] : $optionId;
            try {
                if ($optionId && isset($postData['brand_name']) && !empty($postData['brand_name'])) {
                    $model->checkBrandOption($postData, $optionId);
                }
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($this->__($ex->getMessage()));
                Mage::getSingleton('adminhtml/session')->setShopbrandData($postData);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            try {

                $postData['attribute_status'] = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
//                echo '<pre>';print_r($postData);exit;
                $model->setData($postData);
                $model->save();
                $brandId = $model->getId();
                $storesCollection = $storeModel->getCollection()->addFieldToFilter('brand_id', $brandId);
                foreach ($storesCollection as $storeCollection) {
                    $storeCollection->delete();
                }
                foreach ($storeIds as $storeId) {
                    $data['brand_id'] = $brandId;
                    $data['store_id'] = $storeId;
                    $storeModel->setData($data);
                    $storeModel->save();
                }

                if ($products != -1) {
                    $productids = explode('&', $products);
                    $productids = array_filter($productids, 'is_numeric');
                    if ((count($productids) > 0 && $productids[0] != '')) {
//                $model->setAttributeToProducts($optionId, $productIds);
                        $model->setSqlProducts($optionId, $productids);
                    } else {
                        $productids = $model->getSelectedProducts();
                        $model->removeSelectedProducts($productids);
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The record has been saved.'));
                Mage::getSingleton('adminhtml/session')->setShopbrandData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this record.' . $e->getMessage()));
                Mage::getSingleton('adminhtml/session')->setShopbrandData($postData);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shopbrand')->__('Unable to find shop brand to save.'));
            $this->_redirectReferer();
        }
    }

    public function massDeleteAction() {
        $shopbrandIds = $this->getRequest()->getParam('id');
        if (!is_array($shopbrandIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($shopbrandIds as $shopbrandId) {
                    $shopbrand = Mage::getModel('shopbrand/shopbrand')->load($shopbrandId);
                    $optionId = $shopbrand->getOptionId();
                    $logo = $shopbrand->getLogo();
                    $shopbrand->deleteAttributeOption($optionId);
                    if ($logo != '') {
                        Mage::helper('shopbrand')->deleteLogoImage($logo);
                    }
                    $shopbrand->delete();
                    $storeModel = Mage::getModel('shopbrand/shopbrandstore');
                    $storesCollection = $storeModel->getCollection()->addFieldToFilter('brand_id', $shopbrandId);
                    foreach ($storesCollection as $storeCollection) {
                        $storeCollection->delete();
                    }
                    $shopbrand->unsetData();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($shopbrandIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction() {
        $shopbrandIds = $this->getRequest()->getParam('id');
        if (!is_array($shopbrandIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($shopbrandIds as $shopbrandId) {
                    if ($this->getRequest()->getParam('status') == 2) {
                        $isactive = 0;
                    } else {
                        $isactive = $this->getRequest()->getParam('status');
                    }
                    $shopbrand = Mage::getModel('shopbrand/shopbrand')
                            ->load($shopbrandId)
                            ->setStatus($isactive)
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', count($shopbrandIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $id = $this->getRequest()->getParam('id');
                $model = Mage::getModel('shopbrand/shopbrand');
                $object = Mage::getModel('shopbrand/shopbrand')->load($id);
                $model->setId($id);
                $optionId = $object->getOptionId();
                $logo = $object->getLogo();
                $model->deleteAttributeOption($optionId);
                if ($logo != '') {
                    Mage::helper('shopbrand')->deleteLogoImage($logo);
                }
                $model->delete();
                $storeModel = Mage::getModel('shopbrand/shopbrandstore');
                $storesCollection = $storeModel->getCollection()->addFieldToFilter('brand_id', $id);
                foreach ($storesCollection as $storeCollection) {
                    $storeCollection->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Record was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function brandImportAction() {
        $collection = Mage::getModel('shopbrand/shopbrand')->getCollection();
        $collection->getSelect()->reset('columns')->columns(array('GROUP_CONCAT(option_id) as optionIds'));
        $brandData = $collection->load()->getData();
        $brandIds = $brandData[0]['optionIds'];
        if (count($brandData))
            if ($brandData[0]['optionIds'] == '')
                $brandIds = 0;

        $resource = Mage::getSingleton('core/resource');
        $attributeCode = Mage::helper('shopbrand')->getBrandAttributeCode();
        $storeId = Mage::app()->getStore()->getId();
        $_product = Mage::getModel('catalog/product');
        /*      load options with optionid & option value       */
        $_optionCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($_product->getResource()->getTypeId())
                ->addFieldToFilter('attribute_code', $attributeCode);
        $_optionCollection->getSelect()
                ->reset('columns')
                ->join(array('op' => $resource->getTableName('eav/attribute_option')), 'op.attribute_id=main_table.attribute_id', 'op.option_id')
                ->where('op.option_id not in (' . $brandIds . ')')
                ->join(array('opval' => $resource->getTableName('eav/attribute_option_value')), 'op.option_id=opval.option_id and opval.store_id=' . $storeId, 'value');

        $options = $_optionCollection->load();
        $productUrl = Mage::getModel('catalog/product_url');
        $no_of_import = 0;
        if (count($options) > 0) {
            $shopbrand = Mage::getModel('shopbrand/shopbrand');
            foreach ($options as $option) {
                $shopbrand->unsetData();
                try {
                    $urlKey = $productUrl->formatUrlKey(strtolower($option->getValue()));
                    $urlKey = Mage::helper('shopbrand')->getBrandOptionUrlkey($urlKey);
                    $shopbrand->setBrandName(ucfirst($option->getValue()));
                    $shopbrand->setUrlKey($urlKey);
                    $shopbrand->setStoreId($storeId);
                    $shopbrand->setDescription('');
                    $attrstatus = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
                    $shopbrand->setAttributeStatus($attrstatus);
                    $shopbrand->setBrandAttribute($attributeCode);
                    $shopbrand->setStatus('0');
                    $shopbrand->setOptionId($option->getOptionId());
                    $shopbrand->save();
                    $store = Mage::getModel('shopbrand/shopbrandstore');
                    $store->setBrandId($shopbrand->getId())->setStoreId(0)->save();
                    ++$no_of_import;
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('adminhtml')->__($e)
                    );
                }
            }
            if ($no_of_import)
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__($no_of_import . ' brands are added.'));
        }
        if (!$no_of_import)
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Sorry! no brand is added.'));
        $this->_redirectUrl($this->_getRefererUrl());
        return;
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * get grid of products action
     * @access public
     * @return void
     * @author Sparx Module Creator
     */
    public function productsAction() {
        $this->_initShopbrand();
        $this->loadLayout();
        $block = $this->getLayout()->getBlock('shopbrand.edit.tab.products');
        $block->setShopbrandProducts($this->getRequest()->getPost('shopbrand_products', null));
        $this->renderLayout();
    }

    /**
     * get grid of products action
     * @access public
     * @return void
     * @author Sparx Module Creator
     */
    public function productsgridAction() {
        $this->_initShopbrand();
        $this->loadLayout();
        $block = $this->getLayout()->getBlock('shopbrand.edit.tab.products');
        $block->setShopbrandProducts($this->getRequest()->getPost('shopbrand_products', null));
        $this->renderLayout();
    }

    public function exportCsvAction() {
        $fileName = 'shopbrand.csv';
        $content = $this->getLayout()->createBlock('shopbrand/adminhtml_shopbrand_grid')
                ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction() {
        $fileName = 'shopbrand.xml';
        $content = $this->getLayout()->createBlock('shopbrand/adminhtml_shopbrand_grid')
                ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }

    public function uploadAction() {
        try {
            //Mage::log($_FILES);
            /* $mageVersion = Mage::getVersionInfo();
              if($mageVersion<'1.5.1.0'){
              $uploader = new Mage_Core_Model_File_Uploader('image');
              }else{
              $uploader = new Varien_File_Uploader('image');
              } */
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->addValidateCallback('catalog_product_image', Mage::helper('catalog/image'), 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save($this->getBaseTmpMediaPath());

            Mage::dispatchEvent('catalog_product_gallery_upload_image_after', array(
                'result' => $result,
                'action' => $this
            ));

            /**
             * Workaround for prototype 1.7 methods "isJSON", "evalJSON" on Windows OS
             */
            $result['tmp_name'] = str_replace(DS, "/", $result['tmp_name']);
            $result['path'] = str_replace(DS, "/", $result['path']);
            $tempUrl = $this->_prepareFileForUrl($result['file']);
            if (substr($tempUrl, 0, 1) == '/') {
                $tempUrl = substr($tempUrl, 1);
            }
            $result['url'] = $this->getBaseTmpMediaUrl() . '/' . $tempUrl;
            /* $result['url'] = Mage::getSingleton('catalog/product_media_config')->getTmpMediaUrl($result['file']); */

            $result['file'] = $result['file'];
            $result['cookie'] = array(
                'name' => session_name(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain()
            );
        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function getBaseTmpMediaUrl() {
        return Mage::getBaseUrl('media') . 'shopbrand';
    }

    public function getBaseTmpMediaPath() {
        return Mage::getBaseDir('media') . DS . 'shopbrand';
    }

    protected function _prepareFileForUrl($file) {
        return str_replace(DS, '/', $file);
    }

}

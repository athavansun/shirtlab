<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Shopbrand Index index action
     * @access public
     * @author Sparx
     */
    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * Shopbrand Index view action
     * @access public
     * @author Sparx
     */
    public function viewAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    

}
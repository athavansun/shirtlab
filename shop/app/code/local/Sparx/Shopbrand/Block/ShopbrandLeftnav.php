<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_ShopbrandLeftnav extends Mage_Catalog_Block_Product_Abstract {

    const VIEW_TYPE = 'shopbrand/layerednav/viewtype';

/*
 * @ selects Template for left shop brand option 
 */
    
    protected function _tohtml() {
        if (Mage::getStoreConfig(self::VIEW_TYPE) == "dropdown") {
            $this->setTemplate("shopbrand/dropdown.phtml");
        } elseif (Mage::getStoreConfig(self::VIEW_TYPE) == "list") {
            $this->setTemplate("shopbrand/list.phtml");
        } else {
            $this->setTemplate("shopbrand/dropdown.phtml");
        }

        $this->setLinksforProduct();
        return parent::_toHtml();
    }
  
/*
 * @ Returns collection of model shopbrand with enable status
 */    

    public function getLeftBarBrand() {
        $brandattr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $brandattrstatus = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
        $collection = Mage::getModel('shopbrand/shopbrand')->getCollection()
                      ->addFieldToFilter('status', 1)
                      ->addFieldToFilter('brand_attribute',$brandattr)
                      ->addFieldToFilter('attribute_status',$brandattrstatus);
        $brandstoreTbl = Mage::getSingleton('core/resource')->getTableName('shopbrand/shopbrandstore');
        $currentstore = Mage::app()->getStore()->getId();
        $stores = array(
            $currentstore,
            Mage_Core_Model_App::ADMIN_STORE_ID
        );
        $collection->getSelect()->join(
                array('shopbrandstore' => $brandstoreTbl), 'main_table.id = shopbrandstore.brand_id', array('store'=>'store_id')
        )->where('shopbrandstore.store_id in (?) ', $stores)
         ->group('main_table.id');
        return $collection;
    }

}

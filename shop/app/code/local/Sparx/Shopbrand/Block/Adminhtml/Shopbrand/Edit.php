<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    /**
     * Init class
     */
    public function __construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = 'shopbrand';
        $this->_controller = 'adminhtml_shopbrand';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('shopbrand')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('shopbrand')->__('Delete'));
        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('shopbrand')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), -100);
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('shopbrand_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'shopbrand_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'shopbrand_content');
                }
            }

            function saveAndContinueEdit(){
                               
                    editForm.submit($('edit_form').action+'back/edit/');
            }
		";
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText() {
        if (Mage::registry('shopbrand_data') && Mage::registry('shopbrand_data')->getId()) {
            return Mage::helper('shopbrand')->__("Edit Shop brand '%s'", $this->htmlEscape(Mage::registry('shopbrand_data')->getBrandName()));
        } else {
            return Mage::helper('shopbrand')->__('Add Shop brand');
        }
    }

}

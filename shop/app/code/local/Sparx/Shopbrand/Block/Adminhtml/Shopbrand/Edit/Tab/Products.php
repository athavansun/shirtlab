<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Edit_Tab_Products extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('sparx_shop_brand_products_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getShopbrand()->getId()) {
            $this->setDefaultFilter(array('in_products' => 1));
        }
    }

    /**
     * Prepare Product collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection() {

        $collection = Mage::getModel('catalog/product_link')->useRelatedLinks()
                ->getProductCollection()
                ->addAttributeToSelect('*');
//                $collection = Mage::getResourceModel('catalog/product_collection')
//                ->addAttributeToSelect('*');

        if ($this->isReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = array(0);
            }
            $collection->addFieldToFilter('entity_id', array('in' => $productIds));
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Checks when this block is readonly
     *
     * @return boolean
     */
    public function isReadonly() {
        return 0;
    }

    protected function _prepareColumns() {
        $helper = Mage::helper('shopbrand');

        if (!$this->isReadonly()) {
            $this->addColumn('in_products', array(
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_products',
                'values' => $this->_getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id'
            ));
        }
        $this->addColumn('entity_id', array(
            'type' => 'number',
            'name' => 'entity_id',
            'align' => 'center',
            'index' => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('type', array(
            'header' => $helper->__('Type'),
            'width' => 100,
            'index' => 'type_id',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                ->load()
                ->toOptionHash();

        $this->addColumn('set_name', array(
            'header' => $helper->__('Attrib. Set Name'),
            'width' => 130,
            'index' => 'attribute_set_id',
            'type' => 'options',
            'options' => $sets,
        ));

        $this->addColumn('status_product', array(
            'header' => $helper->__('Status'),
            'width' => 90,
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        $this->addColumn('visibility', array(
            'header' => $helper->__('Visibility'),
            'width' => 90,
            'index' => 'visibility',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_visibility')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header' => $helper->__('SKU'),
            'width' => 80,
            'index' => 'sku'
        ));

        $this->addColumn('price', array(
            'header' => $helper->__('Price'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'price'
        ));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action grid
     * @access protected
     * @return Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Edit_Tab_Product
     * @author Sparx Module Creator
     */
    protected function _prepareMassaction() {
        return $this;
    }

    /**
     * get the current shopbrand
     * @access public
     * @return Sparx_Shopbrand_Model_Shopbrand
     * @author Sparx Module Creator
     */
    public function getShopbrand() {
        return Mage::registry('sparx_shopbrand');
    }

    /**
     * Retrieve selected products
     * @access protected
     * @return array
     * @author Sparx Module Creator
     */
    public function _getSelectedProducts() {
        $entityIds = $this->getSelectedProducts();
        if (!is_array($entityIds)) {
            $entityIds = array($this->getSelectedProducts());
        }
        return $entityIds;
    }

    /**
     * Retrieve selected products
     * @access protected
     * @return array
     * @author Sparx Module Creator
     */
    public function getSelectedProducts() {
        $products = array();
        $selected = Mage::registry('sparx_shopbrand')->getSelectedProducts();
        if (!is_array($selected)) {
            $selected = array();
        }
        foreach ($selected as $product) {
            $products[] = $product;
        }
        return $products;
    }

    /**
     * Apply column Filter on collection
     * @access protected
     * @return array
     * @author Sparx Module Creator
     */
    protected function _addColumnFilterToCollection($column) {

        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {

            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {

                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {

                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/productsGrid', array(
                    'id' => $this->getShopbrand()->getId()
        ));
    }

}
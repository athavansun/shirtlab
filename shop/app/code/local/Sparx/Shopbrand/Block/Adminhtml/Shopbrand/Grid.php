<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    protected $_filteroptions = array();
    public function __construct() {
        parent::__construct();
        $this->setId('sparx_shopbrand_id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

   /**
    * Modify column filter if needed by custom implementation 
    *
    */
    protected function _addColumnFilterToCollection($column)
    {
     if (($column->getId() === 'massaction')) {
       $this->_filteroptions['massaction']=$column->getId();
     }
     if (($column->getId() === 'id')) {
       $this->_filteroptions['id']=$column->getId();
     }
       parent::_addColumnFilterToCollection($column);
       return $this;
    }

    protected function _prepareCollection() {
        $brandattr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $brandattrstatus = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
        $collection = Mage::getModel('shopbrand/shopbrand')->getCollection()
                     ->addFieldToFilter('brand_attribute',$brandattr)
                     ->addFieldToFilter('attribute_status',$brandattrstatus);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $helper = Mage::helper('shopbrand');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id'
        ));
        $this->addColumn('brand_name', array(
            'header' => $helper->__('Name'),
            'index' => 'brand_name'
        ));

        $this->addColumn('products', array(
            'header' => $helper->__('Products'),
            'align' => 'center',
            'type' => 'text',
            'width' => '100px',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Renderer_Gridproductcount',
        ));

        $this->addColumn('url_key', array(
            'header' => $helper->__('Url Key'),
            'index' => 'url_key'
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header' => $helper->__('Store View'),
                'index' => 'store_id',
                'type' => 'store',
                'store_all' => true,
                'store_view' => true,
                'width' => '150px',
                'sortable' => false,
                'filter_condition_callback' => array($this, '_filterStoreCondition'),
            ));
        }
        $this->addColumn('description', array(
            'header' => $helper->__('Description'),
            'index' => 'description'
        ));
        $this->addColumn('logo', array(
            'header' => $helper->__('Logo'),
            'align' => 'center',
            'type' => 'text',
            'width' => '100px',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Renderer_Gridlogo',
        ));
        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'align' => 'left',
            'width' => '100px',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled',
            ),
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('shopbrand')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('shopbrand')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('shopbrand')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('shopbrand')->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection() {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column) {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $filterarr = $this->_filteroptions;
        if(!(is_array($filterarr) && (in_array('massaction', $filterarr) || in_array('id', $filterarr)))){
          $this->getCollection()->addStoreFilter($value);
        }
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('shopbrand')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('shopbrand')->__('Are you sure?')
        ));

        $statuses = Mage::getModel('shopbrand/status')->getOptionArray();
        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('shopbrand')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('shopbrand')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
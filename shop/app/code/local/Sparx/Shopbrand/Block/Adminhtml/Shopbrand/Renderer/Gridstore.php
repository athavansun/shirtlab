<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Renderer_Gridstore extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected function _getValue(Varien_Object $row) {
        $shopbrand_id = $row->getData("id");
        $html = '';
        try {
            $shopbrandstore = Mage::getSingleton('shopbrand/shopbrandstore')->getCollection()
                              ->addFieldToFilter('brand_id', $shopbrand_id);
            $wstorearr = array();
            $gstorearr = array();
            if(count($shopbrandstore)>0){
            foreach($shopbrandstore as $shopstore){
                $shopstoreid = $shopstore->getStoreId();
              $store_model = Mage::getModel('core/store')->load($shopstoreid);
              if($store_model->getId() == Mage_Core_Model_App::ADMIN_STORE_ID){
                  return $html.='All Store Views';
                  break;
              }else{
                  if(count($shopbrandstore)< 2){
                    $store_group = Mage::getModel('core/store_group')->load($store_model->getGroupId());
                    $website = Mage::app()->getWebsite($store_model->getWebsiteId());
                    $html.=$website->getName().'<br/>';
                    $html.='&nbsp;&nbsp;&nbsp;'.$store_group->getName().'<br/>';
                    $html.='&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;'.$store_model->getName().'<br/>';
                  }else{
                      if(!in_array($store_model->getWebsiteId(),$storearr)){
                         $website = Mage::app()->getWebsite($store_model->getWebsiteId());
                         $html.=$website->getName().'<br/>'; 
                         $storearr[] = $store_model->getWebsiteId();
                      }
                      if(!in_array($store_model->getGroupId(),$gstorearr)){
                         $store_group = Mage::getModel('core/store_group')->load($store_model->getGroupId());
                         $html.='&nbsp;&nbsp;&nbsp;'.$store_group->getName().'<br/>';
                         $gstorearr[] = $store_model->getGroupId();
                      }
                     $html.='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$store_model->getName().'<br/>';
                  }
              }
            }
           } 
        } catch (Exception $e) {
            
        }
        return $html;
    }
}
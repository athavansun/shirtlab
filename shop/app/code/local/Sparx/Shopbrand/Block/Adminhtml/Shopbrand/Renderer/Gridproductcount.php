<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Renderer_Gridproductcount extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected function _getValue(Varien_Object $row) {
        $attrValue = Mage::helper('shopbrand')->getBrandAttributeCode();
        $optionId = $row->getData("option_id");
        $products = Mage::getModel('catalog/product')->getCollection();
        $products->addAttributeToSelect($attrValue);
        $products->addFieldToFilter(array(array('attribute' => $attrValue, 'eq' => $optionId)));
        return ((count($products) > 0) ? count($products) : '0');
    }

}
<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('shopbrand_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('shopbrand')->__('Shopbrand Information'));
    }

    protected function _beforeToHtml() {
        $this->addTab('form_section', array(
            'label' => Mage::helper('shopbrand')->__('General'),
            'title' => Mage::helper('shopbrand')->__('Shopbrand Information'),
            'content' => $this->getLayout()->createBlock('shopbrand/adminhtml_shopbrand_edit_tab_form')->toHtml(),
        ));
//        $this->addTab('form_product_section', array(
//            'label' => Mage::helper('shopbrand')->__('Attached Product'),
//            'title' => Mage::helper('shopbrand')->__('Attached Product Information'),
//            'content' => $this->getLayout()->createBlock('shopbrand/adminhtml_shopbrand_edit_tab_products')->toHtml(),
//        ));
        $this->addTab('form_product_section', array(
            'label' => Mage::helper('shopbrand')->__('Attached Product'),
            'title' => Mage::helper('shopbrand')->__('Attached Product Information'),
            'url' => $this->getUrl('*/*/products', array('_current' => true)),
            'class' => 'ajax',
        ));
        
        $content = Mage::getSingleton('core/layout')->createBlock('shopbrand/adminhtml_shopbrand_edit_tab_gallery');
        $content->setId($this->getHtmlId() . '_content')->setElement($this);

        /*$this->addTab('gallery_section', array(
            'label' => Mage::helper('shopbrand')->__('Brand Images'),
            'title' => Mage::helper('shopbrand')->__('Brand Images'),
            'content' => $content->toHtml(),
        ));*/
        
        return parent::_beforeToHtml();
    }

    protected function _updateActiveTab() {
        $tabId = $this->getRequest()->getParam('form_product_section');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        } else {
            $this->setActiveTab('form_section');
        }
    }

}

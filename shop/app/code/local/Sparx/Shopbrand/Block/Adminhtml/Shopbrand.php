<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Adminhtml_Shopbrand extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_blockGroup = 'shopbrand';
        $this->_controller = 'adminhtml_shopbrand';
        $this->_headerText = Mage::helper('shopbrand')->__('Manage Brands');
        $this->_addButtonLabel = 'Add New Brand';
        $this->_addButton('import_brand', array(
            'label'     => Mage::helper('sales')->__('Import Existing Brands'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('shopbrandadmin/adminhtml_index/brandImport') . '\')',
        ));
        parent::__construct();
    }

}
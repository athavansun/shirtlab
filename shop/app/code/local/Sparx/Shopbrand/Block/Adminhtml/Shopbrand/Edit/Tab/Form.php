<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx 
 */
class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * Init class
     */
    public function __construct() {
        parent::__construct();
        $this->setId('sparx_shopbrand_tab_container');
        $this->setTitle($this->__('Shopbrand Information'));
    }

    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm() {
        $model = Mage::registry('sparx_shopbrand');
        $helper = Mage::helper('shopbrand');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $helper->__('Shop Brand Information'),
            'class' => 'fieldset-wide',
        ));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('brand_name', 'text', array(
            'name' => 'brand_name',
            'label' => $helper->__('Name'),
            'title' => $helper->__('Name'),
            'required' => true,
        ));
        $fieldset->addField('url_key', 'text', array(
            'name' => 'url_key',
            'label' => $helper->__('Url Key'),
            'title' => $helper->__('Url Key'),
            'class' => 'validate-identifier',
            'required' => true,
        ));
        $fieldset->addField('store_id', 'multiselect', array(
            'name' => 'store_id[]',
            'label' => $helper->__('Store'),
            'title' => $helper->__('Store'),
            'required' => true,
            'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
        ));
        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => $helper->__('Description'),
            'title' => $helper->__('Description'),
            'wysiwyg' => true,
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'required' => false,
        ));
        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => $helper->__('Sort Order'),
            'title' => $helper->__('Sort Order'),
            'wysiwyg' => false,
            'required' => false,
        ));
        $fieldset->addField('logo', 'image', array(
            'name' => 'logo',
            'label' => $helper->__('Logo'),
            'title' => $helper->__('Logo'),
            'required' => false
        ));
         /*$fieldset->addField('is_home_page', 'select', array(
            'name' => 'is_home_page',
            'label' => $helper->__('Enable on home-page'),
            'title' => $helper->__('Enable on home-page'),            
            'values' => array(
                array(
                    'value' => '0',
                    'label' => $helper->__('No'),
                ),
                array(
                    'value' => '1',
                    'label' => $helper->__('Yes'),
                ),
            ),
        ));*/
        /* ----Custom fields added---- */
        /*$fieldset->addField('brand_address', 'text', array(
            'name' => 'brand_address',
            'label' => $helper->__('Address'),
            'title' => $helper->__('Address'),
            'required' => true,
        ));
        $fieldset->addField('brand_email', 'text', array(
            'name' => 'brand_email',
            'label' => $helper->__('Email'),
            'title' => $helper->__('Email'),
            'class' => 'required-entry validate-email',
            'required' => true,
        ));
        $fieldset->addField('brand_contact', 'text', array(
            'name' => 'brand_contact',
            'label' => $helper->__('Contact Number'),
            'title' => $helper->__('Contact Number'),
            'class' => 'validate-digits',
            'required' => true,
        ));
        $fieldset->addField('brand_timings', 'text', array(
            'name' => 'brand_timings',
            'label' => $helper->__('Opening Timings'),
            'title' => $helper->__('Opening Timings'),
            'required' => true,
        ));*/
        $fieldset->addField('status', 'select', array(
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => '0',
                    'label' => $helper->__('Disable'),
                ),
                array(
                    'value' => '1',
                    'label' => $helper->__('Enable'),
                ),
            ),
            'required' => true
        ));
        
        if (Mage::getSingleton('adminhtml/session')->getShopbrandData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getShopbrandData());
            Mage::getSingleton('adminhtml/session')->setShopbrandData(null);
        } elseif (Mage::registry('sparx_shopbrand')) {
            $form->setValues(Mage::registry('sparx_shopbrand')->getData());
        }
        return parent::_prepareForm();
    }

}

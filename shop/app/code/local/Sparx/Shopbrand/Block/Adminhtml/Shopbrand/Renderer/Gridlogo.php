<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Adminhtml_Shopbrand_Renderer_Gridlogo extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected function _getValue(Varien_Object $row) {
        $shopbrand_id = $row->getData("id");
        $html = '';
        try {
            $img = Mage::getModel('shopbrand/shopbrand')->load($shopbrand_id)->getLogo();
            $x = 50;
            $y = 50;
            $color = "255,255,255";
            if ($img != "") {
                $imgPath = Mage::helper('shopbrand')->getResizedUrl($img, $x, $y, $color);
            } else {
                $imgPath = Mage::getBaseUrl('media') . 'shopbrand/no-image/no-image-icon.jpg';
            }
            $html = "<img src='" . $imgPath . "' border='0'/>";
        } catch (Exception $e) {
            
        }
        return $html;
    }

}
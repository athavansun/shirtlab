<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Shopbrand extends Mage_Core_Block_Template {

    public function _prepareLayout() {

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('shopbrand')->__('Home'),
                'title' => Mage::helper('shopbrand')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));
            $breadcrumbsBlock->addCrumb('shopbrand', array(
                'label' => Mage::helper('shopbrand')->__('Brands')
            ));
        }


        if ($head = $this->getLayout()->getBlock('head')) {
            $head->setTitle(Mage::helper('shopbrand')->getListPageTitle());
            $head->setDescription(Mage::helper('shopbrand')->getListMetaDescription());
            $head->setKeywords(Mage::helper('shopbrand')->getListMetaKeywords());
        }

        return parent::_prepareLayout();
    }

    public function getItems() {
        if (!$this->hasData('items')) {
            $this->setData('items', Mage::registry('items'));
        }
        return $this->getData('items');
    }

}
<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Block_Product_View extends Mage_Core_Block_Template {

    const XML_SHOPBRAND_SHOPBRAND_GENERAL = 'shopbrand/general/attrcode';
    const DEFAULT_PAGE_LIMIT = 30;

    public function __construct() {
        parent::__construct();
        $this->_prepareCollection();
    }

    protected function _prepareLayout() {

        $pagetitle = Mage::helper('shopbrand')->getBrandListPageTitle();
        if ($head = $this->getLayout()->getBlock('head')) {
            $head->setTitle($pagetitle);
        }
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->setTemplate(Mage::getStoreConfig('shopbrand/shopbrand/layout'));
        }

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('shopbrand')->__('Home'),
                'title' => Mage::helper('shopbrand')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));

            $breadcrumbsBlock->addCrumb('brands', array(
                'label' => Mage::helper('shopbrand')->__('Brands')
            ));
        }
        parent::_prepareLayout();
        $defaultlimit = self::DEFAULT_PAGE_LIMIT;
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager = $pager->setTemplate('shopbrand/page/brandpager.phtml');
        $limitvalue = Mage::helper('shopbrand')->getBrandListItemPerPage($defaultlimit);
        $pager->setAvailableLimit($limitvalue);
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }

    protected function _prepareCollection() {
        $brandattr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $brandattrstatus = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
        $collection = Mage::getModel('shopbrand/shopbrand')->getCollection()
                     ->addFieldToFilter('status', 1)
                     ->addFieldToFilter('brand_attribute',$brandattr)
                     ->addFieldToFilter('attribute_status',$brandattrstatus);
        $brandstoreTbl = Mage::getSingleton('core/resource')->getTableName('shopbrand/shopbrandstore');
        $currentstore = Mage::app()->getStore()->getId();
        $stores = array(
            $currentstore,
            Mage_Core_Model_App::ADMIN_STORE_ID
        );
        $collection->getSelect()->join(
                        array('shopbrandstore' => $brandstoreTbl), 'main_table.id = shopbrandstore.brand_id', array('store'=>'store_id')
                )->where('shopbrandstore.store_id in (?) ', $stores)
                 ->group('main_table.id');
//       echo (string)$collection->getSelect();
        $param = $this->getRequest()->getParam('start');
        if (isset($param) && $param != '') {
            $collection->addFieldToFilter('brand_name', array('like' => $param . "%"));
        }
        $this->setCollection($collection);
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

}

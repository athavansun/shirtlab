<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */ 

class Sparx_Shopbrand_Block_Product_List extends Mage_Catalog_Block_Product_List //Mage_Catalog_Block_Product_Abstract {
{
   
    protected $_productCollection;
    protected $_sort_by;

    public function _prepareLayout() {

        //Get Brand
        $brandid = $this->getCurrentbrandId();
        $model   = Mage::getModel('shopbrand/shopbrand')->load($brandid);
        if ($head = $this->getLayout()->getBlock('head')) {
                $head->setTitle($model->getBrandName());
            }
        if ($root = $this->getLayout()->getBlock('root')) {
                $root->setTemplate(Mage::getStoreConfig('shopbrand/brandview/layout'));
            }

        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('shopbrand')->__('Home'),
                'title' => Mage::helper('shopbrand')->__('Go to Home Page'),
                'link' => Mage::getBaseUrl()
            ));

            $breadcrumbsBlock->addCrumb('brands', array(
                'label' => Mage::helper('shopbrand')->__('Brands'),
                'title' => Mage::helper('shopbrand')->__('Brands'),
                'link' => Mage::helper('shopbrand')->getBrandListUrl()
            ));

            $breadcrumbsBlock->addCrumb('view', array(
                'label' => Mage::helper('shopbrand')->__($model->getBrandName())
            ));
        }
        $columnval = 3;
        if($root){
           $columnval = $this->getColumnsValue($root->getTemplate());
        }
        $this->setColumnCount($columnval);
        parent::_prepareLayout();
    }

    protected function _beforeToHtml() {
        parent::_beforeToHtml();
        $toolbar = $this->getToolbarBlock();
        $toolbar->removeOrderFromAvailableOrders('position');
        return $this;
    }
    
    /**
     * Retrieve loaded product collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection() {

        if (is_null($this->_productCollection)) {
            $shopBrandId = $this->getCurrentbrandId();
            $attrCode = Mage::helper('shopbrand')->getBrandAttributeCode();
            $shopBrandModel = Mage::getModel('shopbrand/shopbrand');
            $shopBrandModel->load($shopBrandId);
//            if ($shopBrandId && $shopBrandModel->getId()) {
                $attributes = Mage::getSingleton('catalog/config')
                        ->getProductAttributes();
                $collection = Mage::getResourceModel('catalog/product_collection')
//                             ->addAttributeToSelect('*')
                        ->addAttributeToSelect($attributes)
                        ->addMinimalPrice()
                        ->addFinalPrice()
                        ->addTaxPercents()
                        ->addAttributeToFilter($attrCode, $shopBrandModel->getOptionId())
                        ->addStoreFilter();
                    $attrsets = Mage::helper('shopbrand')->getAttributeSetIds();
                    $collection->addAttributeToFilter('attribute_set_id',array('in'=> $attrsets));
                Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
                Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
                $this->_productCollection = $collection;
//            }
        }
        return $this->_productCollection;
    }

    /* 
     * return current brand id from request
     */
    public function getCurrentbrandId() {
         $shopBrandId = $this->getRequest()->getParam('id');
//         $shopBrandId = 5;
         return  $shopBrandId;
    }
    
    protected function _toHtml() {
        if ($this->_getProductCollection()->count()) {
            return parent::_toHtml();
        }
        return parent::_toHtml();
    }

    /* Set the column count based on the layout template used */
    public function getColumnsValue($pageLayoutRootTemplate) {
        switch ($pageLayoutRootTemplate) {
            case 'page/empty.phtml':
                $_columnCount = 6;
                break;
            
            case 'page/1column.phtml':
                $_columnCount = 5;
                break;

            case 'page/2columns-left.phtml':
                $_columnCount = 4;
                break;

            case 'page/2columns-right.phtml':
                $_columnCount = 4;
                break;

            case 'page/3columns.phtml':
                $_columnCount = 3;
                break;

            default:
                $_columnCount = 3;
                break;
        }
     return $_columnCount;
    }
    
    public function getCrumbtitle() {

        $shopBrandId = $this->getCurrentbrandId();
        $model = Mage::getModel('shopbrand/shopbrand')->load($shopBrandId);
        $title = $model->getBrandName();
        return $title;
    }
    

}
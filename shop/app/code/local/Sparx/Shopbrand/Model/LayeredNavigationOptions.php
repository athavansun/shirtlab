<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_LayeredNavigationOptions{
     public function toOptionArray() {
        return array(
            array('value' => 'dropdown', 'label' => Mage::helper('adminhtml')->__('Drop Down')),
            array('value' => 'list', 'label' => Mage::helper('adminhtml')->__('List')),
        );
    }
}
?>

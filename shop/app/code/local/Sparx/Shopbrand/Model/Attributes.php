<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Model_Attributes {

    protected $_options = array();

    public function toOptionArray() {
        $collection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addVisibleFilter()
                ->addFieldToFilter('main_table.frontend_input', 'select');
        foreach ($collection as $col) {
            $this->_options[] = array(
                'label' => $col->getFrontendLabel(),
                'value' => $col->getAttributeCode()
            );
        }
        return $this->_options;
    }

}

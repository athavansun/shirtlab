<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Model_Status extends Varien_Object
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('shopbrand')->__('Enable'),
            self::STATUS_DISABLED   => Mage::helper('shopbrand')->__('Disable')
        );
    }
}
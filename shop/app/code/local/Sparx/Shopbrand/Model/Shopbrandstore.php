<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Model_Shopbrandstore extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('shopbrand/shopbrandstore');
    }
}
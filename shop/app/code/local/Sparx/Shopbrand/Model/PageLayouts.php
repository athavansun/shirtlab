<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_PageLayouts{
    protected $_options;
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $layouts = array();
			$node = Mage::getConfig()->getNode('global/cms/layouts') ? Mage::getConfig()->getNode('global/cms/layouts') : Mage::getConfig()->getNode('global/page/layouts');
			foreach ($node->children() as $layoutConfig) {
                            if(strtolower((string)$layoutConfig->label) == 'empty'){
				$this->_options[] = array(
				   'value'=>'page/empty.phtml',
				   'label'=>(string)$layoutConfig->label
				);
                            } else{
				$this->_options[] = array(
				   'value'=>(string)$layoutConfig->template,
				   'label'=>(string)$layoutConfig->label
				);
			  }
			}
			
		}
        return $this->_options;
    }
}
?>

<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_Observer extends Mage_Core_Model_Abstract {

    private $attributeId = '';
    private $attributeCode = '';
    private $options = array();
    private $sortOrder = array();
    private $optionValues = array();
    private $deleteValues = array();
    private $deleteOptionIds = array();
    private $helper;
    private $empty = array();

    /*
     * Event for Attribute Save
     * 
     */

    public function checkForShopBrandRequest($observer) {
        $this->helper = Mage::helper('shopbrand');
        $this->attributeId = $observer->getEvent()->getAttribute()->getAttributeId();
        $this->attributeCode = $observer->getEvent()->getAttribute()->getAttributeCode();
        $this->options = $observer->getEvent()->getAttribute()->getOption();
        $this->deleteValues = isset($this->options['delete']) ? $this->options['delete'] : $this->empty;
        $this->optionValues = isset($this->options['value']) ? $this->options['value'] : $this->empty;
        $this->sortOrder = isset($this->options['order']) ? $this->options['order'] : $this->empty;
        if (count($this->deleteValues) > 0) {
            foreach ($this->deleteValues as $key => $deleteValue) {
                if ($deleteValue == 1) {
                    $this->deleteOptionIds[] = $key;
                    $this->helper->deleteShopBrandRecord($key);
                }
            }
        }
        if ($this->attributeCode != $this->helper->getBrandAttributeCode()) {
            return $this;
        }
        if (count($this->optionValues) > 0) {
            $brandoptionsids = $this->helper->getBrandoptionsArray();
            foreach ($this->optionValues as $key => $optionValue) {
                $optionName = $optionValue[0];
                if (isset($this->sortOrder[$key])) {
                    $optionOrder = $this->sortOrder[$key];
                }
                $new = false;
                if (!is_int($key)) {
                    $key = $this->helper->getOptionIdByName($optionName);
                    $new = true;
                }
                if (!in_array($key, $this->deleteOptionIds) && ((array_key_exists('optionid', $brandoptionsids) && in_array($key, $brandoptionsids['optionid'])) || $new)) {
                    $this->helper->saveShopBrandRecord($key, $this->attributeCode, $optionName, $optionOrder, $new);
                }
            }
        }
        return $this;
    }

    /*
     * Event for Enable Config Brand Attribute on system config section save 
     * 
     */

    public function systemConfigChangedSection($observer) {

        $brandattribute = Mage::getStoreConfig('shopbrand/general/attrcode');
        $connectionWrite = Mage::getSingleton('core/resource')->getConnection('core_write');
        $table = "sparx_shopbrand"; // table name
        $tableName = Mage::getSingleton("core/resource")->getTableName($table);
        $connectionWrite->beginTransaction();
        $data = array();
        $data['attribute_status'] = '1';
        $where = $connectionWrite->quoteInto('brand_attribute =?', $brandattribute);
        $connectionWrite->update($tableName, $data, $where);
        $data['attribute_status'] = '0';
        $where = $connectionWrite->quoteInto('brand_attribute !=?', $brandattribute);
        $connectionWrite->update($tableName, $data, $where);
        $connectionWrite->commit();
    }

    public function addToTopmenu(Varien_Event_Observer $observer) {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        $brandTitle=Mage::helper('shopbrand')->getBrandMainHeading();
        
        $url= Mage::getUrl('brands');
        if(Mage::helper('shopbrand')->getBrandSeoUrlSuffix()){
            $url=Mage::getUrl('brands'.Mage::helper('shopbrand')->getBrandSeoUrlSuffix());
        }
        $node = new Varien_Data_Tree_Node(array(
            'name' => $brandTitle,
            'id' => 'brands',
            'url' => $url, 
                ), 'id', $tree, $menu);

        $menu->addChild($node);

        // Children menu items
        $brandattr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $brandattrstatus = Sparx_Shopbrand_Helper_Data::ATTR_STATUS_ENABLED;
        $collection = Mage::getModel('shopbrand/shopbrand')->getCollection()
                      ->addFieldToFilter('status', 1)
                      ->addFieldToFilter('brand_attribute',$brandattr)
                      ->addFieldToFilter('attribute_status',$brandattrstatus);
        $brandstoreTbl = Mage::getSingleton('core/resource')->getTableName('shopbrand/shopbrandstore');
        $currentstore = Mage::app()->getStore()->getId();
        $stores = array(
            $currentstore,
            Mage_Core_Model_App::ADMIN_STORE_ID
        );
        $collection->getSelect()->join(
                array('shopbrandstore' => $brandstoreTbl), 'main_table.id = shopbrandstore.brand_id', array('store'=>'store_id')
        )->where('shopbrandstore.store_id in (?) ', $stores)
         ->group('main_table.id');
       
        if($collection->getSize()){
        foreach ($collection as $brand) {
            $tree = $node->getTree();
             $url= Mage::getUrl($brand->getUrlKey());
        if(Mage::helper('shopbrand')->getBrandSeoUrlSuffix()){
            $url= Mage::getUrl($brand->getUrlKey().Mage::helper('shopbrand')->getBrandSeoUrlSuffix());
        }
            $data = array(
                'name' => $brand->getBrandName(),
                'id' => 'category-node-100' . $brand->getId(),
                'url' => $url,
            );

            $subNode = new Varien_Data_Tree_Node($data, 'id', $tree, $node);
            $node->addChild($subNode);
        }
    }
    }

}

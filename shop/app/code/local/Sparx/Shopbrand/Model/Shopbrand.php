<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_Shopbrand extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('shopbrand/shopbrand');
    }

    /*
     * Delete Attribute Option
     * 
     * @param Int optionId
     * @return Null
     */

    public function deleteAttributeOption($optionId) {
        $options = array();
        $options['delete'][$optionId] = true;
        $options['value'][$optionId] = true;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($options);
    }

    /*
     * Add Attribute Option
     * 
     * @param String optionName, 
     * @param Int sortOrder, 
     * @return Int OptionId
     */

    public function addAttributeOption($optionName, $sortOrder) {
        $configValue = Mage::helper('shopbrand')->getBrandAttributeCode();
        $arg_value = $optionName;
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $configValue);
        $attr_id = $attr->getAttributeId();
        if(!$this->attributeValueExists($configValue, $arg_value)){
        $option['attribute_id'] = $attr_id;
        $option['value'][$arg_value][0] = $arg_value;
        $option['order'][$arg_value] = $sortOrder;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
        $lastId = $setup->getConnection()->lastInsertId();
        $version = substr(Mage::getVersion(), 0,3);
        if($version == '1.4' || $version == '1.5'){
           $attroptcoll = Mage::getModel('eav/entity_attribute_option')
            ->getCollection()
            ->setStoreFilter()
            ->addFieldToFilter('store_value.value_id',array('eq'=>$lastId))
            ->getFirstItem();
        }else{
           $attroptcoll = Mage::getModel('eav/entity_attribute_option')
            ->getCollection()
            ->setStoreFilter()
            ->addFieldToFilter('tsv.value_id',array('eq'=>$lastId))
            ->getFirstItem();
        }
 //      echo (string)$attroptcoll->getselect();exit;
        $optionId = $attroptcoll->getData('option_id');
        return $optionId;
        }
        return $this->attributeValueExists($configValue, $arg_value);
    }

    /*
     * Check exist of Attribute Option
     * 
     * @param String Attribute Code, 
     * @param Option Name, 
     * @return Int OptionId
     */
    function attributeValueExists($arg_attribute, $arg_value) {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');

        $attribute_code = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute = $attribute_model->load($attribute_code);

        $attribute_table = $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                return $option['value'];
            }
        }
        return false;
    }

    /*
     * Update Attribute Option
     * 
     * @param Int OptionId, 
     * @param String OptionNewName Default is 0, 
     * @param Int NewSortOrder, 
     * @return Bool
     */

    public function updateAttributeOption($optionId, $OptionNewName, $NewSortOrder = 0) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr_id = Mage::getModel('eav/entity_attribute_option')->load($optionId)->getAttributeId();
//        $configValue = Mage::helper('shopbrand')->getBrandAttributeCode();
//        $attr = $attr_model->loadByCode('catalog_product', $configValue);
//        $attr_id = $attr->getAttributeId();
        $attr_model->load($attr_id);
        $data = array();
        $values = array(
            $optionId => array(
                0 => $OptionNewName
            )
        );
        $data['option']['value'] = $values;
        $data['option']['order'] = array($optionId => $NewSortOrder);
        $attr_model->addData($data);
        try {
            $attr_model->save();
            return $attr_model->getAttributeCode();
        } catch (Exception $e) {
            Mage::log($e);
            return;
        }
    }

    /*
     * Set Attribute To Products
     * 
     * @param Int OptionId, 
     * @param Array Product Ids, 
     * @return Null
     */

    public function setAttributeToProducts($optionId, $productIds) {
        $attrCode = Mage::helper('shopbrand')->getBrandAttributeCode();
        $productModel = Mage::getModel('catalog/product');
        $productsCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter($attrCode, $optionId);
        foreach ($productsCollection as $product):
            $product = $productModel->load($product->getEntityId());
            $product->setData($attrCode, null);
            $product->save();
        endforeach;
        if (!empty($productIds[0]) && $productIds[0] != '') {
            foreach ($productIds as $entityId):
                $product = $productModel->load($entityId);
                $product->setData($attrCode, $optionId);
                $product->save();
            endforeach;
        }
    }

    /*
     * Set Attribute To Products without loading products 
     * 
     * @param Int OptionId, 
     * @param Array Product Ids, 
     * @return Null
     */
    public function setSqlProducts($optionId, $productIds) {
        $attrCode = Mage::helper('shopbrand')->getBrandAttributeCode();
        if(count($productIds) && $attrCode){
            $attrData = array($attrCode => $optionId);
            $unselattrData = array($attrCode => '');
            $storeId = 0;
            $oldselectedids = $this->getSelectedProducts();
            $unselprodids = array_diff($oldselectedids, $productIds);
         Mage::getSingleton('catalog/product_action') ->updateAttributes($productIds, $attrData, $storeId);
         if(count($unselprodids)){
           Mage::getSingleton('catalog/product_action') ->updateAttributes($unselprodids, $unselattrData, $storeId);
            }
        }
    }

    /*
     * Remove already Selected products for Current brand
     * 
     * @param array product ids
     * @return Null
     */
     public function removeSelectedProducts($productIds) {
        $attrCode = Mage::helper('shopbrand')->getBrandAttributeCode();
        if(count($productIds) && $attrCode){
         $attrData = array($attrCode => '');
         $storeId = 0;
         Mage::getSingleton('catalog/product_action') ->updateAttributes($productIds, $attrData, $storeId);
        }
    }
    
    /**
     * Retrieve selected related products
     *
     * @return array
     */
    public function getSelectedProducts() {
        $entityIds = array();
        $shopBrandId = Mage::app()->getRequest()->getParam('id');
        $attrCode = Mage::helper('shopbrand')->getBrandAttributeCode();
        $shopBrandModel = Mage::getModel('shopbrand/shopbrand');
        $shopBrandModel->load($shopBrandId);
        if($shopBrandId && $shopBrandModel->getId()){
            $productsCollection = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter($attrCode, $shopBrandModel->getOptionId());
            foreach ($productsCollection as $product):
                $entityIds[] = $product->getEntityId();
            endforeach;
        }
        return $entityIds;
    }

     /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param   string $identifier
     * @param   int $storeId
     * @return  int
     */
    public function checkBrandIdentifier($identifier) {
        return $this->_getResource()->checkBrandIdentifier($identifier);
        }
        
    /**
     * Check if brand Url Key exist for specific store
     * return bool
     * @param form PostData
     */  
      public function checkUrlKey($postData){
          return $this->_getResource()->checkUrlKey($postData);
      }
    /**
     * Check if brand Option exist for specific store
     * return bool
     * @param form PostData
     */
    public function checkBrandOption($postData,$optionid) {
        return $this->_getResource()->checkBrandOption($postData,$optionid);
    }
  
   /*
    * @ get Shopbrand attribute option Object filter by option id.
    */
    public function getBrandAttrCollectoin() {
        $brandattrcode =Mage::helper('shopbrand')->getBrandAttributeCode();
        $brandcoll = Mage::getModel('shopbrand/shopbrand')->getCollection()
                ->addFieldToFilter('brand_attribute', $brandattrcode);
        return $brandcoll;
    }
    
}

<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_Mysql4_Shopbrand_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    protected function _construct() {
        $this->_init('shopbrand/shopbrand');
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Sparx_Shopbrand_Model_Resource_Shopbrand_Collection
     */
    public function addStoreFilter($store, $withAdmin = true) {
         if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }
        
        $version = substr(Mage::getVersion(),0,5);
        if($version < '1.4.2'){
            $this->getSelect()->join(
                    array('store_table' => $this->getTable('shopbrand/shopbrandstore')), 'main_table.id = store_table.brand_id', array()
            )->where('store_table.store_id in (?)', array(0, $store))
            ->group('main_table.id');
        }else{
           if (!is_array($store)) {
             $store = array($store);
           }
           if ($withAdmin) {
             $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
           }
           $this->addFilter('store', array('in' => $store), 'public');   
        }
        return $this;
    }

    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore() {
        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                    array('store_table' => $this->getTable('shopbrand/shopbrandstore')), 'main_table.id = store_table.brand_id', array()
            )->group('main_table.id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }

    
    /**
     * collection Count doesn't work with group by columns keep the group by
     */
    public function getSelectCountSql()
    {   
        $this->_renderFilters();
        $countSelect = clone $this->getSelect();
        $countSelect->reset(Zend_Db_Select::ORDER);
        $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $countSelect->reset(Zend_Db_Select::COLUMNS);

        // Count doesn't work with group by columns keep the group by 
        if(count($this->getSelect()->getPart(Zend_Db_Select::GROUP)) > 0) {
            $countSelect->reset(Zend_Db_Select::GROUP);
            $countSelect->distinct(true);
            $group = $this->getSelect()->getPart(Zend_Db_Select::GROUP);
            $countSelect->columns("COUNT(DISTINCT ".implode(", ", $group).")");
        } else {
            $countSelect->columns('COUNT(*)');
}
        return $countSelect;
    }

}
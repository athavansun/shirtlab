<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Model_Mysql4_Shopbrand extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init('shopbrand/shopbrand', 'id');
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param Mage_Cms_Model_Block $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object) {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {
            $stores = array(
                (int) $object->getStoreId(),
                Mage_Core_Model_App::ADMIN_STORE_ID,
            );

            $select->join(
                            array('shopbrandstore' => $this->getTable('shopbrand/shopbrandstore')), $this->getMainTable() . '.id = shopbrandstore.brand_id', array('store_id')
                    )->where('status = ?', 1)
                    ->where('shopbrandstore.store_id in (?) ', $stores)
                    ->order('store_id DESC')
                    ->limit(1);
        }
        return $select;
    }
    
    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Sparx_Shopbrand_Model_Resource_Shopbrand
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object) {

        $select = $this->_getReadAdapter()->select()
                ->from('sparx_shopbrand_store')
                ->where('brand_id = ?', $object->getId());

        if ($data = $this->_getReadAdapter()->fetchAll($select)) {
            $storesArray = array();
            foreach ($data as $row) {
                $storesArray[] = $row['store_id'];
            }
            $object->setData('store_id', $storesArray);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param   string $identifier
     * @param   int $storeId
     * @return  int
     */
    public function checkBrandIdentifier($identifier) {
        $brandAttr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $select = $this->_getReadAdapter()->select()->from(array('main_table' => $this->getMainTable()), 'id')
                ->where('main_table.url_key = ?', array($identifier))
                ->where('main_table.brand_attribute = ?', $brandAttr)
                ->where('main_table.status = 1')
                ->order('main_table.id DESC');

        return $this->_getReadAdapter()->fetchOne($select);
    }

    /**
     * Check for exist of urlkey of brand before saving brand.
     */
    public function checkUrlKey($data) {
        if (!$this->getIsUniqueUrlKey($data)) {
            Mage::throwException(Mage::helper('shopbrand')->__('A urlkey with the same properties already exists.'));
        }
        if (is_numeric($data['url_key'])) {
            Mage::throwException(Mage::helper('shopbrand')->__('A urlkey can not consist only numeric value'));
        }
        return $this;
    }
    /**
     * Check for exist of option of brand before saving brand.
     */
    public function checkBrandOption($data,$optionid) {
        if (!$this->getIsUniqueOptionId($data,$optionid)) {
            Mage::throwException(Mage::helper('shopbrand')->__('Brand option is already exist.'));
        }
        return $this;
    }

    /**
     * Check for unique of urlkey of brand.
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    public function getIsUniqueUrlKey($data) {
        $brandAttr = Mage::helper('shopbrand')->getBrandAttributeCode();
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where($this->getMainTable() . '.brand_attribute = ?', $brandAttr)
                ->where($this->getMainTable() . '.url_key = ?', $data['url_key']);
        if (isset($data['id']) && !empty($data['id'])) {
            $select->where($this->getMainTable() . '.id <> ?', $data['id']);
        }
        if ($this->_getReadAdapter()->fetchRow($select)) {
            return false;
        }
        return true;
    }
    
    /**
     * Check for unique of option of brand.
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    public function getIsUniqueOptionId($data,$optionid) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where($this->getMainTable() . '.option_id = ?', $optionid);
        if (isset($data['id']) && !empty($data['id'])) {
            $select->where($this->getMainTable() . '.id <> ?', $data['id']);
        }
        if ($this->_getReadAdapter()->fetchRow($select)) {
            return false;
        }
        return true;
    }

}
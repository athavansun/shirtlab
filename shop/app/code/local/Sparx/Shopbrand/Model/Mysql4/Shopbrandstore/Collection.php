<?php
/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */

class Sparx_Shopbrand_Model_Mysql4_Shopbrandstore_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    protected function _construct() {
        $this->_init('shopbrand/shopbrandstore');
    }

}
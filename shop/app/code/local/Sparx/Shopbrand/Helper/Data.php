<?php

/**
 * Shopbrand extension
 * @category   Sparx
 * @package    Shopbrand
 * @author     Sparx
 */
class Sparx_Shopbrand_Helper_Data extends Mage_Core_Helper_Abstract {

    public $model;
    public $storeModel;
    public $data = array();
    private $flag = NULL;

    const STATUS_ENABLED = 1;
    const ALL_STORE_VIEWS = 0;
    const ATTR_STATUS_ENABLED = 1;
    const DEFAULT_SEO_URL_SUFFIX = '.html';
    const XML_PATH_SHOPBRAND_PRODUCTPAGE_ENABLE = 'shopbrand/productviewpage/enable';
    const XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDHEADING = 'shopbrand/productviewpage/brandheading';
    const XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDLOGO_WIDTH = 'shopbrand/productviewpage/brandlogowidth';
    const XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDLOGO_HEIGHT = 'shopbrand/productviewpage/brandlogoheight';
    const XML_SHOPBRAND_SHOPBRAND_GENERAL = 'shopbrand/general/attrcode';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_ITEMPERPAGE = 'shopbrand/shopbrand/items_per_page';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_LOGOWIDTH = 'shopbrand/shopbrand/logo_width';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_LOGOHEIGHT = 'shopbrand/shopbrand/logo_height';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_SHOWLOGO = 'shopbrand/shopbrand/show_shopbrand_logo';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_PAGETITLE = 'shopbrand/shopbrand/page_title';
    const XML_SHOPBRAND_SHOPBRAND_BRANDLIST_MAINHEADING = 'shopbrand/shopbrand/main_heading';
    const XML_SHOPBRAND_SHOPBRAND_LEFTNAVIGATION_NOOFBRANDS = 'shopbrand/layerednav/numberofbrands';
    const XML_PATH_SHOPBRAND_BRANDVIEW_LOGO_WIDTH = 'shopbrand/brandview/logo_width';
    const XML_PATH_SHOPBRAND_BRANDVIEW_LOGO_HEIGHT = 'shopbrand/brandview/logo_height';
    const XML_PATH_BRAND_LIST_IDENTIFIER = 'shopbrand/shopbrand/seourl';
    const XML_PATH_BRAND_SEO_URL_SUFFIX = 'shopbrand/brandseo/url';

    /**
     * Returns the resized Image URL
     *
     * @param string $imgUrl - This is relative to the the media folder (custom/module/images/example.jpg)
     * @param int $x Width Default is 100
     * @param int $y Height Default is 100
     */
    public function getResizedUrl($imgUrl, $x = 100, $y = 100, $color = NULL) {
        $imgName = $this->splitImageValue($imgUrl, "name");
        $imgPath = $this->splitImageValue($imgUrl, "path");
        /**
         * Path with Directory Seperator
         */
        $imgPath = str_replace("/", DS, $imgPath);

        /**
         * Absolute full path of Image
         */
        $imgPathFull = Mage::getBaseDir("media") . DS . $imgPath . DS . $imgName;

        $width = $x;
        $height = $y;
        /**
         * Resize folder is widthXheight
         */
        $resizeFolder = $width . "X" . $height;

        /**
         * Image resized path will then be
         */
        $imageResizedPath = Mage::getBaseDir("media") . DS . $imgPath . DS . $resizeFolder . DS . $imgName;

        /**
         * First check in cache i.e image resized path
         * If not in cache then create image of the width=X and height = Y
         */
        $colorArray = array();
        if ($color == NULL) {
            $color = '255,255,255';
        }
        $colorArray = explode(",", $color);
        if (!file_exists($imageResizedPath) && file_exists($imgPathFull)) :

            $imageObj = new Varien_Image($imgPathFull);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(true);
            $imageObj->keeptransparency(FALSE);
            $imageObj->backgroundColor(array(intval($colorArray[0]), intval($colorArray[1]), intval($colorArray[2])));
            $imageObj->resize($width, $height);
            $imageObj->save($imageResizedPath);
        endif;

        /**
         * Else image is in cache replace the Image Path with / for http path.
         */
        $imgUrl = str_replace(DS, "/", $imgPath);

        /**
         * Return full http path of the image
         */
        return Mage::getBaseUrl("media") . $imgUrl . "/" . $resizeFolder . "/" . $imgName;
    }

    /**
     * Splits images Path and Name
     *
     * @param string $imageValue
     * @param string $attr
     * @return string
     */
    public function splitImageValue($imageValue, $attr = "name") {
        $imArray = explode("/", $imageValue);
        if (isset($imArray[count($imArray) - 1]))
            $name = $imArray[count($imArray) - 1];
        if ($attr == "path") {
            if (isset($imArray[count($imArray) - 2]))
                return $imArray[count($imArray) - 2];
        } else
            return $name;
    }

    /*
     * Delete Image
     * 
     * @param string $imageValue
     * @return Bool
     */

    public function deleteLogoImage($imageValue) {
        $imageName = $this->splitImageValue($imageValue);
        $imagePath = $this->splitImageValue($imageValue, 'path');
        $imagePath = str_replace("/", DS, $imagePath);
        $imgPathFull = Mage::getBaseDir("media") . DS . $imagePath . DS . $imageName;
        if (is_file($imgPathFull)) {
            if (unlink($imgPathFull)) {
                return true;
            }
        }
    }

    /*
     * Function Returns Option Id
     * 
     * @param string $optionName
     * @return Int optionId
     */

    public function getOptionIdByName($optionName) {
        $configValue = $this->getBrandAttributeCode();
        $productModel = Mage::getModel('catalog/product');
        $attrOpt = $productModel->getResource()->getAttribute($configValue);
        if ($attrOpt->usesSource()) {
            return $attrOpt->getSource()->getOptionId($optionName);
        }
    }

    /*
     * Delete Shop Brand Record
     * 
     * @param Int $optionId
     * @return Null
     */

    public function deleteShopBrandRecord($optionId) {
        if (is_int($optionId)) {
            $this->model = Mage::getModel('shopbrand/shopbrand');
            $this->storeModel = Mage::getModel('shopbrand/shopbrandstore');
            $this->model->load($optionId, 'option_id');
            $shopbrandId = $this->model->getId();
            $logo = $this->model->getLogo();
            if (!empty($shopbrandId)) {
                $this->model->delete();
                if (!empty($logo))
                    $this->deleteLogoImage($logo);
                $storesCollection = $this->storeModel->getCollection()->addFieldToFilter('brand_id', $shopbrandId);
                foreach ($storesCollection as $storeCollection) {
                    $storeCollection->delete();
                }
                $this->model->unsetData();
            }
        }
    }

    /*
     * Save Shop Brand Record
     * 
     * @param Int optionId
     * @param String optionName
     * @param Int optionOrder Default is 086
     * @return Null
     */

    public function saveShopBrandRecord($optionId, $attributeCode, $optionName, $optionOrder = 0, $new = false) {
        if (!empty($optionId)) {
            $this->model = Mage::getModel('shopbrand/shopbrand');
            $this->storeModel = Mage::getModel('shopbrand/shopbrandstore');
            $brandattrcode = $this->getBrandAttributeCode();
            unset($this->data);
            $this->data['brand_name'] = $optionName;
            $this->data['sort_order'] = $optionOrder;
            $this->data['option_id'] = $optionId;
            $this->data['brand_attribute'] = $brandattrcode;
            if ($attributeCode == $brandattrcode) {
                $this->data['attribute_status'] = self::ATTR_STATUS_ENABLED;
            }
            if ($new) {
                $brandoptionsarr = $this->getBrandoptionsArray();
                $urlKey = Mage::getModel('catalog/product_url')->formatUrlKey(strtolower($optionName));
                $urlKey = $this->getBrandOptionUrlkey($urlKey);
                $this->data['url_key'] = $urlKey; //strtolower(str_replace(' ', '-', $optionName));
                $this->data['description'] = $optionName;
                $this->data['logo'] = '';
                $this->data['status'] = self::STATUS_ENABLED;
            }
            $id = $this->model->load($optionId, 'option_id')->getId();
            if (!empty($id)) {
                $this->data['id'] = $id;
            }
            $this->model->setData($this->data);
            try {
                $this->model->save();
                $this->model->getId();
                unset($this->data);
                $this->storeModel->load($this->model->getId(), 'brand_id');
                $this->flag = $this->storeModel->getId();
                if (empty($this->flag)) {
                    $this->data['brand_id'] = $this->model->getId();
                    $this->data['store_id'] = self::ALL_STORE_VIEWS;
                    $this->storeModel->setData($this->data);
                    $this->storeModel->save();
                }
            } catch (Exception $e) {
                Mage::log($e);
            }
        }
    }

    /*
     * @ returns shop brand Attribute Options array.
     */

    public function getBrandoptionsArray() {
        $brandarr = array();
        $brandcoll = Mage::getModel('shopbrand/shopbrand')->getBrandAttrCollectoin();
        if ($brandcoll->count()) {
            foreach ($brandcoll as $brand) {
                $brandarr['brandname'][$brand->getId()] = $brand->getBrandName();
                $brandarr['optionid'][$brand->getId()] = $brand->getOptionId();
                $brandarr['urlkey'][$brand->getId()] = $brand->getUrlKey();
            }
        }
        return $brandarr;
    }

    /*
     * @ Check for Url Key Existance and return a new urlkey.
     */

    public function getBrandOptionUrlkey($urlKey) {
        $brandoptionsarr = $this->getBrandoptionsArray();
        $brandurlKey = $urlKey;
        $isexist = false;
        if (array_key_exists('urlkey', $brandoptionsarr) && in_array($urlKey, $brandoptionsarr['urlkey'])) {
            $isexist = true;
        }
        if ($isexist) {
            $random_string = chr(rand(97, 122)) . chr(rand(97, 122)) . chr(rand(97, 122));
            $brandurlKey = $brandurlKey . '-' . $random_string;
        }
        return $brandurlKey;
    }

    /*
     * @ returns shop brand configuration status for product view page.
     */

    public function getProductpageBrandEnable() {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOPBRAND_PRODUCTPAGE_ENABLE);
    }

    /*
     * @ returns shop brand configuration status for product view page.
     */

    public function getBrandAttributeCode() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_GENERAL);
    }

    /*
     * @ returns Brand heading for product view page provided from admin
     */

    public function getProductpageBrandHeading() {
        $brandheading = Mage::getStoreConfig(self::XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDHEADING);
        if (!$brandheading || $brandheading == '') {
            $brandheading = "Brand";
        }
        return $brandheading;
    }

    public function getBrandMainHeading() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_MAINHEADING);
    }

    /*
     * @ returns logo height for brand provided from admin
     */

    public function getProductpageBrandLogoHeight() {
        return Mage::getStoreConfig(self::XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDLOGO_HEIGHT);
    }

    /*
     * @ returns logo width for brand povided from admin
     */

    public function getProductpageBrandLogoWidth() {
        return Mage::getStoreConfig(self::XML_PATH_SHOPBRAND_PRODUCTPAGE_BRANDLOGO_WIDTH);
    }

    /*
     * @ returns brand quantity to display per page on product list page
     */

    public function getBrandListItemPerPage($limitvalue) {
        $itemsPerPage = Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_ITEMPERPAGE);
        $itemsValue = array();
        if ($itemsPerPage == 0 || $itemsPerPage == '') {
            $itemsValue[$limitvalue] = $limitvalue;
        } else {
            $itemsValue[$itemsPerPage] = $itemsPerPage;
        }
        return $itemsValue;
    }

    /*
     * @ returns logo width for product list page provided from admin
     */

    public function getBrandListLogoWidth() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_LOGOWIDTH);
    }

    /*
     * @ returns brand logo height for product list page provided from admin
     */

    public function getBrandListLogoHeight() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_LOGOHEIGHT);
    }

    /*
     * @ returns status to display brand logo or name for product list page
     */

    public function getBrandWithLogo() {
        return Mage::getStoreConfigFlag(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_SHOWLOGO);
    }

    /*
     * @ returns brand list page title.
     */

    public function getBrandListPageTitle() {
        $pagetitle = Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_PAGETITLE);
        if ($pagetitle == '') {
            $pagetitle = 'Brands';
        }
        return $pagetitle;
    }

    public function getLeftNavigationNoOfBrands() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_LEFTNAVIGATION_NOOFBRANDS);
    }

    /*
     * @ returns List page title.
     */

    public function getListPageTitle() {
        return Mage::getStoreConfig(self::XML_SHOPBRAND_SHOPBRAND_BRANDLIST_PAGETITLE);
    }

    /*
     * @ returns brand list page Url with suffix.
     */

    public function getBrandListUrl($params = null) {
        //$url = Mage::getUrl() . 'shopbrand/index/index';
        if (is_null($params)) {
            $url = Mage::getUrl('') . $this->getBrandIdentifier() . $this->getBrandSeoUrlSuffix();
        } else {
            $url = Mage::getUrl('') . $this->getBrandIdentifier() . $this->getBrandSeoUrlSuffix() . $params;
        }
        return $url;
    }

    /*
     * @ returns brand page Url with suffix.
     */

    public function getBrandUrl($identifier = null) {
//        $url = Mage::getUrl() . 'shopbrand/index/view?id=' . $id;

        if (is_null($identifier)) {
            $url = Mage::getUrl('') . $this->getBrandIdentifier() . $this->getBrandSeoUrlSuffix();
        } else {
            $url = Mage::getUrl('') . $identifier . $this->getBrandSeoUrlSuffix();
        }
        return $url;
    }

    /*
     * @ returns brand id.
     */

    public function getBrandParamsId() {
        return Mage::app()->getRequest()->getParam('id');
    }

    /*
     * @ returns brand view logo width
     */

    public function getBrandViewLogoWidth() {
        $logowidth = Mage::getStoreConfig(self::XML_PATH_SHOPBRAND_BRANDVIEW_LOGO_WIDTH);
        if ($logowidth == 0 || $logowidth == '') {
            $logowidth = 100;
        }
        return $logowidth;
    }

    /*
     * @ returns brand view logo Height
     */

    public function getBrandViewLogoHeight() {
        $logoheight = Mage::getStoreConfig(self::XML_PATH_SHOPBRAND_BRANDVIEW_LOGO_HEIGHT);
        if ($logoheight == 0 || $logoheight == '') {
            $logoheight = 100;
        }
        return $logoheight;
    }

    /*
     * @ returns brand Default image
     */

    public function getBrandDefaultImage() {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'shopbrand' . DS . 'noimage.jpg';
    }

    /*
     * @ returns Seo url Suffix from admin config 
     */

    public function getBrandSeoUrlSuffix() {
        $SeoSuffix = Mage::getStoreConfig(self::XML_PATH_BRAND_SEO_URL_SUFFIX);
        if ($SeoSuffix == "") {
            $SeoSuffix = self::DEFAULT_SEO_URL_SUFFIX;
        }
        return $SeoSuffix;
    }

    /*
     * @ returns Current product object
     */

    public function getCurrentProduct() {
        $productid = Mage::registry('current_product')->getId();
        $_product = Mage::getModel('catalog/product')->load($productid);
        return $_product;
    }

    /*
     * get Brand Selected by Product
     * return Brand Object
     */

    public function getProductBrand($_product) {
        if (!$_product->getId()) {
            $_product = $this->getCurrentProduct();
        }
        $attrCode = $this->getBrandAttributeCode();
	//$_product = Mage::getModel("catalog/product")->load($_product->getId());
	$attroptid = Mage::getResourceModel('catalog/product')->getAttributeRawValue($_product->getId(), $attrCode, $storeId);
        //$attroptid = $_product->getData($attrCode);
        $model = Mage::getModel('shopbrand/shopbrand')->getCollection()
                ->addFieldToFilter('option_id', $attroptid)
                ->addFieldToFilter('status', 1)
                ->getFirstItem();
        return $model;
    }

    /*
     * @ returns Brand identifier from admin config 
     */

    public function getBrandIdentifier() {
        $brandident = Mage::getStoreConfig(self::XML_PATH_BRAND_LIST_IDENTIFIER);
        if (!$brandident || $brandident == '') {
            $brandident = 'brands';
        }
        return $brandident;
    }

    /*
     * @ returns Attribute sets collection related to brand attribute 
     */

    public function getAttributeSets() {
        $configattrValue = $this->getBrandAttributeCode();
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $configattrValue);
        $attributeId = $attr->getAttributeId();
//        exit;
        $resource = Mage::getSingleton('core/resource');
        $eavattribute = $resource->getTableName('eav/entity_attribute');
        $eaventitytype = $resource->getTableName('eav/entity_type');
        $attrsetcollection = Mage::getModel('eav/entity_attribute_set')
                ->getCollection();
        $attrsetcollection->getSelect()->join(
                array('eav_entity_type' => $eaventitytype), '(eav_entity_type.entity_type_id=main_table.entity_type_id)', array('entity_type_code')
        );
        $attrsetcollection->getSelect()->join(
                array('eav_entity_attribute' => $eavattribute), '(eav_entity_attribute.attribute_set_id=main_table.attribute_set_id)', array('attribute_id')
        );
        $attrsetcollection->addFieldToFilter('eav_entity_attribute.attribute_id', $attributeId);
        $attrsetcollection->addFieldToFilter('eav_entity_type.entity_type_code', Mage_Catalog_Model_Product::ENTITY);
//      echo (string)$attrsetcollection->getselect();    exit;  
        return $attrsetcollection;
    }

    /*
     * @ returns array of attribute set ids
     */

    public function getAttributeSetIds() {
        $attrsetids = array();
        $attrsetcollection = $this->getAttributeSets();
        if ($attrsetcollection->count()) {
            foreach ($attrsetcollection as $attrset) {
                $attrsetids[] = $attrset->getAttributeSetId();
            }
        }
        return $attrsetids;
    }

    public function getSortedImages($content) {
        $imagesArray = json_decode($content, true);
        if (isset($imagesArray) && !empty($imagesArray) && count($imagesArray) > 0) {
            $temp = array();
            foreach ($imagesArray as $key => $image) {
                if ($image['disabled']) {
                    unset($imagesArray[$key]);
                    continue;
                }
                $temp[$key] = $image['position'];
            }
            array_multisort($temp, SORT_ASC, $imagesArray);
        }
        return $imagesArray;
    }

}

?>

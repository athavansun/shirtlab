<?php
/**
 * sparx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the sparx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.sparx.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sparx.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.sparx.com/ for more information
 * or send an email to sales@sparx.com
 *
 * @category   sparx
 * @package    sparx_CustomOptions
 * @copyright  Copyright (c) 2009 sparx (http://www.sparx.com/)
 * @license    http://www.sparx.com/LICENSE-1.0.html
 */

/**
 * Custom Options extension
 *
 * @category   sparx
 * @package    sparx_CustomOptions
 * @author     sparx Dev Team <dev@sparx.com>
 */

class Sparx_CustomOptions_Model_Mysql4_Setup extends Mage_Sales_Model_Mysql4_Setup
{

}
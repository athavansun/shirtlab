<?php
/**
 * Sparx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Sparx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.sparx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.sparx.com/ for more information
 *
 * @category   Sparx
 * @package    Sparx_CustomOptions
 * @copyright  Copyright (c) 2014 Sparx (http://www.sparx.com/)
 * @license    http://www.sparx.com/LICENSE-1.0.html
 */

/**
 * Advanced Product Options extension
 *
 * @category   Sparx
 * @package    Sparx_CustomOptions
 * @author     Sparx Dev Team
 */

class Sparx_CustomOptions_Model_System_Config_Source_Description_Enabled {
    public function toOptionArray() {
        $helper = Mage::helper('customoptions');
        return array(
            array('value' => 0, 'label'=>$helper->__('No')),
            array('value' => 1, 'label'=>$helper->__('Plain Text')),
            array('value' => 2, 'label'=>$helper->__('Tooltip'))
        );
    }

}
<?php
/**
 * Sparx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Sparx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.sparx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.sparx.com/ for more information
 *
 * @category   Sparx
 * @package    Sparx_CustomOptions
 * @copyright  Copyright (c) 2014 Sparx (http://www.sparx.com/)
 * @license    http://www.sparx.com/LICENSE-1.0.html
 */

/**
 * Advanced Product Options extension
 *
 * @category   Sparx
 * @package    Sparx_CustomOptions
 * @author     Sparx Dev Team
 */

if ((string)Mage::getConfig()->getModuleConfig('DerModPro_BCP')->active == 'true'){
    class Sparx_CustomOptions_Model_Catalog_Product_Type_Configurable_Price_Abstract extends DerModPro_BCP_Model_Catalog_Product_Type_Configurable_Price {}
} elseif ((string)Mage::getConfig()->getModuleConfig('Ayasoftware_SimpleProductPricing')->active == 'true'){
    class Sparx_CustomOptions_Model_Catalog_Product_Type_Configurable_Price_Abstract extends Ayasoftware_SimpleProductPricing_Catalog_Model_Product_Type_Configurable_Price {}
} else {
    class Sparx_CustomOptions_Model_Catalog_Product_Type_Configurable_Price_Abstract extends Mage_Catalog_Model_Product_Type_Configurable_Price {}
}
<?php

/**
 * Sparx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Sparx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.sparx.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@sparx.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.sparx.com/ for more information
 * or send an email to sales@sparx.com
 *
 * @category   Sparx
 * @package    Sparx_Adminhtml
 * @copyright  Copyright (c) 2009 Sparx (http://www.sparx.com/)
 * @license    http://www.sparx.com/LICENSE-1.0.html
 */

/**
 * Sparx Adminhtml extension
 *
 * @category   Sparx
 * @package    Sparx_Adminhtml
 * @author     Sparx Dev Team <dev@sparx.com>
 */
class Sparx_Adminhtml_Block_Customoptions_Options_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form(array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('group_id' => (int) $this->getRequest()->getParam('group_id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
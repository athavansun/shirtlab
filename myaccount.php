<?php             
    include_once('includes/function/autoload.php');
    checkSession($_GET['ref']);
?>
<?php include_once('includes/header.php'); ?>
<?php
    $userObj = new User();    
    $genObj = new GeneralFunctions();
    
    $user_id = $_SESSION[USER_ID];
    if(isset($_POST['saveGeneralProfile'])) {
//        echo "<pre>";
//        print_r($_POST);exit;
        include_once("validation_class.php");
        $obj = new validationclass();
                        
        $obj->fnAdd('firma',$_POST['firma'],'req', 'Please enter firm');
        $obj->fnAdd('title',$_POST['title'],'req', 'Please select title.');        
        $obj->fnAdd('fName', $_POST['fName'], 'req', LANG_PLEASE_ENTER_FIRST_NAME);        
        $obj->fnAdd('lName',$_POST['lName'], 'name', LANG_PLEASE_ENTER_LAST_NAME);
        $obj->fnAdd('email',$_POST['email'], 'req', LANG_PLEASE_ENTER_EMAIL_ADDRESS);
        $obj->fnAdd('address1', $_POST['address1'], 'req', LANG_PLEASE_ENTER_ADDRESS);        
        $obj->fnAdd('zip', $_POST['zip'], 'req', LANG_PLEASE_ENTER_ZIP);
        $obj->fnAdd('city', $_POST['city'], 'req', LANG_PLEASE_ENTER_CITY);
        $obj->fnAdd('country', $_POST['country'], 'req', LANG_PLEASE_ENTER_COUNTRY);
        $obj->fnAdd('phone', $_POST['phone'], 'req', 'Please enter Phone No.');
        $obj->fnAdd('mobile', $_POST['mobile'], 'req', 'Please enter Mobile No.');                
        $obj->fnAdd('language', $_POST['language'], 'req', LANG_PLEASE_SELECT_LANGUAGE);        
        if(isset($_POST['password']) && ($_POST['password'] != '')) {
            $obj->fnAdd('password',$_POST['password'],'req', LANG_PLEASE_ENTER_PASSWORD);    
            $obj->fnAdd('password',$_POST['password'], 'min=5', LANG_PLEASE_ENTER_PASSWORD_LENGTH_BETWEEN);
            $obj->fnAdd('password',$_POST['password'], 'max=10', LANG_PLEASE_ENTER_PASSWORD_LENGTH_BETWEEN);
        }        
    
        $arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;
            
        $arr_error['firma'] = $obj->fnGetErr($arr_error['firma']);
        $arr_error['title'] = $obj->fnGetErr($arr_error['title']);
        $arr_error['fName'] = $obj->fnGetErr($arr_error['fName']);
        $arr_error['lName'] = $obj->fnGetErr($arr_error['lName']);
        $arr_error['email'] = $obj->fnGetErr($arr_error['email']);
        $arr_error['address1'] = $obj->fnGetErr($arr_error['address1']);
        $arr_error['zip'] = $obj->fnGetErr($arr_error['zip']);
        $arr_error['city'] = $obj->fnGetErr($arr_error['city']);
        $arr_error['country'] = $obj->fnGetErr($arr_error['country']);
        $arr_error['phone'] = $obj->fnGetErr($arr_error['phone']);
        $arr_error['mobile'] = $obj->fnGetErr($arr_error['mobile']);
        $arr_error['language'] = $obj->fnGetErr($arr_error['language']);
                
        if($str_validate){            
            $_POST = postwithoutspace($_POST);
            $userObj->updateUserInformation($_POST);
            //~ echo '<pre>';
            //~ print_r($_POST);
            //~ echo '</pre>';
        }
    } 
    
    if(isset($_POST['insertAddress'])) {
        include_once("validation_class.php");
        $obj = new validationclass();
                        
        $obj->fnAdd('firma',$_POST['firma'],'req', 'Please enter firm');
        $obj->fnAdd('title',$_POST['title'],'req', 'Please select title.');        
        $obj->fnAdd('fName', $_POST['fName'], 'req', 'Please enter First Name.');        
        $obj->fnAdd('lName',$_POST['lName'], 'name', 'Please enter valid Last Name.');
        $obj->fnAdd('email',$_POST['email'], 'req', 'Please enter Email Address.');
        $obj->fnAdd('address1', $_POST['address1'], 'req', 'Please enter your address.');        
        $obj->fnAdd('zip', $_POST['zip'], 'req', 'Please enter Zip.');
        $obj->fnAdd('city', $_POST['city'], 'req', 'Please enter City.');
        $obj->fnAdd('country', $_POST['country'], 'req', 'Please select Country.');
                
                
                if($userObj->isEori($_POST['country'])){
                    $obj->fnAdd('eoriNo', $_POST['eoriNo'], 'req', LANG_PLEASE_INSERT_EORI_NO);
                }
                
        //$obj->fnAdd('phone', $_POST['phone'], 'req', 'Please enter Phone No.');
        //$obj->fnAdd('mobile', $_POST['mobile'], 'req', 'Please enter Mobile No.');                
        $obj->fnAdd('language', $_POST['language'], 'req', 'Please select Language.');        
            
        $arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;
            
        $arr_error['firma'] = $obj->fnGetErr($arr_error['firma']);
        $arr_error['title'] = $obj->fnGetErr($arr_error['title']);
        $arr_error['fName'] = $obj->fnGetErr($arr_error['fName']);
        $arr_error['lName'] = $obj->fnGetErr($arr_error['lName']);
        $arr_error['email'] = $obj->fnGetErr($arr_error['email']);
        $arr_error['address1'] = $obj->fnGetErr($arr_error['address1']);
        $arr_error['zip'] = $obj->fnGetErr($arr_error['zip']);
        $arr_error['city'] = $obj->fnGetErr($arr_error['city']);
        $arr_error['country'] = $obj->fnGetErr($arr_error['country']);
        //$arr_error['phone'] = $obj->fnGetErr($arr_error['phone']);
        //$arr_error['mobile'] = $obj->fnGetErr($arr_error['mobile']);
        $arr_error['language'] = $obj->fnGetErr($arr_error['language']);
                
                if($userObj->isEori($_POST['country'])){
                    
                    $arr_error['eoriNo'] = $obj->fnGetErr($arr_error['eoriNo']);
                }
                
        if($str_validate){            
            $_POST = postwithoutspace($_POST);
                        
            $userObj->insertAddress($_POST);
        }
    }
    
    if(isset($_POST['updateAddress'])) {
//            echo '<pre>';
//            print_r($_POST);
//            exit;
        include_once("validation_class.php");
        $obj = new validationclass();

        $obj->fnAdd('firma',$_POST['firma'],'req', 'Please enter firm');
        $obj->fnAdd('title',$_POST['title'],'req', 'Please select title.');        
        $obj->fnAdd('fName', $_POST['fName'], 'req', 'Please enter First Name.');        
        $obj->fnAdd('lName',$_POST['lName'], 'name', 'Please enter valid Last Name.');
        $obj->fnAdd('email',$_POST['email'], 'req', 'Please enter Email Address.');
        $obj->fnAdd('address1', $_POST['address1'], 'req', 'Please enter your address.');        
        $obj->fnAdd('zip', $_POST['zip'], 'req', 'Please enter Zip.');
        $obj->fnAdd('city', $_POST['city'], 'req', 'Please enter City.');
        $obj->fnAdd('country', $_POST['country'], 'req', 'Please select Country.');
        //$obj->fnAdd('phone', $_POST['phone'], 'req', 'Please enter Phone No.');
        //$obj->fnAdd('mobile', $_POST['mobile'], 'req', 'Please enter Mobile No.');                
        $obj->fnAdd('language', $_POST['language'], 'req', 'Please select Language.');        
        
//                if($userObj->isEori($_POST['country'])){
//                    $obj->fnAdd('eoriNo', $_POST['eoriNo'], 'req', LANG_PLEASE_INSERT_EORI_NO);
//                }
                
        $arr_error = $obj->fnValidate();
        $str_validate = (count($arr_error)) ? 0 : 1;
            
        $arr_error['firma'] = $obj->fnGetErr($arr_error['firma']);
        $arr_error['title'] = $obj->fnGetErr($arr_error['title']);
        $arr_error['fName'] = $obj->fnGetErr($arr_error['fName']);
        $arr_error['lName'] = $obj->fnGetErr($arr_error['lName']);
        $arr_error['email'] = $obj->fnGetErr($arr_error['email']);
        $arr_error['address1'] = $obj->fnGetErr($arr_error['address1']);
        $arr_error['zip'] = $obj->fnGetErr($arr_error['zip']);
        $arr_error['city'] = $obj->fnGetErr($arr_error['city']);
        $arr_error['country'] = $obj->fnGetErr($arr_error['country']);
//                if($userObj->isEori($_POST['country'])){                    
//                    $arr_error['eoriNo'] = $obj->fnGetErr($arr_error['eoriNo']);
//                }
        //$arr_error['phone'] = $obj->fnGetErr($arr_error['phone']);
        //$arr_error['mobile'] = $obj->fnGetErr($arr_error['mobile']);
        $arr_error['language'] = $obj->fnGetErr($arr_error['language']);
                print_r($arr_error);
        
        if($str_validate){            
                    $_POST = postwithoutspace($_POST);                        
                    $userObj->updateAddress($_POST);
        }
    }
        
    $user = $userObj->getUserFullInfo($_SESSION[USER_ID]);                   
    $totalInvoiceAdd = $userObj->getTotalRecord("in");
    $totalDeliveryAdd = $userObj->getTotalRecord("de");
    $totalGraficAdd = $userObj->getTotalRecord("gr");
        
?>
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>


  <!--Content--> 
          <div id="content" class="active">         
           
            <div class="account-detail active">
            <div id="myAccPopup" class="popup" style="display:none;">                    
                        
             
  
           </div>


<!----------General Profile----------->

                <? echo $_SESSION['SESS_MSG']; unset($_SESSION['SESS_MSG']); ?>
                <?php $registerDate = substr(date('ymd\'hisA', strtotime($user->registerDate)), 0, -1);?>
                <h2><?php echo LANG_YOUR_ACCOUNT; ?>//<span><?php echo " No. ".$registerDate ?></span></h2>
                <ul class="about-profile">
                    <li>
                        <label><?php echo LANG_GENERAL_PROFILE; ?></label>
                        <p><?= stripslashes($user->firma)." // ".stripslashes($user->firstName)." ".stripslashes($user->lastName)." // ".stripslashes($user->zip)." // ".stripslashes($user->city); ?></p>
                        <a href="javascript:;" onClick="editUserInfo('generalProfile','<?= $user_id;?>');"><span><?= LANG_EDIT; ?></span></a>
                    </li>
                    <li>
                        <label><?php echo LANG_ACTUAL_INVOICE_ADDRESS; ?></label>
                        <div class="myaccount-select">
                            <select id="invoiceHeading">
                                    
                                <?= $userObj->getAddressHeading('in'); ?>
                            </select>
                        </div>
                        <a href="javascript:;" onClick="editAddress('invoiceAdd', '<?= LANG_INVOICE; ?>', 'invoiceHeading', 'in');"><span><?= LANG_EDIT; ?></span></a>
                        <?php
                            if($totalInvoiceAdd >= 10) { ?>
                                <a><span class="add-address"><?= LANG_ADD_INVOICE_ADDRESS; ?></span></a>
                        <?php } else {                                                    
                        ?>                        
                            <a href="javascript:void(0);" onClick="addAddress('invoiceAdd', '<?= LANG_INVOICE; ?>', 'in');"><span class="add-address"><?= LANG_ADD_INVOICE_ADDRESS; ?></span></a>                         
                        <?php } ?>
                    </li>
                    <li>
                        <label><?php echo LANG_ACTUAL_DELIVERY_ADDRESS; ?></label>
                        <div class="myaccount-select">
                            <select id="deliveryHeading">
                                <?= $userObj->getAddressHeading('de'); ?>
                            </select>
                        </div>
                        <a href="javascript:;" onClick="editAddress('deliveryAdd', '<?= LANG_DELIVERY; ?>', 'deliveryHeading', 'de');"><span><?= LANG_EDIT; ?></span></a>
                        <?php 
                            if($totalDeliveryAdd >= 10) { ?>                                
                                <a><span class="add-address"><?= LANG_ADD_DELIVERY_ADDRESS; ?></span></a>
                        <?php } else {
                        ?>                        
                            <a href="javascript:;" onClick="addAddress('deliveryAdd', '<?= LANG_DELIVERY; ?>', 'de');"><span class="add-address"><?= LANG_ADD_DELIVERY_ADDRESS; ?></span></a>
                        <?php } ?>
                    </li>
                    <li>
                        <label><?php echo LANG_ACTUAL_DESIGNER_ADDRESS; ?></label>
                        <div class="myaccount-select">
                            <select id="graficHeading">
                                <?= $userObj->getAddressHeading('gr'); ?>
                            </select>
                        </div>
                        <a href="javascript:;" onClick="editAddress('graficAdd', '<?= LANG_GRAFIC; ?>', 'graficHeading', 'gr');"><span><?= LANG_EDIT; ?></span></a>
                        <?php 
                            if($totalGraficAdd >= 10) { ?>                                
                                <a><span class="add-address"><?= LANG_ADD_GRAFIC_ADDRESS; ?></span></a>
                        <?php } else {
                                                    
                        ?>            
                            <a href="javascript:;" onClick="addAddress('graficAdd', '<?= LANG_GRAFIC; ?>', 'gr');"><span class="add-address"><?= LANG_ADD_GRAFIC_ADDRESS; ?></span></a>
                        <?php } ?>
                    </li>
                </ul>
                <ul class="about-orders" style="min-height: 600px;">                    
                    <li>
                        <label class="labelHeading"><?= LANG_INVOICES_OF_DELIVERED_ORDERS; ?></label>
                        <div class="myaccount-input-text">
                            <table class="place-order" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>S. No.</th>
                                    <th><?php echo LANG_ORDER_NO; ?></th>
                                    <th><?= LANG_OPEN_ORDER_IN_SDS_FOR_REORDER; ?></th>                                    
                                    <th><?= LANG_OPTION_NEW; ?></th>
                                    <th><?= LANG_PAYMENT_STATUS; ?></th>
                                    <th><?= LANG_ORDERSTATUS; ?></th>
                                </tr>                                                                  
                               <?php echo $userObj->getUserOrder(); ?>
                            </table>
                        </div>
                    </li>
                    
                    <li>
                        <label class="labelHeading"><?=LANG_ARCHIVE_SAVED_COLLECTION; ?></label>
                        <div class="myaccount-input-text">
                            <table class="place-order" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>S. No.</th>
                                    <th><?= LANG_ADDED_DATE; ?></th>                                    
                                    <th><?= LANG_OPTION; ?></th>
                                    <th><?php echo LANG_DELETE_WORK; ?></th>
                                </tr>                                                                  
                                <?php echo $userObj->getUserCollectionArchive(); ?>                                
                            </table>
                        </div>
                    </li>                    
                  
                    <li style="visibility:hidden;">
                        <label class="labelHeading">old messages / enquirys</label>
                        <div class="myaccount-input-text">
                            <textarea rows="0" cols="0"> <- 120815 How about order GE120312.1 Gorrilla GmbH-> 120816 Your order will be delivered to your enduser adress till 20th august
                            </textarea>
                        </div>
                    </li>
                    <li style="visibility: hidden;">
                        <label class="labelHeading">Send a message / enquiry</label>
                        <div class="myaccount-input-text">
                            <textarea rows="0" cols="0"></textarea>
                        </div>
                    </li>
                    <li>
                        <div class="login-button"><input type="submit" value="<?= LANG_SAVE; ?>" /></div>
                    </li>
       
                </ul>     
            </div>
            
            
              
          </div>
        <!--Content Ends Here-->
      </div>
      
<?php include_once('includes/footer.php'); ?>
<script type="text/javascript">
    function activeForm(fId) {        
        $("#myAccPopup").css("display","block");
        $("#"+fId).css("display", "block");
    }
    
    function closeForm(fId) {
        $("#myAccPopup").css("display","none");        
        $("#"+fId).css("display", "none");
    }            
</script>

<?php

session_start();

class User extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }

    //26April2013
//	function getAllCountry($countryId = ''){
//		$sql = "SELECT id,countryName FROM ".TBL_COUNTRY." AS tbl1 INNER JOIN ".TBL_COUNTRY_DESCRIPTION." AS tbl2 ON (tbl1.id = tbl2.countryId) WHERE tbl2.langId='$_SESSION[DEFAULTLANGUAGEID]' AND tbl1.status='1'  AND tbl1.isDeleted='0'"; 
//		$rst = $this->executeQry($sql);
//		$preTable = " ";
//		while($row = $this->getResultObject($rst)){
//			if($countryId == $row->id){ $selected = 'selected="selected"';}else{  $selected = '';}
//			$preTable .= "<option value='$row->id' $selected >".utf8_encode(ucwords($row->countryName))."</option>";
//		}   
//		return $preTable;
//	}

    function getAllCountry($countryId = '') {
        $outPut = '<option value="">' . LANG_SELECT_COUNTRY . '</option>';
        $query = "select cs.countryId, cd.countryName from " . TBL_COUNRTYSETTING . " as cs INNER JOIN " . TBL_COUNTRY_DESCRIPTION . " as cd ON (cs.countryId = cd.countryId) and cd.langId = '1' and cs.status = '1' and cs.isDeleted = '0'";
        $result = $this->executeQry($query);
        while ($line = $this->getResultObject($result)) {
            $sel = $line->countryId == $countryId ? 'selected="selected"' : '';
            $outPut .= '<option ' . $sel . ' value="' . $line->countryId . '">' . $line->countryName . '</option>';
        }
        return $outPut;
    }

    function getLanguage($languageId = '') {
        $sql = "SELECT id, languageName FROM " . TBL_LANGUAGE . " WHERE status='1'  AND isDeleted='0'";
        $rst = $this->executeQry($sql);
        $preTable = "";
        while ($row = $this->getResultObject($rst)) {
            if ($languageId == $row->id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . utf8_encode(ucwords($row->languageName)) . "</option>";
        }
        return $preTable;
    }

    /*
      function getDate($dd='') {
      for($i = 1; $i<=31; $i++) {
      if($dd == $i) {
      $selected = 'selected="selected"';
      }else {
      $selected = '';
      }
      $html .= '<option value="'.$i.'" '.$selected.'  >'.$i.'</option>';
      }
      return $html;
      }

      function getMonth($mm='') {
      for($i = 1; $i<=12; $i++) {
      if($mm == $i) {
      $selected = 'selected="selected"';
      }else {
      $selected = '';
      }
      $html .= '<option value="'.$i.'" '.$selected.' >'.$i.'</option>';
      }
      return $html;
      }

      function getYear($yy='') {
      $year = date('Y');
      for($i = 1920; $i<=$year; $i++) {
      if($yy == $i) {
      $selected = 'selected="selected"';
      }else {
      $selected = '';
      }
      $html .= '<option value="'.$i.'" '.$selected.' >'.$i.'</option>';
      }
      return $html;
      }
     */

    function getUserTitleList($id = '') {
        $outPut = '';
        $query = "select T.id, TD.titleName from " . TBL_TITLE . " as T INNER JOIN " . TBL_TITLEDESC . " as TD ON (T.id = TD.Id) and T.status = '1' and T.isDeleted = '0' and TD.langId = " . $_SESSION[DEFAULTLANGUAGEID] . " order by TD.titleName";
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            $outPut = '<option value="0">' . LANG_PLEASE_SELECT_TITLE . '</option>';
            while ($line = $this->getResultObject($result)) {
                if ($id == $line->id)
                    $sel = 'selected = "selected"';
                else
                    $sel = '';
                $outPut .='<option ' . $sel . ' value="' . $line->id . '">' . $line->titleName . '</option>';
            }
        }
        return $outPut;
    }

    function getUserFullInfo($userId) {
        $rst = $this->selectQry(TBL_USER, "id=$userId", "", "");
        $row = $this->getResultObject($rst);
        return $row;
    }

    function getTotalRecord($addType) {
        $query = "select count(*) as totalRecord from " . TBL_ADDRESS . " where addressType = '" . $addType . "' and userId = '" . $_SESSION[USER_ID] . "'";
        $result = $this->executeQry($query);
        $row = $this->getResultObject($result);
        $totalRecord = $row->totalRecord;
        return $totalRecord;
    }

    function editUserInfo($userId) {
        include('setlanguage.php');
        $line = $this->getUserFullInfo($userId);
        list($yy, $mm, $dd) = explode('-', $line->birthday);
        $html = '<link type="text/css" href="admin/datepicker/themes/base/ui.all.css" rel="stylesheet" />
                   <!-- <script src="admin/js/jquery-1.6.2.js"></script> -->
                    <script src="admin/js/jquery.ui.core.js"></script>
                    <script src="admin/js/jquery.ui.datepicker.js"></script>
                    <script type="text/javascript">
                        $(function() {
                            $("#birthday").datepicker({
                                    dateFormat: "yy-mm-dd",
                                    showButtonPanel: true
                            });
                        });
                    </script>
                    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
                    <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
                    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
                    <script src="js/ajax.js" type="text/javascript" charset="utf-8"></script>
                <script>
					jQuery(document).ready( function() {                                        
						jQuery("#generalProfilefrm").validationEngine();
					});
				</script>';
        $html .= '<div id="generalProfile" class="myaccount-border-top" style="display:none;">            
                	<div class="myaccount-border-bottom">
                    	<div class="myaccount-border-mid">
                                <h2>' . LANG_GENERAL . '-<span>' . LANG_ADDRESS . '</span></h2>
                        	
                        	<form id="generalProfilefrm" name="generalProfilefrm" method="post" action="">
								<ul>                            
									<li>
										<label>' . LANG_FIRMA_ORGANISATION . '</label>
										<div class="right_input">
											<input type="text" name="firma" value="' . stripslashes($line->firma) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
                                                                                <label>' . LANG_TITLE . '*</label>
										<div class="profile-select">
											<select name="title" class="validate[required] text-input" >
												' . $this->getUserTitleList($line->title) . '
											</select>
										</div>
									</li>
									<li>
                                                                            <label>' . LANG_PERSON_FIRST_NAME . '*</label>										
										<div class="right_input">
											 <input type="text" name="fName" value="' . stripslashes($line->firstName) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
                                                                        <label>' . LANG_PERSON_LAST_NAME . '*</label>
										<div class="right_input">
											 <input type="text" name="lName" value="' . stripslashes($line->lastName) . '" class="validate[required] text-input" />
										</div>
									</li>									
									<li>
										<label>' . LANG_E_MAIL . '*</label>
										<div class="right_input">
											 <input type="text" name="email" value="' . stripslashes($line->email) . '" readonly="readonly" />
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . ' 1 *</label>
										<div class="right_input">
											<input type="text" name="address1" value="' . stripslashes($line->address) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . ' 2 *</label>
										<div class="right_input">
											 <input type="text" name="address2" value="' . stripslashes($line->address1) . '" class="text-input"/>
										</div>
									</li>
									<li>
										<label>' . LANG_PIN_CODE . '*</label>
										<div class="right_input">
											<input type="text" name="zip" value="' . stripslashes($line->zip) . '" class="validate[required,minSize[4],maxSize[6]] text-input" />
										</div>
									</li>
									<li>
                                                                        <label>' . LANG_CITY . '*</label>										
										<div class="right_input">
											<input type="text" name="city" value="' . stripslashes($line->city) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_COUNTRY . '*</label>
										<div class="profile-select">
											<select name="country" class="validate[required] text-input">
												' . $this->getAllCountry($line->countryId) . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PHONE . '*</label>
										<div class="right_input">
											<input type="text" name="phone" value="' . stripslashes($line->phoneNo) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_MOBILE_PHONE . '*</label>
										<div class="right_input">
											<input type="text" name="mobile" value="' . stripslashes($line->mobile) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
                                                                            <label>' . LANG_BIRTHDAY . '*</label>										
										<div class="right_input">
											<input type="text" id="birthday" name="birthday" value="' . stripslashes($line->birthday) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_LANGUAGE . '*</label>
										<div class="profile-select">
											<select name="language" class="validate[required] text-input">
												' . $this->getLanguage($line->language) . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PASSWORD_NEW . ':</label>
										<div class="right_input">
											<input type="password" id="password" name="password" value="" class="validate[optional, minSize[5],maxSize[10]] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_CONFIRM_YOUR_PASSWORD . ':</label>
										<div class="right_input">
											<input type="password" name="c_password" value="" class="validate[optional,equals[password]] text-input" />
										</div>
									</li>
									<li>
										<div class="login-button">
											<input type="submit" name="saveGeneralProfile" value="' . LANG_SAVE . '" />
											<input type="button" name="cancel" value="' . LANG_CANCEL . '" onClick="closeForm(\'generalProfile\');"/>
										</div>
									</li>
									<li>
										<label style="width:100%;">* ' . LANG_THIS_FIELD_IS_REQUIRED_MSG . '</label>
                                                                        </li>
								</ul>
							</form>
                    	</div>
                	</div>
                </div>';
        echo $html;
    }

    function updateUserInformation($post) {
//		 echo "<pre>";
//		 print_r($post);exit;	

        $_SESSION['SESS_MSG'] = "";
        $this->tablename = TBL_USER;
        $this->field_values['firma'] = $post['firma'];
        $this->field_values['title'] = $post['title'];
        $this->field_values['firstName'] = $post['fName'];
        $this->field_values['lastName'] = $post['lName'];
        $this->field_values['email'] = $post['email'];
        $this->field_values['address'] = $post['address1'];
        $this->field_values['address1'] = $post['address2'];
        $this->field_values['zip'] = $post['zip'];
        $this->field_values['city'] = $post['city'];
        $this->field_values['countryId'] = $post['country'];
        $this->field_values['phoneNo'] = $post['phone'];
        $this->field_values['mobile'] = $post['mobile'];
        if (!empty($post['password']) || $post['password'] != '') {
            $this->field_values['userPassword'] = $this->encrypt_password($post['password']);
            $this->field_values['hash'] = md5($post['email'] . ":" . $post['password']);
        }

        $this->field_values['birthday'] = $post['birthday'];
        $this->field_values['language'] = $post['language'];

        $this->condition = "id='" . $_SESSION['USER_ID'] . "'";
        $res = $this->updateQry();
        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_UPDATED_SUCCESSFULLY);
        header("Location:myaccount.php");
        exit;
    }

    function encrypt_password($plain) {
        $password = '';
        for ($i = 0; $i < 10; $i++) {
            $password .= $this->tep_rand();
        }
        $salt = substr(md5($password), 0, 2);
        $password = md5($salt . $plain) . ':' . $salt;
        return $password;
    }

    function tep_rand($min = null, $max = null) {
        static $seeded;
        if (!$seeded) {
            mt_srand((double) microtime() * 1000000);
            $seeded = true;
        }
    }

    function addAddress($get) {
        include('setlanguage.php');
        $line = $this->getUserFullInfo($_SESSION[USER_ID]);
        if ($get['addressType'] == 'in') {
            $generalMessage = LANG_SAVE_ADDRESS_STANDARD;
        }
        if ($get['addressType'] == 'de') {
            $generalMessage = LANG_SAVE_DELIVERY_ADDRESS;
        }
        if ($get['addressType'] == 'gr') {
            $generalMessage = LANG_SAVE_GRAFIC_ADDRESS;
        }

        $html = '<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
<script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="js/ajax.js" type="text/javascript" charset="utf-8"></script><script>
					jQuery(document).ready( function($) {
                                        //alert(1);
                                        
						$("#generalProfilefrm").validationEngine();
                                        
                                        });
					
					$("#sameData").click(function() {
						var chk = $("#sameData").attr(\'checked\');
						if(chk == "checked") {
							$("#firma").val(\'' . $line->firma . '\');
							$("#title").val(\'' . $line->title . '\');
							$("#fName").val(\'' . $line->firstName . '\');
							$("#lName").val(\'' . $line->lastName . '\');
							$("#email").val(\'' . $line->email . '\');
							$("#address1").val(\'' . $line->address . '\');
							$("#address2").val(\'' . $line->address1 . '\');
							$("#zip").val(\'' . $line->zip . '\');
							$("#city").val(\'' . $line->city . '\');
							$("#country").val(\'' . $line->countryId . '\');
							$("#phone").val(\'' . $line->phoneNo . '\');
							$("#mobile").val(\'' . $line->mobile . '\');
							$("#language").val(\'' . $line->language . '\');	
						}else {
							$("#firma").val(\'\');
							$("#title").val(\'\');
							$("#fName").val(\'\');
							$("#lName").val(\'\');
							$("#email").val(\'\');
							$("#address1").val(\'\');
							$("#address2").val(\'\');
							$("#zip").val(\'\');
							$("#city").val(\'\');
							$("#country").val(\'\');
							$("#phone").val(\'\');
							$("#mobile").val(\'\');
							$("#dd").val(\'\');
							$("#mm").val(\'\');
							$("#yy").val(\'\');
							$("#language").val(\'\');
						}
					});					
				</script>';

        $html .= '<div id="' . $get[fId] . '" class="myaccount-border-top" style="display:none;">
                	<div class="myaccount-border-bottom">
                    	<div class="myaccount-border-mid">							
                        	<h2>' . $get[heading] . '-<span>' . LANG_ADDRESS . '</span></h2>
                        	<form id="generalProfilefrm" name="generalProfilefrm" method="post" action="">
								<ul>
									<li>
										<label>' . LANG_USE_DATA_GENERAL_PROFILE . '</label>
										<div>
											<input type="checkbox" id="sameData" name="sameData" value="sameData" />
										</div>
									</li>                            
									<li>
										<label>' . LANG_FIRMA_ORGANISATION . '*</label>
										<div class="right_input">
											<input type="text" id="firma" name="firma" value="" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_TITLE . '</label>
										<div class="profile-select">
											<select id="title" name="title" class="validate[required] text-input">
											' . $this->getUserTitleList() . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PERSON_FIRST_NAME . '*</label>
										<div class="right_input">
											 <input type="text" id="fName" name="fName" value="" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_PERSON_LAST_NAME . '*</label>
										<div class="right_input">
											 <input type="text" id ="lName" name="lName" value="" class="validate[required] text-input" />
										</div>
									</li>									
									<li>
										<label>' . LANG_E_MAIL . '*</label>
										<div class="right_input">
											 <input type="text" id ="email" name="email" value="" class="validate[required,custom[email], text-input"/>
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . '*</label>
										<div class="right_input">
											<input type="text" id ="address1" name="address1" value="" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . '</label>
										<div class="right_input">
											 <input type="text" id="address2" name="address2" value=""/>
										</div>
									</li>
									<li>
										<label>' . LANG_PIN_CODE . '*</label>
										<div class="right_input">
											<input type="text" id="zip" name="zip" value="" class="validate[required,minSize[4],maxSize[10]] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_CITY . '*</label>
										<div class="right_input">
											<input type="text" id="city" name="city" value="" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_COUNTRY . '*</label>
										<div class="profile-select">
											<select id="country" name="country" class="validate[required] text-input" onChange="javascript:getEori(this.value)">
												' . $this->getAllCountry() . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PHONE . '*</label>
										<div class="right_input">
											<input type="text" id="phone" name="phone" value="" class="validate[required] text-input" />
										</div>
									</li>
                                                                        


									<li>
										<label>' . LANG_MOBILE_PHONE . '*</label>
										<div class="right_input">
											<input type="text" id="mobile" name="mobile" value="" class="validate[required] text-input" />
										</div>
									</li>';
                        if ($get['addressType'] == 'in') {                           
                                                         $html .= '<li style="display:none;" id="eoridisp">
										<label>' . LANG_EORI_NUMBER . '*</label>
										<div class="right_input">
											<input type="text" id="eoriNo" name="eoriNo" value="' . stripslashes($line->eoriNo) . '" class="validate[required] text-input" />
										</div>
                                                                                                                                                                    
                                                                                <p style="color: green;"><small style="margin-left:50px; word-wrap:break-word;">*' . LANG_EORI_NOTE_TEXT . '</small></p>
                                                                                
									</li>';
                        }                                                

									
							$html .= '<li>
										<label>' . LANG_LANGUAGE . '*</label>
										<div class="profile-select">
											<select id="language" name="language" class="validate[required] text-input">
												' . $this->getLanguage() . '
											</select>
										</div>
									</li>
									<li>
										<label>' . $generalMessage . '</label>
										<div>
											<input type="checkbox" id="standard" name="standard" value="1" />
										</div>
									</li>  
									
									<li>
										<div class="login-button">
											<input type="hidden" name="addressType" value="' . $_GET[addressType] . '" />
											<input type="submit" name="insertAddress" value="' . LANG_SAVE . '" />
											<input type="button" name="cancel" value="' . LANG_CANCEL . '" onClick="closeForm(\'invoiceAdd\');"/>
										</div>
									</li>
									<li>
										<label style="width:100%;">* ' . LANG_THIS_FIELD_IS_REQUIRED_MSG . '</label>
										<div>
										</div>
									</li> 
								</ul>
							</form>
                    	</div>
                	</div>
                </div>';
        echo $html;
    }

    function insertAddress($post) {
        $query = "insert into " . TBL_ADDRESS . " set userId = '" . $_SESSION[USER_ID] . "', addressType = '" . $post[addressType] . "', firma = '" . $post['firma'] . "', title = '" . $post['title'] . "', firstName = '" . $post['fName'] . "', lastName = '" . $post['lName'] . "', email = '" . $post[email] . "', address = '" . $post['address1'] . "', address1 = '" . $post['address2'] . "', zip = '" . $post['zip'] . "', city = '" . $post['city'] . "', countryId = '" . $post['country'] . "', phoneNo = '" . $post['phone'] . "', mobile = '" . $post['mobile'] . "',eoriNo = '" . $post['eoriNo'] . "', language = '" . $post['language'] . "', status = '1'";
        $result = $this->executeQry($query);
        $inserted_id = mysql_insert_id();

        if (isset($post['standard'])) {
            $query = "update " . TBL_ADDRESS . " set standard = '0' where userId = '" . $_SESSION[USER_ID] . "'";
            $this->executeQry($query);
            $query = "update " . TBL_ADDRESS . " set standard = '1' where id = '" . $inserted_id . "'";
            $this->executeQry($query);
        }

        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_ADDED_SUCCESSFULLY);
        header("Location:myaccount.php");
        exit;
    }

    function getAddressHeading($addType) {
        $sql = "SELECT id, firma, firstName, lastName, zip, city, standard FROM " . TBL_ADDRESS . " WHERE addressType = '" . $addType . "' and userId = '" . $_SESSION[USER_ID] . "' and status='1'";

        $rst = $this->executeQry($sql);
        $preTable = " ";
        while ($row = $this->getResultObject($rst)) {
            if ($row->standard == '1') {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . $row->firma . " // " . $row->firstName . " " . $row->lastName . " // " . $row->zip . " // " . $row->city . "</option>";
        }
        return $preTable;
    }

    function editAddress($get) {
        include('setlanguage.php');
        $rst = $this->selectQry(TBL_ADDRESS, "id=$get[id]", "", "");
        $line = $this->getResultObject($rst);
        $eoriType = 0;
        if ($get['addressType'] == 'in') {
            $generalMessage = LANG_SAVE_ADDRESS_STANDARD;
            $eoriType = $this->fetchValue(TBL_COUNRTYSETTING, 'eoriNo', 'countryId=' . $line->countryId);
        }
        if ($get['addressType'] == 'de') {
            $generalMessage = LANG_SAVE_DELIVERY_ADDRESS;
        }
        if ($get['addressType'] == 'gr') {
            $generalMessage = LANG_SAVE_GRAFIC_ADDRESS;
        }
        $eoriDisp = $eoriType ? '' : 'style="display:none"';
        //$eoriDispValid = $eoriType ? 'validate[required]' : '';
        $html = '               <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>
                                <script src="js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
                                <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
                                <script src="js/ajax.js" type="text/javascript" charset="utf-8"></script>
            <script>
					jQuery(document).ready( function() {
						jQuery("#generalProfilefrm").validationEngine();												
					});					
				</script>';
        $html .= '<div id="' . $get[fId] . '" class="myaccount-border-top" style="display:none;">
                	<div class="myaccount-border-bottom">
                    	<div class="myaccount-border-mid">							
                        	<h2>' . $get[heading] . '-<span>Address</span></h2>
                        	<form id="generalProfilefrm" name="generalProfilefrm" method="post" action="">
								<ul>									                           
									<li>
										<label>' . LANG_FIRMA_ORGANISATION . '*</label>
										<div class="right_input">
											<input type="text" id="firma" name="firma" value="' . stripslashes($line->firma) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_TITLE . '</label>
										<div class="profile-select">
											<select id="title" name="title" class="validate[required] text-input">
											' . $this->getUserTitleList($line->title) . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PERSON_FIRST_NAME . '*</label>
										<div class="right_input">
											 <input type="text" id="fName" name="fName" value="' . stripslashes($line->firstName) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_PERSON_LAST_NAME . '*</label>
										<div class="right_input">
											 <input type="text" id ="lName" name="lName" value="' . stripslashes($line->lastName) . '" class="validate[required] text-input" />
										</div>
									</li>									
									<li>
										<label>' . LANG_E_MAIL . '*</label>
										<div class="right_input">
											 <input type="text" id ="email" name="email" value="' . stripslashes($line->email) . '" />
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . '*</label>
										<div class="right_input">
											<input type="text" id ="address1" name="address1" value="' . stripslashes($line->address) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_ADDRESS . '</label>
										<div class="right_input">
											 <input type="text" id="address2" name="address2" value="' . stripslashes($line->address1) . '"/>
										</div>
									</li>
									<li>
										<label>' . LANG_PIN_CODE . '*</label>
										<div class="right_input">
											<input type="text" id="zip" name="zip" value="' . stripslashes($line->zip) . '" class="validate[required,minSize[4],maxSize[10]] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_CITY . '*</label>
										<div class="right_input">
											<input type="text" id="city" name="city" value="' . stripslashes($line->city) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_COUNTRY . '*</label>
										<div class="profile-select">
											<select id="country" name="country" class="validate[required] text-input" onChange="javascript:getEori(this.value)">
												' . $this->getAllCountry($line->countryId) . '
											</select>
										</div>
									</li>
									<li>
										<label>' . LANG_PHONE . '*</label>
										<div class="right_input">
											<input type="text" id="phone" name="phone" value="' . stripslashes($line->phoneNo) . '" class="validate[required] text-input" />
										</div>
									</li>
									<li>
										<label>' . LANG_MOBILE_PHONE . '*</label>
										<div class="right_input">
											<input type="text" id="mobile" name="mobile" value="' . stripslashes($line->mobile) . '" class="validate[required] text-input" />
										</div>
									</li>';


        if ($get['addressType'] == 'in') {
        $html .= '<li ' . $eoriDisp . ' id="eoridisp">
										<label>' . LANG_EORI_NUMBER . '*</label>
										<div class="right_input">
											<input type="text" id="eoriNo" name="eoriNo" value="' . stripslashes($line->eoriNo) . '" class="validate[required] text-input" />
										</div>
                                                                                <p style="color:green; width:80%; margin-left:40px;"><small>*' . LANG_EORI_NOTE_TEXT . '</small></p>
									</li>';

        }
        $html .= '<li>
										<label>' . LANG_LANGUAGE . '*</label>
										<div class="profile-select">
											<select id="language" name="language" class="validate[required] text-input">
												' . $this->getLanguage($line->language) . '
											</select>
										</div>
									</li>
									<li>
										<label>' . $generalMessage . '</label>
										<div>
											<input type="checkbox" id="standard" name="standard" value="1" />
										</div>
									</li>  
									
									<li>
										<div class="login-button">
											<input type="hidden" name="addressType" value="' . $get[addressType] . '" />
											<input type="hidden" name="id" value="' . $get[id] . '" />
											<input type="submit" name="updateAddress" value="' . LANG_SAVE . '" />
											<input type="button" name="cancel" value="' . LANG_CANCEL . '" onClick="closeForm(\'invoiceAdd\');"/>
										</div>
									</li>
									<li>
										<label style="width:100%;">* ' . LANG_THIS_FIELD_IS_REQUIRED_MSG . '</label>
                                                                        </li>
								</ul>
							</form>
                    	</div>
                	</div>
                </div>';
        echo $html;
    }

    function updateAddress($post) {
        //~ echo "<pre>";
        //~ print_r($post);exit;

        $query = "update " . TBL_ADDRESS . " set userId = '" . $_SESSION[USER_ID] . "', firma = '" . $post['firma'] . "', title = '" . $post['title'] . "', firstName = '" . $post['fName'] . "', lastName = '" . $post['lName'] . "', email = '" . $post[email] . "', address = '" . $post['address1'] . "', address1 = '" . $post['address2'] . "', zip = '" . $post['zip'] . "', city = '" . $post['city'] . "', countryId = '" . $post['country'] . "', phoneNo = '" . $post['phone'] . "', mobile = '" . $post['mobile'] . "',eoriNo = '" . $post['eoriNo'] . "', language = '" . $post['language'] . "', status = '1', modBy = '" . $_SESSION['USER_ID'] . "' where id = '" . $post['id'] . "'";

        $result = $this->executeQry($query);

        if (isset($post['standard'])) {
            $query = "update " . TBL_ADDRESS . " set standard = '0' where userId = '" . $_SESSION[USER_ID] . "' and addressType = '" . $post[addressType] . "'";
            $this->executeQry($query);
            $query = "update " . TBL_ADDRESS . " set standard = '1' where id = '" . $post['id'] . "'";
            $this->executeQry($query);
        }

        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_UPDATED_SUCCESSFULLY);
        header("Location:myaccount.php");
        exit;
    }

    function getUserCollectionArchive() {
        include('setlanguage.php');
        $html = '';
        $count = 1;

        $result = $this->executeQry("select collectionName, addDate,id from " . TBL_COLLECTION . " where userId ='" . $_SESSION['USER_ID'] . "'  group by collectionName order by id desc");
        $num = $this->getTotalRow($result);
        //$num = $this->getTotalRecord();
        if ($num > 0) {
            while ($archive = $this->getResultObject($result)) {
                $html .= '<tr>';
                $html .= '<td>' . $count . '</td>';
                $html .= '<td>' . $archive->addDate . '</td>';
                $html .= '<td><a href="customize.php?collection=' . $archive->collectionName . '" >' . LANG_CUSTOMIZE . '</a></td>
                          <td><a href="pass.php?action=archive&type=deleteRecord&id='.$archive->collectionName.'" onclick="return confirm(\'Are you sure to delete this work?\');">' . LANG_DELETE_WORK . '</a></td>';
                $html .= '</tr>';
                $count++;
            }
        } else {
            $html .= '<tr>
                        <td colspan="3"><h4>You don\'t have collection yet.</h4></td>
                      </tr>';
        }
        return $html;
    }
    
    function deleteRecord($id){
        
        $result = $this->executeQry("Delete from " . TBL_COLLECTION . " where collectionName ='" .$id . "'");
        $result = $this->executeQry("Delete from " . TBL_COLLECTION_DATA . " where collectionName ='" .$id . "'");
        $result = $this->executeQry("Delete from " . TBL_COLLECTION_DECO . " where collectionName ='" .$id . "'");
        $result = $this->executeQry("Delete from " . TBL_COLLECTION_IMAGE . " where collectionName ='" .$id . "'");
        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_UPDATED_SUCCESSFULLY);
        redirect("myaccount.php");exit;
    }
    
    function getUserOrder() {
        include('setlanguage.php');
        $html = '';
        $count = 1;

        $query = "select id, langId, ordReceiptId, orderDate, paymentStatus, orderStatus , collectionName from " . TBL_ORDER . " where userId = '" . $_SESSION['USER_ID'] . "' order by id desc";
        $result = $this->executeQry($query);
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                
                $collectionNameUrl = $line->collectionName!=''?'customize.php?collection=' . $line->collectionName :'javascript:void(0);';
                $paymentStatus = $this->fetchValue(TBL_ORDER_PAYMENTSTATUSDESC, "orderStatus", "orderStatusId = '" . $line->paymentStatus . "' and langId = '" . $line->langId . "'");
                $orderStatus = $this->fetchValue(TBL_ORDERSTATUSDESC, "orderStatus", "orderStatusId = '" . $line->orderStatus . "' and langId = '" . $line->langId . "'");
                $html .= '<tr>';
                $html .= '<td style="width:7%;">' . $count . '</td>';
                $html .= '<td style="width:20%;">' . $line->ordReceiptId . '</td>';
                $html .= '<td style="width:15%;"><a href="' . $collectionNameUrl . '" >' . $line->orderDate . '</a></td>';
                $html .= '<td style="width:20%;"><a href="thanks.php?oId=' . base64_encode($line->id) . '" >' . LANG_VIEW_ORDER . '</a></td>';
                $html .= '<td style="width:20%;">' . $paymentStatus . '</td>';
                $html .= '<td style="width:20%;">' . $orderStatus . '</td>';
                $html .= '</tr>';
                $count++;
            }
        } else {
            $html .= "<tr>
                        <td colspan='4'><h4>You don't have collection yet.</h4></td>
                    </tr>";
        }
        return $html;
    }

    function isEori($countryId) {
        $eoriType = $this->fetchValue(TBL_COUNRTYSETTING, 'eoriNo', 'countryId=' . $countryId);

        if ($eoriType) {
            return 1;
        }
        else
            return 0;
    }

}

// End Class
?>

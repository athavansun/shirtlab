<?php
session_start();
$arr=explode('/',__DIR__); 
array_pop($arr); 

require_once implode('/',$arr)."/JSON.php";
require_once  __DIR__.'/'.'MySqlDriver.php';
require_once  __DIR__.'/'.'MailFunction.php';
require_once  __DIR__.'/'.'GeneralFunctions.php';

class QuantityDelivery extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }

    function getCountry($countryId = '') {
//        $sql = "SELECT c.countryId, cd.countryName FROM " .TBL_COUNRTYSETTING. " AS c INNER JOIN " . TBL_COUNTRY_DESCRIPTION . " AS cd ON c.countryId = cd.countryId AND cd.langId='$_SESSION[DEFAULTLANGUAGEID]' AND c.status='1'  AND c.isDeleted='0' order by cd.countryName"; 
        $sql = "SELECT c.countryId, cd.countryName FROM " . TBL_COUNRTYSETTING . " AS c INNER JOIN " . TBL_COUNTRY_DESCRIPTION . " AS cd ON c.countryId = cd.countryId AND c.status='1'  AND c.isDeleted='0' order by cd.countryName";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        $preTable .= '<option value="">Country of Invoice +delivery</option>';
        while ($row = $this->getResultObject($rst)) {
            if ($countryId == $row->countryId) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->countryId' $selected >" . utf8_encode(ucwords($row->countryName)) . "</option>";
        }
        return $preTable;
    }

    function getCurrency($currencyId = '') {
        $sql = "SELECT cd.id, cd.currencyName, c.currencyCode FROM " . TBL_CURRENCY . " AS c INNER JOIN " . TBL_CURRENCY_DETAIL . " AS cd ON cd.id = c.currencyDetailId and c.status = '1' order by cd.currencyName";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        $preTable .= '<option value="">Currency</option>';
        while ($row = $this->getResultObject($rst)) {
            if ($currencyId == $row->id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . utf8_encode(ucwords($row->currencyName)) . "</option>";
        }
        return $preTable;
    }
    
    function getCurrencyFromCountry($currencyId = '') {
        $sql = "SELECT cd.id, cd.currencyName, c.currencyCode FROM " . TBL_CURRENCY . " AS c INNER JOIN " . TBL_CURRENCY_DETAIL . " AS cd ON cd.id = c.currencyDetailId and c.status = '1' AND cd.id=".$currencyId." order by cd.currencyName";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        //$preTable .= '<option value="">Currency</option>';
        while ($row = $this->getResultObject($rst)) {            
            $preTable .= "<option value='$row->id'>" . utf8_encode(ucwords($row->currencyName)) . "</option>";
        }
        return $preTable;
    }

    function getDeliveryTime($id = '') {
        $sql = "SELECT id, days from " . TBL_DELIVERY_TIME . " order by days";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        $preTable .= '<option value="">Delivery Time</option>';
        while ($row = $this->getResultObject($rst)) {
            if ($id == $row->id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . utf8_encode(ucwords($row->days))." ".LANG_DAYS."</option>";
        }
        return $preTable;
    }

    function getAddressHeading($addType, $id = '') {
        $sql = "SELECT id, firma, firstName, lastName, zip, city, standard FROM " . TBL_ADDRESS . " WHERE addressType = '" . $addType . "' and userId = '" . $_SESSION[USER_ID] . "' and status='1'";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        while ($row = $this->getResultObject($rst)) {
            if ($row->id == $id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . $row->firma . " // " . $row->firstName . " " . $row->lastName . " // " . $row->zip . " // " . $row->city . "</option>";
        }
        return $preTable;
    }

    function getDecoType($basketId, $viewId, $op) {
        include('setlanguage.php');
        $sql = "SELECT id, systemVal FROM " . TBL_SYSTEMCONFIG . " WHERE systemName in ('Embroidery', 'Print')";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        $id = '';
        while ($row = $this->getResultObject($rst)) {
            $decoType = '';
            if($row->systemVal == "Print") {
                $decoType = LANG_PRINT;
            }
            else if($row->systemVal == "Embroidery") {
                $decoType = LANG_EMBRO;
            }
            
            if ($op == '0') {
                $id = $this->fetchValue(TBL_BASKET_COLOR, "type", " basketId = '" . $basketId . "' and viewId ='" . $viewId . "'");
            }

            if ($row->id == $id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . $decoType . "</option>";
            //$preTable .= "<option value='$row->id' $selected >" . $row->systemVal . "</option>";
        }
        return $preTable;
    }

    function insertAddress($post) {
        $query = "insert into " . TBL_ADDRESS . " set userId = '" . $_SESSION[USER_ID] . "', addressType = '" . $post[addressType] . "', firma = '" . $post['firma'] . "', title = '" . $post['title'] . "', firstName = '" . $post['fName'] . "', lastName = '" . $post['lName'] . "', email = '" . $post[email] . "', address = '" . $post['address1'] . "', address1 = '" . $post['address2'] . "', zip = '" . $post['zip'] . "', city = '" . $post['city'] . "', countryId = '" . $post['country'] . "', phoneNo = '" . $post['phone'] . "', mobile = '" . $post['mobile'] . "', language = '" . $post['language'] . "', status = '1'";        
        $result = $this->executeQry($query);
        $inserted_id = mysql_insert_id();

        if (isset($post['standard'])) {
            $query = "update " . TBL_ADDRESS . " set standard = '0' where userId = '" . $_SESSION[USER_ID] . "'";
            $this->executeQry($query);
            $query = "update " . TBL_ADDRESS . " set standard = '1' where id = '" . $inserted_id . "'";
            $this->executeQry($query);
        }

        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_ADDED_SUCCESSFULLY);
        header("Location:quantitydeliverytime.php");
        exit;
    }

    function addQuantity($get) {
        include('setlanguage.php');
        $jsonObj = new JSON();
        $decoObj = (object) $decoObj;
        $decoInfo = array();
        $operation = 1;             //1 for insert 0 for update
        $viewNameArr = array();
        $editViewArr = array();
        $sizeResultSet = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId ='" . $get['basketId'] . "'");
        $num = $this->num_rows($sizeResultSet);
        if ($num > 0) {
            $operation = 0;
        }

        $decoResult = $this->executeQry("select decoInfo from " . TBL_MAINPRODUCT . " where id = '" . $get['mainProdId'] . "'");
        $numDeco = $this->num_rows($decoResult);
        $html = '<div class="popup-window" id="' . $get['fId'] . '">
            <form id="quantityForm" name="fabricQuantity" method="post" action="" onSubmit="return validateQuantity()">
            <input type="hidden" name="basketId" value="' . $get['basketId'] . '" />
            <input type="hidden" name="mainProdId" value="' . $get['mainProdId'] . '" />
            <input type="hidden" name="prodId" value="' . $get['pId'] . '" />
            <input type="hidden" name="fabricQuantityError" id="fabricQuantityError" value="' . LANG_QUANTITY_ERROR . '" />
            <input type="hidden" name="quantityNumberError" id="quantityNumberError" value="' . LANG_QUANTITYNUMBER_ERROR.'" />
            <input type="hidden" name="embroideryError" id="embroideryError" value="' . LANG_EMBROIDERY_ERROR . '" />
            <input type="hidden" name="quantityColorError" id="quantityColorError" value="' . LANG_QUANTITYCOLOR_ERROR . '" />
            <input type="hidden" name="colorNumberError" id="colorNumberError" value="' . LANG_COLORNUMBER_ERROR . '" />    
                

            <ul class="outer">
              <li>
                <label>'.LANG_CHOOSE_FABRIC_QUALITY.'</label>
                  <select name="fQuality" id="fabricQualityId" onChange="changeColorPallet(this.value);" size="1" style="width:auto;">';
        $html .= $this->getFabricQualityByStyleId($get['sId'], $get[qId]);
        $html .= '</select>
                  <span style="word-break:break-word; font-size:12px;">'.LANG_CHANGE_QUALITY_INFO.'</span>
              </li>
              <li>
                  <label>'.LANG_ENTER_QUANTITY.'</label>
                  <div class="categorytype">';
        $html .= $this->availableSize($get['pId'], $get['basketId'], $operation);
        $html .= '</div>
              </li><li id="quantityError" style="display:none;"></li>';
        $html .= ' <script src="js/chosen.jquery.js" type="text/javascript"></script>
  <script type="text/javascript">
      $(".chzn-select").chosen();
          $(".chzn-select1").chosen();
          $(".chzn-select2").chosen();
  </script>';

        $first = true;
        // $totalViewId = '';
        if ($numDeco > 0) {
            $decoRow = $this->fetch_object($decoResult);
            $decoInfo = $jsonObj->decode($decoRow->decoInfo);
            //  $numberOfView = count($decoInfo);
            foreach ($decoInfo as $decoObj) {
                $val = '';
                $check = '';
                $ch = '';
                $viewId = $decoObj->viewId;
                $viewImage = $this->fetchValue(TBL_MAINPRODUCT_VIEW, "viewImage", " mainProdId = '" . $_GET['mainProdId'] . "' and viewId = '" . $viewId . "'");

                $showViewName = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $viewId . "' and langId = '1'");
                $viewName = str_replace(" ", "", $showViewName);
                $langViewName = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $viewId . "' and langId = '".$_SESSION[DEFAULTLANGUAGEID]."'");
                
                
                $viewNameArr[] = $viewName;                
                $numberOfDeco = count($decoObj->decoContentArray);
                if ($numberOfDeco > 0) {
                    if ($operation == '0') {
                        $colorResult = $this->executeQry("SELECT count(id) as colorQuantity, basketId, viewId, colorId, type FROM " . TBL_BASKET_COLOR . " where basketId = '" . $get['basketId'] . "' and viewId = '" . $viewId . "' group by viewId");
                        $colorQuantity = $this->fetch_object($colorResult);
                        $photoPrint = $this->fetchValue(TBL_SYSTEMCONFIG, "id", " systemVal = 'Photo Print'");
                        if ($colorQuantity->type == $photoPrint) {
                            $val = '';
                            $check = 'checked="checked"';
                            $ch = 'disabled = "disabled"';
                        } else {
                            $val = $colorQuantity->colorQuantity;
                            $check = 'disabled = "disabled"';
                            $ch = "";
                        }
                    } else {
                        $ch = 'disabled = "disabled"';
                    }
                    if ($first) {
                        $totalView = $viewName;
                        $first = false;
                    } else {
                        $totalView .=',' . $viewName;
                    }

                    $html .= '<li>
                    <label>' . $langViewName . '</label>
                    <ul>                        
                        <li>
                            <select name="' . $viewName . 'Design" id="' . $viewName . 'Design" size="1" style="width:auto;" onChange="decoType(id, this.value)">
                                <option value="0">'.LANG_CHOOSE_PRINT_EMBROIDERY.'</option>'
                            . $this->getDecoType($get['basketId'], $viewId, $operation) .
                            '</select>
                        </li>
                      <li>
                          <label>'.LANG_QUANTITY_OF_COLORS.':</label>
                          <div class="input-text">
                              <input type="text" name="' . $viewName . 'ColorQty" id="' . $viewName . 'ColorQty" value="' . $val . '" onKeyup="enablePallet(\'' . $viewName . '\', this.value);" ' . $ch . ' />
                          </div>
                      </li>
                      <li>
                          <label>'.LANG_PHOTO_PRINT.' </label>
                          <input type="checkbox" name="' . $viewName . 'PhotoPrint" id="' . $viewName . 'PhotoPrint" onChange="disableDecoType(id, this.value);" ' . $check . ' />
                      </li>
                  </ul>
              </li>
              <li id="typeError_' . $viewName . '" style="display:none; width:28%; float:left;"></li>
              <li id="numberError_' . $viewName . '" style="display:none; width:28%; float:left;"></li>
              <li>
              <ul class="color-type">';
                    $colIdArr = array();
                    $colResult = $this->executeQry("select colorId from " . TBL_BASKET_COLOR . " where basketId = '" . $get['basketId'] . "' and viewId = '" . $viewId . "'");
                    $colNumber = $this->num_rows($colResult);
                    if ($colNumber > 0) {
                        while ($col = $this->fetch_object($colResult)) {
                            $colIdArr[] = $col->colorId;
                        }
                        //$editViewArr['view'][] = array_pop($viewNameArr);
                        $editViewArr['view'][] = $viewName;
                        $editViewArr['length'][] = count($colIdArr);
                    }

                    $numCol = count($colIdArr);
                    $et = '';
                    for ($i = 1; $i <= 8; $i++) {
                        if ($numCol >= $i) {
                            //$palletColor = $this->getPalletColor($get['qId'], $get['basketId'], $viewId, $colIdArr[$i-1]);

                            $divId = $viewName . 'Color' . $i . "_chzn";
                            $et = 'onChange = "removeColor(\'' . $divId . '\')"';

                            $palletColor = $this->getColorPalle($colIdArr[$i - 1]);
                            $html .= '<li class="' . $viewName . '_edit_pallet">
                             <label>Color ' . $i . '</label>
                                 <span style="position:relative; width:77px;">' . $this->getColorCode($colIdArr[$i - 1]) . '
                             <select ' . $et . ' name="' . $viewName . 'Color' . $i . '" id="' . $viewName . 'Color' . $i . '" size="1"  class="colorPallet chzn-select1 " tabindex="2" data-placeholder="Pantone">';
                            $html .= '<option value=""></option>';
                            $html .= $palletColor;

                            $html .= '</select></span></li>';
                        } else {
                            $palletColor = $this->getColorPalle();
                            $html .= '<li>
                             <label> '. LANG_COLOR.' '. $i . '</label>
                             <select name="' . $viewName . 'Color' . $i . '" id="' . $viewName . 'Color' . $i . '" size="1"  class="colorPallet chzn-select1 dis" tabindex="2" data-placeholder="Pantone &reg">';
                            $html .= '<option value=""></option>';
                            $html .= $palletColor;
                            $html .= '</select></li>';
                        }
                    }

                    $comment = $this->fetchValue(TBL_BASKET_DECO, "comment", "viewId = '" . $viewId . "' and basketId = '" . $get['basketId'] . "'");
                    $html .='
               </ul>
               <li><div id="' . $viewName . 'PantoneError" style="font-size:12px; color:red;"></div></li>
               </li>
               <li>
                  <label>'.LANG_COMMENT.' on ' . $langViewName . ' deco:</label>
                  <textarea name="comment_' . $viewId . '" id="comment_' . $viewId . '" rows="0" cols="0">' . $comment . '</textarea>';
                    $html .='<div style="float:right; margin-right:25px;"><img src="' . SITE_URL . __MAINPRODTHUMB__ . $viewImage . '" height="90px;" width="70px;" alt="image" /></div></li>';
                } else {
                    $comment = '';
                    $palletColor = $this->getColorPalle($get['qId']);
                    $html .= '<li>
                    <label>' . $langViewName . '</label>
                    <ul>                        
                        <li> 
                            <select id="' . $viewName . 'Design" name="' . $viewName . 'Design" size="1" style="width:auto;" disabled = true;>
                                <option value="0">'.LANG_CHOOSE_PRINT_EMBROIDERY.'</option>
                                ' . $this->getDecoType($get['basketId'], $viewId, '1') . '
                           </select>
                        </li>
                      <li>
                          <label>'.LANG_QUANTITY_OF_COLORS.':</label>
                          <div class="input-text">
                              <input type="text" name="' . $viewName . 'ColorQty" value="" onkeyup="enablePallet(\'' . $viewName . '\', this.value);" disabled = true; />
                          </div>
                      </li>
                      <li>
                          <label>'.LANG_PHOTO_PRINT.'</label>
                          <input type="checkbox" name="' . $viewName . 'PhotoPrint" disabled = true; />
                      </li>
                  </ul>
              </li>
              <li>
                <ul class="color-type">';
                    for ($i = 1; $i <= 8; $i++) {
                        $html .= '<li>
                             <label>'.LANG_COLOR. ' ' . $i . '</label>
                             <select name="' . $viewName . 'Color' . $i . '" id="' . $viewName . 'Color' . $i . '" size="1"  class="colorPallet chzn-select1" tabindex="2" data-placeholder="Pantone &reg">';
                        $html .= '<option value=""></option>';
                        $html .= '</select></li>';
                    }
                    $html .='
                 </ul>
               </li>
               <li>
                  <label>'.LANG_COMMENT.' on ' . $langViewName . ' deco:</label>
                  <textarea name="comment_' . $viewId . '" id="comment_' . $viewId . '" disabled="" rows="0" cols="0">' . $comment . '</textarea>';
                    $html .='<div style="float:right; margin-right:25px;"><img src="' . SITE_URL . __MAINPRODTHUMB__ . $viewImage . '" height="90px;" width="70px;" alt="image" /></div></li>';
                }
            }
            $jsView = implode(',', $viewNameArr);
            $isEditable = 0;
            if (count($editViewArr) > 0) {
                $isEditable = 1;
                $jsEditView = implode(',', $editViewArr['view']);
                $jsEditLen = implode(',', $editViewArr['length']);
            }
            $html .= '<script type="text/javascript">
                var popup = document.getElementById("quantityForm");                
                                    
                if(' . $isEditable . ' == 1) {
                    popup.load = editPallet("' . $jsEditView . '", "' . $jsEditLen . '", "' . $jsView . '");
                } else {
                    popup.load = disableAllPallet("' . $jsView . '");
                }
                
            </script>';
        }

        $html .='<li>                   
                <div class="login-button" style="margin-left:160px;">
                <input type="hidden" id="totalView" name="totalView" value="' . $totalView . '" />
                    <input type="submit" name="submit" value="'.LANG_ENTER.'">
                    <input type="button" name = "cancel" value="'.LANG_CANCEL.'" onClick="closeForm(\'' . $get['fId'] . '\');">
                </div>     
               </li>
               <li>
                    <div style="font-size:12px;">'.LANG_PANTONE_TRADEMARK.'</div>
               </li>
            </ul>
        </form>
    </div>';
        $html .='<script type="text/javascript" src="js/chosen.jquery.js"></script>
                <script type="text/javascript"> $(".chzn-select").chosen(); </script>';

        echo $html;
    }

    function setProductSize($post) {
        $sizeArr = array();
        $totalQty = 0;

        $isBasket = $this->fetchValue(TBL_BASKET_SIZE, 'id', " basketId = '" . $post['basketId'] . "'");
        if ($isBasket) {
            $this->executeQry("Delete from " . TBL_BASKET_SIZE . " where basketId = '" . $post['basketId'] . "'");
        }

        $rs = $this->executeQry("select sizeId from " . TBL_PRODUCTSIZE . " where pId = '" . $post['prodId'] . "'");

        $num = $this->getTotalRow($rs);
        while ($size = $this->getResultObject($rs)) {
            $preFix = "size_" . $size->sizeId;
            if ($post[$preFix] != '' && $post[$preFix] != '0') {
                $sizeArr[] = "('" . $post[basketId] . "', '" . $size->sizeId . "', '" . $post[$preFix] . "')";
                $totalQty += $post[$preFix];
            }
        }
        $queryVal = implode(', ', $sizeArr);
        if ($queryVal != '') {
            $querySize = "insert into " . TBL_BASKET_SIZE . " (basketId, sizeId, quantity) values " . $queryVal;
            $sizeResult = $this->executeQry($querySize);
            if ($sizeResult) {
                //$productCode = $this->getStyleCode($quality, $styleCode);                
                //$valuePoint = $this->getBasePrice($post['basketId']);
                $valuePoint = $this->getBasePrice($post['basketId']);
                //$valuePoint = $this->getPlainProductSurcharges($vp, $totalQty, $post['basketId']);
                $this->executeQry("update " . TBL_BASKET . " set qualityId = '" . $post[fQuality] . "', quantity = '" . $totalQty . "', base_price = '" . $valuePoint . "' where id = '" . $post['basketId'] . "'");
                $this->executeQry("update " . TBL_MAINPRODUCT . " set qualityId = '" . $post[fQuality] . "' where id='" . $post[mainProdId] . "'");
            }
        }
    }

    function getQuantityDetail($array1, $array2) {
        $temp1 = $temp2 = array();
        $boolean = 0;
        $index;
        $index1 = 0;
        $newIndex;

        for ($i = 0; $i < count($array1); $i++) {
            for ($j = 0; $j < count($temp1); $j++) {
                if ($temp1[$j] == $array1[$i]) {
                    $boolean = 1;
                }
            }
            if ($boolean == 1) {
                for ($k = 0; $k < count($array1); $k++) {
                    if ($array1[$k] == $array1[$i]) {
                        $index = $k;
                        break;
                    }
                }
                for ($l = 0; $l < count($temp1); $l++) {
                    if ($temp1[$l] == $array1[$i]) {
                        $newIndex = $l;
                    }
                }
                $temp2[$newIndex] = $temp2[$newIndex] + $array2[$i];
            } else {
                $temp1[$index1] = $array1[$i];
                $temp2[$index1] = $array2[$i];
                $index1++;
            }
            $boolean = 0;
        }
        $data['fabric'] = $temp1;
        $data['quantity'] = $temp2;
        return $data;
    }

    function getPlainProductSurcharges() {
        $result = $this->executeQry("select mainProdId, quantity from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' order by id");
        if ($this->num_rows($result) > 0) {
            while ($line = $this->fetch_object($result)) {
                $qualityId = $this->fetchValue(TBL_MAINPRODUCT, 'qualityId', ' id = "' . $line->mainProdId . '"');
                $fabricId[] = $this->fetchValue(TBL_FABRICCHARGE, 'fId', " id = '" . $qualityId . "'");
                $quantity[] = $line->quantity;
            }
        }
        return $this->getQuantityDetail($fabricId, $quantity);
    }

    function getDecoQuantitySurcharges() {
        $jsonObj = new JSON();
        $viewObj = (object) $viewObj;
        $result = $this->executeQry("select mainProdId, quantity from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' order by id");
        if ($this->num_rows($result) > 0) {
            while ($line = $this->fetch_object($result)) {
                $decoInfoObj = $this->fetchValue(TBL_MAINPRODUCT, 'decoInfo', ' id = "' . $line->mainProdId . '"');
                $decoInfo = $jsonObj->decode($decoInfoObj);
                foreach ($decoInfo as $decoObj) {
                    foreach ($decoObj->decoContentArray as $deco) {
                        $designId[] = $deco->designId;
                        $quantity[] = $line->quantity;
                    }
                }
            }
        }
        return $this->getQuantityDetail($designId, $quantity);
    }

    function getBasePrice($basketId) {
        $mainProdId = $this->fetchValue(TBL_BASKET, "mainProdId", " id = '" . $basketId . "'");
        $mainResult = $this->executeQry("select rawProdId, styleId, qualityId from " . TBL_MAINPRODUCT . " where id = '" . $mainProdId . "'");
        $mainObj = $this->fetch_object($mainResult);
        $totalQty = (int) $this->getProductQuantity($basketId);
        $styleId = $this->fetchValue(TBL_MAINPRODUCT, "styleId", " id = '" . $mainProdId . "'");
        if ($totalQty != 0) {
            $basePrice = $this->fetchValue(TBL_PRODUCT, "productPrice", " id = '" . $mainObj->rawProdId . "'");
            $qualityObj = $this->getVPByQuality($basePrice, $mainObj->qualityId);
            $qualityVP = $qualityObj['valuePoint'];
            $style = $this->getVPByStyle($basePrice, $mainObj->styleId);
            if ($style->sign == 2) {
                $valuePoint = $qualityVP + $style->valuePoint;
            } else if ($style->sign == 0) {
                $valuePoint = $qualityVP - $style->valuePoint;
            } else {
                $valuePoint = $qualityVP;
            }
        }
        return $valuePoint;
    }

    function getProductQuantity($basketId) {
        $sizeResult = $this->executeQry("select basketId, sum(quantity) as quantity from " . TBL_BASKET_SIZE . " where basketId = '" . $basketId . "'");
        $obj = $this->fetch_object($sizeResult);
        return $totalQty = $obj->quantity;
    }

    function getDecoDesignQuery($decoObj, $basketId) {
        $quantity = $this->fetchValue(TBL_BASKET, "quantity", " id = '" . $basketId . "'");
        $viewId = $decoObj->viewId;
        $query = '';
        foreach ($decoObj->decoContentArray as $deco) {
            $query .= ', ("' . $basketId . '", "' . $viewId . '", "' . $deco->designId . '", "' . $quantity . '", "' . $_SESSION[USER_ID] . '")';
        }
        return $query;
    }

    function setProductDeco($post) {
        $jsonObj = new JSON();
        //   $decoInfo = array();
        $colorArr = array();
        $decoQuery = '';
        $decoDesignQuery = '';
        $PhotoPrintId = $this->fetchValue(TBL_SYSTEMCONFIG, "id", "systemVal = '" . __PHOTOPRINT__ . "'");
        $mainProdId = $this->fetchValue(TBL_BASKET, "mainProdId", "id = '" . $post[basketId] . "'");
        $isBasket = $this->fetchValue(TBL_BASKET_COLOR, 'id', " basketId = '" . $post['basketId'] . "'");
        $isDeco = $this->fetchValue(TBL_BASKET_DECO, 'id', " basketId = '" . $post['basketId'] . "'");
        $isDecoQuantity = $this->fetchValue(TBL_BASKET_DECO_QTY, "quantity", " basketId = '" . $post['basketId'] . "'");
        if ($isBasket) {
            $this->executeQry("Delete from " . TBL_BASKET_COLOR . " where basketId = '" . $post['basketId'] . "'");
        }
        if ($isDeco) {
            $this->executeQry("Delete from " . TBL_BASKET_DECO . " where basketId = '" . $post['basketId'] . "'");
        }
        if ($isDecoQuantity) {
            $this->executeQry("Delete from " . TBL_BASKET_DECO_QTY . " where basketId = '" . $post['basketId'] . "'");
        }

        $decoResult = $this->executeQry("select dataArr, decoInfo from " . TBL_MAINPRODUCT . " where id = '" . $mainProdId . "'");
        $decoRow = $this->fetch_object($decoResult);
        $decoInfo = $jsonObj->decode($decoRow->decoInfo);
        $decoImages = $jsonObj->decode($decoRow->dataArr);
        $ctr = 0;

        foreach ($decoInfo as $decoObj) {
            $viewName = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $decoObj->viewId . "'");
            $viewName = str_replace(" ", "", $viewName);
            $cmt = "comment_" . $decoObj->viewId;
            $comment = $post[$cmt];
            $numberOfDeco = count($decoObj->decoContentArray);

            if ($numberOfDeco > 0) {
                $decoDesignQuery .= $this->getDecoDesignQuery($decoObj, $post['basketId']);
                if ($ctr == 0) {
                    $decoQuery .= $this->getDecoQuery($post, $decoObj, $viewName);
                } else {
                    $decoQuery .= ", " . $this->getDecoQuery($post, $decoObj, $viewName);
                }
                
                $isPhotoPrint = $viewName . "PhotoPrint";
                if (isset($post[$isPhotoPrint])) {
                    $colorArr[] = "('" . $post['basketId'] . "', '" . $decoObj->viewId . "', '', '', '" . $PhotoPrintId . "')";
                } else {
                    $decoType = $viewName . "Design";
                    $num = $viewName . "ColorQty";
                    $numColor = $post[$num];
                    $numColor = ($numColor < 9)?$numColor:8;
                    for ($i = 1; $i <= $numColor; $i++) {
                        $color = $viewName . "Color" . $i;
                        //$colorCode = $this->fetchValue(TBL_PALLETCOLOR, 'colorCode', "id = '".$post[$color]."'");                            
                        $colorCode = $this->fetchValue(TBL_PANTONE, 'colorName', "id = '" . $post[$color] . "'");
                        $colorArr[] = "('" . $post['basketId'] . "', '" . $decoObj->viewId . "', '" . $post[$color] . "', '" . $colorCode . "', '" . $post[$decoType] . "')";
                    }
                }
                $ctr++;
            }
        }

        $val = implode(',', $colorArr);
        if ($val != '') {
            $query = "insert into " . TBL_BASKET_COLOR . " (basketId, viewId, colorId, colorCode, type) values " . $val;
            $this->executeQry($query);
        }

        if ($decoQuery != '') {
            $dQuery = "insert into " . TBL_BASKET_DECO . " (basketId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values " . $decoQuery;
            $saveDeco = $this->executeQry($dQuery);

            //changes start here==================
            $decoQuantity = substr($decoDesignQuery, 1);
            $designQuery = "insert into " . TBL_BASKET_DECO_QTY . " (basketId, viewId, designId, quantity, userId) values " . $decoQuantity;
            $this->executeQry($designQuery);
            //changes end=========================

            if ($saveDeco) {
                $decoPriceObj = $this->getDecoUnitPrice($post['basketId']);

                //$_SESSION["decoPriceObj"] = $decoPriceObj;
                //changes 3May2013
//                $this->executeQry(" update ".TBL_BASKET." set decoPrice = '".$decoPriceObj->decoBasePrice."', decoSurcharge = '".$decoPriceObj->surcharge."', decoUnitPrice = '".$decoPriceObj->decoPrice."' where id = '".$post['basketId']."'");                
                $this->executeQry(" update " . TBL_BASKET . " set decoPrice = '" . $decoPriceObj->decoBasePrice . "', decoSurcharge = '', decoUnitPrice = '" . $decoPriceObj->decoPrice . "' where id = '" . $post['basketId'] . "'");
            }
        }
    }

//    function getDecoQuery($post, $decoObj, $viewName) {        
//        $area = 0;
//        $decoArr = array();
//        $cmt = "comment_".$decoObj->viewId;
//        $comment = $post[$cmt];
//     
//        $photoPrint = $viewName."PhotoPrint";
//        if($post[$photoPrint]) {
//            $colorQuantity = __PHOTOPRINTCOLOR__;
//            $decoType = $this->fetchValue(TBL_SYSTEMCONFIG, "id", "systemVal = '".__PHOTOPRINT__."'");
//        } else {
//            $qtyName = $viewName."ColorQty";
//            $colorQuantity = $post[$qtyName];
//            $typeName = $viewName."Design";
//            $decoType = $post[$typeName];
//        }
//        $quantity = $this->fetchValue(TBL_BASKET, "quantity", " id = '".$post['basketId']."'");
//        foreach($decoObj->decoContentArray as $obj) {
//            $area = $obj->heighCM * $obj->widthCM;
//            $valuePoint = $this->getDecoValuePoint($decoType, $area, $colorQuantity);
//            //changes 3May2013
////            $surchargeObj = $this->getDecoSurcharge($decoType, $valuePoint, $quantity);
////            $decoArr[] = "('".$post['basketId']."', '".$decoObj->viewId."', '".$comment."', '".$colorQuantity."', '".$decoType."', '".$area."', '".$valuePoint."', '".$surchargeObj->surcharge."', '".$surchargeObj->sign."')";
//
//            //start here==========
//            $decoArr[] = "('".$post['basketId']."', '".$decoObj->viewId."', '".$comment."', '".$colorQuantity."', '".$decoType."', '".$area."', '".$valuePoint."', '', '')";
//            //end=====---=========
//        }
//        $val = implode(', ', $decoArr);
//        return $val;
//    }
    //29July
    function getDecoQuery($post, $decoObj, $viewName) {
        $area = 0;
        $decoArr = array();
        $cmt = "comment_" . $decoObj->viewId;
        $comment = $post[$cmt];
        
        $photoPrint = $viewName . "PhotoPrint";
        if ($post[$photoPrint]) {
            $colorQuantity = __PHOTOPRINTCOLOR__;
            $decoType = $this->fetchValue(TBL_SYSTEMCONFIG, "id", "systemVal = '" . __PHOTOPRINT__ . "'");
        } else {
            $qtyName = $viewName . "ColorQty";
            $colorQuantity = $post[$qtyName];
            $colorQuantity = ($colorQuantity < 9)?$colorQuantity:8;
            $typeName = $viewName . "Design";
            $decoType = $post[$typeName];
        }
        foreach ($decoObj->decoContentArray as $obj) {
            $area = $obj->heighCM * $obj->widthCM;
            $valuePoint = $this->getDecoValuePoint($decoType, $area, $colorQuantity);
            $decoArr[] = "('" . $post['basketId'] . "', '" . $decoObj->viewId . "', '" . $comment . "', '" . $colorQuantity . "', '" . $decoType . "', '" . $area . "', '" . $valuePoint . "', '', '')";
        }
        $val = implode(', ', $decoArr);
        return $val;
    }

    function getDecoUnitPrice($basketId) {
        $obj = (object) $obj;
        $price = $surcharge = 0;
        $sql = "select price, surcharge, sign from " . TBL_BASKET_DECO . " where basketId = '" . $basketId . "'";
        $resultSet = $this->executeQry($sql);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($line = $this->fetch_object($resultSet)) {
                $price += $line->price;
                if ($line->sign == 0) {
                    $surcharge -= $line->surcharge;
                } else if ($line->sign == 2) {
                    $surcharge += $line->surcharge;
                }
            }
        }
        $decoUnitPrice = $price + $surcharge;
        $surcharge = str_replace("-", "", $surcharge);
        $obj->surcharge = $surcharge;
        $obj->decoBasePrice = $price;
        $obj->decoPrice = $decoUnitPrice;
        return $obj;
    }

    //delete this code 4May2013
    function getBasketDecoSurcharge($basketId) {
        $surcharge = 0;
        $query = "select surcharge, sign from " . TBL_BASKET_DECO . " where basketId = '" . $basketId . "'";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($line = $this->fetch_object($resultSet)) {
                if ($line->sign == 0) {
                    $surcharge -= $line->surcharge;
                } else if ($line->sign == 2) {
                    $surcharge += $line->surcharge;
                }
            }
            //$surcharge = str_replace("-", "", $surcharge);
        }
        return $surcharge;
    }

    function getDecoSurcharge($decoType, $price, $quantity) {
        $obj = (object) $obj;
        $surcharge = 0;
        $sign = 1;
        if ($decoType == 103) {
            $query = "select * from " . TBL_EMBROIDERY_DISCOUNT . " where type = '1' and pcs <= '" . $quantity . "' order by pcs desc limit 1";
            $resultSet = $this->executeQry($query);
            if ($resultSet) {
                $result = $this->fetch_object($resultSet);
                $sign = $result->sign;
                $surcharge = ($price * $result->charge) / 100;
            }
        } else if ($decoType == 104) {
            $query = "select * from " . TBL_EMBROIDERY_DISCOUNT . " where type = '0' and pcs <= '" . $quantity . "' order by pcs desc limit 1";
            $resultSet = $this->executeQry($query);
            if ($resultSet > 0) {
                $result = $this->fetch_object($resultSet);
                $sign = $result->sign;
                $surcharge = ($price * $result->charge) / 100;
            }
        }
        $obj->sign = $sign;
        $obj->surcharge = $surcharge;
        return $obj;
    }

    function getDecoValuePoint($decoType, $area, $colorQty) {
        $decoVP = 0;
        if ($decoType == 103) {
            $valuePoint = $this->fetchValue(TBL_PRINTING_COLOR, "valuePoint", "color = '" . $colorQty . "'");
            $minArea = $this->fetchValue(TBL_EMBROIDERY_RATE, "min_cm", " type = '1'");
            if ($minArea > $area) {
                $decoVP = $minArea * $valuePoint;
            } else {
                $decoVP = $area * $valuePoint;
            }
            return $decoVP;
        } else if ($decoType == 104) {
            $resultSet = $this->executeQry("select min_cm, valuePoint from " . TBL_EMBROIDERY_RATE . " where type = '0'");
            $result = $this->fetch_object($resultSet);
            if ($result->min_cm > $area) {
                $decoVP = $result->min_cm * $result->valuePoint;
            } else {
                $decoVP = $area * $result->valuePoint;
            }
            return $decoVP;
        } else {
            $colorQty = __PHOTOPRINTCOLOR__;
            $valuePoint = $this->fetchValue(TBL_PRINTING_COLOR, "valuePoint", "color = '" . $colorQty . "'");
            $minArea = $this->fetchValue(TBL_EMBROIDERY_RATE, "min_cm", " type = '1'");
            if ($minArea > $area) {
                $decoVP = $minArea * $valuePoint;
            } else {
                $decoVP = $area * $valuePoint;
            }
            return $decoVP;
        }
    }

    function availableSize($pId, $basketId, $op) {
        $sizeId = array();
        $rs = $this->executeQry("select sizeId from " . TBL_PRODUCTSIZE . " where pId = '" . $pId . "'");
        while ($size = $this->getResultObject($rs)) {
            $sizeId[] = $size->sizeId;
        }

        $strSize = implode(',', $sizeId);
        $rs1 = $this->executeQry("select distinct groupId from " . TBL_SIZE . " where id in (" . $strSize . ") order by groupId");

        $num = $this->getTotalRow($rs1);
        if ($num > 0) {
            while ($row = $this->getResultObject($rs1)) {
                $sizeCategory = $this->fetchValue(TBL_SIZE_GROUPDESC, 'groupName', 'id = "' . $row->groupId . '" and langId = "' . $_SESSION[DEFAULTLANGUAGEID] . '"');
                $html .= '<div class="sizecategory" id="' . $row->groupId . '">
                             <small>' . $sizeCategory . '</small>';
                $query = "select distinct(s.id) as sizeId, ps.id, sd.sizeName from " . TBL_PRODUCTSIZE . " as ps inner join " . TBL_SIZE . " as s inner join " . TBL_SIZEDESC . " as sd on ps.sizeId = s.id and sd.Id = s.id and sd.Id = ps.sizeId and ps.pId = '" . $pId . "' and s.groupId = '" . $row->groupId . "' order by s.sequence asc";

                $result = $this->executeQry($query);
                while ($line = $this->getResultObject($result)) {
                    $val = '';
                    if ($op == '0') {
                        $sizeValue = $this->fetchValue(TBL_BASKET_SIZE, "quantity", " basketId = '" . $basketId . "' and sizeId ='" . $line->sizeId . "'");
                        $val = $sizeValue;
                    }
                    $html .= '<ul>
                            <li>
                                <label>' . $line->sizeName . ':</label>
                                <div class="input-text">
                                    <input type="text" name="size_' . $line->sizeId . '" value="' . $val . '" class="quantity" />
                                </div>
                            </li>
                        </ul>';
                }
                $html .= '</div>';
            }
        }
        return $html;
    }

    function getFabricQualityByStyleId($sId, $qId) {
        $html = '';
        $selected = '';

        $result = $this->executeQry("select fId from " . TBL_PRODUCT_FABRIC . " where sId ='" . $sId . "' order by fId");
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            while ($line = $this->getResultObject($result)) {
                if ($qId == $line->fId) {
                    $sel = 'selected = "selected"';
                } else {
                    $sel = '';
                }

                $html .= '<option value="' . $line->fId . '" ' . $sel . '>' . $this->fetchValue(TBL_FABRICCHARGE, "fQuality", "id='" . $line->fId . "'") . '</option>';
            }
        }
        return $html;
    }

    function getColorPalle($id = '') {
        $palletColor = '';
        $result = $this->query("select * from " . TBL_PANTONE);
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            while ($row = $this->getResultObject($result)) {
                $sel = '';
                $et = '';
                if ($id == $row->id) {
                    $sel = 'selected = "selected"';
                }
                $palletColor .= '<option value="' . $row->id . '" ' . $sel . ' ' . $et . ' colorCode="' . $row->colorCode . '">' . $row->colorName . '<sup> &reg;</sup></option>';

                //$palletColor .= '<div class="chsn-color selected">'.$selectedText.'</div>';
            }
        }
        return $palletColor;
    }

    function getColorCode($id) {
        $palletColor = '';
        $selectedText = $this->fetchValue(TBL_PANTONE, "colorCode", " id = '" . $id . "'");
        $palletColor .= '<div id="colorCover" class="chsn-color selected" style="background-color:' . $selectedText . '">&nbsp;</div>';
        return $palletColor;
    }

    function getEditColor($id) {
        $palletColor = '';
        $result = $this->query("select * from " . TBL_PANTONE);
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            while ($row = $this->getResultObject($result)) {
                $sel = '';
                if ($id == $row->id) {
                    $sel = 'selected = "selected"';
                }
                $palletColor .= '<option value="' . $row->id . '" ' . $sel . '>' . $row->colorName . '</option>';
            }
        }
        return $palletColor;
    }

    function getPalletColor($fId, $basketId, $viewId, $colorId) {
        $palletColor = '';
        $pId = $this->fetchValue(TBL_FABRICCHARGE, 'palletId', " id = '" . $fId . "'");
        $result = $this->query("select id, colorCode from " . TBL_PALLETCOLOR . " where palletId = '" . $pId . "' and colorCode !=''");

        $num = $this->getTotalRow($result);
        if ($num > 0) {
            $palletColor .='<option value="">Select Color</option>';
            while ($row = $this->getResultObject($result)) {
                $sel = '';
                if ($colorId == $row->id) {
                    $sel = 'selected = "selected"';
                }
                $palletColor .= '<option ' . $sel . ' value="' . $row->id . '" style="background-color:' . $row->colorCode . '">' . $row->colorCode . '</option>';
            }
        }
        return $palletColor;
    }

    //may not use========
    function getPrintDecoVP($decoArr, $numberOfColor, $quantity) {
        $vp = (object) $vp;
        $decoVP = array();
        $valuePoint = $this->fetchValue(TBL_PRINTING_COLOR, "valuePoint", "color = '" . $numberOfColor . "'");
        $minArea = $this->fetchValue(TBL_EMBROIDERY_RATE, "min_cm", " type = '1'");
        $decoArea = 0;
        $totalDecoArea = 0;

        foreach ($decoArr as $deco) {
            $decoArea = $deco->heighCM * $deco->widthCM;
            if ($minArea > $decoArea) {
                $decoVP[] = $minArea * $valuePoint;
            } else {
                $decoVP[] = $decoArea * $valuePoint;
            }
        }

        $numberOfDeco = count($decoVP);
        $valuePoint = array_sum($decoVP);
        $number = $numberOfDeco * $quantity;
        $vp->numberOfDeco = $numberOfDeco;
        $vp->valuePoint = $valuePoint;
        $resultSet = $this->executeQry("select * from " . TBL_EMBROIDERY_DISCOUNT . " where pcs <= '" . $number . "' and type = '1' order by pcs desc limit 1");
        $result = $this->getResultObject($resultSet);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $percent = $result->charge;
            $sign = (int) $result->sign;
            if ($sign == 0) {
                $charge = ($valuePoint * $percent) / 100;
                $vp->valuePoint = $valuePoint - $charge;
            } else if ($sign == 2) {
                $charge = ($valuePoint * $percent) / 100;
                $vp->valuePoint = $valuePoint + $charge;
            }
        }
        return $vp;
    }

    //may not use============
    function getEmbroideryDecoVP($decoArr, $quantity) {
        $vp = (object) $vp;
        $decoVP = array();
        $decoArea = 0;

        $resultSet = $this->executeQry("select min_cm, valuePoint from " . TBL_EMBROIDERY_RATE . " where type = '0'");
        $result = $this->fetch_object($resultSet);
        foreach ($decoArr as $deco) {
            $decoArea = $deco->heighCM * $deco->widthCM;
            if ($result->min_cm > $decoArea) {
                $decoVP[] = $result->min_cm * $result->valuePoint;
            } else {
                $decoVP[] = $decoArea * $result->valuePoint;
            }
        }
        $numberOfDeco = count($decoVP);
        $valuePoint = array_sum($decoVP);
        $number = $numberOfDeco * $quantity;
        $vp->numberOfDeco = $numberOfDeco;
        $vp->valuePoint = $valuePoint;
        $resultSet = $this->executeQry("select * from " . TBL_EMBROIDERY_DISCOUNT . " where pcs <= '" . $number . "' and type = '0' order by pcs desc limit 1");
        $result = $this->getResultObject($resultSet);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $percent = $result->charge;
            $sign = (int) $result->sign;
            if ($sign == 0) {
                $charge = ($valuePoint * $percent) / 100;
                $vp->valuePoint = $valuePoint - $charge;
            } else if ($sign == 2) {
                $charge = ($valuePoint * $percent) / 100;
                $vp->valuePoint = $valuePoint + $charge;
            }
        }
        return $vp;
    }

    //may not use=========
    function getPhotoPrintDecoVP($decoArr) {
        $vp = (object) $vp;
        $decoVP = array();
        $valuePoint = $this->fetchValue(TBL_PRINTING_COLOR, "valuePoint", "color = '" . __PHOTOPRINTCOLOR__ . "'");
        $minArea = $this->fetchValue(TBL_EMBROIDERY_RATE, "min_cm", " type = '1'");
        $decoArea = 0;

        foreach ($decoArr as $deco) {
            $decoArea = $deco->heighCM * $deco->widthCM;
            if ($minArea > $decoArea) {
                $decoVP[] = $minArea * $valuePoint;
            } else {
                $decoVP[] = $decoArea * $valuePoint;
            }
        }
        $vp->valuePoint = array_sum($decoVP);
        $vp->numberOfDeco = count($decoVP);
        return $vp;
    }

    //2May2013
//    function getDecoView($basketId, $mainProdId, $totalQty) {
//        $jsonObj = new JSON();        
//        $viewObj = (object)$viewObj;
//        $decoPrice = 0;
//        $totalDecoPrice = 0;
//        $en = '';
//        $dis = '';
//        $totalDeco = 0;
//
//        $en .= '<tr>
//                  <td>Product-Deco</td>
//                  <td></td>
//                  <td></td>
//                </tr>';
//        $decoResult = $this->executeQry("select decoInfo from ".TBL_MAINPRODUCT." where id = '".$mainProdId."'");
//         $num = $this->num_rows($decoResult);
//         if($num > 0) {
//            $decoRow = $this->fetch_object($decoResult);            
//            $decoInfo = $jsonObj->decode($decoRow->decoInfo);
//            foreach($decoInfo as $decoObj) {                
//                $viewId = $decoObj->viewId;
//                $viewName = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '".$viewId."'");
//                $numberOfDeco = count($decoObj->decoContentArray);  //Total number of deco on a view
//                $totalDeco += $numberOfDeco;                        //Total number of deco on a product
//                
//                if($numberOfDeco > 0) {
//                    $colorCode = '';
//                    $typeId = '';
//                    $numberOfColor = 0;
//                    $ctr = 1;
//                    
//                    $colorResult = $this->executeQry("select id, type, colorCode from ".TBL_BASKET_COLOR." where basketId = '".$basketId."' and viewId ='".$viewId."'");
//                    while($colorRow = $this->fetch_object($colorResult)) {
//                        if($ctr != 1) {
//                            $colorCode .= ", ";
//                        }
//                        $colorCode .= $colorRow->colorCode;
//                        $typeId = $colorRow->type;
//                        $numberOfColor++;
//                        $ctr++;
//                    }
//                    $decoVP = $this->getDecoVPByViewId($basketId, $viewId);
//                    $customDuty = $this->addCustomDuty($decoVP->valuePoint);
//                    $valuePoint = $decoVP->valuePoint + $customDuty;
//                    
//                    if($totalQty == 0) {
//                        $price = $this->getPriceSign(0);
//                        $totalPrice = $price;
//                    } else {
//                        $price = $this->getProductPrice($valuePoint);
//                        $totalPrice = $this->getProductPrice($valuePoint, $totalQty);
//                    }
//                    
//                                        
//                    
//                    $en .='<tr>
//                        <td>
//                           '.$viewName.': '.$typeName.' // Deco '.$decoVP->numberOfDeco.' // Quantity of Colors of Design &gt; : '.$decoVP->numberOfColor.' // Colors : '.$colorCode.'
//                        </td>
//                      </tr>';
//                    
//                    $en .= '<tr>
//                            <td>
//                       Quantity of Deco '.$decoVP->numberOfDeco.' in Collection: '.$totalQty.' // Value-Points per Deco '.$decoVP->valuePoint.'
//                       </td>
//                       <td>'.$price.'</td>
//                       <td>'.$totalPrice.'</td>
//                       </tr>';
//                } else {
//                    $dis .= '<tr>
//                              <td>
//                                '.$viewName.': X // Colors of Design on > 0
//                              </td>
//                            </tr>';
//                }
//            }
//         }
//         
//         $viewObj->en = $en;
//         $viewObj->dis = $dis;
//         $viewObj->totalDeco = $totalDeco;
//         return $viewObj;
//    }
//    function getDecoVPByViewId($basketId, $decoObj, $designArr) {        
//        $obj = (object)$obj;
//        $id = $price = $type = array();
//        $totalSurcharge = $surcharge = 0;
//        $totalPrice = $excSurchargePrice = 0;
//        $ctr = 0;
//        $num = count($decoObj->decoContentArray);
//        $priceResult = $this->executeQry("select id, price, type from ".TBL_BASKET_DECO." where basketId = '".$basketId."' and viewId = '".$decoObj->viewId."' order by id");
//        if($this->num_rows($priceResult) > 0) {
//            while($line = $this->fetch_object($priceResult)) {
//                $id[] = $line->id;
//                $price[] = $line->price;
//                $type = $line->type;
//            }
//        }
//        
//        $type = ($type == 103)?1:0;                         //constant value may be some error.
//        foreach($decoObj->decoContentArray as $deco) {
//            $excSurchargePrice += $price[$ctr];
//            $quantity = $designArr[$deco->designId];
//            
////            $surchargeObj = $this->executeQry("select charge, sign from ".TBL_EMBROIDERY_DISCOUNT." where pcs <= '".$quantity."' and type = '".$type."' order by pcs asc limit 1");
//            $surchargeObj = $this->executeQry("select charge, sign from ".TBL_EMBROIDERY_DISCOUNT." where pcs <= '".$quantity."' and type = '".$type."' order by pcs desc limit 1");
//            if($this->num_rows($surchargeObj) > 0) {                
//                $row = $this->fetch_object($surchargeObj);                
//                $surcharge = ($price[$ctr] * $row->charge)/100;
//                if($row->sign == 0) {
//                    $totalPrice += $price[$ctr] - $surcharge;
//                    $totalSurcharge -= $surcharge; 
//                } else if($row->sign == 2) {
//                    $totalPrice += $price[$ctr] + $surcharge;                    
//                    $totalSurcharge += $surcharge;
//                }else {
//                    $totalPrice += $price[$ctr];
//                }
//                $this->executeQry("update ".TBL_BASKET_DECO." set surcharge = '".$surcharge."', sign = '".$row->sign."' where id = '".$id[$ctr]."'");            
//            } else if($quantity) {
//                $minSurchargeObj = $this->executeQry("select charge, sign from ".TBL_EMBROIDERY_DISCOUNT." where type = '".$type."' order by pcs asc limit 1");
//                echo 1;
//                $row = $this->fetch_object($minSurchargeObj);                
//                $surcharge = ($price[$ctr] * $row->charge)/100;
//                if($row->sign == 0) {
//                    $totalPrice += $price[$ctr] - $surcharge;
//                    $totalSurcharge -= $surcharge; 
//                } else if($row->sign == 2) {
//                    $totalPrice += $price[$ctr] + $surcharge;                    
//                    $totalSurcharge += $surcharge;
//                }else {
//                    $totalPrice += $price[$ctr];
//                }
//                $this->executeQry("update ".TBL_BASKET_DECO." set surcharge = '".$surcharge."', sign = '".$row->sign."' where id = '".$id[$ctr]."'");                
//            } else {
//                $totalPrice = $price[$ctr];
//            } 
//            $ctr++;
//        }
//        
//        $decoPrice = $this->fetchValue(TBL_BASKET, "decoPrice", " id = '".$basketId."'");        
//        $decoUnitPrice = $decoPrice + $totalSurcharge;
//        $totalSur = str_replace("-", "", $totalSurcharge);
//        $obj->totalSurcharge = $totalSurcharge;
//        $obj->numberOfDeco = $num;
//        $obj->valuePoint = $totalPrice;
//        $obj->excSurcharge = $excSurchargePrice;        
//        return $obj;
//    }

    function getDecoVPByViewId($basketId, $decoObj, $designArr) {
        $obj = (object) $obj;
        $id = $price = $type = array();
        $totalSurcharge = $surcharge = 0;
        $totalPrice = $excSurchargePrice = 0;
        $ctr = 0;
        $num = count($decoObj->decoContentArray);
        $priceResult = $this->executeQry("select id, price, type from " . TBL_BASKET_DECO . " where basketId = '" . $basketId . "' and viewId = '" . $decoObj->viewId . "' order by id");
        if ($this->num_rows($priceResult) > 0) {
            while ($line = $this->fetch_object($priceResult)) {
                $id[] = $line->id;
                $price[] = $line->price;
                $type = $line->type;
            }
        }

//        echo '<pre>';
//        print_r($decoObj);
        //$type = ($type == 103) ? 1 : 0;                         //constant value may be some error.
        $type = ($type == 104) ? 0 : 1;
        foreach ($decoObj->decoContentArray as $deco) {            
            $excSurchargePrice += $price[$ctr];            
            $quantity = $designArr[$deco->designId];                        

//            $surchargeObj = $this->executeQry("select charge, sign from ".TBL_EMBROIDERY_DISCOUNT." where pcs <= '".$quantity."' and type = '".$type."' order by pcs asc limit 1");
            if ($quantity) {
                $surchargeObj = $this->executeQry("select charge, sign from " . TBL_EMBROIDERY_DISCOUNT . " where pcs <= '" . $quantity . "' and type = '" . $type . "' order by pcs desc limit 1");
                
                if ($this->num_rows($surchargeObj) > 0) {
                    //echo "select charge, sign from " . TBL_EMBROIDERY_DISCOUNT . " where pcs <= '" . $quantity . "' and type = '" . $type . "' order by pcs desc limit 1";
                    $row = $this->fetch_object($surchargeObj);
                } else {
                    $minSurchargeObj = $this->executeQry("select charge, sign from " . TBL_EMBROIDERY_DISCOUNT . " where type = '" . $type . "' order by pcs asc limit 1");
                    $row = $this->fetch_object($minSurchargeObj);
                }
                                
                $surcharge = ($price[$ctr] * $row->charge) / 100;
                if ($row->sign == 0) {
                    $totalPrice += $price[$ctr] - $surcharge;
                    $totalSurcharge -= $surcharge;
                } else if ($row->sign == 2) {
                    $totalPrice += $price[$ctr] + $surcharge;
                    $totalSurcharge += $surcharge;
                } else {
                    $totalPrice += $price[$ctr];
                }
                $this->executeQry("update " . TBL_BASKET_DECO . " set surcharge = '" . $surcharge . "', sign = '" . $row->sign . "' where id = '" . $id[$ctr] . "'");
            } else {
                $totalPrice += $price[$ctr];
            }
            $ctr++;
        }
        $decoPrice = $this->fetchValue(TBL_BASKET, "decoPrice", " id = '" . $basketId . "'");
        $decoUnitPrice = $decoPrice + $totalSurcharge;
        $totalSur = str_replace("-", "", $totalSurcharge);
        $obj->totalSurcharge = $totalSurcharge;
        $obj->numberOfDeco = $num;
        $obj->valuePoint = $totalPrice;
        $obj->excSurcharge = $excSurchargePrice;

        return $obj;
    }

    function getDecoView($basketId, $mainProdId, $totalQty) {
        include('setlanguage.php');        
        $jsonObj = new JSON();
        $viewObj = (object) $viewObj;
        $en = '';
        $dis = '';
        $totalDeco = 0;

        $en .= '<tr>
                  <td>'.LANG_PRODUCT_DECO.'</td>
                  <td></td>
                  <td></td>
                </tr>';
        $decoDesignArr = array();
        $decoQuantityResult = $this->executeQry("select designId, sum(quantity) as quantity  from " . TBL_BASKET_DECO_QTY . " where userId = '" . $_SESSION[USER_ID] . "' group by designId");
        
//        $decoQuantityResult = $this->executeQry("select designId, sum(quantity) as quantity  from " . TBL_BASKET_DECO_QTY . " where basketId = '" . $basketId . "' group by designId");

        if ($this->num_rows($decoQuantityResult) > 0) {
            while ($designLine = $this->fetch_object($decoQuantityResult)) {
                $decoDesignArr[$designLine->designId] = $designLine->quantity;
            }
        }
        
        $decoResult = $this->executeQry("select decoInfo from " . TBL_MAINPRODUCT . " where id = '" . $mainProdId . "'");
        $num = $this->num_rows($decoResult);
        $returnprice = "";
        if ($num > 0) {
            $decoRow = $this->fetch_object($decoResult);
            $decoInfo = $jsonObj->decode($decoRow->decoInfo);
            foreach ($decoInfo as $decoObj) {
                $viewId = $decoObj->viewId;
                $viewName = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $viewId . "' and langId = '".$_SESSION[DEFAULTLANGUAGEID]."'");
                $numberOfDeco = count($decoObj->decoContentArray);  //Total number of deco on a view               
                $totalDeco += $numberOfDeco;                        //Total number of deco on a product
                if ($numberOfDeco > 0) {
                    $colorCode = '';
                    $typeId = '';
                    $numberOfColor = 0;
                    $ctr = 1;
                    $colorResult = $this->executeQry("select id, type, colorCode from " . TBL_BASKET_COLOR . " where basketId = '" . $basketId . "' and viewId ='" . $viewId . "'");
                    while ($colorRow = $this->fetch_object($colorResult)) {
                        if ($ctr != 1) {
                            $colorCode .= ", ";
                        }
                        $colorCode .= $colorRow->colorCode;
                        $typeId = $colorRow->type;
                        $numberOfColor++;
                        $ctr++;
                    }
                    //$decoVP = $this->getDecoVPByViewId($basketId, $viewId);                    

                    $decoVP = $this->getDecoVPByViewId($basketId, $decoObj, $decoDesignArr);                    
                    $customDuty = $this->addCustomDuty($decoVP->valuePoint);                    
                    $valuePoint = $decoVP->valuePoint + $customDuty;
                    
                    if ($totalQty == 0) {
                        $price = $this->getPriceSign(0);
                        $totalPrice = $price;
                        $returnprice = $returnprice+0;
                    } else {
                        $price = $this->getProductPrice($valuePoint);
                        $totalPrice = $this->getProductPrice($valuePoint, $totalQty);
                        $returnprice = $returnprice+$this->getPrice($valuePoint, $totalQty);
                        
                    }

                    $en .='<tr>
                        <td>
                           ' . $viewName . ': ' . $typeName . ' // '. LANG_DECO.' ' . $decoVP->numberOfDeco . ' // '. LANG_QUANTITY_OF_COLORS_OF_DESIGN.' &gt; : ' . $numberOfColor . ' // '. LANG_COLOR.' ' .  $colorCode . '
                        </td>
                      </tr>';

                    $en .= '<tr>
                            <td> ' .LANG_QUANTITY_OF_DECO.' : '.$decoVP->numberOfDeco . LANG_IN_COLLECTION .' : ' . $totalQty . ' //  ' .LANG_VALUE_POINTS_PER_DECO.' '.$decoVP->excSurcharge . '
                       </td>
                       <td>' . $price . '</td>
                       <td>' . $totalPrice . '</td>
                       </tr>';
                } else {
                    $dis .= '<tr>
                              <td>
                              ' . $viewName . ': X // '.LANG_QUANTITY_OF_COLORS_OF_DESIGN.' > 0
                              </td>
                            </tr>';
                }
               
            }
        }
        $viewObj->returnprice = $returnprice;
        $viewObj->en = $en;
        $viewObj->dis = $dis;
        $viewObj->totalDeco = $totalDeco;
        $viewObj->totalSurcharge = $decoVP->totalSurcharge;

        return $viewObj;
    }

    function getDecoQuantity($basketId) {
        $resultSet = $this->executeQry("select sum(quantity) as quantity from " . TBL_BASKET_SIZE . " where basketId = '" . $basketId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $result = $this->fetch_object($resultSet);
            return $result->quantity;
        }
    }

    //3May2013
//    function getDecoVPByViewId($basketId, $viewId) {
//        $obj = (object)$obj;
//        $ctr = 0;
//        $price = 0;
//        $surcharge = 0;
//        $valuePoint = 0;
//        
//        $resultSet = $this->executeQry("select viewId, price, surcharge, sign from ".TBL_BASKET_DECO." where basketId = '".$basketId."' and viewId = '".$viewId."'");
//        $num = $this->num_rows($resultSet);
//        if($num > 0) {
//            while($line = $this->fetch_object($resultSet)) {
//                $price += $line->price;
//                if($line->sign == 0) {
//                    $surcharge -= $line->surcharge;
//                } else if($line->sign == 2) {
//                    $surcharge += $line->surcharge;
//                }
//                $ctr++;                
//            }            
//        }
//        $valuePoint = $price + $surcharge;
//        $obj->numberOfDeco = $ctr;
//        $obj->valuePoint = $valuePoint;
//        return $obj;
//    }
//    function getDecoVPByViewId($basketId, $viewId) {
//        $obj = (object)$obj;
//        $ctr = 0;
//        $price = 0;
//        $surcharge = 0;
//        $valuePoint = 0;
//        
//        $resultSet = $this->executeQry("select viewId, price, surcharge, sign from ".TBL_BASKET_DECO." where basketId = '".$basketId."' and viewId = '".$viewId."'");
//        $num = $this->num_rows($resultSet);
//        if($num > 0) {
//            while($line = $this->fetch_object($resultSet)) {
//                $price += $line->price;
//                if($line->sign == 0) {
//                    $surcharge -= $line->surcharge;
//                } else if($line->sign == 2) {
//                    $surcharge += $line->surcharge;
//                }
//                $ctr++;                
//            }            
//        }
//        $valuePoint = $price + $surcharge;
//        $obj->numberOfDeco = $ctr;
//        $obj->valuePoint = $valuePoint;
//        return $obj;
//    }

    function getDeliverySurcharge($price, $quantity) {
        $obj = (object) $obj;
        $html = '';
        $surcharge = 0.00;

        $defaultDId = $this->fetchValue(TBL_DELIVERY_TIME_CHARGES, "d_id", " sign = '1'");
        $defaultDays = $this->fetchValue(TBL_DELIVERY_TIME, "days", "id = '" . $defaultDId . "'");
        $query = "select * from " . TBL_DELIVERY_TIME . " as dt inner join " . TBL_DELIVERY_TIME_CHARGES . " as dc on dt.id = dc.d_id and dt.id = '" . $_SESSION[DELIVERYTIME] . "'";
        $reslutSet = $this->executeQry($query);
        $delivery = $this->fetch_object($reslutSet);
        if ($defaultDId == $delivery->d_id || $quantity == 0) {
            $html .= '<tr>                        
                <td style="width:520px">'.LANG_SURCHARGE_FOR_OTHER_THAN .' '. $defaultDays .' '.LANG_DAYS_DELIVERY_TIME. ' </td>
                <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge) . '</td>
                <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge) . '</td>
            </tr>';
            $total = $price;
        } else {
            $percent = $delivery->charge;
            $sign = (int) $delivery->sign;
            $surcharge = ($price * $percent) / 100;
            $customDuty = $this->addCustomDuty($surcharge);
            $surcharge += $customDuty;
            if ($sign == 0) {
                $mark = '- ';
            }

            $html .= '<tr>
                 <td style="width:520px">'.LANG_SURCHARGE_FOR_OTHER_THAN .' '. $defaultDays .' '.LANG_DAYS_DELIVERY_TIME. '</td>
                 <td style="width:60px;" class="price">' . $mark . $this->getProductPrice($surcharge) . '</td>
                 <td style="width:60px;" class="price">' . $mark . $this->getProductPrice($surcharge, $quantity) . '</td>
             </tr>';
        }

        $obj->html = $html;
        $obj->surcharge = $surcharge;
        $obj->surchargetotal = $this->getPrice($surcharge, $quantity);
        $obj->sign = $sign;

        return $obj;
    }

    function getDeliveryTimeSurcharge($basePrice) {
        $surcharge = 0.00;
        $defaultDId = $this->fetchValue(TBL_DELIVERY_TIME_CHARGES, "d_id", " sign = '1'");
        $query = "select * from " . TBL_DELIVERY_TIME . " as dt inner join " . TBL_DELIVERY_TIME_CHARGES . " as dc on dt.id = dc.d_id and dt.id = '" . $_SESSION[DELIVERYTIME] . "'";
        $reslutSet = $this->executeQry($query);
        $delivery = $this->fetch_object($reslutSet);

        if ($defaultDId != $delivery->d_id) {
            $percent = $delivery->charge;
            $sign = (int) $delivery->sign;
            $surcharge = ($basePrice * $percent) / 100;
        }
        return $surcharge;
    }

    //13May2013
//    function quantitySurcharge($price, $basketId) {
//        
//        $obj = (object)$obj;
//        $surcharge = 0.00;
//        $totalQuantity = 0;
//        $qtyResult = $this->executeQry("select sum(quantity) as totalQty from ".TBL_BASKET." where userId = {$_SESSION[USER_ID]}");
//        
//        $totalQuantity = $this->fetch_object($qtyResult)->totalQty;        
//        $quantity = $this->fetchValue(TBL_BASKET, "quantity", " id = '".$basketId."'");
//        if($totalQuantity != 0) {
//            $percent = round(($quantity * 100) / $totalQuantity);
//        }else {
//            $percent = 0;
//        }
//        if($percent < __QUANTITYSURCHARGE__) {
//            $charge = $this->fetchValue(TBL_QUANTITY_SURCHARGE, "charge", " quantity >= '".$percent."' order by quantity asc limit 1");
//            if($charge) {
//                $src = ($price * $charge)/100;
//            }
//        }
//        
//        $qtyCustom = $this->addCustomDuty($src);
//        $surcharge = $qtyCustom + $src;
//        if($quantity == 0) {
//            $html .= '<tr>
//                <td style="width:520px">Surcharge for less then 25% of Totalquantity</td>
//                <td style="width:60px;" class="price">'.$this->getProductPrice($surcharge).'</td>
//                <td style="width:60px;" class="price">'.$this->getProductPrice($surcharge).'</td>
//            </tr>';
//        } else {
//            $html .= '<tr>
//                    <td style="width:520px">Surcharge for less then 25% of Totalquantity</td>
//                    <td style="width:60px;" class="price">'.$this->getProductPrice($surcharge).'</td>
//                    <td style="width:60px;" class="price">'.$this->getProductPrice($surcharge, $quantity).'</td>
//                </tr>';
//        }
//        $obj->surcharge = $src;
//        $obj->qtySurcharge = $surcharge;
//        $obj->html = $html;
//        return $obj;
//    }

    function priceToValuePoint($price, $currencyId = 0) {        
        if (!$currencyId) {
            $currencyId = $_SESSION['DEFAULTCURRENCYID'];
        }        
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $currencyId . "'");
        if($price != 0) {
            $valuePoint = $price / $currencyValue;
            return $valuePoint;
        } else {
            return $price;            
        }
    }
    
    function valuePointToPrice($valuePoint, $currencyId = 0) {
        if (!$currencyId) {
            $currencyId = $_SESSION['DEFAULTCURRENCYID'];
        }
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $currencyId . "'"); 
        if($valuePoint != 0) {
            $price = $valuePoint * $currencyValue;
            return $price;
        } else {
            return $valuePoint;            
        }
    }

    function quantitySurcharge($price, $basketId) {
        $price;
        $obj = (object) $obj;
        $surcharge = 0.00;
        $totalQuantity = 0;
        $qtyResult = $this->executeQry("select sum(quantity) as totalQty from " . TBL_BASKET . " where userId = {$_SESSION[USER_ID]}");

        $totalQuantity = $this->fetch_object($qtyResult)->totalQty;
        $quantity = $this->fetchValue(TBL_BASKET, "quantity", " id = '" . $basketId . "'");
        if ($totalQuantity != 0) {
            $percent = round(($quantity * 100) / $totalQuantity);
        } else {
            $percent = 0;
        }

        if ($percent < __QUANTITYSURCHARGE__) {
            $charge = $this->fetchValue(TBL_QUANTITY_SURCHARGE, "charge", " quantity <= '" . $percent . "' order by quantity desc limit 1");
            if ($charge) {
                $src = ($price * $charge) / 100;
            } else {
                $charge = $this->fetchValue(TBL_QUANTITY_SURCHARGE, "charge", " quantity >= '" . $percent . "' order by quantity asc limit 1");
                $src = ($price * $charge) / 100;
            }
        }

//        $qtyCustom = $this->addCustomDuty($src);
//        $surcharge = $qtyCustom + $src;

        $surcharge = $src;
        if ($quantity == 0) {
            $html .= '<tr>
                <td style="width:520px">'.LANG_SURCHARGE_FOR_LESS_THAN.' 25% '.LANG_TOTAL_QUANTITY.'</td>
                <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge) . '</td>
                <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge) . '</td>
            </tr>';
        } else {
            $html .= '<tr>
                    <td style="width:520px">'.LANG_SURCHARGE_FOR_LESS_THAN.' 25% '.LANG_TOTAL_QUANTITY.'</td>
                    <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge) . '</td>
                    <td style="width:60px;" class="price">' . $this->getProductPrice($surcharge, $quantity) . '</td>
                </tr>';
        }
        $obj->surcharge = $src;
        $obj->qtySurcharge = $surcharge;
        $obj->html = $html;
        return $obj;
    }

    function getProductCodeByName($quality, $styleCode) {
        $qCode = substr($quality, 0, 3);
        $sCode = str_replace("xxx", $qCode, $styleCode);
        return $sCode;
    }

    function getProductCodeById($qualityId, $styleId) {
        $quality = $this->fetchValue(TBL_FABRICCHARGE, "fQuality", " id = '" . $qualityId . "'");
        $styleCode = $this->fetchValue(TBL_PRODUCT_STYLE, "styleCode", " id = '" . $styleId . "'");
        $qCode = substr($quality, 0, 3);
        $sCode = str_replace("xxx", $qCode, $styleCode);
        return $sCode;
    }

    function getQuantityPageInfo() {
        $html = '';
        $htmlButton = '';
        $cond = '';
        $viewName = array();
        $viewId = array();
        $product = (object) $product;
        $ctr = 1;
        $totalBasketId = '';
        $displayTotalPrice = "";

        $userId = $this->fetchValue(TBL_COLLECTION, "userId", " collectionName = '" . $_SESSION['collectionName'] . "'");
        if (!$userId) {
            $this->executeQry("update " . TBL_COLLECTION . " set userId = '" . $_SESSION['USER_ID'] . "' where collectionName = '" . $_SESSION['collectionName'] . "'");
        }

        $plainSurcharge = $this->getPlainProductSurcharges();        
        
//        $orderId = $this->fetchValue(TBL_BASKET, "orderId", "userId = '".$_SESSION[USER_ID]."'");
//        if($orderId) {
//            $couponId = $this->fetchValue(TBL_ORDER, "couponId", "id = '".$orderId."'");
//            $this->executeQry("update ".TBL_GIFT_SEND." set couponExpired = '0', couponPriceUsed = '0' where id = '".$couponId."'");            
//        }
        if ($_SESSION[USER_ID] != '') {
            $cond = " where userId = '" . $_SESSION['USER_ID'] . "' order by id asc";
        } else {
            $cond = " where sessionId = '" . session_id() . "' order by id asc";
        }
        $query = "select * from " . TBL_BASKET . $cond;
        $result = $this->executeQry($query);
        $num = $this->num_rows($result);
       
        if ($num > 0) {
            $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id='" . $_SESSION[DELIVERYTIME] . "'");
            while ($line = mysql_fetch_object($result)) {
                $encodeBasketId = base64_encode($line->id);
                $totalQty = $this->fetchValue(TBL_BASKET, "quantity", "id = '" . $line->id . "'");
                $product->totalProduct += $totalQty;
                //=======get deco info========
                $viewObj = $this->getDecoView($line->id, $line->mainProdId, $totalQty);

                $decoUpdateData = $this->getDecoUnitPrice($line->id);
                $this->executeQry("update " . TBL_BASKET . " set decoPrice = '" . $decoUpdateData->decoBasePrice . "', decoSurcharge = '" . $decoUpdateData->surcharge . "', decoUnitPrice = '" . $decoUpdateData->decoPrice . "' where id = '" . $line->id . "'");

                //$country = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "countryId = '" . $_SESSION[DEFAULTCOUNTRYID] . "'");
$country = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
                $totalBasketId .= ", " . $line->id;
                if ($ctr == 1) {
                    $html .= '<div class="delivery-product-basic del">                                
                        <span id="deliveryCountry"> ' .LANG_COUNTRY_OF_DELIVERY.' :'. $country . '</span>
                        <span id="deliveryTime"> ' .LANG_DELIVERY_TIME.' :'. $deliveryTime .' '.LANG_DAY. ' </span>';
                } else {
                    $html .= '<div class="delivery-product-basic del">';
                }
                $mainResult = $this->executeQry("select * from " . TBL_MAINPRODUCT . " where id = '" . $line->mainProdId . "'");
                $mainProd = $this->getResultObject($mainResult);
                $styleCode = $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '" . $mainProd->styleId . "'");
                $quality = $this->fetchValue(TBL_FABRICCHARGE, 'fQuality', " id = '" . $mainProd->qualityId . "'");
                $query = "select viewImage, viewId from " . TBL_MAINPRODUCT_VIEW . " where mainProdId = '" . $line->mainProdId . "' order by viewId";
                $res = $this->executeQry($query);
                $n = $this->num_rows($res);
                if ($n > 0) {
                    $html .= '<ul>';
                    while ($row = $this->fetch_object($res)) {
                        $html .='<li><a href="javascript:void(0);"><img src="' . SITE_URL . __MAINPRODTHUMB__ . $row->viewImage . '" height="140" width="100" alt="image" /></a></li>';
                        $viewName[] = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $row->viewId . "'");
                        $viewId[] = $row->viewId;
                    }

                    $sCode = $this->getProductCodeByName($quality, $styleCode);
                    $html .= '</ul>
                        <strong>' .LANG_PRODUCT_QUANTITY_DELIVERY.' ' . $ctr . '<span>: '.LANG_PROD_CODE.': <span>' . $sCode . '</span></span>
                            <a onClick="form.submit();" href="quantitydeliverytime.php?basketId=' . $encodeBasketId . '">'.LANG_DELETE_PRODUCT.'</a>
                        </strong>
                        </div>                            
                        <div class="fabric-quality del">          
                        <strong>'.LANG_FABRIC_QUALITY.' : <strong>' . $quality . '</strong></strong>';

                    //======get product price info========    
                    $_SESSION[DEFAULTCURRENCYID];
                    $obj = $this->getProductDetails($line->id, $mainProd->rawProdId, $mainProd->qualityId, $mainProd->styleId, $plainSurcharge);
                    
                    $this->executeQry("update " . TBL_BASKET . " set fabricUnitPrice = '" . $obj->unitPrice . "' where id = '" . $line->id . "'"); 
                    $html .= '<table id="productDeco">
                        <tr class="size-description">
                        <td style="width:520px"> ' . $obj->size . '// '. LANG_TOTAL.' : ' . $totalQty . ' '. LANG_PCS.' // '. LANG_VALUE_POINT_PER_SHEET.' ' . $obj->productVP . '</td>
                        
                        <td style="width:60px" class="price">' . $obj->productPrice . '</td>
                        <td style="width:60px" class="price">' . $obj->productTotalPrice . '</td>
                        </tr>';

                    $displayTotalPrice = $displayTotalPrice+$obj->totalPrice;
                    $html .= $viewObj->en;
                    
                    $displayTotalPrice = $displayTotalPrice+$viewObj->returnprice;
                    
                    $curId = $_SESSION['DEFAULTCURRENCYID'];
                    $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
                    
                
                   
                    $html .='</table></div>';
                    $html .= '<div class="product-comment del">                        
                        <table class="about-comment">';
                    $html .= $viewObj->dis;
                    $quantityObj = $this->quantitySurcharge($obj->productVP, $line->id);     
                    
                               
                    $html .= $quantityObj->html;
                    
                   
                    $displayTotalPrice = $displayTotalPrice+($this->getPrice($quantityObj->qtySurcharge));
                    
                    $deliveryObj = $this->getDeliverySurcharge($obj->productVP, $obj->totalQty);
                    $productUnitPrice = $this->getProductPriceById($line->id, $deliveryObj, $obj);
                    $finalPrice = $productUnitPrice * $obj->totalQty;
                    $product->finalPrice += $finalPrice;
                    $html .= $deliveryObj->html;
                    
                  
                    
                    $displayTotalPrice = $displayTotalPrice+($deliveryObj->surchargetotal);
                    
                    $html .='</table>
                        <span>' . LANG_COMMENT_TO_PRODUCT .' '. $ctr . ':</span>
                        <span style="max-width:10%; float:left; color:red; font-weight:bold;">' . LANG_ATTENTION .'</span>
                        <span style="width:85%; float:left; font-weight:bold;"> -> '. wordwrap(LANG_ATTENTION_COMMENT, 97, '<br />') . '</span>
                        <textarea name="comment_' . $line->id . '" id="comment_' . $line->id . '" rows="0" cols="0">' . $line->comment . '</textarea>
                        </div>';
                }
                $htmlButton .='<a href="javascript:void(0);" class="fabric-button" onclick="enterFabricQuantity(\'' . $line->id . '\', \'fabricQuantity\', \'' . $mainProd->rawProdId . '\', \'' . $mainProd->styleId . '\', \'' . $mainProd->qualityId . '\', \'' . $line->mainProdId . '\');">
                <span style="padding:2px 0;">'.LANG_ENTER_FABRIC_QUALITY_QUANTITYS.'</span>
                </a>';
                
                $this->executeQry("update " . TBL_BASKET . " set currencyId = '" . $_SESSION[DEFAULTCURRENCYID] . "', productCode = '" . $sCode . "', quantitySurcharge = '" . $quantityObj->surcharge . "', deliverySurcharge = '" . $deliveryObj->surcharge . "', deliverySign = '" . $deliveryObj->sign . "', unit_price = '" . $productUnitPrice . "', final_price ='" . $finalPrice . "' where id = '" . $line->id . "'");
                $ctr++;
            }
           
 //           $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "' and langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "'");
//            $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "'");
//            $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "'");            
            //$countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "' and langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "'");
            $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['ORDERCOUNTRYID']. "'");
            $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
            $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");

            $vatPrice = $this->getProductVat($product->finalPrice, $vat);
            $excludingVatPrice = $product->finalPrice;
           // $totalPrice = $excludingVatPrice + $vatPrice;
            
            
            
            if($displayTotalPrice>0){
            $reviewtotal = $this->getProductPrice($displayTotalPrice/$currencyValue);	
            $totalPrice = ($displayTotalPrice/$currencyValue) + $vatPrice;
            }else{
            	$reviewtotal = $this->getProductPrice($displayTotalPrice);
            	$totalPrice = $displayTotalPrice + $vatPrice;
            }
            
            
            
            $html .='<input type="hidden" name="vat" value="' . $vatPrice . '" />';
            $html .='<input type="hidden" name="customDuty" value="' . $customDutyPrice . '" />';
            $html .= '<div class="del">
                    <table width="95%" class="total_price">
                    <tr>
                        <td width="70%">'.LANG_TOTAL_ORDER_EXCLUDING_VAT.' : ' . $vat . ' % '.LANG_VAT.' (' . $countryName . ') : </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $reviewtotal . '</td>
                      </tr>
                      <tr>
                        <td width="70%"> ' . $vat . ' % '.LANG_VAT.' (' . $countryName . '): </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $this->getProductPrice($vatPrice) . '</td>
                      </tr>                      
                    </table>
            <table width="95%" class="total_price" style="margin-top:10px;">
                <tr>
                <td width="30%">' .LANG_TOTAL_PRODUCT.' : '. $product->totalProduct . ' </td>
                <td align="right" width="40%"> '. LANG_TOTAL_INC .' : '. $vat . '% '.LANG_VAT . " ". $countryName . ': </td>
                <td width="18%" align="right">' . $this->getProductPrice($totalPrice) . '</td>
                </tr>                      
            </table>
                </div>';

            $html .= '</div></div>';
            $bId = substr($totalBasketId, 1);
            $isProductValid = $this->isProductValid($bId);
            $basketStatus = $this->isBasketEmpty($totalBasketId);
            $html .= '<input type="hidden" name="totalBasketId" id="totalBasketId" value="' . $basketStatus . '" />';
            $html .= '<input type="hidden" name="isProductValid" id="isProductValid" value="' . $isProductValid . '" />';
            $html .= '<div class="delivery-page-right-part">
            <span style="font-size:12px; color:#4D4D4D;">'.LANG_COUNTRY_OF_DELIVERY.':</span>
            <select name="countryId" onChange="changeCountry(this.value);">' . $this->getCountry($_SESSION[ORDERCOUNTRYID] ? $_SESSION[ORDERCOUNTRYID] : '') . '</select>
            <span style="font-size:12px; color:#4D4D4D;">'.LANG_COURRENCY_OF_RATES.' :</span>
            <select name="currencyId" >' . $this->getCurrencyFromCountry($_SESSION['DEFAULTCURRENCYID']) . '</select>
            <span style="font-size:12px; color:#4D4D4D;">'.LANG_DELIVERY_OF_INC_HOLIDAYS.' :</span>
            <select name="deliveryTime" onChange="changeDeliveryTime(this.value);">' . $this->getDeliveryTime($_SESSION['DELIVERYTIME']) . '</select>';
            $html .= $htmlButton;
        }
        return $html;
    }

    function isProductValid($bId) {
        $basketId = explode(',', $bId);
        foreach ($basketId as $id) {
            $num = 0;
            $rs = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $id . "'");
            $num = $this->num_rows($rs);
            if ($num == 0) {
                return 0;
            }
        }
        return 1;
    }

    function getProductVat($finalPrice, $vat) {
        return $vp = ($finalPrice * $vat) / 100;
    }

    function addCustomDuty($valuePoint) {
        //$percent = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "'");
        $percent = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
        return $customDuty = ($valuePoint * $percent) / 100;
        //return $valuePoint + $customDuty;
    }

//    function getProductPriceById($basketId, $obj, $plain) {
//        $price = $valuePoint = 0;
//        $query = "select quantitySurcharge, decoUnitPrice from ".TBL_BASKET." where id = '".$basketId."'";
//        $resultSet = $this->executeQry($query);
//        $num = $this->num_rows($resultSet);
//        if($num > 0) {
//            $line = $this->fetch_object($resultSet);
//            $valuePoint = $plain->pSurcharge + $line->decoUnitPrice + $line->quantitySurcharge;
//            $customDuty = $this->addCustomDuty($valuePoint);       //add custom duty
//            
//            $price = $valuePoint + $customDuty;
//            $obj->sign;
//            if($obj->sign == 0) {
//                $price -= $obj->surcharge;
//            } else if($obj->sign == 2) {
//                $price += $obj->surcharge;
//            }
//        }        
//        return $price;
//    }

    function getProductPriceById($basketId, $obj, $plain) {
        $price = $valuePoint = 0;
        $query = "select quantitySurcharge, decoUnitPrice from " . TBL_BASKET . " where id = '" . $basketId . "'";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $line = $this->fetch_object($resultSet);
            $pSurCustom = $this->addCustomDuty($plain->pSurcharge);
            $decoCustom = $this->addCustomDuty($line->decoUnitPrice);
            //$qtyCustom = $this->addCustomDuty($line->quantitySurcharge);            
            $customDuty = $pSurCustom + $decoCustom;

            $valuePoint = $plain->pSurcharge + $line->decoUnitPrice + $line->quantitySurcharge;
            $price = $valuePoint + $customDuty;
            $obj->sign;
            if ($obj->sign == 0) {
                $price -= $obj->surcharge;
            } else if ($obj->sign == 2) {
                $price += $obj->surcharge;
            }
        }
       // $price = str_replace("-", "", $price);
        return $price;
    }

    function getOrderInfo() {
        $html = '';
        $htmlButton = '';
        $cond = '';
        $viewName = array();
        $viewId = array();
        $product = (object) $product;
        $ctr = 1;
        $displayTotalPrice = "";

        if ($_SESSION[USER_ID] != '') {
            $cond = " where userId = '" . $_SESSION['USER_ID'] . "'";
        } else {
            $cond = " where sessionId = '" . session_id() . "'";
        }
        $orderId = base64_decode($_GET['id']);
        $plainSurcharge = $this->getPlainProductSurcharges();

        $query = "select * from " . TBL_BASKET . $cond;
        $result = $this->executeQry($query);
        $num = $this->num_rows($result);

        if ($num > 0) {
            $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id='" . $_SESSION[DELIVERYTIME] . "'");
            while ($line = mysql_fetch_object($result)) {
                $encodeBasketId = base64_encode($line->id);
                $totalQty = $this->fetchValue(TBL_BASKET, "quantity", "id = '" . $line->id . "'");
                $product->totalProduct += $totalQty;
                //=======get deco info========
                $viewObj = $this->getDecoView($line->id, $line->mainProdId, $totalQty);

                $country = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", "countryId = '" . $_SESSION[ORDERCOUNTRYID] . "'");

                if ($ctr == 1) {
                    $html .= '<div class="delivery-product-basic del">                                
                        <span id="deliveryCountry">' .LANG_COUNTRY_OF_DELIVERY.' :'. $country . '</span>
                        <span id="deliveryTime"> ' .LANG_DELIVERY_TIME.' :'. $deliveryTime .' '.LANG_DAY. ' </span>';
                } else {
                    $html .= '<div class="delivery-product-basic del">';
                }
                $mainResult = $this->executeQry("select * from " . TBL_MAINPRODUCT . " where id = '" . $line->mainProdId . "'");
                $mainProd = $this->getResultObject($mainResult);
                $styleCode = $this->fetchValue(TBL_PRODUCT_STYLE, 'styleCode', " id = '" . $mainProd->styleId . "'");
                $quality = $this->fetchValue(TBL_FABRICCHARGE, 'fQuality', " id = '" . $mainProd->qualityId . "'");
                $query = "select viewImage, viewId from " . TBL_MAINPRODUCT_VIEW . " where mainProdId = '" . $line->mainProdId . "'";
                $res = $this->executeQry($query);
                $n = $this->num_rows($res);
                
                if ($n > 0) {
                    $html .= '<ul>';
                    while ($row = $this->fetch_object($res)) {
                        $html .='<li><a href="#"><img src="' . SITE_URL . __MAINPRODTHUMB__ . $row->viewImage . '" height="140" width="100" alt="image" /></a></li>';
                        $viewName[] = $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = '" . $row->viewId . "'");
                        $viewId[] = $row->viewId;
                    }

                    $sCode = $this->getProductCodeByName($quality, $styleCode);
                    $html .= '</ul>
                        <strong>' .LANG_PRODUCT.' ' . $ctr . '<span>'.LANG_PROD_CODE.'<span>' . $sCode . '</span></span>
                            <a onClick="form.submit();" href="quantitydeliverytime.php?basketId=' . $encodeBasketId . '">'.LANG_DELETE_PRODUCT.'</a>
                        </strong>
                        </div>                            
                        <div class="fabric-quality del">          
                        <strong>'.LANG_FABRIC_QUALITY.' :<strong>' . $quality . '</strong></strong>';

                    //======get product price info========
                    $obj = $this->getProductDetails($line->id, $mainProd->rawProdId, $mainProd->qualityId, $mainProd->styleId, $plainSurcharge);
                    $html .= '<table id="productDeco">
                        <tr class="size-description">                        
                        <td style="width:520px"> ' . $obj->size . '// '. LANG_TOTAL.' : ' . $totalQty . ' '. LANG_PCS.' // '. LANG_VALUE_POINT_PER_SHEET.' ' . $obj->productVP . '</td>
                        <td style="width:60px" class="price">' . $obj->productPrice . '</td>
                        <td style="width:60px" class="price">' . $obj->productTotalPrice . '</td>
                        </tr>';
                    $displayTotalPrice = $displayTotalPrice+$obj->totalPrice;
                    
                    $html .= $viewObj->en;
                    
                    $displayTotalPrice = $displayTotalPrice+$viewObj->returnprice;
                    
                    $html .='</table></div>';
                    $html .= '<div class="product-comment del">                        
                        <table class="about-comment">';
                    
                    $html .= $viewObj->dis;
                    
                    $quantityObj = $this->quantitySurcharge($obj->productVP, $line->id);   
                                     
                    $html .= $quantityObj->html;
                    
                    $displayTotalPrice = $displayTotalPrice+($this->getPrice($quantityObj->qtySurcharge));
                    
                    
                    $deliveryObj = $this->getDeliverySurcharge($obj->productVP, $obj->totalQty);
                    $productUnitPrice = $this->getProductPriceById($line->id, $deliveryObj, $obj);
                    $finalPrice = $productUnitPrice * $obj->totalQty;
                    $product->finalPrice += $finalPrice;
                    $html .= $deliveryObj->html;
                    
                    $displayTotalPrice = $displayTotalPrice+($deliveryObj->surchargetotal);
                    
                    
                    $html .='</table>
                       <span>' . LANG_COMMENT_TO_PRODUCT .' '. $ctr . ':</span>
                        <textarea rows="0" cols="0" disabled="disabled">' . $this->fetchValue(TBL_BASKET, "comment", " id = '" . $line->id . "'") . '</textarea>
                        </div>';
                }

                $htmlButton .='<a href="javascript:void(0);" class="fabric-button" onclick="enterFabricQuantity(\'' . $line->id . '\', \'fabricQuantity\', \'' . $mainProd->rawProdId . '\', \'' . $mainProd->styleId . '\', \'' . $mainProd->qualityId . '\', \'' . $line->mainProdId . '\');">
                <span>'.LANG_ENTER_FABRIC_QUALITY_QUANTITYS.'</span>
                </a>';

                $this->executeQry("update " . TBL_BASKET . " set currencyId = '" . $_SESSION[DEFAULTCURRENCYID] . "', productCode = '" . $sCode . "', quantitySurcharge = '" . $quantityObj->surcharge . "', deliverySurcharge = '" . $deliveryObj->surcharge . "', deliverySign = '" . $deliveryObj->sign . "', unit_price = '" . $productUnitPrice . "', final_price ='" . $finalPrice . "' where id = '" . $line->id . "'");
                $ctr++;
            }
				
            //===============================changes start here=============================
//            $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "' and langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "'");
//            $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "'");
//            $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['DEFAULTCOUNTRYID'] . "'");
            
            //$countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "' and langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "'");
            $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
            $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
            $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");

            $discountAmount = $this->fetchValue(TBL_ORDER, "discount_ammount", " id = '".$orderId."'");
            $excludingVatPrice = $product->finalPrice;
                        
            $currencyId = $this->fetchValue(TBL_ORDER, "currencyId", " id = '".$orderId."'");
            $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");            

            $discountVP = $discountAmount/$curValue;                        
            $priceAfterDiscount = $excludingVatPrice - $discountVP;
            $vatPrice = $this->getProductVat($priceAfterDiscount, $vat);
            $totalPrice = $priceAfterDiscount + $vatPrice;
            
            $curId = $_SESSION['DEFAULTCURRENCYID'];
            $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
            
            
            if($displayTotalPrice>0){
            	
            	$reviewtotal = $this->getProductPrice($displayTotalPrice/$currencyValue);
            	$afterdis = ($displayTotalPrice-$discountAmount)/$currencyValue;
            	$totalPrice = $afterdis + $vatPrice;
            }else{
            	$reviewtotal = $this->getProductPrice($displayTotalPrice);
            	$afterdis = ($displayTotalPrice-$discountAmount);
            	$totalPrice = $afterdis + $vatPrice;
            }
            
            
            

            $html .= '<div class="del">
                    <table width="95%" class="total_price">
                    <tr>
                        <td width="70%">'.LANG_TOTAL_ORDER_EXCLUDING_VAT.' : ' . $vat . ' % '.LANG_VAT.' (' . $countryName . ') :  </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $reviewtotal . '</td>
                      </tr>
                      <tr>
                        <td width="70%">'.LANG_COUPON_DISCOUNT.' :  </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $this->getPriceSign($discountAmount) . '</td>
                      </tr>
                      <tr>
                        <td width="70%"> ' . LANG_TOTAL_ORDER_AFTER_DISCOUNT . ': </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $this->getProductPrice($afterdis) . '</td>
                      </tr>
                      <tr>
                        <td width="70%"> ' . $vat . ' % '.LANG_VAT.' (' . $countryName . '): </td>
                        <td align="right" width="10%"></td>
                        <td width="20%" align="right">' . $this->getProductPrice($vatPrice) . '</td>
                      </tr>                      
                    </table>
            <table width="95%" class="total_price" style="margin-top:10px;">
                <tr>
                <td width="40%">' .LANG_TOTAL_PRODUCT.' : '. $product->totalProduct . ' </td>
                <td align="right" width="30%"> '. LANG_TOTAL_INC .' : '. $vat . '% '.LANG_VAT .' '. $countryName . ': </td>
                <td width="18%" align="right">' . $this->getProductPrice($totalPrice) . '</td>
                </tr>                      
            </table>
                </div>';
            //==================================ends here===================================			
            $address = array();
            $addressResult = $this->executeQry("select invoiceAddId, deliveryAddId, graficAddId from " . TBL_ORDER . " where id = '" . $orderId . "'");

            $addLine = $this->fetch_Object($addressResult);
            $addressId[] = $addLine->invoiceAddId;
            $addressId[] = $addLine->deliveryAddId;
            $addressId[] = $addLine->graficAddId;

            foreach ($addressId as $id) {
                //$addReslutSet = $this->executeQry("select firma, firstName, address, mobile, countryId, email from " . TBL_ADDRESS . " where id = '" . $id . "'");
                $addReslutSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $id . "'");
                $addLine = $this->fetch_Object($addReslutSet);
                $userTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " Id = '" . $addLine->title . "' and langId = '".$_SESSION[DEFAULTLANGUAGEID]."'");                
                $userName = $addLine->firstName." ".$addLine->lastName;
                $userLanguage = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $addLine->language ."'");
                $userCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $addLine->countryId . "' and langId = '1'");
                
                
                $address[] = $addLine->firma . " <br />" . $userTitle . " <br />" . $userName . " <br />" . $addLine->email . " <br />" . $addLine->address . " <br />". $addLine->address1 ."<br />". $addLine->city ."<br />". $addLine->phoneNo . "<br />" . $addLine->mobile . " <br />" . $userLanguage . " <br />". $userCountry ;
            }

            $html .= '<div style="float:left; width:600px;">
            <div class="invoiceDetail">
              <div class="invoiceRow">
                <div class="invoiceHead">'.LANG_INVOICE_ADDRESS.'</div>
                <div class="invoiceHead">'.LANG_DELIVERY_ADDRESS.'</div>
                <div class="invoiceHead">'.LANG_GRAPHIC_DESIGNER_ADDRESS.'</div>
              </div>
              <div class="invoiceRow">';
            foreach ($address as $add) {
                $html .= '<div class="invoiceData">' . $add . '</div>';
            }
            $html .= '</div>
                  </div></div>';

            $html .= '</div></div>';
            $html .= '<div class="delivery-page-right-part">';
            $html .='<a href="javascript:void(0);" id="sendOrder" class="fabric-button">
            <span>'.LANG_SEND_ORDER.'</span></a>';
            //$html .='<a href="javascript:void(0);" class="fabric-button">
//            <span>'.LANG_SAVE_PDF_ON_YOUR_HD.'</span></a>';
            $html .='<a href="quantitydeliverytime.php" class="fabric-button">
            <span>'.LANG_BACK_TO_QUANTITY_DELIVERY.'</span></a>';
        }
        return $html;
    }

    //  15Jan
//    function getProductDetails($basketId, $prodId, $qualityId, $styleId, $plainSurcharge) {
////        echo $basketId, ", ",  $prodId,", ", $qualityId,", ", $styleId,", ", $plainSurcharge;
////        exit;
//        $obj = (object) $obj;
//        $str = '';
//        $totalQty = 0;
//
//        $sql = "select distinct(b.sizeId), b.quantity, sd.sizeName from " . TBL_BASKET_SIZE . " as b inner join " . TBL_SIZEDESC . " as sd on b.sizeId = sd.id and b.basketId = '" . $basketId . "' order by sd.sizeName asc";
//        $result = $this->executeQry($sql);
//        $num = $this->getTotalRow($result);
//        if ($num > 0) {
//            while ($line = $this->fetch_object($result)) {
//                $str .= $line->sizeName . ": &nbsp;" . $line->quantity . " &nbsp;";
//                $totalQty += $line->quantity;
//            }
//        }
//
//        $obj->size = $str;
//        $obj->totalQty = $totalQty;
//
//        if ($totalQty == 0 || $totalQty == '') {
//            $rs = $this->fetchValue(TBL_CURRENCY_DETAIL, "sign", " id = '" . $_SESSION['DEFAULTCURRENCYID'] . "'");
//            $sign = html_entity_decode($rs);
//            $obj->productVP = 0;
//            $obj->productPrice = $this->getPriceSign(0);
//            $obj->productTotalPrice = $this->getPriceSign(0);
//            $obj->unitPrice = 0.00;
//            $obj->totalPrice = 0.00;
//            $obj->pSurcharge = 0.00;
//        } else {
//            $basePrice = $this->fetchValue(TBL_PRODUCT, "productPrice", " id = '" . $prodId . "'");
//            $qualityArr = $this->getVPByQuality($basePrice, $qualityId);
//            $qualityVP = $qualityArr['valuePoint'];
//            $style = $this->getVPByStyle($basePrice, $styleId);            
//            if ($style->sign == 2) {
//                $valuePoint = $qualityVP + $style->valuePoint;
//            } else if ($style->sign == 0) {
//                $valuePoint = $qualityVP - $style->valuePoint;
//            } else {
//                $valuePoint = $qualityVP;
//            }
//
//            $obj->productVP = $valuePoint;
//            //plain product surcharges
//            $key = array_search($qualityArr['fId'], $plainSurcharge['fabric']);
//            $fabricId = $qualityArr['fId'];
//            $plainQuantity = $plainSurcharge['quantity'][$key];
//
//            $plainResult = $this->executeQry("Select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' and pcs <= '" . $plainQuantity . "' order by pcs desc limit 1");
//            if ($this->num_rows($plainResult) > 0) {
//                $line = $this->fetch_object($plainResult);
//            } else {
//                $minPlainResult = $this->executeQry("select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' order by pcs asc limit 1");
//                $line = $this->fetch_object($minPlainResult);
//            }
//
//			$pSurcharge = ($valuePoint * $line->charge) / 100;
//            if ($line->sign == 2) {
//                $incPlainSurcharge = $valuePoint + $pSurcharge;
//            } else if ($line->sign == 0) {
//                $incPlainSurcharge = $valuePoint - $pSurcharge;
//            } else {
//                $incPlainSurcharge = $valuePoint;
//            }
//
//            $this->executeQry("update " . TBL_BASKET . " set plainSurcharge = '" . $pSurcharge . "' where id = '" . $basketId . "'");
//            $percent = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
//
//            $customDuty = ($incPlainSurcharge * $percent) / 100;              //custom duty on baseprice+plain product surcharge
//            $incCustomDuty = $incPlainSurcharge + $customDuty;
//
//            $obj->productPrice = $this->getProductPrice($incCustomDuty);
//            $obj->productTotalPrice = $this->getProductPrice($incCustomDuty, $obj->totalQty);
//            $obj->unitPrice = $this->getPrice($incCustomDuty);
//            $obj->totalPrice = $this->getPrice($incCustomDuty, $obj->totalQty);
//            $obj->pSurcharge = $incPlainSurcharge;
//        }
//        return $obj;
//    }

    
    function getProductDetails($basketId, $prodId, $qualityId, $styleId, $plainSurcharge) {
//        echo $basketId, ", ",  $prodId,", ", $qualityId,", ", $styleId,", ", $plainSurcharge;
//        exit;
        $obj = (object) $obj;
        $str = '';
        $totalQty = 0;

        $sql = "select distinct(b.sizeId), b.quantity, sd.sizeName from " . TBL_BASKET_SIZE . " as b inner join " . TBL_SIZEDESC . " as sd on b.sizeId = sd.id and b.basketId = '" . $basketId . "' order by sd.sizeName asc";
        $result = $this->executeQry($sql);
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                $str .= $line->sizeName . ": &nbsp;" . $line->quantity . " &nbsp;";
                $totalQty += $line->quantity;
            }
        }

        $obj->size = $str;
        $obj->totalQty = $totalQty;

        if ($totalQty == 0 || $totalQty == '') {
            $rs = $this->fetchValue(TBL_CURRENCY_DETAIL, "sign", " id = '" . $_SESSION['DEFAULTCURRENCYID'] . "'");
            $sign = html_entity_decode($rs);
            $obj->productVP = 0;
            $obj->productPrice = $this->getPriceSign(0);
            $obj->productTotalPrice = $this->getPriceSign(0);
            $obj->unitPrice = 0.00;
            $obj->totalPrice = 0.00;
            $obj->pSurcharge = 0.00;
        } else {
            $basePrice = $this->fetchValue(TBL_PRODUCT, "productPrice", " id = '" . $prodId . "'");
            $qualityArr = $this->getVPByQuality($basePrice, $qualityId);
            $qualityVP = $qualityArr['valuePoint'];
            $style = $this->getVPByStyle($basePrice, $styleId);            
            if ($style->sign == 2) {
                $valuePoint = $qualityVP + $style->valuePoint;
            } else if ($style->sign == 0) {
                $valuePoint = $qualityVP - $style->valuePoint;
            } else {
                $valuePoint = $qualityVP;
            }

            $obj->productVP = $valuePoint;
            //plain product surcharges
            $key = array_search($qualityArr['fId'], $plainSurcharge['fabric']);
            $fabricId = $qualityArr['fId'];
            $plainQuantity = $plainSurcharge['quantity'][$key];

            $plainResult = $this->executeQry("Select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' and pcs <= '" . $plainQuantity . "' order by pcs desc limit 1");
            if ($this->num_rows($plainResult) > 0) {
                $line = $this->fetch_object($plainResult);
            } else {
                $minPlainResult = $this->executeQry("select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' order by pcs asc limit 1");
                $line = $this->fetch_object($minPlainResult);
            }

            $pSurcharge = ($valuePoint * $line->charge) / 100;
            if ($line->sign == 2) {
                $incPlainSurcharge = $valuePoint + $pSurcharge;
            } else if ($line->sign == 0) {
                $incPlainSurcharge = $valuePoint - $pSurcharge;
            } else {
                $incPlainSurcharge = $valuePoint;
            }

            $this->executeQry("update " . TBL_BASKET . " set plainSurcharge = '" . $pSurcharge . "' where id = '" . $basketId . "'");
            $percent = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");

            $customDuty = ($incPlainSurcharge * $percent) / 100;              //custom duty on baseprice+plain product surcharge
            $incCustomDuty = $incPlainSurcharge + $customDuty;

            $obj->productPrice = $this->getProductPrice($incCustomDuty);
            $obj->productTotalPrice = $this->getProductPrice($incCustomDuty, $obj->totalQty);
            $obj->unitPrice = $this->getPrice($incCustomDuty);
            $obj->totalPrice = $this->getPrice($incCustomDuty, $obj->totalQty);
            $obj->pSurcharge = $incPlainSurcharge;
        }
        return $obj;
    }
    
    
    function getProductDetailByOrderId($basketId, $prodId, $qualityId, $styleId, $plainSurcharge) {
//        echo $basketId, ", ",  $prodId,", ", $qualityId,", ", $styleId,", ", $plainSurcharge;
//        exit;
        $obj = (object) $obj;
        $str = '';
        $totalQty = 0;

        $sql = "select distinct(b.sizeId), b.quantity, sd.sizeName from " . TBL_BASKET_SIZE . " as b inner join " . TBL_SIZEDESC . " as sd on b.sizeId = sd.id and b.basketId = '" . $basketId . "' order by sd.sizeName asc";
        $result = $this->executeQry($sql);
        $num = $this->getTotalRow($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                $str .= $line->sizeName . ": &nbsp;" . $line->quantity . " &nbsp;";
                $totalQty += $line->quantity;
            }
        }

        $obj->size = $str;
        $obj->totalQty = $totalQty;

        if ($totalQty == 0 || $totalQty == '') {
            $rs = $this->fetchValue(TBL_CURRENCY_DETAIL, "sign", " id = '" . $_SESSION['DEFAULTCURRENCYID'] . "'");
            $sign = html_entity_decode($rs);
            $obj->productVP = 0;
            $obj->productPrice = $this->getPriceSign(0);
            $obj->productTotalPrice = $this->getPriceSign(0);
            $obj->unitPrice = 0.00;
            $obj->totalPrice = 0.00;
            $obj->pSurcharge = 0.00;
        } else {
            $basePrice = $this->fetchValue(TBL_PRODUCT, "productPrice", " id = '" . $prodId . "'");
            $qualityArr = $this->getVPByQuality($basePrice, $qualityId);
            $qualityVP = $qualityArr['valuePoint'];
            $style = $this->getVPByStyle($basePrice, $styleId);            
            if ($style->sign == 2) {
                $valuePoint = $qualityVP + $style->valuePoint;
            } else if ($style->sign == 0) {
                $valuePoint = $qualityVP - $style->valuePoint;
            } else {
                $valuePoint = $qualityVP;
            }

            $obj->productVP = $valuePoint;
            //plain product surcharges
            $key = array_search($qualityArr['fId'], $plainSurcharge['fabric']);
            $fabricId = $qualityArr['fId'];
            $plainQuantity = $plainSurcharge['quantity'][$key];

            $plainResult = $this->executeQry("Select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' and pcs <= '" . $plainQuantity . "' order by pcs desc limit 1");
            if ($this->num_rows($plainResult) > 0) {
                $line = $this->fetch_object($plainResult);
            } else {
                $minPlainResult = $this->executeQry("select * from " . TBL_ACCESSORIES_DISCOUNT . " where fabricId = '" . $fabricId . "' order by pcs asc limit 1");
                $line = $this->fetch_object($minPlainResult);
            }

            $pSurcharge = ($valuePoint * $line->charge) / 100;
            if ($line->sign == 2) {
                $incPlainSurcharge = $valuePoint + $pSurcharge;
            } else if ($line->sign == 0) {
                $incPlainSurcharge = $valuePoint - $pSurcharge;
            } else {
                $incPlainSurcharge = $valuePoint;
            }

            $this->executeQry("update " . TBL_BASKET . " set plainSurcharge = '" . $pSurcharge . "' where id = '" . $basketId . "'");
            $percent = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");

            $customDuty = ($incPlainSurcharge * $percent) / 100;              //custom duty on baseprice+plain product surcharge
            $incCustomDuty = $incPlainSurcharge + $customDuty;

            $obj->productPrice = $this->getProductPrice($incCustomDuty);
            $obj->productTotalPrice = $this->getProductPrice($incCustomDuty, $obj->totalQty);
            $obj->unitPrice = $this->getPrice($incCustomDuty);
            $obj->totalPrice = $this->getPrice($incCustomDuty, $obj->totalQty);
            $obj->pSurcharge = $incPlainSurcharge;
        }
        return $obj;
    }
    
    function getPrice($valuePoint, $quantity = '1') {
        $curId = $_SESSION['DEFAULTCURRENCYID'];
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");

        $line = $this->getResultObject($sql);
        $price = $valuePoint * $currencyValue;        
        $total = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        if ($quantity != '1') {
            $total = $total * $quantity;
        }        
        return $total;
    }

    function getVPPrice($valuePoint, $curId) {
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        return $total = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
    }
    
    function getRoundVPPrice($valuePoint, $curId) {
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        
        return $total = number_format($price, 2, '.', '');
    }
    
    function getVP_Price($valuePoint, $curId) {
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        return $valuePoint * $currencyValue;
    }

    function getVPByQuality($basePrice, $qualityId) {
        //$percent = $this->fetchValue(TBL_FABRICCHARGE, "percent", " id = '".$qualityId."'"); 
        $result = $this->executeQry("select fId, percent from " . TBL_FABRICCHARGE . " where id = '" . $qualityId . "'");
        if ($this->num_rows($result) > 0) {
            $line = $this->fetch_object($result);            
            $obj['valuePoint'] = ($basePrice * $line->percent) / 100;
            $obj['fId'] = $line->fId;
        }
        return $obj;
    }

    function getVPByStyle($basePrice, $styleId) {
        $obj = (object) $obj;
        $rs = $this->executeQry("select sign, percent from " . TBL_PRODUCT_STYLE . " where id= '" . $styleId . "'");
        $line = $this->fetch_object($rs);

        $valuePoint = ($basePrice * $line->percent) / 100;
        $sign = (int) $line->sign;
        $obj->valuePoint = $valuePoint;
        $obj->sign = $sign;
        return $obj;
    }

    //price formation===========
    function formatPrice($price) {
        $curId = $_SESSION['DEFAULTCURRENCYID'];
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        return $price;
    }
    
    function getRoundPriceWithSign($valuePoint, $curId) {
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        $price = number_format($price);
        $price = str_replace("-", "", $price);
        $finalPrice = $leftPlace . "" . $price . "" . $rightPlace;
        return $finalPrice;
    }

    function getPriceWithSign($valuePoint, $curId) {
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $price = $valuePoint * $currencyValue;
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        $price = str_replace("-", "", $price);
        $finalPrice = $leftPlace . "" . $price . "" . $rightPlace;
        return $finalPrice;
    }

    function getPriceSign($price, $quantity = '1') {
        $curId = $_SESSION['DEFAULTCURRENCYID'];
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        if ($quantity != '1') {
            $price = $price * $quantity;
        }
        $price = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        $final_price = $leftPlace . "" . $price . "" . $rightPlace;
        return $final_price;
    }
     
    //15Jan 2013
//    function getProductPrice($valuePoint, $quantity = '1') {
//        $curId = $_SESSION['DEFAULTCURRENCYID'];
//        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
//        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
//        $line = $this->getResultObject($sql);
//        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
//        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
//
//        $price = $valuePoint * $currencyValue;
//        if ($quantity != '1') {
//            $price = $price * $quantity;
//        }
//        $total = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
//        $total = str_replace("-", "", $total);
//        $final_price = $leftPlace . "" . $total . "" . $rightPlace;
//        return $final_price;
//    }
    
    function getProductPrice($valuePoint, $quantity = '1') {
        $curId = $_SESSION['DEFAULTCURRENCYID'];
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", "cur_id ='" . $curId . "'");
        $sql = $this->executeQry("select c.*, cd.sign from " . TBL_CURRENCY . " as c inner join " . TBL_CURRENCY_DETAIL . " as cd where 1 and c.currencyDetailId = cd.id and c.currencyDetailId = '" . $curId . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";

        $price = $valuePoint * $currencyValue;
        $total = number_format($price, $line->decimalPlace, ".", $line->SeparatorFrom);
        if ($quantity != '1') {
            $total = $total * $quantity;
        }        
        $total = str_replace("-", "", $total);
        $final_price = $leftPlace . "" . $total . "" . $rightPlace;
        return $final_price;
    }

    function changeCurrency($get) {      
        $_SESSION['DEFAULTCURRENCYID'] = $get[id];
        $html = $this->getQuantityPageInfo();        
        //echo $_SESSION[DEFAULTCURRENCYID];
    }

    function changeDeliveryTime($get) {
        $_SESSION['DELIVERYTIME'] = $get['id'];
        $html = $this->getQuantityPageInfo();
        echo "1";
    }

    function changeCountry($get) {
        //$_SESSION[DEFAULTCOUNTRYID] = $get['id'];
        $_SESSION['ORDERCOUNTRYID'] = $get['id'];
        
        $currencyId = $this->fetchValue(TBL_COUNTRYSETTING, 'currencyId', 'countryId='.$get['id']);
        
        if($currencyId){
           $_SESSION['DEFAULTCURRENCYID'] = $currencyId; 
        }
        
        $html = $this->getQuantityPageInfo();
        //echo "$html";        
    }

    function isValidGiftVoucher($get) {
        $flag = "fail";
        $code = $get['code'];
        $result = $this->executeQry("select id, couponExpiryDate from " . TBL_GIFT_SEND . " where giftCertificateCode = '" . $code . "' and couponExpired = '0'");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $line = $this->fetch_object($result);
            $validDate = strtotime($line->couponExpiryDate);
            $todayDate = strtotime(date('Y-m-d'));
            if ($todayDate <= $validDate) {
                $flag = "success";
            }
        }
        echo $flag;
    }

    function getDeliveryDay($id) {
        $validate = null;
        $days = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id = '".$id."'");
        $query = "select * from " . TBL_BASKET ." where userId = '" . $_SESSION['USER_ID'] . "' order by id asc";
        $result = $this->executeQry($query);
        if($this->num_rows($result) > 0) {
            $basketId = '';
            while($line = $this->fetch_object($result)) {
                $basketId .= ','.$line->id;
            }
            $basketId = substr($basketId, 1);
            $rs = $this->executeQry("SELECT sum(quantity) as quantity FROM ".TBL_BASKET_SIZE." where basketId IN (".$basketId.")");
            if($this->num_rows($rs) > 0) {
                $row = $this->fetch_object($rs);
            }
            $deliveryResult = $this->executeQry("select days from ".TBL_DELIVERY_TIME." where pcs >= '".$row->quantity."' order by pcs asc limit 1");
            if($this->num_rows($deliveryResult) > 0) {
                $min_day = $this->fetch_object($deliveryResult)->days;
                if($days < $min_day) {
                    $validate = LANG_MIN_DELIVERY_TIME_MSG.' '.$min_day.' '.LANG_MIN_DELIVERY_TIME_MSG1.' '.$row->quantity;
                }
            } else {
                $minQuery = $this->executeQry("select days from ".TBL_DELIVERY_TIME." order by pcs desc limit 1");
                $min_day = $this->fetch_object($minQuery)->days;
                if($days < $min_day) {
                    $validate = LANG_MIN_DELIVERY_TIME_MSG.' '.$min_day.' '.LANG_MIN_DELIVERY_TIME_MSG1.' '.$row->quantity;
                }
            }
        }
        return $validate;
    }
    
    
//    16Oct2013
//    function saveOrder($post) {
//        $hBId = explode(',', $post['basketId']);
//        $bsRecord = count($hBId);
//
//        $countryName = '';
//        $discountAmount = 0;
//        $discountVP = 0;
//        $subTotal = 0;
//        $totalQuantity = 0;
//        $totalAmount = 0;
//        $saveOrderId = 0;
//        $currencyId = $post['currencyId'];
//        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
//        for ($i = 0; $i < $bsRecord; $i++) {
//            $cmt = "comment_" . $hBId[$i];
//            $comment = $post[$cmt];
//            $this->executeQry("update " . TBL_BASKET . " set comment = '" . $comment . "' where id = '" . $hBId[$i] . "'");
//        }
//
//        if (isset($post['giftVoucher']) && $post['giftVoucher'] !== '') {
//            $denominationId = $this->fetchValue(TBL_GIFT_SEND, "giftDenominationId", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
//            $discountAmount = $this->fetchValue(TBL_GIFT_DENOMINATION, "value", "id = '" . $denominationId . "'");
//        }
//        
//        $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");        
//        $curValueItem = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = 1");
//        $convertedDiscAmount = $discountAmount/$curValueItem;
//        $discountVP = $convertedDiscAmount * $curValue;
//             
//        $amtResult = $this->executeQry("select userId, orderId, sum(quantity) as totalQuantity, sum(final_price) as totalAmount from " . TBL_BASKET . " where userId = " . $_SESSION[USER_ID] . " group by userId");
//        if ($amtResult) {            
//            $amtObject = $this->fetch_object($amtResult);
//            //======start new editing==========
//            if($amtObject->totalAmount > $convertedDiscAmount) {
//                $ordId = $amtObject->orderId;
//                $subTotal = $amtObject->totalAmount;
//                $totalQuantity = $amtObject->totalQuantity;
//                $totalAmount = $subTotal - $convertedDiscAmount;                
//                $vatPrice = $this->getProductVat($totalAmount, $vat);
//            } else {
//                $ordId = $amtObject->orderId;
//                $subTotal = $amtObject->totalAmount;                
//                $totalQuantity = $amtObject->totalQuantity;
//                $totalAmount = 0;                
//                $vatPrice = 0;
//                $discountVP = $subTotal * $curValue;
//            }
//            //======end new editing==========
////            $subTotal = $amtObject->totalAmount;
////            $totalQuantity = $amtObject->totalQuantity;
////            $totalAmount = $subTotal - $convertedDiscAmount;
////            $ordId = $amtObject->orderId;
////            $vatPrice = $this->getProductVat($totalAmount, $vat);
//        }
//        $orderReceipt = $this->fetchValue(TBL_ORDER, "ordReceiptId", " id = '" . $ordId . "'");
//        if ($orderReceipt) {
//            $orderNo = $orderReceipt;
//            //=========================update order table======================            
//            $query = "update " . TBL_ORDER . " set orderDate = '" . date('Y-m-d') . "', ipAddress = '" . $_SERVER[REMOTE_ADDR] . "', countryId = '" . $post['countryId'] . "', deliveryId = '" . $_SESSION[DELIVERYTIME] . "', invoiceAddId = '" . $post['invoiceAdd'] . "', deliveryAddId = '" . $post['deliveryAdd'] . "', graficAddId = '" . $post['graficAdd'] . "', ordReceiptId = '" . $orderNo . "', currencyId = '" . $_SESSION[DEFAULTCURRENCYID] . "', langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "', ordQuantity = '" . $totalQuantity . "', ordSubTotal = '" . $subTotal . "', discount_ammount = '" . $discountVP . "', ordTotal = '" . $totalAmount . "', vat = '" . $vatPrice . "', custom_duty = '" . $post['customDuty'] . "'  where id = '" . $ordId . "'";
//            $this->executeQry($query);
//
//            //may not use
//            //===================update orderId in basket table=====================
//            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $ordId . "' where userId = '" . $_SESSION[USER_ID] . "'");
//
//            //===================update order details======================                      
//            $orderDetailsId = array();
//            $orderIdResult = $this->executeQry("select id from " . TBL_ORDERDETAIL . " where orderId = '" . $ordId . "'");
//            $orderIdNum = $this->num_rows($orderIdResult);
//            if ($orderIdNum > 0) {
//                while ($orderIdRow = $this->fetch_object($orderIdResult)) {
//                    $orderDetailsId [] = $orderIdRow->id;
//                }
//            }
//            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId ='" . $ordId . "'");
//            $numRecord = $this->num_rows($basketResultSet);
//            if ($numRecord > 0) {
//                while ($basketRow = $this->fetch_object($basketResultSet)) {
//                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";
//
//                    $this->executeQry($orderDetailQuery);
//                    $orderDetailId = mysql_insert_id();
//
//                    //=====================update order size table===============                
//                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
//                    $orderSizeNum = $this->num_rows($orderSizeResult);
//                    if ($orderSizeNum > 0) {
//                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
//                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
//                            $this->executeQry($orderSizeQuery);
//                        }
//                    }
//
//                    //=====================update order color table===============                    
//                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
//                    $orderColorNum = $this->num_rows($orderColorResult);
//                    if ($orderColorNum > 0) {
//                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
//                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
//                            $this->executeQry($orderColorQuery);
//                        }
//                    }
//
//                    //=====================update order deco table===============
//                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
//                    $orderDecoNum = $this->num_rows($orderDecoResult);
//                    if ($orderDecoNum > 0) {
//                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
//                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
//                            $this->executeQry($orderDecoQuery);
//                        }
//                    }
//                }
//            }
//
//            foreach ($orderDetailsId as $id) {
//                $this->executeQry("delete from " . TBL_ORDERDETAIL . " where id = '" . $id . "'");
//                $this->executeQry("delete from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $id . "'");
//                $this->executeQry("delete from " . TBL_ORDER_COLOR . " where orderDetailId = '" . $id . "'");
//                $this->executeQry("delete from " . TBL_ORDER_DECO . " where orderDetailId = '" . $id . "'");
//            }
//            $saveOrderId = $ordId;
//        } else {
//            //============generating order number====================            
//            $countrySign = $this->fetchValue(TBL_COUNTRY, "country_2_code", " id='" . $_SESSION[ORDERCOUNTRYID] . "'");
//            $date .= date('ymdhms');
//            $invoiceAdd = $this->fetchValue(TBL_ADDRESS, "firma", " id = '" . $post['invoiceAdd'] . "'");
//            $orderNo = $countrySign . $date . $invoiceAdd;
//
//            //=========================insert into order table======================
//            $query = "insert into " . TBL_ORDER . " (orderDate, ipAddress, sessId, userId, countryId, deliveryId, invoiceAddId, deliveryAddId, graficAddId, ordReceiptId, currencyId, langId, ordQuantity, discount_ammount, ordSubTotal, ordTotal, vat, custom_duty) values ('" . date('Y-m-d') . "', '" . $_SERVER[REMOTE_ADDR] . "', '" . session_id() . "', '" . $_SESSION[USER_ID] . "', '" . $post['countryId'] . "', '" . $_SESSION[DELIVERYTIME] . "', '" . $post['invoiceAdd'] . "', '" . $post['deliveryAdd'] . "', '" . $post['graficAdd'] . "', '" . $orderNo . "', '" . $_SESSION[DEFAULTCURRENCYID] . "', '" . $_SESSION[DEFAULTLANGUAGEID] . "', '" . $totalQuantity . "', '" . $discountVP . "', '" . $subTotal . "', '" . $totalAmount . "', '" . $vatPrice . "', '" . $post['customDuty'] . "')";
//            $this->executeQry($query);
//            $orderId = mysql_insert_id();
//
//            //===================update orderId in basket table=====================
//            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $orderId . "' where userId = '" . $_SESSION[USER_ID] . "'");
//
//            //======================insert into order detials table=================
//            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId = '" . $orderId . "'");
//            $numRecord = $this->num_rows($basketResultSet);
//            if ($numRecord > 0) {
//                while ($basketRow = $this->fetch_object($basketResultSet)) {
//                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";
//                    $this->executeQry($orderDetailQuery);
//                    $orderDetailId = mysql_insert_id();
//
//                    //=====================insert into order size table===============
//                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
//                    $orderSizeNum = $this->num_rows($orderSizeResult);
//                    if ($orderSizeNum > 0) {
//                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
//                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
//                            $this->executeQry($orderSizeQuery);
//                        }
//                    }
//
//                    //=====================insert into order color table===============
//                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
//                    $orderColorNum = $this->num_rows($orderColorResult);
//                    if ($orderColorNum > 0) {
//                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
//                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
//                            $this->executeQry($orderColorQuery);
//                        }
//                    }
//
//                    //=====================insert into order deco table===============
//                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
//                    $orderDecoNum = $this->num_rows($orderDecoResult);
//                    if ($orderDecoNum > 0) {
//                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
//                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
//                            $this->executeQry($orderDecoQuery);
//                        }
//                    }
//                }
//            }
//            $saveOrderId = $orderId;            
//        }
//        
//        if($denominationId) {
//            $couponId = $this->fetchValue(TBL_GIFT_SEND, "id", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
//            $this->executeQry("update ".TBL_ORDER." set couponId = '".$couponId."', couponDiscount = '".$discountAmount."' where id = '".$saveOrderId."'");
//          //  $this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '".$discountAmount."', couponExpired = '1' where id = '".$couponId."'");            
//        } else {
//            $denominationId = $this->fetchValue(TBL_ORDER, "couponId", " id = '".$ordId."'");
//            //$this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '0', couponExpired = '0' where id = '".$denominationId."'");
//            $this->executeQry("update ".TBL_ORDER." set couponId = '0', couponDiscount = '0' where id = '".$saveOrderId."'");
//        }
//        
//        $id = base64_encode($saveOrderId);
//        $paymentType = $this->getPaymentType($currencyId, $totalAmount);
//        $paymentMethod = base64_encode($paymentType);
//        header("Location:sendOrder.php?id=" . $id . "&p=" . $paymentMethod);
//        exit;
//    }
    
    function saveOrder($post) {
        $hBId = explode(',', $post['basketId']);
        $bsRecord = count($hBId);

        $countryName = '';
        $discountAmount = 0;
        $discountVP = 0;
        $subTotal = 0;
        $totalQuantity = 0;
        $totalAmount = 0;
        $saveOrderId = 0;
        $currencyId = $post['currencyId'];
        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
        for ($i = 0; $i < $bsRecord; $i++) {
            $cmt = "comment_" . $hBId[$i];
            $comment = $post[$cmt];
            $this->executeQry("update " . TBL_BASKET . " set comment = '" . $comment . "' where id = '" . $hBId[$i] . "'");
        }
        
//17Oct2013
//        if (isset($post['giftVoucher']) && $post['giftVoucher'] !== '') {
//            $denominationId = $this->fetchValue(TBL_GIFT_SEND, "giftDenominationId", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
//            $discountAmount = $this->fetchValue(TBL_GIFT_DENOMINATION, "value", "id = '" . $denominationId . "'");
//        }
        
        if (isset($post['giftVoucher']) && $post['giftVoucher'] !== '') {            
            $denominationResult = $this->executeQry("select giftDenominationId, couponPriceUsed from ".TBL_GIFT_SEND." where giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
            if($this->num_rows($denominationResult) > 0) {
                $denomination = $this->fetch_object($denominationResult);
                $denominationId = $denomination->giftDenominationId;
                $usedPrice = $denomination->couponPriceUsed;
                $couponAmount = $this->fetchValue(TBL_GIFT_DENOMINATION, "value", "id = '" . $denominationId . "'");
                $discountAmount = $couponAmount - $usedPrice;
            }            
        }
        

//        if($currencyId == 1) {
//            $discountVP = $discountAmount;
//        } else {
//            $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");
//            $curValueItem = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = 1");
//            
//            $convertedDiscAmount = $discountAmount/$curValueItem;
//            $discountVP = $convertedDiscAmount * $curValue;
//        }
        
        $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");        
        $curValueItem = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = 1");        
        $convertedDiscAmount = $discountAmount/$curValueItem;        
        $discountVP = $convertedDiscAmount * $curValue;
             
        $amtResult = $this->executeQry("select userId, orderId, sum(quantity) as totalQuantity, sum(final_price) as totalAmount from " . TBL_BASKET . " where userId = " . $_SESSION['USER_ID'] . " group by userId");
        if ($amtResult) {            
            $amtObject = $this->fetch_object($amtResult);
            //======start new editing==========
            if($amtObject->totalAmount > $convertedDiscAmount) {
                $ordId = $amtObject->orderId;
                $subTotal = $amtObject->totalAmount;
                $totalQuantity = $amtObject->totalQuantity;
                $totalAmount = $subTotal - $convertedDiscAmount;                
                $vatPrice = $this->getProductVat($totalAmount, $vat);
            } else {
                $ordId = $amtObject->orderId;
                $subTotal = $amtObject->totalAmount;                
                $totalQuantity = $amtObject->totalQuantity;
                $totalAmount = 0;                
                $vatPrice = 0;
                $discountVP = $subTotal * $curValue;
            }
            //======end new editing==========
//            $subTotal = $amtObject->totalAmount;
//            $totalQuantity = $amtObject->totalQuantity;
//            $totalAmount = $subTotal - $convertedDiscAmount;
//            $ordId = $amtObject->orderId;
//            $vatPrice = $this->getProductVat($totalAmount, $vat);
        }
        $orderReceipt = $this->fetchValue(TBL_ORDER, "ordReceiptId", " id = '" . $ordId . "'");
        if ($orderReceipt) {
            $orderNo = $orderReceipt;
            //=========================update order table======================            
            $query = "update " . TBL_ORDER . " set orderDate = '" . date('Y-m-d') . "', ipAddress = '" . $_SERVER[REMOTE_ADDR] . "', countryId = '" . $post['countryId'] . "', deliveryId = '" . $_SESSION[DELIVERYTIME] . "', invoiceAddId = '" . $post['invoiceAdd'] . "', deliveryAddId = '" . $post['deliveryAdd'] . "', graficAddId = '" . $post['graficAdd'] . "', ordReceiptId = '" . $orderNo . "', currencyId = '" . $_SESSION[DEFAULTCURRENCYID] . "', langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "', ordQuantity = '" . $totalQuantity . "', ordSubTotal = '" . $subTotal . "', discount_ammount = '" . $discountVP . "', ordTotal = '" . $totalAmount . "', vat = '" . $vatPrice . "', custom_duty = '" . $post['customDuty'] . "'  where id = '" . $ordId . "'";
            $this->executeQry($query);

            //may not use
            //===================update orderId in basket table=====================
            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $ordId . "' where userId = '" . $_SESSION[USER_ID] . "'");

            //===================update order details======================                      
            $orderDetailsId = array();
            $orderIdResult = $this->executeQry("select id from " . TBL_ORDERDETAIL . " where orderId = '" . $ordId . "'");
            $orderIdNum = $this->num_rows($orderIdResult);
            if ($orderIdNum > 0) {
                while ($orderIdRow = $this->fetch_object($orderIdResult)) {
                    $orderDetailsId [] = $orderIdRow->id;
                }
            }
            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId ='" . $ordId . "'");
            $numRecord = $this->num_rows($basketResultSet);
            if ($numRecord > 0) {
                while ($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, fabricUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->fabricUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";

                    $this->executeQry($orderDetailQuery);
                    $orderDetailId = mysql_insert_id();

                    //=====================update order size table===============                
                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if ($orderSizeNum > 0) {
                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
                            $this->executeQry($orderSizeQuery);
                        }
                    }

                    //=====================update order color table===============                    
                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if ($orderColorNum > 0) {
                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
                            $this->executeQry($orderColorQuery);
                        }
                    }

                    //=====================update order deco table===============
                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
                    $orderDecoNum = $this->num_rows($orderDecoResult);
                    if ($orderDecoNum > 0) {
                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
                            $this->executeQry($orderDecoQuery);
                        }
                    }
                }
            }

            foreach ($orderDetailsId as $id) {
                $this->executeQry("delete from " . TBL_ORDERDETAIL . " where id = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_COLOR . " where orderDetailId = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_DECO . " where orderDetailId = '" . $id . "'");
            }
            $saveOrderId = $ordId;
        } else {
            //============generating order number====================            
            $countrySign = $this->fetchValue(TBL_COUNTRY, "country_2_code", " id='" . $_SESSION[ORDERCOUNTRYID] . "'");
            $date .= date('ymdhms');
            $invoiceAdd = $this->fetchValue(TBL_ADDRESS, "firma", " id = '" . $post['invoiceAdd'] . "'");
            $orderNo = $countrySign . $date . $invoiceAdd;

            //=========================insert into order table======================
            $query = "insert into " . TBL_ORDER . " (ipAddress, sessId, userId, countryId, deliveryId, invoiceAddId, deliveryAddId, graficAddId, ordReceiptId, currencyId, langId, ordQuantity, discount_ammount, ordSubTotal, ordTotal, vat, custom_duty) values ('" . $_SERVER[REMOTE_ADDR] . "', '" . session_id() . "', '" . $_SESSION[USER_ID] . "', '" . $post['countryId'] . "', '" . $_SESSION[DELIVERYTIME] . "', '" . $post['invoiceAdd'] . "', '" . $post['deliveryAdd'] . "', '" . $post['graficAdd'] . "', '" . $orderNo . "', '" . $_SESSION[DEFAULTCURRENCYID] . "', '" . $_SESSION[DEFAULTLANGUAGEID] . "', '" . $totalQuantity . "', '" . $discountVP . "', '" . $subTotal . "', '" . $totalAmount . "', '" . $vatPrice . "', '" . $post['customDuty'] . "')";
            $this->executeQry($query);
            $orderId = mysql_insert_id();

            //===================update orderId in basket table=====================
            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $orderId . "' where userId = '" . $_SESSION[USER_ID] . "'");

            //======================insert into order detials table=================
            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId = '" . $orderId . "'");
            $numRecord = $this->num_rows($basketResultSet);
            if ($numRecord > 0) {
                while ($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, fabricUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->fabricUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";
                    $this->executeQry($orderDetailQuery);
                    $orderDetailId = mysql_insert_id();

                    //=====================insert into order size table===============
                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if ($orderSizeNum > 0) {
                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
                            $this->executeQry($orderSizeQuery);
                        }
                    }

                    //=====================insert into order color table===============
                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if ($orderColorNum > 0) {
                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
                            $this->executeQry($orderColorQuery);
                        }
                    }

                    //=====================insert into order deco table===============
                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
                    $orderDecoNum = $this->num_rows($orderDecoResult);
                    if ($orderDecoNum > 0) {
                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
                            $this->executeQry($orderDecoQuery);
                        }
                    }
                }
            }
            $saveOrderId = $orderId;            
        }
        
        if($denominationId) {
            $couponId = $this->fetchValue(TBL_GIFT_SEND, "id", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
            $this->executeQry("update ".TBL_ORDER." set couponId = '".$couponId."', couponDiscount = '".$discountAmount."' where id = '".$saveOrderId."'");
            //$this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '".$discountAmount."', couponExpired = '1' where id = '".$couponId."'");
        } else {
            $denominationId = $this->fetchValue(TBL_ORDER, "couponId", " id = '".$ordId."'");
            //$this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '0', couponExpired = '0' where id = '".$denominationId."'");
            $this->executeQry("update ".TBL_ORDER." set couponId = '0', couponDiscount = '0' where id = '".$saveOrderId."'");
        }
        
        $id = base64_encode($saveOrderId);
        $paymentType = $this->getPaymentType($currencyId, $totalAmount);
        $paymentMethod = base64_encode($paymentType);
        header("Location:sendOrder.php?id=" . $id . "&p=" . $paymentMethod);
        exit;
    }
    
    function saveOrder1($post) {
        $hBId = explode(',', $post['basketId']);
        $bsRecord = count($hBId);

        $countryName = '';
        $discountAmount = 0;
        $discountVP = 0;
        $subTotal = 0;
        $totalQuantity = 0;
        $totalAmount = 0;
        $saveOrderId = 0;
        $vatAmount = 0;
        
        $currencyId = $post['currencyId'];
        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $_SESSION['ORDERCOUNTRYID'] . "'");
        for ($i = 0; $i < $bsRecord; $i++) {
            $cmt = "comment_" . $hBId[$i];
            $comment = $post[$cmt];
            $this->executeQry("update " . TBL_BASKET . " set comment = '" . $comment . "' where id = '" . $hBId[$i] . "'");
        }

        if (isset($post['giftVoucher']) && $post['giftVoucher'] !== '') {
            $denominationId = $this->fetchValue(TBL_GIFT_SEND, "giftDenominationId", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
            $discountAmount = $this->fetchValue(TBL_GIFT_DENOMINATION, "value", "id = '" . $denominationId . "'");
        }

        if($currencyId == 1) {
            $discountVP = $discountAmount;
        } else {
            $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$currencyId."'");
            $curValueItem = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = 1");
            
            $convertedDiscAmount = $discountAmount/$curValueItem;
            $discountVP = $convertedDiscAmount * $curValue;
        }
        
        $amtResult = $this->executeQry("select userId, orderId, sum(quantity) as totalQuantity, sum(final_price) as totalAmount from " . TBL_BASKET . " where userId = " . $_SESSION[USER_ID] . " group by userId");
        if ($amtResult) {
            $amtObject = $this->fetch_object($amtResult);
            $subTotal = $amtObject->totalAmount;
            $totalQuantity = $amtObject->totalQuantity;
            $totalAmount = $subTotal - $discountVP;
            $ordId = $amtObject->orderId;
            $vatPrice = $this->getProductVat($totalAmount, $vat);
            //$vatAmount;
        }
        $orderReceipt = $this->fetchValue(TBL_ORDER, "ordReceiptId", " id = '" . $ordId . "'");
        if ($orderReceipt) {
            $orderNo = $orderReceipt;
            
            //=========================update order table======================            
            $query = "update " . TBL_ORDER . " set orderDate = '" . date('Y-m-d') . "', ipAddress = '" . $_SERVER[REMOTE_ADDR] . "', countryId = '" . $post['countryId'] . "', deliveryId = '" . $_SESSION[DELIVERYTIME] . "', invoiceAddId = '" . $post['invoiceAdd'] . "', deliveryAddId = '" . $post['deliveryAdd'] . "', graficAddId = '" . $post['graficAdd'] . "', ordReceiptId = '" . $orderNo . "', currencyId = '" . $_SESSION[DEFAULTCURRENCYID] . "', langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "', ordQuantity = '" . $totalQuantity . "', ordSubTotal = '" . $subTotal . "', discount_ammount = '" . $discountVP . "', ordTotal = '" . $totalAmount . "', vat = '" . $vatPrice . "', custom_duty = '" . $post['customDuty'] . "'  where id = '" . $ordId . "'";
            $this->executeQry($query);

            //may not use
            //===================update orderId in basket table=====================
            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $ordId . "' where userId = '" . $_SESSION[USER_ID] . "'");

            //===================update order details======================                      
            $orderDetailsId = array();
            $orderIdResult = $this->executeQry("select id from " . TBL_ORDERDETAIL . " where orderId = '" . $ordId . "'");
            $orderIdNum = $this->num_rows($orderIdResult);
            if ($orderIdNum > 0) {
                while ($orderIdRow = $this->fetch_object($orderIdResult)) {
                    $orderDetailsId [] = $orderIdRow->id;
                }
            }
            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId ='" . $ordId . "'");
            $numRecord = $this->num_rows($basketResultSet);
            if ($numRecord > 0) {
                while ($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";

                    $this->executeQry($orderDetailQuery);
                    $orderDetailId = mysql_insert_id();

                    //=====================update order size table===============                
                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if ($orderSizeNum > 0) {
                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
                            $this->executeQry($orderSizeQuery);
                        }
                    }

                    //=====================update order color table===============                    
                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if ($orderColorNum > 0) {
                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
                            $this->executeQry($orderColorQuery);
                        }
                    }

                    //=====================update order deco table===============
                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
                    $orderDecoNum = $this->num_rows($orderDecoResult);
                    if ($orderDecoNum > 0) {
                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
                            $this->executeQry($orderDecoQuery);
                        }
                    }
                }
            }

            foreach ($orderDetailsId as $id) {
                $this->executeQry("delete from " . TBL_ORDERDETAIL . " where id = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_COLOR . " where orderDetailId = '" . $id . "'");
                $this->executeQry("delete from " . TBL_ORDER_DECO . " where orderDetailId = '" . $id . "'");
            }
            $saveOrderId = $ordId;
            
        } else {
            //============generating order number====================            
            $countrySign = $this->fetchValue(TBL_COUNTRY, "country_2_code", " id='" . $_SESSION[ORDERCOUNTRYID] . "'");
            $date .= date('ymdhms');
            $invoiceAdd = $this->fetchValue(TBL_ADDRESS, "firma", " id = '" . $post['invoiceAdd'] . "'");
            $orderNo = $countrySign . $date . $invoiceAdd;

            //=========================insert into order table======================
            $query = "insert into " . TBL_ORDER . " (orderDate, ipAddress, sessId, userId, countryId, deliveryId, invoiceAddId, deliveryAddId, graficAddId, ordReceiptId, currencyId, langId, ordQuantity, discount_ammount, ordSubTotal, ordTotal, vat, custom_duty) values ('" . date('Y-m-d') . "', '" . $_SERVER[REMOTE_ADDR] . "', '" . session_id() . "', '" . $_SESSION[USER_ID] . "', '" . $post['countryId'] . "', '" . $_SESSION[DELIVERYTIME] . "', '" . $post['invoiceAdd'] . "', '" . $post['deliveryAdd'] . "', '" . $post['graficAdd'] . "', '" . $orderNo . "', '" . $_SESSION[DEFAULTCURRENCYID] . "', '" . $_SESSION[DEFAULTLANGUAGEID] . "', '" . $totalQuantity . "', '" . $discountVP . "', '" . $subTotal . "', '" . $totalAmount . "', '" . $vatPrice . "', '" . $post['customDuty'] . "')";
            $this->executeQry($query);
            $orderId = mysql_insert_id();

            //===================update orderId in basket table=====================
            $this->executeQry("update " . TBL_BASKET . " set orderId = '" . $orderId . "' where userId = '" . $_SESSION[USER_ID] . "'");

            //======================insert into order detials table=================
            $basketResultSet = $this->executeQry("select * from " . TBL_BASKET . " where userId = '" . $_SESSION[USER_ID] . "' and orderId = '" . $orderId . "'");
            $numRecord = $this->num_rows($basketResultSet);
            if ($numRecord > 0) {
                while ($basketRow = $this->fetch_object($basketResultSet)) {
                    $orderDetailQuery = "insert into " . TBL_ORDERDETAIL . " (orderId, rawProductId, productCode, productId, comment, quantity, base_price, plainSurcharge, quantitySurcharge, deliverySurcharge, deliverySign, decoPrice, decoSurcharge, decoUnitPrice, unit_Price, final_price) values ( '" . $basketRow->orderId . "', '" . $basketRow->rawProductId . "', '" . $basketRow->productCode . "', '" . $basketRow->mainProdId . "', '" . $basketRow->comment . "', '" . $basketRow->quantity . "', '" . $basketRow->base_price . "', '" . $basketRow->plainSurcharge . "', '" . $basketRow->quantitySurcharge . "', '" . $basketRow->deliverySurcharge . "', '" . $basketRow->deliverySign . "', '" . $basketRow->decoPrice . "', '" . $basketRow->decoSurcharge . "', '" . $basketRow->decoUnitPrice . "', '" . $basketRow->unit_price . "', '" . $basketRow->final_price . "')";
                    $this->executeQry($orderDetailQuery);
                    $orderDetailId = mysql_insert_id();

                    //=====================insert into order size table===============
                    $orderSizeResult = $this->executeQry("select * from " . TBL_BASKET_SIZE . " where basketId = '" . $basketRow->id . "'");
                    $orderSizeNum = $this->num_rows($orderSizeResult);
                    if ($orderSizeNum > 0) {
                        while ($orderSizeRow = $this->fetch_object($orderSizeResult)) {
                            $orderSizeQuery = "insert into " . TBL_ORDER_SIZE . " ( orderDetailId, sizeId, quantity) values ( '" . $orderDetailId . "', '" . $orderSizeRow->sizeId . "', '" . $orderSizeRow->quantity . "')";
                            $this->executeQry($orderSizeQuery);
                        }
                    }

                    //=====================insert into order color table===============
                    $orderColorResult = $this->executeQry("select * from " . TBL_BASKET_COLOR . " where basketId = '" . $basketRow->id . "'");
                    $orderColorNum = $this->num_rows($orderColorResult);
                    if ($orderColorNum > 0) {
                        while ($orderColorRow = $this->fetch_object($orderColorResult)) {
                            $orderColorQuery = "insert into " . TBL_ORDER_COLOR . " ( orderDetailId, viewId, colorId, colorCode, type) values ( '" . $orderDetailId . "', '" . $orderColorRow->viewId . "', '" . $orderColorRow->colorId . "', '" . $orderColorRow->colorCode . "', '" . $orderColorRow->type . "')";
                            $this->executeQry($orderColorQuery);
                        }
                    }

                    //=====================insert into order deco table===============
                    $orderDecoResult = $this->executeQry("select * from " . TBL_BASKET_DECO . " where basketId = '" . $basketRow->id . "'");
                    $orderDecoNum = $this->num_rows($orderDecoResult);
                    if ($orderDecoNum > 0) {
                        while ($orderDecoRow = $this->fetch_object($orderDecoResult)) {
                            $orderDecoQuery = "insert into " . TBL_ORDER_DECO . " ( orderDetailId, viewId, comment, numberOfColor, type, area, price, surcharge, sign) values ( '" . $orderDetailId . "', '" . $orderDecoRow->viewId . "', '" . $orderDecoRow->comment . "', '" . $orderDecoRow->numberOfColor . "', '" . $orderDecoRow->type . "', '" . $orderDecoRow->area . "', '" . $orderDecoRow->price . "', '" . $orderDecoRow->surcharge . "', '" . $orderDecoRow->sign . "')";
                            $this->executeQry($orderDecoQuery);
                        }
                    }
                }
            }
            $saveOrderId = $orderId;
        }
                
        if($denominationId) {
            $couponId = $this->fetchValue(TBL_GIFT_SEND, "id", " giftCertificateCode = '" . $post['giftVoucher'] . "' and couponExpired = '0'");
            $this->executeQry("update ".TBL_ORDER." set couponId = '".$couponId."', couponDiscount = '".$discountAmount."' where id = '".$ordId."'");
            $this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '".$discountAmount."', couponExpired = '1' where id = '".$couponId."'");echo 1;exit;
        } else {
            $denominationId = $this->fetchValue(TBL_ORDER, "couponId", " id = '".$ordId."'");
            $this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '0', couponExpired = '0' where id = '".$denominationId."'");
            $this->executeQry("update ".TBL_ORDER." set couponId = '0', couponDiscount = '0' where id = '".$ordId."'");
        }
        $id = base64_encode($saveOrderId);
        $paymentType = $this->getPaymentType($currencyId, $totalAmount);
        $paymentMethod = base64_encode($paymentType);
        header("Location:sendOrder.php?id=" . $id . "&p=" . $paymentMethod);
        exit;
    }

    function getPaymentType($currencyId, $amount) {
        $totalAmount = 0;
        $maxAmount = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName = 'MAX_AMOUNT'");

        $curValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '" . $currencyId . "'");
        $totalAmount = $amount * $curValue;

        //$maxAmount = 100000;
        if ($totalAmount <= $maxAmount) {
            return 1;
        } else {
            return 0;
        }
    }

    function orderDetail($id) {
        $sitename = SITENAME;
        $generalObj = new GeneralFunctions();
        $query = "select * from " . TBL_ORDER . " where id = '" . $id . "'";
        $sql = $this->executeQry($query);
        $num = $this->getTotalRow($sql);
        if ($num > 0) {
            if ($line = $this->getResultObject($sql)) {
                $genTable = $line->ordTotal;
            }
        }
        return $genTable;
    }

    function getFinalProductImage($orderId) {
        $imageArr = array();
        $quantity = array();
        $sizeArr = array();
        $unitCost = array();
        $finalCost = array();
        $amount = array();
        $ctr = 0;

        $currencyId = $this->fetchValue(TBL_ORDER, "currencyId", " id = '" . $orderId . "'");
        $result = $this->executeQry("select id, productId, unit_price, final_price from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "'");
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                $imageName = $this->fetchValue(TBL_MAINPRODUCT_VIEW, "viewImage", " mainProdId = '" . $line->productId . "' order by viewId");
                $imageArr[] = '<img src="' . SITE_URL . __MAINPRODTHUMB__ . $imageName . '" alt="Front Image" title="front Image">';

                $unitCost[] = $this->getVPPrice($line->unit_price, $currencyId);
                $finalCost[] = $this->getVPPrice($line->final_price, $currencyId);
                $sizeResultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $line->id . "'");
                $size_num = $this->num_rows($sizeResultSet);
                if ($size_num > 0) {
                    while ($sizeResult = $this->fetch_object($sizeResultSet)) {
                        $sizeArr[$ctr][] = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $sizeResult->sizeId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");

                        $quantity[$ctr][] = $sizeResult->quantity;
                        $amountArr[$ctr][] = $finalCost[$ctr];
                    }
                }
                $ctr++;
            }
        }

        $html = '<table style="width:100%; font-size:12px;" class="deco_detail">';
        for ($i = 0; $i < $num; $i++) {
            $rowspan = count($sizeArr[$i]);
            //$img ='<td style="width:42%;" rowspan="'.$rowspan.'">'.$imageArr[$i].'</td>';
            for ($j = 0; $j < $rowspan; $j++) {
                $html .= '<tr>';
                if ($j == 0) {
                    $html .='<td style="width:42%;" rowspan="' . $rowspan . '">' . $imageArr[$i] . '</td>';
                }
                $html .='<td style="width:16%;">' . $sizeArr[$i][$j] . '</td>';
                $html .='<td style="width:15%;">' . $quantity[$i][$j] . '</td>';
                if ($j == 0) {
                    $html .='<td style="width:16%;" rowspan="' . $rowspan . '">' . $unitCost[$i] . '</td>';
                }
                $html .='<td style="width:11%;">' . $amountArr[$i][$j] . '</td>';
                $html .= '</tr>';
            }
        }
        $html .= '</table>';

        return $html;
    }

    function getDataArr($orderId) {
        $jsonObj = new JSON();
        $decoPrice = array();
        $currencyId = $this->fetchValue(TBL_ORDER, "currencyId", " id = '" . $orderId . "'");
        $result = $this->executeQry("select id, productCode, productId from " . TBL_ORDERDETAIL . " where orderId = {$orderId} order by id");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $ctr = 0;
            while ($line = $this->fetch_object($result)) {
                $ctr++;
                $decoImage = array();
                $dataArr = $this->fetchValue(TBL_MAINPRODUCT, "dataArr", "id = {$line->productId}");
                $deco = $jsonObj->decode($dataArr);
                foreach ($deco as $obj) {
                    $viewId = $this->fetchValue(TBL_VIEWDESC, "viewId", "viewName = '" . $obj->ViewName . "'");
                    foreach ($obj->contentArray as $img) {
                        $arr = explode("http://", $img);
                        $decoImg = str_replace("original", "thumb", $arr[1]);
                        $decoImage[$viewId][] = "http://" . strip_tags($decoImg);
                    }
                }
                $html .= $this->getPantoneNo($line->id, $decoImage, $ctr, $currencyId, $line->productCode);
            }
            return $html;
        }
    }

    function getDecoPriceById($id, $viewId) {
        $total = 0;
        $resultSet = $this->executeQry("select id, price, surcharge, sign from " . TBL_ORDER_DECO . " where orderDetailId = '" . $id . "' and viewId = '" . $viewId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($line = $this->fetch_object($resultSet)) {
                if ($line->sign == 0) {
                    $total += $line->price - $line->surcharge;
                } else if ($line->sign == 2) {
                    $total += $line->price + $line->surcharge;
                } else {
                    $total += $line->price;
                }
            }
        }
        return $total;
    }

    //===========START testing=================    
    function getDecoTypeById($id) {
        $type = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " id = '" . $id . "'");
        return $type;
    }

    function getProductColorById($id, $viewId) {
        $colorCode = '';
        $resultSet = $this->executeQry("select id, colorCode from " . TBL_ORDER_COLOR . " where orderDetailId = '" . $id . "' and viewId = '" . $viewId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 1;
            while ($line = $this->fetch_object($resultSet)) {
                if ($ctr == $num) {
                    $colorCode .= $line->colorCode;
                } else {
                    $colorCode .= $line->colorCode . ", ";
                }
                $ctr++;
            }
        }
        return $colorCode;
    }

    function getProductComment($orderId) {
        $result = $this->executeQry("select id, productCode, comment from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table  width="100%;">';
            while ($line = $this->fetch_object($result)) {
                $html .= '<tr>';
                $html .= '<td style="width:4%; text-align:center; padding-right:12px;">' . $ctr . '</td>';
                $html .= '<td style="width:12%; padding-right:12px;">' . $line->productCode . '</td>';
                $html .= '<td style="width:62%;">' . $line->comment . '</td>';
                $html .= '</tr>';
                $ctr++;
            }
            $html .='</table>';
        }
        return $html;
    }

    function getProductViewComment($orderId) {
        $result = $this->executeQry("select id, productCode from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table width="100%;">';
            while ($line = $this->fetch_object($result)) {
                $rs = $this->executeQry("select comment, viewId from " . TBL_ORDER_DECO . " where orderDetailId = '" . $line->id . "' order by viewId");
                $viewNum = $this->num_rows($rs);
                if ($viewNum > 0) {
                    while ($row = $this->fetch_object($rs)) {
                        $viewName = $this->fetchValue(TBL_VIEWDESC, "viewName", " viewId = '" . $row->viewId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
                        $html .= '<tr>';
                        $html .= '<td style="width:4%; text-align:center; padding-right:12px;">' . $ctr . '</td>';
                        $html .= '<td style="width:12%; padding-right:12px;">' . $line->productCode . '</td>';
                        $html .= '<td style="width:10%; padding-right:12px;">' . $viewName . '</td>';
                        $html .= '<td style="width:50%;">' . $row->comment . '</td>';
                        $html .= '</tr>';
                        $ctr++;
                    }
                }
            }
            $html .='</table>';
        }
        return $html;
    }

    function isBasketEmpty($bId) {
        $quantity = 0;
        $basketId = explode(',', $bId);
        $num = count($basketId);
        $id = '';
        for ($i = 1; $i < $num; $i++) {
            if ($i == 1) {
                $id = $basketId[$i];
            } else {
                $id .= ", " . $basketId[$i];
            }
        }

        $result = $this->executeQry("select sum(quantity) as quantity from " . TBL_BASKET . " where id in (" . $id . ")");
        $num = $this->num_rows($result);
        if ($num > 0) {
            $line = $this->fetch_object($result);
            $quantity = $line->quantity;
        }
        return $quantity;
    }
    
    function getCouponHtml($couponId) {        
        $html = '';
        $couponQuery = $this->executeQry("select * from ".TBL_GIFT_SEND." where id = '".$couponId."'");
        if($this->num_rows($couponQuery) > 0) {
            $couponResult = $this->fetch_object($couponQuery);
            $html = '<table style="width:100%; font-size:12px;"><tr>';            
            $html .='<td style="width:19%; text-align:left; padding-right:20px;"> '.$couponResult->giftCertificateCode . ' </td>';
            $html .='<td style="width:17%; text-align:left; padding-right:20px;"> '.$couponResult->couponExpiryDate.' </td>';
            $html .='<td style="width:33%; text-align:left; padding-right:20px;">' . $couponResult->toEmail . '</td>';
            $html .= '</tr></table>';
        } else {
            $html = '<table style="width:100%; font-size:12px;"><tr>';            
            $html .='<td style="width:19%; text-align:left; padding-right:20px;"> NA </td>';
            $html .='<td style="width:17%; text-align:left; padding-right:20px;"> NA </td>';
            $html .='<td style="width:33%; text-align:left; padding-right:20px;"> NA </td>';
            $html .= '</tr></table>';
        }
        
        return $html;
    }
    
    
//  15 Jan 2014
//    function getAddressDetail($oId, $uId, $inId, $deId, $grId, $message) {
//        $html = $message;
//        $inResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $inId . "'");
//        $deResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $deId . "'");
//        $grResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $grId . "'");
//        $invoice = $this->fetch_object($inResultSet);
//        $delivery = $this->fetch_object($deResultSet);
//        $grafic = $this->fetch_object($grResultSet);
//
//        $uresult = $this->executeQry("select firstName, lastName from " . TBL_USER . " where id = '" . $uId . "'");
//        $user = $this->fetch_object($uresult);
//        $orderResultSet = $this->executeQry("select orderDate, countryId, deliveryId, ordReceiptId, paymentType, currencyId, ordSubTotal, discount_ammount, ordTotal, vat, custom_duty, couponId from " . TBL_ORDER . " where id ='" . $oId . "'");
//        $order = $this->fetch_object($orderResultSet);
//        $orderReceipt = $order->ordReceiptId;
//        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $order->countryId . "'");
//        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $order->countryId . "'");
//        $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $order->countryId . "'");        
//        
//        $customH = "Custom Duty " . $customDuty . "% in (" . $countryName . "): ";        
//        $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id = '" . $order->deliveryId . "'");
//        if ($deliveryTime == 1) {
//            $deliveryTime = $deliveryTime . " Day";
//        } else {
//            $deliveryTime = $deliveryTime . " Days";
//        }
//
//        $orderDate = $order->orderDate;
//        $userName = stripslashes($user->firstName) . " " . stripslashes($user->lastName);
//
//        $inTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $invoice->title . "' and langId = 1");
//        $inName = $invoice->firstName . " " . $invoice->lastName;
//        $inCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $invoice->countryId . "' and langId = 1");
//        $inLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $invoice->language . "'");
//
//        $deTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $delivery->title . "' and langId = 1");
//        $deName = $delivery->firstName . " " . $delivery->lastName;
//        $deCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $delivery->countryId . "' and langId = 1");
//        $deLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $delivery->language . "'");
//
//        $grTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $grafic->title . "' and langId = 1");
//        $grName = $grafic->firstName . " " . $grafic->lastName;
//        $grCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $grafic->countryId . "' and langId = 1");
//        $grLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $grafic->language . "'");
//
//
//
//        $productComment = $this->getProductComment($oId);
//        $viewComment = $this->getProductViewComment($oId);
//        $fabricDetail = $this->getFabricDetailById($oId, $order->currencyId, $order->countryId);        
//        $decoData = $this->getDecoDetailById($oId, $order->currencyId, $order->countryId);        
//        $orderData = $this->getFinalProductInfo($oId);        
//        $couponData = $this->getCouponHtml($order->couponId);
//        
//        $ineori = $deeori = $greori = 'NA';
//        
//        if($invoice->eoriNo) {
//            $ineori = $invoice->eoriNo;    
//        }
//        if($delivery->eoriNo) {
//           $deeori = $delivery->eoriNo;
//        }
//        if($grafic->eoriNo) {
//            $greori = $grafic->eoriNo;    
//        }
//                
//        $html = str_replace("[SITE_URL]", SITE_URL, $html);
//        $html = str_replace("[LOGING_YOUR_ACCOUNT]", LANG_LOGING_YOUR_ACCOUNT, $html);
//        $html = str_replace("[username]", $userName, $html);
//        $html = str_replace("[ORDERID]", $orderReceipt, $html);
//        $html = str_replace("[ORDERDATE]", $orderDate, $html);
//        $html = str_replace("[DELIVERY_TIME]", $deliveryTime, $html);
//
//        $html = str_replace("[IN_FIRMA]", stripslashes($invoice->firma), $html);
//        $html = str_replace("[IN_TITLE]", $inTitle, $html);
//        $html = str_replace("[IN_NAME]", stripslashes($inName), $html);
//        $html = str_replace("[IN_EMAIL]", stripslashes($invoice->email), $html);
//        $html = str_replace("[IN_ADDRESS]", stripslashes($invoice->address), $html);
//        if ($invoice->address1) {
//            $html = str_replace("[IN_ADDRESS1]", stripslashes($invoice->address1), $html);
//        }
//        $html = str_replace("[IN_ZIP]", stripslashes($invoice->zip), $html);
//        $html = str_replace("[IN_CITY]", stripslashes($invoice->city), $html);
//        $html = str_replace("[IN_COUNTRY]", stripslashes($inCountry), $html);
//        $html = str_replace("[IN_PHONE]", stripslashes($invoice->phoneNo), $html);
//        $html = str_replace("[IN_MOBILE]", stripslashes($invoice->mobile), $html);
//        $html = str_replace("[IN_EORI]", stripslashes($ineori), $html);
//        $html = str_replace("[IN_LANGUAGE]", stripslashes($inLang), $html);
//
//        $html = str_replace("[DE_FIRMA]", stripslashes($delivery->firma), $html);
//        $html = str_replace("[DE_TITLE]", $deTitle, $html);
//        $html = str_replace("[DE_NAME]", stripslashes($deName), $html);
//        $html = str_replace("[DE_EMAIL]", stripslashes($delivery->email), $html);
//        $html = str_replace("[DE_ADDRESS]", stripslashes($delivery->address), $html);
//        if ($delivery->address1) {
//            $html = str_replace("[DE_ADDRESS1]", stripslashes($delivery->address1), $html);
//        }
//        $html = str_replace("[DE_ZIP]", stripslashes($delivery->zip), $html);
//        $html = str_replace("[DE_CITY]", stripslashes($delivery->city), $html);
//        $html = str_replace("[DE_COUNTRY]", stripslashes($deCountry), $html);
//        $html = str_replace("[DE_PHONE]", stripslashes($delivery->phoneNo), $html);
//        $html = str_replace("[DE_MOBILE]", stripslashes($delivery->mobile), $html);
//        $html = str_replace("[DE_EORI]", stripslashes($deeori), $html);
//        $html = str_replace("[DE_LANGUAGE]", stripslashes($deLang), $html);
//
//        $html = str_replace("[GR_FIRMA]", stripslashes($grafic->firma), $html);
//        $html = str_replace("[GR_TITLE]", $grTitle, $html);
//        $html = str_replace("[GR_NAME]", stripslashes($grName), $html);
//        $html = str_replace("[GR_EMAIL]", stripslashes($grafic->email), $html);
//        $html = str_replace("[GR_ADDRESS]", stripslashes($grafic->address), $html);
//        if ($grafic->address1) {
//            $html = str_replace("[GR_ADDRESS1]", stripslashes($grafic->address1), $html);
//        }
//        $html = str_replace("[GR_ZIP]", stripslashes($grafic->zip), $html);
//        $html = str_replace("[GR_CITY]", stripslashes($grafic->city), $html);
//        $html = str_replace("[GR_COUNTRY]", stripslashes($grCountry), $html);
//        $html = str_replace("[GR_PHONE]", stripslashes($grafic->phoneNo), $html);
//        $html = str_replace("[GR_MOBILE]", stripslashes($grafic->mobile), $html);
//        $html = str_replace("[GR_EORI]", stripslashes($greori), $html);
//        $html = str_replace("[GR_LANGUAGE]", stripslashes($grLang), $html);
//        
//
//        $discountAmount = $order->discount_ammount;
//        $excludingVatPrice = $order->ordSubTotal;
//        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$order->currencyId."'");        
//        $discountVP = $discountAmount/$currencyValue;
//        $priceAfterDiscount = $excludingVatPrice - $discountVP;
//        
//        //$priceAfterDiscount = $excludingVatPrice - $discountAmount;
//        $vatPrice = $this->getProductVat($priceAfterDiscount, $vat);
//        $totalPrice = $priceAfterDiscount + $vatPrice;
//            
//        $html = str_replace("[PAYMENTTYPE]", stripslashes($order->paymentType), $html);
////        $html = str_replace("[SUBTOTAL]", stripslashes($this->getPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);
//        
//        $html = str_replace("[SUBTOTAL]", stripslashes($this->getRoundPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);        
//        $html = str_replace("[SUBTOTAL_H]", stripslashes(LANG_SUB_TOTAL), $html);
//        $html = str_replace("[VAT]", stripslashes($this->getPriceWithSign($order->vat, $order->currencyId)), $html);
//        $html = str_replace("[VAT_H]", stripslashes($vat." % ". LANG_VAT_DISCOUNT), $html);
//        $html = str_replace("[CUSTOMDUTY]", stripslashes($this->getPriceWithSign($order->custom_duty, $order->currencyId)), $html);
//        $html = str_replace("[CUSTOMDUTY_H]", stripslashes($customH), $html);
//        
//        $html = str_replace("[COUPANVALUE_H]", stripslashes(LANG_COUPANVALUE_H), $html);
//        $html = str_replace("[COUPANVALUE]", stripslashes($this->getPriceSign($discountAmount)), $html);
//        $html = str_replace("[GRANDTOTAL]", stripslashes($this->getRoundPriceWithSign($totalPrice, $order->currencyId)), $html);
//        $html = str_replace("[GRANDTOTAL_H]", stripslashes(LANG_TOTAL_ORDER), $html);
//        $html = str_replace("[LOGOPATH]", SITE_URL . "images/logo.jpg", $html);
//
//        $html = str_replace("[PRODUCT_COMMENT]", $productComment, $html);
//        $html = str_replace("[VIEW_COMMENT]", $viewComment, $html);
//        $html = str_replace("[FABRIC_DETAIL]", $fabricDetail, $html);
//        $html = str_replace("[DECO_DETAIL]", $decoData, $html);
//        $html = str_replace("[ITEMS]", $orderData, $html);
//        $html = str_replace("[COUPON_DETAIL]", $couponData, $html);
//        $html = str_replace("[TOTAL_AFTER_DISCOUNT]", LANG_TOTAL_ORDER_AFTER_DISCOUNT, $html);//TOTAL_AFTER_DISCOUNT
//        //$html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getPriceWithSign($order->ordTotal, $order->currencyId)), $html);
//        $html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getRoundPriceWithSign($order->ordTotal, $order->currencyId)), $html);
//        
//        return $html;
//    }
//
//    function sendOrderConfirmation($oId, $uId, $paymentMethod) {
//        $mailFunctionObj = new MailFunction;
//        if ($paymentMethod == "paypal") {
//            $mailFunctionObj->mailValue("11", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
//        } else {
//            $mailFunctionObj->mailValue("11", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
//            //$mailFunctionObj->mailValue("18", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
//        }
//        //$_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_EMAIL_TO_RESET_PASSWORD_MSG);
//    }
    
    function getAddressDetail($oId, $uId, $inId, $deId, $grId, $message) {
        $html = $message;
        $inResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $inId . "'");
        $deResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $deId . "'");
        $grResultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $grId . "'");
        $invoice = $this->fetch_object($inResultSet);
        $delivery = $this->fetch_object($deResultSet);
        $grafic = $this->fetch_object($grResultSet);

        $uresult = $this->executeQry("select firstName, lastName from " . TBL_USER . " where id = '" . $uId . "'");
        $user = $this->fetch_object($uresult);
        $orderResultSet = $this->executeQry("select orderDate, countryId, deliveryId, ordReceiptId, paymentType, currencyId, ordSubTotal, discount_ammount, ordTotal, vat, custom_duty, couponId from " . TBL_ORDER . " where id ='" . $oId . "'");
        $order = $this->fetch_object($orderResultSet);
        $orderReceipt = $order->ordReceiptId;
        $vat = $this->fetchValue(TBL_COUNRTYSETTING, "vat", " countryId = '" . $order->countryId . "'");
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", " countryId = '" . $order->countryId . "'");
        $countryName = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $order->countryId . "'");        
        
        $customH = "Custom Duty " . $customDuty . "% in (" . $countryName . "): ";        
        $deliveryTime = $this->fetchValue(TBL_DELIVERY_TIME, "days", " id = '" . $order->deliveryId . "'");
        if ($deliveryTime == 1) {
            $deliveryTime = $deliveryTime . " Day";
        } else {
            $deliveryTime = $deliveryTime . " Days";
        }

        $orderDate = $order->orderDate;
        $userName = stripslashes($user->firstName) . " " . stripslashes($user->lastName);

        $inTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $invoice->title . "' and langId = 1");
        $inName = $invoice->firstName . " " . $invoice->lastName;
        $inCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $invoice->countryId . "' and langId = 1");
        $inLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $invoice->language . "'");

        $deTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $delivery->title . "' and langId = 1");
        $deName = $delivery->firstName . " " . $delivery->lastName;
        $deCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $delivery->countryId . "' and langId = 1");
        $deLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $delivery->language . "'");

        $grTitle = $this->fetchValue(TBL_TITLEDESC, "titleName", " id = '" . $grafic->title . "' and langId = 1");
        $grName = $grafic->firstName . " " . $grafic->lastName;
        $grCountry = $this->fetchValue(TBL_COUNTRY_DESCRIPTION, "countryName", " countryId = '" . $grafic->countryId . "' and langId = 1");
        $grLang = $this->fetchValue(TBL_LANGUAGE, "languageName", " id = '" . $grafic->language . "'");

        $productComment = $this->getProductComment($oId);
        $viewComment = $this->getProductViewComment($oId);
        
        $fabricObj = $this->getFabricDetailById($oId, $order->currencyId, $order->countryId,'Obj');  
      
        $fabricDetail = $fabricObj->html;
        $show_totalPrice = array();
        $show_totalPrice['fab'] = $fabricObj->totalfabricprice;
        
       
        
        $decoDataObj = $this->getDecoDetailById($oId, $order->currencyId, $order->countryId,'Obj');

        $decoData = $decoDataObj->html;
        
        $show_totalPrice['deco'] = $decoDataObj->price;

        $orderDataObj = $this->getFinalProductInfo($oId,$show_totalPrice);   
       
        $orderData = $orderDataObj->html;
        
        $totalCalculatedP = $orderDataObj->price;
        $couponData = $this->getCouponHtml($order->couponId);
        
        $ineori = $deeori = $greori = 'NA';
        
        if($invoice->eoriNo) {
            $ineori = $invoice->eoriNo;    
        }
        if($delivery->eoriNo) {
           $deeori = $delivery->eoriNo;
        }
        if($grafic->eoriNo) {
            $greori = $grafic->eoriNo;    
        }
                
        $html = str_replace("[SITE_URL]", SITE_URL, $html);
        $html = str_replace("[LOGING_YOUR_ACCOUNT]", LANG_LOGING_YOUR_ACCOUNT, $html);
        $html = str_replace("[username]", $userName, $html);
        $html = str_replace("[ORDERID]", $orderReceipt, $html);
        $html = str_replace("[ORDERDATE]", $orderDate, $html);
        $html = str_replace("[DELIVERY_TIME]", $deliveryTime, $html);

        $html = str_replace("[IN_FIRMA]", stripslashes($invoice->firma), $html);
        $html = str_replace("[IN_TITLE]", $inTitle, $html);
        $html = str_replace("[IN_NAME]", stripslashes($inName), $html);
        $html = str_replace("[IN_EMAIL]", stripslashes($invoice->email), $html);
        $html = str_replace("[IN_ADDRESS]", stripslashes($invoice->address), $html);
        if ($invoice->address1) {
            $html = str_replace("[IN_ADDRESS1]", stripslashes($invoice->address1), $html);
        }
        $html = str_replace("[IN_ZIP]", stripslashes($invoice->zip), $html);
        $html = str_replace("[IN_CITY]", stripslashes($invoice->city), $html);
        $html = str_replace("[IN_COUNTRY]", stripslashes($inCountry), $html);
        $html = str_replace("[IN_PHONE]", stripslashes($invoice->phoneNo), $html);
        $html = str_replace("[IN_MOBILE]", stripslashes($invoice->mobile), $html);
        $html = str_replace("[IN_EORI]", stripslashes($ineori), $html);
        $html = str_replace("[IN_LANGUAGE]", stripslashes($inLang), $html);

        $html = str_replace("[DE_FIRMA]", stripslashes($delivery->firma), $html);
        $html = str_replace("[DE_TITLE]", $deTitle, $html);
        $html = str_replace("[DE_NAME]", stripslashes($deName), $html);
        $html = str_replace("[DE_EMAIL]", stripslashes($delivery->email), $html);
        $html = str_replace("[DE_ADDRESS]", stripslashes($delivery->address), $html);
        if ($delivery->address1) {
            $html = str_replace("[DE_ADDRESS1]", stripslashes($delivery->address1), $html);
        }
        $html = str_replace("[DE_ZIP]", stripslashes($delivery->zip), $html);
        $html = str_replace("[DE_CITY]", stripslashes($delivery->city), $html);
        $html = str_replace("[DE_COUNTRY]", stripslashes($deCountry), $html);
        $html = str_replace("[DE_PHONE]", stripslashes($delivery->phoneNo), $html);
        $html = str_replace("[DE_MOBILE]", stripslashes($delivery->mobile), $html);
        $html = str_replace("[DE_EORI]", stripslashes($deeori), $html);
        $html = str_replace("[DE_LANGUAGE]", stripslashes($deLang), $html);

        $html = str_replace("[GR_FIRMA]", stripslashes($grafic->firma), $html);
        $html = str_replace("[GR_TITLE]", $grTitle, $html);
        $html = str_replace("[GR_NAME]", stripslashes($grName), $html);
        $html = str_replace("[GR_EMAIL]", stripslashes($grafic->email), $html);
        $html = str_replace("[GR_ADDRESS]", stripslashes($grafic->address), $html);
        if ($grafic->address1) {
            $html = str_replace("[GR_ADDRESS1]", stripslashes($grafic->address1), $html);
        }
        $html = str_replace("[GR_ZIP]", stripslashes($grafic->zip), $html);
        $html = str_replace("[GR_CITY]", stripslashes($grafic->city), $html);
        $html = str_replace("[GR_COUNTRY]", stripslashes($grCountry), $html);
        $html = str_replace("[GR_PHONE]", stripslashes($grafic->phoneNo), $html);
        $html = str_replace("[GR_MOBILE]", stripslashes($grafic->mobile), $html);
        $html = str_replace("[GR_EORI]", stripslashes($greori), $html);
        $html = str_replace("[GR_LANGUAGE]", stripslashes($grLang), $html);
        

        $discountAmount = $order->discount_ammount;
        $excludingVatPrice = $order->ordSubTotal;
        $currencyValue = $this->fetchValue(TBL_VALUE_POINTS, "value", " cur_id = '".$order->currencyId."'");        
        $discountVP = $discountAmount/$currencyValue;
        $priceAfterDiscount = $excludingVatPrice - $discountVP;
        
        //$priceAfterDiscount = $excludingVatPrice - $discountAmount;
        $vatPrice = $this->getProductVat($priceAfterDiscount, $vat);
        $totalPrice = $priceAfterDiscount + $vatPrice;
        
        $totalafdis = $totalCalculatedP-$discountAmount;
        
        $grandtotal = ($totalafdis/$currencyValue)+$order->vat;
            
        $html = str_replace("[PAYMENTTYPE]", stripslashes($order->paymentType), $html);
        $html = str_replace("[SUBTOTAL]", stripslashes($this->getPriceWithSign(($totalCalculatedP/$currencyValue), $order->currencyId)), $html);
        
        //$html = str_replace("[SUBTOTAL]", stripslashes($this->getRoundPriceWithSign($order->ordSubTotal, $order->currencyId)), $html);        
        $html = str_replace("[SUBTOTAL_H]", stripslashes(LANG_SUB_TOTAL), $html);
        $html = str_replace("[VAT]", stripslashes($this->getPriceWithSign($order->vat, $order->currencyId)), $html);
        $html = str_replace("[VAT_H]", stripslashes($vat." % ". LANG_VAT_DISCOUNT), $html);
        $html = str_replace("[CUSTOMDUTY]", stripslashes($this->getPriceWithSign($order->custom_duty, $order->currencyId)), $html);
        $html = str_replace("[CUSTOMDUTY_H]", stripslashes($customH), $html);
        
        $html = str_replace("[COUPANVALUE_H]", stripslashes(LANG_COUPANVALUE_H), $html);
        $html = str_replace("[COUPANVALUE]", stripslashes($this->getPriceSign($discountAmount)), $html);
        $html = str_replace("[GRANDTOTAL]", stripslashes($this->getPriceWithSign($grandtotal, $order->currencyId)), $html);
        $html = str_replace("[GRANDTOTAL_H]", stripslashes(LANG_TOTAL_ORDER), $html);
        $html = str_replace("[LOGOPATH]", SITE_URL . "images/logo.jpg", $html);

        $html = str_replace("[PRODUCT_COMMENT]", $productComment, $html);
        $html = str_replace("[VIEW_COMMENT]", $viewComment, $html);
        $html = str_replace("[FABRIC_DETAIL]", $fabricDetail, $html);
        $html = str_replace("[DECO_DETAIL]", $decoData, $html);
        $html = str_replace("[ITEMS]", $orderData, $html);
        $html = str_replace("[COUPON_DETAIL]", $couponData, $html);
        $html = str_replace("[TOTAL_AFTER_DISCOUNT]", LANG_TOTAL_ORDER_AFTER_DISCOUNT, $html);//TOTAL_AFTER_DISCOUNT
        $html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getPriceWithSign(($totalafdis/$currencyValue), $order->currencyId)), $html);
        //$html = str_replace("[TOTAL_AFTER_DISCOUNT_VALUE]", stripslashes($this->getRoundPriceWithSign($order->ordTotal, $order->currencyId)), $html);
        
        return $html;
    }

    function sendOrderConfirmation($oId, $uId, $paymentMethod) {
        $mailFunctionObj = new MailFunction;
        if ($paymentMethod == "paypal") {
            $mailFunctionObj->mailValue("11", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
        } else {
            $mailFunctionObj->mailValue("11", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
            //$mailFunctionObj->mailValue("18", $_SESSION[DEFAULTLANGUAGEID], $oId, $uId);
        }
        //$_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_EMAIL_TO_RESET_PASSWORD_MSG);
    }

    function checkAddress() {
        $orderId = $this->fetchValue(TBL_BASKET, "orderId", "userId = '" . $_SESSION[USER_ID] . "'");
        if ($orderId != '0') {
            $addReslutSet = $this->executeQry("select invoiceAddId, deliveryAddId, graficAddId from " . TBL_ORDER . " where id = '" . $orderId . "'");
            $addReslut = $this->fetch_object($addReslutSet);
            return $addReslut;
        }
    }

    function deleteProdBasket($basketId) {
        $mainProdId = $this->fetchValue(TBL_BASKET, "mainProdId", " id = '" . $basketId . "'");
        $this->executeQry("delete from " . TBL_BASKET . " where id = '" . $basketId . "'");
        $this->executeQry("delete from " . TBL_BASKET_COLOR . " where basketId = '" . $basketId . "'");
        $this->executeQry("delete from " . TBL_BASKET_SIZE . " where basketId = '" . $basketId . "'");
        $this->executeQry("delete from " . TBL_MAINPRODUCT . " where id = '" . $mainProdId . "'");
        $this->executeQry("delete from " . TBL_MAINPRODUCT_VIEW . " where mainProdId = '" . $mainProdId . "'");
    }

    function getOrderAmountDetail($orderId) {
        $resultSet = $this->executeQry("select * from " . TBL_ORDER . " where id = '" . $orderId . "'");
        $result = $this->fetch_object($resultSet);
        return $result;
    }

    function invoiceDetails($invoiceId) {
        $resultSet = $this->executeQry("select * from " . TBL_ADDRESS . " where id = '" . $invoiceId . "'");
        $result = $this->fetch_object($resultSet);
        return $result;
    }

    function saveFinalOrder($orderId, $paymentType, $transNo = '') {
        $resultSet = $this->executeQry("select id from " . TBL_BASKET . " where orderId = '" . $orderId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($row = $this->fetch_object($resultSet)) {
                $this->executeQry("delete from " . TBL_BASKET_DECO . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_DECO_QTY . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_COLOR . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_SIZE . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET . " where id = '" . $row->id . "'");
            }
            
            if(isset($_COOKIE ['collection'])){
                $collectionName = $_COOKIE ['collection'];
            }else{
                $collectionName = '';
            }
                        
            if ($paymentType == 'paypal') {
                $this->executeQry("update " . TBL_ORDER . " set paymentStatus = '1', paymentMessage = 'Completed', transactionNo = '" . $transNo . "', paymentType = '" . $paymentType . "', collectionName = '".$collectionName."' where id = '" . $orderId . "'");
            } else if ($paymentType == "bank") {
                $this->executeQry("update " . TBL_ORDER . " set paymentStatus = '2', paymentMessage = 'Completed', paymentType = '" . $paymentType . "', collectionName = '".$collectionName."' where id = '" . $orderId . "'");
            }
            
            $this->updateValue(TBL_COLLECTION, "collectionName = ".$collectionName, "is_order = '2'");
            $this->sendOrderConfirmation($orderId, $_SESSION[USER_ID], $paymentType);
            $this->updateCouponPrice($orderId);
        }
        //echo "<script>window.location.href='http://www.shirtlabweb.com/thanks.php?oId=".base64_encode($orderId)."'</script>";
        redirect("thanks.php?oId=".base64_encode($orderId));exit;
        //echo "<script>window.location.href='thanks.php?oId=" . base64_encode($orderId) . "'</script>";
        exit;
    }
    
    function updateCouponPrice($orderId) {
        $orderRs = $this->executeQry("select couponId, couponDiscount, ordSubTotal from ".TBL_ORDER." where id = '".$orderId."'");
        if($this->num_rows($orderRs) > 0) {
            $order = $this->fetch_object($orderRs);
            if($order->couponId) {
                $couponVP = $this->priceToValuePoint($order->couponDiscount, 1);
                $usedPrice = $this->fetchValue(TBL_GIFT_SEND, "couponPriceUsed", " id = '".$order->couponId."'");
                if($couponVP > $order->ordSubTotal) {                    
                    $price = $this->valuePointToPrice($order->ordSubTotal, 1);
                    $total = $price + $usedPrice;
                    $this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '".$total."' where id = '".$order->couponId."'");
                } else {
                    $total = $order->couponDiscount + $usedPrice;
                    $this->executeQry("update ".TBL_GIFT_SEND." set couponPriceUsed = '".$total."', couponExpired = '1' where id = '".$order->couponId."'");
                }
            }
        }
    }

    //=====generate invoice=======
    function getFabricDetailById($orderId, $curId, $countryId, $returntype='nor') {
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", "countryId = '" . $countryId . "'");
        $query = "select id, rawProductId, productCode, productId, quantity, base_price, quantitySurcharge, deliverySurcharge, deliverySign, fabricUnitPrice from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        $obj = (object) $obj;
        $totalfabricprice = array();
        if ($num > 0) {
            $html = '';
            $ctr = 1;
            $html .='<table width="100%;">';
            while ($line = $this->fetch_object($resultSet)) {
                $qualityId = $this->fetchValue(TBL_MAINPRODUCT, "qualityId", " id = '" . $line->productId . "'");
                $quality = $this->fetchValue(TBL_FABRICCHARGE, "fQuality", " id = '" . $qualityId . "'");

                $price = $line->base_price + $line->plainSurcharge;
                $cDuty = ($price * $customDuty) / 100;
                $basePrice = $price + $cDuty;
                $fabric_price = $this->getVPPrice($basePrice, $curId);
                $deSurcharge = $this->getVPPrice($line->deliverySurcharge, $curId);
                $qtySurcharge = $this->getVPPrice($line->quantitySurcharge, $curId);
                $totalVP = $line->base_price + $line->quantitySurcharge;
                $total = $this->getVPPrice($totalVP, $curId);
                $total = $fabric_price + $qtySurcharge;
                if ($line->deliverySign == 0) {
                    if ($deSurcharge != 0) {
                        $deSurcharge = "- " . $deSurcharge;
                    }
                    $total = $total - $deSurcharge;
                } else if ($line->deliverySign == 2) {
                    $total = $total + $deSurcharge;
                }
                $total = number_format($total * $line->quantity, 2);

                $html .= '<tr>';
                $html .= '<td style="width:5%; text-align:center; padding-right:2px;">' . $ctr . '</td>';
                $html .= '<td style="width:14%;">' . $line->productCode . '</td>';
                $html .= '<td style="width:20%; padding-right:1%;">' . $quality . '</td>';
                $html .= '<td style="width:15%; padding-right:2px; padding-left:4%;">' . $line->quantity . '</td>';
                $html .= '<td style="width:10%; padding-right:2px; padding-left:3%;">' . $line->fabricUnitPrice . '</td>';
                //$html .= '<td style="width:10%; padding-right:2px; padding-left:2.5%;">'.$deSurcharge.'</td>';
                $html .= '<td style="width:10%; padding-right:2px; padding-left:2.5%;">' . $deSurcharge . '</td>';

                $html .= '<td style="width:10%; padding-right:2px;  padding-left:2%;">' . $qtySurcharge . '</td>';
                $html .= '<td style="width:10%;  padding-left:2%; color:#ffffff;">' . ($line->fabricUnitPrice+$deSurcharge+$qtySurcharge) . '</td>';
                $html .= '</tr>';
               
                $totalfabricprice[$line->id] = ($line->fabricUnitPrice*$line->quantity)+$deSurcharge+$qtySurcharge;
                $ctr++;
            }
            $html .='</table>';
        }
        if($returntype=='Obj'){
        	$obj->totalfabricprice=$totalfabricprice;
        	$obj->html=$html; 
        	return $obj;           	
        }else{
        return $html;
        }
    }

    function getDecoDetailById($orderId, $currencyId, $countryId,$returnType='nor') {
        $customDuty = $this->fetchValue(TBL_COUNRTYSETTING, "customDuty", "countryId = '" . $countryId . "'");
        $jsonObj = new JSON();
        $decoPrice = array();
        $obj = (object) $obj;
        $html = '';
        $query = "select id, productCode, productId from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 0;
            while ($line = $this->fetch_object($resultSet)) {
                $decoImage = array();
                $dataArr = $this->fetchValue(TBL_MAINPRODUCT, "dataArr", "id = {$line->productId}");
                $deco = $jsonObj->decode($dataArr);
                $num = count($deco);
                if ($num > 0) {
                    foreach ($deco as $obj) {
                        $viewId = $this->fetchValue(TBL_VIEWDESC, "viewId", "viewName = '" . $obj->ViewName . "'");
                        foreach ($obj->contentArray as $img) {
                            $arr = explode("http://", $img);
                            $decoImg = str_replace("original", "thumb", $arr[1]);
                            $decoImage[$viewId][] = "http://" . strip_tags($decoImg);
                        }
                    }
                    
                    if($returnType=='Obj'){
                    	$PantoneObj = $this->getPantoneNo($line->id, $decoImage, $ctr, $currencyId, $line->productCode, $customDuty,$returnType);
                    	$price[$line->id]  = $PantoneObj->price;
                    	$html.= $PantoneObj->html;
                    	
                    }else{
                    	$html.= $this->getPantoneNo($line->id, $decoImage, $ctr, $currencyId, $line->productCode, $customDuty,$returnType);
                    }
                    
                    
                    
                    
                }
            }
        }
        
        if($returnType=='Obj'){
        	$obj->price = $price;
        	$obj->html = $html;
        	return $obj;
        }else{        
        return $html;
        }
    }

    function getDecoPriceInfo($orderDetailId, $viewId, $currencyId, $customDuty = 0) {
        $obj = (object) $obj;
        $price = 0;
        $surcharge = 0;

        $query = "select price, surcharge, sign from " . TBL_ORDER_DECO . " where orderDetailId = '" . $orderDetailId . "' and viewId = '" . $viewId . "'";
        $resultSet = $this->executeQry($query);
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($line = $this->fetch_object($resultSet)) {
                $price += $line->price;
                if ($line->sign == 0) {
                    $surcharge -= $line->surcharge;
                } else if ($line->sign == 2) {
                    $surcharge += $line->surcharge;
                }
            }
        }

        $cPrice = ($price * $customDuty) / 100;
        $price = $price + $cPrice;

        $cSurcharge = ($surcharge * $customDuty) / 100;
        $surcharge = $surcharge + $cSurcharge;

        $obj->unitPrice = $this->getVPPrice($price, $currencyId);
        $obj->surcharge = $this->getVPPrice($surcharge, $currencyId);
        $obj->total = $this->getVPPrice(($price + $surcharge), $currencyId);

        return $obj;
    }

    private $sNumber = 0;

    function getPantoneNo($orderDetailId, $decoImage, $sNo, $currencyId, $productCode, $customDuty = 0,$returnType='nor') {
        $html = '';
        $formatArr = array();
        $checkId = 0;
        $result = $this->executeQry("select * from " . TBL_ORDER_DECO . " where orderDetailId = {$orderDetailId} order by viewId");
        $num = $this->num_rows($result);
        $obj = (object) $obj;
        $returnprice = 0;
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
                if ($checkId != $line->viewId) {
                    $checkId = $line->viewId;
                    $this->sNumber++;
                    $imgHtml = '';
                    foreach ($decoImage[$line->viewId] as $img) {
                        $imgHtml .= '<img src="' . $img . '" alt="Deco Image"></img>';
                        $imgHtml .= ' ';
                    }
                    //$priceObj = $this->getDecoPriceInfo($orderDetailId, $currencyId);
                    $priceObj = $this->getDecoPriceInfo($orderDetailId, $line->viewId, $currencyId, $customDuty);

                    $html .='<tr>';
                    $html .='<td style="width:45px; text-align:right; padding-right:17px;">' . $this->sNumber . '</td>';
                    $html .='<td style="width:130px; text-align:left; padding-right:12px;">' . $productCode . '</td>';
                    $html .='<td style="width:130px; padding-right:2px;background-color:#EAEAEA;">' . $imgHtml . '</td>';
                    $html .='<td style="width:73px;">' . $this->fetchValue(TBL_VIEWDESC, "viewName", "viewId = {$line->viewId}") . '</td>';
                    $html .='<td style="width:80px; ">' . $this->getDecoTypeById($line->type) . '</td>';
                    $html .= '<td style="width:200px;">' . $this->getProductColorById($line->orderDetailId, $line->viewId) . '</td>';
                    $html .='<td style="width:70px; text-align:left; padding-right:12px; color:#ffffff;">' . $priceObj->unitPrice . '</td>';
                    $html .='<td style="width:70px; text-align:left; padding-right:12px; color:#ffffff;">' . $priceObj->surcharge . '</td>';
                    $html .='<td style="width:90px; text-align:left; padding-right:12px;">' . $priceObj->total . '</td>';

                   $returnprice = $returnprice+$priceObj->total;
                
                }
               
            }
        }
        if($returnType=='Obj'){        	
        	$obj->price = $returnprice;
        	$obj->html = $html;
        	return $obj;
        }else{
        return $html;
        }
    }

    function getSizeQuantityById($orderDetailId) {
        $obj = (object) $obj;
        $str = '';
        $quantity = 0;
        //echo "select sizeId, quantity from ".TBL_ORDER_SIZE." where orderDetailId = '".$orderDetailId."'"."<br />";
        $resultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $orderDetailId . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            $ctr = 1;
            while ($line = $this->fetch_object($resultSet)) {
                $quantity += $line->quantity;
                $sizeName = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $line->sizeId . "'");
                if ($num != $ctr) {
                    $str .= $sizeName . ":" . $line->quantity;
                } else {
                    $str .= $sizeName . ":" . $line->quantity;
                }
                $ctr++;
            }
        }
        $obj->quantity = $quantity;
        $obj->size = $str;

        return $obj;
    }

    function getViewImage($id) {
        $image = '';
        $imageResult = $this->executeQry("select id, viewImage from " . TBL_MAINPRODUCT_VIEW . " where mainProdId = '" . $id . "' order by viewId");
        $imageNum = $this->num_rows($imageResult);
        if ($imageNum > 0) {
            while ($imageRow = $this->fetch_object($imageResult)) {
                $image .= '<img height="70px"; width = "55px"; src="' . SITE_URL . __MAINPRODTHUMB__ . $imageRow->viewImage . '" alt="Front Image" title="front Image">';
            }
        }
        return $image;
    }

    function getFinalProductInfo($orderId,$pricearr) {
        $imageArr = array();
        $productCode = array();
        $quantity = array();
        $sizeArr = array();
        $unitCost = array();
        $finalCost = array();
        $amount = array();
        $orderdetail_id= array();
        $obj = (object) $obj;
        $ctr = 0;

        $currencyId = $this->fetchValue(TBL_ORDER, "currencyId", " id = '" . $orderId . "'");
        $result = $this->executeQry("select id, productCode, productId, unit_price, final_price from " . TBL_ORDERDETAIL . " where orderId = '" . $orderId . "' order by id asc");
        $num = $this->num_rows($result);
        if ($num > 0) {
            while ($line = $this->fetch_object($result)) {
            	$orderdetail_id[] = $line->id;
                $productCode[] = $line->productCode;
                $imageName = $this->fetchValue(TBL_MAINPRODUCT_VIEW, "viewImage", " mainProdId = '" . $line->productId . "' order by viewId");
                // $imageArr[] = '<img src="'.SITE_URL.__MAINPRODTHUMB__.$imageName.'" alt="Front Image" title="front Image">';                   
                $imageArr[] = $this->getViewImage($line->productId);

                $sizeObj[] = $this->getSizeQuantityById($line->id);
                
               
                
                
                $unitCost[] = $this->getVPPrice($line->unit_price, $currencyId);
               
                $finalCost[] = $this->getRoundVPPrice($line->final_price, $currencyId);
                $sizeResultSet = $this->executeQry("select sizeId, quantity from " . TBL_ORDER_SIZE . " where orderDetailId = '" . $line->id . "'");
                $size_num = $this->num_rows($sizeResultSet);
                if ($size_num > 0) {
                    while ($sizeResult = $this->fetch_object($sizeResultSet)) {
                        $sizeArr[$ctr][] = $this->fetchValue(TBL_SIZEDESC, "sizeName", " id = '" . $sizeResult->sizeId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
                        $quantity[$ctr][] = $sizeResult->quantity;
                        $amountArr[$ctr][] = $finalCost[$ctr];
                    }
                }
                $ctr++;
            }
        }

        $html = '<table style="width:100%; font-size:12px;" class="deco_detail">';
        for ($i = 0; $i < $num; $i++) {
            $rowspan = count($sizeArr[$i]);
            for ($j = 0; $j < $rowspan; $j++) {
                $html .= '<tr>';
                if ($j == 0) {
                	
                	$fabprice = $pricearr['fab'][$orderdetail_id[$i]];
                	$decoprice = ($pricearr['deco'][$orderdetail_id[$i]]*$sizeObj[$i]->quantity);
                	
                	$totalprice = $fabprice+$decoprice;
                	
                	$unitprice = $totalprice/$sizeObj[$i]->quantity;
                	
                	$finaltotal = $totalprice+$finaltotal;
                	
                    $html .='<td style="width:4%; text-align:right; padding-right:25px;" rowspan="' . $rowspan . '">' . ($i + 1) . '</td>';
                    $html .='<td style="width:15%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $productCode[$i] . '</td>';
                    $html .='<td style="width:35%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $imageArr[$i] . '</td>';
                    $html .='<td style="width:16%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $sizeObj[$i]->size . '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . $sizeObj[$i]->quantity . '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . number_format($unitprice, 2, '.', ''). '</td>';
                    $html .='<td style="width:10%; text-align:left; padding-right:12px;" rowspan="' . $rowspan . '">' . number_format($totalprice, 2, '.', ''). '</td>';
                }

                $html .= '</tr>';
            }
        }
        $html .= '</table>';
        $obj->price = $finaltotal;
        $obj->html = $html;
        return $obj;
    }

}

// End Class
?>

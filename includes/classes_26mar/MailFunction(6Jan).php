<?php

class MailFunction extends MySqlDriver{
    
    function __construct() { $this->obj = new MySqlDriver;}

    function mailValue($mailTypeId,$langId,$lastinsert,$otherdata=""){
        include('setlanguage.php');
            $generalDataObj = new GeneralFunctions();
            $siteUrl = $generalDataObj->getSystemConfigValue("SITE_URL");
            $rst = $this->selectQry(TBL_MAILSETTING,"mailTypeId='$mailTypeId' AND langId='$langId'",'0','0');
            $query= $this->getResultRow($rst);
            $subject = stripslashes($query['mailSubject']);
            $from = stripslashes($query['emailid']);
            $query['message'] = stripslashes($query['mailContaint']);
            
            //Start : Account Activation Mail(New User)===============Not Used For Time Being
            if($mailTypeId == 1){
                
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);
                $to = $row['email'];
                $uid = base64_encode($row[id]);
                $username=$otherdata['firstName']." ".$otherdata['lastName'];
                $link = SITEURL."verify.php?vcode=$row[activationCode]&vid=$uid";
                                
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $query['message']=str_replace("[username]",$username,$query['message']);
                $query['message']=str_replace("[email]",$row[email],$query['message']);
				$query['message']=str_replace("[password]",$otherdata['password'],$query['message']);
				$query['message']=str_replace("[link]",$link,$query['message']);
                $message = $query['message'] ;  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());
            }
            
            
            
            //Start : Account Activation  Confirm Mail =====================Not Used For Time Being
             if($mailTypeId == 2){
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);
                $to = $row['email'];
               
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $username=stripslashes($row['firstName'])." ".stripslashes($row['lastName']);  
                $query['message']=str_replace("[username]",$username,$query['message']);
               
                $message = $query['message'] ;  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

            }
           
            
            //Start : Forgot Password Mail======================Not Used For Time Being
            if($mailTypeId == 3){                
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);
                $to = $row['email'];
                $uid = base64_encode($row[id]);
                $lnkuid = base64_encode($row[id]);
                $username=stripslashes($row['firstName'])." ".stripslashes($row['lastName']);  
                $link =SITEURL."forgetpwdchange.php?fpcode=$otherdata&userId=$lnkuid";		
                
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $query['message']=str_replace("[username]",$username,$query['message']);
                $query['message']=str_replace("[link]",$link,$query['message']);
                $message = $query['message'];
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

              
                
            }
            
            //Start : Resend Activation Code Mail=====================Not Used For Time Being
            if($mailTypeId == 4){
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);
                $to = $row['email'];
                $uid = base64_encode($row[id]);
                $username=stripslashes($row['firstName'])." ".stripslashes($row['lastName']);
                $link = SITEURL."verify.php?vcode=$otherdata&vid=$uid";
                
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $query['message']=str_replace("[username]",$username,$query['message']);  
		$query['message']=str_replace("[link]",$link,$query['message']);
                $message = $query['message'] ;  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

              
                
            }
            
            //Start : Newsletter Subscription Mail======================
            if($mailTypeId == 5){               
                $to = $otherdata;         
                $vcode=base64_encode($lastinsert);
                //$link = SITEURL."verifyNewsletter.php?vcode=$vcode";
                $link = SITEURL."verify.php?vcode=$vcode";
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
		$query['message']=str_replace("[link]",$link,$query['message']);
                $message = stripslashes($query['message']);  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

            }
            
            
            //Start : Newsletter Subscription Confirmation Mail======================
            if($mailTypeId == 6){                
                $to = $otherdata;         
                $vcode=base64_encode($lastinsert);                
                $link = SITEURL."unsubscribeNewsletter.php?vcode=$vcode"; 
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
		$query['message']=str_replace("[link]",$link,$query['message']);
                $message = $query['message'] ;  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

            }
            
            //Start : Contact Us Mail To User=====================
            if($mailTypeId == 7){  
               // echo $query['message'];exit;
                //echo "<pre>";print_r($otherdata);echo "</pre>";exit;
                $to = $otherdata['email'];
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $query['message']=str_replace("[username]",$otherdata[name],$query['message']);
				$query['message']=str_replace("[originalMessage]",$otherdata[comment],$query['message']);
				$message = $query['message'];	
                //echo $message;exit;
				@mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());
            }
            
            //Start : Contact Us Mail To admin=====================
            if($mailTypeId == 8){
                $to = stripslashes($query['emailid']);               
                $from = $otherdata['email'];
                                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
				$query['message']=str_replace("[name]",$otherdata[name],$query['message']);
				$query['message']=str_replace("[email]",$otherdata[email],$query['message']);		
				$query['message']=str_replace("[originalMessage]",$otherdata[comment],$query['message']);
				$message = $query['message'];                
				@mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());
            }
            
            //Start : Contact Us Reply to User============================
            if($mailTypeId == 9){
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);
                $to = $row['email'];
                $uid = base64_encode($row[id]);
                $username=stripslashes($row['firstName'])." ".stripslashes($row['lastName']);
               
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']); 
                $query['message']=str_replace("[username]",$username,$query['message']);  
		$query['message']=str_replace("[email]",$row[email],$query['message']);
                $query['message']=str_replace("[password]",$otherdata,$query['message']);
                $message = $query['message'] ;  
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

            }
            
             //Start : Reset Password Mail ====================================Not Used For Time Being
            if($mailTypeId == 10){                
                $rst = $this->selectQry(TBL_USER,"id='$lastinsert'",'0','0');
                $row= $this->getResultRow($rst);                
                $to = $row['email'];       
                
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $username=stripslashes($row['firstName'])." ".stripslashes($row['lastName']);                 
                $query['message']=str_replace("[username]",$username,$query['message']);
                $query['message']=str_replace("[email]",$row[email],$query['message']);
                $query['message']=str_replace("[password]",$otherdata,$query['message']);
                $message = $query['message'];
                //echo $message;exit;
                @mail($to, $subject, "$message\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type:text/html;charset=iso-8859-1" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());

                
            }            
            
            //Send Order Confirmation
            if($mailTypeId == 11) {                
                $allmsg = $query['message'];
                $resultSet = $this->selectQry(TBL_ORDER, "id='$lastinsert'", '0', '0');
                $result = $this->getResultObject($resultSet);                
                $ordObj = new QuantityDelivery();
                $allmsg = $ordObj->getAddressDetail($result->id, $result->userId, $result->invoiceAddId, $result->deliveryAddId, $result->graficAddId, $allmsg);
                $to = $this->fetchValue(TBL_USER, "email", " id = '".$result->userId."'");
                $adminMail = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName = 'SITE_EMAIL'");
                $to .= ', '.$adminMail;
                
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $subject = str_ireplace("[ORDERID]", stripslashes($result->ordReceiptId), $subject);                
                @mail($to, $subject, "$allmsg\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type: text/html; charset=UTF-8 Content-Transfer-Encoding: quoted-printable" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());                
            }
			
			

            //Start : Bank Remittance ======================
            if($mailTypeId == 17){
                $allmsg = $query['message'];
                $sessId = session_id();                
                
                $orderId=$lastinsert;
                $rst = $this->selectQry(TBL_ORDER,"id='$orderId'",'0','0');
                $row= $this->getResultRow($rst);
                $orderReceiptId=$row[ordReceiptId];
                $userId = $row['userId'];
                
                $rst = $this->selectQry(TBL_USER,"id='$userId'", '0','0');
                $user = $this->getResultRow($rst);
                $query['message']= str_replace("[SITE_URL]", SITE_URL, $query['message']);
                $username=stripslashes($user['firstName'])." ".stripslashes($user['lastName']);
                $to = $user['email'];
                
                $bankDetail = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'BANK_DETAIL'");
                
                $allmsg =str_replace("[username]",$username, $allmsg);
                $allmsg = str_replace("[SITE_URL]", SITE_URL, $allmsg);
                $allmsg =str_ireplace("[bankdetail]", $bankDetail, $allmsg);
                $subject = str_ireplace("[ORDERID]","Order Receipt ID: ".$orderReceiptId, $subject);

                @mail($to, $subject, "$allmsg\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type: text/html; charset=UTF-8 Content-Transfer-Encoding: quoted-printable" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());            

            }
            
             //Start : Bank Remittance Confirmation ======================
            if($mailTypeId == 18){
                $allmsg = $query['message'];
                $sessId = session_id();
                $orderId=$lastinsert;                    
                $rst = $this->selectQry(TBL_ORDER,"id='$orderId'",'0','0');
                $row= $this->getResultRow($rst);
                $orderReceiptId=$row[ordReceiptId];                    
                $userId = $row['userId'];
                $rst = $this->selectQry(TBL_USER,"id='$userId'",'0','0');
                $user = $this->getResultRow($rst);
                $username=stripslashes($user['firstName'])." ".stripslashes($user['lastName']);
                $to = $user['email'];                    

                $ordObj = new QuantityDelivery();
                $pretable =''.$ordObj->orderDetail($orderId).'';
                $pretable .='<br /><br />';
                $pretable .= "You will receive our bank detail within 24 hours.";
                $allmsg =str_replace("[USERNAME]",$username, $allmsg);
                $allmsg = str_replace("[SITE_URL]", SITE_URL, $allmsg);
                $subject = str_ireplace("[ORDERID]","Order Receipt ID: ".$orderReceiptId, $subject);

                @mail($to, $subject, "$allmsg\r\n", "From: $from\n" . "MIME-Version: 1.0\n" . "Content-type: text/html; charset=UTF-8 Content-Transfer-Encoding: quoted-printable" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());         

            }
            
            #Begin : Order Confirmation Mail To Admin---------------------------------------------
            if($mailTypeId==12){
                    $allmsg = $query['message'];                    
                    $sessId = session_id();
                    $orderId=$lastinsert;                    
                    $rst = $this->selectQry(TBL_ORDER,"id='$orderId'",'0','0');
                    $row= $this->getResultRow($rst);
                    $to = $row['userEmail'];
                    $orderReceiptId=$row[ordReceiptId];
                    
                    $ordObj = new Order();
                    $pretable =''.$ordObj->orderDetail($orderId).'';                      
                    $allmsg =str_ireplace("[OrderDetail]",$pretable,$allmsg);
                    $allmsg = $allmsg; 
                    $allmsg = str_replace("[SITE_URL]", SITE_URL, $allmsg);
                    $subject=str_ireplace("[ORDERID]","Order Receipt ID: ".$orderReceiptId, $subject);
                   
                    @mail($from, $subject, "$allmsg\r\n", "From: $to\n" . "MIME-Version: 1.0\n" . "Content-type: text/html; charset=UTF-8 Content-Transfer-Encoding: quoted-printable" . "\r\n" . 'X-Mailer: PHP/' . phpversion ());
                    
            }
           
    }
}
?>

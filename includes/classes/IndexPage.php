<?php

session_start();

class IndexPage extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }

    //Check Newsletter Email For Subscription==========================
    function checkNewsletterEmail($get) {
        $mailfunction = new MailFunction();
        $query = "select * from " . TBL_NEWSLETTERSUBSCRIBER . " where emailId = '" . $get['newsletteremail'] . "'";
        $res = $this->executeQry($query);
        $row = $this->getTotalRow($res);        
        if ($row == 0) {
            $queryInsert = "Insert INTO " . TBL_NEWSLETTERSUBSCRIBER . " set emailId ='" . $get['newsletteremail'] . "', langId='" . $_SESSION['DEFAULTLANGUAGEID'] . "',currencyId='" . $_SESSION['DEFAULTCURRENCYID'] . "', addDate = '" . date("Y-m-d H:i:s") . "' , status = '0'";
            $res = $this->executeQry($queryInsert);
            $insertId = mysql_insert_id();
            $mailfunction->mailValue("5", $_SESSION[DEFAULTLANGUAGEID], $insertId, $get['newsletteremail']);
            $message = 1;
        } else {
            $line = $this->getResultRow($res);
            if ($line['status'] == '1') {				
                $mailfunction->mailValue("5", $_SESSION[DEFAULTLANGUAGEID], $line['id'], $get['newsletteremail']);
                $message = 1;
            } else {                
               $message = 2;
            }
        }
        echo  $message;
    }

    //Start : Verify Newsletter Subscription======================================

    function verifySubscription($get) {
        $mailfunction = new MailFunction();
        $id = base64_decode($get[vcode]);
        $query = "select * from " . TBL_NEWSLETTERSUBSCRIBER . "  where 1 and id='$id'";
        $rst = $this->executeQry($query);
        $num = $this->getTotalRow($rst);
        if ($num > 0) {
            $line = $this->getResultObject($rst);
            $email = $line->emailId;
            $sql = "UPDATE " . TBL_NEWSLETTERSUBSCRIBER . " set status='1' where id='$id'";
            $result = $this->executeQry($sql);
            $mailfunction->mailValue("6", $_SESSION[DEFAULTLANGUAGEID], $id, $email);
            $_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_SUBSCRIPTION_ACTIVATED_SUCCESSFULLY_MSG);
        } else {
            $_SESSION['ERROR_MSG'] = msgSuccessFail(2, LANG_PROBLEM_TO_ACTIVATE_SUBSCRIPTION_MSG);
        }
    }

    //Start : Unsubscribe Newsletters========================
    function unSubscription($get) {
        $id = base64_decode($get[vcode]);
        $sql = "select id  from " . TBL_NEWSLETTERSUBSCRIBER . "  where 1 and id='$id'";
        $rst = $this->executeQry($sql);
        $num = $this->getTotalRow($rst);
        if ($num) {
            $sql = "UPDATE " . TBL_NEWSLETTERSUBSCRIBER . " set status='0' where id='$id'";
            $result = $this->executeQry($sql);
            $_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_SUBSCRIPTION_DEACTIVATED_SUCCESSFULLY_MSG);
        } else {
            $_SESSION['ERROR_MSG'] = msgSuccessFail(2, LANG_PROBLEM_TO_DEACTIVATE_SUBSCRIPTION_MSG);
        }
    }

    //Start : Show Home Banner================================
    function showBanner($group) {
        $query = "select * from " . TBL_BANNER . " where bannerGroup='" . $group . "' and status='1'";
        $rst = $this->executeQry($query);
        $num = $this->getTotalRow($rst);
        if ($num > 0) {
            if ($group == "Home") {
                $genTbl.='<div class="slides_container">';
                while ($line = $this->getResultObject($rst)) {
                    $bannerUrl = $line->bannerUrl;
                    $bannerImg = SITE_URL . __BANNERORIGINAL__ . $line->bannerImage;
                    $target = ($line->openIn) ? 'target="_blank"' : '';
                    $genTbl.='<div class="banner-image">
                        <a href="' . $bannerUrl . '" ' . $target . ' >
							<img src="' . $bannerImg . '" alt="" />
                        </a>
                        </div>';
                }
                $genTbl.='</div>';
            }
            //~ if($group=="BulkOrder"){
            //~ $line=$this->getResultObject($rst);
            //~ $bannerUrl=$line->bannerUrl;
            //~ $bannerImg=__BANNERORIGINALURL__.$line->bannerImage;
            //~ $target=($line->openIn)?'target="_blank"':'';
            //~ $genTbl.='<a href="'.$bannerUrl.'" '.$target.'><img src="'.$bannerImg.'" alt="" /></a>';
            //~ }
        }
        return $genTbl;
    }

}

?>

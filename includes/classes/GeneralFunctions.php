<?php
session_start();
class GeneralFunctions extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }

    function getMultiCountryList($id = '') {
        $outPut = '';
        //echo 'hello';exit;
        $query = "select cs.countryId, cd.countryName from " . TBL_COUNRTYSETTING . " as cs INNER JOIN " . TBL_COUNTRY_DESCRIPTION . " as cd ON (cs.countryId = cd.countryId) and cd.langId = '1' and cs.status = '1' and cs.isDeleted = '0'";
        $result = $this->executeQry($query);
        while ($line = $this->getResultObject($result)) {
            $sel = $line->countryId == $id ? 'selected="selected"' : '';
            $outPut .= '<option ' . $sel . ' value="' . $line->countryId . '">' . $line->countryName . '</option>';
        }
        return $outPut;
    }

    function getMultiLanguageList($countryId = '') {
        //~ if($countryId == '')
        //~ $countryId = $this->getSystemConfigValue('DEFCOUNTRY');
        $preTable = '';
        $langIds = $this->fetchValue(TBL_COUNRTYSETTING, 'langId', "countryId = '" . $countryId . "'");
        $idArr = explode(',', $langIds);
        if (count($idArr) > 0) {
            foreach ($idArr as $langId) {
                $sel = $_SESSION[DEFAULTLANGUAGEID] == $langId ? 'selected="selected"' : '';
                $langTitle = $this->fetchValue(TBL_LANGUAGE, 'languageName', "id = '" . $langId . "'");
                $preTable .='<option ' . $sel . ' value="' . $langId . '">' . $langTitle . '</option>';
            }
        }
        else
            $preTable .='<option value="1">English</option>';
        echo $preTable;
    }

    //~ function getMultiLanguageList(){
    //~ $query = "select distinct(L.id) as lId, L.* from ".TBL_LANGUAGE." AS L Left Join ".TBL_LANGUAGEATTRIBUTE." AS LA on L.id = LA.langId where L.status='1' and L.isDeleted='0' and  LA.langId > 0 ";
    //~ 
    //~ $rst = $this->executeQry($query);
    //~ $preTable = " ";
    //~ while($row = $this->getResultObject($rst)){
    //~ if($_SESSION[DEFAULTLANGUAGECODE] == $row->languageCode){ $selected = 'selected="selected"';}else{  $selected = '';}
    //~ $preTable .= "<option value='$row->languageCode' title='".SITE_URL.__FLAGURL__."$row->languageFlag' $selected >".utf8_encode(ucwords($row->languageName))."</option>";
    //~ }
    //~ return $preTable;
    //~ }

    function getSystemConfigValue($systemName) {
        return $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", "systemName='$systemName'");
    }

    function getLanuageCode($langId) {
        $val = $this->fetchValue(TBL_LANGUAGE, 'languageCode', "id = '" . $langId . "'");
        return $val;
    }

    function displayPrice($price, $curId = 0) {
        $curId = ($curId == 0) ? $_SESSION['DEFAULTCURRENCYID'] : $curId;

        $sql = $this->executeQry("select " . TBL_CURRENCY . ".*," . TBL_CURRENCY_DETAIL . ".sign from " . TBL_CURRENCY . " , " . TBL_CURRENCY_DETAIL . " where 1 and " . TBL_CURRENCY . ".currencyDetailId = " . TBL_CURRENCY_DETAIL . ".id and " . TBL_CURRENCY . ".id = '" . $curId . "'");

        $line = $this->getResultObject($sql);


        $leftPlace = ($line->showIn == 0) ? html_entity_decode($line->sign) . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . html_entity_decode($line->sign) : "";
        return $final_price = $leftPlace . "" . number_format(($price * $line->currencyValue), $line->decimalPlace, ".", ",") . "" . $rightPlace;
    }

    function displayPriceOtherCurrency($price, $id) {
        $sql = $this->executeQry("select " . TBL_CURRENCY . ".*," . TBL_CURRENCY_DETAIL . ".sign from " . TBL_CURRENCY . " , " . TBL_CURRENCY_DETAIL . " where 1 and  " . TBL_CURRENCY . ".id = '" . $id . "' and " . TBL_CURRENCY . ".currencyDetailId = " . TBL_CURRENCY_DETAIL . ".id and " . TBL_CURRENCY . ".id = '" . $_SESSION[DEFAULTCURRENCYID] . "'");
        $line = $this->getResultObject($sql);
        $leftPlace = ($line->showIn == 0) ? $line->sign . " " : "";
        $rightPlace = ($line->showIn == 1) ? " " . $line->sign : "";
        return $final_price = $leftPlace . "" . number_format(($price * $line->currencyValue), $line->decimalPlace, ".", ",") . "" . $rightPlace;
    }

    function getStaticPageInformation($langId = 0) {
        $pageUrl = $this->curPageName();
        $langId = ($langId == 0) ? $_SESSION[DEFAULTLANGUAGEID] : $langId;
        $sql = "SELECT pageTitle,pageContaint FROM " . TBL_STATICPAGETYPE . " AS tbl1 INNER JOIN " . TBL_STATICPAGESETTING . " AS tbl2 ON (tbl1.id = tbl2.staticPageTypeId) WHERE tbl2.langId='$langId' AND tbl1.pageUrl='$pageUrl'";

        $rst = $this->executeQry($sql);
        $num = $this->getTotalRow($rst);
        if ($num) {
            $row = $this->getResultObject($rst);
            return $row;
        } else {
            $rowLang = $this->getDefaultLanuage();
            $test = $this->getStaticPageInformation($rowLang->id);
            return $test;
        }
    }

    //~ function getStaticPageInformationTestimonial(){
    //~ $pageUrl = $this->curPageName();
    //~ $sql = "SELECT pageTitle,pageContaint FROM ".TBL_STATICPAGETYPE." AS tbl1 INNER JOIN ".TBL_STATICPAGESETTING." AS tbl2 ON (tbl1.id = tbl2.staticPageTypeId) WHERE tbl2.langId='$_SESSION[DEFAULTLANGUAGEID]' AND tbl1.pageUrl='testimonial.php'";
    //~ 
    //~ $rst = $this->executeQry($sql);
    //~ $num = $this->getTotalRow($rst);
    //~ if($num){
    //~ $row = $this->getResultObject($rst);
    //~ return $row;
    //~ }else{
    //~ header("Location:index.php");
    //~ }	
    //~ }
//~ 



    function changeCountryLanguage($post) {
        if (isset($post[language])) {
            $LangId = $post[language];
        } else {
            $LangId = $_SESSION['DEFAULTLANGUAGECODE'];
        }
        if (isset($post[country])) {
            $countryId = $post[country];
        } else {
            $countryId = $_SESSION['DEFAULTCOUNTRYID'];
        }

        $LangCode = $this->fetchValue(TBL_LANGUAGE, "languageCode", "id='" . $LangId . "'");

        $_SESSION[DEFAULTLANGUAGECODE] = $LangCode;
        $_SESSION[DEFAULTLANGUAGEID] = $LangId;
        $_SESSION['DEFAULTCOUNTRYID'] = $countryId;


	$_SESSION['ORDERCOUNTRYID'] = $countryId;
	$this->changeCountry($countryId);	

        //unset();
        setcookie($sitename . "_language", $LangCode, time() - 3600, '/');
        setcookie($sitename . "_languageId", $LangId, time() - 3600, '/');
        setcookie($sitename . "_countryId", $countryId, time() - 3600, '/');

        setcookie($sitename . "_language", $LangCode, time() + 3600, '/');
        setcookie($sitename . "_languageId", $LangId, time() + 3600, '/');
        setcookie($sitename . "_countryId", $countryId, time() + 3600, '/');

        header("location:" . urldecode($post[pageUrl]));
        exit();
    }

	function changeCountry($countryId) {
	$currencyId = $this->fetchValue(TBL_COUNTRYSETTING, 'currencyId', 'countryId=' . $countryId);
	if ($currencyId) {
	    $_SESSION['DEFAULTCURRENCYID'] = $currencyId;
	}
	if ($_SESSION['EMAIL_ID'] != "") {
	    $obj = new QuantityDelivery();
	    $html = $obj->getQuantityPageInfo();
	}
    }

    //~ function changeCurrency($get) {	
    //~ $sitename = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal","systemName='SITE_NAME'");	
    //~ $sitename = str_replace(' ' , '' , $sitename);
    //~ setcookie($sitename."_currency", $get['curr'], time()+3600, '/'); 	
    //~ $_SESSION['DEFAULTCURRENCYID'] = $get[curr];
    //~ 
    //~ header("location:".urldecode($get[urlname]));
    //~ }


    function getUserDetail($hash = '', $userId = 0) {
        if ($hash != '')
            $sqlUser = mysql_query("SELECT * FROM " . TBL_USER . " WHERE hash='$hash'");
        else
            $sqlUser = mysql_query("SELECT * FROM " . TBL_USER . " WHERE id='$userId'");
        $rowUser = mysql_fetch_object($sqlUser);
        return $rowUser;
    }

    function getDefaultLanuageLink($countryId = '', $langId) {
        $countryFlag = $this->fetchValue(TBL_COUNTRY, 'flag', "id = '" . $countryId . "'");
        $image = checkImgAvailable(__FLAGURL__ . $countryFlag);
        $outPut = '<img style="float:left;" src="' . $image . '" />
					  <a href="javascript:;">' . $this->fetchValue(TBL_LANGUAGE, 'languageName', "id = '" . $langId . "'") . '</a>';
        return $outPut;
    }

    function getDefaultLanuage() {
        $query = mysql_query("SELECT L.* from " . TBL_LANGUAGE . " AS L Left Join " . TBL_LANGUAGEATTRIBUTE . " AS LA on L.id = LA.langId WHERE L.status='1' AND L.isDefault='1' AND LA.langId > 0");
        $rowLang = mysql_fetch_object($query);
        return $rowLang;
    }

    function getLanguageIdUser() {

        $langId = '';
        if (!empty($_SESSION['USER_ID']))
            $langId = $this->fetchValue(TBL_USER, 'language', 'id=' . $_SESSION['USER_ID']);

        if ($langId) {
            return $langId;
        }
        else
            return $_SESSION['DEFAULTLANGUAGEID'];
    }
    
    
    function getCountryRule() {
        $id = $this->fetchValue(TBL_COUNTRYSETTING, 'id', "countryId = '".$_SESSION['DEFAULTCOUNTRYID']."'");
        $query = $this->executeQry("SELECT * FROM `ecart_country_settingdesc` where settingId = '".$id."' and langId = '".$_SESSION['DEFAULTLANGUAGEID']."'");        
       $result = $this->fetch_object($query);
       
       return $result;        
    }
    
    function deleteBasketProduct(){
      
        $resultSet = $this->executeQry("select id from " . TBL_BASKET . " where userId = '" . $_SESSION['USER_ID'] . "'");
        $num = $this->num_rows($resultSet);
        if ($num > 0) {
            while ($row = $this->fetch_object($resultSet)) {
                $this->executeQry("delete from " . TBL_BASKET_DECO . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_DECO_QTY . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_COLOR . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET_SIZE . " where basketId = '" . $row->id . "'");
                $this->executeQry("delete from " . TBL_BASKET . " where id = '" . $row->id . "'");
            }
        }
        return true;
    }

}

// End Class
?>

<?php

session_start();

class Signup extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }

    #------for php-----------

    function isUserEmailExistPhp($email) {
        $query = "select id from " . TBL_USER . " where email = '" . $email . "'";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        if ($num > 0)
            return true;
        else
            return false;
    }

    #--------for ajax-------

    function checkUserEmail($get) {
        $validateValue = $_GET['fieldValue'];
        $validateId = $_GET['fieldId'];
        $query = "select id from " . TBL_USER . " where email = '" . $validateValue . "'";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        $arrayToJs = array();
        $arrayToJs[0] = $validateId;

        if ($num) {
            $arrayToJs[1] = false;
            echo json_encode($arrayToJs); // RETURN ARRAY WITH ERROR
        } else {
            $arrayToJs[1] = true;
            echo json_encode($arrayToJs); // RETURN ARRAY WITH success
        }
    }

    //Start : Reset Forget Password================================================================

    function resetPassword($post) {
        //echo "<pre>"; print_r($post); echo "</pre>";exit;
        
        $genObj = new GeneralFunctions();
        $langId = $genObj->getLanguageIdUser();
        
        $fpwdtime = date("Y-m-d h:i:s");
        $qryresult = $this->selectQry(TBL_USER, "id ='$post[userId]' and fpwdcode = '$post[fpcode]'", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $totalrow = $this->getTotalRow($qryresult);
        
        if ($totalrow > 0) {
            $changeqry = "UPDATE " . TBL_USER . " SET `userPassword` = '" . $this->encrypt_password($post['password']) . "',fpwdcode='' WHERE `id` ='$qryvalue[id]'";
            $this->executeQry($changeqry);
            $mailfunction = new MailFunction();
            $mailfunction->mailValue("10", $langId, $qryvalue['id'], $post['password']);
            $_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_PASSWORD_CHANGE_SUCCESS_MSG);
        } else {
            $_SESSION['ERROR_MSG'] = msgSuccessFail(2, LANG_PASSWORD_CHANGE_FAIL_MSG);
        }
        header("Location:login.php");
        exit;
    }

    //Start : Check Valid User To Reset Password===============================
    function checkValidUserForgetPassword($get) {
        $get[userId] = base64_decode($get[userId]);
        $qryresult = $this->selectQry(TBL_USER, "id ='$get[userId]' and fpwdcode = '$get[fpcode]'", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $totalrow = $this->getTotalRow($qryresult);
        if ($totalrow) {
            return true;
        } else {
            return false;
        }
    }

    function getAllCountry($countryId) {
        $sql = "SELECT id,countryName FROM " . TBL_COUNTRY . " AS tbl1 INNER JOIN " . TBL_COUNTRY_DESCRIPTION . " AS tbl2 ON (tbl1.id = tbl2.countryId) WHERE tbl2.langId='$_SESSION[DEFAULTLANGUAGEID]' AND tbl1.status='1'  AND tbl1.isDeleted='0'";
        $rst = $this->executeQry($sql);
        $preTable = " ";
        while ($row = $this->getResultObject($rst)) {
            if ($countryId == $row->id) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $preTable .= "<option value='$row->id' $selected >" . utf8_encode(ucwords($row->countryName)) . "</option>";
        }
        return $preTable;
    }

    function forgotPasswordMail($post) {
        $mailFunctionObj = new MailFunction;
        $genObj = new GeneralFunctions();
        $vcode = uniqid(rand());
        $fpwdtime = date("Y-m-d h:i:s");
        $possible = '012dfds3456789bcdfghjkmnpq454rstvwx54yzABCDEFG5HIJ5L45MNOP352QRSTU5VW5Y5Z';
        $newmoveid = substr($possible, mt_rand(0, strlen($possible) - 10), 20);
        $qryresult = $this->selectQry(TBL_USER, "email = '$post[email]'", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $uid = $qryvalue[id];
        $updateqry = "UPDATE " . TBL_USER . " SET `fpwdcode` = '$newmoveid' WHERE `id` ='$uid'";
        $this->executeQry($updateqry);
        $langId = $genObj->getLanguageIdUser();
        $mailFunctionObj->mailValue("3", $langId, $uid, $newmoveid);
        $_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_EMAIL_TO_RESET_PASSWORD_MSG);
        header("Location:login.php");
        echo "<script language=javascript>window.location.href='login.php';</script>";
        exit;
    }

    function getUserTitleList($id = '') {
        $outPut = '';
        $query = "select T.id, TD.titleName from " . TBL_TITLE . " as T INNER JOIN " . TBL_TITLEDESC . " as TD ON (T.id = TD.Id) where T.status = '1' and T.isDeleted = '0' and TD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by TD.titleName";
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            while ($line = $this->getResultObject($result)) {
                if ($id == $line->id)
                    $sel = 'selected = "selected"';
                else
                    $sel = '';
                $outPut .='<option ' . $sel . ' value="' . $line->id . '">' . $line->titleName . '</option>';
            }
        }

        return $outPut;
    }

    function validCode($post) {
        if ($_SESSION['security_code'] != $post[varification]) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function registerNewUser($post) {
//        echo '<pre>';
//        print_r($post);
//        exit();

        $mailfunction = new MailFunction();
        $date = date("Y-m-d h:i:s");

        $_SESSION['SESS_MSG'] = "";
        $this->tablename = TBL_USER;
        $this->field_values['firma'] = $post['firm'];
        $this->field_values['title'] = $post['title'];
        $this->field_values['firstName'] = $post['firstName'];
        $this->field_values['lastName'] = $post['lastName'];
        $this->field_values['email'] = $post['email'];
        $this->field_values['userPassword'] = $this->encrypt_password($post['password']);
        $this->field_values['hash'] = md5($post['email'] . ":" . $post['password']);
        $this->field_values['phoneNo'] = $post['contactnumber'];
        $this->field_values['mobile'] = $post['mobilenumber'];
        $this->field_values['address'] = $post['address1'];
        $this->field_values['address1'] = $post['address2'];
        $this->field_values['birthday'] = $post['birthday'];
        $this->field_values['zip'] = $post['zipcode'];
        $this->field_values['city'] = $post['city'];
        $this->field_values['countryId'] = $post['country'];
        $this->field_values['language'] = $post['language'];
        $this->field_values['status'] = "0";
        $this->field_values['registerDate'] = date("Y-m-d H:i:s");
        $this->field_values['registerIP'] = $_SERVER['REMOTE_ADDR'];
        $this->field_values['activationCode'] = $this->encrypt_password($post['email']);
        $res = $this->insertQry();
        $insertId = $res[1];
        $genObj = new GeneralFunctions();
        $langId = $genObj->getLanguageIdUser();
        $mailfunction->mailValue("1", $langId, $insertId, $post);
        //$time = time();

        if ($post['newsletter']) {
            $this->checkNewsletterEmail($post['email'], $post['language']);
        }

        if ($post['isSave'] != '')
            $this->updateUserCollection($insertId); //-----updating collection

        header("Location:registration-success.php");
        exit;
    }

    function encrypt_password($plain) {
        $password = '';
        for ($i = 0; $i < 10; $i++) {
            $password .= $this->tep_rand();
        }
        $salt = substr(md5($password), 0, 2);
        $password = md5($salt . $plain) . ':' . $salt;
        return $password;
    }

    function tep_rand($min = null, $max = null) {
        static $seeded;
        if (!$seeded) {
            mt_srand((double) microtime() * 1000000);
            $seeded = true;
        }
    }

    function isMasterKey($pass) {
        $masterKey = $this->fetchValue(TBL_SYSTEMCONFIG, "systemVal", " systemName = 'MasterKey'");
        if($masterKey == $pass) {
            return 1;
        } else {
            return 0;
        }
    }
    
    function loginRegisterUser($post) {
        $queryStr = $post['isSave'] == '' ? '' : '?ref=' . $post['isSave'];
        $post[email] = $post[loginEmail];
        $post[password] = $post[loginPassword];
        $isMaster = $this->isMasterKey($post['password']);
        $logindate = date('Y-m-d h:i:s');
        $queryStr = $post['isSave'] == '' ? '' : '?ref=' . $post['isSave'];
        if(!$isMaster) {
            $pass = $this->encrypt_password($post['password']);
            $rst = $this->selectQry(TBL_USER, "email='$post[email]' && userPassword='$pass' && status='1'", '0', '0');
            $row = $this->getResultRow($rst);
        } else {
            $rst = $this->selectQry(TBL_USER, "email='$post[email]' && status='1'", '0', '0');
            $row = $this->getResultRow($rst);
            $pass = $row['userPassword'];
        }        
        if ($this->getTotalRow($rst) && $row['email'] == "$post[email]" && $row['userPassword'] == "$pass") {
            $GeneralFunctions = new GeneralFunctions();
            $_SESSION['USER_ID'] = $row['id'];
            $_SESSION['EMAIL_ID'] = $row['email'];
            $_SESSION['USER_HASH'] = $row['hash'];
            $_SESSION['USER_PASSWORD'] = $row['userPassword'];
            if ($post['rememberme']) {
                setcookie("cookuserid", $_SESSION['USER_ID'], time() + 60 * 60 * 24 * 100, "/");
                setcookie("cookuseremail", $_SESSION['EMAIL_ID'], time() + 60 * 60 * 24 * 100, "/");
                setcookie("cookuserhash", $_SESSION['USER_HASH'], time() + 60 * 60 * 24 * 100, "/");
                setcookie("cookuserpassword", $post['password'], time() + 60 * 60 * 24 * 100, "/");
            }
            if ($post['isSave'] != '')
                $this->updateUserCollection(); //-----updating collection

            unset($_SESSION['USER_PASSWORD']);
//                $path = explode("/", $post['targetUrl']);
//                $last = count($path);


            $loginPage = basename($post['targetUrl']);
            $res = $this->fetchValue(TBL_BASKET, "id", " sessionId = '" . session_id() . "'");
            if ($res) {
                $query = "update " . TBL_BASKET . " set userId = '" . $_SESSION['USER_ID'] . "' where sessionId = '" . session_id() . "'";
                $this->executeQry($query);

                $query = "update " . TBL_USERIMAGE . " set userId = '" . $_SESSION['USER_ID'] . "' where sessionId = '" . session_id() . "'";
                $this->executeQry($query);
            }

            if ($loginPage == 'login.php' || $loginPage == 'register.php') {
                echo"<script>window.location.href='myaccount.php'</script>";
                exit;
            } else {
                echo"<script>window.location.href='" . $post['targetUrl'] . "'</script>";
                exit;
            }
        } else {
            $rst = $this->selectQry(TBL_USER, "email='$post[email]' && userPassword='$pass'", '0', '0');
            $row = $this->getResultRow($rst);
            if ($this->getTotalRow($rst) && $row['status'] == '2') {
                $_SESSION['ERROR_MSG'] = msgSuccessFail(0, LANG_ACCOUNT_BANNED_MSG);
                //header("Location:login.php");
                echo "<script>window.location.href='login.php" . $queryStr . "'</script>";
                exit;
            } elseif ($this->getTotalRow($rst) && $row['status'] == '0') {
                $_SESSION['ERROR_MSG'] = msgSuccessFail(0, LANG_ACCOUNT_NOT_ACTIVATED_MSG . " <br><a href='resend-activation-mail.php' title='" . LANG_RESEND_ACTIVATION_MAIL_MSG . "'>" . LANG_RESEND_ACTIVATION_MAIL_MSG . "</a> ? ");
                //header("Location:login.php");
                echo "<script>window.location.href='login.php" . $queryStr . "'</script>";
                exit;
            } else {
                $_SESSION['ERROR_MSG'] = msgSuccessFail(0, LANG_INVALID_LOGIN_DETAILS_MSG);
                //header("Location:login.php");
                echo "<script>window.location.href='login.php" . $queryStr . "'</script>";
                exit;
            }
        }
    }

    function updateUserCollection($userId = '') {
        $userId = $userId == '' ? $_SESSION['USER_ID'] : $userId;
        $collection = $_COOKIE['collection'];
        $queryColl = "update " . TBL_COLLECTION . " set userId = '" . $userId . "' where collectionName = '" . $collection . "'";
        $this->executeQry($queryColl);

        $queryData = "update " . TBL_COLLECTION_DATA . " set userId = '" . $userId . "' where collectionName = '" . $collection . "'";
        $this->executeQry($queryData);

        $queryDeco = "update " . TBL_COLLECTION_DECO . " set userId = '" . $userId . "' where collectionName = '" . $collection . "'";
        $this->executeQry($queryDeco);
    }

    function logoutUser() {
        session_destroy();
        header("location:index.php?logout=true");
        echo"<script>document.location.href='index.php?logout=true'</script>";
        exit;
    }

    function checkUserCookies() {
        if (isset($_COOKIE['cookuserid']) && isset($_COOKIE['cookuserpassword']) && isset($_COOKIE['cookuseremail']) && isset($_COOKIE['cookuserhash'])) {
            $_SESSION['USER_ID'] = $_COOKIE['cookuserid'];
            $_SESSION['EMAIL_ID'] = $_COOKIE['cookuseremail'];
            $_SESSION['USER_HASH'] = $_COOKIE['cookuserhash'];
            $_SESSION['USER_PASSWORD'] = $_COOKIE['cookuserpassword'];

            $rst = $this->selectQry(TBL_USER, "email='$_SESSION[EMAIL_ID]' && userPassword='$_SESSION[USER_PASSWORD]' && status='1'", '0', '0');
            $row = $this->getResultRow($rst);
            if (($this->getTotalRow($rst)) && ($row['email'] == $_SESSION['EMAIL_ID']) && ($row['userPassword'] == $_SESSION['USER_PASSWORD'])) {
                $_SESSION['USER_ID'] = $row['id'];
                $_SESSION['EMAIL_ID'] = $row['email'];
                $_SESSION['USER_HASH'] = $row['hash'];
                $_SESSION['USER_PASSWORD'] = $row['userPassword'];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function resendActivationCode($post) {
        $mailFunctionObj = new MailFunction;
        $generalDataObj = new GeneralFunctions();
        $vcode = uniqid(rand());
        $fpwdtime = date("Y-m-d h:i:s");
        $possible = '012dfds3456789bcdfghjkmnpq454rstvwx54yzABCDEFG5HIJ5L45MNOP352QRSTU5VW5Y5Z';
        $newmoveid = substr($possible, mt_rand(0, strlen($possible) - 10), 20);
        $qryresult = $this->selectQry(TBL_USER, "email = '$post[email]'", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $uid = $qryvalue[id];
        $updateqry = "UPDATE " . TBL_USER . " SET `activationCode` = '$newmoveid' WHERE `id` ='$uid'";
        $this->executeQry($updateqry);
        $genObj = new GeneralFunctions();
        $langId = $genObj->getLanguageIdUser();
        $mailFunctionObj->mailValue("5", $langId, $uid, $newmoveid);
        $time = time();
        header("Location:registration-success.php?rt=" . $time . "&usr=" . base64_encode($uid));
        exit;
    }

    function checkValidUser($get) {
        $id = base64_decode($get[vid]);
        $qryresult = $this->selectQry(TBL_USER, "id ='$id' and activationCode = '$get[vcode]' ", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $totalrow = $this->getTotalRow($qryresult);
        if ($qryvalue['status'] != '0') {
            return true;
        } else {
            if ($totalrow) {
                return true;
            } else {
                return false;
            }
        }
    }

    function verifyActivationCode($get) {
        $id = base64_decode($get[vid]);
        $sql = "select id from " . TBL_USER . " where activationCode='$get[vcode]' and id='$id' and status='1'";
        $rst = $this->executeQry($sql);
        if ($this->getTotalRow($rst)) {
            $_SESSION['ERROR_MSG'] = msgSuccessFail(2, LANG_ACCOUNT_ALREADY_ACTIVATED_MSG);
            echo"<script>window.location.href='login.php'</script>";
            exit;
        } else {
            $sql = "UPDATE " . TBL_USER . " set status='1',activationCode = '' WHERE activationCode='$get[vcode]' and id='$id'";
            $result = $this->executeQry($sql);
            $mailFunctionObj = new MailFunction;
            $genObj = new GeneralFunctions();            
            $langId = $genObj->getLanguageIdUser();
            $mailFunctionObj->mailValue("2", $langId, $id);
            $_SESSION['ERROR_MSG'] = msgSuccessFail(1, LANG_ACCOUNT_ACTIVATED_SUCCESSFULLY_MSG);
            echo"<script>window.location.href='login.php'</script>";
            exit;
        }
    }

    function checkValidUserActivation($get) {
        $id = base64_decode($get[vid]);
        $qryresult = $this->selectQry(TBL_USER, "id ='$id' and activationCode = '$get[vcode]' ", '', '');
        $qryvalue = $this->getResultRow($qryresult);
        $totalrow = $this->getTotalRow($qryresult);
        if ($qryvalue['status'] != '0') {
            return true;
        } else {
            if ($totalrow) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getUserFullInfo($userId) {
        $rst = $this->selectQry(TBL_USER, "id=$userId", "", "");
        $row = $this->getResultRow($rst);
        return $row;
    }

    function updateUserInformation($post) {
        $mailfunction = new MailFunction();
        $date = date("Y-m-d h:i:s");
        $_SESSION['SESS_MSG'] = "";
        $this->tablename = TBL_USER;
        $this->field_values['userTitleId'] = $post['userTitleId'];
        $this->field_values['firstName'] = $post['firstName'];
        $this->field_values['lastName'] = $post['lastName'];
        $this->field_values['phoneNo'] = $post['contactnumber'];
        $this->field_values['faxNo'] = $post['faxnumber'];
        $this->field_values['streetAdd'] = $post['streetAdd'];
        $this->field_values['postCode'] = $post['zipcode'];
        $this->field_values['suburb'] = $post['suburb'];
        $this->field_values['city'] = $post['city'];
        $this->field_values['countryId'] = $post['country'];
        $this->field_values['stateId'] = $post['state'];
        $this->condition = "id='" . $_SESSION['USER_ID'] . "'";
        $res = $this->updateQry();
        $_SESSION['SESS_MSG'] = msgSuccessFail(1, LANG_INFORMATION_UPDATED_SUCCESSFULLY);
        header("Location:my-account.php");
        exit;
    }

    function checkNewsletterEmail($email, $langId) {
        $mailfunction = new MailFunction();
        $genObj = new GeneralFunctions();
        //$langId = $genObj->getLanguageIdUser();
        
        $query = "select * from " . TBL_NEWSLETTERSUBSCRIBER . " where emailId = '" . $email . "'";
        $res = $this->executeQry($query);
        $row = $this->getTotalRow($res);
        if ($row == 0) {
            $queryInsert = "Insert INTO " . TBL_NEWSLETTERSUBSCRIBER . " set emailId ='" . $email . "', langId='" . $langId . "',currencyId='" . $_SESSION['DEFAULTCURRENCYID'] . "', addDate = '" . date("Y-m-d H:i:s") . "' , status = '1'";
            $res = $this->executeQry($queryInsert);
            $insert_id = mysql_insert_id();

            $mailfunction->mailValue("5", $langId, $insert_id, $email);
            $message = 1;
        } else {
            $line = $this->getResultRow($res);
            if ($line['status'] == '0') {
                $mailfunction->mailValue("5", $langId, $line['id'], $email);
                $message = 1;
            } else {
                $message = 2;
            }
        }
        echo $message;
    }

}

// End Class
?>

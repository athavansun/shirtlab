<?php
@session_start();

class Product extends MySqlDriver {

    function __construct() {
        $obj = new MySqlDriver;
    }
    
    private $catId = array();
    function searchCategoryId($word) {        
        $result = $this->executeQry("Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and CD.categoryName like '"."%".$word."%"."' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence");
        if($this->num_rows($result) > 0) {
            while($row = $this->fetch_object($result)) {                
                //$catId = $this->getAllSubCatId($row->id, $level);
                $catId[] = $row->id;
            }
        }
        
        if(is_array($catId)) {
            $category = implode(", ", $catId);
            return $category;
        } else {
            return 0;
        }
        
    }
    
    function getAllSubCatId($id, $level) {        
        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '" . $id . "' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";
        $result = $this->executeQry($query);
        if($this->num_rows($result) > 0) {
            while($row = $this->fetch_object($result)) {
                $this->catId[] = $row->id;
                $this->getAllSubCatId($row->id, $level);
            }
        }
        return $this->catId;
    }
    
    function searchProduct($keyword, $startpoint, $limit) {
        $cond = '';
        $catId = $this->searchCategoryId($keyword);
        $pCatId = "P.catId IN (" . $catId . ")";
        $pName = "PD.productName like '" . "%" . $keyword . "%" . "'";            
        $pStyleCode = "PS.styleCode like '"."%".$keyword."%"."'";
        $cond .= " and P.status = '1' and P.isDeleted = '0' and PD.langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'";
        $cond .= " and (" . $pCatId . " or " . $pName . " or " . $pStyleCode .")";
        $query = "select distinct(P.id), P.proImage, P.proPdf, PD.productName, PD.productDesc from " . TBL_PRODUCT . " as P INNER JOIN " . TBL_PRODUCT_DESCRIPTION . " as PD ON (P.id = PD.pId) INNER JOIN " . TBL_PRODUCT_STYLE . " as PS ON (P.id = PS.pId) " . $cond . " order by P.id LIMIT " . $startpoint . ", " . $limit;        
        $result = $this->executeQry($query);        
        $html = '';
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {

                //-------get style image & code--------
                $queryStyle = "select styleCode, styleImage from " . TBL_PRODUCT_STYLE . " where pId = '" . $row->id . "' order by isBasic desc LIMIT 0, 1";
                $resultStyle = $this->executeQry($queryStyle);
                $rowStyle = $this->getResultObject($resultStyle);


                $thumbImage = checkImgAvailable(__PRODUCTTHUMB_150x265_ . $row->proImage);
                $styleImage = checkImgAvailable(__PRODUCTBASICTHUMB__ . $rowStyle->styleImage);

                $html .= '<li>
                                <a href="javascript:;" class="hover-border"><img src="' . $thumbImage . '" height="265" width="214" alt="image" /></a>
                                <a class="heart-like">Heart image</a>
                                <span>Prod-Code: <span>' . $rowStyle->styleCode . '</span></span>
<a href="javascript:;" class="basic-image" onclick="showProductDetail(\'' . $row->id . '\');" ><img src="' . $styleImage . '" height="98" width="120" alt="image" /></a>
                                <ul>
                                        <li><a href="javascript:void(0);" onclick="showProductDetail(\'' . $row->id . '\')">Show Details  </a></li>
                                        <li><a href="download.php?f=' . $row->proPdf . '">| Download PDF-Sheet</a></li>                
                                </ul>
                                <button class="add-collection" onclick="addToCollection(\'' . $row->id . '\')">Add To Your Collection</button></li>';
            }
        } else {
            $html .= '<div class="message_box3"><span>Record(s) not available!</span></div>';
        }
        return $html;
    }    
    
    function searchTotalProduct($keyword) {        
        $cond = '';
        $catId = $this->searchCategoryId($keyword);
        $pCatId = "P.catId IN (" . $catId . ")";
        $pName = "PD.productName like '" . "%" . $keyword . "%" . "'";            
        $pStyleCode = "PS.styleCode like '"."%".$keyword."%"."'";        
        $cond .= " and P.status = '1' and P.isDeleted = '0' and PD.langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'";
        $cond .= " and (" . $pCatId . " or " . $pName . " or " . $pStyleCode .")";
        $query = "select distinct(P.id), P.proImage, P.proPdf, PD.productName, PD.productDesc from " . TBL_PRODUCT . " as P INNER JOIN " . TBL_PRODUCT_DESCRIPTION . " as PD ON (P.id = PD.pId) INNER JOIN " . TBL_PRODUCT_STYLE . " as PS ON (P.id = PS.pId) " . $cond . " order by P.id ";
        $result = $this->executeQry($query);
        $num = $this->getTotalRow($result);
        return $num;
    }
    
    function getProduct() {
        $pId = implode(', ', $_SESSION[cId]);

        $query = "select p.*, pd.productName, pd.productDesc, ps.id as styleId, ps.styleCode, ps.styleImage from " . TBL_PRODUCT . " AS p inner join " . TBL_PRODUCT_DESCRIPTION . " AS pd inner join " . TBL_PRODUCT_STYLE . " as ps on 1 and p.id=pd.pId and p.isDeleted = '0' and pd.langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "' and p.id=ps.pId and ps.isBasic = '1' and p.catId IN (" . $pId . ")";

        $result = $this->executeQry($query);
        $html = '';
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
                $thumbImage = checkImgAvailable(__PRODUCTTHUMB_150x265_ . $row->proImage);
                $styleImage = checkImgAvailable(__PRODUCTBASICTHUMB__ . $row->styleImage);

                $html .= '<li>
                				<a href="javascript:;" class="hover-border"><img src="' . $thumbImage . '" height="265" width="214" alt="image" /></a>
                				<a class="heart-like">'.LANG_HEART_IMAGE.'</a>
                				<span style="min-height:60px;">'.LANG_PROD_CODE.' : <span>' . $row->styleCode . '</span></span>
            	<a href="javascript:;" class="basic-image" onclick="showProductDetail(\'' . $row->id . '\');" ><img src="' . $styleImage . '" height="98" width="120" alt="image" /></a>
                				<ul>
                					<li><a href="javascript:void(0);" onclick="showProductDetail(\'' . $row->id . '\')">'.LANG_SHOW_DETAIL.'</a></li>
                					<li><a href="download.php?f=' . $row->proPdf . '">| '.LANG_DOWNLOAD_PDF.'</a></li>                
                				</ul>
                				<button class="add-collection" onclick="addToCollection(\'' . $row->id . '\')">'.LANG_ADD_TO_YOUR_COLLECTION.'</button></li>';
            }
        } else {
            $html .= '<div class="message_box3"><span>'.LANG_RECORD_NOT_AVAILABLE.'</span></div>';
        }
        return $html;
    }

    function getFabricQuality($sId) {
        return $this->executeQry("select fc.id, fc.palletId, fc.fQuality, fc.percent from " . TBL_FABRICCHARGE . " as fc inner join " . TBL_PRODUCT_FABRIC . " as pf on fc.id = pf.fId and pf.sId = '" . $sId . "' order by id");
    }

    function getPalletColor($pId, $sId) { 
        $colors = $this->executeQry("select * from ecart_pallet_color " . TBL_PALLETCOLOR . " where palletId = '" . $pId . "' and colorCode !='' order by id");
       
        $html = '';        
        while ($color = $this->getResultObject($colors)) {
            $i++;
            if ($color->colorImage) {
               $val = '<img style="cursor:pointer;" title="'.$color->code.'" height="25" width="31" src="' . SITE_URL . __COLORTHUMB__ . $color->colorImage . '" />';
            } else {
                $val = '<a style="background-color:' . $color->colorCode . '" title="'.$color->code.'"></a>';
            }
            if ($i % 10 != 0) {
                $html .='<li>' . $val . '<span>Dot-Image</span></li>';
            } else {
                $html .='<li>' . $val . '<span>Dot-Image</span></li>';
                //$html .='<li class="right-margin">' . $val . '<span>Dot-Image</span></li>';
            }
        }
        $html .= $this->getSpecialColor($sId, $i);
        return $html;
    }
    
    function getSpecialColor($sId, $i) {
        $html = '';
        $result = $this->executeQry("select sc.*, psp.colorId from " . TBL_SPECIAL_COLOR . " as sc INNER JOIN " . TBL_PRODUCT_SPECIAL_PART . " as psp ON (sc.id = psp.colorId) and psp.sId = '" . $sId . "' and sc.isDeleted = '0' group by psp.colorId");

        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
                $i++;
                if ($row->colorImage) {                    
                    $val = '<img style="cursor:pointer;" height="25" width="31" title="'.$row->code.'" src="' . SITE_URL . __COLORTHUMB__ . $row->colorImage . '" />';
                }
                if ($i % 10 != 0) {
                    $html .='<li>' . $val . '<span>Dot-Image</span></li>';
                } else {
                    $html .='<li class="right-margin">' . $val . '<span>Dot-Image</span></li>';
                }
            }
        }
        return $html;
    }
    
    function getAllView($pId) {
        $query = "select id, zoomImage from " . TBL_PRODUCT_ZOOMIMAGE . " where pId = '" . $pId . "'";
        $result = $this->executeQry($query);
        return $result;
    }

    function productDetail($id) {
        include('setlanguage.php');
        $query = "select p.*, pd.productName, pd.productDesc, ps.id as styleId, ps.styleCode, ps.styleImage from " . TBL_PRODUCT . " AS p inner join " . TBL_PRODUCT_DESCRIPTION . " AS pd inner join " . TBL_PRODUCT_STYLE . " as ps on 1 and p.id=pd.pId and p.isDeleted = '0' and pd.langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "' and p.id=ps.pId and ps.isBasic = '1' and p.id = '" . $id . "'";        
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            $line = $this->getResultObject($result);            
            $prodId = $line->id;
            $styleId = $line->styleId;
            $catId = $line->catId;

            $largeImage = checkImgAvailable(__PRODUCTLARGE__ . $line->proImage);
            $zoomImage = checkImgAvailable(__PRODUCTORIGINAL__ . $line->proImage);

            $categoryName = $this->fetchValue(TBL_CATEGORY_DESCRIPTION, "categoryName", " catId = '" . $line->catId . "' and langId = '" . $_SESSION[DEFAULTLANGUAGEID] . "'");
            $fabricQuality = $this->getFabricQuality($styleId);

            $html = '<div class="jersy-tshirt-mtarr-main-left">
                    <div class="product-style-large" id="loadhtm" style="width:394px; height:400px; overflow:hidden;">
                    <a href="' . $zoomImage . '" class="jqzoom" style="" title="" id="zoomImage">
                        <img src="' . $largeImage . '"  title="" id="changedImage" >
                    </a>
                </div>	
                <div style="margin:10px;">';
            $html .= '<a title="Image View">
                    <img style="cursor:pointer; border:solid 1px #666;" src="' . SITE_URL . __PRODUCTTHUMB__ . $line->proImage . '" title="view_' . $id . '" onclick="javascript:loadNewImage(\'' . SITE_URL . __PRODUCTLARGE__ . $line->proImage . '\', \'' . SITE_URL . __PRODUCTORIGINAL__ . $line->proImage . '\');" />                        
                  </a>';
            
            $resultZoom = $this->getAllView($id);
            while ($row = $this->getResultObject($resultZoom)) {
                $html .= '<a title="Image View">
                    <img style="cursor:pointer; border:solid 1px #666;" src="' . SITE_URL . __ZOOMPRODUCTTHUMB__ . $row->zoomImage . ' " title="view_' . $row->id . '" onclick="javascript:loadNewImage(\'' . SITE_URL . __ZOOMPRODUCTLARGE__ . $row->zoomImage . '\', \'' . SITE_URL . __ZOOMPRODUCTEXTRALARGE__ . $row->zoomImage . '\');"  />
                  </a>';
            }
            $html .='</div>
						
			</div>
			<div class="jersy-tshirt-mtarr-main-description">
                                <ul class="rip "><li><span>' . $categoryName . '</span></li></ul>
				<h3>' . $line->productName . '<span></span></h3>
				
				<h4>'.LANG_STYLE.'</h4>
				<ul class="style-list">				
					' . $line->productDesc . '
				</ul>
               
			<h4>'.LANG_PRODUCT_FABRIC_QUALITY.'</h4>';
            $palletId;
            while ($row = $this->getResultObject($fabricQuality)) {
                $i++;

                $style = $line->styleCode;
                $orderCode = explode("//", $row->fQuality);
                $xxx = substr($orderCode[0], 0, 3);
                $styleCode = str_replace("xxx", $xxx, $style);



                $html .='<div class="mittelschwerer">                                
                            <span>' . $row->fQuality . '<span>Ordering-code: ' . $styleCode . ' <span>> '.LANG_VP.': ' . $this->getValuePoint($row->percent, $line->productPrice) . '</span></span></span>
                            </div>';
                if ($i == 1) {
                    $palletId = $row->palletId;
                }
            }


            $html .='<h4 class="top-space">'.LANG_AVAILABLE_COLORS.'</h4>
					<ul class="standerd-faben">';
            $html .= $this->getPalletColor($palletId, $line->styleId);
            $html .='</ul>
			</div>
			<div class="varlanten">
				<h4>'.LANG_VARIANTS.':</h4>
				<ul>';

            $result = $this->executeQry("Select styleCode, styleImage, sign, percent from " . TBL_PRODUCT_STYLE . " where pId = '" . $prodId . "' and isDeleted = '0' and status = '1' and isBasic = '0' order by styleCode Limit 5");
            while ($row = $this->getResultObject($result)) {
                $styleImage = checkImgAvailable(__PRODUCTBASICTHUMB__ . $row->styleImage);
                $html .='<li>
                                              <a>
						 <img src="' . $styleImage . '" height="98" width="120" alt="image" />
                                              </a>
                                              <span style="margin-left:10px;">                                                                          <span>' . $row->styleCode . '</span>
                                              </span>
                                              <button class="preis-button" style="color:#000000; background-color:#000000;">PreisPunkte: ' . $this->getStyleValuePoint($line->productPrice, $row->percent, $row->sign) . '</button>
                                           </li>';
            }
            $html .='</ul>
			</div>';

            //$imageResult = '';
//            $path = $this->fetchValue(TBL_CATEGORY, "path", "id = '".$catId."'");
//            $gender = substr($path, 5, 1);
//         
//            $query = "select * from ".TBL_PRODUCT_PAGE_IMAGE." where catId = '".$gender."'";
            //$html .= $query;

            $query = "select * from " . TBL_PRODUCT_PAGE_IMAGE . " where catId = '" . $catId . "'";

            $resultSet = $this->executeQry($query);
            if ($resultSet) {
                $imageResult = $this->getResultObject($resultSet);
            } else {
                $resultSet = $this->executeQry("Select * from " . TBL_PRODUCT_PAGE_IMAGE . " where isDefault = '1'");
                $imageResult = $this->getResultObject($resultSet);
            }

            //showImage
            
//            $loadPageImageUrl = SITE_URL.'convert.php?url='.PAGEORIGINALIMAGEPATH . $imageResult->productPageImage.'&wid=424&hei=145';
//            $loadNewPageImageUrl = SITE_URL.'convert.php?url='.PAGEORIGINALIMAGEPATH . $imageResult->productPageImage.'&wid=424&hei=300';
            
            $loadPageImageUrl = SITE_URL . __PAGELARGEIMAGE__ . $imageResult->productPageImage;
            $loadNewPageImageUrl = __PAGEEXTRALARGEIMAGEPATH__. $imageResult->productPageImage;
            
             //$loadNewPageImageUrl = __PAGEEXTRALARGEIMAGEPATH__. 'MTA_BodyScale1.jpg';
             
            $loadPageSizeImageUrl = SITE_URL . __PAGELARGEIMAGE__ . $imageResult->sizeImage;
            $loadNewPageSizeImageUrl = __PAGEEXTRALARGEIMAGEPATH__. $imageResult->sizeImage;
            //MTA_Mearurment-Sketch.jpg
            //$loadNewPageSizeImageUrl = __PAGEEXTRALARGEIMAGEPATH__.'MTA_Mearurment-Sketch.jpg';
            
            $html .='<div class="different-size-weight">
				<a href="javascript:;"><img src="' . $loadPageImageUrl . '" height="145" width="424" alt="image" onclick="showImage(\''.$loadNewPageImageUrl.'\');"/></a>                    
			</div>
			<div class="different-size">
				<a href="javascript:;">	
				<img src="' . $loadPageSizeImageUrl . '" height="148" width="242"  alt="image" onclick="showImage(\''.$loadNewPageSizeImageUrl.'\');"/></a> 			</div>			
			<div class="subpage-logo">'.LANG_TEXT_FOR_SHIRT_LAB_IMAGE.'
			<!--<img src="images/shirtlab-won-text.jpg" height="62" width="179" alt="text" /> -->
				<a href="javascript:void(0)"><img src="images/subpage-logo.jpg" height="62" width="179" alt="logo" /></a> 
			</div>';            
            echo $html;
        } else {
            return "fail";
        }
    }

    function getCategoryMenu($args = '') {
        //print_r($args);
        unset($_SESSION[cId]);
        if ($args != '') {
            extract($args); //extracting values
            $flag = true;
        } else {
            $flag = true;
        }
        $outPut = '';
        $subOutPut = '';

        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '0' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";
        $result = $this->executeQry($query);

        $outPut .= '<div class="about-product" id="tab-menu">';
        $outPut .= '<ul>';
        if ($this->getTotalRow($result) > 0) {
            while ($row = $this->getResultObject($result)) {
//                echo "<pre>";
//                print_r($row);
                if ($flag) {
                    if ($row->id == $gender) {
                        unset($_SESSION[cId]);
                        $class = 'class="active classtab"';
                        $_SESSION[cId][] = $row->id;
                        $flag = false;
                    } else if ($gender == '') {
                        $class = 'class="active classtab"';
                        $_SESSION[cId][] = $row->id;
                        $flag = false;
                    } else {
                        $class = 'class="classtab"';
                    }
                    $outPut .= '<li><a ' . $class . ' href="product.php?' . $this->makeLink($row->path) . '" >' . $row->categoryName . '</a></li>';
                    $subOutPut = $this->getSubMenu($row->id, $args);
                } else {
                    $outPut .= '<li><a href="product.php?' . $this->makeLink($row->path) . '" class="classtab">' . $row->categoryName . '</a></li>';
                }
            }
        } else {
            
        }
        $outPut .= '</ul>';
        $outPut .= '</div>';

        echo $outPut;
        echo $subOutPut;
    }

    private $arr = array();

    function getSubMenu($pId, $param = '', $callCount = '1') {
        $html = '';
        $subHtml = '';
        $ctr = 1;
        $callCount = 1;
        $callCount++;
        if (count($param) > 0) {
            extract($param);
        }

        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '" . $pId . "' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";

        $result = $this->executeQry($query);
        $html .='<div class="tabbing-content">';
        if ($this->getTotalRow($result) > 0) {
            $html .= '<div id="tab1" class="hideall">';
            $html .= '<div class="about-product">';
            $html .= '<ul>';

            while ($line = $this->getResultObject($result)) {
                if ($ctr == 1 && $type == '') {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[type] = $this->level3($line->id, $param);
                } else if ($type == $line->id) {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[type] = $this->level3($line->id, $param);
                } else {
                    $class = 'class = ""';
                }
                $html .= '<li ' . $class . '><a href="product.php?' . $this->makeLink($line->path) . '">' . $line->categoryName . '</a></li>';
                $ctr++;
            }
            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';

            $html .= $this->arr[type];
            $html .= $this->arr[name];
            $html .= $this->arr[collar];
        } else {
            
        }
        $html .= '</div>';
        return $html . $subHtml;
    }

    function level3($pId, $param) {
        $ctr = 1;
        $html = '';
        if (count($param) > 0) {
            extract($param);
        }
        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '" . $pId . "' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            $html .='<div class="tabbing-content">';
            $html .= '<div id="tab1" class="hideall">';
            $html .= '<div class="about-product">';
            $html .= '<ul>';
            while ($line = $this->getResultObject($result)) {
                $class = '';
                //print_r($param);
                if ($ctr == 1 && $name == '') {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[name] = $this->level4($line->id, $param);
                } else if ($name == $line->id) {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[name] = $this->level4($line->id, $param);
                } else {
                    $class = 'class = ""';
                }

                $html .= '<li ' . $class . '><a href="product.php?' . $this->makeLink($line->path) . '">' . $line->categoryName . '</a></li>';
                $ctr++;
            }

            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    function level4($pId, $param) {
        $ctr = 1;
        $html = '';
        if (count($param) > 0) {
            extract($param);
        }
        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '" . $pId . "' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            $html .='<div class="tabbing-content">';
            $html .= '<div id="tab1" class="hideall">';
            $html .= '<div class="about-product">';
            $html .= '<ul>';
            while ($line = $this->getResultObject($result)) {
                $class = '';
                //~ echo "<pre>";print_r($param);
                if ($ctr == 1 && $collar == '') {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[collar] = $this->level5($line->id, $param);
                } else if ($collar == $line->id) {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                    $this->arr[collar] = $this->level5($line->id, $param);
                } else {
                    $class = 'class = ""';
                }

                $html .= '<li ' . $class . '><a href="product.php?' . $this->makeLink($line->path) . '">' . $line->categoryName . '</a></li>';
                $ctr++;
            }

            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    function level5($pId, $param) {
        $ctr = 1;
        $html = '';
        if (count($param) > 0) {
            extract($param);
        }
        $query = "Select C.id, C.path, C.parent_id, CD.categoryName from " . TBL_CATEGORY . " as C INNER JOIN " . TBL_CATEGORY_DESCRIPTION . " as CD ON (C.id = CD.catId) and C.parent_id = '" . $pId . "' and C.status = '1' and C.isDeleted = '0' and CD.langId = '" . $_SESSION['DEFAULTLANGUAGEID'] . "' order by C.sequence";
        $result = $this->executeQry($query);
        if ($this->getTotalRow($result) > 0) {
            $html .='<div class="tabbing-content">';
            $html .= '<div id="tab1" class="hideall">';
            $html .= '<div class="about-product">';
            $html .= '<ul>';
            while ($line = $this->getResultObject($result)) {
                $class = '';
                if ($ctr == 1 && $sleeve == '') {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                } else if ($sleeve == $line->id) {
                    $class = 'class = "active"';
                    $_SESSION[cId][] = $line->id;
                } else {
                    $class = 'class = ""';
                }

                $html .= '<li ' . $class . '><a href="product.php?' . $this->makeLink($line->path) . '">' . $line->categoryName . '</a></li>';
                $ctr++;
            }

            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    function makeLink($path) {
        //$path = '-1-2-7-12-';
        $path = substr($path, 1);
        $path = substr($path, 0, -1);
        $pathArr = explode('-', $path);
        $pos = 1;
        $linkArr = array();
        foreach ($pathArr as $val) {
            $linkArr[] = $this->giveAlias($pos, $val);
            $pos++;
        }
        $link = implode('&', $linkArr);
        return $link;
    }

    function giveAlias($pos, $val) {
        if ($pos == 1)
            return "gender=" . $val;
        else if ($pos == 2)
            return "type=" . $val;
        else if ($pos == 3)
            return "name=" . $val;
        else if ($pos == 4)
            return "collar=" . $val;
        else
            return "sleeve=" . $val;
    }

    function adToCollection($pId) {
        include('setlanguage.php');
        $arr = array();
        if (!$this->checkExistingCollection($pId)) {
            if (!isset($_SESSION[productId])) {
                $_SESSION['productId'] = array();
                $_SESSION['productId'][] = $pId;
                $arr = $_SESSION['productId'];
                echo LANG_ADD_TO_COLLECTION;
            } else {
                $arr = $_SESSION['productId'];
                if (in_array($pId, $arr)) {
                    //echo "fail";                    
                    echo LANG_ALREADY_IN_COLLECTION;                                    
                } else {
                    $_SESSION['productId'][] = $pId;
                    //echo "success";
                    echo LANG_ADD_TO_COLLECTION;
                }
            }
        } else {            
            echo LANG_ALREADY_IN_COLLECTION;
        }
    }

    function getValuePoint($percent, $price) {
        $valuePoint = ($price * $percent) / 100;
        return number_format($valuePoint, 4);
    }

    function getStyleValuePoint($price, $percent, $sign) {
        $value = $price * $percent / 100;

        if ($sign == 2) {
            $valuePoint = $price + $value;
        } else if ($sign == 0) {
            $valuePoint = $price - $value;
        } else {
            $valuePoint = $price;
        }
        return number_format($valuePoint, 4);
    }

    function checkExistingCollection($pId) {
        if ($_COOKIE ['collection'] != '') {
            $collectionName = $_COOKIE ['collection'];
            $query = "Select pId from " . TBL_COLLECTION . " where collectionName = '" . $collectionName . "' and pId = '" . $pId . "'";

            $result = $this->executeQry($query);
            $num = $this->getTotalRow($result);
            if ($num > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function changeImage() {
        echo $_GET['name'];
    }

}

// End Class
?>

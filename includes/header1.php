<?php
	ob_start();
	session_start();
	include_once('config/configure.php');
	include_once('includes/function/autoload.php');
	include('setlanguage.php');
	$genObj = new GeneralFunctions();
	if(isset($_POST['langSubmit']))
	{
		$genObj->changeCountryLanguage($_POST);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Shirtlab</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript">
  jQuery(".language a").live('click',function(){
			jQuery(".services li ul").slideToggle();	
	});
	function callFlashFunction()
	{
		alert('Hello');
		var flashObj = document.getElementById('flashContent');
		if(flashObj){
			flashObj.test();
		}
		else{
			return false;
		}
	}
</script>
</head>
<body>
<!--Wrapper Starts Here-->
<div id="wrapper">
  <div class="border-top">
    <div class="border-bottom">
      <div class="border-middle">
        <!--Page Starts Here-->
        <div class="page">
          <!--Header-->
          <div id="header">
            <h1><a href="index.php"><img src="images/logo.jpg" height="66" width="274" alt="logo" /></a></h1>
            <div class="services"> <span>
              <input type="text" value="type your text here"  onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" class="o-que"/>
              <input type="button" value="" />
              </span>
              <ul>
				  <li class="language"><?=$genObj->getDefaultLanuageLink($_SESSION['DEFAULTCOUNTRYID'],$_SESSION['DEFAULTLANGUAGEID']);?>
					  <!--<img style="float:left;" src="images/in.png" />
					  <a href="javascript:;">Language</a>-->
				  <form method="post" action="">
					  <input type="hidden" name="pageUrl" value="<?=urlencode(getCurPageUrl());?>" />
                	<ul>
                    	<li>
                        	<label>Select Country:</label>
                            <select name="country" onchange="return getLanguageList(this.value);">
								<?=$genObj->getMultiCountryList($_SESSION['DEFAULTCOUNTRYID']);?>
                            </select>
                        </li>
                        <li>
                        	<label>Select Language:</label>
                            <select name="language" id="languageList">
                            	<?php $genObj->getMultiLanguageList($_SESSION['DEFAULTCOUNTRYID']);?>
                            </select>
                        </li>
                        <input type="submit" name="langSubmit" value="Go" />
                    </ul>
                  </form>
                </li>
				<?php
					$class = '';
					$pageName = basename($_SERVER['PHP_SELF']);
					if($_SESSION['USER_ID'] == '') {
				?>
                <li class="create-account"><a href="register.php">Create Account</a></li>
                <li class="login"><a href="login.php">Login</a></li>
                <?php } else {?>
                <li class="create-account"><a href="myaccount.php">My Account</a></li>
                <li class="login"><a href="logout.php">Logout</a></li>
                <?php } ?>
                <li class="save"><a onclick="callFlashFunction();" href="javascript:;">Save</a></li>
              </ul>
            </div>
            <?php
				if($pageName != 'customize.php')
				{
            ?>
            <div class="navigation">
              <ul>
                <li <?=$pageName == 'product.php'? 'class = "active"':''?>><a href="product.php"><span>Product-Selector</span></a></li>
                <li><a href="customize.php"><span>Customize  Product</span></a></li>
                <li><a href="customize.php?ref=deco"><span>Add Deco on Product</span></a></li>
                <li <?=$pageName == 'quantitydeliverytime.php'? 'class = "active"':''?>><a href="quantitydeliverytime.php"><span>Quantity + Deliverytime</span></a></li>
                <li <?=$pageName == 'sendorder.php'? 'class = "active"':''?>><a href="sendorder.php"><span>Send Order</span></a></li>
              </ul>
            </div>
            <? } ?>
          </div>
          <!--Header-->

<?php
session_start();

function __autoload($class_name) {
if (!isset($_SESSION['DEFAULTLANGUAGECODE']))
{	
	$_SESSION['DEFAULTLANGUAGECODE'] = 'eng';
	$_SESSION['DEFAULTLANGUAGEID'] = 1;
	
}
    $file =  'includes/classes/'.$class_name . '.php';
  //  echo $file;
	if(file_exists($file)){ 
		require_once($file);
	}
	else{
		die('File Not Found');
	}
}

function checkImgAvailable($image)
{
	$imagePath = ABSOLUTEPATH.$image;
	if(file_exists($imagePath))
		return SITE_URL.$image;
	else
		return NOIMAGE;
}

function getCurPageUrl1()
{
	$pageUrl = $_SERVER['REQUEST_URI'];
	$pageUrlArr = explode('/',$pageUrl);
	$pageUrl = str_replace('/'.$pageUrlArr[1],'',$pageUrl);
	return substr($pageUrl,'1');
}

function getCurPageUrl()
{
	$pageUrl = $_SERVER['REQUEST_URI'];
	$pageUrlArr = explode('/',$pageUrl);
	return $pageUrl = array_pop($pageUrlArr);	
}

function redirect($page)
{	 
    if(!headers_sent())
        header("location:$page");
    else
        echo "<script>window.location.href='$page'</script>";echo 'fbdfvfv';exit;
}


function checkSession($ref = '') {
    require_once('includes/classes/Signup.php');
    $signupObj = new Signup();
    $sitepath = SITEURL;
    $chkcookie = $signupObj->checkUserCookies();
    if ((empty($_SESSION['USER_ID']) || empty($_SESSION['USER_HASH'])) && (!$chkcookie)) {
        $prev_page = basename($_SERVER['PHP_SELF']);
        if (!empty($_SERVER['QUERY_STRING'])) {
            $prev_page .= "?" . $_SERVER['QUERY_STRING'];
        }

    if($ref != '')
        $queryStr = '?ref='.$ref;
    else
        $queryStr = '';
    
        $_SESSION['PREV_PAGE_NAME'] = $_SERVER['HTTP_REFERER'];
        echo "<script language='javascript'>window.location='" . $sitepath . "login.php".$queryStr."';</script>";
        exit;
    }
}

function unsets($post,$unset_value){
	if(!is_array($unset_value)){$temp=$unset_value;$unset_value=array($temp);}
	if(!empty($post) && !empty($unset_value)){
		foreach($unset_value as $val){
			if(isset($post[$val])){
				unset($post[$val]);
			}
		}
	}
	return $post;
}

function postwithoutspace($post)
{
   foreach($post as $key=>$value)
	{
	  $value= htmlspecialchars(mysql_real_escape_string(trim($value)));
	  $value = str_ireplace("script", "blocked", $value);
	  $post[$key]= $value;
	}
	return  $post;
}
	
function getjsconstant()
{
	$tbjs= '<script type="text/javascript">
	var LANG_INVALID_EMAIL_ADDRESS_MSG="'.LANG_INVALID_EMAIL_ADDRESS_MSG.'";
	</script>';

	return $tbjs;
}



function displayWithStripslashes($post) {
	if($post){
   		foreach($post as $key=>$value) {
	  		$value=stripslashes(trim($value));
	  		$post[$key]= $value;
		}
	}
	return  $post;
}
		
	
function datediff($date1,$date2,$format='d'){
    $difference = abs(strtotime($date2) - strtotime($date1));
		switch (strtolower($format)){
			case 'd':
				$days = round((($difference/60)/60)/24,0);
				break;
			case 'm':
				$days = round(((($difference/60)/60)/24)/30,0);
				break;
			case 'y':
				$days = round(((($difference/60)/60)/24)/365,0);
				break;
		}
	return $days;
}

 
function msgSuccessFail($type,$msg){
// 0 for fail 1 for success 2 for notice
	if($type == '0'){
		$preTable = "<div class='message_box'><span>$msg</span></div>";
	} elseif($type == '1') {
		$preTable = "<div class='error-message'><span>$msg</span></div>";
	}elseif($type == '2') {
		$preTable = "<div class='message_box'><span>$msg</span></div>";
	}
	return $preTable;
}

function findexts($filename) {
  $filename = strtolower($filename) ;
  $exts = @split("[/\\.]", $filename) ;
  $n = count($exts)-1;
  $exts = $exts[$n];
  return $exts;
}

function getPageName() {	
	$pageName   = basename($_SERVER['PHP_SELF']);
	if ($pageName=='')
		$pageName = "index.php";
	return $pageName;	
}


function getImagePath($mainImage,$mainWidth,$mainHeight,$noImage,$noImageWidth,$noImageHeight) {
	$imageDetail = array();
	if(file_exists($mainImage)) {
		array_push($imageDetail,$mainImage);
		array_push($imageDetail,$mainWidth);
		array_push($imageDetail,$mainHeight);						
	} else {
		array_push($imageDetail,$noImage);
		array_push($imageDetail,$noImageWidth);
		array_push($imageDetail,$noImageHeight);							
	}	
	return $imageDetail;
}
 function ValidBrowser() {
 		$result = 1;	
	 if(strstr($_SERVER['HTTP_USER_AGENT'],'iPad') || strstr($_SERVER['HTTP_USER_AGENT'],'iPhone')) {   
	 	$result = 0;	 
	 }
	 return $result;		
 }


?>

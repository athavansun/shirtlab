							</div>
                        <!--Page Ends Here-->
                    </div>
                </div>
            </div>
            <!--Footer-->
            <div id="footer">
                <div class="page">
                    <div class="navigation-footer">
                        <h3><?= LANG_NAVIGATION ?></h3>
                        <ul>
                            <li><a target="_blank" href="<?=SITE_URL;?>"><?= LANG_HOME ?></a></li>
<!--                        <li><a href="product.php"><?= LANG_PRODUCT ?></a></li>			-->
                            <li><a target="_blank" href="aboutus.php"><?= LANG_ABOUT_US ?></a></li>
                            <li><a target="_blank" href="contactus.php"><?= LANG_CONTACT ?></a></li>
                            <li><a target="_blank" href="term.php"><?= LANG_TERMS_AND_CONDITIONS ?></a></li>
                            <li><a target="_blank" href="information.php"><?= LANG_INFORMATION ?></a></li>
                        </ul>
                    </div>
                    <div class="navigation-footer blog-feedfrom">
                        <h3><?= LANG_BLOG_FEED_FROM ?></h3>
                        <ul>
                            <li><a href="pellentesque-habitant-morbi.php" target="_blank"><?= LANG_BLOG?></a></li>
<!--                            <li><a href="vestibulum-tortor.php">Vestibulum tortor quam, feugiat vitae</a></li>-->
<!--                            <li><a href="norbi-tristique.php">Morbi tristique senectus et netus</a></li>-->
<!--                            <li><a href="et-malesuada-fames.php">Et malesuada fames ac turpis egestas </a></li>-->
                        </ul>
                    </div>
                    <div class="social-icon">
                        <h3><?= LANG_FOLLOW_US ?></h3>
                        <?php $genObj = new GeneralFunctions();?>
                        <ul>
                            <?php if($genObj->getSystemConfigValue('FACE_BOOK_STATUS')) { ?>
                            <li><a href="<?=$genObj->getSystemConfigValue('FACE_BOOK_URL');?>" target="_blank"><img src="images/facebook.png" height="29" width="29" title="facebook" alt="facebook" /></a></li>
                            <?php } if($genObj->getSystemConfigValue('TWITTER_STATUS')) { ?>                            
                            <li><a href="<?=$genObj->getSystemConfigValue('TWITTER_URL');?>" target="_blank"><img src="images/twitter.png" height="29" width="29" title="twitter" alt="twitter" /></a></li>
                            <?php } if($genObj->getSystemConfigValue('FLICKR_STATUS')) { ?>
                            <li><a href="<?=$genObj->getSystemConfigValue('FLICKR_URL');?>" target="_blank"><img src="images/flickr.png" height="29" width="29" alt="flickr" title="flickr" /></a></li>
                            <?php } if($genObj->getSystemConfigValue('PICASA_STATUS')) { ?>
                            <li><a href="<?=$genObj->getSystemConfigValue('PICASA_URL');?>" target="_blank"><img src="images/picassa.png" height="29" width="29" title="picasa" alt="picasa" /></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="news-letter">
                        <h3><?= LANG_NEWS_LETTER ?></h3>
                        <p><?= LANG_NEWLETTER_CONTENT ?></p>
                        <span>
							<form name="newsletter" id="newsletter" method="post" action="">
                            <input type="text" name="newsletteremail" id="newsletteremail" value="<?= LANG_EMAIL ?>" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" class="o-que subscribe-text"/>
                            <input type="button" id="newsId" name="subscribe" value="<?= LANG_SUBSCRIBE?>" class="subscribe-button"/>
                            </form>
                        </span>
                        <div id="newsInfo" style="color:#A91D1D;"></div>
                   </div>
                </div>
                <div class="footer-bottom">
                    <div class="page"> <span>© 2012  Shirtlab. All Rights Reserved.</span> </div>
                </div>
            </div>
            <!--Footer-->
        </div>
        <!--Wrapper Ends Here-->
        <?php include_once("js/newsletter-validation.php");  ?>	
        <input type="hidden" id="regex" value="LANG_NONE" />
        <input type="hidden" id="alertText" value="<?= LANG_FIELD_REQUIRED; ?>" />
        <input type="hidden" id="alertTextCheckboxMultiple" value="<?= LANG_SELECT_OPTION; ?>" />
        <input type="hidden" id="alertTextCheckboxe" value="<?= LANG_ALERT_CHECKBOX; ?>" />
        <input type="hidden" id="alertTextDateRange" value="<?= LANG_ALERTTEXTDATERANGE; ?>" />        
    </body>
</html>

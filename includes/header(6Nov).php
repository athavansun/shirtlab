<?php
ob_start();
session_start();
include_once('config/configure.php');
include_once('includes/function/autoload.php');
include('setlanguage.php');
$genObj = new GeneralFunctions();
//print_r($_POST);
if (isset($_POST['saveStatus'])) {    
    $genObj->changeCountryLanguage($_POST);
}
if (isset($_POST['proSearch'])) {
    header('Location:search.php?q=' . urlencode($_POST['keyword']));
    exit();
}
if (isset($_SESSION['saveSatus'])) {
    echo "session set";
}

$currentFile = $_SERVER["PHP_SELF"];
$parts = explode('/', $currentFile);

if (count($parts) > 0) {
    $currentPage = $parts[count($parts) - 1];
} else {
    $currentPage = $currentFile;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Welcome to Shirtlab</title>
        <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
        <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="js/ajax.js"></script>
        <script type="text/javascript" src="js/jquery_ajax.js"></script>
<?= getjsconstant(); ?>
        <script type="text/javascript">
            jQuery(".language a").live('click',function(){
                jQuery(".services li ul").slideToggle();	
            });
        
            function callFlashFunction()
            {
                //alert('Hello');
                var flashObj = document.getElementById('flashContent');
                if(flashObj){
                    //alert("save");
                    flashObj.saveCallBackPhp("save");
                }
                else{
                    return false;
                }
            }       
            
            function callFlashFunctionForLang()
            {
                //alert('Hello');
                var flashObj = document.getElementById('flashContent');
                if(flashObj){
                    //alert("save");
                    flashObj.saveCallBackPhp("lang");
                }
                else{
                    return false;
                }
            }  
        
            function xyz() {                
                var page = $("#pageUrl").val();
                if(page == 'customize.php') {                    
                    callFlashFunctionForLang();
                    checkStatus = setInterval(submitFrm,100);
                }else {
                    $("#langForm").submit();
                }
            }
            
            function submitFrm()
            {                
                var status = getCookie('saveStatus');
                if(status == 1) {                    
                    $("#langForm").submit();
                    clearInterval(checkStatus);
                }
            }
    
            function getCookie(c_name)
            {
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1)
                {
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1)
                {
                    c_value = null;
                }
                else
                {
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1)
                    {
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }

        </script>

    </head>
    <body>
        <!--Wrapper Starts Here-->
        <div id="wrapper">
            <div class="border-top">
                <div class="border-bottom">
                    <div class="border-middle">
                        <!--Page Starts Here-->
                        <div class="page">
                            <!--Header-->
                            <div id="header">
                                <h1><a href="index.php"><img src="images/logo.jpg" height="66" width="274" alt="logo" /></a></h1>
                                <div class="services">
                                    <span>                    
                                        <form method="post" action="" onsubmit="return validate();">
                                            <input autocomplete="off" type="text" id="keyword" name="keyword" value="<?php if ($_GET['q'] != '') {
    echo $_GET['q'];
} else {
    echo LANG_TYPE_YOUR_TEXT_HERE;
} ?>" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" class="o-que"/>
                                            <input type="submit" name="proSearch" value="" />
                                        </form>
                                    </span>
                                    <ul>
                                        <li class="language"><?= $genObj->getDefaultLanuageLink($_SESSION['DEFAULTCOUNTRYID'], $_SESSION['DEFAULTLANGUAGEID']); ?>
      <!--					<img style="float:left;" src="images/in.png" />
                                                <a href="javascript:;">Language</a>-->
                                            <form id="langForm" method="post" action="">
                                                <input type="hidden" name="saveStatus" id="saveStatus" value="" />
                                                <input type="hidden" id="pageUrl" name="pageUrl" value="<?= urlencode((getCurPageUrl()) ? getCurPageUrl() : "index.php"); ?>" />
                                                <ul>
                                                    <li>
                                                        <label><?= LANG_SELECT_COUNTRY ?></label>
                                                        <select name="country" onchange="return getLanguageList(this.value);">
<?= $genObj->getMultiCountryList($_SESSION['DEFAULTCOUNTRYID']); ?>
                                                        </select>
                                                    </li>
                                                    <li>
                                                        <label><?= LANG_SELECT_LANGUAGE ?></label>
                                                        <select name="language" id="languageList">
<?php $genObj->getMultiLanguageList($_SESSION['DEFAULTCOUNTRYID']); ?>
                                                        </select>
                                                    </li>
                                                  <!-- <input type="submit" name="langSubmit" id="langSubmit" value="<?= LANG_GO ?>" /> -->                      
                                                    <input type="button" name="langSubmit" id="langSubmit" value="<?= LANG_GO ?>" onclick="xyz();" /> 
                                                </ul>
                                            </form>
                                        </li>
                                        <?php
                                        $class = '';
                                        $pageName = basename($_SERVER['PHP_SELF']);
                                        if ($_SESSION['USER_ID'] == '') {
                                            ?>
                                            <li class="create-account"><a href="register.php<?= $_GET['ref'] == '' ? '' : '?ref=' . $_GET['ref']; ?>"><?= LANG_CREATE_ACCOUNT ?></a></li>
                                            <li class="login"><a href="login.php" onclick="javascript:return getAlertLoginMessage();"><?= LANG_LOGIN ?></a></li>
<?php } else { ?>
                                            <li class="create-account"><a href="myaccount.php"><?php echo LANG_MY_ACCOUNT ?></a></li>
                                            <li class="login"><a href="logout.php"><?= LANG_LOGOUT ?></a></li>
<?php } ?>
                                        <li class="save"><a onclick="javascript: callFlashFunction();" href="javascript:void(0);"><?= LANG_SAVE ?></a></li>
                                    </ul>
                                </div>
                                <?php
                                if ($pageName != 'customize.php') {
                                    ?>
                                    <!--            <div class="navigation">
                                                  <ul>
                                                    <li <?= $pageName == 'product.php' ? 'class = "active"' : '' ?>><a href="product.php"><span>Product-Selector</span></a></li>                
                                                    <li><a href="customize.php"><span>Customize  Product</span></a></li>
                                                    <li><a href="customize.php?ref=deco"><span>Add Deco on Product</span></a></li>
                                                    <li <?= $pageName == 'quantitydeliverytime.php' ? 'class = "active"' : '' ?>><a href="quantitydeliverytime.php"><span>Quantity + Deliverytime</span></a></li>
                                                    <li <?= $pageName == 'sendOrder.php' ? 'class = "active"' : '' ?>><a href="sendOrder.php"><span>Send Order</span></a></li>
                                                  </ul>
                                                </div>-->

                                    <div class="navigation">
                                        <ul>
                                            <li <?= $pageName == 'product.php' ? 'class = "active"' : '' ?>><a href="product.php"><span><?= LANG_PRODUCT ?></span></a></li>                
                                            <li><a href="customize.php"><span><?= LANG_CUSTOMIZE ?></span></a></li>
                                            <li><a href="customize.php?ref=deco"><span><?= LANG_PRODUCTDECO ?></span></a></li>
                                            <li <?= $pageName == 'quantitydeliverytime.php' ? 'class = "active"' : '' ?>><a href="quantitydeliverytime.php"><span><?= LANG_PRODUCTQUANITITY ?></span></a></li>
                                            <li <?= $pageName == 'sendOrder.php' ? 'class = "active"' : '' ?>><a href="sendOrder.php"><span><?= LANG_SENDORDER ?></span></a></li>
                                        </ul>
                                    </div>
                                <? } ?>
                                <input type="hidden" value="<?php echo stripslashes(LANG_ALL_THE_CHANGES_YOU_HAVE_MADE_WILL_BE_LOST) ?>" id="loginAlertMsg" />
                            </div>
                            <!--Header-->
                            <script type="text/javascript">
              
                                function getAlertLoginMessage(){
                                   
                                    var pageName="<?php echo $currentPage ?>";
						                                
                                    if(pageName=='customize.php'){
                                        alert($('#loginAlertMsg').val()); 
                                        return false;
                                    }                                    
                                    else
                                        return true;
                                }              
                            </script>
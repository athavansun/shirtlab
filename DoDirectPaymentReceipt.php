<?php

ob_start();
session_start();

/***********************************************************
DoDirectPaymentReceipt.php

Submits a credit card transaction to PayPal using a
DoDirectPayment request.

The code collects transaction parameters from the form
displayed by DoDirectPayment.php then constructs and sends
the DoDirectPayment request string to the PayPal server.
The paymentType variable becomes the PAYMENTACTION parameter
of the request string.

After the PayPal server returns the response, the code
displays the API request and response in the browser.
If the response from PayPal was a success, it displays the
response parameters. If the response was an error, it
displays the errors.

Called by DoDirectPayment.php.

Calls CallerService.php and APIError.php.

***********************************************************/

require_once('includes/function/autoload.php');

require_once('includes/classes/GeneralFunctions.php');
//	echo "<pre>";
//	print_r($_POST);die;

$orderObj = new QuantityDelivery();	
require_once 'CallerService.php';

$orderId = $_POST['orderId'];
$orderAmount = $orderObj->getOrderAmountDetail($orderId);   

$invoiceId = $orderAmount->invoiceAddId;
$userDetail = $orderObj->invoiceDetails($invoiceId);
    
$_SESSION[DEFAULTLANGUAGECODE] = 'eng';
$_SESSION[DEFAULTLANGUAGEID] = 1;

$totalamount = $orderAmount->ordTotal;
$expDateYear = $_POST['expiryYear'];
$padDateMonth = $_POST['expiryMonth'];
    

$currencyCode = $orderObj->fetchValue(TBL_CURRENCY_DETAIL, "currencyCode", " id = '".$orderAmount->currencyId."'");

$paymentType =urlencode( $_POST['paymentType']); 
$creditCardType =urlencode($_POST['cardType']);
$creditCardNumber = urlencode($_POST['cardNumber']);
$cvv2Number = urlencode($_POST['cardCVV']); 
$amount = urlencode($totalamount);

$orderId=urlencode($orderId); 
$uid=urlencode($orderAmount->userId);


$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".$padDateMonth.$expDateYear."&CVV2=$cvv2Number";

/* Make the API call to PayPal, using API signature.
   The API response is stored in an associative array called $resArray */
$resArray=hash_call("doDirectPayment",$nvpstr);

/* Display the API response back to the browser.
   If the response from PayPal was a success, display the response parameters'
   If the response was an error, display the errors received using APIError.php.
   */
$ack = strtoupper($resArray["ACK"]);

if($ack!="SUCCESS")  { 
    $_SESSION['RESPONSETXT']=$ack;
	 echo "<script>window.location.href='cancelled.php'</script>";	 
   }
else{
$orderObj->saveFinalOrder($orderId, "paypal", $resArray['TRANSACTIONID']);

//echo "<script>window.location.href='thanks.php?oid=".base64_encode($orderId)."'</script>";	
}  
   

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" > 
    <title>DoDirectPayment</title>
   
	<?=headContent();?>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" > 
<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
<body>

<!--page start here-->
<div id="page">

	<div id="outer-container">

<!--page start here-->
		<div id="container">
			<?php include_once("includes/header.php");?>
	           <div id="content">
				  
                  
               <div class="content-mid">
                    
                    	<div class="content-mid-left">
                        <fieldset>
                        <legend>Thank you for your payment!</legend>
						

        <?php 
   		 	require_once 'ShowAllResponse.php';
   		 ?>
 </fieldset>
                        </div>
		  <?php include_once("includes/right.php")  ?>
                      </div>
                    
           

               </div>
              
               		
		</div>
 <div id="footer">
 
                	<?php include_once("includes/footer.php");?>
                </div>
</div>

</div></div>

</body>

</html>
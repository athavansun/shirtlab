<?php include_once('includes/header.php'); ?>
<?php    
        if(isset($_SESSION['USER_ID']) && $_SESSION['USER_ID'] != 0) {
            echo"<script>window.location.href='myaccount.php'</script>";exit;
        }
        
        $targetUrl = $_SERVER[HTTP_REFERER];
            
	$signupObj=new Signup;
	if(isset($_POST['login'])){           
        require_once('validation_class.php');
        $obj = new validationclass();
            $obj->fnAdd("loginEmail", $_POST["loginEmail"], "req", LANG_THIS_FIELD_IS_REQUIRED_MSG);
            $obj->fnAdd('loginEmail',$_POST['loginEmail'], 'email', LANG_INVALID_EMAIL_ADDRESS_MSG);
            $obj->fnAdd('loginPassword',$_POST['loginPassword'], 'req','Invalid Password.');

            $arr_error = $obj->fnValidate();
            $str_validate = (count($arr_error)) ? 0 : 1;

            $arr_error[loginEmail] = $obj->fnGetErr($arr_error[loginEmail]);
            $arr_error[loginPassword] = $obj->fnGetErr($arr_error[loginPassword]);

            #isEmail Exists-----------------------------------
            if(!$signupObj->isUserEmailExistPhp($_POST[loginEmail])){
                    $arr_error[loginEmail]= '<span class="alert-red alert-icon">'.LANG_EMAIL_DOES_NOT_EXISTS_MSG.'</span>';
                    $str_validate=0;
            }

            if($str_validate){            
                    $_POST = postwithoutspace($_POST);                
                    $signupObj->loginRegisterUser($_POST);
            }
	}
	
	if(isset($_COOKIE['cookuseremail']) && isset($_COOKIE['cookuseremail']))
	{
		$_POST['loginEmail'] = $_COOKIE['cookuseremail'];
		$password = $_COOKIE['cookuserpassword'];
		$sel = 'checked = "checked"';
	}
	else
	{
		$_POST['loginEmail'] = '';
		$password = '';
	}
        $queryStr = isset($_GET['ref']) ? $_GET['ref'] : $_POST['isSave'];
?>
<div id="content" >
	<div class="subpage-border-top">
		<div class="subpage-border-bottom">
			<div class="subpage-border-mid">
				<div class="loginpage-bottom">
					<h2><?= LANG_LOGIN?></h2>
						<div class="loginpage-mid">
							 <?php echo $_SESSION['ERROR_MSG']; unset($_SESSION['ERROR_MSG']); ?>
							<span><?= LANG_ENTER_YOUR_EMAIL_ADDRESS_AND_PASSWORD_TO_LOGIN ?></span>
							
							<form name="loginForm" id="loginForm" method="POST" action="">
                            <input type="hidden" name="isSave" value="<?=$queryStr?>" />
							<ul>
							<li>
							<label><?= LANG_EMAIL?></label>
							<div class="right_input">
							<input type="text" value="<?=$_POST['loginEmail'];?>" name="loginEmail" class="o-que email-text" />
							</div>
							<?=$arr_error[loginEmail];?>
							</li>
							<li>
							<label><?= LANG_PASSWORD_LOGIN;?></label>
							<div class="right_input">
							<input type="password" value="<?=$password;?>" name="loginPassword" class="o-que email-text" />
							</div>
							<?=$arr_error[loginPassword];?>
							<a href="forgetpassword.php" class="forget-password"><?= '('.LANG_FORGET_YOUR_PASSWORD.')'?></a>                                    </li>
							<li>
							<div class="remember"><input type="checkbox" <?=$sel;?> name="rememberme" /><?= LANG_REMEMBER_ME?></div>
							<div class="login-button"><input name="login" type="submit" value="<?= LANG_LOGIN?>" /></div>
							</li>
						  </ul>
                                <input type="hidden" name="targetUrl" value="<?= $targetUrl;?>" />
						  </form>
						</div>
				</div>
				
			</div>
		</div>
	</div>
  <!--Content-->
</div>
<?php include_once('includes/footer.php'); ?>
